import Vue from "vue";
import Vuex from "vuex";
import rule14 from "../assets/jsonFile/rule14.json";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    testCase: "",
    titleHeader: "",
    roles: [],
    timeTest: null,
    jsonDataTest: [],
    selectCase: { requestHeader: "", requestBody: "" },
    agenData: {
      agentCode: "",
      agentName: "",
      agentGroup: "",
      agentGroupName: "",
      applicationNumber: "",
      agentLicenceNumber: "",
    },
    requestHeader: {
      transactionId: null,
      registeredSystem: "",
      section: "",
    },
    basicInsureDetail: {
      planCode: "",
      planName: "",
      insuredAmount: "",
      premium: "",
      premiumAmount: "",
      topupPremiumAmount: "",
      planType: "",
      premiumIndicator: "",
      premiumPayment: "",
      planHeaderProductDetail: {
        planCode: "",
      },
    },
    beneficiaryDetails: [],
    payorBenefitPersonalData: {
      idCard: "",
      fullName: "",
      provinceCode: "",
      sex: "",
      age: "",
      birthday: "",
      weight: "",
      birthWeight: "",
      height: "",
      occupationCode: "",
      clientNumber: "",
      nationality: "",
      ageInMonth: "",
      firstName: "",
      lastName: "",
      enFirstName: "",
      enLastName: "",
      enMiddleName: "",
      maritalStatus: "",
      minorMaritalStatus: "",
      healthQuestion: "",
      payorRelationship: "",
      isIDCard: "",

      addressDetail: {
        buildingName: "",
        floor: "",
        no: "",
        moo: "",
        houseNumber: "",
        avenue: "",
        street: "",
        district: "",
        subDisctrict: "",
        provinceCode: "",
        postalCode: "",
        telephoneNumber: "",
        mobileNumber: "",
        email: "",
        fax: "",
      },

      // health: {
      //   alcohol: "",
      //   alcoholType: "",
      //   otherSymptoms: "",
      //   medicalHistoryIllness: "",
      //   medicalHistoryExamination: "",
      //   criticalIllnessDisease: "",
      //   rejectedDeclinedPostponed: "",
      //   sixMonthChange: "",
      //   sixMonthChangeIncreased: "",
      //   sixMonthChangeDecreased: "",
      // },
      pbHealth: {
        criticalIllnessDisease: "",
        visionDisorderFemale: "",
        symptomFemaleList: "",
        medicalHistory: "",
        medicalHistoryExamination: "",
        rejectedDeclinedPostponed: "",
        alcohol: "",
        alcoholQuit: "",
        alcoholDateQuit: "",
        alcoholType: "",
        alcoholQuantity: "",
        alcoholFrequency: "",
        sixMonthChange: "",
        sixMonthChangeIncreased: "",
        sixMonthChangeKgIncreased: "",
        sixMonthChangeDecreased: "",
        sixMonthChangeKgDecreased: "",
      },
      fatca: {
        usNationalityAndBirthCountry: "",
        usNationalityAndBirthCountryDetail: "",
        usGreenCard: "",
        obligationToPayTax: "",
        residentForTaxUSCollection: "",
        pbUSNationalityAndBirthCountry: "",
        pbUSNationalityAndBirthCountryDetail: "",
        pbUSGreenCard: "",
        pbObligationToPayTax: "",
        pbResidentForTaxUSCollection: "",
      },
    },
    personalData: {
      idCard: "",
      fullName: "",
      provinceCode: "",
      sex: "",
      age: "",
      birthday: "",
      weight: "",
      birthWeight: "",
      height: "",
      healthQuestion: "",
      mobileNumber: "",
      occupationCode: "",
      clientNumber: "",
      nationality: "",
      ageInMonth: "",
      firstName: "",
      lastName: "",
      enFirstName: "",
      enLastName: "",
      enMiddleName: "",
      maritalStatus: "",
      minorMaritalStatus: "",
      isIDCard: "",
      health: {
        drugDealing: "",
        drugIntake: "",
        alcohol: "",
        alcoholQuit: "",
        alcoholDateQuit: "",
        alcoholType: "",
        alcoholQuantity: "",
        alcoholFrequency: "",
        smoking: "",
        smokingQuit: "",
        smokePerDay: "",
        smokePeriod: "",
        sixMonthChange: "",
        sixMonthChangeIncreased: "",
        sixMonthChangeKgIncreased: "",
        sixMonthChangeDecreased: "",
        sixMonthChangeKgDecreased: "",
        insuredIllness: "",
        otherSymptoms: "",
        medicalHistoryIllness: "",
        medicalHistoryExamination: "",
        criticalIllnessDisease: "",
        rejectedDeclinedPostponed: "",
        drugProblem: "",
        drugType: "",
        spouseHIV: "",
        spouseHepatitis: "",
        criticalIllnessDetailList: [],
        medicalHistoryExaminationDetailList: [],
        medicalHistoryIllnessSymptomDetailList: []
      },
      addressDetail: {
        buildingName: "",
        floor: "",
        no: "",
        moo: "",
        houseNumber: "",
        avenue: "",
        street: "",
        district: "",
        subDisctrict: "",
        provinceCode: "",
        postalCode: "",
        telephoneNumber: "",
        mobileNumber: "",
        email: "",
        fax: "",
      },
      presentAddressDetail: {
        buildingName: "",
        floor: "",
        no: "",
        moo: "",
        houseNumber: "",
        avenue: "",
        street: "",
        district: "",
        subDisctrict: "",
        provinceCode: "",
        postalCode: "",
        telephoneNumber: "",
        mobileNumber: "",
        email: "",
        fax: "",
      },
      officeAddressDetail: {
        buildingName: "",
        floor: "",
        no: "",
        moo: "",
        houseNumber: "",
        avenue: "",
        street: "",
        district: "",
        subDisctrict: "",
        provinceCode: "",
        postalCode: "",
        telephoneNumber: "",
        mobileNumber: "",
        email: "",
        fax: "",
      },
      fatca: {
        usNationalityAndBirthCountry: "",
        usNationalityAndBirthCountryDetail: "",
        usGreenCard: "",
        obligationToPayTax: "",
        residentForTaxUSCollection: "",
        pbUSNationalityAndBirthCountry: "",
        pbUSNationalityAndBirthCountryDetail: "",
        pbUSGreenCard: "",
        pbObligationToPayTax: "",
        pbResidentForTaxUSCollection: "",
      },
    },
    riderInsureDetails: [
      // {
      // 	planCode: '',
      // 	planName: '',
      // 	insuredAmount: '',
      // 	premiumAmount: '',
      // 	topupPremiumAmount: '',
      // 	planType: '',
      // 	premiumIndicator: ''
      // }
    ],

    requestBody: {
      appNumber: "",
      agentCode: "",
      requestDate: "",
      appType: "",
      appNo: "",
      clientRegionAddress: "",
      clientHouseRegistration: "",
      clientAllAddress: ""
    },
  },
  mutations: {
    // addTestCase(state, data) {
    // 	state.testCase.push(data);
    // },

    setTestCase(state, data) {
      state.testCase = data;
      state.riderInsureDetails = []
      switch (data) {
        case "RULE_03":
          {
            state.requestHeader = {
              registeredSystem: "RULE03",
              section: "3",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 03 : ตรวจสอบทุนประกัน";
          }
          break;
        case "RULE_04":
          {
            state.requestHeader = {
              registeredSystem: "RULE04",
              section: "4",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 04 : งวดการชำระเงินเบี้ยประกันภัย";
          }
          break;
        case "RULE_05":
          {
            state.requestHeader = {
              registeredSystem: "RULE05",
              section: "5",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 05 : ช่องทางการขาย";
          }
          break;
        case "RULE_06":
          {
            state.requestHeader = {
              registeredSystem: "RULE06",
              section: "6",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 06 : ตรวจสุขภาพ";
          }
          break;
        case "RULE_07":
          {
            state.requestHeader = {
              registeredSystem: "RULE07",
              section: "7",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 07 : MIB";
          }
          break;
        case "RULE_11":
          {
            state.requestHeader = {
              registeredSystem: "RULE11",
              section: "11",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 11 : ตรวจสอบข้อมูลผู้เอาประกัน";
          }
          break;
        case "RULE_12":
          {
            state.requestHeader = {
              registeredSystem: "RULE12",
              section: "12",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 12 : ประวัติการ Claim ";
          }
          break;
        case "RULE_13":
          {
            state.requestHeader = {
              registeredSystem: "RULE13",
              section: "13",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 13 : อาชีพที่ต้องขอเอกสารเพิ่มเติม";
          }
          break;
        case "RULE_14":
          {
            state.requestHeader = {
              registeredSystem: "RULE14",
              section: "14",
            };
            state.jsonDataTest = rule14;
            state.titleHeader = "Rule 14 : เอกสารทางการเงิน";
          }
          break;

        case "RULE_15":
          {
            state.requestHeader = {
              registeredSystem: "RULE15",
              section: "15",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 15 : ข้อมูลแบบประกันซ้ำ ";
          }
          break;

        case "RULE_16":
          {
            state.requestHeader = {
              registeredSystem: "RULE16",
              section: "16",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 16 : ตรวจสอบข้อมูลผู้ชําระเบี้ยประกัน";
          }
          break;
        case "RULE_17":
          {
            state.requestHeader = {
              registeredSystem: "RULE17",
              section: "17",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 17 : ความสัมพันธ์ผู้รับผลประโยชน์";
          }
          break;
        case "RULE_20":
          {
            state.requestHeader = {
              registeredSystem: "RULE20",
              section: "20",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 20 : fatca";
          }
          break;
        case "RULE_21":
          {
            state.requestHeader = {
              registeredSystem: "RULE21",
              section: "21",
            };
            state.jsonDataTest = [];
            state.titleHeader = "Rule 21 : KYC";
          }
          break;
        default:
          state.requestHeader = {
            transactionId: "",
            registeredSystem: "",
            section: "",
          };
          break;
      }
      state.testCase = data;
    },
    setRole(state, data) {
      state.roles = data;
    },
    setAgenData(state, data) {
      state.agenData = { ...state.agenData, ...data };
    },
    setHeader(state, data) {
      state.requestHeader = { ...state.requestHeader, ...data };
    },
    setInsureDetail(state, data) {
      state.basicInsureDetail = { ...state.basicInsureDetail, ...data };
    },
    setDeneficiaryDetails(state, data) {
      state.beneficiaryDetails = data;
    },
    setPBPersonalData(state, data) {
      state.payorBenefitPersonalData = {
        ...state.payorBenefitPersonalData,
        ...data,
      };
    },
    setPersonalData(state, data) {
      state.personalData = { ...state.personalData, ...data };
    },
    setRiderInsureDetails(state, data) {
      state.riderInsureDetails = data;
    },
    setCriticalIllnessDetailList(state, data) {
      state.personalData.health.criticalIllnessDetailList = data;
    },
    setMedicalHistoryExaminationDetailList(state, data) {
      state.personalData.health.medicalHistoryExaminationDetailList = data;
    },
    setMedicalHistoryIllnessSymptomDetailList(state, data) {
      state.personalData.health.medicalHistoryIllnessSymptomDetailList = data;
    },
    setRequestBody(state, data) {
      state.requestBody = data;
    },
    setSelectCase(state, data) {
      state.requestBody.agentCode = data.requestBody.agentCode;
      state.requestBody.appNumber = data.requestBody.appNumber;
      state.requestBody.appType = data.requestBody.appType;
      state.requestBody.requestDate = data.requestBody.requestDate;
      state.personalData = {
        ...state.personalData,
        ...data.requestBody.personalData,
      };
    },
    setTimeTest(state, data) {
      console.log(data);
      state.requestHeader.transactionId = data
      state.timeTest = data
    }
  },
  actions: {
    // addTestCase(context, data) {
    // 	context.commit('addTestCase', data);
    // },
    setTestCase(context, data) {
      context.commit("setTestCase", data);
    },
    setRole(context, data) {
      context.commit("setRole", data);
    },
    setAgenData(context, data) {
      context.commit("setAgenData", data);
    },
    setHeader(context, data) {
      context.commit("setHeader", data);
    },
    setInsureDetail(context, data) {
      context.commit("setInsureDetail", data);
    },
    setDeneficiaryDetails(context, data) {
      context.commit("setDeneficiaryDetails", data);
    },
    setPBPersonalData(context, data) {
      context.commit("setPBPersonalData", data);
    },
    setPersonalData(context, data) {
      context.commit("setPersonalData", data);
    },
    setRiderInsureDetails(context, data) {
      context.commit("setRiderInsureDetails", data);
    },
    setCriticalIllnessDetailList(context, data) {
      context.commit("setCriticalIllnessDetailList", data);
    },
    setMedicalHistoryExaminationDetailList(context, data) {
      context.commit("setMedicalHistoryExaminationDetailList", data);
    },
    setMedicalHistoryIllnessSymptomDetailList(context, data) {
      context.commit("setMedicalHistoryIllnessSymptomDetailList", data);
    },
    setRequestBody(context, data) {
      context.commit("setRequestBody", data);
    },
    setSelectCase(context, data) {
      context.commit("setSelectCase", data);
    },
    setTimeTest(context, data) {
      context.commit("setTimeTest", data);
    }
  },
  modules: {},
  methods: {},
});
