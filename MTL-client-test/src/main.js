import Vue from 'vue'
import App from './App.vue'
import Loading from './components/Loading/Loading.vue'
import VueLoading from 'vuejs-loading-plugin'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './plugins/element.js'
import store from './store'
Vue.config.productionTip = false
Vue.use(BootstrapVue);

Vue.use(VueLoading, {
  dark: true, // default false
  text: 'Ladataan', // default 'Loading'
  loading: false, // default false
  customLoader: Loading, // replaces the spinner and text with your own
  background: 'rgb(248, 249, 249,0.9)', // set custom background
  classes: ['myclass'] // array, object or string
})

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
