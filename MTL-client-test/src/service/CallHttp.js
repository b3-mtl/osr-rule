import ApiMain from './ApiMain';
import ApiMaster from './Api/ApiMaster';
import ApiMapping from './Api/ApiMapping';
import ApiSubmit from './Api/ApiSubmit';

export default async () => {
     const baseUrl = 'http://10.200.110.85:9080/cms-service/';
     //   let token = get(JSON.parse(localStorage.getItem('userData')),'token')
     let token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6IlJPTEVfQURNSU4iLCJvcmdhbml6YXRpb24iOiJDaGFpbmdtYWkiLCJpYXQiOjE1ODc1MjE1MjYsImV4cCI6MTU4NzUzOTUyNn0.55ovPNMt6lgfxAPit-O5zHYS_gQ9G3Yhoc6YjgoQCs0"
     const APIToken = await ApiMain(baseUrl,
          {
               authorization: "Bearer " + token,
          })
     //http://10.200.110.81:9080
     const BWServer = await ApiMain('http://10.200.110.85:9080/mtl-osr/',
          {
               //  authorization:"Bearer "+token,
          })
     return {
          ...ApiMaster(APIToken),
          ...ApiMapping(APIToken),
          ...ApiSubmit(BWServer)
     }
}
