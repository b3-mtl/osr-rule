export default (Api)=>{
    const getAllRole = async()=>{
      return await Api.get('api/ruleMaster/get-all')
    }

    const getAllChannel = async()=>{
      return await Api.get('api/channelMaster/get')
    }
    
    return {
      getAllRole,
      getAllChannel
    }
}