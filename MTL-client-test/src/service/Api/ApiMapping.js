export default (Api) => {
	const getMappRuleChannel = async (channelCode) => {
		return await Api.post('api/channelMapping/get-all-Rule-Channel',{channelCode});
	};
	return {
		getMappRuleChannel
	};
};
