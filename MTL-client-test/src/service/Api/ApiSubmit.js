export default (Api)=>{
    const submitTest = async (object) => {
        console.log(object)
		return await Api.post('api/underwriting/execute',object);
	};
    return {
        submitTest
    }
}