package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RulesResponse {
	
	private  List<Message>  messageList;
	private  List<Document>  documentList;
	private  String referUnderwriter;
	
    private int messageCount;   
	private int documentCount;  
	
	private String allRuleExecuteLog;
}
