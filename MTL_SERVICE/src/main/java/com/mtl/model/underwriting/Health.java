package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Health {
	
	 String drugDealing;
	 String drugIntake;
	 String drugIntakeQuit;
	 String alcohol;
	 String alcoholQuit;
	 String smoking;
	 String smokingQuit;
	 String sixMonthChange;
	 String sixMonthChangeIncreased;
	 String sixMonthChangeDecreased;
	 String insuredIllness;
	 String otherSymptoms;
	 String medicalHistoryIllness;
	 String medicalHistoryExamination;
	 String criticalIllnessDisease;
	 String visionDisorderFemale;
	 String rejectedDeclinedPostponed;
	 String drugProblem;
	 String symptomFemaleListPayors;
	 String symptomFemaleList;
	 String symtonName;
	 String medicalHistoryPayor;
	 String medicalHistory;
	 String medicalHistoryList;
	 String rate;
	 String alcoholType;
	 String alcoholQuantity;
	 String alcoholFrequency;
	 String sixMonthChangeKgIncreased;
	 String sixMonthChangeKgDecreased;
	 String drugType;
	 String smokePerDay;
	 String smokePeriod;
	 String spouseHIV;
	 String spouseHepatitis;
	 List<CriticalIllnessDisease> criticalIllnessDetailList;
	 List<MedicalHistoryExamination> medicalHistoryExaminationList;
	 List<MedicalHistoryExamination> medicalHistoryExaminationDetailList;
	 List<MedicalHistoryExamination> medicalHistoryIllnessSymptomDetailList;
	 String alcoholDateQuit;
	 String smokingDateQuit;
	 
}
