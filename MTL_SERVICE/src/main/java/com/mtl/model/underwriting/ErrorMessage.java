package com.mtl.model.underwriting;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorMessage { 
 
	private List<ErrorMessageDesc> errorList;
}
