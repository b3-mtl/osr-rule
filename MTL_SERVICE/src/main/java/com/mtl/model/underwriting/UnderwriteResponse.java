package com.mtl.model.underwriting;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnderwriteResponse  {
    
	// Technical Information
    private ResponseHeader responseHeader;
    // Service Result information
    private ResponseBody responseBody;
	
//	private List<String> log;
	@JsonIgnore
	private Long timeUse;
	
	public UnderwriteResponse(List<String> message) {
		super();
		 
//		this.log = new ArrayList<String>();
	}

	public UnderwriteResponse() {
		super();
		 
//		this.log = new ArrayList<String>();
	}
}
