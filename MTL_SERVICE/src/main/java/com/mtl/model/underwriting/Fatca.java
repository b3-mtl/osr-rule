
package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fatca {

	private String usNationalityAndBirthCountry;
	private String usNationalityAndBirthCountryDetail;
	private String usGreenCard;
	private String obligationToPayTax;
	private String residentForTaxUSCollection;
	private String pbUSNationalityAndBirthCountry;
	private String pbUSNationalityAndBirthCountryDetail;
	private String pbUSGreenCard;
	private String pbObligationToPayTax;
	private String pbResidentForTaxUSCollection;

}
