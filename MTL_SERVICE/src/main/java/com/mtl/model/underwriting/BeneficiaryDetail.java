package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BeneficiaryDetail {	
	
	private String idCard;
	private String fullName;
	private String firstName;
	private String lastName;
	private String enFirstName;
	private String enLastName;
	private String age;
	private String address;
	private String percent;
	private String relation;
	
	private List<String> mibCodeList;// fillter 80%
	private List<String> mibAmloCodeList; //Amlo only -> fillter 90%

}
