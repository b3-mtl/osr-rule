package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicalHistoryExamination {
	
	String examinationName;
	String examinationDate;
	String physicianOpinion;
	String symptomName;
	String treatmentDate;
	String medicalHistoryName;
	String hospitalName;
	String result;
	String doctorName;


}
