package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CriticalIllnessDisease {
	
	String diseaseName;
	String treatmentDate;
	String treatmentAndCurrentSymptoms;
	String hospitalName;


}
