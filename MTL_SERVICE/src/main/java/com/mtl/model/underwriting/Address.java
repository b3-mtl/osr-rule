
package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {

//	private String buildingName;
//	private String floor;
//	private String no;
	private String moo;
	private String houseNumber;
//	private String avenue;
//	private String street;
	private String district;
	private String subDisctrict;
	private String provinceCode;
	private String clientHouseRegistration; // rule21 kyc
	private String clientAllAddress; // rule21 kyc
	private String clientRegionAddress; // rule21 kyc
//	private String postalCode;
//	private String telephoneNumber;
//	private String email;
//	private String fax;

}
