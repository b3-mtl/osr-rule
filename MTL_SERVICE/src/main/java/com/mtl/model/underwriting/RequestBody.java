
package com.mtl.model.underwriting;

import java.util.List;

import com.mtl.model.common.AgentInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestBody {
	
 
	private String appNo;
	private String appForm;
	private String agentCode;
	private String requestDate;// วันที่ขอเอาประกัน (วันที่เขียนเอกสารใบคำขอเอาประกันชีวิต)
	private PersonalData personalData;
	private PBPersonalData payorBenefitPersonalData;
	private String ClientHouseRegistration;
	private String ClientAllAddress;
	private String ClientRegionAddress;
	private List<BeneficiaryDetail> beneficiaryDetails;
	//private PayerData payerData;
	private InsureDetail basicInsureDetail;
	private List<InsureDetail> riderInsureDetails;
//	private AgentInfo agentInfo;

}
