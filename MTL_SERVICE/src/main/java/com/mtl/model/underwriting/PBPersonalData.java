
package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PBPersonalData {

	private String idCard;
	private String fullName;
	private String provinceCode;
	private String sex;
	private String age;
	private String birthday;
	private String weight;
	private String birthWeight;
	private String height;
	private String occupationCode;
	private String clientNumber;
	private String nationality;
	private String ageInMonth;
	private String firstName;
	private String lastName;
	private String enFirstName;
	private String enLastName;
	private String enMiddleName;
	private String maritalStatus;
	private String minorMaritalStatus;
	private String isIDCard; 
	private String mobileNumber;
	private String payorRelationship;
	
	private Address addressDetail;
	private Address presentAddressDetail;
	private Address officeAddressDetail;
	private Fatca fatca;

	private String healthQuestion;
	private Health pbHealth;
	
	private List<String> mibCodeList;
	private List<String> mibAmloCodeList;
	private String[]  claimCodeList;

}
