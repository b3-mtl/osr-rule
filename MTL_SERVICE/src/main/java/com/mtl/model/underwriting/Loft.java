package com.mtl.model.underwriting;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Loft {
	
	private String idCard;
	private String cardType;
	private String planCode;
	private long sumInsured;
	private String agentCode;
	private Date submitDate;
	private String submitSystem;
	private String appStatus;
}
