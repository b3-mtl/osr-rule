
package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnderwriteRequest  {


    private RequestHeader requestHeader;
 
    private RequestBody requestBody;

 
	
}
