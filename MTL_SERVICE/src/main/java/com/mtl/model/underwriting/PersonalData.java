
package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonalData {

	private String idCard;
	private String fullName;
	private boolean isFlagClaim;
	private String sex;
	private String age;
	private String birthday;
	private String weight;
	private String birthWeight;
	private String height;
	private String occupationCode;
	private String clientNumber;
	private String nationality;
	private String ageInMonth;
	private String ageDay;
	private String firstName;
	private String lastName;
	private String enFirstName;
	private String enLastName;
	private String enMiddleName;
	private String maritalStatus;
	private String minorMaritalStatus;
	private String isIDCard;
	private String mobileNumber;
	private String additionalInformation;
	private String foreignIdCard;
	
	private Address addressDetail;
	private Address presentAddressDetail;
	private Address officeAddressDetail;
	
	private String healthQuestion;
	private Health health;
	
	private Fatca fatca;
	
	
	private List<String> mibCodeList;
	private List<String> mibAMLOCodeList;
	private String[]  claimCodeList;
	
	

}
