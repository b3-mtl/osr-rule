
package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestHeader {

	private String transactionId;
	private String registeredSystem;
	private String section;

 
}
