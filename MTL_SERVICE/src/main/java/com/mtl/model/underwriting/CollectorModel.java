package com.mtl.model.underwriting;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import com.mtl.collector.model.ClientProfile;
import com.mtl.collector.ws.codegen.getMIB.MibResultList;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import lombok.Data;

@Data
public class CollectorModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private String[] personClaimCodes = null;
	private MibResultList[] mibResultList = null;
	private List<String> personMIBCodeList = null;
	private List<String> personMIBAmloCodeList = null;
	private ClientProfile clientProfile = null;
    private List<HistoryInsure> historyInsureList;
    private AgentInfo agentDetail;
    private BigDecimal historyInsure;
    
}
