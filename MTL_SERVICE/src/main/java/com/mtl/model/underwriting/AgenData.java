
package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgenData {

	private String agentCode;
	private String agentName;
	private String agentGroup;
	private String agentGroupName;
	private String applicationNumber;
	private String agentLicenceNumber;

}
