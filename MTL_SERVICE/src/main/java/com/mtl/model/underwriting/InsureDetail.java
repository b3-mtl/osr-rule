
package com.mtl.model.underwriting;

import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import com.mtl.model.common.PlanHeader;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsureDetail {

	private String planCode;
	private String planName;
	private String insuredAmount;
	private String premiumIndicator;
	private String premiumPayment;
	private PlanHeader planHeader;
	private String premiumAmount;
	private String premium;
	private String serviceBranch;
	private String channelCode;
	private String policyStatus = "";
	
	private String policyNumber;
	private String status = "";
	private String projectType ;
	private String writingAgent;
	private Date issueDate;
	// Rule 14
	private String groupChannelCode; //ประเภทช่องทางการจำหน่าย ===>>  สนใจ  MRTA MLTA LRTA ORD 

}
