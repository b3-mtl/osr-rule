package com.mtl.model.underwriting;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorMessageDesc { 
	private String errorCode;
	private String errorDesc;  
 
}
