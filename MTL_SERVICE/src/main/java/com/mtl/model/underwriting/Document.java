package com.mtl.model.underwriting;

import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Document {
	
	private String code;
	private String descTH;
	private String descEN;
	private String type;
	private String rule;
	
	@Override
	public int hashCode() {
		return Objects.hash(code, descEN, descTH, type ,rule);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		return Objects.equals(code, other.code) && Objects.equals(descEN, other.descEN)
				&& Objects.equals(descTH, other.descTH) && Objects.equals(type, other.type) && Objects.equals(rule, other.rule);
	}
	
	
}
