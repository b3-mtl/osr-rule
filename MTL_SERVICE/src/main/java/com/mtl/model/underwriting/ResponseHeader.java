
package com.mtl.model.underwriting;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseHeader { 
 
	private String transactionId;
	private String status;
	private LocalDateTime timestamp;	
	private String timeUsage;
	private ErrorMessage errorMessage;

}
