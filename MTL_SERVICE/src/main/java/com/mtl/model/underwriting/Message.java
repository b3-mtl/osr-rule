package com.mtl.model.underwriting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
	private String status;
	private String code;
	private String messageTH;
	private String messageEN;
}
