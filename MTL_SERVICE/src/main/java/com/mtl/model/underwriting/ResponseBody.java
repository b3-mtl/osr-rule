
package com.mtl.model.underwriting;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseBody {
	//private String underwriteStatus;
	//private String referUnderwriter;
    private int messageCount;  
	private List<Message> messageList;
	 private int documentCount;  
	private List<Document> documentList;
}
