package com.mtl.model.rule;

public class MibAmlo {

	private String relation;
	private String addmib;
	private Long id;
	
	
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getAddmib() {
		return addmib;
	}
	public void setAddmib(String addmib) {
		this.addmib = addmib;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
