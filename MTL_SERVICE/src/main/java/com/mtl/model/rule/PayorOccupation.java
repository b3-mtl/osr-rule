package com.mtl.model.rule;

public class PayorOccupation {
 
	/*
OCCUPATION_CODE
PB
REFER_UNDERWRITER
OCCUPATION_CODE_AMLO
RULE_MESSAGE_CODE_DESC
	 */
	
	private String occupationCode;
	private String pb;
	private String referUnderwriter;
	private String occupationCodeAmlo;
	private String ruleMessageCode;
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getPb() {
		return pb;
	}
	public void setPb(String pb) {
		this.pb = pb;
	}
	public String getReferUnderwriter() {
		return referUnderwriter;
	}
	public void setReferUnderwriter(String referUnderwriter) {
		this.referUnderwriter = referUnderwriter;
	}
	public String getOccupationCodeAmlo() {
		return occupationCodeAmlo;
	}
	public void setOccupationCodeAmlo(String occupationCodeAmlo) {
		this.occupationCodeAmlo = occupationCodeAmlo;
	}
	public String getRuleMessageCode() {
		return ruleMessageCode;
	}
	public void setRuleMessageCode(String ruleMessageCode) {
		this.ruleMessageCode = ruleMessageCode;
	}
	
 
	
}
