package com.mtl.model.rule;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "RuleMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleMessage implements Serializable{
	public final static String SPLIT = "!";
	private static final long serialVersionUID = -5416663049337045118L;
	private static final String pattern = "###,###.##";

	@XmlElement(name = "messageCode", required = false)
	private String messageCode;
	@XmlElement(name = "descCode", required = false)
	private String descCode;
	@XmlElement(name = "priority", required = false)
	private short priority;
	@XmlElement(name = "message", required = false)
	private String message;
	@XmlElement(name = "desc", required = false)
	private String desc;
	@XmlElement(name = "showMessage", required = false)
	private boolean showMessage;
	@XmlElement(name = "messageStatus", required = false)
	private int messageStatus;

	public RuleMessage() {
		message = "";
		messageCode = "";
		descCode = "";
		desc = "";
		priority = Short.MAX_VALUE;
		showMessage = true;
		messageStatus = 1;
	}

	public RuleMessage(String messageCode){
		this();
		this.messageCode = messageCode;
		this.message = messageCode;
	}

	public RuleMessage(String messageCode,String descCode){
		this();
		this.messageCode = messageCode;
		this.message = messageCode;
		this.descCode = descCode;
	} 



	public String getDesc(){
		DecimalFormat myFormatter = new DecimalFormat(pattern);

		String resultMessage=this.message;
		String descStr = this.descCode;
		if(descStr!=null&&descStr!=""&& this.message!=null){
			String[] descArr = descStr.split(SPLIT);
			for(String des:descArr){
				try
				{
					BigDecimal value = new BigDecimal(des);
					resultMessage = resultMessage.replaceFirst("\\$", myFormatter.format(value));
				}
				catch(NumberFormatException e)
				{
					resultMessage = resultMessage.replaceFirst("\\$", des);
					//not a double
				}
				//				resultMessage = resultMessage.replaceFirst("\\$", des);edit By TeTe 27/01/2014
			}
			return resultMessage;
		}	
		return resultMessage;
	}


}
