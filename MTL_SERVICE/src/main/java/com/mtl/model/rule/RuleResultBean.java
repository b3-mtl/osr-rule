package com.mtl.model.rule;

import java.io.Serializable;
import java.util.List;

import com.mtl.model.underwriting.Document;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RuleResultBean implements Serializable{
	
	private String messageCode;
	private List<String> stringInMessageCode;
	private List<Document> documentList;
	private boolean noParameterMessage =false;
 
 

}
