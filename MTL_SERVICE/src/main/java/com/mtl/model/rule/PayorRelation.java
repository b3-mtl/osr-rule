package com.mtl.model.rule;

public class PayorRelation {
 
	
	private String relation;
	private String messageCode;
	private String messagDesc;
	private String documentCode;
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessagDesc() {
		return messagDesc;
	}
	public void setMessagDesc(String messagDesc) {
		this.messagDesc = messagDesc;
	}
	public String getDocumentCode() {
		return documentCode;
	}
	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}
 
	 
	
}
