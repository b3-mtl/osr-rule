package com.mtl.model.rule;

import java.io.Serializable;
import java.util.List;

public class DocumentDesc implements Serializable{
	
	private String docCode;
	private String docDesc;
	public String getDocCode() {
		return docCode;
	}
	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}
	public String getDocDesc() {
		return docDesc;
	}
	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}
 
	
	

}
