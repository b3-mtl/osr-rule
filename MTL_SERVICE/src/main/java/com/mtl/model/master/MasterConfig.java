package com.mtl.model.master;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.ToString;

@ToString
@XmlRootElement(name = "master-config")
public class MasterConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Config> config;

	/**
	 * @return the config
	 */
	public List<Config> getConfig() {
		return config;
	}

	/**
	 * @param config the config to set
	 */
	@XmlElement(name = "config")
	public void setConfig(List<Config> config) {
		this.config = config;
	}

	@ToString
	@XmlRootElement(name = "config")
	public static class Config implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String id;
		private String name;
		private String sql;

		/**
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		@XmlAttribute
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		@XmlElement
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the sql
		 */
		public String getSql() {
			return sql;
		}

		/**
		 * @param sql the sql to set
		 */
		@XmlElement
		public void setSql(String sql) {
			this.sql = sql;
		}
	}

}
