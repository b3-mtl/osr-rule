package com.mtl.model.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;


@Data
public class DecisionTableModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tableName;
	private Map<Integer, String> column;
	private List<Object[]> table;

	public DecisionTableModel() {
		this.table = new ArrayList<Object[]>();
		this.column = new HashMap<Integer, String>();
	}

	public synchronized void putColumn(Integer index, String fieldName) {
		this.column.put(index, fieldName);
	}

	public synchronized void addData(Object[] data) {
		this.table.add(data);
	}

	@Override
	public String toString() {
		
		StringBuffer text = new StringBuffer();
		text.append("Table name: ").append( tableName).append(",");
		text.append("Column:[ ").append(column).append("]");
		
		return text.toString();
	}
}
