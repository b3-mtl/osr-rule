package com.mtl.model.master;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class PlanHeaderModel implements Serializable {

	private static final long serialVersionUID = 1L;
	private String planCode;
	private Date effectiveDate;
	private Date expiryDate;
	private Double minimumAmount;
	private Double maximumAmount;
	private String nonMedQualification;
	private Integer lowerAge;
	private Integer upperAge;
	private String sex;
	private String channel;
	private String payment;
	private String agentQualification;
	private String divType;
	private String divOption;
	private String flagMicrr;
	private String premiumIndicator;
	private String plantype;
	private String hb2tVal;
	private String uvType;
	private String classOfBusiness;
	private String uwRatingKey;
	private String businessPlanGroup;
	private String uwRatio;
	private String premiumChangeIndicator;
	private String premiumChangeYear;
	private String maturityIndicator;
	private String maturityYear;
	private String planGrouptype;
	private Date prDate;
	private String productLicense;
	private String rpuValue;
	private String etiValue;
	private String anntyStartInd;
	private String anntyCntlKey;
	private String mx40value;
	private String surrenderType;
	private String planSystem;
	private String takafulType;
	private String phEtFaceType;
	private String riderAddInd;
	private String typeInsurance;
	private String paType;
	private String takafulGroupType;
	private String ncType;
	private String kLeasingNml;
	private String kkAffinityNml;

}
