package com.mtl.model.common;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PlanPermission implements Serializable {
 /*
  * PLAN_CODE
CHANNEL
EXCEPT_MIB
EXCEPT_MIB_SPECIAL
EXCEPT_COF
EXCEPT_BMI
EXCEPT_INSURED_CLAIM
EXCEPT_HIV
EXCEPT_HEALTH_BENEFIT
EXCEPT_PB_MIB
EXCEPT_PB_CLAIM
EXCEPT_PB_HEALTH
  */
	    private String planCode; 
	    private String channel; 
	    private String exceptMib; 
	    private String  exceptMibSpecial; 
	    private String  exceptCOF; 
	    private String  exceptBMI; 
	    private String  exceptInsuredClaim; 
	    private String  exceptHIV; 
	    private String  exceptHealthBenefit; 
	    private String  exceptPBMIB; 
	    private String  exceptPBClaim; 
	    private String  exceptPBHealth; 
 
}; 
