package com.mtl.model.common;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AgentInfo implements Serializable { 
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 897407052299988970L;

    private  String planCode; 
	private  String agentCode;
    private  String agentGroup;
    private  String agentChannel;
    private  String qualifiation; 
    private  String serviceBranch; 
    private  String agentStatus; 
    private  Date startDate; 
    private  Date expiryDate; 
    private  String agentBlackListCode;

}
