package com.mtl.model.common;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PlanType implements Serializable {

	    /**
	 * 
	 */
	private static final long serialVersionUID = 2455904120632824119L;
	
	private String planCode; 
	private String planType;
 
}; 
