package com.mtl.model.common;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PlanHeader implements Serializable {
	
	  
	    private String planCode; 
	    private String agentQualification; 
	    private String channel; 
	    private Double maximumAmount; 
	    private Double minimumAmount; 
	    private String payment; 
	    private String planType; 
	    private String planGroupType; // BASIC,RIDER 
	    private String classOfBusiness; // planGroupR 
	    private String premiumIndicator; 
	    private String sex;
	    private String ncType;
	    private String uwRatio;
	    private String uwRatingKey;
	    private String planSystem;
	    private BigInteger lowerAge;
	    private BigInteger upperAge;
}
