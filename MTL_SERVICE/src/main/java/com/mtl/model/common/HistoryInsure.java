package com.mtl.model.common;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response.PolicyList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HistoryInsure {

	

	private String planCode; 
	private String planType; 
    private String planGroupType; // (BASIC,RIDER)
    private String documentType; //(CP,NCP) // column NML_TYPE ได้จาก NC_TYPE ในตาราง DT06_MAP_NML_TYP โดยใช้   NC_TYPE  ==> NC_TYPE ได้จากตาราง PLAN_HEADER โดยใช้ PLAN_CODE 
    private Date issueDate; 
    private BigDecimal faceAmount;
    private BigDecimal sumInsure; 
    private String status; 
    private String systemName;
    private String serviceBrach;
    private String channelCode;
	private String groupChannelCode; //ประเภทช่องทางการจำหน่าย ===>>  สนใจ  MRTA MLTA LRTA ORD 
	private String policyNumber;
	private String writingAgent;
    
}
