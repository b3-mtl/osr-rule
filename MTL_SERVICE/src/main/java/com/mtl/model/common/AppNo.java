package com.mtl.model.common;

import java.io.Serializable;
import java.math.BigInteger;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AppNo implements Serializable {
 
	/**
	* 
	*/
	private static final long serialVersionUID = 3083528500464712932L;
	private String appFormNo; 
	private String appFormNoKey; 
	private String channelGroup; 
	private String description; 
	private String isExcept; 
}; 
