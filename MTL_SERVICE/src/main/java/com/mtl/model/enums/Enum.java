package com.mtl.model.enums;

public class Enum {
	public static enum CollectorName {
		DC01
	}

	public static enum RuleName {
		RULE_01("serviceRule1"), RULE_13("serviceRule13");

		private final String className;

		RuleName(String className) {
			this.className = className;
		}

		public String className() {
			return this.className;
		}
	}
}
