package com.mtl.collector.master;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;


@Getter
public abstract class AbstactFactory<K, V> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<K, V> value;

	public AbstactFactory() {
		super();
		this.value = new HashMap<K, V>();

	}

	public synchronized void put(K key, V value) {
		this.value.put(key, value);
	}

	public V get(K key) {
		return this.value.get(key);
	}

	public Map<K, V> getFactory(){
		return value;
	}
	
	public void clear() {
		this.value = new HashMap<K, V>();
	}
	
	
}
