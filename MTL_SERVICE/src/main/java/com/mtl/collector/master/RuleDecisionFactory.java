package com.mtl.collector.master;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.mtl.model.master.DecisionTableModel;

public class RuleDecisionFactory  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private static RuleDecisionFactory instance;
    
	private Map<String, DecisionTableModel> value;

	private  RuleDecisionFactory() {
		clear();

	}
	
	 // get the only object available
    public static RuleDecisionFactory getInstance() {
    	if(instance==null)
    		instance = new RuleDecisionFactory();
        return instance;
    }

	public synchronized void put(String key, DecisionTableModel value) {
		this.value.put(key, value);
	}

	public DecisionTableModel get(String key) {
		return this.value.get(key);
	}

	public boolean containsKey( String key ){
		return value.containsKey(key);
	}
	
	public void clear() {
		this.value = new HashMap<String, DecisionTableModel>();
	}
	


}
