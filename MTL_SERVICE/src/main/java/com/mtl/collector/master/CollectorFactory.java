package com.mtl.collector.master;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mtl.api.core.repository.interfaces.MsCollectorMappingRepository;
import com.mtl.api.core.repository.interfaces.MsRuleRepository;

@Component
public class CollectorFactory extends AbstactFactory<String, Set<String>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(CollectorFactory.class);

	private MsCollectorMappingRepository collectorMappingRepository;
	private MsRuleRepository ruleRepository;

	
	/**
	 * Initial Collector Factory
	 * @param collectorMappingRepository
	 * @param ruleRepository
	 */
	public CollectorFactory(MsCollectorMappingRepository collectorMappingRepository, MsRuleRepository ruleRepository) {
		super();
		this.collectorMappingRepository = collectorMappingRepository;
		this.ruleRepository = ruleRepository;
		load();
	}

	/**
	 * Load data master to instance 
	 */
	public void load() {

		super.clear();

//		ruleRepository.findAll().forEach(rule -> {
//
//			Set<String> set = new HashSet<>();
//			collectorMappingRepository.findAllByRuleCode(rule).forEach(collector -> {
//
//				set.add(collector.getCollectorCode().getCollectorCode());
//			});
//			super.put(rule.getRuleCode(), set);
//		});

		log.info(super.getValue());
	}
	
	/**
	 * 
	 * @param ruleName
	 * @return
	 */
	public Set<String> get(Set<String> ruleName ) {
		Set<String> collector = new HashSet<>();
		List<String> listCollectorCode = new ArrayList<>();
		List<String> listCollect = new ArrayList<>();
		List<String> listRuleCode = new ArrayList<>();
		
		
		//check null
		if( ruleName == null || ruleName.isEmpty() )
			return null;
		
		//flow set data
		for (Iterator<String> it = ruleName.iterator(); it.hasNext(); ) {
			String className = it.next();
			
			//get ruleCode
			listRuleCode = ruleRepository.getRuleCode(className);
			for(String ruleCode:listRuleCode) {
				
				//get collectorDesc
				listCollect = collectorMappingRepository.findAllByRuleCodeNativeQuery(ruleCode);
				for(String obj:listCollect) {
					
					//set collector to list
//					MsCollector detail = obj.getCollectorCode();
					listCollectorCode.add(obj);
				}
				
			}
	    }
		collector.addAll(listCollectorCode);
		
		return collector;
	}

}
