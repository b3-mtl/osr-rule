package com.mtl.collector.master;

import java.io.Serializable;
import java.text.MessageFormat;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mtl.api.core.repository.interfaces.MessageMappingRepository;

@Component
public class MessageMappingFactory extends AbstactFactory<String, String> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(MessageMappingFactory.class);
//	@Autowired
//	private MessageMappingRepository messageMappingRepository;
    private static MessageMappingFactory instance;
    
	
	
	@PostConstruct
	public void loadFactory() {
	
		log.info("loadFactory");
	
//		MessageMappingFactory.getInstance().clear();
//		messageMappingRepository.findAll().forEach(item -> {
//			MessageMappingFactory.getInstance().put(item.getMessageCode(), item.getMessagetTh());
//		});
	}

	public String getMessage(String code) {
		Long start = System.currentTimeMillis();

		String result = super.get(code);
		Long end = System.currentTimeMillis();
		return result;
	}
	public String getMessage(String code, Object ...args) {
		Long start = System.currentTimeMillis();

		String result = super.get(code);
		result = MessageFormat.format(result, args);
		Long end = System.currentTimeMillis();
		return result;
	}

	
	public static MessageMappingFactory getInstance() {
		if( instance == null )
			instance = new MessageMappingFactory();
		
		return instance;
	}

}
