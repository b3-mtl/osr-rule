package com.mtl.collector.master;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.api.core.repository.interfaces.MsPlanHeaderRepository;
import com.mtl.model.master.PlanHeaderModel;
import com.mtl.model.utils.ModelObjectUtils;

@Service
public class PlanHeaderFactory extends AbstactFactory<String, PlanHeaderModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(PlanHeaderFactory.class);
	private static PlanHeaderFactory instance;
	
//	@Autowired
//	private MsPlanHeaderRepository planHeaderRepository;

	@PostConstruct
	public void loadFactory() {
	
		log.info("loadFactory");
	
//		PlanHeaderFactory.getInstance().clear();
//		planHeaderRepository.findAll().forEach(item -> {
//			PlanHeaderModel model = ModelObjectUtils.MAPPER.convertValue(item, PlanHeaderModel.class);
//
////			super.put(item.getPlanCode(), model);
//			PlanHeaderFactory.getInstance().put(item.getPlanCode(), model);
//		});
	}

	public PlanHeaderModel getPlanDetail(String planCode) {
		Long start = System.currentTimeMillis();

		PlanHeaderModel result = super.get(planCode);
		Long end = System.currentTimeMillis();
//		log.info("GetPlanDetail start time: " + (start) + " Ms.");
//		log.info("GetPlanDetail start time: " + (end - start) + " Ms.");
//		log.info("GetPlanDetail take time: " + (end - start) + " Ms.");
		return result;
	}

	
	public static PlanHeaderFactory getInstance() {
		if( instance == null )
			instance = new PlanHeaderFactory();
		return instance;
	}
}
