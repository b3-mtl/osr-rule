package com.mtl.collector.master;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mtl.appcache.ApplicationCache;

@Component
public class RuleEngineFactory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(RuleEngineFactory.class);
	
	
	@Autowired
	private ApplicationCache applicationCache;

	/**
	 * Find a rule engine factory to execute for this transaction
	 * @param appType
	 * @param channel
	 * @param section
	 * @return Map of <RuleId, Rule Class Name)
	 *	- RuleId naming of rule
	 *	- ClassName Factory of rule
	 */
	public Map<String, String> getRuleSet( String channel, String section) {
		Map<String, String> rules = null; 
		String channelSectionKey = channel+section;  
		List<String> ruleList = applicationCache.getChannelRuleCashMap(channelSectionKey);
		if(ruleList!=null&&ruleList.size()>0) { 
			rules = new HashMap(); 
			for(String rule:ruleList) {
				rules.put(rule, rule);
			}			
		} 

		return rules;
	}

}
