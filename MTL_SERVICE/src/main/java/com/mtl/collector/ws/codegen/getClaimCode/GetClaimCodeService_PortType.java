/**
 * GetClaimCodeService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getClaimCode;

public interface GetClaimCodeService_PortType extends java.rmi.Remote {
    public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeResponse getClaimCode(com.mtl.collector.ws.codegen.getClaimCode.HeaderGetClaimCodeRequest request) throws java.rmi.RemoteException;
}
