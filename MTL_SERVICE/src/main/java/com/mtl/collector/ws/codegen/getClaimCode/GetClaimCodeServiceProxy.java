package com.mtl.collector.ws.codegen.getClaimCode;

public class GetClaimCodeServiceProxy implements com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getClaimCodeService_PortType = null;
  
  public GetClaimCodeServiceProxy() {
    _initGetClaimCodeServiceProxy();
  }
  
  public GetClaimCodeServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetClaimCodeServiceProxy();
  }
  
  private void _initGetClaimCodeServiceProxy() {
    try {
      getClaimCodeService_PortType = (new com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_ServiceLocator()).getGetClaimCodeWsImplPort();
      if (getClaimCodeService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getClaimCodeService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getClaimCodeService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getClaimCodeService_PortType != null)
      ((javax.xml.rpc.Stub)getClaimCodeService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getGetClaimCodeService_PortType() {
    if (getClaimCodeService_PortType == null)
      _initGetClaimCodeServiceProxy();
    return getClaimCodeService_PortType;
  }
  
  public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeResponse getClaimCode(com.mtl.collector.ws.codegen.getClaimCode.HeaderGetClaimCodeRequest request) throws java.rmi.RemoteException{
    if (getClaimCodeService_PortType == null)
      _initGetClaimCodeServiceProxy();
    return getClaimCodeService_PortType.getClaimCode(request);
  }
  
  
}