package com.mtl.collector.ws.codegen.searchagentdetailbyagentcode;

public class SearchAgentDetailByAgentCodePortProxy implements com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort searchAgentDetailByAgentCodePort = null;
  
  public SearchAgentDetailByAgentCodePortProxy() {
    _initSearchAgentDetailByAgentCodePortProxy();
  }
  
  public SearchAgentDetailByAgentCodePortProxy(String endpoint) {
    _endpoint = endpoint;
    _initSearchAgentDetailByAgentCodePortProxy();
  }
  
  private void _initSearchAgentDetailByAgentCodePortProxy() {
    try {
      searchAgentDetailByAgentCodePort = (new com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortServiceLocator()).getSearchAgentDetailByAgentCodePortSoap11();
      if (searchAgentDetailByAgentCodePort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)searchAgentDetailByAgentCodePort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)searchAgentDetailByAgentCodePort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (searchAgentDetailByAgentCodePort != null)
      ((javax.xml.rpc.Stub)searchAgentDetailByAgentCodePort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort getSearchAgentDetailByAgentCodePort() {
    if (searchAgentDetailByAgentCodePort == null)
      _initSearchAgentDetailByAgentCodePortProxy();
    return searchAgentDetailByAgentCodePort;
  }
  
  public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeResponse searchAgentDetailByAgentCode(com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeRequest searchAgentDetailByAgentCodeRequest) throws java.rmi.RemoteException{
    if (searchAgentDetailByAgentCodePort == null)
      _initSearchAgentDetailByAgentCodePortProxy();
    return searchAgentDetailByAgentCodePort.searchAgentDetailByAgentCode(searchAgentDetailByAgentCodeRequest);
  }
  
  
}