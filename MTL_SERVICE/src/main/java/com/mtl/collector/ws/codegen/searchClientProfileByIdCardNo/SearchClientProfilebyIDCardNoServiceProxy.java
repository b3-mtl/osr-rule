package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

public class SearchClientProfilebyIDCardNoServiceProxy implements com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService searchClientProfilebyIDCardNoService = null;
  
  public SearchClientProfilebyIDCardNoServiceProxy() {
    _initSearchClientProfilebyIDCardNoServiceProxy();
  }
  
  public SearchClientProfilebyIDCardNoServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSearchClientProfilebyIDCardNoServiceProxy();
  }
  
  private void _initSearchClientProfilebyIDCardNoServiceProxy() {
    try {
      searchClientProfilebyIDCardNoService = (new com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator()).getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort();
      if (searchClientProfilebyIDCardNoService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)searchClientProfilebyIDCardNoService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)searchClientProfilebyIDCardNoService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (searchClientProfilebyIDCardNoService != null)
      ((javax.xml.rpc.Stub)searchClientProfilebyIDCardNoService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService getSearchClientProfilebyIDCardNoService() {
    if (searchClientProfilebyIDCardNoService == null)
      _initSearchClientProfilebyIDCardNoServiceProxy();
    return searchClientProfilebyIDCardNoService;
  }
  
  public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoResponse searchClientProfilebyIDCardNo(com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderSearchClientProfilebyIDCardNoRequest request) throws java.rmi.RemoteException{
    if (searchClientProfilebyIDCardNoService == null)
      _initSearchClientProfilebyIDCardNoServiceProxy();
    return searchClientProfilebyIDCardNoService.searchClientProfilebyIDCardNo(request);
  }
  
  
}