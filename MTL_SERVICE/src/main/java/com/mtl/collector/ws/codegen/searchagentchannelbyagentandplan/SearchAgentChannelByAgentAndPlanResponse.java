/**
 * SearchAgentChannelByAgentAndPlanResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan;

public class SearchAgentChannelByAgentAndPlanResponse  implements java.io.Serializable {
    private java.lang.String agentCode;

    private java.lang.String planCode;

    private java.lang.String groupChannel;
    
    private int status;
    
    private java.lang.String statusDescription;

    public SearchAgentChannelByAgentAndPlanResponse() {
    }

    public SearchAgentChannelByAgentAndPlanResponse(
           java.lang.String agentCode,
           java.lang.String planCode,
           java.lang.String groupChannel,
           int status,
           java.lang.String statusDescription) {
           this.agentCode = agentCode;
           this.planCode = planCode;
           this.groupChannel = groupChannel;
           this.status = status;
           this.statusDescription = statusDescription;
    }


    /**
     * Gets the agentCode value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @return agentCode
     */
    public java.lang.String getAgentCode() {
        return agentCode;
    }


    /**
     * Sets the agentCode value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @param agentCode
     */
    public void setAgentCode(java.lang.String agentCode) {
        this.agentCode = agentCode;
    }


    /**
     * Gets the planCode value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @return planCode
     */
    public java.lang.String getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String planCode) {
        this.planCode = planCode;
    }


    /**
     * Gets the groupChannel value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @return groupChannel
     */
    public java.lang.String getGroupChannel() {
        return groupChannel;
    }


    /**
     * Sets the groupChannel value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @param groupChannel
     */
    public void setGroupChannel(java.lang.String groupChannel) {
        this.groupChannel = groupChannel;
    }
    
    /**
     * Gets the status value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
    /**
     * Gets the status value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @return status
     */
    public java.lang.String getStatusDescription() {
        return statusDescription;
    }


    /**
     * Sets the status value for this SearchAgentChannelByAgentAndPlanResponse.
     * 
     * @param status
     */
    public void setStatusDescription(java.lang.String statusDescription) {
        this.statusDescription = statusDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchAgentChannelByAgentAndPlanResponse)) return false;
        SearchAgentChannelByAgentAndPlanResponse other = (SearchAgentChannelByAgentAndPlanResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.agentCode==null && other.getAgentCode()==null) || 
             (this.agentCode!=null &&
              this.agentCode.equals(other.getAgentCode()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              this.planCode.equals(other.getPlanCode()))) &&
            ((this.groupChannel==null && other.getGroupChannel()==null) || 
             (this.groupChannel!=null &&
              this.groupChannel.equals(other.getGroupChannel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAgentCode() != null) {
            _hashCode += getAgentCode().hashCode();
        }
        if (getPlanCode() != null) {
            _hashCode += getPlanCode().hashCode();
        }
        if (getGroupChannel() != null) {
            _hashCode += getGroupChannel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchAgentChannelByAgentAndPlanResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", ">SearchAgentChannelByAgentAndPlanResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", "agentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", "planCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", "groupChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
