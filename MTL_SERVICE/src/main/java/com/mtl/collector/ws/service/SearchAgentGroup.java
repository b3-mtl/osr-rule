package com.mtl.collector.ws.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupRequest;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupResponse;
import com.mtl.model.common.AgentGroup;

@Service
public class SearchAgentGroup {

	@Value("${ws.getAgentDetail.endpoint}")
	private String endpoint;

	public AgentGroup getAgentDetailByAgentCode(String agentCode) throws Exception {
		AgentGroup agentReturn = new AgentGroup();
		try {
			agentReturn = new AgentGroup();
			SearchAgentGroupRequest request = new SearchAgentGroupRequest(agentCode);
			SearchAgentGroupPortServiceLocator locator = new SearchAgentGroupPortServiceLocator();
			locator.setSearchAgentGroupPortSoap11EndpointAddress(endpoint);
			SearchAgentGroupResponse res = locator.getSearchAgentGroupPortSoap11().searchAgentGroup(request);

			agentReturn.setAgentGroup(res.getAgentGroup());
			// agentReturn.setStatus(res.getStatusDescription());

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return agentReturn;
	}

}
