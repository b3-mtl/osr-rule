/**
 * SearchClientProfilebyIDCardNoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

public interface SearchClientProfilebyIDCardNoService extends java.rmi.Remote {
    public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoResponse searchClientProfilebyIDCardNo(com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderSearchClientProfilebyIDCardNoRequest request) throws java.rmi.RemoteException;
}
