/**
 * GetMIBService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

public interface GetMIBService_Service extends javax.xml.rpc.Service {
    public java.lang.String getGetMIBServicePortAddress();

    public com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getGetMIBServicePort() throws javax.xml.rpc.ServiceException;

    public com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getGetMIBServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
