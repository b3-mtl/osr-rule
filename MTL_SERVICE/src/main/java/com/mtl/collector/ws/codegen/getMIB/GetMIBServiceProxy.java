package com.mtl.collector.ws.codegen.getMIB;

public class GetMIBServiceProxy implements com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getMIBService_PortType = null;
  
  public GetMIBServiceProxy() {
    _initGetMIBServiceProxy();
  }
  
  public GetMIBServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetMIBServiceProxy();
  }
  
  private void _initGetMIBServiceProxy() {
    try {
      getMIBService_PortType = (new com.mtl.collector.ws.codegen.getMIB.GetMIBService_ServiceLocator()).getGetMIBServicePort();
      if (getMIBService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getMIBService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getMIBService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getMIBService_PortType != null)
      ((javax.xml.rpc.Stub)getMIBService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getGetMIBService_PortType() {
    if (getMIBService_PortType == null)
      _initGetMIBServiceProxy();
    return getMIBService_PortType;
  }
  
  public com.mtl.collector.ws.codegen.getMIB.Response getMIB(com.mtl.collector.ws.codegen.getMIB.Request name) throws java.rmi.RemoteException{
    if (getMIBService_PortType == null)
      _initGetMIBServiceProxy();
    return getMIBService_PortType.getMIB(name);
  }
  
  
}