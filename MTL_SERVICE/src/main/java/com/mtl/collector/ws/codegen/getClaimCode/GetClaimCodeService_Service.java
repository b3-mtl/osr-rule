/**
 * GetClaimCodeService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getClaimCode;

public interface GetClaimCodeService_Service extends javax.xml.rpc.Service {

/**
 * OSB Service
 */
    public java.lang.String getGetClaimCodeWsImplPortAddress();

    public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getGetClaimCodeWsImplPort() throws javax.xml.rpc.ServiceException;

    public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getGetClaimCodeWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
