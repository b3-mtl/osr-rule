/**
 * SearchClientProfilebyIDCardNoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

public class SearchClientProfilebyIDCardNoResponse  extends com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderXml  implements java.io.Serializable {
    private java.lang.String codeClient;

    private java.lang.String title;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.lang.String titleEng;

    private java.lang.String firstNameEng;

    private java.lang.String lastNameEng;

    private java.lang.String ssn;

    private java.util.Date birthDate;

    private java.lang.String address1;

    private java.lang.String address2;

    private java.lang.String address3;

    private java.lang.String city;

    private java.lang.String province;

    private java.lang.String country;

    private java.lang.String district;

    private java.lang.String addressPostal;

    private java.lang.String address21;

    private java.lang.String address22;

    private java.lang.String address23;

    private java.lang.String city2;

    private java.lang.String province2;

    private java.lang.String country2;

    private java.lang.String district2;

    private java.lang.String address2Postal;

    private java.lang.String address31;

    private java.lang.String address32;

    private java.lang.String address33;

    private java.lang.String city3;

    private java.lang.String province3;

    private java.lang.String country3;

    private java.lang.String district3;

    private java.lang.String address3Postal;

    private java.lang.String sex;

    private java.lang.String companyName;

    private java.lang.String companyNameEng;

    public SearchClientProfilebyIDCardNoResponse() {
    }

    public SearchClientProfilebyIDCardNoResponse(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String codeClient,
           java.lang.String title,
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.String titleEng,
           java.lang.String firstNameEng,
           java.lang.String lastNameEng,
           java.lang.String ssn,
           java.util.Date birthDate,
           java.lang.String address1,
           java.lang.String address2,
           java.lang.String address3,
           java.lang.String city,
           java.lang.String province,
           java.lang.String country,
           java.lang.String district,
           java.lang.String addressPostal,
           java.lang.String address21,
           java.lang.String address22,
           java.lang.String address23,
           java.lang.String city2,
           java.lang.String province2,
           java.lang.String country2,
           java.lang.String district2,
           java.lang.String address2Postal,
           java.lang.String address31,
           java.lang.String address32,
           java.lang.String address33,
           java.lang.String city3,
           java.lang.String province3,
           java.lang.String country3,
           java.lang.String district3,
           java.lang.String address3Postal,
           java.lang.String sex,
           java.lang.String companyName,
           java.lang.String companyNameEng) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.codeClient = codeClient;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.titleEng = titleEng;
        this.firstNameEng = firstNameEng;
        this.lastNameEng = lastNameEng;
        this.ssn = ssn;
        this.birthDate = birthDate;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.city = city;
        this.province = province;
        this.country = country;
        this.district = district;
        this.addressPostal = addressPostal;
        this.address21 = address21;
        this.address22 = address22;
        this.address23 = address23;
        this.city2 = city2;
        this.province2 = province2;
        this.country2 = country2;
        this.district2 = district2;
        this.address2Postal = address2Postal;
        this.address31 = address31;
        this.address32 = address32;
        this.address33 = address33;
        this.city3 = city3;
        this.province3 = province3;
        this.country3 = country3;
        this.district3 = district3;
        this.address3Postal = address3Postal;
        this.sex = sex;
        this.companyName = companyName;
        this.companyNameEng = companyNameEng;
    }


    /**
     * Gets the codeClient value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return codeClient
     */
    public java.lang.String getCodeClient() {
        return codeClient;
    }


    /**
     * Sets the codeClient value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param codeClient
     */
    public void setCodeClient(java.lang.String codeClient) {
        this.codeClient = codeClient;
    }


    /**
     * Gets the title value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the firstName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the titleEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return titleEng
     */
    public java.lang.String getTitleEng() {
        return titleEng;
    }


    /**
     * Sets the titleEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param titleEng
     */
    public void setTitleEng(java.lang.String titleEng) {
        this.titleEng = titleEng;
    }


    /**
     * Gets the firstNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return firstNameEng
     */
    public java.lang.String getFirstNameEng() {
        return firstNameEng;
    }


    /**
     * Sets the firstNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param firstNameEng
     */
    public void setFirstNameEng(java.lang.String firstNameEng) {
        this.firstNameEng = firstNameEng;
    }


    /**
     * Gets the lastNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return lastNameEng
     */
    public java.lang.String getLastNameEng() {
        return lastNameEng;
    }


    /**
     * Sets the lastNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param lastNameEng
     */
    public void setLastNameEng(java.lang.String lastNameEng) {
        this.lastNameEng = lastNameEng;
    }


    /**
     * Gets the ssn value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return ssn
     */
    public java.lang.String getSsn() {
        return ssn;
    }


    /**
     * Sets the ssn value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param ssn
     */
    public void setSsn(java.lang.String ssn) {
        this.ssn = ssn;
    }


    /**
     * Gets the birthDate value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return birthDate
     */
    public java.util.Date getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.util.Date birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the address1 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address1
     */
    public java.lang.String getAddress1() {
        return address1;
    }


    /**
     * Sets the address1 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address1
     */
    public void setAddress1(java.lang.String address1) {
        this.address1 = address1;
    }


    /**
     * Gets the address2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address2
     */
    public java.lang.String getAddress2() {
        return address2;
    }


    /**
     * Sets the address2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address2
     */
    public void setAddress2(java.lang.String address2) {
        this.address2 = address2;
    }


    /**
     * Gets the address3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address3
     */
    public java.lang.String getAddress3() {
        return address3;
    }


    /**
     * Sets the address3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address3
     */
    public void setAddress3(java.lang.String address3) {
        this.address3 = address3;
    }


    /**
     * Gets the city value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the province value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return province
     */
    public java.lang.String getProvince() {
        return province;
    }


    /**
     * Sets the province value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param province
     */
    public void setProvince(java.lang.String province) {
        this.province = province;
    }


    /**
     * Gets the country value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the district value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return district
     */
    public java.lang.String getDistrict() {
        return district;
    }


    /**
     * Sets the district value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param district
     */
    public void setDistrict(java.lang.String district) {
        this.district = district;
    }


    /**
     * Gets the addressPostal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return addressPostal
     */
    public java.lang.String getAddressPostal() {
        return addressPostal;
    }


    /**
     * Sets the addressPostal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param addressPostal
     */
    public void setAddressPostal(java.lang.String addressPostal) {
        this.addressPostal = addressPostal;
    }


    /**
     * Gets the address21 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address21
     */
    public java.lang.String getAddress21() {
        return address21;
    }


    /**
     * Sets the address21 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address21
     */
    public void setAddress21(java.lang.String address21) {
        this.address21 = address21;
    }


    /**
     * Gets the address22 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address22
     */
    public java.lang.String getAddress22() {
        return address22;
    }


    /**
     * Sets the address22 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address22
     */
    public void setAddress22(java.lang.String address22) {
        this.address22 = address22;
    }


    /**
     * Gets the address23 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address23
     */
    public java.lang.String getAddress23() {
        return address23;
    }


    /**
     * Sets the address23 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address23
     */
    public void setAddress23(java.lang.String address23) {
        this.address23 = address23;
    }


    /**
     * Gets the city2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return city2
     */
    public java.lang.String getCity2() {
        return city2;
    }


    /**
     * Sets the city2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param city2
     */
    public void setCity2(java.lang.String city2) {
        this.city2 = city2;
    }


    /**
     * Gets the province2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return province2
     */
    public java.lang.String getProvince2() {
        return province2;
    }


    /**
     * Sets the province2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param province2
     */
    public void setProvince2(java.lang.String province2) {
        this.province2 = province2;
    }


    /**
     * Gets the country2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return country2
     */
    public java.lang.String getCountry2() {
        return country2;
    }


    /**
     * Sets the country2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param country2
     */
    public void setCountry2(java.lang.String country2) {
        this.country2 = country2;
    }


    /**
     * Gets the district2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return district2
     */
    public java.lang.String getDistrict2() {
        return district2;
    }


    /**
     * Sets the district2 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param district2
     */
    public void setDistrict2(java.lang.String district2) {
        this.district2 = district2;
    }


    /**
     * Gets the address2Postal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address2Postal
     */
    public java.lang.String getAddress2Postal() {
        return address2Postal;
    }


    /**
     * Sets the address2Postal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address2Postal
     */
    public void setAddress2Postal(java.lang.String address2Postal) {
        this.address2Postal = address2Postal;
    }


    /**
     * Gets the address31 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address31
     */
    public java.lang.String getAddress31() {
        return address31;
    }


    /**
     * Sets the address31 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address31
     */
    public void setAddress31(java.lang.String address31) {
        this.address31 = address31;
    }


    /**
     * Gets the address32 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address32
     */
    public java.lang.String getAddress32() {
        return address32;
    }


    /**
     * Sets the address32 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address32
     */
    public void setAddress32(java.lang.String address32) {
        this.address32 = address32;
    }


    /**
     * Gets the address33 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address33
     */
    public java.lang.String getAddress33() {
        return address33;
    }


    /**
     * Sets the address33 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address33
     */
    public void setAddress33(java.lang.String address33) {
        this.address33 = address33;
    }


    /**
     * Gets the city3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return city3
     */
    public java.lang.String getCity3() {
        return city3;
    }


    /**
     * Sets the city3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param city3
     */
    public void setCity3(java.lang.String city3) {
        this.city3 = city3;
    }


    /**
     * Gets the province3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return province3
     */
    public java.lang.String getProvince3() {
        return province3;
    }


    /**
     * Sets the province3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param province3
     */
    public void setProvince3(java.lang.String province3) {
        this.province3 = province3;
    }


    /**
     * Gets the country3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return country3
     */
    public java.lang.String getCountry3() {
        return country3;
    }


    /**
     * Sets the country3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param country3
     */
    public void setCountry3(java.lang.String country3) {
        this.country3 = country3;
    }


    /**
     * Gets the district3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return district3
     */
    public java.lang.String getDistrict3() {
        return district3;
    }


    /**
     * Sets the district3 value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param district3
     */
    public void setDistrict3(java.lang.String district3) {
        this.district3 = district3;
    }


    /**
     * Gets the address3Postal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return address3Postal
     */
    public java.lang.String getAddress3Postal() {
        return address3Postal;
    }


    /**
     * Sets the address3Postal value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param address3Postal
     */
    public void setAddress3Postal(java.lang.String address3Postal) {
        this.address3Postal = address3Postal;
    }


    /**
     * Gets the sex value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return sex
     */
    public java.lang.String getSex() {
        return sex;
    }


    /**
     * Sets the sex value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param sex
     */
    public void setSex(java.lang.String sex) {
        this.sex = sex;
    }


    /**
     * Gets the companyName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return companyName
     */
    public java.lang.String getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param companyName
     */
    public void setCompanyName(java.lang.String companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the companyNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @return companyNameEng
     */
    public java.lang.String getCompanyNameEng() {
        return companyNameEng;
    }


    /**
     * Sets the companyNameEng value for this SearchClientProfilebyIDCardNoResponse.
     * 
     * @param companyNameEng
     */
    public void setCompanyNameEng(java.lang.String companyNameEng) {
        this.companyNameEng = companyNameEng;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchClientProfilebyIDCardNoResponse)) return false;
        SearchClientProfilebyIDCardNoResponse other = (SearchClientProfilebyIDCardNoResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.codeClient==null && other.getCodeClient()==null) || 
             (this.codeClient!=null &&
              this.codeClient.equals(other.getCodeClient()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.titleEng==null && other.getTitleEng()==null) || 
             (this.titleEng!=null &&
              this.titleEng.equals(other.getTitleEng()))) &&
            ((this.firstNameEng==null && other.getFirstNameEng()==null) || 
             (this.firstNameEng!=null &&
              this.firstNameEng.equals(other.getFirstNameEng()))) &&
            ((this.lastNameEng==null && other.getLastNameEng()==null) || 
             (this.lastNameEng!=null &&
              this.lastNameEng.equals(other.getLastNameEng()))) &&
            ((this.ssn==null && other.getSsn()==null) || 
             (this.ssn!=null &&
              this.ssn.equals(other.getSsn()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.address1==null && other.getAddress1()==null) || 
             (this.address1!=null &&
              this.address1.equals(other.getAddress1()))) &&
            ((this.address2==null && other.getAddress2()==null) || 
             (this.address2!=null &&
              this.address2.equals(other.getAddress2()))) &&
            ((this.address3==null && other.getAddress3()==null) || 
             (this.address3!=null &&
              this.address3.equals(other.getAddress3()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.province==null && other.getProvince()==null) || 
             (this.province!=null &&
              this.province.equals(other.getProvince()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.district==null && other.getDistrict()==null) || 
             (this.district!=null &&
              this.district.equals(other.getDistrict()))) &&
            ((this.addressPostal==null && other.getAddressPostal()==null) || 
             (this.addressPostal!=null &&
              this.addressPostal.equals(other.getAddressPostal()))) &&
            ((this.address21==null && other.getAddress21()==null) || 
             (this.address21!=null &&
              this.address21.equals(other.getAddress21()))) &&
            ((this.address22==null && other.getAddress22()==null) || 
             (this.address22!=null &&
              this.address22.equals(other.getAddress22()))) &&
            ((this.address23==null && other.getAddress23()==null) || 
             (this.address23!=null &&
              this.address23.equals(other.getAddress23()))) &&
            ((this.city2==null && other.getCity2()==null) || 
             (this.city2!=null &&
              this.city2.equals(other.getCity2()))) &&
            ((this.province2==null && other.getProvince2()==null) || 
             (this.province2!=null &&
              this.province2.equals(other.getProvince2()))) &&
            ((this.country2==null && other.getCountry2()==null) || 
             (this.country2!=null &&
              this.country2.equals(other.getCountry2()))) &&
            ((this.district2==null && other.getDistrict2()==null) || 
             (this.district2!=null &&
              this.district2.equals(other.getDistrict2()))) &&
            ((this.address2Postal==null && other.getAddress2Postal()==null) || 
             (this.address2Postal!=null &&
              this.address2Postal.equals(other.getAddress2Postal()))) &&
            ((this.address31==null && other.getAddress31()==null) || 
             (this.address31!=null &&
              this.address31.equals(other.getAddress31()))) &&
            ((this.address32==null && other.getAddress32()==null) || 
             (this.address32!=null &&
              this.address32.equals(other.getAddress32()))) &&
            ((this.address33==null && other.getAddress33()==null) || 
             (this.address33!=null &&
              this.address33.equals(other.getAddress33()))) &&
            ((this.city3==null && other.getCity3()==null) || 
             (this.city3!=null &&
              this.city3.equals(other.getCity3()))) &&
            ((this.province3==null && other.getProvince3()==null) || 
             (this.province3!=null &&
              this.province3.equals(other.getProvince3()))) &&
            ((this.country3==null && other.getCountry3()==null) || 
             (this.country3!=null &&
              this.country3.equals(other.getCountry3()))) &&
            ((this.district3==null && other.getDistrict3()==null) || 
             (this.district3!=null &&
              this.district3.equals(other.getDistrict3()))) &&
            ((this.address3Postal==null && other.getAddress3Postal()==null) || 
             (this.address3Postal!=null &&
              this.address3Postal.equals(other.getAddress3Postal()))) &&
            ((this.sex==null && other.getSex()==null) || 
             (this.sex!=null &&
              this.sex.equals(other.getSex()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.companyNameEng==null && other.getCompanyNameEng()==null) || 
             (this.companyNameEng!=null &&
              this.companyNameEng.equals(other.getCompanyNameEng())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCodeClient() != null) {
            _hashCode += getCodeClient().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getTitleEng() != null) {
            _hashCode += getTitleEng().hashCode();
        }
        if (getFirstNameEng() != null) {
            _hashCode += getFirstNameEng().hashCode();
        }
        if (getLastNameEng() != null) {
            _hashCode += getLastNameEng().hashCode();
        }
        if (getSsn() != null) {
            _hashCode += getSsn().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getAddress1() != null) {
            _hashCode += getAddress1().hashCode();
        }
        if (getAddress2() != null) {
            _hashCode += getAddress2().hashCode();
        }
        if (getAddress3() != null) {
            _hashCode += getAddress3().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getProvince() != null) {
            _hashCode += getProvince().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getDistrict() != null) {
            _hashCode += getDistrict().hashCode();
        }
        if (getAddressPostal() != null) {
            _hashCode += getAddressPostal().hashCode();
        }
        if (getAddress21() != null) {
            _hashCode += getAddress21().hashCode();
        }
        if (getAddress22() != null) {
            _hashCode += getAddress22().hashCode();
        }
        if (getAddress23() != null) {
            _hashCode += getAddress23().hashCode();
        }
        if (getCity2() != null) {
            _hashCode += getCity2().hashCode();
        }
        if (getProvince2() != null) {
            _hashCode += getProvince2().hashCode();
        }
        if (getCountry2() != null) {
            _hashCode += getCountry2().hashCode();
        }
        if (getDistrict2() != null) {
            _hashCode += getDistrict2().hashCode();
        }
        if (getAddress2Postal() != null) {
            _hashCode += getAddress2Postal().hashCode();
        }
        if (getAddress31() != null) {
            _hashCode += getAddress31().hashCode();
        }
        if (getAddress32() != null) {
            _hashCode += getAddress32().hashCode();
        }
        if (getAddress33() != null) {
            _hashCode += getAddress33().hashCode();
        }
        if (getCity3() != null) {
            _hashCode += getCity3().hashCode();
        }
        if (getProvince3() != null) {
            _hashCode += getProvince3().hashCode();
        }
        if (getCountry3() != null) {
            _hashCode += getCountry3().hashCode();
        }
        if (getDistrict3() != null) {
            _hashCode += getDistrict3().hashCode();
        }
        if (getAddress3Postal() != null) {
            _hashCode += getAddress3Postal().hashCode();
        }
        if (getSex() != null) {
            _hashCode += getSex().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getCompanyNameEng() != null) {
            _hashCode += getCompanyNameEng().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchClientProfilebyIDCardNoResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.searchclientprofilebyidcardno.ws.application.muangthai.co.th/", "searchClientProfilebyIDCardNoResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodeClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titleEng");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TitleEng"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstNameEng");
        elemField.setXmlName(new javax.xml.namespace.QName("", "FirstNameEng"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastNameEng");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LastNameEng"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ssn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Ssn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("province");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Province"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("district");
        elemField.setXmlName(new javax.xml.namespace.QName("", "District"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressPostal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AddressPostal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address21");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address21"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address22");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address22"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address23");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address23"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "City2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("province2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Province2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Country2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("district2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "District2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2Postal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address2Postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address31");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address31"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address32");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address32"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address33");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address33"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "City3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("province3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Province3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Country3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("district3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "District3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address3Postal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Address3Postal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Sex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyNameEng");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CompanyNameEng"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
