package com.mtl.collector.ws.service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.ws.codegen.getMIB.GetMIBService_ServiceLocator;
import com.mtl.collector.ws.codegen.getMIB.MibResultList;
import com.mtl.collector.ws.codegen.getMIB.Response;

@Service
public class GetMIBWsClientService {
	
	@Value("${ws.getmib.endpoint}")
	private String endpoint;

	public List<String> getMIB(String appFormNumber,String[] planCode,String idCard,int percent) throws RemoteException,ServiceException {
		
		com.mtl.collector.ws.codegen.getMIB.Request req = new 	com.mtl.collector.ws.codegen.getMIB.Request();
		List<String> res = new ArrayList<String>();
		
	    req.setIdCard(idCard);
	    req.setAppFormNumber(appFormNumber);
	    req.setPlanCode(planCode);
	    req.setFilterPercent(percent);
	    System.out.println(" MIB appFormNumber:"+appFormNumber+" ,idCard:"+idCard);
	    com.mtl.collector.ws.codegen.getMIB.Response rs = null;
		try {
			GetMIBService_ServiceLocator locator = new GetMIBService_ServiceLocator();
			locator.setGetMIBServicePortEndpointAddress(endpoint);
			rs = locator.getGetMIBServicePort().getMIB(req);
			if(rs !=null) {
				if(rs.getMibResults() !=null) {
					for(MibResultList temp:rs.getMibResults()) {
						String paddedString11 = StringUtils.leftPad(String.valueOf(temp.getMI_CUAS11()), 3,"0");
						String code11 = temp.getMI_CUAS01()+paddedString11;
						String paddedString21 = StringUtils.leftPad(String.valueOf(temp.getMI_CUAS21()), 3,"0");
						String code21 = temp.getMI_CUAS02()+paddedString21;
						res.add(code11);
						res.add(code21);
					}
				}
				
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
 
	}
	
	public List<String> getMIBRule17(String firstName,String lastName,int percent) throws RemoteException,ServiceException {
		
		com.mtl.collector.ws.codegen.getMIB.Request req = new 	com.mtl.collector.ws.codegen.getMIB.Request();
		List<String> res = new ArrayList<String>();
		
		req.setFirstName(firstName);
		req.setLastName(lastName);
	    req.setFilterPercent(percent);
	    System.out.println(" MIB Rule 17 firstName:"+firstName+" ,lastName:"+lastName);
	    com.mtl.collector.ws.codegen.getMIB.Response rs = null;
		try {
			GetMIBService_ServiceLocator locator = new GetMIBService_ServiceLocator();
			locator.setGetMIBServicePortEndpointAddress(endpoint);
			rs = locator.getGetMIBServicePort().getMIB(req);
			if(rs !=null) {
				if(rs.getMibResults() !=null) {
					for(MibResultList temp:rs.getMibResults()) {
						String paddedString11 = StringUtils.leftPad(String.valueOf(temp.getMI_CUAS11()), 3,"0");
						String code11 = temp.getMI_CUAS01()+paddedString11;
						String paddedString21 = StringUtils.leftPad(String.valueOf(temp.getMI_CUAS21()), 3,"0");
						String code21 = temp.getMI_CUAS02()+paddedString21;
						res.add(code11);
						res.add(code21);
					}
					
				}
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
 
	}
	
	public boolean getFlagClaim(String appFormNumber,String[] planCode,String idCard,int percent) throws RemoteException,ServiceException {
		
		com.mtl.collector.ws.codegen.getMIB.Request req = new 	com.mtl.collector.ws.codegen.getMIB.Request();

	    req.setIdCard(idCard);
	    req.setAppFormNumber(appFormNumber);
	    req.setPlanCode(planCode);
	    req.setFilterPercent(percent);
	    Response rs = null;
		try {
			GetMIBService_ServiceLocator locator = new GetMIBService_ServiceLocator();
			locator.setGetMIBServicePortEndpointAddress(endpoint);
			rs = locator.getGetMIBServicePort().getMIB(req);
			
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
		return rs.isFlagClaim();
 
	}
}
