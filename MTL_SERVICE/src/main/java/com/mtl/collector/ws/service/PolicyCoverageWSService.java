package com.mtl.collector.ws.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

@Service
public class PolicyCoverageWSService {

	@Value("${ws.DataColPolicyCoverage.endpoint}")
	private String endpoint;

	@Value("${webservice.init.enable}")
	private String initFlag;
	
	@Autowired
	private WebServiceTemplate  webServiceTemplate  ;
	
	public com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response  getPolicy(String clientNo){
		 
		webServiceTemplate.setDefaultUri(endpoint);
		
		com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Request request = new com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Request();
	 
 		com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Request.ClientList clientList = new com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Request.ClientList();

		clientList.getClientNumber().add(clientNo);
		
		request.setClientList(clientList);
 
				
		com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response response =(com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response)webServiceTemplate.marshalSendAndReceive(request, new WebServiceMessageCallback() {
					public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
						try {
							SOAPMessage soapMessageTeachTable = ((SaajSoapMessage)message).getSaajMessage();				 
					       ByteArrayOutputStream out = new ByteArrayOutputStream();
					       soapMessageTeachTable.writeTo(out);
				         //  System.out.println(" getPolicy SOAP Request Payload: " + new String(out.toByteArray()));
					         
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}); 	
 
		if(response!=null) {
			if(response.getPolicyList()!=null) {
				System.out.println(" GetDataColPolicyCoverage Found PolicyList size:"+response.getPolicyList().size());
			}
		}
		
		return response;

		
	}
	
 
}