/**
 * SearchAgentGroupPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentgroup;

public interface SearchAgentGroupPort extends java.rmi.Remote {
    public com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupResponse searchAgentGroup(com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupRequest searchAgentGroupRequest) throws java.rmi.RemoteException;
}
