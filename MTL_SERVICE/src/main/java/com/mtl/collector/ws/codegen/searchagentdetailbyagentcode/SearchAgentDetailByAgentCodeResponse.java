/**
 * SearchAgentDetailByAgentCodeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentdetailbyagentcode;

public class SearchAgentDetailByAgentCodeResponse  implements java.io.Serializable {
    private int status;

    private java.lang.String statusDescription;

    private java.lang.String agentCode;

    private java.lang.String qualifiation;

    private java.lang.String serviceBranch;

    private java.lang.String agentStatus;

    private java.lang.String channelCode;

    private java.util.Date startDate;

    private java.util.Date expiryDate;

    private java.lang.String agentBlackListCode;

    public SearchAgentDetailByAgentCodeResponse() {
    }

    public SearchAgentDetailByAgentCodeResponse(
           int status,
           java.lang.String statusDescription,
           java.lang.String agentCode,
           java.lang.String qualifiation,
           java.lang.String serviceBranch,
           java.lang.String agentStatus,
           java.lang.String channelCode,
           java.util.Date startDate,
           java.util.Date expiryDate,
           java.lang.String agentBlackListCode) {
           this.status = status;
           this.statusDescription = statusDescription;
           this.agentCode = agentCode;
           this.qualifiation = qualifiation;
           this.serviceBranch = serviceBranch;
           this.agentStatus = agentStatus;
           this.channelCode = channelCode;
           this.startDate = startDate;
           this.expiryDate = expiryDate;
           this.agentBlackListCode = agentBlackListCode;
    }


    /**
     * Gets the status value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the statusDescription value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return statusDescription
     */
    public java.lang.String getStatusDescription() {
        return statusDescription;
    }


    /**
     * Sets the statusDescription value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param statusDescription
     */
    public void setStatusDescription(java.lang.String statusDescription) {
        this.statusDescription = statusDescription;
    }


    /**
     * Gets the agentCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return agentCode
     */
    public java.lang.String getAgentCode() {
        return agentCode;
    }


    /**
     * Sets the agentCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param agentCode
     */
    public void setAgentCode(java.lang.String agentCode) {
        this.agentCode = agentCode;
    }


    /**
     * Gets the qualifiation value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return qualifiation
     */
    public java.lang.String getQualifiation() {
        return qualifiation;
    }


    /**
     * Sets the qualifiation value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param qualifiation
     */
    public void setQualifiation(java.lang.String qualifiation) {
        this.qualifiation = qualifiation;
    }


    /**
     * Gets the serviceBranch value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return serviceBranch
     */
    public java.lang.String getServiceBranch() {
        return serviceBranch;
    }


    /**
     * Sets the serviceBranch value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param serviceBranch
     */
    public void setServiceBranch(java.lang.String serviceBranch) {
        this.serviceBranch = serviceBranch;
    }


    /**
     * Gets the agentStatus value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return agentStatus
     */
    public java.lang.String getAgentStatus() {
        return agentStatus;
    }


    /**
     * Sets the agentStatus value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param agentStatus
     */
    public void setAgentStatus(java.lang.String agentStatus) {
        this.agentStatus = agentStatus;
    }


    /**
     * Gets the channelCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return channelCode
     */
    public java.lang.String getChannelCode() {
        return channelCode;
    }


    /**
     * Sets the channelCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param channelCode
     */
    public void setChannelCode(java.lang.String channelCode) {
        this.channelCode = channelCode;
    }


    /**
     * Gets the startDate value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return startDate
     */
    public java.util.Date getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expiryDate value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return expiryDate
     */
    public java.util.Date getExpiryDate() {
        return expiryDate;
    }


    /**
     * Sets the expiryDate value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param expiryDate
     */
    public void setExpiryDate(java.util.Date expiryDate) {
        this.expiryDate = expiryDate;
    }


    /**
     * Gets the agentBlackListCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @return agentBlackListCode
     */
    public java.lang.String getAgentBlackListCode() {
        return agentBlackListCode;
    }


    /**
     * Sets the agentBlackListCode value for this SearchAgentDetailByAgentCodeResponse.
     * 
     * @param agentBlackListCode
     */
    public void setAgentBlackListCode(java.lang.String agentBlackListCode) {
        this.agentBlackListCode = agentBlackListCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchAgentDetailByAgentCodeResponse)) return false;
        SearchAgentDetailByAgentCodeResponse other = (SearchAgentDetailByAgentCodeResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.status == other.getStatus() &&
            ((this.statusDescription==null && other.getStatusDescription()==null) || 
             (this.statusDescription!=null &&
              this.statusDescription.equals(other.getStatusDescription()))) &&
            ((this.agentCode==null && other.getAgentCode()==null) || 
             (this.agentCode!=null &&
              this.agentCode.equals(other.getAgentCode()))) &&
            ((this.qualifiation==null && other.getQualifiation()==null) || 
             (this.qualifiation!=null &&
              this.qualifiation.equals(other.getQualifiation()))) &&
            ((this.serviceBranch==null && other.getServiceBranch()==null) || 
             (this.serviceBranch!=null &&
              this.serviceBranch.equals(other.getServiceBranch()))) &&
            ((this.agentStatus==null && other.getAgentStatus()==null) || 
             (this.agentStatus!=null &&
              this.agentStatus.equals(other.getAgentStatus()))) &&
            ((this.channelCode==null && other.getChannelCode()==null) || 
             (this.channelCode!=null &&
              this.channelCode.equals(other.getChannelCode()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.expiryDate==null && other.getExpiryDate()==null) || 
             (this.expiryDate!=null &&
              this.expiryDate.equals(other.getExpiryDate()))) &&
            ((this.agentBlackListCode==null && other.getAgentBlackListCode()==null) || 
             (this.agentBlackListCode!=null &&
              this.agentBlackListCode.equals(other.getAgentBlackListCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getStatus();
        if (getStatusDescription() != null) {
            _hashCode += getStatusDescription().hashCode();
        }
        if (getAgentCode() != null) {
            _hashCode += getAgentCode().hashCode();
        }
        if (getQualifiation() != null) {
            _hashCode += getQualifiation().hashCode();
        }
        if (getServiceBranch() != null) {
            _hashCode += getServiceBranch().hashCode();
        }
        if (getAgentStatus() != null) {
            _hashCode += getAgentStatus().hashCode();
        }
        if (getChannelCode() != null) {
            _hashCode += getChannelCode().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getExpiryDate() != null) {
            _hashCode += getExpiryDate().hashCode();
        }
        if (getAgentBlackListCode() != null) {
            _hashCode += getAgentBlackListCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchAgentDetailByAgentCodeResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", ">SearchAgentDetailByAgentCodeResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "statusDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "agentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qualifiation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "qualifiation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "serviceBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "agentStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "channelCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiryDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "expiryDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentBlackListCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "agentBlackListCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
