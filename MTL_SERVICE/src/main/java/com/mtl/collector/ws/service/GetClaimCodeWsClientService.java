package com.mtl.collector.ws.service;

import java.rmi.RemoteException;
import java.util.Arrays;

import javax.xml.rpc.ServiceException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_ServiceLocator;

@Service
public class GetClaimCodeWsClientService {
	
	@Value("${ws.GetClaimCode.endpoint}")
	private String endpoint;
	
	public String[] getClaimCode(String clientNumber) throws RemoteException,ServiceException {
		String[] response = null;
		
		com.mtl.collector.ws.codegen.getClaimCode.HeaderGetClaimCodeRequest request = new com.mtl.collector.ws.codegen.getClaimCode.HeaderGetClaimCodeRequest();
		request.setDetail("");
		request.setServiceID("");
		request.setCallName("");
		request.setReason("");
		System.out.println(" Client No:"+clientNumber);
		request.setClientNumber(clientNumber);
		com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeResponse res;
		try {
			GetClaimCodeService_ServiceLocator locator = new GetClaimCodeService_ServiceLocator();
			locator.setGetClaimCodeWsImplPortEndpointAddress(endpoint);
			res = locator.getGetClaimCodeWsImplPort().getClaimCode(request);
			response = res.getCode(); 
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
		return response;

	}

}
