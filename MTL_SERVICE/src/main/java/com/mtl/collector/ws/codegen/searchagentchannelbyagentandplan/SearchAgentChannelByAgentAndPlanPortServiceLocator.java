/**
 * SearchAgentChannelByAgentAndPlanPortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan;

import org.springframework.beans.factory.annotation.Value;

public class SearchAgentChannelByAgentAndPlanPortServiceLocator extends org.apache.axis.client.Service implements com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortService {
	
    public SearchAgentChannelByAgentAndPlanPortServiceLocator() {
    }

    public SearchAgentChannelByAgentAndPlanPortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SearchAgentChannelByAgentAndPlanPortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SearchAgentChannelByAgentAndPlanPortSoap11
    private java.lang.String SearchAgentChannelByAgentAndPlanPortSoap11_address = "";

    public java.lang.String getSearchAgentChannelByAgentAndPlanPortSoap11Address() {
        return SearchAgentChannelByAgentAndPlanPortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName = "SearchAgentChannelByAgentAndPlanPortSoap11";

    public java.lang.String getSearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName() {
        return SearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName;
    }

    public void setSearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName(java.lang.String name) {
        SearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName = name;
    }

    public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort getSearchAgentChannelByAgentAndPlanPortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SearchAgentChannelByAgentAndPlanPortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSearchAgentChannelByAgentAndPlanPortSoap11(endpoint);
    }

    public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort getSearchAgentChannelByAgentAndPlanPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortSoap11Stub _stub = new com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortSoap11Stub(portAddress, this);
            _stub.setPortName(getSearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSearchAgentChannelByAgentAndPlanPortSoap11EndpointAddress(java.lang.String address) {
        SearchAgentChannelByAgentAndPlanPortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortSoap11Stub _stub = new com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortSoap11Stub(new java.net.URL(SearchAgentChannelByAgentAndPlanPortSoap11_address), this);
                _stub.setPortName(getSearchAgentChannelByAgentAndPlanPortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SearchAgentChannelByAgentAndPlanPortSoap11".equals(inputPortName)) {
            return getSearchAgentChannelByAgentAndPlanPortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", "SearchAgentChannelByAgentAndPlanPortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://searchagentchannelbyagentandplan.ws.muangthai.co.th", "SearchAgentChannelByAgentAndPlanPortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SearchAgentChannelByAgentAndPlanPortSoap11".equals(portName)) {
            setSearchAgentChannelByAgentAndPlanPortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
