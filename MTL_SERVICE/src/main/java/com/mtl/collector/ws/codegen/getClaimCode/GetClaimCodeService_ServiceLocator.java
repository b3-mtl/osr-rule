/**
 * GetClaimCodeService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getClaimCode;

import org.springframework.beans.factory.annotation.Value;

public class GetClaimCodeService_ServiceLocator extends org.apache.axis.client.Service implements com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_Service {

    public GetClaimCodeService_ServiceLocator() {
    }


    public GetClaimCodeService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetClaimCodeService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetClaimCodeWsImplPort
    private java.lang.String GetClaimCodeWsImplPort_address = "";

    public java.lang.String getGetClaimCodeWsImplPortAddress() {
        return GetClaimCodeWsImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetClaimCodeWsImplPortWSDDServiceName = "GetClaimCodeWsImplPort";

    public java.lang.String getGetClaimCodeWsImplPortWSDDServiceName() {
        return GetClaimCodeWsImplPortWSDDServiceName;
    }

    public void setGetClaimCodeWsImplPortWSDDServiceName(java.lang.String name) {
        GetClaimCodeWsImplPortWSDDServiceName = name;
    }

    public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getGetClaimCodeWsImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetClaimCodeWsImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetClaimCodeWsImplPort(endpoint);
    }

    public com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType getGetClaimCodeWsImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeWsImplPortBindingStub _stub = new com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getGetClaimCodeWsImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetClaimCodeWsImplPortEndpointAddress(java.lang.String address) {
        GetClaimCodeWsImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeWsImplPortBindingStub _stub = new com.mtl.collector.ws.codegen.getClaimCode.GetClaimCodeWsImplPortBindingStub(new java.net.URL(GetClaimCodeWsImplPort_address), this);
                _stub.setPortName(getGetClaimCodeWsImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetClaimCodeWsImplPort".equals(inputPortName)) {
            return getGetClaimCodeWsImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.getclaimcode.ws.application.muangthai.co.th/", "GetClaimCodeService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.getclaimcode.ws.application.muangthai.co.th/", "GetClaimCodeWsImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetClaimCodeWsImplPort".equals(portName)) {
            setGetClaimCodeWsImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
