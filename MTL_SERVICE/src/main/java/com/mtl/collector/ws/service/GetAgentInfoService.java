package com.mtl.collector.ws.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanRequest;
import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanResponse;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeRequest;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeResponse;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupRequest;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupResponse;
import com.mtl.model.common.AgentInfo;

@Service
public class GetAgentInfoService { 
	
	@Value("${ws.getAgentDetail.endpoint}")
	private String endpoint;
	
	public AgentInfo getAgentDetailByAgentCode(String agentCode ,String planCode) throws Exception {	
	    AgentInfo agentInfo = new AgentInfo();
	    try{
			 SearchAgentDetailByAgentCodeRequest request = new  SearchAgentDetailByAgentCodeRequest(agentCode);
			 SearchAgentDetailByAgentCodePortServiceLocator locator= new  SearchAgentDetailByAgentCodePortServiceLocator();
			 locator.setSearchAgentDetailByAgentCodePortSoap11EndpointAddress(endpoint);
	         SearchAgentDetailByAgentCodeResponse res = locator.getSearchAgentDetailByAgentCodePortSoap11().searchAgentDetailByAgentCode(request);
	         
	         SearchAgentGroupRequest request2 = new SearchAgentGroupRequest(agentCode);
			 SearchAgentGroupPortServiceLocator locator2 = new SearchAgentGroupPortServiceLocator();
			 locator2.setSearchAgentGroupPortSoap11EndpointAddress(endpoint);
			 SearchAgentGroupResponse res2 = locator2.getSearchAgentGroupPortSoap11().searchAgentGroup(request2);
 
			 System.out.println(" getAgentDetailByAgentCode --> response.getAgentCode() :"+res.getAgentCode());
			 System.out.println(" getAgentDetailByAgentCode --> response.getAgentChannel() :"+res.getChannelCode());
			 System.out.println(" getAgentDetailByAgentCode --> response.getAgentGroup() :"+res2.getAgentGroup());
			 System.out.println(" getAgentDetailByAgentCode --> response.getServiceBranch() :"+res.getServiceBranch());
			 
			 agentInfo.setPlanCode(planCode);
			 agentInfo.setAgentCode(agentCode);
			 agentInfo.setAgentChannel(res.getChannelCode());
			 if(res2.getAgentGroup()!=null){
				 agentInfo.setAgentGroup(res2.getAgentGroup());
			 }else{
				 agentInfo.setAgentGroup("1");
			 }
			 
			 agentInfo.setQualifiation(res.getQualifiation());
			 agentInfo.setServiceBranch(res.getServiceBranch());
	    }catch(Exception ex) {
	    	ex.printStackTrace();
	    	throw ex;
	    } 
		return agentInfo;
	}
	 
} 
