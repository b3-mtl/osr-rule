package com.mtl.collector.ws.service;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.model.ClientProfile;
import com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator;

@Service
public class GetSearchClientProfileByIdCardNoWsClientService {
 
	@Value("${ws.SearchClientProfilebyIDCardNo.endpoint}")
	private String endpoint;
	
	public ClientProfile searchClientProfilebyIDCardNo(String idCardNo) throws RemoteException,ServiceException {	
		
		com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderSearchClientProfilebyIDCardNoRequest request = new com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderSearchClientProfilebyIDCardNoRequest();

		request.setIdCardNo(idCardNo);
		request.setDetail("");
		request.setServiceID("");
		request.setCallName("");
		request.setReason("");
		
		com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoResponse response =null;
		ClientProfile agentDetail = new ClientProfile();
		try {
			SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator locator = new SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator();
			locator.setSearchClientProfilebyIDCardNoWsImplPortBindingQSPortEndpointAddress(endpoint);
			response = locator.getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort().searchClientProfilebyIDCardNo(request);
			
			
			//com.mtl.collector.ws.SearchClientProfilebyIDCardNo.Response response =response1.getResponse();
			
			agentDetail.setCodeClient(response.getCodeClient());
			agentDetail.setTitle(response.getTitle());
			agentDetail.setFirstName(response.getFirstName());
			agentDetail.setLastName(response.getLastName());
			agentDetail.setTitleEng(response.getTitleEng());
			agentDetail.setFirstNameEng(response.getFirstNameEng());
			agentDetail.setLastNameEng(response.getLastNameEng());
			agentDetail.setSsn(response.getSsn());
			// Please Chang value to Simple Date Format
			String birthDatestr = response.getBirthDate()==null?"":response.getBirthDate().toString();
			agentDetail.setBirthDate(birthDatestr);
			agentDetail.setAddress1(response.getAddress1());
			agentDetail.setAddress2(response.getAddress2());
			agentDetail.setAddress3(response.getAddress3());
			agentDetail.setCity(response.getCity());
			agentDetail.setProvince(response.getProvince());
			agentDetail.setCountry(response.getCountry());
			agentDetail.setDistrict(response.getDistrict());
			agentDetail.setAddressPostal(response.getAddressPostal());
			agentDetail.setSex(response.getSex());
			
			 		
			//System.out.println("getClientProfileByIdCardNo :::::::::::::::::::::: " + response.getCodeClient());
		} catch (RemoteException e) {
			e.printStackTrace();
			throw e;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		}
		
		return agentDetail;	
	}

}
