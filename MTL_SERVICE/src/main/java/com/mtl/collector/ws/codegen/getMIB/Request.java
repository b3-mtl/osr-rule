/**
 * Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

public class Request  implements java.io.Serializable {
    private java.lang.String appFormNumber;

    private java.util.Calendar birthDate;

    private java.lang.String clientNumber;

    private int filterPercent;

    private java.lang.String firstName;

    private java.lang.String fullName;

    private java.lang.String idCard;

    private java.lang.String lastName;

    private java.lang.String nationalityCode;

    private java.lang.String[] planCode;

    public Request() {
    }

    public Request(
           java.lang.String appFormNumber,
           java.util.Calendar birthDate,
           java.lang.String clientNumber,
           int filterPercent,
           java.lang.String firstName,
           java.lang.String fullName,
           java.lang.String idCard,
           java.lang.String lastName,
           java.lang.String nationalityCode,
           java.lang.String[] planCode) {
           this.appFormNumber = appFormNumber;
           this.birthDate = birthDate;
           this.clientNumber = clientNumber;
           this.filterPercent = filterPercent;
           this.firstName = firstName;
           this.fullName = fullName;
           this.idCard = idCard;
           this.lastName = lastName;
           this.nationalityCode = nationalityCode;
           this.planCode = planCode;
    }


    /**
     * Gets the appFormNumber value for this Request.
     * 
     * @return appFormNumber
     */
    public java.lang.String getAppFormNumber() {
        return appFormNumber;
    }


    /**
     * Sets the appFormNumber value for this Request.
     * 
     * @param appFormNumber
     */
    public void setAppFormNumber(java.lang.String appFormNumber) {
        this.appFormNumber = appFormNumber;
    }


    /**
     * Gets the birthDate value for this Request.
     * 
     * @return birthDate
     */
    public java.util.Calendar getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this Request.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.util.Calendar birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the clientNumber value for this Request.
     * 
     * @return clientNumber
     */
    public java.lang.String getClientNumber() {
        return clientNumber;
    }


    /**
     * Sets the clientNumber value for this Request.
     * 
     * @param clientNumber
     */
    public void setClientNumber(java.lang.String clientNumber) {
        this.clientNumber = clientNumber;
    }


    /**
     * Gets the filterPercent value for this Request.
     * 
     * @return filterPercent
     */
    public int getFilterPercent() {
        return filterPercent;
    }


    /**
     * Sets the filterPercent value for this Request.
     * 
     * @param filterPercent
     */
    public void setFilterPercent(int filterPercent) {
        this.filterPercent = filterPercent;
    }


    /**
     * Gets the firstName value for this Request.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this Request.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the fullName value for this Request.
     * 
     * @return fullName
     */
    public java.lang.String getFullName() {
        return fullName;
    }


    /**
     * Sets the fullName value for this Request.
     * 
     * @param fullName
     */
    public void setFullName(java.lang.String fullName) {
        this.fullName = fullName;
    }


    /**
     * Gets the idCard value for this Request.
     * 
     * @return idCard
     */
    public java.lang.String getIdCard() {
        return idCard;
    }


    /**
     * Sets the idCard value for this Request.
     * 
     * @param idCard
     */
    public void setIdCard(java.lang.String idCard) {
        this.idCard = idCard;
    }


    /**
     * Gets the lastName value for this Request.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this Request.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the nationalityCode value for this Request.
     * 
     * @return nationalityCode
     */
    public java.lang.String getNationalityCode() {
        return nationalityCode;
    }


    /**
     * Sets the nationalityCode value for this Request.
     * 
     * @param nationalityCode
     */
    public void setNationalityCode(java.lang.String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }


    /**
     * Gets the planCode value for this Request.
     * 
     * @return planCode
     */
    public java.lang.String[] getPlanCode() {
        return planCode;
    }


    /**
     * Sets the planCode value for this Request.
     * 
     * @param planCode
     */
    public void setPlanCode(java.lang.String[] planCode) {
        this.planCode = planCode;
    }

    public java.lang.String getPlanCode(int i) {
        return this.planCode[i];
    }

    public void setPlanCode(int i, java.lang.String _value) {
        this.planCode[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Request)) return false;
        Request other = (Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.appFormNumber==null && other.getAppFormNumber()==null) || 
             (this.appFormNumber!=null &&
              this.appFormNumber.equals(other.getAppFormNumber()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.clientNumber==null && other.getClientNumber()==null) || 
             (this.clientNumber!=null &&
              this.clientNumber.equals(other.getClientNumber()))) &&
            this.filterPercent == other.getFilterPercent() &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.fullName==null && other.getFullName()==null) || 
             (this.fullName!=null &&
              this.fullName.equals(other.getFullName()))) &&
            ((this.idCard==null && other.getIdCard()==null) || 
             (this.idCard!=null &&
              this.idCard.equals(other.getIdCard()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.nationalityCode==null && other.getNationalityCode()==null) || 
             (this.nationalityCode!=null &&
              this.nationalityCode.equals(other.getNationalityCode()))) &&
            ((this.planCode==null && other.getPlanCode()==null) || 
             (this.planCode!=null &&
              java.util.Arrays.equals(this.planCode, other.getPlanCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAppFormNumber() != null) {
            _hashCode += getAppFormNumber().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getClientNumber() != null) {
            _hashCode += getClientNumber().hashCode();
        }
        _hashCode += getFilterPercent();
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getFullName() != null) {
            _hashCode += getFullName().hashCode();
        }
        if (getIdCard() != null) {
            _hashCode += getIdCard().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getNationalityCode() != null) {
            _hashCode += getNationalityCode().hashCode();
        }
        if (getPlanCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPlanCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPlanCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("appFormNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "appFormNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "birthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clientNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filterPercent");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filterPercent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "firstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fullName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCard");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idCard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationalityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nationalityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "planCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
