package com.mtl.collector.ws.codegen.searchagentgroup;

public class SearchAgentGroupPortProxy implements com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPort {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPort searchAgentGroupPort = null;
  
  public SearchAgentGroupPortProxy() {
    _initSearchAgentGroupPortProxy();
  }
  
  public SearchAgentGroupPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initSearchAgentGroupPortProxy();
  }
  
  private void _initSearchAgentGroupPortProxy() {
    try {
      searchAgentGroupPort = (new com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPortServiceLocator()).getSearchAgentGroupPortSoap11();
      if (searchAgentGroupPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)searchAgentGroupPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)searchAgentGroupPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (searchAgentGroupPort != null)
      ((javax.xml.rpc.Stub)searchAgentGroupPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPort getSearchAgentGroupPort() {
    if (searchAgentGroupPort == null)
      _initSearchAgentGroupPortProxy();
    return searchAgentGroupPort;
  }
  
  public com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupResponse searchAgentGroup(com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupRequest searchAgentGroupRequest) throws java.rmi.RemoteException{
    if (searchAgentGroupPort == null)
      _initSearchAgentGroupPortProxy();
    return searchAgentGroupPort.searchAgentGroup(searchAgentGroupRequest);
  }
  
  
}