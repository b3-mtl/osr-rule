
package com.mtl.collector.ws.codegen.GetDataColPolicyCoverage;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mtl.collector.ws.GetDataColPolicyCoverage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mtl.collector.ws.GetDataColPolicyCoverage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Request.ClientList }
     * 
     */
    public Request.ClientList createRequestClientList() {
        return new Request.ClientList();
    }

    /**
     * Create an instance of {@link Response.PolicyList }
     * 
     */
    public Response.PolicyList createResponsePolicyList() {
        return new Response.PolicyList();
    }

    /**
     * Create an instance of {@link CoverageLists }
     * 
     */
    public CoverageLists createCoverageLists() {
        return new CoverageLists();
    }

}
