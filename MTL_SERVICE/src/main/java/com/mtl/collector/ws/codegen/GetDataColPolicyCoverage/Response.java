
package com.mtl.collector.ws.codegen.GetDataColPolicyCoverage;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PolicyList" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="Relationship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PolicyStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TrailerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ClientNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ChannelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="WritingAgent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ServicingBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CoverageList" type="{http://www.example.org/SOA-BRMS4NB-GetDataColPolicyCoverageService/}CoverageLists" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyList"
})
@XmlRootElement(name = "Response")
public class Response {

    @XmlElement(name = "PolicyList", required = true)
    protected List<Response.PolicyList> policyList;

    /**
     * Gets the value of the policyList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Response.PolicyList }
     * 
     * 
     */
    public List<Response.PolicyList> getPolicyList() {
        if (policyList == null) {
            policyList = new ArrayList<Response.PolicyList>();
        }
        return this.policyList;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="Relationship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PolicyStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TrailerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ClientNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ChannelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="WritingAgent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ServicingBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CoverageList" type="{http://www.example.org/SOA-BRMS4NB-GetDataColPolicyCoverageService/}CoverageLists" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "policyNumber",
        "relationship",
        "policyStatus",
        "trailerNumber",
        "clientNumber",
        "channelCode",
        "writingAgent",
        "servicingBranch",
        "coverageList"
    })
    public static class PolicyList {

        @XmlElement(name = "PolicyNumber")
        protected String policyNumber;
        @XmlElement(name = "Relationship")
        protected String relationship;
        @XmlElement(name = "PolicyStatus")
        protected String policyStatus;
        @XmlElement(name = "TrailerNumber")
        protected String trailerNumber;
        @XmlElement(name = "ClientNumber")
        protected String clientNumber;
        @XmlElement(name = "ChannelCode")
        protected String channelCode;
        @XmlElement(name = "WritingAgent")
        protected String writingAgent;
        @XmlElement(name = "ServicingBranch")
        protected String servicingBranch;
        @XmlElement(name = "CoverageList")
        protected List<CoverageLists> coverageList;

        /**
         * Gets the value of the policyNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyNumber() {
            return policyNumber;
        }

        /**
         * Sets the value of the policyNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyNumber(String value) {
            this.policyNumber = value;
        }

        /**
         * Gets the value of the relationship property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRelationship() {
            return relationship;
        }

        /**
         * Sets the value of the relationship property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRelationship(String value) {
            this.relationship = value;
        }

        /**
         * Gets the value of the policyStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyStatus() {
            return policyStatus;
        }

        /**
         * Sets the value of the policyStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyStatus(String value) {
            this.policyStatus = value;
        }

        /**
         * Gets the value of the trailerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrailerNumber() {
            return trailerNumber;
        }

        /**
         * Sets the value of the trailerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrailerNumber(String value) {
            this.trailerNumber = value;
        }

        /**
         * Gets the value of the clientNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientNumber() {
            return clientNumber;
        }

        /**
         * Sets the value of the clientNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientNumber(String value) {
            this.clientNumber = value;
        }

        /**
         * Gets the value of the channelCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannelCode() {
            return channelCode;
        }

        /**
         * Sets the value of the channelCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannelCode(String value) {
            this.channelCode = value;
        }

        /**
         * Gets the value of the writingAgent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWritingAgent() {
            return writingAgent;
        }

        /**
         * Sets the value of the writingAgent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWritingAgent(String value) {
            this.writingAgent = value;
        }

        /**
         * Gets the value of the servicingBranch property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServicingBranch() {
            return servicingBranch;
        }

        /**
         * Sets the value of the servicingBranch property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServicingBranch(String value) {
            this.servicingBranch = value;
        }

        /**
         * Gets the value of the coverageList property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the coverageList property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCoverageList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CoverageLists }
         * 
         * 
         */
        public List<CoverageLists> getCoverageList() {
            if (coverageList == null) {
                coverageList = new ArrayList<CoverageLists>();
            }
            return this.coverageList;
        }

    }

}
