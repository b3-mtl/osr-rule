/**
 * SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

import org.springframework.beans.factory.annotation.Value;

public class SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator extends org.apache.axis.client.Service implements com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingQSService {

	
    public SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator() {
    }


    public SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SearchClientProfilebyIDCardNoWsImplPortBindingQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SearchClientProfilebyIDCardNoWsImplPortBindingQSPort
    private java.lang.String SearchClientProfilebyIDCardNoWsImplPortBindingQSPort_address = "";

    public java.lang.String getSearchClientProfilebyIDCardNoWsImplPortBindingQSPortAddress() {
        return SearchClientProfilebyIDCardNoWsImplPortBindingQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName = "SearchClientProfilebyIDCardNoWsImplPortBindingQSPort";

    public java.lang.String getSearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName() {
        return SearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName;
    }

    public void setSearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName(java.lang.String name) {
        SearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName = name;
    }

    public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SearchClientProfilebyIDCardNoWsImplPortBindingQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort(endpoint);
    }

    public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingStub _stub = new com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingStub(portAddress, this);
            _stub.setPortName(getSearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSearchClientProfilebyIDCardNoWsImplPortBindingQSPortEndpointAddress(java.lang.String address) {
        SearchClientProfilebyIDCardNoWsImplPortBindingQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingStub _stub = new com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoWsImplPortBindingStub(new java.net.URL(SearchClientProfilebyIDCardNoWsImplPortBindingQSPort_address), this);
                _stub.setPortName(getSearchClientProfilebyIDCardNoWsImplPortBindingQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SearchClientProfilebyIDCardNoWsImplPortBindingQSPort".equals(inputPortName)) {
            return getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.searchclientprofilebyidcardno.ws.application.muangthai.co.th/", "SearchClientProfilebyIDCardNoWsImplPortBindingQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.searchclientprofilebyidcardno.ws.application.muangthai.co.th/", "SearchClientProfilebyIDCardNoWsImplPortBindingQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SearchClientProfilebyIDCardNoWsImplPortBindingQSPort".equals(portName)) {
            setSearchClientProfilebyIDCardNoWsImplPortBindingQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
