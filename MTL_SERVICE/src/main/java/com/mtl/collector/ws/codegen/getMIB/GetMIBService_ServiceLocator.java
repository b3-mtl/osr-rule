/**
 * GetMIBService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

import org.springframework.beans.factory.annotation.Value;

public class GetMIBService_ServiceLocator extends org.apache.axis.client.Service implements com.mtl.collector.ws.codegen.getMIB.GetMIBService_Service {
	
    public GetMIBService_ServiceLocator() {
    }


    public GetMIBService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetMIBService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetMIBServicePort
    private java.lang.String GetMIBServicePort_address = "";

    public java.lang.String getGetMIBServicePortAddress() {
        return GetMIBServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetMIBServicePortWSDDServiceName = "GetMIBServicePort";

    public java.lang.String getGetMIBServicePortWSDDServiceName() {
        return GetMIBServicePortWSDDServiceName;
    }

    public void setGetMIBServicePortWSDDServiceName(java.lang.String name) {
        GetMIBServicePortWSDDServiceName = name;
    }

    public com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getGetMIBServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetMIBServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetMIBServicePort(endpoint);
    }

    public com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType getGetMIBServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mtl.collector.ws.codegen.getMIB.GetMIBServicePortBindingStub _stub = new com.mtl.collector.ws.codegen.getMIB.GetMIBServicePortBindingStub(portAddress, this);
            _stub.setPortName(getGetMIBServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetMIBServicePortEndpointAddress(java.lang.String address) {
        GetMIBServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mtl.collector.ws.codegen.getMIB.GetMIBService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mtl.collector.ws.codegen.getMIB.GetMIBServicePortBindingStub _stub = new com.mtl.collector.ws.codegen.getMIB.GetMIBServicePortBindingStub(new java.net.URL(GetMIBServicePort_address), this);
                _stub.setPortName(getGetMIBServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetMIBServicePort".equals(inputPortName)) {
            return getGetMIBServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "GetMIBService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "GetMIBServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetMIBServicePort".equals(portName)) {
            setGetMIBServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
