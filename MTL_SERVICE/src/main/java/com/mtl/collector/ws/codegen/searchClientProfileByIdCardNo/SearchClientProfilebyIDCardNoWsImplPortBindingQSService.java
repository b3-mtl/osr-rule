/**
 * SearchClientProfilebyIDCardNoWsImplPortBindingQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

public interface SearchClientProfilebyIDCardNoWsImplPortBindingQSService extends javax.xml.rpc.Service {

/**
 * OSB Service
 */
    public java.lang.String getSearchClientProfilebyIDCardNoWsImplPortBindingQSPortAddress();

    public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort() throws javax.xml.rpc.ServiceException;

    public com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.SearchClientProfilebyIDCardNoService getSearchClientProfilebyIDCardNoWsImplPortBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
