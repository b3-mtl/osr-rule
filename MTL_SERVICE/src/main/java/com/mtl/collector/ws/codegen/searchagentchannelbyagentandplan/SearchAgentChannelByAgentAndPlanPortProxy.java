package com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan;

public class SearchAgentChannelByAgentAndPlanPortProxy implements com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort {
  private String _endpoint = null;
  private com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort searchAgentChannelByAgentAndPlanPort = null;
  
  public SearchAgentChannelByAgentAndPlanPortProxy() {
    _initSearchAgentChannelByAgentAndPlanPortProxy();
  }
  
  public SearchAgentChannelByAgentAndPlanPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initSearchAgentChannelByAgentAndPlanPortProxy();
  }
  
  private void _initSearchAgentChannelByAgentAndPlanPortProxy() {
    try {
      searchAgentChannelByAgentAndPlanPort = (new com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortServiceLocator()).getSearchAgentChannelByAgentAndPlanPortSoap11();
      if (searchAgentChannelByAgentAndPlanPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)searchAgentChannelByAgentAndPlanPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)searchAgentChannelByAgentAndPlanPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (searchAgentChannelByAgentAndPlanPort != null)
      ((javax.xml.rpc.Stub)searchAgentChannelByAgentAndPlanPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort getSearchAgentChannelByAgentAndPlanPort() {
    if (searchAgentChannelByAgentAndPlanPort == null)
      _initSearchAgentChannelByAgentAndPlanPortProxy();
    return searchAgentChannelByAgentAndPlanPort;
  }
  
  public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanResponse searchAgentChannelByAgentAndPlan(com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanRequest searchAgentChannelByAgentAndPlanRequest) throws java.rmi.RemoteException{
    if (searchAgentChannelByAgentAndPlanPort == null)
      _initSearchAgentChannelByAgentAndPlanPortProxy();
    return searchAgentChannelByAgentAndPlanPort.searchAgentChannelByAgentAndPlan(searchAgentChannelByAgentAndPlanRequest);
  }
  
  
}