package com.mtl.collector.ws.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanRequest;
import com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanResponse;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeRequest;
import com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeResponse;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupPortServiceLocator;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupRequest;
import com.mtl.collector.ws.codegen.searchagentgroup.SearchAgentGroupResponse;
import com.mtl.model.common.AgentInfo;

@Service
public class GetGroupChannelWsService { 
	
	@Value("${ws.getAgentDetail.endpoint}")
	private String endpoint;
	
	public String getGroupChannelByAgentCodeAndPlanCodeWsService(String agentCode ,String planCode) throws Exception {	
		String groupChannelCode = "";
	    try{
	    	
			 SearchAgentChannelByAgentAndPlanRequest request = new SearchAgentChannelByAgentAndPlanRequest(agentCode,planCode);
			 SearchAgentChannelByAgentAndPlanPortServiceLocator locator = new SearchAgentChannelByAgentAndPlanPortServiceLocator();
			 locator.setSearchAgentChannelByAgentAndPlanPortSoap11EndpointAddress(endpoint);
			 SearchAgentChannelByAgentAndPlanResponse res = new SearchAgentChannelByAgentAndPlanResponse();
			 if(StringUtils.isNoneBlank(planCode)) {
				 res = locator.getSearchAgentChannelByAgentAndPlanPortSoap11().searchAgentChannelByAgentAndPlan(request);
				 groupChannelCode = res.getGroupChannel();
//				 System.out.println(" GetGroupChannelByAgentCodeAndPlanCodeWsService --> res.getGroupChannel() :"+res.getGroupChannel());
			 }

			 
	    }catch(Exception ex) {
	    	ex.printStackTrace();
	    	throw ex;
	    } 
		return groupChannelCode;
	}
	 
} 
