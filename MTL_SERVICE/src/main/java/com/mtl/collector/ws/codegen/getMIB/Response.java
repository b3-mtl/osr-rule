/**
 * Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

public class Response  implements java.io.Serializable {
    private boolean flagClaim;

    private boolean flagMibHealth;

    private com.mtl.collector.ws.codegen.getMIB.MibResultList[] mibResults;

    public Response() {
    }

    public Response(
           boolean flagClaim,
           boolean flagMibHealth,
           com.mtl.collector.ws.codegen.getMIB.MibResultList[] mibResults) {
           this.flagClaim = flagClaim;
           this.flagMibHealth = flagMibHealth;
           this.mibResults = mibResults;
    }


    /**
     * Gets the flagClaim value for this Response.
     * 
     * @return flagClaim
     */
    public boolean isFlagClaim() {
        return flagClaim;
    }


    /**
     * Sets the flagClaim value for this Response.
     * 
     * @param flagClaim
     */
    public void setFlagClaim(boolean flagClaim) {
        this.flagClaim = flagClaim;
    }


    /**
     * Gets the flagMibHealth value for this Response.
     * 
     * @return flagMibHealth
     */
    public boolean isFlagMibHealth() {
        return flagMibHealth;
    }


    /**
     * Sets the flagMibHealth value for this Response.
     * 
     * @param flagMibHealth
     */
    public void setFlagMibHealth(boolean flagMibHealth) {
        this.flagMibHealth = flagMibHealth;
    }


    /**
     * Gets the mibResults value for this Response.
     * 
     * @return mibResults
     */
    public com.mtl.collector.ws.codegen.getMIB.MibResultList[] getMibResults() {
        return mibResults;
    }


    /**
     * Sets the mibResults value for this Response.
     * 
     * @param mibResults
     */
    public void setMibResults(com.mtl.collector.ws.codegen.getMIB.MibResultList[] mibResults) {
        this.mibResults = mibResults;
    }

    public com.mtl.collector.ws.codegen.getMIB.MibResultList getMibResults(int i) {
        return this.mibResults[i];
    }

    public void setMibResults(int i, com.mtl.collector.ws.codegen.getMIB.MibResultList _value) {
        this.mibResults[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Response)) return false;
        Response other = (Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.flagClaim == other.isFlagClaim() &&
            this.flagMibHealth == other.isFlagMibHealth() &&
            ((this.mibResults==null && other.getMibResults()==null) || 
             (this.mibResults!=null &&
              java.util.Arrays.equals(this.mibResults, other.getMibResults())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isFlagClaim() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isFlagMibHealth() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getMibResults() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMibResults());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMibResults(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flagClaim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flagClaim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flagMibHealth");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flagMibHealth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mibResults");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mibResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "mibResultList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
