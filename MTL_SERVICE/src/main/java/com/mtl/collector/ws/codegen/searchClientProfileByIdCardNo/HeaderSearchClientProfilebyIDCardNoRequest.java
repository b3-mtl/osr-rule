/**
 * HeaderSearchClientProfilebyIDCardNoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo;

public class HeaderSearchClientProfilebyIDCardNoRequest  extends com.mtl.collector.ws.codegen.searchClientProfileByIdCardNo.HeaderXml  implements java.io.Serializable {
    private java.lang.String idCardNo;

    public HeaderSearchClientProfilebyIDCardNoRequest() {
    }

    public HeaderSearchClientProfilebyIDCardNoRequest(
           java.lang.String serviceID,
           java.lang.String callName,
           java.lang.String requestDate,
           java.lang.String respondDate,
           int status,
           java.lang.String reason,
           java.lang.String detail,
           java.lang.String idCardNo) {
        super(
            serviceID,
            callName,
            requestDate,
            respondDate,
            status,
            reason,
            detail);
        this.idCardNo = idCardNo;
    }


    /**
     * Gets the idCardNo value for this HeaderSearchClientProfilebyIDCardNoRequest.
     * 
     * @return idCardNo
     */
    public java.lang.String getIdCardNo() {
        return idCardNo;
    }


    /**
     * Sets the idCardNo value for this HeaderSearchClientProfilebyIDCardNoRequest.
     * 
     * @param idCardNo
     */
    public void setIdCardNo(java.lang.String idCardNo) {
        this.idCardNo = idCardNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HeaderSearchClientProfilebyIDCardNoRequest)) return false;
        HeaderSearchClientProfilebyIDCardNoRequest other = (HeaderSearchClientProfilebyIDCardNoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.idCardNo==null && other.getIdCardNo()==null) || 
             (this.idCardNo!=null &&
              this.idCardNo.equals(other.getIdCardNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getIdCardNo() != null) {
            _hashCode += getIdCardNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HeaderSearchClientProfilebyIDCardNoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.searchclientprofilebyidcardno.ws.application.muangthai.co.th/", "headerSearchClientProfilebyIDCardNoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCardNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdCardNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
