/**
 * GetMIBService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

public interface GetMIBService_PortType extends java.rmi.Remote {
    public com.mtl.collector.ws.codegen.getMIB.Response getMIB(com.mtl.collector.ws.codegen.getMIB.Request name) throws java.rmi.RemoteException;
}
