/**
 * SearchAgentDetailByAgentCodePortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentdetailbyagentcode;

public interface SearchAgentDetailByAgentCodePortService extends javax.xml.rpc.Service {
    public java.lang.String getSearchAgentDetailByAgentCodePortSoap11Address();

    public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort getSearchAgentDetailByAgentCodePortSoap11() throws javax.xml.rpc.ServiceException;

    public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort getSearchAgentDetailByAgentCodePortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
