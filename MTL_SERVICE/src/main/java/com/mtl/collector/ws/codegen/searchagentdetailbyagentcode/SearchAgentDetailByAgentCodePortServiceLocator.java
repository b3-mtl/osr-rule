/**
 * SearchAgentDetailByAgentCodePortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentdetailbyagentcode;

import org.springframework.beans.factory.annotation.Value;

public class SearchAgentDetailByAgentCodePortServiceLocator extends org.apache.axis.client.Service implements com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortService {
	
    public SearchAgentDetailByAgentCodePortServiceLocator() {
    }


    public SearchAgentDetailByAgentCodePortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SearchAgentDetailByAgentCodePortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SearchAgentDetailByAgentCodePortSoap11
    private java.lang.String SearchAgentDetailByAgentCodePortSoap11_address = "";

    public java.lang.String getSearchAgentDetailByAgentCodePortSoap11Address() {
        return SearchAgentDetailByAgentCodePortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SearchAgentDetailByAgentCodePortSoap11WSDDServiceName = "SearchAgentDetailByAgentCodePortSoap11";

    public java.lang.String getSearchAgentDetailByAgentCodePortSoap11WSDDServiceName() {
        return SearchAgentDetailByAgentCodePortSoap11WSDDServiceName;
    }

    public void setSearchAgentDetailByAgentCodePortSoap11WSDDServiceName(java.lang.String name) {
        SearchAgentDetailByAgentCodePortSoap11WSDDServiceName = name;
    }

    public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort getSearchAgentDetailByAgentCodePortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SearchAgentDetailByAgentCodePortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSearchAgentDetailByAgentCodePortSoap11(endpoint);
    }

    public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort getSearchAgentDetailByAgentCodePortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortSoap11Stub _stub = new com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortSoap11Stub(portAddress, this);
            _stub.setPortName(getSearchAgentDetailByAgentCodePortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSearchAgentDetailByAgentCodePortSoap11EndpointAddress(java.lang.String address) {
    	SearchAgentDetailByAgentCodePortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePort.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortSoap11Stub _stub = new com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodePortSoap11Stub(new java.net.URL(SearchAgentDetailByAgentCodePortSoap11_address), this);
                _stub.setPortName(getSearchAgentDetailByAgentCodePortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SearchAgentDetailByAgentCodePortSoap11".equals(inputPortName)) {
            return getSearchAgentDetailByAgentCodePortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "SearchAgentDetailByAgentCodePortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://searchagentdetailbyagentcode.ws.muangthai.co.th", "SearchAgentDetailByAgentCodePortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SearchAgentDetailByAgentCodePortSoap11".equals(portName)) {
            setSearchAgentDetailByAgentCodePortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
