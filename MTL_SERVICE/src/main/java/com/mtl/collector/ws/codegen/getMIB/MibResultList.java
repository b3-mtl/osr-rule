/**
 * MibResultList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.getMIB;

public class MibResultList  implements java.io.Serializable {
    private java.lang.String ACTIVE_STATUS;

    private java.lang.String ASSOCIATE_NAME;

    private java.lang.String DATE_OF_DEATH;

    private java.lang.String DECEASED;

    private java.lang.String EX_RELATIONSHIP;

    private java.lang.Integer MI_BIRTH_DATE;

    private java.lang.Integer MI_CARDNO;

    private java.lang.String MI_CODE_CLIENT;

    private java.lang.Integer MI_COMCOD;

    private java.lang.String MI_CUAS01;

    private java.lang.String MI_CUAS02;

    private java.lang.Integer MI_CUAS11;

    private java.lang.Integer MI_CUAS21;

    private java.lang.String MI_FILLER;

    private java.lang.String MI_FNAME;

    private java.lang.String MI_FNAME_L3_27;

    private java.lang.String MI_FNAME_L3_3;

    private java.lang.String MI_FULLNAME;

    private java.lang.String MI_IDNO;

    private java.lang.String MI_LNAME;

    private java.lang.String MI_LNAME_L3_27;

    private java.lang.String MI_LNAME_L3_3;

    private java.lang.Integer MI_LUP_DATE;

    private java.lang.String MI_NEW;

    private java.lang.String MI_OP_CODE;

    private java.lang.String MI_P_STATUS;

    private java.lang.String MI_RCVCOM;

    private java.lang.Integer MI_RCV_DATE;

    private java.lang.String MI_RELATE;

    private java.lang.String MI_REMARK;

    private java.lang.Integer MI_RESULT;

    private java.lang.String MI_SEX;

    private java.lang.String MI_STATUS;

    private java.lang.String MI_TITLE;

    private java.lang.String OCCUPATION_TITLE;

    private java.lang.String RELATIONSHIP_TYPE;

    private java.lang.String REMARK_RELATIONSHIP;

    private java.lang.String TO_DATE_WORK;

    public MibResultList() {
    }

    public MibResultList(
           java.lang.String ACTIVE_STATUS,
           java.lang.String ASSOCIATE_NAME,
           java.lang.String DATE_OF_DEATH,
           java.lang.String DECEASED,
           java.lang.String EX_RELATIONSHIP,
           java.lang.Integer MI_BIRTH_DATE,
           java.lang.Integer MI_CARDNO,
           java.lang.String MI_CODE_CLIENT,
           java.lang.Integer MI_COMCOD,
           java.lang.String MI_CUAS01,
           java.lang.String MI_CUAS02,
           java.lang.Integer MI_CUAS11,
           java.lang.Integer MI_CUAS21,
           java.lang.String MI_FILLER,
           java.lang.String MI_FNAME,
           java.lang.String MI_FNAME_L3_27,
           java.lang.String MI_FNAME_L3_3,
           java.lang.String MI_FULLNAME,
           java.lang.String MI_IDNO,
           java.lang.String MI_LNAME,
           java.lang.String MI_LNAME_L3_27,
           java.lang.String MI_LNAME_L3_3,
           java.lang.Integer MI_LUP_DATE,
           java.lang.String MI_NEW,
           java.lang.String MI_OP_CODE,
           java.lang.String MI_P_STATUS,
           java.lang.String MI_RCVCOM,
           java.lang.Integer MI_RCV_DATE,
           java.lang.String MI_RELATE,
           java.lang.String MI_REMARK,
           java.lang.Integer MI_RESULT,
           java.lang.String MI_SEX,
           java.lang.String MI_STATUS,
           java.lang.String MI_TITLE,
           java.lang.String OCCUPATION_TITLE,
           java.lang.String RELATIONSHIP_TYPE,
           java.lang.String REMARK_RELATIONSHIP,
           java.lang.String TO_DATE_WORK) {
           this.ACTIVE_STATUS = ACTIVE_STATUS;
           this.ASSOCIATE_NAME = ASSOCIATE_NAME;
           this.DATE_OF_DEATH = DATE_OF_DEATH;
           this.DECEASED = DECEASED;
           this.EX_RELATIONSHIP = EX_RELATIONSHIP;
           this.MI_BIRTH_DATE = MI_BIRTH_DATE;
           this.MI_CARDNO = MI_CARDNO;
           this.MI_CODE_CLIENT = MI_CODE_CLIENT;
           this.MI_COMCOD = MI_COMCOD;
           this.MI_CUAS01 = MI_CUAS01;
           this.MI_CUAS02 = MI_CUAS02;
           this.MI_CUAS11 = MI_CUAS11;
           this.MI_CUAS21 = MI_CUAS21;
           this.MI_FILLER = MI_FILLER;
           this.MI_FNAME = MI_FNAME;
           this.MI_FNAME_L3_27 = MI_FNAME_L3_27;
           this.MI_FNAME_L3_3 = MI_FNAME_L3_3;
           this.MI_FULLNAME = MI_FULLNAME;
           this.MI_IDNO = MI_IDNO;
           this.MI_LNAME = MI_LNAME;
           this.MI_LNAME_L3_27 = MI_LNAME_L3_27;
           this.MI_LNAME_L3_3 = MI_LNAME_L3_3;
           this.MI_LUP_DATE = MI_LUP_DATE;
           this.MI_NEW = MI_NEW;
           this.MI_OP_CODE = MI_OP_CODE;
           this.MI_P_STATUS = MI_P_STATUS;
           this.MI_RCVCOM = MI_RCVCOM;
           this.MI_RCV_DATE = MI_RCV_DATE;
           this.MI_RELATE = MI_RELATE;
           this.MI_REMARK = MI_REMARK;
           this.MI_RESULT = MI_RESULT;
           this.MI_SEX = MI_SEX;
           this.MI_STATUS = MI_STATUS;
           this.MI_TITLE = MI_TITLE;
           this.OCCUPATION_TITLE = OCCUPATION_TITLE;
           this.RELATIONSHIP_TYPE = RELATIONSHIP_TYPE;
           this.REMARK_RELATIONSHIP = REMARK_RELATIONSHIP;
           this.TO_DATE_WORK = TO_DATE_WORK;
    }


    /**
     * Gets the ACTIVE_STATUS value for this MibResultList.
     * 
     * @return ACTIVE_STATUS
     */
    public java.lang.String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }


    /**
     * Sets the ACTIVE_STATUS value for this MibResultList.
     * 
     * @param ACTIVE_STATUS
     */
    public void setACTIVE_STATUS(java.lang.String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }


    /**
     * Gets the ASSOCIATE_NAME value for this MibResultList.
     * 
     * @return ASSOCIATE_NAME
     */
    public java.lang.String getASSOCIATE_NAME() {
        return ASSOCIATE_NAME;
    }


    /**
     * Sets the ASSOCIATE_NAME value for this MibResultList.
     * 
     * @param ASSOCIATE_NAME
     */
    public void setASSOCIATE_NAME(java.lang.String ASSOCIATE_NAME) {
        this.ASSOCIATE_NAME = ASSOCIATE_NAME;
    }


    /**
     * Gets the DATE_OF_DEATH value for this MibResultList.
     * 
     * @return DATE_OF_DEATH
     */
    public java.lang.String getDATE_OF_DEATH() {
        return DATE_OF_DEATH;
    }


    /**
     * Sets the DATE_OF_DEATH value for this MibResultList.
     * 
     * @param DATE_OF_DEATH
     */
    public void setDATE_OF_DEATH(java.lang.String DATE_OF_DEATH) {
        this.DATE_OF_DEATH = DATE_OF_DEATH;
    }


    /**
     * Gets the DECEASED value for this MibResultList.
     * 
     * @return DECEASED
     */
    public java.lang.String getDECEASED() {
        return DECEASED;
    }


    /**
     * Sets the DECEASED value for this MibResultList.
     * 
     * @param DECEASED
     */
    public void setDECEASED(java.lang.String DECEASED) {
        this.DECEASED = DECEASED;
    }


    /**
     * Gets the EX_RELATIONSHIP value for this MibResultList.
     * 
     * @return EX_RELATIONSHIP
     */
    public java.lang.String getEX_RELATIONSHIP() {
        return EX_RELATIONSHIP;
    }


    /**
     * Sets the EX_RELATIONSHIP value for this MibResultList.
     * 
     * @param EX_RELATIONSHIP
     */
    public void setEX_RELATIONSHIP(java.lang.String EX_RELATIONSHIP) {
        this.EX_RELATIONSHIP = EX_RELATIONSHIP;
    }


    /**
     * Gets the MI_BIRTH_DATE value for this MibResultList.
     * 
     * @return MI_BIRTH_DATE
     */
    public java.lang.Integer getMI_BIRTH_DATE() {
        return MI_BIRTH_DATE;
    }


    /**
     * Sets the MI_BIRTH_DATE value for this MibResultList.
     * 
     * @param MI_BIRTH_DATE
     */
    public void setMI_BIRTH_DATE(java.lang.Integer MI_BIRTH_DATE) {
        this.MI_BIRTH_DATE = MI_BIRTH_DATE;
    }


    /**
     * Gets the MI_CARDNO value for this MibResultList.
     * 
     * @return MI_CARDNO
     */
    public java.lang.Integer getMI_CARDNO() {
        return MI_CARDNO;
    }


    /**
     * Sets the MI_CARDNO value for this MibResultList.
     * 
     * @param MI_CARDNO
     */
    public void setMI_CARDNO(java.lang.Integer MI_CARDNO) {
        this.MI_CARDNO = MI_CARDNO;
    }


    /**
     * Gets the MI_CODE_CLIENT value for this MibResultList.
     * 
     * @return MI_CODE_CLIENT
     */
    public java.lang.String getMI_CODE_CLIENT() {
        return MI_CODE_CLIENT;
    }


    /**
     * Sets the MI_CODE_CLIENT value for this MibResultList.
     * 
     * @param MI_CODE_CLIENT
     */
    public void setMI_CODE_CLIENT(java.lang.String MI_CODE_CLIENT) {
        this.MI_CODE_CLIENT = MI_CODE_CLIENT;
    }


    /**
     * Gets the MI_COMCOD value for this MibResultList.
     * 
     * @return MI_COMCOD
     */
    public java.lang.Integer getMI_COMCOD() {
        return MI_COMCOD;
    }


    /**
     * Sets the MI_COMCOD value for this MibResultList.
     * 
     * @param MI_COMCOD
     */
    public void setMI_COMCOD(java.lang.Integer MI_COMCOD) {
        this.MI_COMCOD = MI_COMCOD;
    }


    /**
     * Gets the MI_CUAS01 value for this MibResultList.
     * 
     * @return MI_CUAS01
     */
    public java.lang.String getMI_CUAS01() {
        return MI_CUAS01;
    }


    /**
     * Sets the MI_CUAS01 value for this MibResultList.
     * 
     * @param MI_CUAS01
     */
    public void setMI_CUAS01(java.lang.String MI_CUAS01) {
        this.MI_CUAS01 = MI_CUAS01;
    }


    /**
     * Gets the MI_CUAS02 value for this MibResultList.
     * 
     * @return MI_CUAS02
     */
    public java.lang.String getMI_CUAS02() {
        return MI_CUAS02;
    }


    /**
     * Sets the MI_CUAS02 value for this MibResultList.
     * 
     * @param MI_CUAS02
     */
    public void setMI_CUAS02(java.lang.String MI_CUAS02) {
        this.MI_CUAS02 = MI_CUAS02;
    }


    /**
     * Gets the MI_CUAS11 value for this MibResultList.
     * 
     * @return MI_CUAS11
     */
    public java.lang.Integer getMI_CUAS11() {
        return MI_CUAS11;
    }


    /**
     * Sets the MI_CUAS11 value for this MibResultList.
     * 
     * @param MI_CUAS11
     */
    public void setMI_CUAS11(java.lang.Integer MI_CUAS11) {
        this.MI_CUAS11 = MI_CUAS11;
    }


    /**
     * Gets the MI_CUAS21 value for this MibResultList.
     * 
     * @return MI_CUAS21
     */
    public java.lang.Integer getMI_CUAS21() {
        return MI_CUAS21;
    }


    /**
     * Sets the MI_CUAS21 value for this MibResultList.
     * 
     * @param MI_CUAS21
     */
    public void setMI_CUAS21(java.lang.Integer MI_CUAS21) {
        this.MI_CUAS21 = MI_CUAS21;
    }


    /**
     * Gets the MI_FILLER value for this MibResultList.
     * 
     * @return MI_FILLER
     */
    public java.lang.String getMI_FILLER() {
        return MI_FILLER;
    }


    /**
     * Sets the MI_FILLER value for this MibResultList.
     * 
     * @param MI_FILLER
     */
    public void setMI_FILLER(java.lang.String MI_FILLER) {
        this.MI_FILLER = MI_FILLER;
    }


    /**
     * Gets the MI_FNAME value for this MibResultList.
     * 
     * @return MI_FNAME
     */
    public java.lang.String getMI_FNAME() {
        return MI_FNAME;
    }


    /**
     * Sets the MI_FNAME value for this MibResultList.
     * 
     * @param MI_FNAME
     */
    public void setMI_FNAME(java.lang.String MI_FNAME) {
        this.MI_FNAME = MI_FNAME;
    }


    /**
     * Gets the MI_FNAME_L3_27 value for this MibResultList.
     * 
     * @return MI_FNAME_L3_27
     */
    public java.lang.String getMI_FNAME_L3_27() {
        return MI_FNAME_L3_27;
    }


    /**
     * Sets the MI_FNAME_L3_27 value for this MibResultList.
     * 
     * @param MI_FNAME_L3_27
     */
    public void setMI_FNAME_L3_27(java.lang.String MI_FNAME_L3_27) {
        this.MI_FNAME_L3_27 = MI_FNAME_L3_27;
    }


    /**
     * Gets the MI_FNAME_L3_3 value for this MibResultList.
     * 
     * @return MI_FNAME_L3_3
     */
    public java.lang.String getMI_FNAME_L3_3() {
        return MI_FNAME_L3_3;
    }


    /**
     * Sets the MI_FNAME_L3_3 value for this MibResultList.
     * 
     * @param MI_FNAME_L3_3
     */
    public void setMI_FNAME_L3_3(java.lang.String MI_FNAME_L3_3) {
        this.MI_FNAME_L3_3 = MI_FNAME_L3_3;
    }


    /**
     * Gets the MI_FULLNAME value for this MibResultList.
     * 
     * @return MI_FULLNAME
     */
    public java.lang.String getMI_FULLNAME() {
        return MI_FULLNAME;
    }


    /**
     * Sets the MI_FULLNAME value for this MibResultList.
     * 
     * @param MI_FULLNAME
     */
    public void setMI_FULLNAME(java.lang.String MI_FULLNAME) {
        this.MI_FULLNAME = MI_FULLNAME;
    }


    /**
     * Gets the MI_IDNO value for this MibResultList.
     * 
     * @return MI_IDNO
     */
    public java.lang.String getMI_IDNO() {
        return MI_IDNO;
    }


    /**
     * Sets the MI_IDNO value for this MibResultList.
     * 
     * @param MI_IDNO
     */
    public void setMI_IDNO(java.lang.String MI_IDNO) {
        this.MI_IDNO = MI_IDNO;
    }


    /**
     * Gets the MI_LNAME value for this MibResultList.
     * 
     * @return MI_LNAME
     */
    public java.lang.String getMI_LNAME() {
        return MI_LNAME;
    }


    /**
     * Sets the MI_LNAME value for this MibResultList.
     * 
     * @param MI_LNAME
     */
    public void setMI_LNAME(java.lang.String MI_LNAME) {
        this.MI_LNAME = MI_LNAME;
    }


    /**
     * Gets the MI_LNAME_L3_27 value for this MibResultList.
     * 
     * @return MI_LNAME_L3_27
     */
    public java.lang.String getMI_LNAME_L3_27() {
        return MI_LNAME_L3_27;
    }


    /**
     * Sets the MI_LNAME_L3_27 value for this MibResultList.
     * 
     * @param MI_LNAME_L3_27
     */
    public void setMI_LNAME_L3_27(java.lang.String MI_LNAME_L3_27) {
        this.MI_LNAME_L3_27 = MI_LNAME_L3_27;
    }


    /**
     * Gets the MI_LNAME_L3_3 value for this MibResultList.
     * 
     * @return MI_LNAME_L3_3
     */
    public java.lang.String getMI_LNAME_L3_3() {
        return MI_LNAME_L3_3;
    }


    /**
     * Sets the MI_LNAME_L3_3 value for this MibResultList.
     * 
     * @param MI_LNAME_L3_3
     */
    public void setMI_LNAME_L3_3(java.lang.String MI_LNAME_L3_3) {
        this.MI_LNAME_L3_3 = MI_LNAME_L3_3;
    }


    /**
     * Gets the MI_LUP_DATE value for this MibResultList.
     * 
     * @return MI_LUP_DATE
     */
    public java.lang.Integer getMI_LUP_DATE() {
        return MI_LUP_DATE;
    }


    /**
     * Sets the MI_LUP_DATE value for this MibResultList.
     * 
     * @param MI_LUP_DATE
     */
    public void setMI_LUP_DATE(java.lang.Integer MI_LUP_DATE) {
        this.MI_LUP_DATE = MI_LUP_DATE;
    }


    /**
     * Gets the MI_NEW value for this MibResultList.
     * 
     * @return MI_NEW
     */
    public java.lang.String getMI_NEW() {
        return MI_NEW;
    }


    /**
     * Sets the MI_NEW value for this MibResultList.
     * 
     * @param MI_NEW
     */
    public void setMI_NEW(java.lang.String MI_NEW) {
        this.MI_NEW = MI_NEW;
    }


    /**
     * Gets the MI_OP_CODE value for this MibResultList.
     * 
     * @return MI_OP_CODE
     */
    public java.lang.String getMI_OP_CODE() {
        return MI_OP_CODE;
    }


    /**
     * Sets the MI_OP_CODE value for this MibResultList.
     * 
     * @param MI_OP_CODE
     */
    public void setMI_OP_CODE(java.lang.String MI_OP_CODE) {
        this.MI_OP_CODE = MI_OP_CODE;
    }


    /**
     * Gets the MI_P_STATUS value for this MibResultList.
     * 
     * @return MI_P_STATUS
     */
    public java.lang.String getMI_P_STATUS() {
        return MI_P_STATUS;
    }


    /**
     * Sets the MI_P_STATUS value for this MibResultList.
     * 
     * @param MI_P_STATUS
     */
    public void setMI_P_STATUS(java.lang.String MI_P_STATUS) {
        this.MI_P_STATUS = MI_P_STATUS;
    }


    /**
     * Gets the MI_RCVCOM value for this MibResultList.
     * 
     * @return MI_RCVCOM
     */
    public java.lang.String getMI_RCVCOM() {
        return MI_RCVCOM;
    }


    /**
     * Sets the MI_RCVCOM value for this MibResultList.
     * 
     * @param MI_RCVCOM
     */
    public void setMI_RCVCOM(java.lang.String MI_RCVCOM) {
        this.MI_RCVCOM = MI_RCVCOM;
    }


    /**
     * Gets the MI_RCV_DATE value for this MibResultList.
     * 
     * @return MI_RCV_DATE
     */
    public java.lang.Integer getMI_RCV_DATE() {
        return MI_RCV_DATE;
    }


    /**
     * Sets the MI_RCV_DATE value for this MibResultList.
     * 
     * @param MI_RCV_DATE
     */
    public void setMI_RCV_DATE(java.lang.Integer MI_RCV_DATE) {
        this.MI_RCV_DATE = MI_RCV_DATE;
    }


    /**
     * Gets the MI_RELATE value for this MibResultList.
     * 
     * @return MI_RELATE
     */
    public java.lang.String getMI_RELATE() {
        return MI_RELATE;
    }


    /**
     * Sets the MI_RELATE value for this MibResultList.
     * 
     * @param MI_RELATE
     */
    public void setMI_RELATE(java.lang.String MI_RELATE) {
        this.MI_RELATE = MI_RELATE;
    }


    /**
     * Gets the MI_REMARK value for this MibResultList.
     * 
     * @return MI_REMARK
     */
    public java.lang.String getMI_REMARK() {
        return MI_REMARK;
    }


    /**
     * Sets the MI_REMARK value for this MibResultList.
     * 
     * @param MI_REMARK
     */
    public void setMI_REMARK(java.lang.String MI_REMARK) {
        this.MI_REMARK = MI_REMARK;
    }


    /**
     * Gets the MI_RESULT value for this MibResultList.
     * 
     * @return MI_RESULT
     */
    public java.lang.Integer getMI_RESULT() {
        return MI_RESULT;
    }


    /**
     * Sets the MI_RESULT value for this MibResultList.
     * 
     * @param MI_RESULT
     */
    public void setMI_RESULT(java.lang.Integer MI_RESULT) {
        this.MI_RESULT = MI_RESULT;
    }


    /**
     * Gets the MI_SEX value for this MibResultList.
     * 
     * @return MI_SEX
     */
    public java.lang.String getMI_SEX() {
        return MI_SEX;
    }


    /**
     * Sets the MI_SEX value for this MibResultList.
     * 
     * @param MI_SEX
     */
    public void setMI_SEX(java.lang.String MI_SEX) {
        this.MI_SEX = MI_SEX;
    }


    /**
     * Gets the MI_STATUS value for this MibResultList.
     * 
     * @return MI_STATUS
     */
    public java.lang.String getMI_STATUS() {
        return MI_STATUS;
    }


    /**
     * Sets the MI_STATUS value for this MibResultList.
     * 
     * @param MI_STATUS
     */
    public void setMI_STATUS(java.lang.String MI_STATUS) {
        this.MI_STATUS = MI_STATUS;
    }


    /**
     * Gets the MI_TITLE value for this MibResultList.
     * 
     * @return MI_TITLE
     */
    public java.lang.String getMI_TITLE() {
        return MI_TITLE;
    }


    /**
     * Sets the MI_TITLE value for this MibResultList.
     * 
     * @param MI_TITLE
     */
    public void setMI_TITLE(java.lang.String MI_TITLE) {
        this.MI_TITLE = MI_TITLE;
    }


    /**
     * Gets the OCCUPATION_TITLE value for this MibResultList.
     * 
     * @return OCCUPATION_TITLE
     */
    public java.lang.String getOCCUPATION_TITLE() {
        return OCCUPATION_TITLE;
    }


    /**
     * Sets the OCCUPATION_TITLE value for this MibResultList.
     * 
     * @param OCCUPATION_TITLE
     */
    public void setOCCUPATION_TITLE(java.lang.String OCCUPATION_TITLE) {
        this.OCCUPATION_TITLE = OCCUPATION_TITLE;
    }


    /**
     * Gets the RELATIONSHIP_TYPE value for this MibResultList.
     * 
     * @return RELATIONSHIP_TYPE
     */
    public java.lang.String getRELATIONSHIP_TYPE() {
        return RELATIONSHIP_TYPE;
    }


    /**
     * Sets the RELATIONSHIP_TYPE value for this MibResultList.
     * 
     * @param RELATIONSHIP_TYPE
     */
    public void setRELATIONSHIP_TYPE(java.lang.String RELATIONSHIP_TYPE) {
        this.RELATIONSHIP_TYPE = RELATIONSHIP_TYPE;
    }


    /**
     * Gets the REMARK_RELATIONSHIP value for this MibResultList.
     * 
     * @return REMARK_RELATIONSHIP
     */
    public java.lang.String getREMARK_RELATIONSHIP() {
        return REMARK_RELATIONSHIP;
    }


    /**
     * Sets the REMARK_RELATIONSHIP value for this MibResultList.
     * 
     * @param REMARK_RELATIONSHIP
     */
    public void setREMARK_RELATIONSHIP(java.lang.String REMARK_RELATIONSHIP) {
        this.REMARK_RELATIONSHIP = REMARK_RELATIONSHIP;
    }


    /**
     * Gets the TO_DATE_WORK value for this MibResultList.
     * 
     * @return TO_DATE_WORK
     */
    public java.lang.String getTO_DATE_WORK() {
        return TO_DATE_WORK;
    }


    /**
     * Sets the TO_DATE_WORK value for this MibResultList.
     * 
     * @param TO_DATE_WORK
     */
    public void setTO_DATE_WORK(java.lang.String TO_DATE_WORK) {
        this.TO_DATE_WORK = TO_DATE_WORK;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MibResultList)) return false;
        MibResultList other = (MibResultList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ACTIVE_STATUS==null && other.getACTIVE_STATUS()==null) || 
             (this.ACTIVE_STATUS!=null &&
              this.ACTIVE_STATUS.equals(other.getACTIVE_STATUS()))) &&
            ((this.ASSOCIATE_NAME==null && other.getASSOCIATE_NAME()==null) || 
             (this.ASSOCIATE_NAME!=null &&
              this.ASSOCIATE_NAME.equals(other.getASSOCIATE_NAME()))) &&
            ((this.DATE_OF_DEATH==null && other.getDATE_OF_DEATH()==null) || 
             (this.DATE_OF_DEATH!=null &&
              this.DATE_OF_DEATH.equals(other.getDATE_OF_DEATH()))) &&
            ((this.DECEASED==null && other.getDECEASED()==null) || 
             (this.DECEASED!=null &&
              this.DECEASED.equals(other.getDECEASED()))) &&
            ((this.EX_RELATIONSHIP==null && other.getEX_RELATIONSHIP()==null) || 
             (this.EX_RELATIONSHIP!=null &&
              this.EX_RELATIONSHIP.equals(other.getEX_RELATIONSHIP()))) &&
            ((this.MI_BIRTH_DATE==null && other.getMI_BIRTH_DATE()==null) || 
             (this.MI_BIRTH_DATE!=null &&
              this.MI_BIRTH_DATE.equals(other.getMI_BIRTH_DATE()))) &&
            ((this.MI_CARDNO==null && other.getMI_CARDNO()==null) || 
             (this.MI_CARDNO!=null &&
              this.MI_CARDNO.equals(other.getMI_CARDNO()))) &&
            ((this.MI_CODE_CLIENT==null && other.getMI_CODE_CLIENT()==null) || 
             (this.MI_CODE_CLIENT!=null &&
              this.MI_CODE_CLIENT.equals(other.getMI_CODE_CLIENT()))) &&
            ((this.MI_COMCOD==null && other.getMI_COMCOD()==null) || 
             (this.MI_COMCOD!=null &&
              this.MI_COMCOD.equals(other.getMI_COMCOD()))) &&
            ((this.MI_CUAS01==null && other.getMI_CUAS01()==null) || 
             (this.MI_CUAS01!=null &&
              this.MI_CUAS01.equals(other.getMI_CUAS01()))) &&
            ((this.MI_CUAS02==null && other.getMI_CUAS02()==null) || 
             (this.MI_CUAS02!=null &&
              this.MI_CUAS02.equals(other.getMI_CUAS02()))) &&
            ((this.MI_CUAS11==null && other.getMI_CUAS11()==null) || 
             (this.MI_CUAS11!=null &&
              this.MI_CUAS11.equals(other.getMI_CUAS11()))) &&
            ((this.MI_CUAS21==null && other.getMI_CUAS21()==null) || 
             (this.MI_CUAS21!=null &&
              this.MI_CUAS21.equals(other.getMI_CUAS21()))) &&
            ((this.MI_FILLER==null && other.getMI_FILLER()==null) || 
             (this.MI_FILLER!=null &&
              this.MI_FILLER.equals(other.getMI_FILLER()))) &&
            ((this.MI_FNAME==null && other.getMI_FNAME()==null) || 
             (this.MI_FNAME!=null &&
              this.MI_FNAME.equals(other.getMI_FNAME()))) &&
            ((this.MI_FNAME_L3_27==null && other.getMI_FNAME_L3_27()==null) || 
             (this.MI_FNAME_L3_27!=null &&
              this.MI_FNAME_L3_27.equals(other.getMI_FNAME_L3_27()))) &&
            ((this.MI_FNAME_L3_3==null && other.getMI_FNAME_L3_3()==null) || 
             (this.MI_FNAME_L3_3!=null &&
              this.MI_FNAME_L3_3.equals(other.getMI_FNAME_L3_3()))) &&
            ((this.MI_FULLNAME==null && other.getMI_FULLNAME()==null) || 
             (this.MI_FULLNAME!=null &&
              this.MI_FULLNAME.equals(other.getMI_FULLNAME()))) &&
            ((this.MI_IDNO==null && other.getMI_IDNO()==null) || 
             (this.MI_IDNO!=null &&
              this.MI_IDNO.equals(other.getMI_IDNO()))) &&
            ((this.MI_LNAME==null && other.getMI_LNAME()==null) || 
             (this.MI_LNAME!=null &&
              this.MI_LNAME.equals(other.getMI_LNAME()))) &&
            ((this.MI_LNAME_L3_27==null && other.getMI_LNAME_L3_27()==null) || 
             (this.MI_LNAME_L3_27!=null &&
              this.MI_LNAME_L3_27.equals(other.getMI_LNAME_L3_27()))) &&
            ((this.MI_LNAME_L3_3==null && other.getMI_LNAME_L3_3()==null) || 
             (this.MI_LNAME_L3_3!=null &&
              this.MI_LNAME_L3_3.equals(other.getMI_LNAME_L3_3()))) &&
            ((this.MI_LUP_DATE==null && other.getMI_LUP_DATE()==null) || 
             (this.MI_LUP_DATE!=null &&
              this.MI_LUP_DATE.equals(other.getMI_LUP_DATE()))) &&
            ((this.MI_NEW==null && other.getMI_NEW()==null) || 
             (this.MI_NEW!=null &&
              this.MI_NEW.equals(other.getMI_NEW()))) &&
            ((this.MI_OP_CODE==null && other.getMI_OP_CODE()==null) || 
             (this.MI_OP_CODE!=null &&
              this.MI_OP_CODE.equals(other.getMI_OP_CODE()))) &&
            ((this.MI_P_STATUS==null && other.getMI_P_STATUS()==null) || 
             (this.MI_P_STATUS!=null &&
              this.MI_P_STATUS.equals(other.getMI_P_STATUS()))) &&
            ((this.MI_RCVCOM==null && other.getMI_RCVCOM()==null) || 
             (this.MI_RCVCOM!=null &&
              this.MI_RCVCOM.equals(other.getMI_RCVCOM()))) &&
            ((this.MI_RCV_DATE==null && other.getMI_RCV_DATE()==null) || 
             (this.MI_RCV_DATE!=null &&
              this.MI_RCV_DATE.equals(other.getMI_RCV_DATE()))) &&
            ((this.MI_RELATE==null && other.getMI_RELATE()==null) || 
             (this.MI_RELATE!=null &&
              this.MI_RELATE.equals(other.getMI_RELATE()))) &&
            ((this.MI_REMARK==null && other.getMI_REMARK()==null) || 
             (this.MI_REMARK!=null &&
              this.MI_REMARK.equals(other.getMI_REMARK()))) &&
            ((this.MI_RESULT==null && other.getMI_RESULT()==null) || 
             (this.MI_RESULT!=null &&
              this.MI_RESULT.equals(other.getMI_RESULT()))) &&
            ((this.MI_SEX==null && other.getMI_SEX()==null) || 
             (this.MI_SEX!=null &&
              this.MI_SEX.equals(other.getMI_SEX()))) &&
            ((this.MI_STATUS==null && other.getMI_STATUS()==null) || 
             (this.MI_STATUS!=null &&
              this.MI_STATUS.equals(other.getMI_STATUS()))) &&
            ((this.MI_TITLE==null && other.getMI_TITLE()==null) || 
             (this.MI_TITLE!=null &&
              this.MI_TITLE.equals(other.getMI_TITLE()))) &&
            ((this.OCCUPATION_TITLE==null && other.getOCCUPATION_TITLE()==null) || 
             (this.OCCUPATION_TITLE!=null &&
              this.OCCUPATION_TITLE.equals(other.getOCCUPATION_TITLE()))) &&
            ((this.RELATIONSHIP_TYPE==null && other.getRELATIONSHIP_TYPE()==null) || 
             (this.RELATIONSHIP_TYPE!=null &&
              this.RELATIONSHIP_TYPE.equals(other.getRELATIONSHIP_TYPE()))) &&
            ((this.REMARK_RELATIONSHIP==null && other.getREMARK_RELATIONSHIP()==null) || 
             (this.REMARK_RELATIONSHIP!=null &&
              this.REMARK_RELATIONSHIP.equals(other.getREMARK_RELATIONSHIP()))) &&
            ((this.TO_DATE_WORK==null && other.getTO_DATE_WORK()==null) || 
             (this.TO_DATE_WORK!=null &&
              this.TO_DATE_WORK.equals(other.getTO_DATE_WORK())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getACTIVE_STATUS() != null) {
            _hashCode += getACTIVE_STATUS().hashCode();
        }
        if (getASSOCIATE_NAME() != null) {
            _hashCode += getASSOCIATE_NAME().hashCode();
        }
        if (getDATE_OF_DEATH() != null) {
            _hashCode += getDATE_OF_DEATH().hashCode();
        }
        if (getDECEASED() != null) {
            _hashCode += getDECEASED().hashCode();
        }
        if (getEX_RELATIONSHIP() != null) {
            _hashCode += getEX_RELATIONSHIP().hashCode();
        }
        if (getMI_BIRTH_DATE() != null) {
            _hashCode += getMI_BIRTH_DATE().hashCode();
        }
        if (getMI_CARDNO() != null) {
            _hashCode += getMI_CARDNO().hashCode();
        }
        if (getMI_CODE_CLIENT() != null) {
            _hashCode += getMI_CODE_CLIENT().hashCode();
        }
        if (getMI_COMCOD() != null) {
            _hashCode += getMI_COMCOD().hashCode();
        }
        if (getMI_CUAS01() != null) {
            _hashCode += getMI_CUAS01().hashCode();
        }
        if (getMI_CUAS02() != null) {
            _hashCode += getMI_CUAS02().hashCode();
        }
        if (getMI_CUAS11() != null) {
            _hashCode += getMI_CUAS11().hashCode();
        }
        if (getMI_CUAS21() != null) {
            _hashCode += getMI_CUAS21().hashCode();
        }
        if (getMI_FILLER() != null) {
            _hashCode += getMI_FILLER().hashCode();
        }
        if (getMI_FNAME() != null) {
            _hashCode += getMI_FNAME().hashCode();
        }
        if (getMI_FNAME_L3_27() != null) {
            _hashCode += getMI_FNAME_L3_27().hashCode();
        }
        if (getMI_FNAME_L3_3() != null) {
            _hashCode += getMI_FNAME_L3_3().hashCode();
        }
        if (getMI_FULLNAME() != null) {
            _hashCode += getMI_FULLNAME().hashCode();
        }
        if (getMI_IDNO() != null) {
            _hashCode += getMI_IDNO().hashCode();
        }
        if (getMI_LNAME() != null) {
            _hashCode += getMI_LNAME().hashCode();
        }
        if (getMI_LNAME_L3_27() != null) {
            _hashCode += getMI_LNAME_L3_27().hashCode();
        }
        if (getMI_LNAME_L3_3() != null) {
            _hashCode += getMI_LNAME_L3_3().hashCode();
        }
        if (getMI_LUP_DATE() != null) {
            _hashCode += getMI_LUP_DATE().hashCode();
        }
        if (getMI_NEW() != null) {
            _hashCode += getMI_NEW().hashCode();
        }
        if (getMI_OP_CODE() != null) {
            _hashCode += getMI_OP_CODE().hashCode();
        }
        if (getMI_P_STATUS() != null) {
            _hashCode += getMI_P_STATUS().hashCode();
        }
        if (getMI_RCVCOM() != null) {
            _hashCode += getMI_RCVCOM().hashCode();
        }
        if (getMI_RCV_DATE() != null) {
            _hashCode += getMI_RCV_DATE().hashCode();
        }
        if (getMI_RELATE() != null) {
            _hashCode += getMI_RELATE().hashCode();
        }
        if (getMI_REMARK() != null) {
            _hashCode += getMI_REMARK().hashCode();
        }
        if (getMI_RESULT() != null) {
            _hashCode += getMI_RESULT().hashCode();
        }
        if (getMI_SEX() != null) {
            _hashCode += getMI_SEX().hashCode();
        }
        if (getMI_STATUS() != null) {
            _hashCode += getMI_STATUS().hashCode();
        }
        if (getMI_TITLE() != null) {
            _hashCode += getMI_TITLE().hashCode();
        }
        if (getOCCUPATION_TITLE() != null) {
            _hashCode += getOCCUPATION_TITLE().hashCode();
        }
        if (getRELATIONSHIP_TYPE() != null) {
            _hashCode += getRELATIONSHIP_TYPE().hashCode();
        }
        if (getREMARK_RELATIONSHIP() != null) {
            _hashCode += getREMARK_RELATIONSHIP().hashCode();
        }
        if (getTO_DATE_WORK() != null) {
            _hashCode += getTO_DATE_WORK().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MibResultList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mib.muangthai.co.th/", "mibResultList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACTIVE_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ACTIVE_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ASSOCIATE_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ASSOCIATE_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE_OF_DEATH");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATE_OF_DEATH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DECEASED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DECEASED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EX_RELATIONSHIP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EX_RELATIONSHIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_BIRTH_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_BIRTH_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CARDNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CARDNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CODE_CLIENT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CODE_CLIENT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_COMCOD");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_COMCOD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CUAS01");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CUAS01"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CUAS02");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CUAS02"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CUAS11");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CUAS11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_CUAS21");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_CUAS21"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_FILLER");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_FILLER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_FNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_FNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_FNAME_L3_27");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_FNAME_L3_27"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_FNAME_L3_3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_FNAME_L3_3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_FULLNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_FULLNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_IDNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_IDNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_LNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_LNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_LNAME_L3_27");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_LNAME_L3_27"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_LNAME_L3_3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_LNAME_L3_3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_LUP_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_LUP_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_NEW");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_NEW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_OP_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_OP_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_P_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_P_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_RCVCOM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_RCVCOM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_RCV_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_RCV_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_RELATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_RELATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_REMARK");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_REMARK"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_RESULT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_RESULT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_SEX");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_SEX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MI_TITLE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MI_TITLE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OCCUPATION_TITLE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OCCUPATION_TITLE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RELATIONSHIP_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RELATIONSHIP_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REMARK_RELATIONSHIP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "REMARK_RELATIONSHIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TO_DATE_WORK");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TO_DATE_WORK"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
