/**
 * SearchAgentChannelByAgentAndPlanPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan;

public interface SearchAgentChannelByAgentAndPlanPortService extends javax.xml.rpc.Service {
    public java.lang.String getSearchAgentChannelByAgentAndPlanPortSoap11Address();

    public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort getSearchAgentChannelByAgentAndPlanPortSoap11() throws javax.xml.rpc.ServiceException;

    public com.mtl.collector.ws.codegen.searchagentchannelbyagentandplan.SearchAgentChannelByAgentAndPlanPort getSearchAgentChannelByAgentAndPlanPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
