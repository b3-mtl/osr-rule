/**
 * SearchAgentDetailByAgentCodePort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mtl.collector.ws.codegen.searchagentdetailbyagentcode;

public interface SearchAgentDetailByAgentCodePort extends java.rmi.Remote {
    public com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeResponse searchAgentDetailByAgentCode(com.mtl.collector.ws.codegen.searchagentdetailbyagentcode.SearchAgentDetailByAgentCodeRequest searchAgentDetailByAgentCodeRequest) throws java.rmi.RemoteException;
}
