package com.mtl.collector.service.rule16;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanPermission;

@Service
public class PlanPermissionService {
	@Autowired
	private ApplicationCache applicationCache;

	public PlanPermission getPlanPermissionByPlanCode(String planCode) {
		HashMap<String, PlanPermission> planPermission = applicationCache.getPlanPermissionAppCashMap();
		for (Map.Entry<String, PlanPermission> mapObject : planPermission.entrySet()) {
			if (mapObject.getKey().indexOf(planCode) >= 0) {
				return mapObject.getValue();
			}
		}
		return null;
	}
}
