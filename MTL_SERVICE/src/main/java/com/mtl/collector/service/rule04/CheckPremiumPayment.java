package com.mtl.collector.service.rule04;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.MsPremiumPayment;
import com.mtl.rule.model.DT04PremiumPaymentSpecial;

@Service
public class CheckPremiumPayment {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * @param agentChannel
	 * @param premium
	 * 
	 * Check by planCode and agentChannel and premiumPayment
	 * find key in cache 
	 * 
	 * @return (true or false)
	 */
	public boolean checkPremiumPayment(String planCode,String agentChannel,String premium) {
		String key = "";
		key += planCode+"|"+agentChannel;
		List<String> premiumPayment = applicationCache.getMsPremiumPayment().get(key);
		
		if(premiumPayment != null && premiumPayment.size() > 0) {
			for(String temp:premiumPayment) {
				if(temp.equals(premium)) {
					return true;
				}else {
					continue;
				}
			}
			return false;
		}else {
			return false;
		}
	}
	
	/**
	 * @param planCode
	 * @param premium
	 * 
	 * Check by planCode and premiumPayment
	 * find key in cache 
	 * 
	 * @return (true or false)
	 */
	public boolean checkPremiumPaymentSpecial(String planCode,String premium) {
		int condition = 0;
		String key = "";
		key += planCode+"|"+premium;
		DT04PremiumPaymentSpecial premiumPayment = applicationCache.rule04_dt04PremiumPaymentSpecial.get(key);
		if(premiumPayment != null) {
			condition = 1;
			return true;
		}else {
			condition = 2;
			return false;
		}
//		return condition;
	}

}
