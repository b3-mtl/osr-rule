package com.mtl.collector.service.rule03;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPG;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPT;
import com.mtl.rule.model.DT03OPDSum;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
public class CheckDiagnosis {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public List<DT03DiagnosisSumInsuredPT> checkDiagnosisPlanType(String planType, String age) {
		
		
		List<DT03DiagnosisSumInsuredPT> valueList = applicationCache.getRule03_dt03DiagnosisPT().get(planType);
		List<DT03DiagnosisSumInsuredPT> res = new ArrayList<DT03DiagnosisSumInsuredPT>();
		if(valueList != null) {
			for(DT03DiagnosisSumInsuredPT item:valueList) {
				boolean checkAge = false;
				if(RuleConstants.OTHERWISE.equals(item.getMinAge()) && RuleConstants.OTHERWISE.equals(item.getMaxAge())) {
					checkAge = true;
				}else {
					checkAge = RuleUtils.betweenNumber(Long.valueOf(age), Long.valueOf(item.getMinAge()), Long.valueOf(item.getMaxAge()));
				}
				if(checkAge) {
					res.add(item);
				}
			}
		}
		return res;
		
//		return applicationCache.getRule03_dt03DiagnosisPT().get(planType);
		
		
	}
	
	public List<DT03DiagnosisSumInsuredPG> checkDiagnosisPlanGroup(String planGroup, String age) {
		List<DT03DiagnosisSumInsuredPG> valueList = applicationCache.getRule03_dt03DiagnosisPG().get(planGroup);
		List<DT03DiagnosisSumInsuredPG> res = new ArrayList<DT03DiagnosisSumInsuredPG>();
		if(valueList != null) {
			for(DT03DiagnosisSumInsuredPG item:valueList) {
				boolean checkAge = false;
				if(RuleConstants.OTHERWISE.equals(item.getMinAge()) && RuleConstants.OTHERWISE.equals(item.getMaxAge())) {
					checkAge = true;
				}else {
					checkAge = RuleUtils.betweenNumber(Long.valueOf(age), Long.valueOf(item.getMinAge()), Long.valueOf(item.getMaxAge()));
				}
				if(checkAge) {
					res.add(item);
				}
			}
		}
		return res;
	}

}
