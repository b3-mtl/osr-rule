package com.mtl.collector.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.MappingClientLocationCode;
import com.mtl.rule.repository.jpa.MappingClientLocationCodeRepository;
import com.mtl.rule.util.RuleConstants;

@Service
public class GetClientLocationService {
	
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * Use JPA
	 * Find by provinceCode in Database
	 */
	public String getClientLocal(String provinceCode) {
		MappingClientLocationCode location = applicationCache.getMappingClientLocationCode(provinceCode.trim()); 
		if(location != null) {
			return location.getClientLocationCode().toString();
		}else {
			location = applicationCache.getMappingClientLocationCode(RuleConstants.OTHERWISE);
			return location.getClientLocationCode().toString();
		}
	}
}
