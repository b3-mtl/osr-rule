package com.mtl.collector.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;
import com.mtl.rule.model.DT03ACCSum;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPG;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
public class CheckAccident {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}
	
	public String[] getPlanTypeFormPlanCode(String key) {
		List<String> valueList = applicationCache.getMsParameter().get(key);
		Set<String> planTypeList = new HashSet<String>();
		if(valueList!=null) {
			for(String temp:valueList) {
				PlanHeader phdr = applicationCache.getPlanHeaderAppCashMap().get(temp);
				if(phdr != null) {
					planTypeList.add(phdr.getPlanType());
				}
			}
		}
		
		String[] planType = new String[planTypeList.size()];
		planTypeList.toArray(planType);
		
		
		return planType;
	}



}
