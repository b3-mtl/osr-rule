package com.mtl.collector.service.rule06;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.model.Dt06Uwqag;

@Service
public class GetMedicalLimitType {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * @param channelCode
	 * @param agentQualification
	 * 
	 * Find medical_type in applicationCache on Case
	 * Case 1 by planCode , channelCode , agentQualification
	 * Case 2 by planCode , channelCode 
	 * Case 3 by planCode , agentQualification
	 * Case 4 by planCode , "Otherwise"
	 * 
	 * @return object Dt06Uwqag
	 */
	public List<Dt06Uwqag> getMedical(String planCode , String channelCode , String agentQualification){
		String key = planCode+"|"+channelCode+"|"+agentQualification;
		List<Dt06Uwqag> uwqag = applicationCache.getRule06Dt06UwqagMap(key);
		if(uwqag == null) {
			key = planCode+"|"+channelCode+"|";
			uwqag = applicationCache.getRule06Dt06UwqagMap(key);
			
			if(uwqag == null) {
				key = planCode+"|"+"|"+agentQualification;
				uwqag = applicationCache.getRule06Dt06UwqagMap(key);
				
				if(uwqag == null) {
					key = planCode+"|"+"|Otherwise";
					uwqag = applicationCache.getRule06Dt06UwqagMap(key);
					
					if(uwqag == null) {
						key = planCode+"|"+"|";
						uwqag = applicationCache.getRule06Dt06UwqagMap(key);
					}
				}
			}
		}
		return uwqag;
	}
	
	public boolean checkMedicalTypN(String planCode , String channelCode , String agentQualification){
		boolean isTypeN =false;
		String key = planCode+"|"+channelCode+"|"+agentQualification;
		List<Dt06Uwqag> uwqag = applicationCache.getRule06Dt06UwqagMap(key);
		if(uwqag == null) {
			key = planCode+"|"+channelCode+"|";
			uwqag = applicationCache.getRule06Dt06UwqagMap(key);
			if(uwqag == null) {
				key = planCode+"|"+"|";
				uwqag = applicationCache.getRule06Dt06UwqagMap(key);
				if(uwqag == null) {
					key = planCode+"|"+"|"+RuleConstants.OTHERWISE;
					uwqag = applicationCache.getRule06Dt06UwqagMap(key);
				}
			}
		}
		if(uwqag!=null) { 
			if(RuleConstants.NO.equals(uwqag.get(0).getMedicalLimitType())) {
				isTypeN= true;
			}
		} 
		
		return isTypeN;
	}
}
