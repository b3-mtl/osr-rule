package com.mtl.collector.service.rule06;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.DT06UWRQ;
import com.mtl.rule.repository.Dt06MapMedicalDiseaseDao;
import com.mtl.rule.repository.Dt06UwrqDao;

@Service
public class GetDocumenService {
	
	@Autowired
	private Dt06UwrqDao dt06UwrqDao;
	
	@Autowired
	private Dt06MapMedicalDiseaseDao mapMedicalDiseaseDao;
	
	/**
	 * 
	 * @param clientLocal
	 * @param agenQua
	 * @param age
	 * @param sumInsure
	 * @param sex
	 * 
	 * Find object DT06UWRQ
	 * 
	 * @return
	 */
	public DT06UWRQ getDocDetail(String clientLocal, String agenQua,String age, BigDecimal sumInsure, String sex) {
        System.out.println(" ### Luckup table UWRQ [clientLocal:"+clientLocal+",agentQualify:"+agenQua+", age:"+age+", sumInsure:"+sumInsure+", sex:"+sex);
		return dt06UwrqDao.findResult(clientLocal,agenQua,age, sumInsure, sex);
	}
	
	/**
	 * @param resultCode
	 * 
	 * Split resultCode to length 2
	 * Find document by charList
	 * 
	 * @return
	 */
	public List<DT06MapMedicalDisease> getDocument(String resultCode){
		 System.out.println(" ### Rule6 getDocument by Code:"+resultCode); 
		List<String> charList = new ArrayList<String>();
		if(StringUtils.isNotBlank(resultCode)) {
			int length = resultCode.length() / 2 ;
			String charector = "";
			for(int i = 0 ; i< length ; i++) {
				charector = resultCode.substring((i*2), (i*2)+2);
				charList.add(charector);
			}
			
			List<DT06MapMedicalDisease> docList = mapMedicalDiseaseDao.findDocument(charList);
			
			return docList;
		}else {
			return null;
		}
	}
	
}
