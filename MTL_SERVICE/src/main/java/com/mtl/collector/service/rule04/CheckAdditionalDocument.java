package com.mtl.collector.service.rule04;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT04AppMdc;

@Service
public class CheckAdditionalDocument {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	
	/**
	 * @param planCode
	 * @param sumInsured
	 * @param premiumPayment
	 * 
	 * find in cache
	 * case 1 -> find by key = planCode,sumInsured,premiumPayment
	 * case 2 -> find by key = planCode
	 * case 3 -> find by key = "Otherwise",premiumPayment
	 * 
	 * @return object DT04AppMdc
	 */
	public DT04AppMdc ListAdditionalDoc(String planCode ,Long sumInsured ,String premiumPayment) {
		String key = "";
		key = planCode+"|"+premiumPayment;
		DT04AppMdc doc = applicationCache.getRule04_dt04AppMdc().get(key);
		if(doc == null ) {
			key = planCode+"|";
			doc = applicationCache.getRule04_dt04AppMdc().get(key);
			
			if(doc == null ) {
				key = "Otherwise|"+premiumPayment;
				doc = applicationCache.getRule04_dt04AppMdc().get(key);
			}
			
		}
		if(doc != null) {
			boolean next = false ;
			if(doc.getSumInsuredBasic() !=null) {
				next = doc.getSumInsuredBasic().compareTo(sumInsured) <= 0;
				if(next) {
					return doc;
				}else {
					return null;
				}
			}else {
				return doc;
			}
		}else {
			return doc;
		}
	}
}
