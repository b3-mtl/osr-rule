package com.mtl.collector.service.rule16;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.rule.model.DT16MibHighRisk;
import com.mtl.rule.repository.DT16MibHighRiskDao;

@Service
public class CheckMibHighRisk {

	@Autowired
	private DT16MibHighRiskDao dt16ClaimHistoryDao;
	
	public List<DT16MibHighRisk> checkMibHighRiskList(List<String> mibHighRiskList) {
		List<DT16MibHighRisk> list = dt16ClaimHistoryDao.findMibHighRisk(mibHighRiskList);
		return list;
	}
}
