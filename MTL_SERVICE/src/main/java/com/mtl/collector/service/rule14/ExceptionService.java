package com.mtl.collector.service.rule14;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.AppNo;
import com.mtl.rule.model.DT14Exception;
import com.mtl.rule.model.DT14ExceptionAppNo;
import com.mtl.rule.util.RuleConstants;

@Service
public class ExceptionService {
	
	@Autowired
	private ApplicationCache applicationCache;

	/**
	 * @param planCode
	 * @param plantype
	 * @param channelCode
	 * 
	 * Check exception
	 * Case 1 Found in dt14ExceptionDao
	 * Case 2 planType =="AA" or "AB"
	 * Case 3 Check agentChannelCode =="25" or "28"
	 * 
	 * @return
	 */
	public boolean getException(String planCode , String plantype, String channelCode){
		boolean exceptionResult = false;
		DT14Exception exList = applicationCache.getRule14Exception(planCode);
		if(exList != null) {
			exceptionResult= true;
		}
		
		if(checkPlanTypeException(plantype)) {
			exceptionResult= true;
		}
		
		if(checkAgentChannelCodeException(channelCode)) {
			exceptionResult= true;
		}
		 
		return exceptionResult;
	}
	
	/**
	 * @param planType
	 * 
	 * Check planType =="AA" or "AB"
	 * 
	 * @return
	 */
	public boolean checkPlanTypeException(String planType){
		if(planType.equals("AA") || planType.equals("AB")) {
			
			return true ;
		}else {
			
			return false ;
		}
	}
	
	/**
	 * @param agentChannelCode
	 * 
	 * Check agentChannelCode =="25" or "28"
	 * 
	 * @return
	 */
	public boolean checkAgentChannelCodeException(String agentChannelCode){
		if("25".equals(agentChannelCode) || "28".equals(agentChannelCode)) {
			
			return true ;
		}else {
			
			return false ;
		}
	}
	
	/**
	 * @param planType
	 * @param agentChannelCode
	 * 
	 * Check except planType & agentChannelCode
	 * 
	 * @return
	 */
	public boolean checkPlanAndChannelException(String planType,String agentChannelCode){
		
		if(checkPlanTypeException(planType)&&checkAgentChannelCodeException(agentChannelCode)) {
			return true;
		}else {
			return false;
		}
		
	}
	
	/**
	 * @param appNoKey
	 * 
	 * Check found appNo in MS_APP_NO
	 * 
	 * @return
	 */
	public boolean checkAppNoException(String appNoKey){
		
		AppNo appNo = applicationCache.getAppNoAppCashMap(appNoKey);
		if(appNo!=null) {
			DT14ExceptionAppNo obj = applicationCache.getRule14ExceptionAppNo().get(appNoKey);
			if(obj!=null) {
				if(RuleConstants.YES.equals(obj.getIsExcept())){
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}else {
			return false;
		}
		
	}

	/**
	 * @param planCode
	 * 
	 * Check planCode in (WON05D,WON05E,WON10D,WON10E)
	 * 
	 * @return
	 */
	public boolean isNHW(String planCode){
		if(planCode.equals("WON05D") || planCode.equals("WON05E") || planCode.equals("WON10D") || planCode.equals("WON10E") ) {
 
			return true;
		}else {
			return false;
		}
		
	}
	
	/**
	 * @param planType
	 * 
	 * Check planType in (AA,AB,A1,A2,A3)
	 * 
	 * @return
	 */
	public boolean isPlanTypeACCIDENT(String planType){
		if(planType.equals("AA") || planType.equals("AB") || planType.equals("A1") || planType.equals("A2") || planType.equals("A3") ) {
 
			return true;
		}else {
			return false;
		}
		
	}
	
	
	
	
}
