package com.mtl.collector.service.rule04;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;

@Service
public class ExceptPlanService {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * find by planCode 
	 * found -> true
	 * not found -> false
	 * 
	 * @return boolean
	 */
	public Boolean findPlanCode(String planCode) {
		HashMap<String, String> cacheData = applicationCache.getRule04_dt04ExceptPlanCode();
		if(cacheData.containsKey(planCode)) {
			return true;
		}else {
			return false;
		}
	}
}
