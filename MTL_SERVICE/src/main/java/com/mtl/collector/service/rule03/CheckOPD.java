package com.mtl.collector.service.rule03;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT03OPDSum;

@Service
public class CheckOPD {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public DT03OPDSum checkOPDSumInsured(String channel, String system) {
		String key = channel + "|" + system;
		DT03OPDSum valueList = applicationCache.getRule03_dt03OPDSum().get(key);

		if (valueList == null) {
			key = "Otherwise|OtherChannel";
			valueList = applicationCache.getRule03_dt03OPDSum().get(key);
			if (valueList == null) {
				return null;
			} else {
				return valueList;
			}
		} else {
			return valueList;
		}

	}

}
