package com.mtl.collector.service.rule14;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT14DocumentClaim;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
public class ClaimService {
	

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkAppTypes(String appType) {
		String key =RuleConstants.RULE_14.APP_TYPE+"|"+appType;
		List<DT14DocumentClaim> ls = applicationCache.getRule14ExceptionClaim(key);
		if(ls != null && ls.size() > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean checkPlanCode(String planCode) {
		String key =RuleConstants.RULE_14.PLAN_CODE+"|"+planCode;
		List<DT14DocumentClaim> ls = applicationCache.getRule14ExceptionClaim(key);
		if(ls != null && ls.size() > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public DT14DocumentClaim findByGroupChannelCodeAndSumInsured(String groupChannelCode,BigDecimal sumInsured) {
		String key =RuleConstants.RULE_14.GROUP_CHANNEL+"|"+groupChannelCode;
		List<DT14DocumentClaim> ls = applicationCache.getRule14ExceptionClaim(key);
		boolean checkSum;
		BigDecimal min = BigDecimal.ZERO;
		BigDecimal max = BigDecimal.ZERO;
		
		for(DT14DocumentClaim temp:ls) {
			checkSum = false;
			min = new BigDecimal(temp.getSumInsuredMin());
			max = new BigDecimal(temp.getSumInsuredMax());
			checkSum = RuleUtils.betweenNumber(sumInsured, min, max);
			if(checkSum) {
				return temp;
			}
		}
		return null;
		
	}
}
	
