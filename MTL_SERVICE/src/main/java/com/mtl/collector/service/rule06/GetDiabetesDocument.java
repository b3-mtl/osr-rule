package com.mtl.collector.service.rule06;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.rule.model.DT06DiabetesDocument;
import com.mtl.rule.repository.Dt06DiabetesDocumentDao;

@Service
public class GetDiabetesDocument {

	@Autowired
	private Dt06DiabetesDocumentDao dt06DiabetesDocumentDao;
	
	/**
	 * @param sumInsured
	 * @param age
	 * @param planType
	 * 
	 * Case 1 Find by planType & sumInsured : if==null -> Go to Case 2
	 * Case 2 Find by planType & Age
	 * 
	 * @return List<DT06DiabetesDocument>
	 */
	public List<DT06DiabetesDocument> getOtherDocument(BigDecimal sumInsured ,String age, String planType){
		List<DT06DiabetesDocument> docList = new ArrayList<DT06DiabetesDocument>();
		if(StringUtils.isNoneBlank(planType)) {
			
			docList = dt06DiabetesDocumentDao.findByPlanTypeAndSumInsured(planType,sumInsured);
			if(docList == null || docList.size() == 0) {
				
				docList = dt06DiabetesDocumentDao.findByPlanTypeAndAge(planType,age);
			}
		}
		return docList;
		
	}
}
