package com.mtl.collector.service.rule03;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;
import com.mtl.rule.model.DT03SumByPlan;

@Service
public class CheckSumByPlan {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public String checkBetweenSumInsured(PlanHeader plan,DT03SumByPlan item, BigDecimal sum) {
		String planTypeSum = ","+item.getPlanType()+",";
		String planTypeHdr = ","+plan.getPlanType()+",";
		if(planTypeSum.contains(planTypeHdr)) {
			
			boolean checkMin = false;
			boolean checkMax = false;
			if(item.getMinSuminsured() == null) {
				checkMin = true;
			}else {
				BigDecimal tempMin = new BigDecimal(item.getMinSuminsured());
				checkMin = tempMin.compareTo(sum) > 0;
			}
			
			if(item.getMaxSuminsured() == null) {
				checkMax = true;
			}else {
				BigDecimal tempMax = new BigDecimal(item.getMaxSuminsured());
				checkMax = tempMax.compareTo(sum) < 0;
			}
			
			
			if(checkMin || checkMax) {
				return item.getMessageCode();
			}else {
				return "";
			}
		}else {
			return "";
		}
	}
	
	
	public DT03SumByPlan checkBetweenSumInsuredNew(PlanHeader plan,DT03SumByPlan item, BigDecimal sum) {
		String planTypeSum = ""+item.getPlanType()+"";
		String planTypeHdr = ""+plan.getPlanType()+"";
		if(planTypeSum.contains(planTypeHdr)) {
			
			boolean checkMin = false;
			boolean checkMax = false;
			if(item.getMinSuminsured() != null) {
				BigDecimal tempMin = new BigDecimal(item.getMinSuminsured());
				checkMin = tempMin.compareTo(sum) > 0;
			}
			
			if(item.getMaxSuminsured() != null) {
				BigDecimal tempMax = new BigDecimal(item.getMaxSuminsured());
				checkMax = tempMax.compareTo(sum) < 0;
			}
			
			
			if(checkMin || checkMax) {
				return item;
				
			}else {
				return null;

			}
		}else {
			return null;
		}
	}

}
