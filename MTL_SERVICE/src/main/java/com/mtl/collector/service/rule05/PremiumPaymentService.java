package com.mtl.collector.service.rule05;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.MsPremiumPayment;

@Service
public class PremiumPaymentService {
	 
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * @param channel
	 * 
	 * find in cache MS_PREMIUM_PAYMENT
	 * find by key = planCode,channel
	 * found -> return true
	 * not found -> return false
	 * 
	 * @return
	 */
	public boolean isInPremiumPaymentChannel(String planCode,String channel) {	
		
		String key ="";
		key = planCode+"|"+channel;
		if(applicationCache.getRule05_premiumPayment().containsKey(key)) {
			return true;
		}else {
			return false;	
		}
	}
	 
}
