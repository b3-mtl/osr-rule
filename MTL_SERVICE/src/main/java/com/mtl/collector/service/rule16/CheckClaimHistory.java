package com.mtl.collector.service.rule16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule16ClaimHistoryDao;
import com.mtl.rule.model.ClaimHistoryList;
import com.mtl.rule.model.DT16ClaimHistory;


@Service
public class CheckClaimHistory {

	@Autowired
	private ApplicationCache applicationCache;
	

	public ClaimHistoryList checkClaimHistoryCodeList(String[] claimCodeList) {
		List<DT16ClaimHistory> inDb = new ArrayList<DT16ClaimHistory>();
		List<String> notIndb = new ArrayList<String>();
		ClaimHistoryList claimList = new ClaimHistoryList();
		HashMap<String, DT16ClaimHistory> dt16ClaimHistoryMap = applicationCache.getClaimHistoryMap();
		for (String code : claimCodeList) {
			DT16ClaimHistory object = dt16ClaimHistoryMap.get(code);
			if (object != null)
			{
				inDb.add(object);
			}
			else
			{
				notIndb.add(code);
			}
		}
		claimList.setInDb(inDb);
		claimList.setNotIndb(notIndb);
		
		return claimList;
	}
	
}
