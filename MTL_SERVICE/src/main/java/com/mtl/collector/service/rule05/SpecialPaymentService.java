package com.mtl.collector.service.rule05;

import java.util.HashMap;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;

@Service
public class SpecialPaymentService {
	 
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * @param channel
	 * 
	 * find in cache DT05_SPECIAL_PAYMENT
	 * find by key = planCode,channel
	 * found -> return 1
	 * not found -> return 2
	 * 
	 * @return
	 */
	public int isInSpecialPaymentChannel(String planCode,String channel) {	
		
		int returnVal = 1; 
		HashMap< String , Set<String>> specialPaymentMap =applicationCache.getRule05_specialPaymentChannelMap();
		
		Set<String> notInStringSet  = specialPaymentMap.get(planCode);
		
		if(notInStringSet!=null) {
			boolean isContain =notInStringSet.contains(channel);
			if(isContain) {			
				returnVal = 1;
			}else {
				returnVal = 2;
			}
		}else {
			returnVal = 3;
		}

		return returnVal;	
	}
}
