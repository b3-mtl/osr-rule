package com.mtl.collector.service;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.LoftDao;
import com.mtl.collector.model.ClientProfile;
import com.mtl.collector.service.rule06.GetMapNmlTypeService;
import com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.CoverageLists;
import com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response;
import com.mtl.collector.ws.codegen.GetDataColPolicyCoverage.Response.PolicyList;
import com.mtl.collector.ws.service.GetSearchClientProfileByIdCardNoWsClientService;
import com.mtl.collector.ws.service.PolicyCoverageWSService;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.Loft;
import com.mtl.rule.model.MapProject;
import com.mtl.rule.util.RuleUtils;

@Service
public class GetCollectorHistoryInsure {
	@Value("${webservice.init.enable}")
	private String initFlag;
 	
	@Autowired
	private PolicyCoverageWSService policyCoverageWSService;
	
	@Autowired
	private GetSearchClientProfileByIdCardNoWsClientService getSearchClientProfileByIdCardNoWsClientService;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private GetMapNmlTypeService getMapNmlTypeService;
	
	@Autowired
	private LoftDao loftDao;

	public BigDecimal getHistoryInsure(String clientNo) {
		// TODO connect collector history around year

		//Response mibResponse = etDataColPolicyCoverageWSService.getGetDataColPolicyCoverage(clientNo);
		//if(!"N".equals(initFlag)) {
			Response mibResponse = policyCoverageWSService.getPolicy(clientNo);
			
			BigDecimal maxSumInsured = BigDecimal.ZERO;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -1);
			Date thisDate = cal.getTime();
			
			for (PolicyList policy : mibResponse.getPolicyList()) {
				for (CoverageLists coverage : policy.getCoverageList()) {
					if(thisDate.after(convertDateToXMLGregorianCalender(coverage.getIssueDate()))) {
						if(coverage.getSumInsured().compareTo(maxSumInsured) == 1) {
							maxSumInsured = coverage.getSumInsured();
						}
					}
				}
			}
			

			return maxSumInsured;
		//}else {
		//	return BigDecimal.ZERO;
		//}
	}
	
	public List<HistoryInsure> getHistoryInsureList(String idCard) {
		// TODO connect collector history around year
		List<HistoryInsure> list = new ArrayList<HistoryInsure>();
		HistoryInsure temp = null;
		ClientProfile tempClientProfile = null;
		
		try {
			tempClientProfile = getSearchClientProfileByIdCardNoWsClientService.searchClientProfilebyIDCardNo(idCard);
		} catch (RemoteException | ServiceException e1) {
			e1.printStackTrace();
		}
		if(tempClientProfile == null || StringUtils.isAllBlank(tempClientProfile.getCodeClient())) {

		}else {
			
			
			Response mibResponse = policyCoverageWSService.getPolicy(tempClientProfile.getCodeClient());
			
			for (PolicyList policy : mibResponse.getPolicyList()) {
				
				if(policy.getRelationship().equals("I")) {
					
					for (CoverageLists coverage : policy.getCoverageList()) {
						System.out.println("PlanCode Coverage ::::::::::::::::::"+coverage.getPlanCode());
						String planCode = coverage.getPlanCode().trim();
//						if( "RWP  M".contains(planCode) || "WPRD01".contains(planCode) || "1".contains(planCode) ){
//							continue;
//						}
						PlanHeader planHdr = applicationCache.getPlanHeaderAppCashMap().get(planCode);
						if(planHdr == null) {
							continue;
						}
						
						String ncType = planHdr.getNcType();
						String branch = policy.getServicingBranch();
						String channelCode = policy.getChannelCode();
						
						String docType = getMapNmlTypeService.findDocType( ncType, branch);
						
						temp = new HistoryInsure();
						temp.setDocumentType(docType);

						temp.setPlanCode(coverage.getPlanCode());
						temp.setPlanGroupType(planHdr.getPlanGroupType());
						Date date = toDate( coverage.getIssueDate());
						temp.setIssueDate(date);
						temp.setSumInsure(coverage.getSumInsured());
						temp.setFaceAmount(coverage.getFaceAmount());
						temp.setStatus(coverage.getCoverageStatus());
						
						temp.setServiceBrach(policy.getServicingBranch());
						temp.setWritingAgent(policy.getWritingAgent());
						temp.setChannelCode(policy.getChannelCode());
						temp.setPolicyNumber(policy.getPolicyNumber());		
						
					

						
						list.add(temp);
					}
				}
			}
			

		}
		
		List<Loft> loftList = loftDao.findLoftByIdCard(idCard);
		if(loftList!=null){
			for(Loft tempLoft: loftList) {
				String planCode = tempLoft.getPlanCode().trim();
				if( "RWP  M".contains(planCode) || "WPRD01".contains(planCode) || "1".contains(planCode) ){
					continue;
				}
				PlanHeader planHdr = applicationCache.getPlanHeaderAppCashMap().get(tempLoft.getPlanCode().trim());
				if(planHdr==null){
					continue;
				}
//				GregorianCalendar c = new GregorianCalendar();
//				c.setTime(tempLoft.getSubmitDate());
//				XMLGregorianCalendar date = null;
//				try {
//					date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
//				} catch (DatatypeConfigurationException e) {
//					e.printStackTrace();
//				}
				
				temp = new HistoryInsure();
				temp.setDocumentType(tempLoft.getCardType());
				temp.setPlanCode(tempLoft.getPlanCode().trim());
				
//				System.out.print(tempLoft.getPlanCode().trim()+"   ");
//				System.out.print(" | "+planHdr.getPlanCode());
//				System.out.println();
				temp.setPlanGroupType(planHdr.getPlanGroupType());
				
				
				temp.setIssueDate(tempLoft.getSubmitDate());
				temp.setSumInsure(new BigDecimal(tempLoft.getSumInsured()));
				temp.setFaceAmount(new BigDecimal(tempLoft.getSumInsured()));
				temp.setStatus(tempLoft.getAppStatus());
				list.add(temp);
				
				
			}
		}
		
		
		return list;
	}
	
	public static final Date convertDateToXMLGregorianCalender(XMLGregorianCalendar calendar  ){
        return calendar.toGregorianCalendar().getTime();
        
    }
	
	public List<HistoryInsure> getAllHistoryInsure(String idCard) {
		List<HistoryInsure> list = new ArrayList<HistoryInsure>();
		HistoryInsure temp = null;
		ClientProfile tempClientProfile = null;
		
		try {
			tempClientProfile = getSearchClientProfileByIdCardNoWsClientService.searchClientProfilebyIDCardNo(idCard);
		} catch (RemoteException | ServiceException e) {
			e.printStackTrace();
		}
		if(tempClientProfile != null ){
			Response mibResponse = policyCoverageWSService.getPolicy(tempClientProfile.getCodeClient());
			PlanHeader planHdr = null;
			for (PolicyList policy : mibResponse.getPolicyList()) {
				
				for (CoverageLists coverage : policy.getCoverageList()) {
					System.out.println("planCode ="+coverage.getPlanCode().trim());
					planHdr = applicationCache.getPlanHeaderAppCashMap().get(coverage.getPlanCode().trim());
					temp = new HistoryInsure();
					temp.setPlanCode(coverage.getPlanCode());
					temp.setSumInsure( coverage.getSumInsured());
					if(planHdr != null && StringUtils.isNotBlank(planHdr.getPlanType())){
						temp.setPlanType(planHdr.getPlanType());
						temp.setSystemName(planHdr.getPlanSystem());
					}else {
						temp.setPlanType("");
						temp.setSystemName("");
					}
					
					
					temp.setIssueDate(toDate(coverage.getIssueDate()));
					temp.setStatus(coverage.getCoverageStatus());
					list.add(temp);
				}
			}
		}
		
		return list;
	}
	public static Date toDate(XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }


}
