package com.mtl.collector.service.rule13;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanPermission;
import com.mtl.rule.model.DT13AmloPilot;
import com.mtl.rule.util.RuleConstants;


@Service
public class GetType {


	@Autowired
	private ApplicationCache applicationCache;
	

	public boolean getPlanPermission(String planCode) {
		
		String key = planCode+"|";
		PlanPermission exhiv = applicationCache.getPlanPermissionAppCashMap().get(key);
		
		if(exhiv == null ) {
			return false;
		}else if(exhiv.getExceptHIV() == RuleConstants.TRUE){
			return true;
		}else if(exhiv.getExceptHIV() == RuleConstants.FALSE) {
			return false;
		}else {
			return false;
		}
				
	}
	
	
	public DT13AmloPilot getAmloPilot(String occupationCode) {
//		DT13AmloPilot exAmloPilot  = dt13AmloPilotDao.findAmloPilot(occupationCode);
		String key = occupationCode;
		DT13AmloPilot exAmloPilot = applicationCache.getRule13AmloPilot(key);
		
		if(exAmloPilot == null) {
//			exAmloPilot.setOccupationCode("false");
		}
		
		return applicationCache.getRule13AmloPilot(key);
	}
	
}