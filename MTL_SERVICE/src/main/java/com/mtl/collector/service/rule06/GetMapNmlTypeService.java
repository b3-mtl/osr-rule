package com.mtl.collector.service.rule06;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.Dt06MapNmlType;
import com.mtl.rule.util.RuleConstants;

@Service
public class GetMapNmlTypeService {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	public String findDocType(String ncType, String branch) {
		System.out.println(ncType);
		System.out.println(branch);
//		if(ncType == null) {
//			return "CP";
//		}
		Dt06MapNmlType defaultCP = applicationCache.getDt06MapNmlType(ncType+"|"+branch);
		if(defaultCP == null) {
			defaultCP = applicationCache.getDt06MapNmlType(ncType+"|");
			if(defaultCP == null) {
				defaultCP = applicationCache.getDt06MapNmlType(RuleConstants.NA+"|");
			}
		}
		return defaultCP.getDocumentType();
	}
	
}