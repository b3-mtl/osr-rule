package com.mtl.collector.service.rule06;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.Dt06MapNmlType;
import com.mtl.rule.util.RuleConstants;

@Service
public class GetNcTypeService {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	public String getNc(String ncType) {
		 Dt06MapNmlType val = applicationCache.getDt06MapNmlType(ncType);
		if(val == null) {
			val = applicationCache.getDt06MapNmlType(RuleConstants.NA);
			return val.getDocumentType();
		}else {
			return val.getDocumentType();
		}
		
	}
}
