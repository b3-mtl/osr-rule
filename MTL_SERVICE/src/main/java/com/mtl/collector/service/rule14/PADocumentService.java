package com.mtl.collector.service.rule14;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT14PADocument;
import com.mtl.rule.util.RuleUtils;

@Service
public class PADocumentService {
	
	@Autowired
	private ApplicationCache applicationCache;

	
	/**
	 * @param sumInsured
	 * 
	 * Find PADocument in Database by sumInsured
	 * 
	 * @return
	 */
	public DT14PADocument getPADocument(BigDecimal sumInsured) {
		List<DT14PADocument> ls = applicationCache.getPADocument("PA_DOCUMENT");
		boolean checkSum;
		BigDecimal min = BigDecimal.ZERO;
		BigDecimal max = BigDecimal.ZERO;
		for(DT14PADocument temp:ls) {
			checkSum = false;
			min = new BigDecimal(temp.getSumInsuredMin());
			max = new BigDecimal(temp.getSumInsuredMax());
			checkSum = RuleUtils.betweenNumber(sumInsured, min, max);
			if(checkSum) {
				return temp;
			}
		}
		return null;

	}
	
}
	
