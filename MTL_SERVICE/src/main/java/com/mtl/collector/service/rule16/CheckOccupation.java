package com.mtl.collector.service.rule16;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.rule.model.DT16Occupation;
import com.mtl.rule.repository.DT16OccupationDao;

@Service
public class CheckOccupation {

	@Autowired
	private DT16OccupationDao dt16OccupationDao;
	
	public List<DT16Occupation> checkOccpuationCode(String occupationCode) {
		List<DT16Occupation> list = dt16OccupationDao.findOccupation(occupationCode);
		return list;
	}
}
