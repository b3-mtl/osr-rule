package com.mtl.collector.service.rule14;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT14HnwDocument;
import com.mtl.rule.model.DT14PADocument;
import com.mtl.rule.repository.DT14HnwDocumentDao;
import com.mtl.rule.util.RuleUtils;

@Service
public class HnwService {

	@Autowired
	private ApplicationCache applicationCache;

	/**
	 * 
	 * @param sumInsured
	 * @param age
	 * 
	 * Find HnwDocument in Database by sumInsured , age
	 * 
	 * @return
	 */
	public DT14HnwDocument getHnwDocument(BigDecimal sumInsured,String age) {
		List<DT14HnwDocument> ls = applicationCache.getHnwDocument("HNW_DOCUMENT");
		boolean checkSum,checkAge;
		BigDecimal min = BigDecimal.ZERO;
		BigDecimal max = BigDecimal.ZERO;
		BigDecimal ageBig = BigDecimal.ZERO;
		
		for(DT14HnwDocument temp:ls) {
			checkSum = false;
			checkAge = false;
			min = new BigDecimal(temp.getSumInsuredMin());
			max = new BigDecimal(temp.getSumInsuredMax());
			ageBig = new BigDecimal(age);
			checkSum = RuleUtils.betweenNumber(sumInsured, min, max);
			checkAge = RuleUtils.betweenNumber(ageBig, min, max);
			if(checkSum && checkAge) {
				return temp;
			}
		}
		return null;
	}
	
}
	
