package com.mtl.collector.service.rule16;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.rule.model.DT16MibAmlo;
import com.mtl.rule.repository.DT16MibAmloDao;

@Service
public class CheckMibAmlo {

	@Autowired
	private DT16MibAmloDao dt16MibAmloDao;
	
	public List<DT16MibAmlo> checkMibAmloList(List<String> mibList) {
		List<DT16MibAmlo> list = dt16MibAmloDao.findMibAmlo(mibList);
		return list;
	}
}
