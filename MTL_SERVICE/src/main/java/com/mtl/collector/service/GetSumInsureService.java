package com.mtl.collector.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.rule.util.RuleUtils;

@Service
public class GetSumInsureService {
	
 
	@Autowired
	private ApplicationCache applicationCache;
	
	
	/**
	 * Sum range in 1 year
	 * Calculate by UWRatio in planHeader
	 */
	public BigDecimal getSumHistoryInsureOneYear(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			
			//calculate year before now 1 year
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(new Date());
			calDate.add(Calendar.YEAR, -1);
			Date nowDate = calDate.getTime();
			
			for(HistoryInsure temp:historyList) {
				Date pastDate = temp.getIssueDate();
				if(nowDate.compareTo(pastDate) > 0 || nowDate.compareTo(pastDate) == 0) {
					System.out.println(" HHHHHHHHHHHH PlanCode:"+temp.getPlanCode()); 

					String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
					PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
					System.out.println(" HHHHHHHHHHHH PlanCode:"+planHeader);
					
					BigDecimal ratio = BigDecimal.ONE;
					if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}else{
						ratio = new BigDecimal("1.00");
					}
					historyInsure = ratio.multiply(historyInsure.add(temp.getSumInsure()));
					
				}
			}
		}	
		return historyInsure;
	}
	
	/**
	 * Sum range in 1 year
	 * Compare by docType
	 * Calculate by UWRatio in planHeader
	 */
	public BigDecimal getSumHistoryInsureByDocType(String docType,List<HistoryInsure> historyList){
		System.out.println(" History List:"+historyList);
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			
			//calculate year before now 1 year
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(new Date());
			calDate.add(Calendar.YEAR, -1);
			Date nowDate = calDate.getTime();
			System.out.println(" History   nowDate:"+nowDate );
			for(HistoryInsure temp:historyList) {
				
				Date pastDate = temp.getIssueDate();
				System.out.println(" History   PlanCode:"+temp.getPlanCode()+" , Insuere"+temp.getSumInsure()+" issueDate:"+temp.getIssueDate()+" , DocType:"+temp.getDocumentType());
				
				if(nowDate.compareTo(pastDate) < 0 || nowDate.compareTo(pastDate) == 0) {
					
					if(docType.equals(temp.getDocumentType())) {
						PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode());
						BigDecimal ratio = BigDecimal.ONE;
						if (planHeader != null && planHeader.getUwRatio() != null) {
							System.out.println(" planHeader.getUwRatio():"+planHeader.getUwRatio());
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}else{
							ratio = new BigDecimal("1.00");
						}
						
						BigDecimal tempSumInsure = ratio.multiply(temp.getSumInsure());
						historyInsure = historyInsure.add(tempSumInsure);
						//historyInsure = ratio.multiply(historyInsure.add(temp.getSumInsure()));
						System.out.println(" planHeader.getUwRatio():"+ratio+" tempSumInsure:"+tempSumInsure+" historyInsure:"+historyInsure);
					}else {
						System.out.println(" docType:"+docType+" TempDocType:"+temp.getDocumentType());
					}
				}else {
					System.out.println(" History pastDate Not in rank PlanCode:"+temp.getPlanCode()+" , Insuere"+temp.getSumInsure()+" issueDate:"+temp.getIssueDate());
				}
			}
		}
		
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureByPlanGroup(List<HistoryInsure> historyList,String planGroup){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {

			for(HistoryInsure temp:historyList) {

				String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				System.out.println(" HHHHHHHHHHHH PlanCode:"+planHeader);
				
				if(planHeader.getPlanGroupType().equals(planGroup)) {
					BigDecimal ratio = BigDecimal.ONE;
					if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}else{
						ratio = new BigDecimal("1.00");
					}
					
					historyInsure = ratio.multiply(historyInsure.add(temp.getSumInsure()));
				}	
			}
		}
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureByFindPlanType(List<HistoryInsure> historyList, String key){
		List<String> valueList = applicationCache.getMsParameter().get(key);
		BigDecimal historyInsure = BigDecimal.ZERO;
		for(String planType:valueList) {
			
			if(historyList!=null) {
				for(HistoryInsure temp:historyList) {
					BigDecimal InsureSum = BigDecimal.ZERO; 
					String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
					PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
					
					if(planType.equals(planHeader.getPlanType())) {
						BigDecimal ratio = BigDecimal.ONE;
						if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}else{
							ratio = new BigDecimal("1.00");
						}
						
						if("F".equals(planHeader.getUwRatingKey()) || planHeader.getUwRatingKey() == null){
							InsureSum = temp.getFaceAmount();
						}else{
							InsureSum = temp.getSumInsure();
						}
						if(InsureSum==null){
							InsureSum  = BigDecimal.ZERO;
						}
						historyInsure = historyInsure.add(ratio.multiply(InsureSum));
					}	
				}
			}
		}
		
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureByFindPlanCode(List<InsureDetail> accidentPlan ,List<HistoryInsure> historyList, String key){
		List<String> valueList = applicationCache.getMsParameter().get(key);
		BigDecimal allSuminsure = BigDecimal.ZERO;
		BigDecimal currentInsure = BigDecimal.ZERO;
		BigDecimal historyInsure = BigDecimal.ZERO;
		
		for(String planCode:valueList) {
			for(HistoryInsure temp:historyList) {
				BigDecimal InsureSum = BigDecimal.ZERO; 
				String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				
				if(planCode.contains(planCodeTrim)) {
					BigDecimal ratio = BigDecimal.ONE;
					if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}else{
						ratio = new BigDecimal("1.00");
					}
					if("F".equals(planHeader.getUwRatingKey()) || planHeader.getUwRatingKey() == null){
						InsureSum = temp.getFaceAmount();
					}else{
						InsureSum = temp.getSumInsure();
					}

					if(InsureSum==null){
						InsureSum  = BigDecimal.ZERO;
					}
					historyInsure = historyInsure.add(ratio.multiply(InsureSum));
				}	
			}
			
			for(InsureDetail temp:accidentPlan) {
				String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				BigDecimal insure = new BigDecimal(temp.getInsuredAmount());
				
				if(planCode.contains(planCodeTrim)) {
					BigDecimal ratio = BigDecimal.ONE;
					if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}else{
						ratio = new BigDecimal("1.00");
					}
					currentInsure = currentInsure.add(ratio.multiply(insure));
				}
			}
		}
		allSuminsure = currentInsure.add(historyInsure);
		return allSuminsure;
	}
	
	public BigDecimal getSumHistoryInsureByPlanType(List<HistoryInsure> historyList, String[] valueList){
		BigDecimal historyInsure = BigDecimal.ZERO; 
		for(String planType:valueList) {
			if(historyList !=null) {

				for(HistoryInsure temp:historyList) {
					BigDecimal InsureSum = BigDecimal.ZERO; 
					String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
					PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
					if(planHeader.getPlanType()!=null){
						if(planType.contains(planHeader.getPlanType())) {
							BigDecimal ratio = BigDecimal.ONE;
							if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
								ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
							}else{
								ratio = new BigDecimal("1.00");
							}
							
							if("F".equals(planHeader.getUwRatingKey()) || planHeader.getUwRatingKey() == null){
								InsureSum = temp.getFaceAmount();
							}else{
								InsureSum = temp.getSumInsure();
							}
							if(InsureSum==null){
								InsureSum  = BigDecimal.ZERO;
							}
							historyInsure = historyInsure.add(ratio.multiply(InsureSum));
						}	
					}
				}
			}
		}
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureByPlanType(List<HistoryInsure> historyList, String planType){
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(StringUtils.isNoneBlank(planType)) {
			if(historyList !=null) {

				for(HistoryInsure temp:historyList) {
					BigDecimal InsureSum = BigDecimal.ZERO; 
					String planCodeTrim = temp.getPlanCode()==null?"":temp.getPlanCode().trim();
					PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
					if(planHeader.getPlanType() != null){		
						if(planType.contains(planHeader.getPlanType())) {
							BigDecimal ratio = BigDecimal.ONE;
							if (StringUtils.isNotEmpty(planHeader.getUwRatio())  ) {
								ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
							}else{
								ratio = new BigDecimal("1.00");
							}
							if("F".equals(planHeader.getUwRatingKey()) || planHeader.getUwRatingKey() == null){
								InsureSum = temp.getFaceAmount();
							}else{
								InsureSum = temp.getSumInsure();
							}
							if(InsureSum==null){
								InsureSum  = BigDecimal.ZERO;
							}
							historyInsure = historyInsure.add(ratio.multiply(InsureSum));
						}	
					}
				}
			}
		}
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureByPlanChannelGroup(String docType,List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			
			for(HistoryInsure temp:historyList) {
				if(docType.equals(temp.getDocumentType())) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
			}
		}
		
		return historyInsure;
	}
	
	public BigDecimal getSumHistoryInsureBasic(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			for(HistoryInsure temp:historyList) {
				
				if(temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
			}
		}
		
		return historyInsure;
	}
	
	/**
	 * @param historyList
	 * 
	 * For sumInsure in historyList
	 * Only BASIC and in (MRTA,MLTA)
	 * 
	 * @return
	 */
	public BigDecimal getSumHistoryInsureMR(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			for(HistoryInsure temp:historyList) {
				
				if(temp.getGroupChannelCode().equals("MRTA") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
				if(temp.getGroupChannelCode().equals("MLTA") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
			}
		}
		
		return historyInsure;
	}
	
	/**
	 * @param historyList
	 * 
	 * For sumInsure in historyList
	 * Only BASIC and ORD
	 * 
	 * @return
	 */
	public BigDecimal getSumHistoryInsureRT(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			for(HistoryInsure temp:historyList) {
				
				if(temp.getGroupChannelCode().equals("ORD") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
			}
		}
		
		return historyInsure;
	}
	
	/**
	 * @param historyList
	 * 
	 * For sumInsure in historyList
	 * Only BASIC and LRTA
	 * 
	 * @return
	 */
	public BigDecimal getSumHistoryInsureCB(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			for(HistoryInsure temp:historyList) {
				if(temp.getGroupChannelCode().equals("LRTA") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
			}
		}
		
		return historyInsure;
	}
	
	/**
	 * @param historyList
	 * 
	 * For sumInsure in historyList
	 * Only BASIC and in (WON05D,WON05E,WON10D,WON10E)
	 * 
	 * @return
	 */
	public BigDecimal getSumHistoryInsureHnw(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			
			for(HistoryInsure temp:historyList) {
				
				if(temp.getPlanCode().equals("WON05D") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}

				if(temp.getPlanCode().equals("WON05E") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
				if(temp.getPlanCode().equals("WON10D") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
				if(temp.getPlanCode().equals("WON10E") && temp.getPlanGroupType().equals("BASIC")) {
					historyInsure = historyInsure.add(temp.getSumInsure());
				}
				
			}
		}
		
		return historyInsure;
	}
	
	/**
	 * @param historyList
	 * 
	 * For sumInsure in historyList
	 * 
	 * @return historyInsure
	 */
	public BigDecimal getSumHistoryInsurePA(List<HistoryInsure> historyList){
		
		BigDecimal historyInsure = BigDecimal.ZERO; 
		if(historyList !=null) {
			for(HistoryInsure temp:historyList) {
				historyInsure = historyInsure.add(temp.getSumInsure());

			}
		}
		
		return historyInsure;
	}
	
	public BigDecimal getSumInsuredByPlanType(String planType, List<HistoryInsure> insureDetails, String systemName) {
		BigDecimal sumInsured = BigDecimal.ZERO;
	
		if (insureDetails != null && insureDetails.size() > 0) {
			for ( HistoryInsure insure : insureDetails) {
				if (insure.getPlanType() != null && RuleUtils.isOneOf(planType, insure.getPlanType()) && systemName.equals(insure.getSystemName())) {
					sumInsured = sumInsured.add(insure.getSumInsure());
				}
			}
		}
		
		System.out.println("sumInsured : " +sumInsured);
		return sumInsured;
	}
}
