package com.mtl.collector.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT03HealthSuminsured;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
public class CheckHealth {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}


	
	
	
	
	
	public List<DT03HealthSuminsured> fillterData(List<DT03HealthSuminsured> value, BigDecimal allSumInsuredBasic, String ageDay , String ageMonth, String ageYear){
		List<DT03HealthSuminsured> res = new ArrayList<DT03HealthSuminsured>();
		if(value!=null) {
			for(DT03HealthSuminsured temp:value) {
				boolean checkAgeMonth = false;
				if(temp.getMinAgeMonth() == null || RuleConstants.OTHERWISE.equals(temp.getMinAgeMonth())
						||	temp.getMaxAgeMonth() == null || RuleConstants.OTHERWISE.equals(temp.getMaxAgeMonth())) {
					checkAgeMonth = false;
				}else {
					if(ageMonth.isEmpty()){
						ageMonth="0";
					}
					if("0".equals(ageYear) && "0".equals(ageMonth)){
						checkAgeMonth = RuleUtils.betweenNumber(Long.valueOf(ageDay), Long.valueOf(temp.getMinAgeMonth()), Long.valueOf(temp.getMaxAgeMonth()));
						if(checkAgeMonth){
							continue;
						}
					}
					checkAgeMonth = RuleUtils.betweenNumber(Long.valueOf(ageMonth), Long.valueOf(temp.getMinAgeMonth()), Long.valueOf(temp.getMaxAgeMonth()));
				}
				
				if(checkAgeMonth) {
					boolean checkAgeYear = RuleUtils.betweenNumberYear(Long.valueOf(ageYear), Long.valueOf(temp.getMinAgeYear()), Long.valueOf(temp.getMaxAge()));
					
					if(checkAgeYear) {
						BigDecimal sumMin = new BigDecimal(temp.getMinSuminsured());
						BigDecimal sumMax = new BigDecimal(temp.getMaxSuminsured());
						boolean checkSum = RuleUtils.betweenNumber(allSumInsuredBasic, sumMin, sumMax);
						
						if(checkSum) {
							res.add(temp);
						}
					}
				}else {
					if(res.size() == 0) {
						res.add(temp);
					}
				}
			}
		}
		return res;
	}

}
