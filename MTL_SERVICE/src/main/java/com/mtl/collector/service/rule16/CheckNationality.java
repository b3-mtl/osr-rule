package com.mtl.collector.service.rule16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.dao.Rule16VerifyNationalityDao;
import com.mtl.rule.model.DT04AppMdc;
import com.mtl.rule.model.DT16VerifyNationality;

@Service
public class CheckNationality {

	@Autowired
	private Rule16VerifyNationalityDao verifyNationalityDao;
	
	public HashMap< String , DT16VerifyNationality> CheckHealthQuestionAndNationality() {
		return verifyNationalityDao.getAll();

	}
}
