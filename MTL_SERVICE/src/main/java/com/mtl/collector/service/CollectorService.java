package com.mtl.collector.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.api.core.constants.DataCollectorConstant.DataCollector;
import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.CommonMasterDataDao;
import com.mtl.collector.model.ClientProfile;
import com.mtl.collector.ws.codegen.getMIB.MibResultList;
import com.mtl.collector.ws.service.GetAgentInfoService;
import com.mtl.collector.ws.service.GetClaimCodeWsClientService;
import com.mtl.collector.ws.service.GetGroupChannelWsService;
import com.mtl.collector.ws.service.GetMIBWsClientService;
import com.mtl.collector.ws.service.GetSearchClientProfileByIdCardNoWsClientService;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.common.PlanType;
import com.mtl.model.underwriting.BeneficiaryDetail;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.PBPersonalData;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.RequestBody;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.MsInvesmentPlanType;
import com.mtl.rule.util.RuleConstants;

@Service
public class CollectorService {

	private static final Logger log = LogManager.getLogger(CollectorService.class);

	@Autowired
	private GetClaimCodeWsClientService getClaimCodeWsClientService;

	@Autowired
	private GetMIBWsClientService getMIBWsClientService;

	@Autowired
	private GetSearchClientProfileByIdCardNoWsClientService getSearchClientProfileByIdCardNoWsClientService;
	
	@Autowired
	private GetGroupChannelWsService getGroupChannelWsService;

	@Autowired
	private GetCollectorHistoryInsure getCollectorHistoryInsure;

	@Autowired
	private GetAgentInfoService getAgentInfoService;

	@Autowired
	private ApplicationCache applicationCache;

	public CollectorModel getDataCollector(UnderwriteRequest requestModel, Set<String> collector,Map<String, String> ruleSet) throws Exception {
		String state = "\n";
		CollectorModel collectorModel = new CollectorModel();
		Set<String> rules = new HashSet<String>(ruleSet.values());
		if (collector == null || collector.size() == 0) {
			state = " No Data Collecter List !!!!";
			log.info(state);
			collector = new HashSet<String>();
		}
		
		
		


		// Set PlanHeader for Basic and Rider
		if (requestModel.getRequestBody().getBasicInsureDetail() != null
				&& StringUtils.isNotBlank(requestModel.getRequestBody().getBasicInsureDetail().getPlanCode())) {
			String planCode = requestModel.getRequestBody().getBasicInsureDetail().getPlanCode();
			PlanHeader baiscPlanHeader = applicationCache.getPlanHeaderAppCashMap().get(planCode);
			/* Set MsParameter  */
			PlanType planType = applicationCache.getPlanTypeCacheMap().get(planCode);
			if(planType != null) {
				baiscPlanHeader.setPlanType(planType.getPlanType());
			}
			/* Set MsInvesmentPlanType  */
			MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(planCode);
			if(planTypeInvestMent != null) {
				baiscPlanHeader.setPlanType(planTypeInvestMent.getInvestmentType());
			}
			
			requestModel.getRequestBody().getBasicInsureDetail().setPlanHeader(baiscPlanHeader);
		}

		List<InsureDetail> riderInsureList = requestModel.getRequestBody().getRiderInsureDetails();
		if (riderInsureList != null) {
			for (InsureDetail insureTmp : riderInsureList) {
				PlanHeader riderPlanHeader = applicationCache.getPlanHeaderAppCashMap().get(insureTmp.getPlanCode());
				PlanType planType = applicationCache.getPlanTypeCacheMap().get(insureTmp.getPlanCode());
				if(planType != null) {
					riderPlanHeader.setPlanType(planType.getPlanType());
				}
				/* Set MsParameter  */
				PlanType planTypeR = applicationCache.getPlanTypeCacheMap().get(insureTmp.getPlanCode());
				if(planTypeR != null) {
					riderPlanHeader.setPlanType(planTypeR.getPlanType());
				}
				/* Set MsInvesmentPlanType  */
				MsInvesmentPlanType planTypeInvestMentR = applicationCache.getMsInvesmentPlanType().get(insureTmp.getPlanCode());
				if(planTypeInvestMentR != null) {
					riderPlanHeader.setPlanType(planTypeInvestMentR.getInvestmentType());
					riderPlanHeader.setPlanSystem(planTypeInvestMentR.getSystemName());
				}
				
				insureTmp.setPlanHeader(riderPlanHeader);
			}
		}

		// Collector by Collector mapping

		// DC1: Claim Code
		PersonalData currentPersonalData = requestModel.getRequestBody().getPersonalData();
//		PBPersonalData pbPersonalData = requestModel.getRequestBody().getPayorBenefitPersonalData();
		if (collector.contains(DataCollector.DC1)) {
			String idCardNo = currentPersonalData.getIdCard();
			ClientProfile tempClientProfile = null;
			tempClientProfile = getSearchClientProfileByIdCardNoWsClientService.searchClientProfilebyIDCardNo(idCardNo);
			collectorModel.setClientProfile(tempClientProfile);
			currentPersonalData.setClientNumber(tempClientProfile.getCodeClient());

			String[] tempClaimCodes = null;
			if (tempClientProfile.getCodeClient() != null) {
				tempClaimCodes = getClaimCodeWsClientService.getClaimCode(tempClientProfile.getCodeClient());
			}
			collectorModel.setPersonClaimCodes(tempClaimCodes);
//			pbPersonalData.setClaimCodeList(tempClaimCodes);
			currentPersonalData.setClaimCodeList(tempClaimCodes);
			if (tempClaimCodes != null) {
				state = state + DataCollector.DC1 + " Data Collector  [GetClaimCodeWS Size:" + tempClaimCodes.length
						+ "]" + "\n";
			}
		}

		// DC2: MIB
		if (collector.contains(DataCollector.DC2)) {
			
			String appFormNumber = requestModel.getRequestBody().getAppForm();
			List<String> plan = new ArrayList<String>();
			InsureDetail basic = requestModel.getRequestBody().getBasicInsureDetail();
			if(basic!=null) {
				plan.add(basic.getPlanCode());
			}
			List<InsureDetail> rider = requestModel.getRequestBody().getRiderInsureDetails();
			if(rider !=null) {
				for (InsureDetail temp : rider) {
					plan.add(temp.getPlanCode());
				}
			}
			String[] planCode = plan.toArray(new String[0]);
			String idCard = requestModel.getRequestBody().getPersonalData().getIdCard();

			System.out.println(" appFormNumber:" + appFormNumber + " idCard:" + idCard);
			
			if (rules.contains("serviceRule17")) {
				if (requestModel.getRequestBody().getBeneficiaryDetails() != null
						&& requestModel.getRequestBody().getBeneficiaryDetails().size() > 0) {
					List<String> tempMIBsAmlo = new ArrayList<String>();
					for (BeneficiaryDetail tmp : requestModel.getRequestBody().getBeneficiaryDetails()) {
						List<String> code = getMIBWsClientService.getMIBRule17(tmp.getFirstName(), tmp.getLastName(),RuleConstants.RULE_07.FIXED_FILLTERPERCENT_AMLO);
						tempMIBsAmlo.addAll(code);
						tmp.setMibAmloCodeList(tempMIBsAmlo);
					}
				}

			} else if (rules.contains("serviceRule16")) {
				PBPersonalData payorBenefitPersonalData = requestModel.getRequestBody().getPayorBenefitPersonalData();
				if (payorBenefitPersonalData != null && StringUtils.isNotBlank(payorBenefitPersonalData.getIdCard())) {
					String pbidCard = payorBenefitPersonalData.getIdCard();
					ClientProfile tempClientProfile = null;
					tempClientProfile = getSearchClientProfileByIdCardNoWsClientService
							.searchClientProfilebyIDCardNo(pbidCard);
					if (tempClientProfile == null) {
						state = state + " serviceRule16 PBPersonalData [SearchClientProfileByIdCardNoWs: Not Found"
								+ "\n";
					} else {
						requestModel.getRequestBody().getPayorBenefitPersonalData()
								.setClientNumber(tempClientProfile.getCodeClient());
						state = state + " serviceRule16 PBPersonalData [SearchClientProfileByIdCardNoWs CodeClient:"
								+ tempClientProfile.getCodeClient() + "]" + "\n";
					}

					String pbtClientNumber = tempClientProfile.getCodeClient();
					String[] tempClaimCodes = null;
					if(pbtClientNumber !=null) {
						tempClaimCodes = getClaimCodeWsClientService.getClaimCode(pbtClientNumber);
					}
					payorBenefitPersonalData.setClaimCodeList(tempClaimCodes);
					if (tempClaimCodes != null && tempClaimCodes.length > 0) {
						state = state + " serviceRule16  PBPersonalData [GetClaimCodeWS Size:" + tempClaimCodes.length
								+ "]" + "\n";

					} else {
						state = state + " serviceRule16 PBPersonalData  [GetClaimCodeWS Size:Not Found ]" + "\n";
					}
					
					List<String> tempMIBs = getMIBWsClientService.getMIB(appFormNumber, planCode, pbidCard,
							RuleConstants.RULE_07.FIXED_FILLTERPERCENT_MIB);
					List<String> tempAMLOMIBs = getMIBWsClientService.getMIB(appFormNumber, planCode, pbidCard,
							RuleConstants.RULE_07.FIXED_FILLTERPERCENT_AMLO);
					payorBenefitPersonalData.setMibCodeList(tempMIBs);
					payorBenefitPersonalData.setMibAmloCodeList(tempAMLOMIBs);
					collectorModel.setPersonMIBCodeList(tempMIBs);
					collectorModel.setPersonMIBAmloCodeList(tempAMLOMIBs);

				}

			} else {
				boolean isFlagClaim = false;
				List<String> tempMIBs = getMIBWsClientService.getMIB(appFormNumber, planCode, idCard,
						RuleConstants.RULE_07.FIXED_FILLTERPERCENT_MIB);
				List<String> tempMIBsAmlo = getMIBWsClientService.getMIB(appFormNumber, planCode, idCard,
						RuleConstants.RULE_07.FIXED_FILLTERPERCENT_AMLO);
				collectorModel.setPersonMIBCodeList(tempMIBs);
				collectorModel.setPersonMIBAmloCodeList(tempMIBsAmlo);
				currentPersonalData.setMibCodeList(tempMIBs);
				currentPersonalData.setMibAMLOCodeList(tempMIBsAmlo);
				isFlagClaim = getMIBWsClientService.getFlagClaim(appFormNumber, planCode, idCard,
						RuleConstants.RULE_07.FIXED_FILLTERPERCENT_MIB);
				currentPersonalData.setFlagClaim(isFlagClaim);
				if (tempMIBs != null) {
					state = state + DataCollector.DC2 + " Data Collector  [MIBWS Size:" + tempMIBs.size() + "]" + "\n";
				}
				if (tempMIBsAmlo != null) {
					state = state + DataCollector.DC2 + " Data Collector  [MIBWS Amlo Size:" + tempMIBsAmlo.size() + "]"
							+ "\n";
				}
			}
		}

		// DC3:Seacrh Client Profile
		if (collector.contains(DataCollector.DC3)) {
			String idCardNo = requestModel.getRequestBody().getPersonalData().getIdCard();
			ClientProfile tempClientProfile = null;
			tempClientProfile = getSearchClientProfileByIdCardNoWsClientService.searchClientProfilebyIDCardNo(idCardNo);
			collectorModel.setClientProfile(tempClientProfile);
			if (tempClientProfile == null) {
				state = state + DataCollector.DC3 + " Data Collector  [SearchClientProfileByIdCardNoWs: Not Found"
						+ "\n";

			} else {
				requestModel.getRequestBody().getPersonalData().setClientNumber(tempClientProfile.getCodeClient());
				state = state + DataCollector.DC3 + " Data Collector  [SearchClientProfileByIdCardNoWs CodeClient:"
						+ tempClientProfile.getCodeClient() + "]" + "\n";
			}
		}
		
		// DC4:History Insure
		List<HistoryInsure> personHistoryList = new ArrayList<HistoryInsure>();
		if (collector.contains(DataCollector.DC4)) {
			String clientNo = requestModel.getRequestBody().getPersonalData().getClientNumber();
			BigDecimal historyInsusre = getCollectorHistoryInsure.getHistoryInsure(clientNo);
			collectorModel.setHistoryInsure(historyInsusre);
			state = state + DataCollector.DC4 + " Data Collector  [CollectorHistoryInsure clientNo:" + clientNo + " -->"
					+ historyInsusre + "]" + "\n";

			// get HistoryInsure List from WS
			String idCard = requestModel.getRequestBody().getPersonalData().getIdCard();
			List<HistoryInsure> temp = getCollectorHistoryInsure.getHistoryInsureList(idCard);
			personHistoryList = temp;
			collectorModel.setHistoryInsureList(personHistoryList);
		}

		// DC5:Agent Info
		if (collector.contains(DataCollector.DC5)) {
			String agentCode = requestModel.getRequestBody().getAgentCode();
			String planCode = requestModel.getRequestBody().getBasicInsureDetail().getPlanCode();
			AgentInfo agentDetail = getAgentInfoService.getAgentDetailByAgentCode(agentCode,planCode);

			collectorModel.setAgentDetail(agentDetail);
			state = state + DataCollector.DC5
					+ " Data Collector  [SearchagentChannelbyAgentAndPlanWsClientService  GroupChannel:"
					+ agentDetail.getAgentChannel() + "]" + "\n";
			
			if (rules.contains("serviceRule14")) {
				InsureDetail basicInsureDetail = requestModel.getRequestBody().getBasicInsureDetail();
				List<InsureDetail> riderInsureListTmp = requestModel.getRequestBody().getRiderInsureDetails();
				
				String groupChannelBasic = getGroupChannelWsService.getGroupChannelByAgentCodeAndPlanCodeWsService(agentCode, planCode);
				basicInsureDetail.setGroupChannelCode(groupChannelBasic);
				if (riderInsureList != null) {
					for (InsureDetail insureTmp : riderInsureListTmp) {
						String groupChannel = getGroupChannelWsService.getGroupChannelByAgentCodeAndPlanCodeWsService(agentCode, insureTmp.getPlanCode());

						insureTmp.setGroupChannelCode(groupChannel);
					}
				}

				if (personHistoryList != null && personHistoryList.size() > 0) {
					for (HistoryInsure hisTmp : personHistoryList) {
						String groupChannel = getGroupChannelWsService.getGroupChannelByAgentCodeAndPlanCodeWsService(agentCode, hisTmp.getPlanCode());

						hisTmp.setGroupChannelCode(groupChannel);
					}
				}
			}
		}


//		log.info(state);
		return collectorModel;
	}

	public static final Date convertDateToXMLGregorianCalender(XMLGregorianCalendar calendar) {
		return calendar.toGregorianCalendar().getTime();

	}

}
