package com.mtl.collector.service.rule03;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mtl.appcache.ApplicationCache;

@Service
public class CheckTerm {

	@Autowired
	private ApplicationCache applicationCache;

	public boolean checkValueInParameter(String key, String value) {
		List<String> valueList = applicationCache.getMsParameter().get(key);

		if (valueList != null && valueList.size() > 0) {
			for (String temp : valueList) {
				if (temp.equals(value)) {
					return true;
				} else {
					continue;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public boolean checkBetweenSumInsured(Double min, Double max, BigDecimal sum) {
		BigDecimal tempMin = new BigDecimal(min);
		BigDecimal tempMax = new BigDecimal(max);
		boolean rs = tempMin.compareTo(sum) < 0 && tempMax.compareTo(sum) > 0;
		return rs;

	}

}
