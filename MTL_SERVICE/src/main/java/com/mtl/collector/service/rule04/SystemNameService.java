package com.mtl.collector.service.rule04;

import org.springframework.stereotype.Service;

import com.mtl.rule.util.RuleConstants;

@Service
public class SystemNameService {
	
	public Boolean checkName(String systemName) {
		if(RuleConstants.RULE_04.IL.equals(systemName)) {
			return true;
		}else {
			return false;
		}
	}
	
	public Boolean checkNamePOS(String systemName) {
		if(RuleConstants.RULE_04.POS.equals(systemName)) {
			return true;
		}else {
			return false;
		}
	}
}
