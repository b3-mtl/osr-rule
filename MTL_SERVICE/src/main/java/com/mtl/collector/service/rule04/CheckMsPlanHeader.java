package com.mtl.collector.service.rule04;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.rule.util.RuleConstants;
import com.mtl.api.core.entity.MsPlanHeader;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;

@Service
public class CheckMsPlanHeader {
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * @param planCode
	 * 
	 * Check Premium Indicator in MS_PLAN_HEADER is "2" by planCode
	 * 
	 * @return boolean
	 */
	public Boolean checkPremiumIndicator(String planCode) {
		HashMap<String, PlanHeader> cacheData = applicationCache.getPlanHeaderAppCashMap();
		String indicator = cacheData.get(planCode).getPremiumIndicator();
		if(RuleConstants.RULE_04.PREMIUM_INDICATOR.equals(indicator)) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * @param planCode
	 * 
	 * Check planGroup in MS_PLAN_HEADER is "BASIC" by planCode
	 * 
	 * @return boolean
	 */
	public Boolean checkBasicPlan(String planCode) {
		HashMap<String, PlanHeader> cacheData = applicationCache.getPlanHeaderAppCashMap();
		String plan = cacheData.get(planCode).getPlanGroupType();
		if(RuleConstants.RULE_04.PLAN_GROUP_TYPE.equals(plan)) {
			return true;
		}else {
			return false;
		}
	}

}
