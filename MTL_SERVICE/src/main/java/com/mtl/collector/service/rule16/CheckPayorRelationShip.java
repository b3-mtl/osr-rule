package com.mtl.collector.service.rule16;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.rule.model.DT16PayorRelation;
import com.mtl.rule.repository.DT16PayorRelationDao;

@Service
public class CheckPayorRelationShip {

	@Autowired
	private DT16PayorRelationDao dt16PayorRelationDao;
	
	public List<DT16PayorRelation> checkPayorRelationShip(String payorRelation) {
		List<DT16PayorRelation> list = dt16PayorRelationDao.findPayorRelation(payorRelation);
		return list;
	}
}
