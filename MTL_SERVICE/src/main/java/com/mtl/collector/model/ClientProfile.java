
package com.mtl.collector.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientProfile {

	private String codeClient;
	private String title;
	private String firstName;
	private String lastName;
	private String titleEng;
	private String firstNameEng;
	private String lastNameEng;
	private String ssn;
	private String birthDate;
	private String address1; 
	private String address2; 
	private String address3;
	private String city;
	private String province;
	private String country;
	private String district;
	private String addressPostal;
	private String sex;
	
 
 

}
