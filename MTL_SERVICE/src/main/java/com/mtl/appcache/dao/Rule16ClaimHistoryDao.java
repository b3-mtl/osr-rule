package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14PADocument;
import com.mtl.rule.model.DT16ClaimHistory;

@Repository
public class Rule16ClaimHistoryDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String ,DT16ClaimHistory> getAll() {
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_CLAIM_HISTORY");
		List<DT16ClaimHistory> dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT16ClaimHistory.class));
		return rewriteData(dataRes);
	}
	

	private HashMap< String ,DT16ClaimHistory> rewriteData(List<DT16ClaimHistory> ls){
		HashMap< String , DT16ClaimHistory> dt16ClaimHistoryMap = new HashMap< String ,DT16ClaimHistory>();	
		for(DT16ClaimHistory item : ls)
		{
			dt16ClaimHistoryMap.put(item.getCode(), item);
		}
		return dt16ClaimHistoryMap;
	}
	
}
