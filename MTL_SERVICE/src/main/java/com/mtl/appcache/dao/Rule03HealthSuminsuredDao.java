package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03HealthSuminsured;

@Repository
public class Rule03HealthSuminsuredDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , List<DT03HealthSuminsured>> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_HEALTH_SUM ORDER BY MIN_AGE_MONTH , MIN_AGE_YEAR , MAX_SUMINSURED_FIXED_MIX , MAX_SUMINSURED_DISTR , MAX_SUMINSURED_HEALTH , MAX_SUMINSURED_FIXED ");
	 
		List<DT03HealthSuminsured> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03HealthSuminsured.class));
		System.out.println(" Get All DT03_HEALTH_SUM.. Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
 
	private HashMap< String ,  List<DT03HealthSuminsured> > rewriteData(List<DT03HealthSuminsured> list){		
		HashMap< String ,  List<DT03HealthSuminsured> > mapReturn = new HashMap< String ,  List<DT03HealthSuminsured>> ();		
		for (DT03HealthSuminsured  item : list) {
			
			String[] agentGroup = item.getAgentGroup().split(",");
			for(String group:agentGroup) {
				if(mapReturn.containsKey(group)) {
					List<DT03HealthSuminsured> temp = new ArrayList<DT03HealthSuminsured>();
					temp.addAll(mapReturn.get(group));
					temp.add(item);
					mapReturn.replace(group, temp);
				}else {
					List<DT03HealthSuminsured> temp = new ArrayList<DT03HealthSuminsured>();
					temp.add(item);
					mapReturn.put(group, temp);
				}
			}
		}
		return mapReturn;
	}
}