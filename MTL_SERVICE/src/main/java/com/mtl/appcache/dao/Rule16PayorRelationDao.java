package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.rule.PayorRelation;

@Repository
public class Rule16PayorRelationDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , PayorRelation> getRelationMap(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from dt16_payor_relation ");
	 
		List<PayorRelation> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		System.out.println(" ###### Found Rule 16 dt16_payor_relation List Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
	private RowMapper<PayorRelation> rowMapper = new RowMapper<PayorRelation>() {
		
		@Override
		public PayorRelation mapRow(ResultSet rs, int arg1) throws SQLException {			
			PayorRelation map = new PayorRelation();	
			map.setRelation(rs.getString("RELATIONSHIP"));
			map.setMessageCode(rs.getString("MESSAGE_CODE"));
			map.setMessagDesc(rs.getString("MESSAGE_DESC"));
			map.setDocumentCode(rs.getString("DOCUMENT"));
			return map;
		}
		
	};
	
 
	private HashMap< String , PayorRelation> rewriteData(List<PayorRelation> mapBeanList){		
		HashMap< String , PayorRelation> mapReturn = new HashMap< String , PayorRelation>();		
		for (PayorRelation  item : mapBeanList) {
			mapReturn.put(item.getRelation(), item); 
		}
		return mapReturn;
	}
}