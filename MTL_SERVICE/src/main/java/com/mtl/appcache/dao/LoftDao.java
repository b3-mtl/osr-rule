package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.underwriting.Loft;

@Repository
public class LoftDao {
	
	@Qualifier("commonLoftJdbcTemplate")
	@Autowired
	private CommonJdbcTemplate commonLoftJdbcTemplate;
	
	public List<Loft> findLoftByIdCard(String idCard){
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sb.append(" SELECT * FROM EAPP.VW_INFORMATION_4OSR WHERE ID_CARD = ?");
		params.add(idCard);
		List<Loft> ls = commonLoftJdbcTemplate.executeQuery(sb.toString(), params.toArray(), rowMapper);
		return ls;
	}
 
	private RowMapper<Loft> rowMapper = new RowMapper<Loft>() {		
		@Override
		public Loft mapRow(ResultSet rs, int arg1) throws SQLException {			
			Loft res = new Loft();
			res.setAgentCode(rs.getString("AGENT_CODE"));
			res.setAppStatus(rs.getString("APP_STATUS"));
			res.setCardType(rs.getString("CARD_TYPE"));
			res.setIdCard(rs.getString("ID_CARD"));
			res.setPlanCode(rs.getString("PLAN_CODE"));
			res.setSubmitDate(rs.getDate("SUBMIT_DATE"));
			res.setSubmitSystem(rs.getString("SUBMIT_SYSTEM"));
			res.setSumInsured(rs.getLong("SUM_INSURED"));
			return res;
		}
	};
	
}
