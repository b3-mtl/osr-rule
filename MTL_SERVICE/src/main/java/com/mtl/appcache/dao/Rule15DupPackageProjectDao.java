package com.mtl.appcache.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT15DupPackageProject;

@Repository
public class Rule15DupPackageProjectDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT15DupPackageProject> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT15_DUPLICATE_PACKAGE_PROJECT ");
	 
		List<DT15DupPackageProject> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT15DupPackageProject.class));
		System.out.println(" Get All DT15_DUPLICATE_PACKAGE_PROJECT.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
	
	
}
