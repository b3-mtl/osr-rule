package com.mtl.appcache.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT15DupPlanForPolicy;

@Repository
public class Rule15DupPlanForPolicyDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT15DupPlanForPolicy> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT15_DUPLICATE_PLAN_FOR_POLICY ");
	 
		List<DT15DupPlanForPolicy> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT15DupPlanForPolicy.class));
		System.out.println(" Get All DT15_DUPLICATE_PLAN_FOR_POLICY.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
	
}
