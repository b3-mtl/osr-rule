package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Drug;
import com.mtl.rule.model.DT11Smoke;

@Repository
public class Rule11DT11DrugDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	
	public List<DT11Drug> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from DT11_DRUG");
	 
		List<DT11Drug> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT11Drug.class));
		System.out.println(" ###### Found Rule 11 DT11_DRUG List Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
}
