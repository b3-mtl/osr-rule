package com.mtl.appcache.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.common.PlanPermission;

@Repository
public class PlanPermissionDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	 
	private static final Logger log = LogManager.getLogger(PlanPermissionDao.class);
	
	public HashMap< String , PlanPermission> getAllPlanPermission(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM PLAN_PERMISSION  ");
		
		List<PlanPermission> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All PLAN_PERMISSION.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
	private RowMapper<PlanPermission> rowMapper = new RowMapper<PlanPermission>() {		
		@Override
		public PlanPermission mapRow(ResultSet rs, int arg1) throws SQLException {			
			PlanPermission item = new PlanPermission(); 
 
			
			item.setPlanCode(rs.getString("PLAN_CODE"));
			item.setChannel(rs.getString("CHANNEL"));
			item.setExceptMib(rs.getString("EXCEPT_MIB"));
			item.setExceptMibSpecial(rs.getString("EXCEPT_MIB_SPECIAL"));
			item.setExceptCOF(rs.getString("EXCEPT_COF"));
			item.setExceptBMI(rs.getString("EXCEPT_BMI"));
			item.setExceptInsuredClaim(rs.getString("EXCEPT_INSURED_CLAIM"));
			item.setExceptHIV(rs.getString("EXCEPT_HIV"));
			item.setExceptHealthBenefit(rs.getString("EXCEPT_HEALTH_BENEFIT"));
			item.setExceptPBMIB(rs.getString("EXCEPT_PB_MIB"));
			item.setExceptPBClaim(rs.getString("EXCEPT_PB_CLAIM"));
			item.setExceptPBHealth(rs.getString("EXCEPT_PB_HEALTH"));
		 
 
			return item;
		}
		
	};
	
	private HashMap< String , PlanPermission> rewriteData(List<PlanPermission> docListIn){		
		HashMap< String , PlanPermission> returnMap = new HashMap< String ,PlanPermission>();		
		String key ="";
		for (PlanPermission item : docListIn) {
			String[] plancode = item.getPlanCode().split(",");
			for (String plan : plancode) {
				
				if(item.getChannel() !=null) {
					key = plan.trim()+"|"+item.getChannel();
					if(!returnMap.containsKey(key)) {
						returnMap.put(key, item);
					}
				}else {
					key = plan.trim()+"|";
					if(!returnMap.containsKey(key)) {
						returnMap.put(key, item);
					}
				}
			}
		}
 		return returnMap;
	}
	
	
	
}
