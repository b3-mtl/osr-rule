package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03HealthSuminsured;
import com.mtl.rule.model.DT03HealthSuminsuredInclude;
import com.mtl.rule.model.DT11Examination;

@Repository
public class Rule03HealthSumInCludeDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public List<DT03HealthSuminsuredInclude> getAll() {
		List<DT03HealthSuminsuredInclude> dataRes = new ArrayList<DT03HealthSuminsuredInclude>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_HEALTH_SUM_INCLUDE ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03HealthSuminsuredInclude.class));
		return dataRes;
	}
	
}