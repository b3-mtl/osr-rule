package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT17MIBAMLO;

@Repository
public class Rule17MIBAMLODao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<String> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from dt17_MIB_AMLO ");
	 
		List<DT17MIBAMLO> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		System.out.println(" ###### Found Rule 17 dt17_MIB_AMLO List Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
	private RowMapper<DT17MIBAMLO> rowMapper = new RowMapper<DT17MIBAMLO>() {
		
		@Override
		public DT17MIBAMLO mapRow(ResultSet rs, int arg1) throws SQLException {			
			DT17MIBAMLO map = new DT17MIBAMLO();	
			map.setRelation(rs.getString("RELATION"));
			map.setAddmib(rs.getString("ADDMIB"));
			map.setId(rs.getLong("ID"));
			return map;
		}
		
	};
	
 
	private List<String> rewriteData(List<DT17MIBAMLO> mapBeanList){		
		List<String> mapReturn = new ArrayList<String>();		
		for (DT17MIBAMLO  item : mapBeanList) {
			mapReturn.add(item.getRelation()); 
		}
		return mapReturn;
	}
}
