package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16Alcohol;

@Repository
public class Rule16DT16AlcoholDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT16Alcohol> getAll() {
		List<DT16Alcohol> dataRes = new ArrayList<DT16Alcohol>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT16_ALCOHOL");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT16Alcohol.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT16Alcohol> rewriteData(List<DT16Alcohol> dataMap){
		HashMap< String , DT16Alcohol>   alcohol= new HashMap< String , DT16Alcohol>();
		for(DT16Alcohol alcoholdata:dataMap)
		{
			String key = alcoholdata.getSex()+"|"+alcoholdata.getAlcoholType();
			alcohol.put(key, alcoholdata);
		}
		return alcohol;		
	}
}
