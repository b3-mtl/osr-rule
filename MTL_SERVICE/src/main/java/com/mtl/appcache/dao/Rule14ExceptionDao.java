package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14Exception;

@Repository
public class Rule14ExceptionDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, DT14Exception> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_EX.* ");
		sb.append("  FROM DT14_EXCEPTION DT_EX  WHERE IS_DELETE = 'N' ");
		List<DT14Exception> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT14Exception.class));
		return rewriteData(ls);
	}
	
	private HashMap< String , DT14Exception> rewriteData(List<DT14Exception> ls){
		HashMap< String , DT14Exception> DT14ExceptionMap = new HashMap< String , DT14Exception>();
		String key ="";
		for ( DT14Exception item : ls) {
			key = "";
			String[] plancode;
			if(StringUtils.isNotBlank(item.getPlanCode())) {
				
				plancode = item.getPlanCode().split(",");
				
				for (String plan : plancode) {
					key = plan;
					DT14ExceptionMap.put(key,item);
				}
			}
		}
		
		return DT14ExceptionMap;
	}

}
