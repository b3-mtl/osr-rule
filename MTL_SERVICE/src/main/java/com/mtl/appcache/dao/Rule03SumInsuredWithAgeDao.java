package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.DT03SumInsuredWithAge;

@Repository
public class Rule03SumInsuredWithAgeDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03SumInsuredWithAge> getAll() {
		List<DT03SumInsuredWithAge> dataRes = new ArrayList<DT03SumInsuredWithAge>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_SUM_INSURED_WITH_AGE ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03SumInsuredWithAge.class));
		return dataRes;
	}
	
	
}
