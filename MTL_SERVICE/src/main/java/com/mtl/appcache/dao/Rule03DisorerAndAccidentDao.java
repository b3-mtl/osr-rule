package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03HealthSuminsured;
import com.mtl.rule.model.DT03HealthSuminsuredInclude;
import com.mtl.rule.model.Dt03BasicPlanSumInsured;
import com.mtl.rule.model.Dt03DisorderAndAccident;

@Repository
public class Rule03DisorerAndAccidentDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<Dt03DisorderAndAccident> getAll() {
		List<Dt03DisorderAndAccident> dataRes = new ArrayList<Dt03DisorderAndAccident>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_DISORER_AND_ACCIDENT ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(Dt03DisorderAndAccident.class));
		return dataRes;
	}
	
}