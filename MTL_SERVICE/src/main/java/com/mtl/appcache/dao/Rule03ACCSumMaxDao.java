package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ACCSumMax;

@Repository
public class Rule03ACCSumMaxDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ACCSumMax> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_ACC_SUM_MAX ");
	 
		List<DT03ACCSumMax> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03ACCSumMax.class));
		System.out.println(" Get All DT03_ACC_SUM_MAX.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
}