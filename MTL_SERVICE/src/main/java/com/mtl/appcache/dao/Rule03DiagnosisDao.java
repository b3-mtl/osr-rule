package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPT;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPG;

@Repository
public class Rule03DiagnosisDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , List<DT03DiagnosisSumInsuredPT>> getPTAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_DIAG_SUM_PT ");
	 
		List<DT03DiagnosisSumInsuredPT> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03DiagnosisSumInsuredPT.class));
		System.out.println(" Get All DT03_DIAG_SUM_PT.. Size: "+returnMapBeanList.size());
		return rewriteDataPT(returnMapBeanList);
	}
	
	public HashMap< String , List<DT03DiagnosisSumInsuredPG>> getPGAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_DIAG_SUM_PG ");
	 
		List<DT03DiagnosisSumInsuredPG> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03DiagnosisSumInsuredPG.class));
		System.out.println(" Get All DT03_DIAG_SUM_PG.. Size: "+returnMapBeanList.size());
		return rewriteDataPG(returnMapBeanList);
	}
 
	private HashMap< String ,  List<DT03DiagnosisSumInsuredPT> > rewriteDataPT(List<DT03DiagnosisSumInsuredPT> list){		
		HashMap< String ,  List<DT03DiagnosisSumInsuredPT> > mapReturn = new HashMap< String ,  List<DT03DiagnosisSumInsuredPT>> ();		
		for (DT03DiagnosisSumInsuredPT  item : list) {
			
			String[] listPT = item.getPlanType().split(",");
			for(String planType :listPT) {
				if(mapReturn.containsKey(planType)) {
					List<DT03DiagnosisSumInsuredPT> temp = new ArrayList<DT03DiagnosisSumInsuredPT>();
					temp.addAll(mapReturn.get(planType));
					temp.add(item);
					mapReturn.replace(planType, temp);
				}else {
					List<DT03DiagnosisSumInsuredPT> temp = new ArrayList<DT03DiagnosisSumInsuredPT>();
					temp.add(item);
					mapReturn.put(planType, temp);
				}
				
			}
		}
		return mapReturn;
	}
	
	private HashMap< String ,   List<DT03DiagnosisSumInsuredPG> > rewriteDataPG(List<DT03DiagnosisSumInsuredPG> list){		
		HashMap< String ,  List<DT03DiagnosisSumInsuredPG> > mapReturn = new HashMap< String ,  List<DT03DiagnosisSumInsuredPG>> ();		
		for (DT03DiagnosisSumInsuredPG  item : list) {
			
			String[] listPG = item.getPlanGroup().split(",");
			for(String planGroup :listPG) {
				if(mapReturn.containsKey(planGroup)) {
					List<DT03DiagnosisSumInsuredPG> temp = new ArrayList<DT03DiagnosisSumInsuredPG>();
					temp.addAll(mapReturn.get(planGroup));
					temp.add(item);
					mapReturn.replace(planGroup, temp);
				}else {
					List<DT03DiagnosisSumInsuredPG> temp = new ArrayList<DT03DiagnosisSumInsuredPG>();
					temp.add(item);
					mapReturn.put(planGroup, temp);
				}
				
			}
		}
		return mapReturn;
	}
}