package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;

@Repository
public class Rule12ClaimCodeHistoryDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , String> getClaimCodeHistory(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT code, refer_claim_history FROM DT12_CLAIM_HISTORY ");
	 
		List<MapBean> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		System.out.println(" Get All DT12_CLAIM_HISTORY.. Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
	private RowMapper<MapBean> rowMapper = new RowMapper<MapBean>() {
		
		@Override
		public MapBean mapRow(ResultSet rs, int arg1) throws SQLException {			
			MapBean map = new MapBean();	
			map.setKeystr(rs.getString("code"));
			map.setValueStr(rs.getString("refer_claim_history"));		
			//System.out.println(rs.getString("plan_code")+":"+rs.getString("refer_claim"));
			return map;
		}
		
	}; 
 
	private HashMap< String ,  String > rewriteData(List<MapBean> mapBeanList){		
		HashMap< String ,  String > mapReturn = new HashMap< String ,  String> ();		
		for (MapBean  item : mapBeanList) {
			mapReturn.put(item.getKeystr(), item.getValueStr());
			   
		}
		return mapReturn;
	}
}