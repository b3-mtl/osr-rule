package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14ExceptionAppNo;

@Repository
public class Rule14ExceptionAppNoDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, DT14ExceptionAppNo> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_EX.* ");
		sb.append("  FROM DT14_EXCEPTION_APPNO DT_EX ");
		List<DT14ExceptionAppNo> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT14ExceptionAppNo.class));
		return rewriteData(ls);
	}
	
	private HashMap< String , DT14ExceptionAppNo> rewriteData(List<DT14ExceptionAppNo> ls){
		HashMap< String , DT14ExceptionAppNo> DT14ExceptionAppNoMap = new HashMap< String , DT14ExceptionAppNo>();
		String key ="";
		for ( DT14ExceptionAppNo item : ls) {
			key = item.getAppFormNoKey();
			DT14ExceptionAppNoMap.put(key,item);
		}
		return DT14ExceptionAppNoMap;
	}

}
