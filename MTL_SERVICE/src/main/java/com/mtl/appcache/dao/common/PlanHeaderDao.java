package com.mtl.appcache.dao.common;

import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsPlanHeader;
import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.DocumentDesc;
import com.mtl.rule.model.MappingClientLocationCode;
import com.mtl.rule.repository.jpa.MappingClientLocationCodeRepository;

@Repository
public class PlanHeaderDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(PlanHeaderDao.class);

	@Autowired
	private MappingClientLocationCodeRepository locationRepo;

	public HashMap<String, PlanHeader> getAllPlanHeader() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_PLAN_HEADER  ");
		
		List<MsPlanHeader> ls = commonJdbcTemplate.executeQuery(sb.toString(), 
				new Object[] {}, new BeanPropertyRowMapper<>(MsPlanHeader.class));
		log.info(" Get All MS_PLAN_HEADER.. Size:"+ls.size());
		return rewriteData(ls);
	}

	private HashMap<String, PlanHeader> rewriteData(List<MsPlanHeader> docListIn) {
		HashMap<String, PlanHeader> returnMap = new HashMap<String, PlanHeader>();

		docListIn.forEach((item) -> {
			String key = item.getPlanCode().trim();

			PlanHeader planHeader = new PlanHeader();

			planHeader.setAgentQualification(item.getAgentQualification());
			planHeader.setChannel(item.getChannel());
			planHeader.setClassOfBusiness(item.getClassOfBusiness());
			planHeader.setMaximumAmount(item.getMaximumAmount());
			planHeader.setMinimumAmount(item.getMinimumAmount());
			planHeader.setPayment(item.getPayment());
			planHeader.setPlanCode(item.getPlanCode().trim());
			planHeader.setPlanType(item.getPlantype());
			planHeader.setPlanGroupType(item.getPlanGrouptype());
			planHeader.setPremiumIndicator(item.getPremiumIndicator());
			planHeader.setSex(item.getSex());
			planHeader.setNcType(item.getNcType());
			planHeader.setUwRatio(item.getUwRatio());
			planHeader.setUwRatingKey(item.getUwRatingKey());
			planHeader.setLowerAge(item.getLowerAge());
			planHeader.setUpperAge(item.getUpperAge());
			planHeader.setPlanSystem(item.getPlanSystem());
			returnMap.put(key, planHeader);
		});
		return returnMap;
	}

	public HashMap<String, MappingClientLocationCode> getAllClient() {
		List<MappingClientLocationCode> ls = locationRepo.findAll();
		return rewriteDataClient(ls);
	}

	private HashMap<String, MappingClientLocationCode> rewriteDataClient(List<MappingClientLocationCode> lsclient) {

		HashMap<String, MappingClientLocationCode> returnMap = new HashMap<String, MappingClientLocationCode>();

		for (MappingClientLocationCode item : lsclient) {
			returnMap.put(item.getProvinceCode(), item);
		}
		return returnMap;
	}
}
