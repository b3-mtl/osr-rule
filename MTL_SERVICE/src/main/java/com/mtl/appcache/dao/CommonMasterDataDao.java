package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.common.CodeDescBean;
import com.mtl.model.rule.DocumentDesc;

@Repository
public class CommonMasterDataDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	 
 
	
	public HashMap< String , DocumentDesc> getDocumentDescription(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT(code),description FROM MS_DOCUMENT_DESCRIOPTION  ");
 
		
		List<HashMap<String, String>> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		return rewriteData(ls);
	}
	
	private RowMapper<HashMap<String, String>> rowMapper = new RowMapper<HashMap<String, String>>() {		
		@Override
		public HashMap<String, String> mapRow(ResultSet rs, int arg1) throws SQLException {			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("CODE", rs.getString("CODE"));
			map.put("DESCRIPTION", rs.getString("DESCRIPTION"));
			return map;
		}
		
	};
	
	private HashMap< String , DocumentDesc> rewriteData(List<HashMap<String, String>> lsChannel){
		
		HashMap< String , DocumentDesc> returnMap = new HashMap< String ,DocumentDesc>();
		
		for (HashMap<String, String> item : lsChannel) {
			String code = item.get("CODE");
			String description = item.get("DESCRIPTION");
			DocumentDesc tmp = new DocumentDesc();
			tmp.setDocCode(code);
			tmp.setDocDesc(description);
			returnMap.put(code, tmp);			 
		}
 		return returnMap;
	}
	
	
	public HashMap< String , String> getPlanDesc(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT PLAN_CODE,PLAN_DESC FROM MS_PLAN_DESC  "); 
		List<CodeDescBean> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapperPlanDesc);
		return  rewriteDataPlanDesc(ls);
	}
	
	private RowMapper<CodeDescBean> rowMapperPlanDesc = new RowMapper<CodeDescBean>() {		
		@Override
		public CodeDescBean mapRow(ResultSet rs, int arg1) throws SQLException {			
			CodeDescBean returnVal = new CodeDescBean();
			returnVal.setCode(rs.getString("PLAN_CODE").trim());
			returnVal.setDesc(rs.getString("PLAN_DESC"));
		 		 System.out.print(" ######## PlancodeDesc:"+ rs.getString("PLAN_CODE")+" "+rs.getString("PLAN_DESC"));
			return returnVal;
		}
		
	};
	
	private HashMap< String , String> rewriteDataPlanDesc(List<CodeDescBean> listIn){ 
		HashMap< String , String> returnMap = new HashMap< String ,String>(); 
		for (CodeDescBean item : listIn) {
			String code = item.getCode();
			String description = item.getDesc(); 
			returnMap.put(code, description); 
		}
 		return returnMap;
	}
	
	
	public CodeDescBean getPlanDescByPlanCode(String planCode){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT PLAN_CODE,PLAN_DESC FROM MS_PLAN_DESC where  PLAN_CODE ='"+planCode+"'"); 
		CodeDescBean ls = commonJdbcTemplate.executeQueryForObject(sb.toString(),new Object[] {  }, rowMapperPlanDesc);
		return   ls ;
	}
	
 

}
