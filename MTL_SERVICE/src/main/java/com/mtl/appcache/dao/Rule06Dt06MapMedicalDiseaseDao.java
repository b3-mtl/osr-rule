package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.Dt06Uwqag;

@Repository
public class Rule06Dt06MapMedicalDiseaseDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, DT06MapMedicalDisease> getAllDoc() {
		StringBuilder sql  = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT  * ");
		sql.append(" FROM DT06_MAP_MEDICAL_DISEASE  ");
		sql.append(" WHERE 1=1");
		sql.append(" AND IS_DELETE != 'Y' ");
		sql.append(" ORDER BY PRIORITY ASC ");
		
		List<DT06MapMedicalDisease> ls = commonJdbcTemplate.executeQuery(sql.toString()
				, params.toArray()
				, new BeanPropertyRowMapper<>(DT06MapMedicalDisease.class)
			);
		System.out.println(" Get All DT06_MAP_MEDICAL_DISEASE.. Size: "+ls.size());
		return rewriteData(ls);

	}
	
	private HashMap< String ,  DT06MapMedicalDisease > rewriteData(List<DT06MapMedicalDisease> dataList){		
		HashMap< String ,  DT06MapMedicalDisease > mapReturn = new HashMap< String ,  DT06MapMedicalDisease> ();		
		for (DT06MapMedicalDisease item : dataList) {
			mapReturn.put(item.getCode(), item);
			   
		}
		return mapReturn;
	}
}
