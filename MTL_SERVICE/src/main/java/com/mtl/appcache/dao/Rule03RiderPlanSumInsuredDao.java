package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.Dt03BasicPlanSumInsured;

@Repository
public class Rule03RiderPlanSumInsuredDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03RiderPlanSumInsured> getAll() {
		List<DT03RiderPlanSumInsured> dataRes = new ArrayList<DT03RiderPlanSumInsured>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_RIDER_PLAN_SUM_INSURED ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03RiderPlanSumInsured.class));
		return dataRes;
	}
	
	
}
