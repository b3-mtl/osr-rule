package com.mtl.appcache.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.MsParameter;

@Repository
public class MsParameterDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(MsParameterDao.class);
	
	public HashMap< String , List<String>> getMsParameter(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_PARAMETER ");
	 
		List<MsParameter> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All MS_PARAMETER.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
	private RowMapper<MsParameter> rowMapper = new RowMapper<MsParameter>() {
		
		@Override
		public MsParameter mapRow(ResultSet rs, int arg1) throws SQLException {			
			MsParameter map = new MsParameter();	
			map.setKey(rs.getString("key"));
			map.setValue(rs.getString("value"));
			return map;
		}
		
	}; 
 
	private HashMap< String ,  List<String> > rewriteData(List<MsParameter> rs){		
		HashMap< String ,  List<String> > mapReturn = new HashMap< String ,  List<String>> ();
		List<String> temp = null;
		for (MsParameter  item : rs) {
			String key = item.getKey();
			String[] valueList = item.getValue().split(",");
			
			for(String value:valueList) {
				if(mapReturn.containsKey(key)) {
					temp = new ArrayList<String>();
					temp.addAll(mapReturn.get(key));
					temp.add(value.trim());
					mapReturn.replace(item.getKey(), temp);	
				}else {
					temp = new ArrayList<String>();
					temp.add(value.trim());
					mapReturn.put(item.getKey(), temp);				
				} 
			}
		}
		return mapReturn;
	}
}