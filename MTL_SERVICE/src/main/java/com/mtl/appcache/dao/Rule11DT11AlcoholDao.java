package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Alcohol;

@Repository
public class Rule11DT11AlcoholDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT11Alcohol> getAll() {
		List<DT11Alcohol> dataRes = new ArrayList<DT11Alcohol>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_ALCOHOL");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11Alcohol.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT11Alcohol> rewriteData(List<DT11Alcohol> dataMap){
		HashMap< String , DT11Alcohol>   alcohol= new HashMap< String , DT11Alcohol>();
		for(DT11Alcohol alcoholdata:dataMap)
		{
			String key = alcoholdata.getSex()+"|"+alcoholdata.getAlcoholType();
			alcohol.put(key, alcoholdata);
		}
		return alcohol;		
	}
}
