package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ChannelSumInsuredEqual;

@Repository
public class Rule03ChannelSumInsuredEqualDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ChannelSumInsuredEqual> getAll() {
		List<DT03ChannelSumInsuredEqual> dataRes = new ArrayList<DT03ChannelSumInsuredEqual>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_CHANNEL_SUMINSURED_EQUAL ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03ChannelSumInsuredEqual.class));
		return dataRes;
	}
	
}
