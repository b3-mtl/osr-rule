package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16FemaleDiseaseList;

@Repository
public class Rule16DT16FemaleDiseaseListDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT16FemaleDiseaseList> getAll() {
		List<DT16FemaleDiseaseList> dataRes = new ArrayList<DT16FemaleDiseaseList>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT16_FEMALE_DISEASE_LIST");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT16FemaleDiseaseList.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT16FemaleDiseaseList> rewriteData(List<DT16FemaleDiseaseList> dataMap){
		HashMap< String , DT16FemaleDiseaseList>   femaleDiseaseList = new HashMap< String , DT16FemaleDiseaseList>();
		dataMap.stream().parallel().forEach((data)->{
			femaleDiseaseList.put(data.getSymtonName(), data);
		});
		return femaleDiseaseList;		
	}
}
