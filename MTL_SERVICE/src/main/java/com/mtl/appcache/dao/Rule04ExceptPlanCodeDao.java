package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT04ExceptPlanCode;

@Repository
public class Rule04ExceptPlanCodeDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, String > findAll() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT04_EXCEPT_PLAN_CODE  WHERE IS_DELETE = 'N' ");
		List<DT04ExceptPlanCode> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT04ExceptPlanCode.class));
		System.out.println(" Get All DT04_EXCEPT_PLAN_CODE.. Size: "+ls.size());
		return rewriteData(ls);
	}

	
	private HashMap< String , String> rewriteData(List<DT04ExceptPlanCode> ls){
		HashMap< String , String> mapVal = new HashMap< String , String>();
		String[] planCodeLs  = null;
		for (DT04ExceptPlanCode item : ls) {
			planCodeLs = item.getPlanCode().trim().split(",");
			if(planCodeLs != null) {
				for (String key : planCodeLs) {
					mapVal.put(key, item.getResult());
				}
				
			}
			
		}
		return mapVal;
	}
}
