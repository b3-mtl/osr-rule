package com.mtl.appcache.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14PADocument;

@Repository
public class Rule14PADocumentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, List<DT14PADocument>> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_PA.* ");
		sb.append("  FROM DT14_PA_DOCUMENT DT_PA  WHERE IS_DELETE = 'N' ");
		List<DT14PADocument> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT14PADocument.class));
		return rewriteData(ls);
	}
	
	private HashMap< String , List<DT14PADocument>> rewriteData(List<DT14PADocument> ls){
		HashMap< String , List<DT14PADocument>> DT14PADocumentMap = new HashMap< String , List<DT14PADocument>>();
		String key ="PA_DOCUMENT";

		DT14PADocumentMap.put(key,ls);

		return DT14PADocumentMap;
	}
}
