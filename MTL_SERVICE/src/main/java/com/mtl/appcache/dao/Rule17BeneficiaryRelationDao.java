package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT17BeneficiaryRelation;

@Repository
public class Rule17BeneficiaryRelationDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , DT17BeneficiaryRelation> getBeneficiaryRelation(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from dt17_beneficiary_relation ");
	 
		List<DT17BeneficiaryRelation> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT17BeneficiaryRelation.class));
		System.out.println(" ###### Found Rule 17 dt17_beneficiary_relation List Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
 
	private HashMap< String , DT17BeneficiaryRelation> rewriteData(List<DT17BeneficiaryRelation> mapBeanList){		
		HashMap< String , DT17BeneficiaryRelation> mapReturn = new HashMap< String , DT17BeneficiaryRelation>();		
		for (DT17BeneficiaryRelation  item : mapBeanList) {
			String key = item.getRelationTh().trim();
			mapReturn.put(key, item); 
		}
		return mapReturn;
	}
}