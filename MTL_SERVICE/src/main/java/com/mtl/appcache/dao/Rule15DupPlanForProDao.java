package com.mtl.appcache.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT15DupPlanForPro;

@Repository
public class Rule15DupPlanForProDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT15DupPlanForPro> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT15_DUP_PLAN_FOR_PRO ");
	 
		List<DT15DupPlanForPro> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT15DupPlanForPro	.class));
		System.out.println(" Get All DT15_DUP_PLAN_FOR_PRO.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
	
}
