package com.mtl.appcache.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.common.AppNo;

@Repository
public class AppNoDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	 
	private static final Logger log = LogManager.getLogger(AppNoDao.class);
	
	public HashMap< String , AppNo> getAllAppNo(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_APP_NO  ");
		
		List<AppNo> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All MS_APP_NO.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
 
	private RowMapper<AppNo> rowMapper = new RowMapper<AppNo>() {		
		@Override
		public AppNo mapRow(ResultSet rs, int arg1) throws SQLException {			
			AppNo doc = new AppNo(); 
			doc.setAppFormNo(rs.getString("APP_FORM_NO"));
			doc.setAppFormNoKey(rs.getString("APP_FORM_NO_KEY"));
			doc.setChannelGroup(rs.getString("CHANNEL_GROUP"));
			doc.setDescription(rs.getString("DESCRIPTION"));
			doc.setIsExcept(rs.getString("IS_EXCEPT"));
			return doc;
		}
		
	};
	
	private HashMap< String , AppNo> rewriteData(List<AppNo> docListIn){		
		HashMap< String , AppNo> returnMap = new HashMap< String ,AppNo>();		
		String key ="";
		for (AppNo item : docListIn) {
			key = item.getAppFormNoKey();
			returnMap.put(key, item);
		}
 		return returnMap;
	}
	
}
