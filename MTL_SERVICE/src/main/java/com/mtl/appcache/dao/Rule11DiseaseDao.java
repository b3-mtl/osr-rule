package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Disease;

@Repository
public class Rule11DiseaseDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT11Disease> getAll() {
		List<DT11Disease> dataRes = new ArrayList<DT11Disease>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_DISEASE");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11Disease.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT11Disease> rewriteData(List<DT11Disease> dataMap){
		HashMap< String , DT11Disease> diseaseList = new HashMap< String , DT11Disease>();
		for(DT11Disease disease:dataMap)
		{
			String key = disease.getDiseaseName().trim()+"|"+disease.getMedical();
			diseaseList.put(key, disease);
		}
		return diseaseList;		
	}
}
