package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT04AppMdc;

@Repository
public class Rule04AppMdcDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT04AppMdc > getAllAppMdc() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT04_APP_MDC  WHERE IS_DELETE = 'N' ");
		List<DT04AppMdc> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT04AppMdc.class));
		System.out.println(" Get All DT04_APP_MDC.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , DT04AppMdc> rewriteData(List<DT04AppMdc> ls){
		HashMap< String , DT04AppMdc> mapVal = new HashMap< String , DT04AppMdc>();
		String key ="";
		
		for(DT04AppMdc item : ls) {
			String[] plancode;
			String[] premium;
			
			if(StringUtils.isNotBlank(item.getPlanCode())) {
				plancode = item.getPlanCode().split(",");
				
				for (String plan : plancode) {
					if(item.getSumInsuredBasic()!=null) {
						if(StringUtils.isNoneBlank(item.getPremiumPayment())) {
							premium = item.getPremiumPayment().split(",");
							
							for(String payment :premium) {
								key = plan.trim()+"|"+payment.trim();
								
								if(mapVal.containsKey(key)) {
									continue;
								}
								mapVal.put(key,item);
							}
						}else {
							key = plan.trim()+"|";
							
							if(mapVal.containsKey(key)) {
								continue;
							}
							mapVal.put(key,item);
						}
					}else {
						if(StringUtils.isNoneBlank(item.getPremiumPayment())) {
							premium = item.getPremiumPayment().split(",");
							
							for(String payment :premium) {
								key = plan.trim()+"|"+payment.trim();
								
								if(mapVal.containsKey(key)) {
									continue;
								}
								mapVal.put(key,item);
							}
						}else {
							key = plan.trim()+"|";
							
							if(mapVal.containsKey(key)) {
								continue;
							}
							mapVal.put(key,item);
						}
					}
				}
			}
		}
		return mapVal;
	}

}
