package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT07MIBHnw;

@Repository
public class Rule07Dt07MIBHnwDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT07MIBHnw > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT07_MIB_HNW ");
		sb.append(" ORDER BY CODE ASC ");
		List<DT07MIBHnw> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT07MIBHnw.class));
		System.out.println(" Get All DT07_MIB_HNW.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , DT07MIBHnw> rewriteData(List<DT07MIBHnw> ls){
		
		HashMap< String , DT07MIBHnw> mapVal = new HashMap< String , DT07MIBHnw>();
		String key ="";
		for ( DT07MIBHnw item : ls) {
			key = item.getCode();
			mapVal.put(key, item);
		}
		return mapVal;
	}
}
