package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT07MIB;

@Repository
public class Rule07Dt07MIBDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT07MIB > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT07_MIB ");
		sb.append(" ORDER BY CODE ASC ");
		List<DT07MIB> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT07MIB.class));
		System.out.println(" Get All DT07_MIB.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , DT07MIB> rewriteData(List<DT07MIB> ls){
		
		HashMap< String , DT07MIB> mapVal = new HashMap< String , DT07MIB>();
		String key ="";
		for ( DT07MIB item : ls) {
			key = item.getCode();
			mapVal.put(key, item);
		}
		return mapVal;
	}
}
