package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16MibHighRisk;

@Repository
public class Rule16DT16MibHighRiskDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT16MibHighRisk> getAll() {
		List<DT16MibHighRisk> dataRes = new ArrayList<DT16MibHighRisk>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT16_MIB_HIGH_RISK");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT16MibHighRisk.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT16MibHighRisk> rewriteData(List<DT16MibHighRisk> dataMap){
		HashMap< String , DT16MibHighRisk>   dt16MibHighRisk= new HashMap< String , DT16MibHighRisk>();
		dataMap.stream().parallel().forEach((data)->{
			dt16MibHighRisk.put(data.getCode(),data);
		});
		return dt16MibHighRisk;		
	}
}
