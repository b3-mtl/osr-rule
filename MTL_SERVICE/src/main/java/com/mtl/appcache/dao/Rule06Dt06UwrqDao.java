package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT06UWRQ;

@Repository
public class Rule06Dt06UwrqDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, List<DT06UWRQ> > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT06_UWRQ  WHERE IS_DELETE = 'N' ");
		sb.append(" ORDER BY CLIENT_LOCATION_CODE ASC ,AGENT_QUALIFICATION_UWRQ ASC ");
		List<DT06UWRQ> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT06UWRQ.class));
		System.out.println(" Get All DT06_UWRQ.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , List<DT06UWRQ>> rewriteData(List<DT06UWRQ> ls){
		HashMap< String , List<DT06UWRQ>> dt06UwqagMap = new HashMap< String , List<DT06UWRQ>>();
		String key ="", oldKey = "";
		List<DT06UWRQ> listItem = null;
		for ( DT06UWRQ item : ls) {
			key = item.getClientLocationCode()+"|"+item.getAgentQualificationUwrq();
			if(!key.equals(oldKey)) {
				if(listItem != null && listItem.size() > 0) {
					dt06UwqagMap.put(oldKey, listItem);
				}
				listItem = new ArrayList<DT06UWRQ>();
			}
			oldKey = key;
			listItem.add(item);
		}
		
		if(listItem.size() > 0) {
			dt06UwqagMap.put(key, listItem);
		}
		return dt06UwqagMap;
	}

}
