package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03SumInsuredWithMustHaveRider;

@Repository
public class Rule03SumInsuredWithMustHaveRiderDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03SumInsuredWithMustHaveRider> getAll() {
		List<DT03SumInsuredWithMustHaveRider> dataRes = new ArrayList<DT03SumInsuredWithMustHaveRider>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_SUM_INSURED_WITH_MUST_HAV ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03SumInsuredWithMustHaveRider.class));
		return dataRes;
	}
	
	
}
