package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13AmloPilot;

@Repository
public class Rule13Dt13AmloPilotDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT13AmloPilot > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT13_AMLO_PILOT ");
		List<DT13AmloPilot> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT13AmloPilot.class));
		System.out.println(" Get All DT13_AMLO_PILOT.. Size: "+ls.size());
		return rewriteData(ls);
	}
	
	private HashMap< String , DT13AmloPilot> rewriteData(List<DT13AmloPilot> ls){
		HashMap< String , DT13AmloPilot> mapVal = new HashMap< String , DT13AmloPilot>();
		String key ="";
		for ( DT13AmloPilot item : ls) {
			key = item.getOccupationCode();
			mapVal.put(key, item);
		}
		return mapVal;
	}
	
	
}
