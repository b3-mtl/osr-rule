package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.Dt06Uwqag;

@Repository
public class Rule06Dt06UwqagDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, List<Dt06Uwqag>> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_UW.* ");
		sb.append("  FROM DT06_UWQAG DT_UW  WHERE IS_DELETE = 'N' ");
		List<Dt06Uwqag> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(Dt06Uwqag.class));
		System.out.println(" Get All DT06_UWQAG.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , List<Dt06Uwqag>> rewriteData(List<Dt06Uwqag> ls){
		HashMap< String , List<Dt06Uwqag>> dt06UwqagMap = new HashMap< String , List<Dt06Uwqag>>();
		String key ="";
		for ( Dt06Uwqag item : ls) {
			key = "";
			String[] plancode;
			if(StringUtils.isNotBlank(item.getPlanCode())) {
				
				plancode = item.getPlanCode().split(",");
				for (String plan : plancode) {
					key = plan;
					if(StringUtils.isNoneBlank(item.getChannelCode())) {
						key += "|"+item.getChannelCode();
					}else {
						key += "|";
					}
					if(StringUtils.isNoneBlank(item.getAgentQualification())) {
						key += "|"+item.getAgentQualification();
					}else {
						key += "|";
					}
					List<Dt06Uwqag> value = new ArrayList<Dt06Uwqag>();
					if(dt06UwqagMap.containsKey(key)) {
						value.addAll(dt06UwqagMap.get(key));
						value.add(item);
					}else {
						value.add(item);
					}
					dt06UwqagMap.put(key,value);
				}
			}
		}
		return dt06UwqagMap;
	}


}
