package com.mtl.appcache.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT15DupPlanForClient;

@Repository
public class Rule15DupPlanForClientDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT15DupPlanForClient> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT15_DUPLICATE_PLAN_FOR_CLIENT ");
	 
		List<DT15DupPlanForClient> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT15DupPlanForClient	.class));
		System.out.println(" Get All DT15_DUPLICATE_PLAN_FOR_CLIENT.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
	
}
