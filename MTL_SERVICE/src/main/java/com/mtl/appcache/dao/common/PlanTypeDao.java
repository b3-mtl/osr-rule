package com.mtl.appcache.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.common.PlanType;

@Repository
public class PlanTypeDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	 
	private static final Logger log = LogManager.getLogger(PlanTypeDao.class);
	
	public HashMap< String , PlanType> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM PLAN_TYPE  ");
		
		List<PlanType> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All PLAN_TYPE.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
	private RowMapper<PlanType> rowMapper = new RowMapper<PlanType>() {		
		@Override
		public PlanType mapRow(ResultSet rs, int arg1) throws SQLException {			
			PlanType item = new PlanType(); 
			
			item.setPlanCode(rs.getString("PLAN_CODE"));
			item.setPlanType(rs.getString("PLAN_TYPE"));
 
			return item;
		}
		
	};
	
	private HashMap< String , PlanType> rewriteData(List<PlanType> list){		
		HashMap< String , PlanType> returnMap = new HashMap< String ,PlanType>();		
		for (PlanType item : list) {
			String key = item.getPlanCode();
			returnMap.put(key, item);
		}
 		return returnMap;
	}
	
	
	
}
