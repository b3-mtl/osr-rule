package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ChannelSumInsured;
import com.mtl.rule.model.Dt03DisorderAndAccident;

@Repository
public class Rule03ChannelSumInsuredDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ChannelSumInsured> getAll() {
		List<DT03ChannelSumInsured> dataRes = new ArrayList<DT03ChannelSumInsured>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_CHANNEL_SUM_INSURED ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03ChannelSumInsured.class));
		return dataRes;
	}
	
}
