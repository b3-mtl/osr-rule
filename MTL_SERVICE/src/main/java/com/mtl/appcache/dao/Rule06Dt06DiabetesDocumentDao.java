package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT06DiabetesDocument;
import com.mtl.rule.model.DT06UWRQ;

@Repository
public class Rule06Dt06DiabetesDocumentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, List<DT06DiabetesDocument> > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT06_DIABETES_DOCUMENT  WHERE IS_DELETE = 'N' ");
		sb.append(" ORDER BY PLAN_TYPE ASC ");
		List<DT06DiabetesDocument> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT06DiabetesDocument.class));
		System.out.println(" Get All DT06_DIABETES_DOCUMENT.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , List<DT06DiabetesDocument>> rewriteData(List<DT06DiabetesDocument> ls){
		HashMap< String , List<DT06DiabetesDocument>> mapVal = new HashMap< String , List<DT06DiabetesDocument>>();
		String key ="", oldKey = "";
		List<DT06DiabetesDocument> listItem = null;
		for ( DT06DiabetesDocument item : ls) {
			key = item.getPlanType();
			if(!key.equals(oldKey)) {
				if(listItem != null && listItem.size() > 0) {
					mapVal.put(oldKey, listItem);
				}
				listItem = new ArrayList<DT06DiabetesDocument>();
			}
			oldKey = key;
			listItem.add(item);
		}
		
		if(listItem.size() > 0) {
			mapVal.put(key, listItem);
		}
		return mapVal;
	}

}
