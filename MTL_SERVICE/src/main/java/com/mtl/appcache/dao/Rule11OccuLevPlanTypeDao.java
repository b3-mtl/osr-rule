package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanType;

@Repository
public class Rule11OccuLevPlanTypeDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT11OccuLevPlanType> getAll() {
		List<DT11OccuLevPlanType> dataRes = new ArrayList<DT11OccuLevPlanType>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_OCCU_LEV_PLAN_TYPE");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11OccuLevPlanType.class));
		return dataRes;
	}
	
}
