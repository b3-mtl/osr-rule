package com.mtl.appcache.dao.common;

import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.MsAgentInfo;

@Repository
public class MsAgentInfoDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(MsAgentInfoDao.class);
	
	public HashMap< String , MsAgentInfo> getResult(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_AGENT_INFO ");
		
		List<MsAgentInfo> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  },new BeanPropertyRowMapper<>(MsAgentInfo.class));
		log.info(" Get All MS_AGENT_INFO.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
	
	private HashMap< String , MsAgentInfo> rewriteData(List<MsAgentInfo> angeLs){
		
		HashMap< String , MsAgentInfo> hasmapVal = new HashMap< String , MsAgentInfo>();
		
		String key ="";
		for (MsAgentInfo item : angeLs) {
			key = item.getAgCodeNumber();
			hasmapVal.put(key, item);
		}
		return hasmapVal;
	}
}
