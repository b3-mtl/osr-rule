package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ExtracareHbSuminsured;

@Repository
public class Rule03ExtracareHbSuminsuredDao {
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ExtracareHbSuminsured> getAll() {
		List<DT03ExtracareHbSuminsured> dataRes = new ArrayList<DT03ExtracareHbSuminsured>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_EXTRACARE_HB_SUMINSURED ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03ExtracareHbSuminsured.class));
		return dataRes;
	}
	
}
