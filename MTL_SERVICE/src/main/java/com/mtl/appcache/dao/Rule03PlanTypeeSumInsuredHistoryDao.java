package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03PlanTypeeSumInsuredHistory;

@Repository
public class Rule03PlanTypeeSumInsuredHistoryDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03PlanTypeeSumInsuredHistory> getAll() {
		List<DT03PlanTypeeSumInsuredHistory> dataRes = new ArrayList<DT03PlanTypeeSumInsuredHistory>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_PLAN_TYPE_SUMIN_HISTORY ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03PlanTypeeSumInsuredHistory.class));
		return dataRes;
	}
	
}
