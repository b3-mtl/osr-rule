package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.rule.PayorOccupation;

@Repository
public class Rule16PayorOccupationDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , PayorOccupation> getOccupationMap(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from dt16_occupation ");
	 
		List<PayorOccupation> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		System.out.println(" ###### Found Rule 16 dt16_payor_occupation List Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
	private RowMapper<PayorOccupation> rowMapper = new RowMapper<PayorOccupation>() {
		/*
		OCCUPATION_CODE
		PB
		REFER_UNDERWRITER
		OCCUPATION_CODE_AMLO
		RULE_MESSAGE_CODE_DESC
			 */
		@Override
		public PayorOccupation mapRow(ResultSet rs, int arg1) throws SQLException {			
			PayorOccupation map = new PayorOccupation();	 
			map.setOccupationCode(rs.getString("OCCUPATION_CODE"));
			map.setPb(rs.getString("PB"));
			map.setReferUnderwriter(rs.getString("REFER_UNDERWRITER"));
			map.setOccupationCodeAmlo(rs.getString("OCCUPATION_CODE_AMLO"));
			map.setRuleMessageCode(rs.getString("RULE_MESSAGE_CODE_DESC"));
			return map;
		}
		
	};
	
 
	private HashMap< String , PayorOccupation> rewriteData(List<PayorOccupation> mapBeanList){		
		HashMap< String , PayorOccupation> mapReturn = new HashMap< String , PayorOccupation>();		
		for (PayorOccupation  item : mapBeanList) {
			mapReturn.put(item.getOccupationCode(), item); 
		}
		return mapReturn;
	}
}