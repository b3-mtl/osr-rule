package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuSuminsured;
import com.mtl.rule.model.DT11OcuupationAccident;

@Repository
public class Rule11OccupationAccidentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT11OcuupationAccident> getAll() {
		List<DT11OcuupationAccident> dataRes = new ArrayList<DT11OcuupationAccident>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_OCCUPATION_ACCIDENT ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11OcuupationAccident.class));
		return dataRes;
	}
	
}
