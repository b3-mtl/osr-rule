package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13RiskOccupation;

@Repository
public class Rule13Dt13RiskOccupationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT13RiskOccupation > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT13_RISK_OCCUPATION ");
		List<DT13RiskOccupation> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT13RiskOccupation.class));
		System.out.println(" Get All DT13_RISK_OCCUPATION.. Size: "+ls.size());
		return rewriteData(ls);
	}
	
	private HashMap< String , DT13RiskOccupation> rewriteData(List<DT13RiskOccupation> ls){
		HashMap< String , DT13RiskOccupation> mapVal = new HashMap< String , DT13RiskOccupation>();
		String key ="";
		for ( DT13RiskOccupation item : ls) {
			key = item.getOccupationCode();
			if(item.getDocumentCode() == null) {
				continue;
			}else {
				mapVal.put(key, item);				
			}
		}
		return mapVal;
	}
	
}
