package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.DT03RiskDiagnosisPlanType;
import com.mtl.rule.model.MapProject;

@Repository
public class MapProjectDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<MapProject> getAll() {
		List<MapProject> dataRes = new ArrayList<MapProject>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM MAP_PROJECT ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(MapProject.class));
		return dataRes;
	}
	
	
}
