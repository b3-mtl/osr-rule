package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.Dt06MapNmlType;
import com.mtl.rule.model.Dt06Uwqag;

@Repository
public class Rule06Dt06MapNmlTypeDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, Dt06MapNmlType> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_MNT.TC_TYPE AS KEYWORD, DT_MNT.*  ");
		sb.append(" FROM DT06_MAP_NML_TYPE DT_MNT  ");
		sb.append(" WHERE dt_mnt.is_delete = 'N' ");
		List<Dt06MapNmlType> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(Dt06MapNmlType.class));
		System.out.println(" Get All DT06_MAP_NML_TYPE.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , Dt06MapNmlType> rewriteData(List<Dt06MapNmlType> ls){
		HashMap< String , Dt06MapNmlType> dt06UwqagMap = new HashMap< String , Dt06MapNmlType>();
		String key ="";
		for ( Dt06MapNmlType item : ls) {
			key = "";
			if(StringUtils.isNotBlank(item.getTcType())) {
				key = item.getTcType();
			}
			if(StringUtils.isNotBlank(item.getBRANCH())) {
				key += "|"+item.getBRANCH();
			}else {
				key += "|";
			}
			dt06UwqagMap.put(key,item);
		}
		return dt06UwqagMap;
	}

}