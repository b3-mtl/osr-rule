package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03AccidentForTakaful;
import com.mtl.rule.model.DT03AccidentForTakafulNc;
import com.mtl.rule.model.DT03DHealthAgentGroup;
import com.mtl.rule.model.MapProject;

@Repository
public class Rule03DHealthAgentGroupDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03DHealthAgentGroup> getAll() {
		List<DT03DHealthAgentGroup> dataRes = new ArrayList<DT03DHealthAgentGroup>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_D_HEALTH_AGENTGROUP ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03DHealthAgentGroup.class));
		return dataRes;
	}
	
}

