package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ProjectSumIndured;

@Repository
public class Rule03ProjectSumInduredDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ProjectSumIndured> getAll() {
		List<DT03ProjectSumIndured> dataRes = new ArrayList<DT03ProjectSumIndured>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_PROJECT_SUM_INSURED ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03ProjectSumIndured.class));
		return dataRes;
	}
	
	
}
