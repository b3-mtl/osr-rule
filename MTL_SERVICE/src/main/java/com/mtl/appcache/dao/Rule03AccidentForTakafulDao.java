package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03AccidentForTakaful;
import com.mtl.rule.model.DT03AccidentForTakafulNc;
import com.mtl.rule.model.MapProject;

@Repository
public class Rule03AccidentForTakafulDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03AccidentForTakaful> getAll() {
		List<DT03AccidentForTakaful> dataRes = new ArrayList<DT03AccidentForTakaful>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_ACCIDENT_FOR_TAKAFUL ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03AccidentForTakaful.class));
		return dataRes;
	}
	
}
