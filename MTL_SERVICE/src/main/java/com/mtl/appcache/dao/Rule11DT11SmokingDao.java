package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Smoke;

@Repository
public class Rule11DT11SmokingDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	
	public List<DT11Smoke> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append("select * from DT11_SMOKING ORDER BY SMOKE_PER_DAY + 0 DESC");
	 
		List<DT11Smoke> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT11Smoke.class));
		System.out.println(" ###### Found Rule 21 DT11_SMOKING List Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
}
