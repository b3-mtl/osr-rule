package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.DT03RiskDiagnosisPlanType;

@Repository
public class Rule03RiskDiagnosisPlanTypeDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03RiskDiagnosisPlanType> getAll() {
		List<DT03RiskDiagnosisPlanType> dataRes = new ArrayList<DT03RiskDiagnosisPlanType>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_RISK_DIAGNOSIS_PLAN_TYPE ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03RiskDiagnosisPlanType.class));
		return dataRes;
	}
	
	
}
