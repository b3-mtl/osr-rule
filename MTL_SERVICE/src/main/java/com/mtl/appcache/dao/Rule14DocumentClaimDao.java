package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14DocumentClaim;
import com.mtl.rule.util.RuleConstants;

@Repository
public class Rule14DocumentClaimDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, List<DT14DocumentClaim>> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_EX.* ");
		sb.append("  FROM DT14_DOCUMENT_CLAIM DT_EX  WHERE IS_DELETE = 'N' ORDER BY CLAIM_CATEGORY ASC ");
		List<DT14DocumentClaim> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT14DocumentClaim.class));

		
		return rewriteData(ls);
	}
	

	private HashMap< String , List<DT14DocumentClaim>> rewriteData(List<DT14DocumentClaim> ls){
		HashMap< String , List<DT14DocumentClaim>> dT14ExceptionClaimMap = new HashMap< String , List<DT14DocumentClaim>>();
		List<DT14DocumentClaim> temp = null;
		String keyCate ="" ,keyItem ="";
		for ( DT14DocumentClaim item : ls) {
			keyCate = item.getClaimCategory();
			keyItem ="";
			if(RuleConstants.RULE_14.APP_TYPE.equals(item.getClaimCategory())) {
				if(StringUtils.isNotBlank(item.getAppType())) {
					String[] appType = item.getAppType().split(",");
					for(int i =0; i < appType.length; i++ ) {
						keyItem = keyCate+"|"+appType[i];
						
						if(dT14ExceptionClaimMap.containsKey(keyItem)) {
							temp = dT14ExceptionClaimMap.get(keyItem);
						}else {
							temp = new ArrayList<DT14DocumentClaim>();
						}
						temp.add(item);
						dT14ExceptionClaimMap.put(keyItem,temp);
					}
				}else {
					keyItem = keyCate+"|";
					temp = new ArrayList<DT14DocumentClaim>();
					temp.add(item);
					dT14ExceptionClaimMap.put(keyItem,temp);
				}
			} else if(RuleConstants.RULE_14.PLAN_CODE.equals(item.getClaimCategory())) {
				if(StringUtils.isNotBlank(item.getPlanCode())) {
					String[] itemArray = item.getPlanCode().split(",");
					for(int i =0; i < itemArray.length; i++ ) {
						keyItem = keyCate+"|"+itemArray[i];
						if(dT14ExceptionClaimMap.containsKey(keyItem)) {
							temp = dT14ExceptionClaimMap.get(keyItem);
						}else {
							temp = new ArrayList<DT14DocumentClaim>();
						}
						temp.add(item);
						dT14ExceptionClaimMap.put(keyItem,temp);
					}
				}else {
					keyItem = keyCate+"|";
					temp = new ArrayList<DT14DocumentClaim>();
					temp.add(item);
					dT14ExceptionClaimMap.put(keyItem,temp);
				}
			} else if(RuleConstants.RULE_14.PLAN_TYPE.equals(item.getClaimCategory())) {
				if(StringUtils.isNotBlank(item.getPlanType())) {
					String[] itemArray = item.getPlanType().split(",");
					for(int i =0; i < itemArray.length; i++ ) {
						keyItem = keyCate+"|"+itemArray[i];
						if(dT14ExceptionClaimMap.containsKey(keyItem)) {
							temp = dT14ExceptionClaimMap.get(keyItem);
						}else {
							temp = new ArrayList<DT14DocumentClaim>();
						}
						temp.add(item);
						
						dT14ExceptionClaimMap.put(keyItem,temp);
					}
				}else {
					keyItem = keyCate+"|";
					temp = new ArrayList<DT14DocumentClaim>();
					temp.add(item);
					dT14ExceptionClaimMap.put(keyItem,temp);
				}
			} else if(RuleConstants.RULE_14.GROUP_CHANNEL.equals(item.getClaimCategory())) {
				if(StringUtils.isNotBlank(item.getGroupChannel())) {
					String[] itemArray = item.getGroupChannel().split(",");
					for(int i =0; i < itemArray.length; i++ ) {
						keyItem = keyCate+"|"+itemArray[i];
						if(dT14ExceptionClaimMap.containsKey(keyItem)) {
							temp = dT14ExceptionClaimMap.get(keyItem);
						}else {
							temp = new ArrayList<DT14DocumentClaim>();
						}
						temp.add(item);
						dT14ExceptionClaimMap.put(keyItem,temp);
					}
				}else {
					keyItem = keyCate+"|";
					temp = new ArrayList<DT14DocumentClaim>();
					temp.add(item);
					dT14ExceptionClaimMap.put(keyItem,temp);
				}
			}
		}
		
		List<DT14DocumentClaim> testls = dT14ExceptionClaimMap.get("PLAN_TYPE|AA");
		for (DT14DocumentClaim itemtest : testls) {
			System.out.println("CLAIM_CATEGORY ="+itemtest.getClaimCategory());
			System.out.println("APP_TYPE ="+itemtest.getAppType());
			System.out.println("CLAIM_LIST ="+itemtest.getClaimList());
		}
	
		return dT14ExceptionClaimMap;
	}
	
//	private HashMap< String , List<DT14ExceptionClaim>> rewriteData(List<DT14ExceptionClaim> ls){
//		HashMap< String , List<DT14ExceptionClaim>> DT14ExceptionClaimMap = new HashMap< String , List<DT14ExceptionClaim>>();
//		List<DT14ExceptionClaim> temp = null;
//		String key ="";
//		for ( DT14ExceptionClaim item : ls) {
//			key = item.getClaimCategory();                        
//			if(StringUtils.isNotBlank(item.getPlanCode())) {
//				
//				String[] planCode =
//				for (String plan : planCode) {
//					
//					key = key+"|"+plan;
//					// check dupp key ?
//					if(DT14ExceptionClaimMap.containsKey(key)) {
//						temp = DT14ExceptionClaimMap.get(key); //get old value
//						temp.add(item); //add new value
//						DT14ExceptionClaimMap.remove(key);
//						DT14ExceptionClaimMap.put(key,temp);
//					}else {
//						temp = new ArrayList<DT14ExceptionClaim>();
//						DT14ExceptionClaimMap.put(key,temp);
//					}
//				}
//				
//			}
//			if(StringUtils.isNotBlank(item.getAppType())) {
//				
//				String[] appType = item.getAppType().split(",");
//				for (String type : appType) {
//					temp = new ArrayList<DT14ExceptionClaim>();
//					temp.add(item);
//					key = key+"|"+type;
//					DT14ExceptionClaimMap.put(key,temp);
//				}
//			}
//			if(StringUtils.isNotBlank(item.getGroupChannel())) {
//				temp = new ArrayList<DT14ExceptionClaim>();
//				temp.add(item);
//				key = key+"|"+item.getGroupChannel();
//				DT14ExceptionClaimMap.put(key,temp);
//			}
//			if(StringUtils.isNotBlank(item.getPlanType())) {
//				
//				String[] planType = item.getPlanType().split(",");
//				for (String type : planType) {
//					temp = new ArrayList<DT14ExceptionClaim>();
//					temp.add(item);
//					key = key+"|"+type;
//					DT14ExceptionClaimMap.put(key,temp);
//				}
//			}
//		}
//		
//		return DT14ExceptionClaimMap;
//	}
	
}
