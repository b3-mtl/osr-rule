package com.mtl.appcache.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.appcache.dao.common.PlanTypeDao;
import com.mtl.model.common.PlanType;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanAcc;
import com.mtl.rule.model.MsInvesmentPlanType;

@Repository
public class MsInvesmentPlanTypeDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(MsInvesmentPlanTypeDao.class);
	
	
//	public List<MsInvesmentPlanType> getAll() {
//		List<MsInvesmentPlanType> dataRes = new ArrayList<MsInvesmentPlanType>();
//		StringBuilder sqlBuilder = new StringBuilder();
//		sqlBuilder.append(" SELECT * FROM MS_INVESTMENT_PLAN_TYPE");
//		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(MsInvesmentPlanType.class));
//		return dataRes;
//	}
	
	public HashMap< String , MsInvesmentPlanType> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_INVESTMENT_PLAN_TYPE  ");
		
		List<MsInvesmentPlanType> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All PLAN_TYPE.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
	private RowMapper<MsInvesmentPlanType> rowMapper = new RowMapper<MsInvesmentPlanType>() {		
		@Override
		public MsInvesmentPlanType mapRow(ResultSet rs, int arg1) throws SQLException {			
			MsInvesmentPlanType item = new MsInvesmentPlanType(); 
			
			item.setPlanCode(rs.getString("PLAN_CODE"));
			item.setInvestmentType(rs.getString("INVESTMENT_TYPE"));
			item.setSystemName(rs.getString("SYSTEM_NAME"));
			return item;
		}
		
	};
	
	private HashMap< String , MsInvesmentPlanType> rewriteData(List<MsInvesmentPlanType> list){		
		HashMap< String , MsInvesmentPlanType> returnMap = new HashMap< String ,MsInvesmentPlanType>();		
		for (MsInvesmentPlanType item : list) {
			String key = item.getPlanCode();
			returnMap.put(key, item);
		}
 		return returnMap;
	}
	
}
