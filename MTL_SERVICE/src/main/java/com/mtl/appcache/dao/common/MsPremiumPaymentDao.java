package com.mtl.appcache.dao.common;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.appcache.dao.MapBean;
import com.mtl.rule.model.MsPremiumPayment;

@Repository
public class MsPremiumPaymentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(MsPremiumPaymentDao.class);

	public HashMap<String, List<String> > findAll() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM MS_PREMIUM_PAYMENT ");
		
		List<MapBean> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMappePremiumPayment);
		log.info(" Get All MS_PREMIUM_PAYMENT.. Size:"+ls.size());
		return rewriteData(ls);
		
	}
	
	private RowMapper<MapBean> rowMappePremiumPayment = new RowMapper<MapBean>() {
		
		@Override
		public MapBean mapRow(ResultSet rs, int arg1) throws SQLException {			
			MapBean map = new MapBean();
			String key = rs.getString("PLAN_CODE")+"@"+rs.getString("CHANNEL_CODE")+"@"+rs.getString("PREMIUM_PAYMENT");
			
			map.setKeystr(key);
			map.setValueStr("true");
			return map;
		}
		
	};
	
	private HashMap< String , List<String>> rewriteData(List<MapBean> ls){
		HashMap< String , List<String>> mapVal = new HashMap< String , List<String>>();
		
		List<String> tempList = null;
		String key ="";
		for(MapBean item : ls) {
			key = item.getKeystr();
			String[] temp = key.split("@",3);
			String planCode = temp[0];
			String tempChannelCode = temp[1];
			String tempPremiumPayment = temp[2];
			String[] channelCode;
			String[] premiumPayment;
			
			if(StringUtils.isNotBlank(tempChannelCode)) {
				channelCode = tempChannelCode.split(",");
				
				for (String code : channelCode) {
					key = planCode+"|"+code;
					
					if(StringUtils.isNoneBlank(tempPremiumPayment)) {
						premiumPayment = tempPremiumPayment.split(",");
						tempList = new ArrayList<String>();
						for(String premium :premiumPayment) {
							tempList.add(premium.trim());
						}
						mapVal.put(key,tempList);
					}else {
						tempList = new ArrayList<String>();
						tempList.add("");
						mapVal.put(key,tempList);
					}
				}
			}
		}
		return mapVal;
	}
}
