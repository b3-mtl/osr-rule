package com.mtl.appcache.dao.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.MsSuperVIP;

@Repository
public class MsSuperVip {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	private static final Logger log = LogManager.getLogger(MsSuperVip.class);

	public HashMap<String,MsSuperVIP> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM MS_SUPER_VIP");
		
		List<MsSuperVIP> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {},
				new BeanPropertyRowMapper<>(MsSuperVIP.class));
		log.info(" Get All MS_SUPER_VIP.. Size:"+ls.size());
		return rewriteData(ls);
	}

	private HashMap<String,MsSuperVIP> rewriteData(List<MsSuperVIP> ls) {
		HashMap<String,MsSuperVIP> DT16MsSuperVipMap = new HashMap<String,MsSuperVIP>();
		ls.forEach((data)->{
			DT16MsSuperVipMap.put(data.getCscli()+"",data);
		});
		return DT16MsSuperVipMap;
	}
}
