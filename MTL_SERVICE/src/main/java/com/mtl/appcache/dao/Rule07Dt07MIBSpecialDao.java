package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT07MIBSpecial;

@Repository
public class Rule07Dt07MIBSpecialDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT07MIBSpecial > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT07_MIB_SPECIAL ");
		sb.append(" ORDER BY CODE ASC ");
		List<DT07MIBSpecial> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT07MIBSpecial.class));
		System.out.println(" Get All DT07_MIB_SPECIAL.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , DT07MIBSpecial> rewriteData(List<DT07MIBSpecial> ls){
		HashMap< String , DT07MIBSpecial> mapVal = new HashMap< String , DT07MIBSpecial>();
		String key ="";
		for ( DT07MIBSpecial item : ls) {
			key = item.getCode();
			mapVal.put(key, item);
		}
		return mapVal;
	}
}
