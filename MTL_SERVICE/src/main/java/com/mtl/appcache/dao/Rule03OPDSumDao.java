package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03OPDSum;

@Repository
public class Rule03OPDSumDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , DT03OPDSum> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_OPD_SUM ");
	 
		List<DT03OPDSum> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03OPDSum.class));
		System.out.println(" Get All DT03_OPD_SUM.. Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
 
	private HashMap< String ,  DT03OPDSum > rewriteData(List<DT03OPDSum> list){		
		HashMap< String ,  DT03OPDSum > mapReturn = new HashMap< String ,  DT03OPDSum> ();		
		for (DT03OPDSum  item : list) {
			
			String[] channelCode = item.getChannelCode().split(",");
			for(String code:channelCode) {
				mapReturn.put(code+"|"+item.getSystemRegisted() , item);
			}
		}
		return mapReturn;
	}
}