package com.mtl.appcache.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT15DupPackagePlan;

@Repository
public class Rule15DupPackagePlanDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT15DupPackagePlan> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT15_DUPLICATE_PACKAGE_PLAN ");
	 
		List<DT15DupPackagePlan> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT15DupPackagePlan.class));
		System.out.println(" Get All DT15_DUPLICATE_PACKAGE_PLAN.. Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}
	
}
