package com.mtl.appcache.dao;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT04PremiumPaymentSpecial;

@Repository
public class Rule04PremiumPaymentSpecialDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT04PremiumPaymentSpecial > getAllPremiumPaymentSpecial() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");
		sb.append("  FROM DT04_PREMIUM_PAYMENT_SPECIAL WHERE IS_DELETE = 'N' ");
		List<DT04PremiumPaymentSpecial> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT04PremiumPaymentSpecial.class));
		System.out.println(" Get All DT04_PREMIUM_PAYMENT_SPECIAL.. Size: "+ls.size());
		return rewriteData(ls);
	}

	private HashMap< String , DT04PremiumPaymentSpecial> rewriteData(List<DT04PremiumPaymentSpecial> ls){
		HashMap< String , DT04PremiumPaymentSpecial> mapVal = new HashMap< String , DT04PremiumPaymentSpecial>();
		String key ="";
		DT04PremiumPaymentSpecial value = null;
		for(DT04PremiumPaymentSpecial item : ls) {
			String planCode = item.getPlanCode();
			String[] premium = item.getPremiumPayment().split(",");
			for(String payment : premium) {
				key = planCode+"|"+payment;
				if(mapVal.containsKey(key)) {
					continue;
				}
				value = new DT04PremiumPaymentSpecial();
				value.setPlanCode(planCode);
				value.setPremiumPayment(payment);
				mapVal.put(key,value);
			}
		}
		return mapVal;
	}

}
