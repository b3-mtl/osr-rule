package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;

@Repository
public class Rule05Dt05SpecialPaymentDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , Set<String>> getSpecialPaymentChannel(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select plan_code,not_in_channel from dt05_special_payment ");
	 
		List<MapBean> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		System.out.println(" Get All DT05_SPECIAL_PAYMENT.. Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
	private RowMapper<MapBean> rowMapper = new RowMapper<MapBean>() {
		
		@Override
		public MapBean mapRow(ResultSet rs, int arg1) throws SQLException {			
			MapBean map = new MapBean();	
			map.setKeystr(rs.getString("plan_code"));
			map.setValueStr(rs.getString("not_in_channel"));	
			//System.out.println(rs.getString("plan_code")+":"+rs.getString("not_in_channel"));
			return map;
		}
		
	}; 
 
	private HashMap< String , Set<String>> rewriteData(List<MapBean> mapBeanList){		
		HashMap< String , Set<String>> mapReturn = new HashMap< String , Set<String>>();		
		for (MapBean  item : mapBeanList) {
			Set<String> currentSetString =mapReturn.get(item.getKeystr());
			if(currentSetString==null) {
				currentSetString =  new HashSet<String>();				
				String rowSetString =  item.getValueStr();
				
				if(rowSetString!=null) {
					StringTokenizer st = new StringTokenizer(rowSetString,",");  
				     while (st.hasMoreTokens()) {  
				        // System.out.println(st.nextToken());  
				         currentSetString.add(st.nextToken());
				     }  				
					mapReturn.put(item.getKeystr(), currentSetString);					
				}
 
			}else {
				
				String rowSetString =  item.getValueStr();
				if(rowSetString!=null) {
				StringTokenizer st = new StringTokenizer(rowSetString,",");  
			     while (st.hasMoreTokens()) {  
			        // System.out.println(st.nextToken());  
			         currentSetString.add(st.nextToken());
			     }  
				}
				
			}
			   
		}
		return mapReturn;
	}
	
	
 
}
