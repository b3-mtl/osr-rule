package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14HnwDocument;

@Repository
public class Rule14HnwDocumentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap<String, List<DT14HnwDocument>> getCollector() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DT_HNW.* ");
		sb.append("  FROM DT14_HNW_DOCUMENT DT_HNW  WHERE IS_DELETE = 'N' ");
		List<DT14HnwDocument> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT14HnwDocument.class));
		return rewriteData(ls);
	}
	
	private HashMap< String , List<DT14HnwDocument>> rewriteData(List<DT14HnwDocument> ls){
		HashMap< String , List<DT14HnwDocument>> DT14PADocumentMap = new HashMap< String , List<DT14HnwDocument>>();
		String key ="HNW_DOCUMENT";

		DT14PADocumentMap.put(key,ls);

		return DT14PADocumentMap;
	}
}
