package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ACCSum;

@Repository
public class Rule03ACCSumDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , List<DT03ACCSum>> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT03_ACC_SUM ORDER by id DESC  ");
	 
		List<DT03ACCSum> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT03ACCSum.class));
		System.out.println(" Get All DT03_ACC_SUM.. Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
 
	private HashMap< String ,  List<DT03ACCSum> > rewriteData(List<DT03ACCSum> list){		
		HashMap< String ,  List<DT03ACCSum> > mapReturn = new HashMap< String ,  List<DT03ACCSum>> ();		
		for (DT03ACCSum  item : list) {
			
			String[] planType = item.getPlanType().split(",");
			for(String type:planType) {
				
				if(mapReturn.containsKey(type)) {
					List<DT03ACCSum> temp = mapReturn.get(type);
					mapReturn.remove(type);
					temp.add(item);
					mapReturn.put(type, temp);
				}else {
					List<DT03ACCSum> temp = new ArrayList<DT03ACCSum>();
					temp.add(item);
					mapReturn.put(type, temp);
				}
			}
		}
		return mapReturn;
	}
}