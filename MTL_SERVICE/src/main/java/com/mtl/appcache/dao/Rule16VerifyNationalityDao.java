package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16VerifyNationality;
import com.mtl.rule.model.MsSuperVIP;

@Repository
public class Rule16VerifyNationalityDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT16VerifyNationality> getAll() {
		List<DT16VerifyNationality> dataRes = new ArrayList<DT16VerifyNationality>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT16_VERIFY_NATIONALITY");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT16VerifyNationality.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT16VerifyNationality> rewriteData(List<DT16VerifyNationality> dataMap){
		HashMap< String , DT16VerifyNationality>   verifyNationalityMap = new HashMap< String , DT16VerifyNationality>();
		dataMap.stream().parallel().forEach((data)->{
			verifyNationalityMap.put(data.getHealthQuestion()+"|"+data.getNationality(), data);
		});
		return verifyNationalityMap;		
	}
}
