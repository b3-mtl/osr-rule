package com.mtl.appcache.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.model.underwriting.Document;

@Repository
public class DocumentDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	 
	private static final Logger log = LogManager.getLogger(DocumentDao.class);
	
	public HashMap< String , Document> getDocument(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM MS_DOCUMENT_DESCRIOPTION  ");
		
		List<Document> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		log.info(" Get All MS_DOCUMENT_DESCRIOPTION.. Size:"+ls.size());
		return rewriteData(ls);
	}
	
 
	private RowMapper<Document> rowMapper = new RowMapper<Document>() {		
		@Override
		public Document mapRow(ResultSet rs, int arg1) throws SQLException {			
			Document doc = new Document();
			doc.setCode(rs.getString("CODE"));
			doc.setDescTH(rs.getString("DESCRIPTION"));
			doc.setDescEN(rs.getString("DESC_US"));
			doc.setType(rs.getString("DOC_TYPE"));
			return doc;
		}
		
	};
	
	private HashMap< String , Document> rewriteData(List<Document> docListIn){		
		HashMap< String , Document> returnMap = new HashMap< String ,Document>();		
		String key ="";
		for (Document item : docListIn) {
			key = item.getCode();
			returnMap.put(key, item);
		}
 		return returnMap;
	}
	
}
