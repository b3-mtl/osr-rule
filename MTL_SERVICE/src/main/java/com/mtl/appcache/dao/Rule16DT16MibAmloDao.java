package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16MibAmlo;

@Repository
public class Rule16DT16MibAmloDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , DT16MibAmlo> getAlll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from DT16_MIB_AMLO ");
	 
		List<DT16MibAmlo> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT16MibAmlo.class));
		System.out.println(" ###### Found Rule 16 Rule16DT16MibAmloDao List Size: "+returnMapBeanList.size());
		return rewriteData(returnMapBeanList);
	}
	
 
	private HashMap< String , DT16MibAmlo> rewriteData(List<DT16MibAmlo> mapBeanList){	
		HashMap< String , DT16MibAmlo>  mapData = new HashMap< String , DT16MibAmlo>();
		mapBeanList.forEach(data->{
			mapData.put(data.getCode(), data);
		});
		return mapData;
	}
}