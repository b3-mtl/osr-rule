package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;

@Repository
public class CollectorRuleDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , List<String>> getCollector(){
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT MS_R.RULE_CODE AS KEYWORD  ");
		sb.append(" , LISTAGG(MS_C.COLLECTOR_CODE , ',') WITHIN GROUP (ORDER BY MS_C.COLLECTOR_CODE ASC ) AS COLLECTOR_ID ");
		sb.append(" FROM MS_COLLECTOR_MAPPING MS_R_C ");
		sb.append(" INNER JOIN MS_RULE MS_R ON MS_R.RULE_CODE  = MS_R_C.RULE_CODE ");
		sb.append(" INNER JOIN MS_COLLECTOR MS_C ON MS_C.COLLECTOR_CODE = MS_R_C.COLLECTOR_CODE ");
		sb.append(" WHERE 1=1 ");
		sb.append(" GROUP BY MS_R.RULE_CODE ");
 
		List<HashMap<String, String>> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		return rewriteData(ls);
	}
	
	private RowMapper<HashMap<String, String>> rowMapper = new RowMapper<HashMap<String, String>>() {
		
		@Override
		public HashMap<String, String> mapRow(ResultSet rs, int arg1) throws SQLException {
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("KEYWORD", rs.getString("KEYWORD"));
			map.put("COLLECTOR_ID", rs.getString("COLLECTOR_ID"));
			return map;
		}
		
	};
	
	private HashMap< String , List<String>> rewriteData(List<HashMap<String, String>> lsChannel){
		
		HashMap< String , List<String>> channelRuleCashMap = new HashMap< String , List<String>>();
		
		for (HashMap<String, String> item : lsChannel) {
			String dataList = item.get("COLLECTOR_ID");
			List<String> list = new ArrayList<String>(Arrays.asList(dataList.split(",")));
			channelRuleCashMap.put(item.get("KEYWORD"), list);
		}
		return channelRuleCashMap;
	}
}
