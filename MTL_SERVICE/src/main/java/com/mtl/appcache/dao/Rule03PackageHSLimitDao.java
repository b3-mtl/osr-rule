package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03PackageHSLimit;
import com.mtl.rule.model.DT03PaidUpSumInsured;

@Repository
public class Rule03PackageHSLimitDao {
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03PackageHSLimit> getAll() {
		List<DT03PackageHSLimit> dataRes = new ArrayList<DT03PackageHSLimit>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_PACKAGE_HS_LIMIT ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03PackageHSLimit.class));
		return dataRes;
	}
}
