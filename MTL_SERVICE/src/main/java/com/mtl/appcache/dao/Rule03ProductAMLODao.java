package com.mtl.appcache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT03ProductAMLO;

@Repository
public class Rule03ProductAMLODao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT03ProductAMLO> getAll() {
		List<DT03ProductAMLO> dataRes = new ArrayList<DT03ProductAMLO>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT03_PRODUCT_AMLO ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT03ProductAMLO.class));
		return dataRes;
	}
	
}
