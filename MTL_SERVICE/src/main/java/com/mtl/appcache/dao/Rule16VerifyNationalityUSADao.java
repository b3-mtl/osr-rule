package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16VerifyNationalityUSA;

@Repository
public class Rule16VerifyNationalityUSADao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public HashMap< String , DT16VerifyNationalityUSA> getAll() {
		List<DT16VerifyNationalityUSA> dataRes = new ArrayList<DT16VerifyNationalityUSA>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT16_VERIFY_NATIONALITY_USA");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT16VerifyNationalityUSA.class));
		return rewriteData(dataRes);
	}
	
	private HashMap< String , DT16VerifyNationalityUSA> rewriteData(List<DT16VerifyNationalityUSA> dataMap){
		HashMap< String , DT16VerifyNationalityUSA>   verifyNationalityMap = new HashMap< String , DT16VerifyNationalityUSA>();
		dataMap.stream().parallel().forEach((data)->{
			verifyNationalityMap.put(data.getNationality(), data);
		});
		return verifyNationalityMap;		
	}
}
