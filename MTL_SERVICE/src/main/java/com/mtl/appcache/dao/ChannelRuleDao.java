package com.mtl.appcache.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;

@Repository
public class ChannelRuleDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public HashMap< String , List<String>> getRule(){
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT CONCAT(MS_C.CHANNEL_CODE, MS_R_M.SECTION) AS KEYWORD ");
		sb.append(" , LISTAGG(MS_R.CLASS_NAME , ',') WITHIN GROUP (ORDER BY MS_R.RULE_CODE ASC ) AS RULE_ID");
		sb.append(" FROM MS_RULE_CHANNEL_MAPPING MS_R_M");
		sb.append(" INNER JOIN MS_RULE MS_R ON MS_R.RULE_CODE  = MS_R_M.RULE_CODE");
		sb.append(" INNER JOIN MS_CHANNEL MS_C ON MS_C.CHANNEL_CODE = MS_R_M.CHANNEL_CODE");
		sb.append(" WHERE 1=1");
		sb.append(" AND MS_R_M.IS_ACTIVE = 'Y' ");
		sb.append(" GROUP BY CONCAT(MS_C.CHANNEL_CODE, MS_R_M.SECTION)");
		
		List<HashMap<String, String>> ls = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, rowMapper);
		return rewriteData(ls);
	}
	
	private RowMapper<HashMap<String, String>> rowMapper = new RowMapper<HashMap<String, String>>() {
		
		@Override
		public HashMap<String, String> mapRow(ResultSet rs, int arg1) throws SQLException {
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("KEYWORD", rs.getString("KEYWORD"));
			map.put("RULE", rs.getString("RULE_ID"));
			return map;
		}
		
	};
	
	private HashMap< String , List<String>> rewriteData(List<HashMap<String, String>> lsChannel){
		
		HashMap< String , List<String>> channelRuleCashMap = new HashMap< String , List<String>>();
		
		for (HashMap<String, String> item : lsChannel) {
			String rule = item.get("RULE");
			List<String> list = new ArrayList<String>(Arrays.asList(rule.split(",")));
			channelRuleCashMap.put(item.get("KEYWORD"), list);
		}
		return channelRuleCashMap;
	}
}
