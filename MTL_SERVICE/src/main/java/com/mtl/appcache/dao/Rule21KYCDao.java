package com.mtl.appcache.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT21KYC;

@Repository
public class Rule21KYCDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT21KYC> getAll(){
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from DT21_VERIFY_PERSONAL_KYC ");
	 
		List<DT21KYC> returnMapBeanList = commonJdbcTemplate.executeQuery(sb.toString(),new Object[] {  }, new BeanPropertyRowMapper<>(DT21KYC.class));
		System.out.println(" ###### Found Rule 21 DT21_VERIFY_PERSONAL_KYC List Size: "+returnMapBeanList.size());
		return returnMapBeanList;
	}

}
