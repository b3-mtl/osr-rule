package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanAcc;

@Repository
public class Rule11OccuLevPlanAccDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT11OccuLevPlanAcc> getAll() {
		List<DT11OccuLevPlanAcc> dataRes = new ArrayList<DT11OccuLevPlanAcc>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_EXAMINATION");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11OccuLevPlanAcc.class));
		return dataRes;
	}
	
}
