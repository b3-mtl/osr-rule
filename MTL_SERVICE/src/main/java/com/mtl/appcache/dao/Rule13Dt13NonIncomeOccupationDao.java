package com.mtl.appcache.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13NonIncomeOccupation;

@Repository
public class Rule13Dt13NonIncomeOccupationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT13NonIncomeOccupation > getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * ");	
		sb.append("  FROM DT13_NON_INCOME_OCCUPATION ");
		List<DT13NonIncomeOccupation> ls = commonJdbcTemplate.executeQuery(sb.toString()
																		, new Object[] {}
				  														, new BeanPropertyRowMapper<>(DT13NonIncomeOccupation.class));
		System.out.println(" Get All DT13_NON_INCOME_OCCUPATION.. Size: "+ls.size());
		return rewriteData(ls);
	}
	
	private HashMap< String , DT13NonIncomeOccupation> rewriteData(List<DT13NonIncomeOccupation> ls){
		HashMap< String , DT13NonIncomeOccupation> mapVal = new HashMap< String , DT13NonIncomeOccupation>();
		String key ="";
		for ( DT13NonIncomeOccupation item : ls) {
			key = item.getOccupationCode();
			mapVal.put(key, item);
		}
		return mapVal;
	}
	
}
