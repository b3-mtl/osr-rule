package com.mtl.appcache.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanCode;

@Repository
public class Rule11OccuLevPlanCodeDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT11OccuLevPlanCode> getAll() {
		List<DT11OccuLevPlanCode> dataRes = new ArrayList<DT11OccuLevPlanCode>();
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT * FROM DT11_OCCU_PLAN_CODE ");
		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(),new Object[] {}, new BeanPropertyRowMapper<>(DT11OccuLevPlanCode.class));
		return dataRes;
	}
	
}
