package com.mtl.appcache;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("reloadCache")
@CrossOrigin(origins = "*")
public class ReloadCacheController {
	
	private static final Logger logger = LoggerFactory.getLogger(ReloadCacheController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@GetMapping("")
	@ResponseBody
	public String currentDate(
			HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache started ]]");
		String message = "";
		String status = "";
		try {

			applicationCache.reloadData();	
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		
		return "{status : '"+status+"', message : '"+message+"' }";
	}
	
}
