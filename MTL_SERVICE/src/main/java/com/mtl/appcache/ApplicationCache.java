package com.mtl.appcache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mtl.appcache.dao.ChannelRuleDao;
import com.mtl.appcache.dao.MapProjectDao;
import com.mtl.appcache.dao.MsInvesmentPlanTypeDao;
import com.mtl.appcache.dao.Rule03ACCSumDao;
import com.mtl.appcache.dao.Rule03ACCSumMaxDao;
import com.mtl.appcache.dao.Rule03AccidentForTakafulDao;
import com.mtl.appcache.dao.Rule03AccidentForTakafulNcDao;
import com.mtl.appcache.dao.Rule03ChannelSumInsuredDao;
import com.mtl.appcache.dao.Rule03ChannelSumInsuredEqualDao;
import com.mtl.appcache.dao.Rule03DHealthAgentGroupDao;
import com.mtl.appcache.dao.Rule03DT03BasicPlanSumInsuredDao;
import com.mtl.appcache.dao.Rule03DiagnosisDao;
import com.mtl.appcache.dao.Rule03DisorerAndAccidentDao;
import com.mtl.appcache.dao.Rule03ExtracareHbSuminsuredDao;
import com.mtl.appcache.dao.Rule03FixSumInsuredDao;
import com.mtl.appcache.dao.Rule03HBIncludeHistoryTeleDao;
import com.mtl.appcache.dao.Rule03HealthSumInCludeDao;
import com.mtl.appcache.dao.Rule03HealthSuminsuredDao;
import com.mtl.appcache.dao.Rule03OPDSumDao;
import com.mtl.appcache.dao.Rule03PackageHSLimitDao;
import com.mtl.appcache.dao.Rule03PaidUpSumInsuredDao;
import com.mtl.appcache.dao.Rule03PlanCodeSumInsuredHistoryDao;
import com.mtl.appcache.dao.Rule03PlanTypeeSumInsuredHistoryDao;
import com.mtl.appcache.dao.Rule03ProductAMLODao;
import com.mtl.appcache.dao.Rule03ProjectSumInduredDao;
import com.mtl.appcache.dao.Rule03RiderPlanSumInsuredDao;
import com.mtl.appcache.dao.Rule03RiskDiagnosisPlanTypeDao;
import com.mtl.appcache.dao.Rule03SumByPlanDao;
import com.mtl.appcache.dao.Rule03SumInsuredWithAgeDao;
import com.mtl.appcache.dao.Rule03SumInsuredWithMustHaveRiderDao;
import com.mtl.appcache.dao.Rule04AppMdcDao;
import com.mtl.appcache.dao.Rule04ExceptPlanCodeDao;
import com.mtl.appcache.dao.Rule04PremiumPaymentSpecialDao;
import com.mtl.appcache.dao.Rule05Dt05SpecialPaymentDao;
import com.mtl.appcache.dao.Rule06Dt06DiabetesDocumentDao;
import com.mtl.appcache.dao.Rule06Dt06MapMedicalDiseaseDao;
import com.mtl.appcache.dao.Rule06Dt06MapNmlTypeDao;
import com.mtl.appcache.dao.Rule06Dt06UwqagDao;
import com.mtl.appcache.dao.Rule06Dt06UwrqDao;
import com.mtl.appcache.dao.Rule07Dt07MIBDao;
import com.mtl.appcache.dao.Rule07Dt07MIBHnwDao;
import com.mtl.appcache.dao.Rule07Dt07MIBSpecialDao;
import com.mtl.appcache.dao.Rule11DT11AlcoholDao;
import com.mtl.appcache.dao.Rule11DT11DrugDao;
import com.mtl.appcache.dao.Rule11DT11SmokingDao;
import com.mtl.appcache.dao.Rule11DiseaseDao;
import com.mtl.appcache.dao.Rule11ExaminationDao;
import com.mtl.appcache.dao.Rule11OccuLevPlanAccDao;
import com.mtl.appcache.dao.Rule11OccuLevPlanCodeDao;
import com.mtl.appcache.dao.Rule11OccuLevPlanTypeDao;
import com.mtl.appcache.dao.Rule11OccuSuminsuredDao;
import com.mtl.appcache.dao.Rule11OccupationAccidentDao;
import com.mtl.appcache.dao.Rule12ClaimCodeHistoryDao;
import com.mtl.appcache.dao.Rule13Dt13AmloPilotDao;
import com.mtl.appcache.dao.Rule13Dt13NonIncomeOccupationDao;
import com.mtl.appcache.dao.Rule13Dt13RiskOccupationDao;
import com.mtl.appcache.dao.Rule14DocumentClaimDao;
import com.mtl.appcache.dao.Rule14ExceptionAppNoDao;
import com.mtl.appcache.dao.Rule14ExceptionDao;
import com.mtl.appcache.dao.Rule14HnwDocumentDao;
import com.mtl.appcache.dao.Rule14PADocumentDao;
import com.mtl.appcache.dao.Rule15DupPackagePlanDao;
import com.mtl.appcache.dao.Rule15DupPackageProjectDao;
import com.mtl.appcache.dao.Rule15DupPlanForClientDao;
import com.mtl.appcache.dao.Rule15DupPlanForPolicyDao;
import com.mtl.appcache.dao.Rule15DupPlanForProDao;
import com.mtl.appcache.dao.Rule16ClaimHistoryDao;
import com.mtl.appcache.dao.Rule16DT16AlcoholDao;
import com.mtl.appcache.dao.Rule16DT16FemaleDiseaseListDao;
import com.mtl.appcache.dao.Rule16DT16MibAmloDao;
import com.mtl.appcache.dao.Rule16DT16MibHighRiskDao;
import com.mtl.appcache.dao.Rule16PayorOccupationDao;
import com.mtl.appcache.dao.Rule16PayorRelationDao;
import com.mtl.appcache.dao.Rule16VerifyNationalityDao;
import com.mtl.appcache.dao.Rule16VerifyNationalityUSADao;
import com.mtl.appcache.dao.Rule17BeneficiaryRelationDao;
import com.mtl.appcache.dao.Rule17MIBAMLODao;
import com.mtl.appcache.dao.Rule21KYCDao;
import com.mtl.appcache.dao.common.AppNoDao;
import com.mtl.appcache.dao.common.DocumentDao;
import com.mtl.appcache.dao.common.MsParameterDao;
import com.mtl.appcache.dao.common.MsPremiumPaymentDao;
import com.mtl.appcache.dao.common.MsSuperVip;
import com.mtl.appcache.dao.common.PlanHeaderDao;
import com.mtl.appcache.dao.common.PlanPermissionDao;
import com.mtl.appcache.dao.common.PlanTypeDao;
import com.mtl.model.common.AppNo;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.common.PlanType;
import com.mtl.model.rule.PayorOccupation;
import com.mtl.model.rule.PayorRelation;
import com.mtl.model.underwriting.Document;
import com.mtl.rule.model.DT03ACCSum;
import com.mtl.rule.model.DT03ACCSumMax;
import com.mtl.rule.model.DT03AccidentForTakaful;
import com.mtl.rule.model.DT03AccidentForTakafulNc;
import com.mtl.rule.model.DT03ChannelSumInsured;
import com.mtl.rule.model.DT03ChannelSumInsuredEqual;
import com.mtl.rule.model.DT03DHealthAgentGroup;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPG;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPT;
import com.mtl.rule.model.DT03ExtracareHbSuminsured;
import com.mtl.rule.model.DT03FixSumInsured;
import com.mtl.rule.model.DT03HBIncludeHistoryTele;
import com.mtl.rule.model.DT03HealthSuminsured;
import com.mtl.rule.model.DT03HealthSuminsuredInclude;
import com.mtl.rule.model.DT03OPDSum;
import com.mtl.rule.model.DT03PackageHSLimit;
import com.mtl.rule.model.DT03PaidUpSumInsured;
import com.mtl.rule.model.DT03PlanCodeSumInsuredHistory;
import com.mtl.rule.model.DT03PlanTypeeSumInsuredHistory;
import com.mtl.rule.model.DT03ProductAMLO;
import com.mtl.rule.model.DT03ProjectSumIndured;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.DT03RiskDiagnosisPlanType;
import com.mtl.rule.model.DT03SumByPlan;
import com.mtl.rule.model.DT03SumInsuredWithAge;
import com.mtl.rule.model.DT03SumInsuredWithMustHaveRider;
import com.mtl.rule.model.DT04AppMdc;
import com.mtl.rule.model.DT04PremiumPaymentSpecial;
import com.mtl.rule.model.DT06DiabetesDocument;
import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.DT06UWRQ;
import com.mtl.rule.model.DT07MIB;
import com.mtl.rule.model.DT07MIBHnw;
import com.mtl.rule.model.DT07MIBSpecial;
import com.mtl.rule.model.DT11Alcohol;
import com.mtl.rule.model.DT11BMIBaby;
import com.mtl.rule.model.DT11BMIChildAndAdult;
import com.mtl.rule.model.DT11BMIDocument;
import com.mtl.rule.model.DT11Disease;
import com.mtl.rule.model.DT11Drug;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanAcc;
import com.mtl.rule.model.DT11OccuLevPlanCode;
import com.mtl.rule.model.DT11OccuLevPlanType;
import com.mtl.rule.model.DT11OccuSuminsured;
import com.mtl.rule.model.DT11OcuupationAccident;
import com.mtl.rule.model.DT11PlanReferPremiumPlus;
import com.mtl.rule.model.DT11PremiumPlus;
import com.mtl.rule.model.DT11RejectDeclinePostpone;
import com.mtl.rule.model.DT11RiskAreasHouseRegis;
import com.mtl.rule.model.DT11RiskAreasOffice;
import com.mtl.rule.model.DT11RiskAreasPrestAddr;
import com.mtl.rule.model.DT11RiskMobileNumber;
import com.mtl.rule.model.DT11Smoke;
import com.mtl.rule.model.DT11VerifyNationUSA;
import com.mtl.rule.model.DT11VerifyNationWithPlan;
import com.mtl.rule.model.DT11VerifyNationality;
import com.mtl.rule.model.DT11VerifySexByPlancode;
import com.mtl.rule.model.DT13AmloPilot;
import com.mtl.rule.model.DT13NonIncomeOccupation;
import com.mtl.rule.model.DT13RiskOccupation;
import com.mtl.rule.model.DT14DocumentClaim;
import com.mtl.rule.model.DT14Exception;
import com.mtl.rule.model.DT14ExceptionAppNo;
import com.mtl.rule.model.DT14HnwDocument;
import com.mtl.rule.model.DT14PADocument;
import com.mtl.rule.model.DT15DupPackagePlan;
import com.mtl.rule.model.DT15DupPackageProject;
import com.mtl.rule.model.DT15DupPlanForClient;
import com.mtl.rule.model.DT15DupPlanForPolicy;
import com.mtl.rule.model.DT15DupPlanForPro;
import com.mtl.rule.model.DT16Alcohol;
import com.mtl.rule.model.DT16ClaimHistory;
import com.mtl.rule.model.DT16FemaleDiseaseList;
import com.mtl.rule.model.DT16MibAmlo;
import com.mtl.rule.model.DT16MibHighRisk;
import com.mtl.rule.model.DT16VerifyNationality;
import com.mtl.rule.model.DT16VerifyNationalityUSA;
import com.mtl.rule.model.DT17BeneficiaryRelation;
import com.mtl.rule.model.DT21KYC;
import com.mtl.rule.model.Dt03BasicPlanSumInsured;
import com.mtl.rule.model.Dt03DisorderAndAccident;
import com.mtl.rule.model.Dt06MapNmlType;
import com.mtl.rule.model.Dt06Uwqag;
import com.mtl.rule.model.MapProject;
import com.mtl.rule.model.MappingClientLocationCode;
import com.mtl.rule.model.MsInvesmentPlanType;
import com.mtl.rule.model.MsSuperVIP;
import com.mtl.rule.repository.Dt11BmiDao;
import com.mtl.rule.repository.Dt11PremiumDao;
import com.mtl.rule.repository.Dt11RejectDao;
import com.mtl.rule.repository.Dt11RiskDao;
import com.mtl.rule.repository.Dt11VerifyDao;

@Component
public class ApplicationCache {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationCache.class);
	// Common
	public HashMap<String, String> planDescMap = new HashMap<String, String>();
	public HashMap<String, Document> documentCashMap = new HashMap<String, Document>();
	public HashMap<String, List<String>> channelRuleCashMap = new HashMap<String, List<String>>();
	public HashMap<String, PlanHeader> planHeaderAppCashMap = new HashMap<String, PlanHeader>();
	public HashMap<String, PlanType> planTypeCacheMap = new HashMap<String, PlanType>();
	public HashMap<String, MsInvesmentPlanType>  msInvesmentPlanType = new HashMap<String, MsInvesmentPlanType>();
	
	
	public HashMap<String, AppNo> appNoAppCashMap = new HashMap<String, AppNo>();
	public HashMap<String, List<String>> msPremiumPayment = new HashMap<String, List<String>>();
	public HashMap<String, List<String>> msParameter = new HashMap<String, List<String>>();
	public HashMap<String, PlanPermission> planPermissionAppCashMap = new HashMap<String, PlanPermission>();
	public HashMap<String, MappingClientLocationCode> mas_mappingClientLocationCode = new HashMap<String, MappingClientLocationCode>();
	public HashMap<String, MsSuperVIP> msSuperVip = new HashMap<String, MsSuperVIP>();
	public List<MapProject> mapProjectList = new ArrayList<MapProject>();
	
	// Decision Rule 3
	public HashMap<String, DT03OPDSum> rule03_dt03OPDSum = new HashMap<String, DT03OPDSum>();
	public HashMap<String, List<DT03ACCSum>> rule03_dt03ACCSum = new HashMap<String, List<DT03ACCSum>>();
	public List<DT03ACCSumMax> rule03_dt03ACCSumMax = new ArrayList<DT03ACCSumMax>();
	public List<DT03SumByPlan> rule03_dt03SumByPlan = new ArrayList<DT03SumByPlan>();
	public HashMap<String, List<DT03HealthSuminsured>> rule03_dt03HealthSuminsured= new HashMap<String, List<DT03HealthSuminsured>>();
	public HashMap<String, List<DT03DiagnosisSumInsuredPT>> rule03_dt03DiagnosisPT = new HashMap<String, List<DT03DiagnosisSumInsuredPT>>();
	public HashMap<String, List<DT03DiagnosisSumInsuredPG>> rule03_dt03DiagnosisPG = new HashMap<String, List<DT03DiagnosisSumInsuredPG>>();
	public List<DT03HealthSuminsuredInclude> rule03_dt03HealthSumInClude = new ArrayList<DT03HealthSuminsuredInclude>();
	
	public List<Dt03BasicPlanSumInsured> rule03_dt03BasicPlanSumInsured = new ArrayList<Dt03BasicPlanSumInsured>();
	public List<Dt03DisorderAndAccident> rule03_dT03DisorderAndAccident = new ArrayList<Dt03DisorderAndAccident>();
	
	public List<DT03PaidUpSumInsured> rule03_dT03PaidUpSumInsured = new ArrayList<DT03PaidUpSumInsured>();
	public List<DT03PlanCodeSumInsuredHistory> rule03_dT03PlanCodeSumInsuredHistory  = new ArrayList<DT03PlanCodeSumInsuredHistory>();
	public List<DT03PlanTypeeSumInsuredHistory> rule03_dT03PlanTypeeSumInsuredHistory  = new ArrayList<DT03PlanTypeeSumInsuredHistory>();
	public List<DT03ProjectSumIndured> rule03_dT03ProjectSumIndured  = new ArrayList<DT03ProjectSumIndured>();
	public List<DT03RiderPlanSumInsured> rule03_dT03_RiderPlanSumInsured  = new ArrayList<DT03RiderPlanSumInsured>();
	public List<DT03SumInsuredWithAge> rule03_dT03_SumInsuredWithAge  = new ArrayList<DT03SumInsuredWithAge>();
	public List<DT03SumInsuredWithMustHaveRider> rule03_dT03_SumInsuredWithMustHaveRider  = new ArrayList<DT03SumInsuredWithMustHaveRider>();
	public List<DT03FixSumInsured> rule03_dT03_FixSumInsured  = new ArrayList<DT03FixSumInsured>();
	public List<DT03PackageHSLimit> rule03_dT03_PackageHSLimit  = new ArrayList<DT03PackageHSLimit>();
	public List<DT03ProductAMLO> rule03_dT03_ProductAMLO  = new ArrayList<DT03ProductAMLO>();
	public List<DT03ChannelSumInsured> rule03_dT03_ChannelSumInsured  = new ArrayList<DT03ChannelSumInsured>();
	public List<DT03ChannelSumInsuredEqual> rule03_dT03_ChannelSumInsuredEqual  = new ArrayList<DT03ChannelSumInsuredEqual>();
	public List<DT03HBIncludeHistoryTele> rule03_dT03_hbIncludeHistoryTele = new ArrayList<DT03HBIncludeHistoryTele>();
	public List<DT03ExtracareHbSuminsured> rule03_dT03_extracareHbSuminsured = new ArrayList<DT03ExtracareHbSuminsured>();
	public List<DT03RiskDiagnosisPlanType> rule03_dT03_riskDiagnosisPlanType = new ArrayList<DT03RiskDiagnosisPlanType>();

	public List<DT03DHealthAgentGroup> rule03_dT03DHealthAgentGroup = new ArrayList<DT03DHealthAgentGroup>();
	public List<DT03AccidentForTakaful> rule03_dT03AccidentForTakaful = new ArrayList<DT03AccidentForTakaful>();
	public List<DT03AccidentForTakafulNc> rule03_dT03AccidentForTakafulNc = new ArrayList<DT03AccidentForTakafulNc>();

	
	// Decision Rule 4
	public HashMap<String, String> rule04_dt04ExceptPlanCode = new HashMap<String, String>();
	public HashMap<String, DT04AppMdc> rule04_dt04AppMdc = new HashMap<String, DT04AppMdc>();
	public HashMap<String, DT04PremiumPaymentSpecial> rule04_dt04PremiumPaymentSpecial = new HashMap<String, DT04PremiumPaymentSpecial>();

	// Decision Rule 5
	public HashMap<String, Set<String>> rule05_specialPaymentChannelMap = new HashMap<String, Set<String>>();
	public HashMap<String, List<String>> rule05_premiumPayment = new HashMap<String, List<String>>();

	// Decision Rule 6
	public HashMap<String, List<Dt06Uwqag>> rule06_dt06UwqagMap = new HashMap<String, List<Dt06Uwqag>>();
	public HashMap<String, Dt06MapNmlType> rule06_dt06MapNmlTypeMap = new HashMap<String, Dt06MapNmlType>();
	public HashMap<String, DT06MapMedicalDisease> rule06_dt06MapMedicalDisease = new HashMap<String, DT06MapMedicalDisease>();
	public HashMap<String, List<DT06UWRQ>> rule06_dt06Uwrq = new HashMap<String, List<DT06UWRQ>>();
	public HashMap<String, List<DT06DiabetesDocument>> rule06_dt06DiabetesDocument = new HashMap<String, List<DT06DiabetesDocument>>();

	// Decision Rule 7
	public HashMap<String, DT07MIB> rule07_mibMap = new HashMap<String, DT07MIB>();
	public HashMap<String, DT07MIBSpecial> rule07_mibSpecialMap = new HashMap<String, DT07MIBSpecial>();
	public HashMap<String, DT07MIBHnw> rule07_mibHnwMap = new HashMap<String, DT07MIBHnw>();

	// Decision Rule 11
	public HashMap<String, DT11RiskAreasOffice> rule11_riskAreasOfficeMap = new HashMap<String, DT11RiskAreasOffice>();
	public HashMap<String, DT11RiskAreasHouseRegis> rule11_riskAreasHouseRegisMap = new HashMap<String, DT11RiskAreasHouseRegis>();
	public HashMap<String, DT11RiskAreasPrestAddr> rule11_riskAreasPrestAddr = new HashMap<String, DT11RiskAreasPrestAddr>();
	public HashMap<String, DT11RiskMobileNumber> rule11_riskMobileNumber = new HashMap<String, DT11RiskMobileNumber>();
	public HashMap<String, DT11RejectDeclinePostpone> rule11_rejectDeclinePostpone = new HashMap<String, DT11RejectDeclinePostpone>();
	public HashMap<String, DT11PremiumPlus> rule11_premiumPlus = new HashMap<String, DT11PremiumPlus>();
	public HashMap<String, DT11PlanReferPremiumPlus> rule11_planReferPremiumPlus = new HashMap<String, DT11PlanReferPremiumPlus>();
	public HashMap<String, DT11BMIChildAndAdult> rule11_BMIChildAndAdult = new HashMap<String, DT11BMIChildAndAdult>();
	public HashMap<String, DT11BMIBaby> rule11_BMIBaby = new HashMap<String, DT11BMIBaby>();
	public HashMap<String, DT11VerifySexByPlancode> rule11_verifySexByPlancode = new HashMap<String, DT11VerifySexByPlancode>();
	public HashMap<String, DT11VerifyNationWithPlan> rule11_listOtwVerifyNationWPlan = new HashMap<String, DT11VerifyNationWithPlan>();
	public HashMap<String, DT11VerifyNationWithPlan> rule11_listVerifyNationWPlan = new HashMap<String, DT11VerifyNationWithPlan>();
	public List<DT11BMIDocument> rule11_BMIDocument = new ArrayList<DT11BMIDocument>();
	public HashMap<String, List<DT11VerifyNationality>> rule11_verifyNotinalrity = new HashMap<String, List<DT11VerifyNationality>>();
	public HashMap<String, DT11VerifyNationUSA> rule11_verifyNationalityUSA = new HashMap<String, DT11VerifyNationUSA>();
	public HashMap<String, DT11Alcohol> rule11Alcohol = new HashMap<String, DT11Alcohol>();
	public HashMap<String, DT11Disease> rule11Disease = new HashMap<String, DT11Disease>();
	public List<DT11Smoke> rule11Smoke = new ArrayList<DT11Smoke>();
	public List<DT11Drug> rule11Drug = new ArrayList<DT11Drug>();
	public List<DT11Examination> rule11Examination = new ArrayList<DT11Examination>();
	
	public List<DT11OccuLevPlanAcc> rule11OccuLevPlanAcc = new ArrayList<DT11OccuLevPlanAcc>();
	public List<DT11OccuLevPlanCode> rule11OccuLevPlanCode = new ArrayList<DT11OccuLevPlanCode>();
	public List<DT11OccuLevPlanType> rule11OccuLevPlanType = new ArrayList<DT11OccuLevPlanType>();
	public List<DT11OccuSuminsured> rule11OccuSuminsured = new ArrayList<DT11OccuSuminsured>();
//	public List<MsInvesmentPlanType> msInvesmentPlanType = new ArrayList<MsInvesmentPlanType>();
	public List<DT11OcuupationAccident> rule11OccupationAccident = new ArrayList<DT11OcuupationAccident>();

	// Decision Rule 12
	public HashMap<String, String> claimCodeHistoryMap = new HashMap<String, String>();
	public HashMap<String, String> exceptionClaimCodeMap = new HashMap<String, String>();

	// Decision Rule 13
	public HashMap<String, DT13AmloPilot> rule13AmloPilot = new HashMap<String, DT13AmloPilot>();
	public HashMap<String, DT13NonIncomeOccupation> rule13NonIncomeOccupation = new HashMap<String, DT13NonIncomeOccupation>();
	public HashMap<String, DT13RiskOccupation> rule13RiskOccupation = new HashMap<String, DT13RiskOccupation>();

	// Decision Rule 14
	public HashMap<String, DT14Exception> rule14Exception = new HashMap<String, DT14Exception>();
	public HashMap<String, List<DT14DocumentClaim>> rule14ExceptionClaim = new HashMap<String, List<DT14DocumentClaim>>();
	public HashMap<String, DT14ExceptionAppNo> rule14ExceptionAppNo = new HashMap<String, DT14ExceptionAppNo>();
	public HashMap<String, List<DT14PADocument>> paDocument = new HashMap<String, List<DT14PADocument>>();
	public HashMap<String, List<DT14HnwDocument>> hnwDocument = new HashMap<String, List<DT14HnwDocument>>();

	
	// Decision Rule 15
	public List<DT15DupPackagePlan> dupPackagePlan = new ArrayList<DT15DupPackagePlan>();
	public List<DT15DupPackageProject> dupPackageProject = new ArrayList<DT15DupPackageProject>();
	public List<DT15DupPlanForClient> dupPlanForClient = new ArrayList<DT15DupPlanForClient>();
	public List<DT15DupPlanForPolicy> dupPlanForPolicy = new ArrayList<DT15DupPlanForPolicy>();
	public List<DT15DupPlanForPro> dupPlanForPro = new ArrayList<DT15DupPlanForPro>();


	// Decision Rule 16
	public HashMap<String, PayorRelation> payorRelationMap = new HashMap<String, PayorRelation>();
	public HashMap<String, PayorOccupation> payorOccupationMap = new HashMap<String, PayorOccupation>();
	public HashMap<String, DT16ClaimHistory> claimHistoryMap = new HashMap<String, DT16ClaimHistory>();
	public HashMap<String, DT16VerifyNationality> verifyNationality = new HashMap<String, DT16VerifyNationality>();
	public HashMap<String, DT16VerifyNationalityUSA> verifyNationalityUSA = new HashMap<String, DT16VerifyNationalityUSA>();
	public HashMap<String, DT16FemaleDiseaseList> femaleDiseaseList = new HashMap<String, DT16FemaleDiseaseList>();
	public HashMap<String, DT16Alcohol> rule16Alcohol = new HashMap<String, DT16Alcohol>();
	public HashMap<String, DT16MibAmlo> dt16MibAmlo;
	public HashMap<String, DT16MibHighRisk> dt16MibHighRisk;

	// Decision Rule 17
	public HashMap<String, DT17BeneficiaryRelation> beneficiaryRelationMap = new HashMap<String, DT17BeneficiaryRelation>();
	public List<String> dt17MIBAMLOList = new ArrayList<String>();
	
	// Decision Rule 21
	public List<DT21KYC> dt21kyc = new ArrayList<DT21KYC>();

	@Autowired
	private MsSuperVip msSuperVipDao;

	@Autowired
	private DocumentDao documentDao;

	@Autowired
	private AppNoDao appNoDao;

	@Autowired
	private PlanPermissionDao planPermissionDao;

	@Autowired
	private PlanHeaderDao planHeaderDao;

	@Autowired
	private ChannelRuleDao channelRuleDao;
	
	@Autowired
	private PlanTypeDao planTypeDao;

	@Autowired
	private MsPremiumPaymentDao msPremiumPaymentDao;

	@Autowired
	private MapProjectDao mapProjectDao;
	
	@Autowired
	private Rule03OPDSumDao rule03OPDSumDao;

	@Autowired
	private Rule03ACCSumDao rule03ACCSumDao;
	
	@Autowired
	private Rule03ACCSumMaxDao rule03ACCSumMaxDao;
	
	@Autowired
	private Rule03SumByPlanDao rule03SumByPlanDao;

	@Autowired
	private Rule03DiagnosisDao rule03DiagnosisDao;
	
	@Autowired
	private Rule03HealthSuminsuredDao rule03HealthSuminsuredDao;

	@Autowired
	private Rule03DT03BasicPlanSumInsuredDao rule03DT03BasicPlanSumInsuredDao;
	
	@Autowired
	private Rule03PaidUpSumInsuredDao paidUpSumInsuredDao;
	
	@Autowired
	private Rule03PlanCodeSumInsuredHistoryDao planCodeSumInsuredHistoryDao;
	
	@Autowired
	private Rule03PlanTypeeSumInsuredHistoryDao planTypeeSumInsuredHistoryDao;
	
	@Autowired
	private Rule03ProjectSumInduredDao projectSumInduredDao;
	
	@Autowired
	private Rule03RiderPlanSumInsuredDao riderPlanSumInsuredDao;
	
	@Autowired
	private Rule03SumInsuredWithAgeDao sumInsuredWithAgeDao;
	
	@Autowired
	private Rule03SumInsuredWithMustHaveRiderDao sumInsuredWithMustHaveRiderDao;
	
	@Autowired
	private Rule03FixSumInsuredDao fixSumInsuredDao;
	
	@Autowired
	private Rule03PackageHSLimitDao packageHSLimitDao;
	
	@Autowired
	private Rule03ProductAMLODao productAMLODao;
	
	@Autowired
	private Rule03ChannelSumInsuredDao channelSumInsuredDao;
	
	@Autowired
	private Rule03ChannelSumInsuredEqualDao channelSumInsuredEqualDao;
	
	@Autowired
	private Rule03HBIncludeHistoryTeleDao hBIncludeHistoryTeleDao;
	
	@Autowired
	private Rule03ExtracareHbSuminsuredDao extracareHbSuminsuredDao;
	
	@Autowired
	private Rule03RiskDiagnosisPlanTypeDao riskDiagnosisPlanTypeDao;
	
	@Autowired
	private Rule03DisorerAndAccidentDao rule03DisorerAndAccidentDao;
	
	@Autowired
	private Rule03DHealthAgentGroupDao dHealthAgentGroupDao;
	
	@Autowired
	private Rule03AccidentForTakafulDao accidentForTakafulDao ;
	
	@Autowired
	private Rule03AccidentForTakafulNcDao accidentForTakafulNcDao;
	
	
	@Autowired
	private Rule04ExceptPlanCodeDao rule04Dt04ExceptPlanCodeDao;

	@Autowired
	private Rule04AppMdcDao rule04Dt04AppMdcDao;

	@Autowired
	private Rule04PremiumPaymentSpecialDao rule04Dt04PremiumPaymentSpecialDao;

	@Autowired
	private Rule05Dt05SpecialPaymentDao rule5SpecialPaymentDao;

	@Autowired
	private Rule06Dt06UwqagDao dt06UwqagDao;

	@Autowired
	private Rule06Dt06MapNmlTypeDao dt06MapNmlTypeDao;

	@Autowired
	private Rule06Dt06MapMedicalDiseaseDao rule06Dt06MapMedicalDiseaseDao;

	@Autowired
	private Rule06Dt06UwrqDao rule06Dt06UwrqDao;

	@Autowired
	private Rule07Dt07MIBDao rule07Dt07MIBDao;

	@Autowired
	private Rule07Dt07MIBSpecialDao rule07Dt07MIBSpecialDao;

	@Autowired
	private Rule07Dt07MIBHnwDao rule07Dt07MIBHnwDao;

	@Autowired
	private Rule06Dt06DiabetesDocumentDao rule06Dt06DiabetesDocumentDao;

	// ===> Autowired rule 11 <===

	@Autowired
	private Dt11RiskDao dt11RiskDao;

	@Autowired
	private Dt11RejectDao dt11RejectDao;

	@Autowired
	private Dt11PremiumDao dt11PremiumDao;

	@Autowired
	private Dt11VerifyDao dt11VerifyDao;

	@Autowired
	private Dt11BmiDao dt11BmiDao;
	
	@Autowired
	private Rule11DT11AlcoholDao rule11DT11AlcoholDao;
	
	@Autowired
	private Rule11DiseaseDao rule11DiseaseDao;
	
	@Autowired
	private Rule11DT11SmokingDao rule11DT11SmokingDao;
	
	@Autowired
	private Rule11DT11DrugDao rule11DT11DrugDao;
	
	@Autowired
	private Rule11ExaminationDao rule11ExaminationDao;
	
	@Autowired
	private Rule11OccuLevPlanAccDao  rule11OccuLevPlanAccDao;
	
	@Autowired
	private Rule11OccuLevPlanCodeDao  rule11OccuLevPlanCodeDao;
	
	@Autowired
	private Rule11OccuLevPlanTypeDao  rule11OccuLevPlanTypeDao;
	
	@Autowired
	private Rule11OccuSuminsuredDao  rule11OccuSuminsuredDao;
	
	@Autowired
	private MsInvesmentPlanTypeDao  msInvesmentPlanTypeDao;
	
	@Autowired
	private Rule11OccupationAccidentDao  rule11OccupationAccidentDao;
	
	// ===> end Autowired rule 11 <===

	@Autowired
	private Rule13Dt13AmloPilotDao rule13Dt13AmloPilotDao;

	@Autowired
	private Rule13Dt13NonIncomeOccupationDao rule13Dt13NonIncomeOccupationDao;

	@Autowired
	private Rule13Dt13RiskOccupationDao rule13Dt13RiskOccupationDao;

	@Autowired
	private Rule03HealthSumInCludeDao rule03HealthSumInCludeDao;
	
	@Autowired
	private Rule12ClaimCodeHistoryDao rule12ClaimCodeHistoryDao;

	@Autowired
	private Rule14ExceptionDao rule14ExceptionDao;

	@Autowired
	private Rule14ExceptionAppNoDao rule14ExceptionAppNoDao;

	@Autowired
	private Rule14DocumentClaimDao rule14ExceptionClaimDao;

	@Autowired
	private Rule14PADocumentDao rule14PADocumentDao;

	@Autowired
	private Rule14HnwDocumentDao rule14HnwDocumentDao;
	
	// Rule 15
	
	@Autowired
	Rule15DupPackagePlanDao rule15DupPackagePlanDao;
	
	@Autowired
	Rule15DupPackageProjectDao rule15DupPackageProjectDao;
	
	@Autowired
	Rule15DupPlanForClientDao rule15DupPlanForClientDao;
	
	@Autowired
	Rule15DupPlanForPolicyDao rule15DupPlanForPolicyDao;
	
	@Autowired
	Rule15DupPlanForProDao rule15DupPlanForProDao;
	
	
	
	// Rule 16
	@Autowired
	private Rule16PayorOccupationDao rule16PayorOccupationDao;

	@Autowired
	private Rule16PayorRelationDao rule16PayorRelationDao;

	@Autowired
	private Rule16ClaimHistoryDao rule16ClaimHistoryDao;

	@Autowired
	private Rule16VerifyNationalityDao rule16VerifyNationalityDao;

	@Autowired
	private Rule16VerifyNationalityUSADao rule16VerifyNationalityUsaDao;

	@Autowired
	private Rule16DT16FemaleDiseaseListDao rule16DT16FemaleDiseaseListDao;

	@Autowired
	private Rule16DT16AlcoholDao rule16DT16AlcoholDao;

	@Autowired
	private Rule16DT16MibAmloDao rule16DT16MibAmloDao;
	
	@Autowired
	private Rule16DT16MibHighRiskDao rule16DT16MibHighRiskDao;
//End  
	@Autowired
	private Rule17BeneficiaryRelationDao rule17BeneficiaryRelationDao;

	@Autowired
	private Rule17MIBAMLODao rule17MIBAMLODao;
	
	@Autowired
	private Rule21KYCDao rule21KYCDao;

	@Autowired
	private MsParameterDao msParameterDao;

	/** Reload */
	@PostConstruct
	public synchronized void loadCache() {
		reloadData();
	}

	public void reloadData() {
		logger.info("======== Start Load Application Cache ======");

		planHeaderAppCashMap = planHeaderDao.getAllPlanHeader();
		documentCashMap = documentDao.getDocument();
		appNoAppCashMap = appNoDao.getAllAppNo();
		planPermissionAppCashMap = planPermissionDao.getAllPlanPermission();
		msPremiumPayment = msPremiumPaymentDao.findAll();
		msParameter = msParameterDao.getMsParameter();
		msSuperVip = msSuperVipDao.getCollector();
		planTypeCacheMap = planTypeDao.getAll();
		msInvesmentPlanType = msInvesmentPlanTypeDao.getAll();
		mapProjectList = mapProjectDao.getAll();
		
		channelRuleCashMap = channelRuleDao.getRule();
		mas_mappingClientLocationCode = planHeaderDao.getAllClient();

		// Rule 3
		rule03_dt03OPDSum = rule03OPDSumDao.getAll();
		rule03_dt03ACCSum = rule03ACCSumDao.getAll();
		rule03_dt03ACCSumMax = rule03ACCSumMaxDao.getAll();
		rule03_dt03SumByPlan = rule03SumByPlanDao.getAll();
		rule03_dt03HealthSuminsured = rule03HealthSuminsuredDao.getAll();
		rule03_dt03DiagnosisPT = rule03DiagnosisDao.getPTAll();
		rule03_dt03DiagnosisPG = rule03DiagnosisDao.getPGAll();
		rule03_dt03HealthSumInClude = rule03HealthSumInCludeDao.getAll();
		rule03_dt03BasicPlanSumInsured = rule03DT03BasicPlanSumInsuredDao.getAll();
		rule03_dT03DisorderAndAccident = rule03DisorerAndAccidentDao.getAll();
		
		rule03_dT03PaidUpSumInsured = paidUpSumInsuredDao.getAll();
		rule03_dT03PlanCodeSumInsuredHistory = planCodeSumInsuredHistoryDao.getAll();
		rule03_dT03PlanTypeeSumInsuredHistory = planTypeeSumInsuredHistoryDao.getAll();
		rule03_dT03ProjectSumIndured = projectSumInduredDao.getAll();
		
		rule03_dT03_RiderPlanSumInsured = riderPlanSumInsuredDao.getAll();
		rule03_dT03_SumInsuredWithAge = sumInsuredWithAgeDao.getAll();
		rule03_dT03_SumInsuredWithMustHaveRider = sumInsuredWithMustHaveRiderDao.getAll();
		rule03_dT03_FixSumInsured = fixSumInsuredDao.getAll();
		rule03_dT03_PackageHSLimit = packageHSLimitDao.getAll();
		rule03_dT03_ProductAMLO = productAMLODao.getAll();
		rule03_dT03_ChannelSumInsured = channelSumInsuredDao.getAll();
		rule03_dT03_ChannelSumInsuredEqual = channelSumInsuredEqualDao.getAll();
		rule03_dT03_hbIncludeHistoryTele = hBIncludeHistoryTeleDao.getAll();
		rule03_dT03_extracareHbSuminsured = extracareHbSuminsuredDao.getAll();
		rule03_dT03_riskDiagnosisPlanType = riskDiagnosisPlanTypeDao.getAll();

		rule03_dT03DHealthAgentGroup = dHealthAgentGroupDao.getAll();
		rule03_dT03AccidentForTakaful = accidentForTakafulDao.getAll();
		rule03_dT03AccidentForTakafulNc = accidentForTakafulNcDao.getAll();
		
		
		// Rule 4
		rule04_dt04ExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
		rule04_dt04AppMdc = rule04Dt04AppMdcDao.getAllAppMdc();
		rule04_dt04PremiumPaymentSpecial = rule04Dt04PremiumPaymentSpecialDao.getAllPremiumPaymentSpecial();

		// Rule 5
		rule05_specialPaymentChannelMap = rule5SpecialPaymentDao.getSpecialPaymentChannel();
		rule05_premiumPayment = msPremiumPayment;

		// Rule 6
		rule06_dt06UwqagMap = dt06UwqagDao.getCollector();
		rule06_dt06MapNmlTypeMap = dt06MapNmlTypeDao.getCollector();
		rule06_dt06MapMedicalDisease = rule06Dt06MapMedicalDiseaseDao.getAllDoc();
		rule06_dt06Uwrq = rule06Dt06UwrqDao.getAllList();
		rule06_dt06DiabetesDocument = rule06Dt06DiabetesDocumentDao.getAllList();

		// Rule 7
		rule07_mibMap = rule07Dt07MIBDao.getAllList();
		rule07_mibSpecialMap = rule07Dt07MIBSpecialDao.getAllList();
		rule07_mibHnwMap = rule07Dt07MIBHnwDao.getAllList();

		// Rule 11
		rule11_riskAreasOfficeMap = dt11RiskDao.getAllList();
		rule11_riskAreasHouseRegisMap = dt11RiskDao.getAllListRiskAreasHouseRegis();
		rule11_riskAreasPrestAddr = dt11RiskDao.getAllListRiskAreasPrestAddr();
		rule11_riskMobileNumber = dt11RiskDao.getAllListRiskMobileNumber();
		rule11_rejectDeclinePostpone = dt11RejectDao.getAllList();
		rule11_premiumPlus = dt11PremiumDao.getListDT11PremiumPlus();
		rule11_planReferPremiumPlus = dt11PremiumDao.getPlanReferPremiumPlus();
		rule11_BMIChildAndAdult = dt11BmiDao.getAllBMIChildAndAdult();
		rule11_BMIBaby = dt11BmiDao.getDt11BMIBaby();
		rule11_verifySexByPlancode = dt11BmiDao.getVerifySexByPlancode();
		rule11_listVerifyNationWPlan = dt11VerifyDao.getAllList(false);
		rule11_listOtwVerifyNationWPlan = dt11VerifyDao.getAllList(true);
		rule11_verifyNationalityUSA = dt11VerifyDao.getDt11VerifyNationalityUSA();
		rule11_verifyNotinalrity = dt11VerifyDao.getAllListVerifyNationality();
		rule11_BMIDocument = dt11BmiDao.getDt11BMIDocument();
		rule11Alcohol = rule11DT11AlcoholDao.getAll();
		rule11Disease = rule11DiseaseDao.getAll();
		rule11Smoke = rule11DT11SmokingDao.getAll();
		rule11Drug = rule11DT11DrugDao.getAll();
		rule11Examination = rule11ExaminationDao.getAll();
		
		rule11OccuLevPlanAcc = rule11OccuLevPlanAccDao.getAll();
		rule11OccuLevPlanCode = rule11OccuLevPlanCodeDao.getAll();
		rule11OccuLevPlanType = rule11OccuLevPlanTypeDao.getAll();
		rule11OccuSuminsured = rule11OccuSuminsuredDao.getAll();
		
		rule11OccupationAccident = rule11OccupationAccidentDao.getAll();
		
		

		// Rule 12
		claimCodeHistoryMap = rule12ClaimCodeHistoryDao.getClaimCodeHistory();

		// Rule 13
		rule13AmloPilot = rule13Dt13AmloPilotDao.getAllList();
		rule13NonIncomeOccupation = rule13Dt13NonIncomeOccupationDao.getAllList();
		rule13RiskOccupation = rule13Dt13RiskOccupationDao.getAllList();

		// Rule 14
		rule14Exception = rule14ExceptionDao.getCollector();
		rule14ExceptionClaim = rule14ExceptionClaimDao.getCollector();
		rule14ExceptionAppNo = rule14ExceptionAppNoDao.getCollector();
		paDocument = rule14PADocumentDao.getCollector();
		hnwDocument = rule14HnwDocumentDao.getCollector();

		
		// Rule 15
		dupPackagePlan = rule15DupPackagePlanDao.getAll();
		dupPackageProject = rule15DupPackageProjectDao.getAll();
		dupPlanForClient = rule15DupPlanForClientDao.getAll();
		dupPlanForPolicy = rule15DupPlanForPolicyDao.getAll();
		dupPlanForPro = rule15DupPlanForProDao.getAll();
		
		// Rule 16
		payorRelationMap = rule16PayorRelationDao.getRelationMap();
		payorOccupationMap = rule16PayorOccupationDao.getOccupationMap();
		claimHistoryMap = rule16ClaimHistoryDao.getAll();
		verifyNationality = rule16VerifyNationalityDao.getAll();
		verifyNationalityUSA = rule16VerifyNationalityUsaDao.getAll();
		femaleDiseaseList = rule16DT16FemaleDiseaseListDao.getAll();
		rule16Alcohol = rule16DT16AlcoholDao.getAll();
		dt16MibAmlo = rule16DT16MibAmloDao.getAlll();
		dt16MibHighRisk = rule16DT16MibHighRiskDao.getAll();

		// Rule 17
		beneficiaryRelationMap = rule17BeneficiaryRelationDao.getBeneficiaryRelation();
		dt17MIBAMLOList = rule17MIBAMLODao.getAll();
		
		// Rule 21
		dt21kyc = rule21KYCDao.getAll(); 
		logger.info("======== End Load Application Cache ======");

	}
	
//	public HashMap<String, PlanType> getPlanTypeCacheMap() {
//		return planTypeCacheMap;
//	}
//
//	public void setPlanTypeCacheMap(HashMap<String, PlanType> planTypeCacheMap) {
//		this.planTypeCacheMap = planTypeCacheMap;
//	}

	public HashMap<String, DT16MibHighRisk> getDt16MibHighRisk() {
		return dt16MibHighRisk;
	}

	public void setDt16MibHighRisk(HashMap<String, DT16MibHighRisk> dt16MibHighRisk) {
		this.dt16MibHighRisk = dt16MibHighRisk;
	}

	public HashMap<String, DT16MibAmlo> getDt16MibAmlo() {
		return dt16MibAmlo;
	}

	public void setDt16MibAmlo(HashMap<String, DT16MibAmlo> dt16MibAmlo) {
		this.dt16MibAmlo = dt16MibAmlo;
	}

	public HashMap<String, DT16Alcohol> getRule16Alcohol() {
		return rule16Alcohol;
	}

	public void setRule16Alcohol(HashMap<String, DT16Alcohol> rule16Alcohol) {
		this.rule16Alcohol = rule16Alcohol;
	}

	public HashMap<String, DT16FemaleDiseaseList> getFemaleDiseaseList() {
		return femaleDiseaseList;
	}

	public void setFemaleDiseaseList(HashMap<String, DT16FemaleDiseaseList> femaleDiseaseList) {
		this.femaleDiseaseList = femaleDiseaseList;
	}

	public HashMap<String, DT16VerifyNationalityUSA> getVerifyNationalityUSA() {
		return verifyNationalityUSA;
	}

	public void setVerifyNationalityUSA(HashMap<String, DT16VerifyNationalityUSA> verifyNationalityUSA) {
		this.verifyNationalityUSA = verifyNationalityUSA;
	}

	public HashMap<String, DT16VerifyNationality> getVerifyNationality() {
		return verifyNationality;
	}

	public void setVerifyNationality(HashMap<String, DT16VerifyNationality> verifyNationality) {
		this.verifyNationality = verifyNationality;
	}

	public HashMap<String, PayorRelation> getPayorRelationMap() {
		return payorRelationMap;
	}

	public HashMap<String, PayorOccupation> getPayorOccupationMap() {
		return payorOccupationMap;
	}

	public void setPayorOccupationMap(HashMap<String, PayorOccupation> payorOccupationMap) {
		this.payorOccupationMap = payorOccupationMap;
	}

	public void setPayorRelationMap(HashMap<String, PayorRelation> payorRelationMap) {
		this.payorRelationMap = payorRelationMap;
	}

	public HashMap<String, DT16ClaimHistory> getClaimHistoryMap() {
		return claimHistoryMap;
	}

	public void setClaimHistoryMap(HashMap<String, DT16ClaimHistory> claimHistoryMap) {
		this.claimHistoryMap = claimHistoryMap;
	}

	public HashMap<String, MsSuperVIP> getMsSuperVip() {
		return msSuperVip;
	}

	public void setMsSuperVip(HashMap<String, MsSuperVIP> msSuperVip) {
		this.msSuperVip = msSuperVip;
	}

	public HashMap<String, List<String>> getMsParameter() {
		return msParameter;
	}

	public void setMsParameter(HashMap<String, List<String>> msParameter) {
		this.msParameter = msParameter;
	}

	public void setRule13AmloPilot(HashMap<String, DT13AmloPilot> amloPilotCashMap) {
		this.rule13AmloPilot = amloPilotCashMap;
	}

	public DT13AmloPilot getRule13AmloPilot(String key) {
		return rule13AmloPilot.get(key);
	}

	public void setRule13NonIncomeOccupation(HashMap<String, DT13NonIncomeOccupation> nonIncomOccupationCashMap) {
		this.rule13NonIncomeOccupation = nonIncomOccupationCashMap;
	}

	public DT13NonIncomeOccupation getRule13NonIncomeOccupation(String key) {
		return rule13NonIncomeOccupation.get(key);
	}

	public void setRule13RiskOccupation(HashMap<String, DT13RiskOccupation> amloPilotCashMap) {
		this.rule13RiskOccupation = amloPilotCashMap;
	}

	public DT13RiskOccupation getRule13RiskOccupation(String key) {
		return rule13RiskOccupation.get(key);
	}

	public HashMap<String, List<String>> getMsPremiumPayment() {
		return msPremiumPayment;
	}

	public List<DT03SumByPlan> getRule03_dt03SumByPlan() {
		return rule03_dt03SumByPlan;
	}

	public void setRule03_dt03SumByPlan(List<DT03SumByPlan> rule03_dt03SumByPlan) {
		this.rule03_dt03SumByPlan = rule03_dt03SumByPlan;
	}

	public void setMsPremiumPayment(HashMap<String, List<String>> msPremiumPayment) {
		this.msPremiumPayment = msPremiumPayment;
	}

	public List<String> getChannelRuleCashMap(String key) {
		return channelRuleCashMap.get(key);
	}

	public List<Dt06Uwqag> getRule06Dt06UwqagMap(String key) {
		return rule06_dt06UwqagMap.get(key);
	}

	public void setRule06Dt06UwqagMap(HashMap<String, List<Dt06Uwqag>> rule06_dt06UwqagMap) {
		this.rule06_dt06UwqagMap = rule06_dt06UwqagMap;
	}

	public DT06MapMedicalDisease getDt06MapMedicalDisease(String key) {
		return rule06_dt06MapMedicalDisease.get(key);
	}

	public void setDt06MapMedicalDisease(HashMap<String, DT06MapMedicalDisease> rule06_dt06MapMedicalDisease) {
		this.rule06_dt06MapMedicalDisease = rule06_dt06MapMedicalDisease;
	}

	public List<DT06UWRQ> getDt06Uwrq(String key) {
		return rule06_dt06Uwrq.get(key);
	}

	public void setDt06Uwrq(HashMap<String, List<DT06UWRQ>> rule06_dt06Uwrq) {
		this.rule06_dt06Uwrq = rule06_dt06Uwrq;
	}

	public List<DT06DiabetesDocument> getDT06DiabetesDocument(String key) {
		return rule06_dt06DiabetesDocument.get(key);
	}

	public MappingClientLocationCode getMappingClientLocationCode(String key) {
		return mas_mappingClientLocationCode.get(key);
	}

	public HashMap<String, PlanHeader> getPlanHeaderAppCashMap() {
		return planHeaderAppCashMap;
	}

	public void setPlanHeaderAppCashMap(HashMap<String, PlanHeader> planHeaderAppCashMap) {
		this.planHeaderAppCashMap = planHeaderAppCashMap;
	}

	public void setChannelRuleCashMap(HashMap<String, List<String>> channelRuleCashMap) {
		this.channelRuleCashMap = channelRuleCashMap;
	}

	public HashMap<String, DT17BeneficiaryRelation> getBeneficiaryRelationMap() {
		return beneficiaryRelationMap;
	}

	public void setBeneficiaryRelationMap(HashMap<String, DT17BeneficiaryRelation> beneficiaryRelationMap) {
		this.beneficiaryRelationMap = beneficiaryRelationMap;
	}

	public HashMap<String, List<DT03ACCSum>> getRule03_dt03ACCSum() {
		return rule03_dt03ACCSum;
	}

	public void setRule03_dt03ACCSum(HashMap<String, List<DT03ACCSum>> rule03_dt03ACCSum) {
		this.rule03_dt03ACCSum = rule03_dt03ACCSum;
	}

	public List<DT03ACCSumMax> getRule03_dt03ACCSumMax() {
		return rule03_dt03ACCSumMax;
	}

	public void setRule03_dt03ACCSumMax(List<DT03ACCSumMax> rule03_dt03ACCSumMax) {
		this.rule03_dt03ACCSumMax = rule03_dt03ACCSumMax;
	}

	public HashMap<String, List<DT03DiagnosisSumInsuredPT>> getRule03_dt03DiagnosisPT() {
		return rule03_dt03DiagnosisPT;
	}

	public void setRule03_dt03DiagnosisPT(HashMap<String, List<DT03DiagnosisSumInsuredPT>> rule03_dt03DiagnosisPT) {
		this.rule03_dt03DiagnosisPT = rule03_dt03DiagnosisPT;
	}

	public HashMap<String, List<DT03DiagnosisSumInsuredPG>> getRule03_dt03DiagnosisPG() {
		return rule03_dt03DiagnosisPG;
	}

	public void setRule03_dt03DiagnosisPG(HashMap<String, List<DT03DiagnosisSumInsuredPG>> rule03_dt03DiagnosisPG) {
		this.rule03_dt03DiagnosisPG = rule03_dt03DiagnosisPG;
	}

	public HashMap<String, DT03OPDSum> getRule03_dt03OPDSum() {
		return rule03_dt03OPDSum;
	}

	public void setRule03_dt03OPDSum(HashMap<String, DT03OPDSum> rule03_dt03OPDSum) {
		this.rule03_dt03OPDSum = rule03_dt03OPDSum;
	}

	public HashMap<String, List<DT03HealthSuminsured>> getRule03_dt03HealthSuminsured() {
		return rule03_dt03HealthSuminsured;
	}
	
	public List<DT03HealthSuminsuredInclude> getRule03_dt03HealthSumInClude() {
		return rule03_dt03HealthSumInClude;
	}

	public void setRule03_dt03HealthSumInClude(List<DT03HealthSuminsuredInclude> rule03_dt03HealthSumInClude) {
		this.rule03_dt03HealthSumInClude = rule03_dt03HealthSumInClude;
	}

	public List<Dt03BasicPlanSumInsured> getRule03_dt03BasicPlanSumInsured() {
		return rule03_dt03BasicPlanSumInsured;
	}

	public void setRule03_dt03BasicPlanSumInsured(List<Dt03BasicPlanSumInsured> rule03_dt03BasicPlanSumInsured) {
		this.rule03_dt03BasicPlanSumInsured = rule03_dt03BasicPlanSumInsured;
	}

	public List<Dt03DisorderAndAccident> getRule03_dT03DisorderAndAccident() {
		return rule03_dT03DisorderAndAccident;
	}

	public void setRule03_dT03DisorderAndAccident(List<Dt03DisorderAndAccident> rule03_dT03DisorderAndAccident) {
		this.rule03_dT03DisorderAndAccident = rule03_dT03DisorderAndAccident;
	}

	public void setRule03_dt03HealthSuminsured(HashMap<String, List<DT03HealthSuminsured>> rule03_dt03HealthSuminsured) {
		this.rule03_dt03HealthSuminsured = rule03_dt03HealthSuminsured;
	}
	
	public List<DT03PaidUpSumInsured> getRule03_dT03PaidUpSumInsured() {
		return rule03_dT03PaidUpSumInsured;
	}

	public void setRule03_dT03PaidUpSumInsured(List<DT03PaidUpSumInsured> rule03_dT03PaidUpSumInsured) {
		this.rule03_dT03PaidUpSumInsured = rule03_dT03PaidUpSumInsured;
	}

	public List<DT03PlanCodeSumInsuredHistory> getRule03_dT03PlanCodeSumInsuredHistory() {
		return rule03_dT03PlanCodeSumInsuredHistory;
	}

	public void setRule03_dT03PlanCodeSumInsuredHistory(
			List<DT03PlanCodeSumInsuredHistory> rule03_dT03PlanCodeSumInsuredHistory) {
		this.rule03_dT03PlanCodeSumInsuredHistory = rule03_dT03PlanCodeSumInsuredHistory;
	}

	public List<DT03PlanTypeeSumInsuredHistory> getRule03_dT03PlanTypeeSumInsuredHistory() {
		return rule03_dT03PlanTypeeSumInsuredHistory;
	}

	public void setRule03_dT03PlanTypeeSumInsuredHistory(
			List<DT03PlanTypeeSumInsuredHistory> rule03_dT03PlanTypeeSumInsuredHistory) {
		this.rule03_dT03PlanTypeeSumInsuredHistory = rule03_dT03PlanTypeeSumInsuredHistory;
	}

	public List<DT03ProjectSumIndured> getRule03_dT03ProjectSumIndured() {
		return rule03_dT03ProjectSumIndured;
	}

	public void setRule03_dT03ProjectSumIndured(List<DT03ProjectSumIndured> rule03_dT03ProjectSumIndured) {
		this.rule03_dT03ProjectSumIndured = rule03_dT03ProjectSumIndured;
	}

	public List<DT03RiderPlanSumInsured> getRule03_dT03_RiderPlanSumInsured() {
		return rule03_dT03_RiderPlanSumInsured;
	}

	public void setRule03_dT03_RiderPlanSumInsured(List<DT03RiderPlanSumInsured> rule03_dT03_RiderPlanSumInsured) {
		this.rule03_dT03_RiderPlanSumInsured = rule03_dT03_RiderPlanSumInsured;
	}

	public List<DT03SumInsuredWithAge> getRule03_dT03_SumInsuredWithAge() {
		return rule03_dT03_SumInsuredWithAge;
	}

	public void setRule03_dT03_SumInsuredWithAge(List<DT03SumInsuredWithAge> rule03_dT03_SumInsuredWithAge) {
		this.rule03_dT03_SumInsuredWithAge = rule03_dT03_SumInsuredWithAge;
	}

	public List<DT03SumInsuredWithMustHaveRider> getRule03_dT03_SumInsuredWithMustHaveRider() {
		return rule03_dT03_SumInsuredWithMustHaveRider;
	}

	public void setRule03_dT03_SumInsuredWithMustHaveRider(
			List<DT03SumInsuredWithMustHaveRider> rule03_dT03_SumInsuredWithMustHaveRider) {
		this.rule03_dT03_SumInsuredWithMustHaveRider = rule03_dT03_SumInsuredWithMustHaveRider;
	}

	public List<DT03FixSumInsured> getRule03_dT03_FixSumInsured() {
		return rule03_dT03_FixSumInsured;
	}

	public void setRule03_dT03_FixSumInsured(List<DT03FixSumInsured> rule03_dT03_FixSumInsured) {
		this.rule03_dT03_FixSumInsured = rule03_dT03_FixSumInsured;
	}

	public List<DT03PackageHSLimit> getRule03_dT03_PackageHSLimit() {
		return rule03_dT03_PackageHSLimit;
	}

	public void setRule03_dT03_PackageHSLimit(List<DT03PackageHSLimit> rule03_dT03_PackageHSLimit) {
		this.rule03_dT03_PackageHSLimit = rule03_dT03_PackageHSLimit;
	}

	public List<DT03ProductAMLO> getRule03_dT03_ProductAMLO() {
		return rule03_dT03_ProductAMLO;
	}

	public void setRule03_dT03_ProductAMLO(List<DT03ProductAMLO> rule03_dT03_ProductAMLO) {
		this.rule03_dT03_ProductAMLO = rule03_dT03_ProductAMLO;
	}

	public List<DT03ChannelSumInsured> getRule03_dT03_ChannelSumInsured() {
		return rule03_dT03_ChannelSumInsured;
	}

	public void setRule03_dT03_ChannelSumInsured(List<DT03ChannelSumInsured> rule03_dT03_ChannelSumInsured) {
		this.rule03_dT03_ChannelSumInsured = rule03_dT03_ChannelSumInsured;
	}

	public List<DT03ChannelSumInsuredEqual> getRule03_dT03_ChannelSumInsuredEqual() {
		return rule03_dT03_ChannelSumInsuredEqual;
	}

	public void setRule03_dT03_ChannelSumInsuredEqual(List<DT03ChannelSumInsuredEqual> rule03_dT03_ChannelSumInsuredEqual) {
		this.rule03_dT03_ChannelSumInsuredEqual = rule03_dT03_ChannelSumInsuredEqual;
	}

	public List<DT03HBIncludeHistoryTele> getRule03_dT03_hbIncludeHistoryTele() {
		return rule03_dT03_hbIncludeHistoryTele;
	}

	public void setRule03_dT03_hbIncludeHistoryTele(List<DT03HBIncludeHistoryTele> rule03_dT03_hbIncludeHistoryTele) {
		this.rule03_dT03_hbIncludeHistoryTele = rule03_dT03_hbIncludeHistoryTele;
	}

	public List<DT03ExtracareHbSuminsured> getRule03_dT03_extracareHbSuminsured() {
		return rule03_dT03_extracareHbSuminsured;
	}

	public void setRule03_dT03_extracareHbSuminsured(List<DT03ExtracareHbSuminsured> rule03_dT03_extracareHbSuminsured) {
		this.rule03_dT03_extracareHbSuminsured = rule03_dT03_extracareHbSuminsured;
	}

	public List<DT03RiskDiagnosisPlanType> getRule03_dT03_riskDiagnosisPlanType() {
		return rule03_dT03_riskDiagnosisPlanType;
	}

	public void setRule03_dT03_riskDiagnosisPlanType(List<DT03RiskDiagnosisPlanType> rule03_dT03_riskDiagnosisPlanType) {
		this.rule03_dT03_riskDiagnosisPlanType = rule03_dT03_riskDiagnosisPlanType;
	}
	
	

	public List<DT03DHealthAgentGroup> getRule03_dT03DHealthAgentGroup() {
		return rule03_dT03DHealthAgentGroup;
	}

	public void setRule03_dT03DHealthAgentGroup(List<DT03DHealthAgentGroup> rule03_dT03DHealthAgentGroup) {
		this.rule03_dT03DHealthAgentGroup = rule03_dT03DHealthAgentGroup;
	}

	public List<DT03AccidentForTakaful> getRule03_dT03AccidentForTakaful() {
		return rule03_dT03AccidentForTakaful;
	}

	public void setRule03_dT03AccidentForTakaful(List<DT03AccidentForTakaful> rule03_dT03AccidentForTakaful) {
		this.rule03_dT03AccidentForTakaful = rule03_dT03AccidentForTakaful;
	}

	public List<DT03AccidentForTakafulNc> getRule03_dT03AccidentForTakafulNc() {
		return rule03_dT03AccidentForTakafulNc;
	}

	public void setRule03_dT03AccidentForTakafulNc(List<DT03AccidentForTakafulNc> rule03_dT03AccidentForTakafulNc) {
		this.rule03_dT03AccidentForTakafulNc = rule03_dT03AccidentForTakafulNc;
	}

	public HashMap<String, DT04AppMdc> getRule04_dt04AppMdc() {
		return rule04_dt04AppMdc;
	}

	public void setRule04_dt04AppMdc(HashMap<String, DT04AppMdc> rule04_dt04AppMdc) {
		this.rule04_dt04AppMdc = rule04_dt04AppMdc;
	}

	public void setRule04_dt04ExceptPlanCode(HashMap<String, String> rule04_dt04ExceptPlanCode) {
		this.rule04_dt04ExceptPlanCode = rule04_dt04ExceptPlanCode;
	}

	public HashMap<String, String> getRule04_dt04ExceptPlanCode() {
		return rule04_dt04ExceptPlanCode;
	}

	public HashMap<String, DT04PremiumPaymentSpecial> getRule04_dt04PremiumPaymentSpecial() {
		return rule04_dt04PremiumPaymentSpecial;
	}

	public void setRule04_dt04PremiumPaymentSpecial(
			HashMap<String, DT04PremiumPaymentSpecial> rule04_dt04PremiumPaymentSpecial) {
		this.rule04_dt04PremiumPaymentSpecial = rule04_dt04PremiumPaymentSpecial;
	}

	public HashMap<String, Set<String>> getRule05_specialPaymentChannelMap() {
		return rule05_specialPaymentChannelMap;
	}

	public void setRule05_specialPaymentChannelMap(HashMap<String, Set<String>> rule05_specialPaymentChannelMap) {
		this.rule05_specialPaymentChannelMap = rule05_specialPaymentChannelMap;
	}

	public HashMap<String, List<String>> getRule05_premiumPayment() {
		return rule05_premiumPayment;
	}

	public void setRule05_premiumPayment(HashMap<String, List<String>> rule05_premiumPayment) {
		this.rule05_premiumPayment = rule05_premiumPayment;
	}

	public List<String> getDt17MIBAMLOList() {
		return dt17MIBAMLOList;
	}

	public void setDt17MIBAMLOList(List<String> dt17mibamloList) {
		dt17MIBAMLOList = dt17mibamloList;
	}

	public HashMap<String, Document> getDocumentCashMap() {
		return documentCashMap;
	}

	public void setDocumentCashMap(HashMap<String, Document> documentCashMap) {
		this.documentCashMap = documentCashMap;
	}

	public AppNo getAppNoAppCashMap(String key) {
		return appNoAppCashMap.get(key);
	}

	public void setAppNoAppCashMap(HashMap<String, AppNo> appNoAppCashMap) {
		this.appNoAppCashMap = appNoAppCashMap;
	}

	public HashMap<String, String> getClaimCodeHistoryMap() {
		return claimCodeHistoryMap;
	}

	public void setClaimCodeHistoryMap(HashMap<String, String> claimCodeHistoryMap) {
		this.claimCodeHistoryMap = claimCodeHistoryMap;
	}

	public HashMap<String, String> getPlanDescMap() {
		return planDescMap;
	}

	public void setPlanDescMap(HashMap<String, String> planDescMap) {
		this.planDescMap = planDescMap;
	}

	public Dt06MapNmlType getDt06MapNmlType(String key) {
		return rule06_dt06MapNmlTypeMap.get(key);
	}

	public void setDt06MapNmlType(HashMap<String, Dt06MapNmlType> rule06_dt06MapNmlTypeMap) {
		this.rule06_dt06MapNmlTypeMap = rule06_dt06MapNmlTypeMap;
	}

	public HashMap<String, DT07MIB> getRule07_mibMap() {
		return rule07_mibMap;
	}

	public void setRule07_mibMap(HashMap<String, DT07MIB> rule07_mibMap) {
		this.rule07_mibMap = rule07_mibMap;
	}

	public HashMap<String, DT07MIBSpecial> getRule07_mibSpecialMap() {
		return rule07_mibSpecialMap;
	}

	public void setRule07_mibSpecialMap(HashMap<String, DT07MIBSpecial> rule07_mibSpecialMap) {
		this.rule07_mibSpecialMap = rule07_mibSpecialMap;
	}

	public HashMap<String, DT07MIBHnw> getRule07_mibHnwMap() {
		return rule07_mibHnwMap;
	}

	public void setRule07_mibHnwMap(HashMap<String, DT07MIBHnw> rule07_mibHnwMap) {
		this.rule07_mibHnwMap = rule07_mibHnwMap;
	}

	public HashMap<String, String> getExceptionClaimCodeMap() {
		return exceptionClaimCodeMap;
	}

	public void setExceptionClaimCodeMap(HashMap<String, String> exceptionClaimCodeMap) {
		this.exceptionClaimCodeMap = exceptionClaimCodeMap;
	}

	public HashMap<String, PlanPermission> getPlanPermissionAppCashMap() {
		return planPermissionAppCashMap;
	}

	public void setPlanPermissionAppCashMap(HashMap<String, PlanPermission> planPermissionAppCashMap) {
		this.planPermissionAppCashMap = planPermissionAppCashMap;
	}

	public DT14Exception getRule14Exception(String key) {
		return rule14Exception.get(key);
	}

	public void setRule14Exception(HashMap<String, DT14Exception> rule14Exception) {
		this.rule14Exception = rule14Exception;
	}

	public HashMap<String, DT14ExceptionAppNo> getRule14ExceptionAppNo() {
		return rule14ExceptionAppNo;
	}

	public void setRule14ExceptionAppNo(HashMap<String, DT14ExceptionAppNo> rule14ExceptionAppNo) {
		this.rule14ExceptionAppNo = rule14ExceptionAppNo;
	}

	public List<DT14DocumentClaim> getRule14ExceptionClaim(String key) {
		return rule14ExceptionClaim.get(key);
	}

	public void setRule14ExceptionClaim(HashMap<String, List<DT14DocumentClaim>> rule14ExceptionClaim) {
		this.rule14ExceptionClaim = rule14ExceptionClaim;
	}

	public List<DT14PADocument> getPADocument(String key) {
		return paDocument.get(key);
	}

	public void setPADocument(HashMap<String, List<DT14PADocument>> dt14paDocument) {
		paDocument = dt14paDocument;
	}

	public List<DT14HnwDocument> getHnwDocument(String key) {
		return hnwDocument.get(key);
	}

	public void setHnwDocument(HashMap<String, List<DT14HnwDocument>> dt14HnwDocument) {
		hnwDocument = dt14HnwDocument;
	}

	// ===> get set rule 11 <===
	public HashMap<String, DT11RiskAreasOffice> getRule11_riskAreasOfficeMap() {
		return rule11_riskAreasOfficeMap;
	}

	public void setRule11_riskAreasOfficeMap(HashMap<String, DT11RiskAreasOffice> rule11_riskAreasOfficeMap) {
		this.rule11_riskAreasOfficeMap = rule11_riskAreasOfficeMap;
	}

	public HashMap<String, DT11RiskAreasHouseRegis> getRule11_riskAreasHouseRegisMap() {
		return rule11_riskAreasHouseRegisMap;
	}

	public void setRule11_riskAreasHouseRegisMap(
			HashMap<String, DT11RiskAreasHouseRegis> rule11_riskAreasHouseRegisMap) {
		this.rule11_riskAreasHouseRegisMap = rule11_riskAreasHouseRegisMap;
	}

	public HashMap<String, DT11RiskAreasPrestAddr> getRule11_riskAreasPrestAddr() {
		return rule11_riskAreasPrestAddr;
	}

	public void setRule11_riskAreasPrestAddr(HashMap<String, DT11RiskAreasPrestAddr> rule11_riskAreasPrestAddr) {
		this.rule11_riskAreasPrestAddr = rule11_riskAreasPrestAddr;
	}

	public HashMap<String, DT11RiskMobileNumber> getRule11_riskMobileNumber() {
		return rule11_riskMobileNumber;
	}

	public void setRule11_riskMobileNumber(HashMap<String, DT11RiskMobileNumber> rule11_riskkMobileNumber) {
		this.rule11_riskMobileNumber = rule11_riskkMobileNumber;
	}

	public HashMap<String, DT11RejectDeclinePostpone> getRule11_rejectDeclinePostpone() {
		return rule11_rejectDeclinePostpone;
	}

	public void setRule11_rejectDeclinePostpone(
			HashMap<String, DT11RejectDeclinePostpone> rule11_rejectDeclinePostpone) {
		this.rule11_rejectDeclinePostpone = rule11_rejectDeclinePostpone;
	}

	public HashMap<String, DT11PremiumPlus> getRule11_premiumPlus() {
		return rule11_premiumPlus;
	}

	public void setRule11_premiumPlus(HashMap<String, DT11PremiumPlus> rule11_premiumPlus) {
		this.rule11_premiumPlus = rule11_premiumPlus;
	}

	public HashMap<String, DT11PlanReferPremiumPlus> getRule11_planReferPremiumPlus() {
		return rule11_planReferPremiumPlus;
	}

	public void setRule11_planReferPremiumPlus(HashMap<String, DT11PlanReferPremiumPlus> rule11_planReferPremiumPlus) {
		this.rule11_planReferPremiumPlus = rule11_planReferPremiumPlus;
	}

	public HashMap<String, DT11VerifyNationUSA> getRule11_verifyNationalityUSA() {
		return rule11_verifyNationalityUSA;
	}

	public void setRule11_verifyNationalityUSA(HashMap<String, DT11VerifyNationUSA> rule11_verifyNationalityUSA) {
		this.rule11_verifyNationalityUSA = rule11_verifyNationalityUSA;
	}

	public HashMap<String, DT11BMIChildAndAdult> getRule11_BMIChildAndAdult() {
		return rule11_BMIChildAndAdult;
	}

	public void setRule11_BMIChildAndAdult(HashMap<String, DT11BMIChildAndAdult> rule11_BMIChildAndAdult) {
		this.rule11_BMIChildAndAdult = rule11_BMIChildAndAdult;
	}

	public HashMap<String, DT11BMIBaby> getRule11_BMIBaby() {
		return rule11_BMIBaby;
	}

	public void setRule11_BMIBaby(HashMap<String, DT11BMIBaby> rule11_BMIBaby) {
		this.rule11_BMIBaby = rule11_BMIBaby;
	}

	public HashMap<String, DT11VerifySexByPlancode> getRule11_verifySexByPlancode() {
		return rule11_verifySexByPlancode;
	}

	public void setRule11_verifySexByPlancode(HashMap<String, DT11VerifySexByPlancode> rule11_verifySexByPlancode) {
		this.rule11_verifySexByPlancode = rule11_verifySexByPlancode;
	}

	public List<DT11BMIDocument> getRule11_BMIDocument() {
		return rule11_BMIDocument;
	}

	public void setRule11_BMIDocument(List<DT11BMIDocument> rule11_BMIDocument) {
		this.rule11_BMIDocument = rule11_BMIDocument;
	}

	public HashMap<String, DT11VerifyNationWithPlan> getRule11_listOtwVerifyNationWPlan() {
		return rule11_listOtwVerifyNationWPlan;
	}

	public void setRule11_listOtwVerifyNationWPlan(
			HashMap<String, DT11VerifyNationWithPlan> rule11_listOtwVerifyNationWPlan) {
		this.rule11_listOtwVerifyNationWPlan = rule11_listOtwVerifyNationWPlan;
	}

	public HashMap<String, DT11VerifyNationWithPlan> getRule11_listVerifyNationWPlan() {
		return rule11_listVerifyNationWPlan;
	}

	public void setRule11_listVerifyNationWPlan(
			HashMap<String, DT11VerifyNationWithPlan> rule11_listVerifyNationWPlan) {
		this.rule11_listVerifyNationWPlan = rule11_listVerifyNationWPlan;
	}

	public HashMap<String, List<DT11VerifyNationality>> getRule11_verifyNotinalrity() {
		return rule11_verifyNotinalrity;
	}

	public void setRule11_verifyNotinalrity(HashMap<String, List<DT11VerifyNationality>> rule11_verifyNotinalrity) {
		this.rule11_verifyNotinalrity = rule11_verifyNotinalrity;
	}
	
	public HashMap<String, DT11Alcohol> getRule11Alcohol() {
		return rule11Alcohol;
	}

	public void setRule11Alcohol(HashMap<String, DT11Alcohol> rule11Alcohol) {
		this.rule11Alcohol = rule11Alcohol;
	}
	
	public HashMap<String, DT11Disease> getRule11Disease() {
		return rule11Disease;
	}

	public void setRule11Disease(HashMap<String, DT11Disease> rule11Disease) {
		this.rule11Disease = rule11Disease;
	}

	public List<DT11Smoke> getDT11Smoke() {
		return rule11Smoke;
	}

	public void setDT11Smoke(List<DT11Smoke> rule11Smoke) {
		this.rule11Smoke = rule11Smoke;
	}
	
	public List<DT11Drug> getDT11Drug() {
		return rule11Drug;
	}

	public void setDT11Drug(List<DT11Drug> rule11Drug) {
		this.rule11Drug = rule11Drug;
	}

	public List<DT11Examination> getRule11Examination() {
		return rule11Examination;
	}

	public void setRule11Examination(List<DT11Examination> rule11Examination) {
		this.rule11Examination = rule11Examination;
	}

	public List<DT11OccuLevPlanAcc> getRule11OccuLevPlanAcc() {
		return rule11OccuLevPlanAcc;
	}

	public void setRule11OccuLevPlanAcc(List<DT11OccuLevPlanAcc> rule11OccuLevPlanAcc) {
		this.rule11OccuLevPlanAcc = rule11OccuLevPlanAcc;
	}

	public List<DT11OccuLevPlanCode> getRule11OccuLevPlanCode() {
		return rule11OccuLevPlanCode;
	}

	public void setRule11OccuLevPlanCode(List<DT11OccuLevPlanCode> rule11OccuLevPlanCode) {
		this.rule11OccuLevPlanCode = rule11OccuLevPlanCode;
	}

	public List<DT11OccuLevPlanType> getRule11OccuLevPlanType() {
		return rule11OccuLevPlanType;
	}

	public void setRule11OccuLevPlanType(List<DT11OccuLevPlanType> rule11OccuLevPlanType) {
		this.rule11OccuLevPlanType = rule11OccuLevPlanType;
	}

	public List<DT11OccuSuminsured> getRule11OccuSuminsured() {
		return rule11OccuSuminsured;
	}

	public void setRule11OccuSuminsured(List<DT11OccuSuminsured> rule11OccuSuminsured) {
		this.rule11OccuSuminsured = rule11OccuSuminsured;
	}


	public List<DT11OcuupationAccident> getRule11OccupationAccident() {
		return rule11OccupationAccident;
	}

	public void setRule11OccupationAccident(List<DT11OcuupationAccident> rule11OccupationAccident) {
		this.rule11OccupationAccident = rule11OccupationAccident;
	}

	public List<DT21KYC> getDt21kyc() {
		return dt21kyc;
	}

	public void setDt21kyc(List<DT21KYC> dt21kyc) {
		this.dt21kyc = dt21kyc;
	}
	
	public HashMap<String, PlanType> getPlanTypeCacheMap() {
		return planTypeCacheMap;
	}

	public void setPlanTypeCacheMap(HashMap<String, PlanType> planTypeCacheMap) {
		this.planTypeCacheMap = planTypeCacheMap;
	}

	public HashMap<String, MsInvesmentPlanType> getMsInvesmentPlanType() {
		return msInvesmentPlanType;
	}

	public void setMsInvesmentPlanType(HashMap<String, MsInvesmentPlanType> msInvesmentPlanType) {
		this.msInvesmentPlanType = msInvesmentPlanType;
	}

	public List<MapProject> getMapProjectList() {
		return mapProjectList;
	}

	public void setMapProjectList(List<MapProject> mapProjectList) {
		this.mapProjectList = mapProjectList;
	}

	public List<DT15DupPackagePlan> getDupPackagePlan() {
		return dupPackagePlan;
	}

	public void setDupPackagePlan(List<DT15DupPackagePlan> dupPackagePlan) {
		this.dupPackagePlan = dupPackagePlan;
	}

	public List<DT15DupPackageProject> getDupPackageProject() {
		return dupPackageProject;
	}

	public void setDupPackageProject(List<DT15DupPackageProject> dupPackageProject) {
		this.dupPackageProject = dupPackageProject;
	}

	public List<DT15DupPlanForClient> getDupPlanForClient() {
		return dupPlanForClient;
	}

	public void setDupPlanForClient(List<DT15DupPlanForClient> dupPlanForClient) {
		this.dupPlanForClient = dupPlanForClient;
	}

	public List<DT15DupPlanForPolicy> getDupPlanForPolicy() {
		return dupPlanForPolicy;
	}

	public void setDupPlanForPolicy(List<DT15DupPlanForPolicy> dupPlanForPolicy) {
		this.dupPlanForPolicy = dupPlanForPolicy;
	}

	public List<DT15DupPlanForPro> getDupPlanForPro() {
		return dupPlanForPro;
	}

	public void setDupPlanForPro(List<DT15DupPlanForPro> dupPlanForPro) {
		this.dupPlanForPro = dupPlanForPro;
	}

	
	
	
	// ===> end get set rule 11 <===
	
	
	
	
	
}
