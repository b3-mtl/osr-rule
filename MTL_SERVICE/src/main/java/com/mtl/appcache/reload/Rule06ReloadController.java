package com.mtl.appcache.reload;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule06Dt06MapMedicalDiseaseDao;
import com.mtl.appcache.dao.Rule06Dt06MapNmlTypeDao;
import com.mtl.appcache.dao.Rule06Dt06UwqagDao;
import com.mtl.appcache.dao.Rule06Dt06UwrqDao;
import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.DT06UWRQ;
import com.mtl.rule.model.Dt06MapNmlType;
import com.mtl.rule.model.Dt06Uwqag;


@Controller
@RequestMapping("rule06ReloadCache")
@CrossOrigin(origins = "*")
public class Rule06ReloadController {
	
	private static final Logger logger = LoggerFactory.getLogger(Rule06ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule06Dt06UwqagDao rule06Dt06UwqagDao;
	
	@Autowired
	private Rule06Dt06UwrqDao rule06Dt06UwrqDao;
	
	@Autowired
	private Rule06Dt06MapNmlTypeDao rule06Dt06MapNmlTypeDao;
	
	@Autowired
	private Rule06Dt06MapMedicalDiseaseDao rule06Dt06MapMedicalDiseaseDao;
	
	
	@GetMapping("/uwqag")
	@ResponseBody
	public String reloadUWQAG( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule06 UWQAG started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<Dt06Uwqag>> rule6DecisionTableWQAGMap = new HashMap< String , List<Dt06Uwqag>>(); 
			rule6DecisionTableWQAGMap = rule06Dt06UwqagDao.getCollector();
			applicationCache.setRule06Dt06UwqagMap(rule6DecisionTableWQAGMap); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
//		return "{status : '"+status+"', message : '"+message+"' }";
	}
	@GetMapping("/uwrq")
	@ResponseBody
	public String reloadUWRQ( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule06 UWRQ started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<DT06UWRQ> > rule6DecisionTableUWRQMap = new HashMap< String , List<DT06UWRQ> >(); 
			rule6DecisionTableUWRQMap = rule06Dt06UwrqDao.getAllList();
			applicationCache.setDt06Uwrq(rule6DecisionTableUWRQMap); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		
		return status;
	}
	@GetMapping("/mapNmlType")
	@ResponseBody
	public String reloadMapNmlType( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule06 UWRQ started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , Dt06MapNmlType> rule6DecisionTableMapNmlType = new HashMap< String , Dt06MapNmlType>(); 
			rule6DecisionTableMapNmlType = rule06Dt06MapNmlTypeDao.getCollector();
			applicationCache.setDt06MapNmlType(rule6DecisionTableMapNmlType); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		
		return status;
	}
	
	
	@GetMapping("/mapMedicalDesease")
	@ResponseBody
	public String reloadMapMedicalDesease( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule06 UWRQ started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT06MapMedicalDisease> rule6DecisionTableMapMedicalDesease = new HashMap< String , DT06MapMedicalDisease>(); 
			rule6DecisionTableMapMedicalDesease = rule06Dt06MapMedicalDiseaseDao.getAllDoc();
			applicationCache.setDt06MapMedicalDisease(rule6DecisionTableMapMedicalDesease); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		
		return status;
	}
}
