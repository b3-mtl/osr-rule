package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule16VerifyNationalityDao;
import com.mtl.rule.model.DT16VerifyNationality;

@Controller
@RequestMapping("rule16ReloadCache")
@CrossOrigin(origins = "*")
public class Rule16ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule16ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule16VerifyNationalityDao rule16VerifyNationalityDao;
	
	@GetMapping("/verifyNationality")
	@ResponseBody
	public String reloadVerifyNationality( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Verify Nationality started ]]");
		String message = "";
		String status = "";
		try {
			HashMap<String, DT16VerifyNationality> verifyNationality = new HashMap<String, DT16VerifyNationality>();
			verifyNationality = rule16VerifyNationalityDao.getAll();
			applicationCache.setVerifyNationality(verifyNationality);
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	
	@GetMapping("/verifyNationalityUsa")
	@ResponseBody
	public String reloadVerifyNationalityUsa( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Verify Nationality USA started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/mibAmlo")
	@ResponseBody
	public String reloadMibAmlo( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Mib Amlo started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/mibHighRisk")
	@ResponseBody
	public String reloadMibHighRisk( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Mib High Risk started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/occupation")
	@ResponseBody
	public String reloadOccupation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Occupation started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/alcohol")
	@ResponseBody
	public String reloadAlcohol( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Alcohol started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/cliamHistory")
	@ResponseBody
	public String reloadCliamHistory( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Cliam History started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/femaleDiseaseList")
	@ResponseBody
	public String reloadFemaleDiseaseList( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Female Disease List started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/payorRelation")
	@ResponseBody
	public String reloadPayorRelation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule16 Payor Relation started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
