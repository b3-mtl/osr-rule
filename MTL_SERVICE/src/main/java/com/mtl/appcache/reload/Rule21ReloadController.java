package com.mtl.appcache.reload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule17BeneficiaryRelationDao;
import com.mtl.appcache.dao.Rule17MIBAMLODao;
import com.mtl.appcache.dao.Rule21KYCDao;
import com.mtl.rule.model.DT17BeneficiaryRelation;
import com.mtl.rule.model.DT21KYC;

@Controller
@RequestMapping("rule21ReloadCache")
@CrossOrigin(origins = "*")
public class Rule21ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule21ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule21KYCDao  rule21KYCDAO;
	
	
	@GetMapping("/kyc")
	@ResponseBody
	public String reloadkyc( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule21 kyc Relation started ]]");
		String message = "";
		String status = "";
		try {
			List<DT21KYC> rule21kyc = new ArrayList<DT21KYC>(); 
			rule21kyc = rule21KYCDAO.getAll();
			applicationCache.setDt21kyc(rule21kyc);
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	}
	

