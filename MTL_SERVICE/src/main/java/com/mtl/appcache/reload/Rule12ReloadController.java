package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule12ClaimCodeHistoryDao;

@Controller
@RequestMapping("rule12ReloadCache")
@CrossOrigin(origins = "*")
public class Rule12ReloadController {

	
	private static final Logger logger = LoggerFactory.getLogger(Rule12ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule12ClaimCodeHistoryDao rule12ClaimCodeHistoryDao;
	
	
	@GetMapping("/claimCodeHistory")
	@ResponseBody
	public String reloadBeneficiaryRelation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule12 Claim Code History started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
