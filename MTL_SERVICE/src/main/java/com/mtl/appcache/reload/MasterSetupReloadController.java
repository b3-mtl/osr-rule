package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.common.AppNoDao;
import com.mtl.appcache.dao.common.DocumentDao;
import com.mtl.appcache.dao.common.PlanPermissionDao;
import com.mtl.model.common.AppNo;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.underwriting.Document;

@Controller
@RequestMapping("masterSetupReloadCache")
@CrossOrigin(origins = "*")
public class MasterSetupReloadController {
	
	private static final Logger logger = LoggerFactory.getLogger(MasterSetupReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private AppNoDao appNoDao;
	
	@Autowired
	private DocumentDao documentDao;
	
	@Autowired
	private PlanPermissionDao  planPermissionDao;
	
	@GetMapping("/appNo")
	@ResponseBody
	public String reloadAppNo( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup App No started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , AppNo> masterSetupDecisionTableAppNo = new HashMap< String , AppNo>(); 
			masterSetupDecisionTableAppNo = appNoDao.getAllAppNo();
			applicationCache.setAppNoAppCashMap(masterSetupDecisionTableAppNo); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/document")
	@ResponseBody
	public String reloadDocument( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Document Description started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , Document> masterSetupDecisionTableDocumentDescription = new HashMap< String , Document>(); 
			masterSetupDecisionTableDocumentDescription = documentDao.getDocument();
			applicationCache.setDocumentCashMap(masterSetupDecisionTableDocumentDescription); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/planPermission")
	@ResponseBody
	public String reloadPlanPermission( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Plan Permission started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/manageApptype")
	@ResponseBody
	public String reloadManageApptype( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Manage Apptype started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/registeredSystem")
	@ResponseBody
	public String reloadRegisteredSystem( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Registered System started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	
	@GetMapping("/dataCollectorMaster")
	@ResponseBody
	public String reloadDataCollectorMaster( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Data Collector Master started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/planDescription")
	@ResponseBody
	public String reloadPlanDescription( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Plan Description started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/superVIP")
	@ResponseBody
	public String reloadSuperVIP( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup Super VIP started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/parameter")
	@ResponseBody
	public String reloadMsParameter( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache MasterSetup MS Parameter started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , PlanPermission> masterSetupDecisionTablePlanPermission = new HashMap< String , PlanPermission>(); 
//			masterSetupDecisionTablePlanPermission = planPermissionDao.getAllPlanPermission();
//			applicationCache.setPlanPermissionAppCashMap(masterSetupDecisionTablePlanPermission); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
