package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;

@Controller
@RequestMapping("rule07ReloadCache")
@CrossOrigin(origins = "*")
public class Rule07ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule07ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@GetMapping("/mib")
	@ResponseBody
	public String reloadMib( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule07 MIB started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/mibGio")
	@ResponseBody
	public String reloadMibGio( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule07 MIB GIO started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/mibHnw")
	@ResponseBody
	public String reloadMibHnw( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule07 MIB HNW started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/mibSpecial")
	@ResponseBody
	public String reloadMibSpecail( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule07 MIB SPECIAL started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
