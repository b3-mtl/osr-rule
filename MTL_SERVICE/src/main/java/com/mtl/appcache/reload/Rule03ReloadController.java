package com.mtl.appcache.reload;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;

@Controller
@RequestMapping("rule03ReloadCache")
@CrossOrigin(origins = "*")
public class Rule03ReloadController {
	
	private static final Logger logger = LoggerFactory.getLogger(Rule03ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;

	@GetMapping("/accSum")
	@ResponseBody
	public String reloadAccSum( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Acc Sum started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/accSumMax")
	@ResponseBody
	public String reloadAccSumMax( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Acc Sum Max started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/diagSumPG")
	@ResponseBody
	public String reloadDiagSumPG( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Diag Sum PG started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/diagSumPT")
	@ResponseBody
	public String reloadDiagSumPT( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Diag Sum PT started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/healthSum")
	@ResponseBody
	public String reloadHealthSum( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Health Sum started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/opdSum")
	@ResponseBody
	public String reloadOpdSum( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Opd Sum started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/sumByPlan")
	@ResponseBody
	public String reloadSumByPlan( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule03 Sum By Plan started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
//			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
//			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
}
