package com.mtl.appcache.reload;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.ChannelRuleDao;
import com.mtl.appcache.dao.CollectorRuleDao;

@Controller
@RequestMapping("mappingSetupReloadCache")
@CrossOrigin(origins = "*")
public class MappingSetupReloadController {

	private static final Logger logger = LoggerFactory.getLogger(MappingSetupReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private ChannelRuleDao channelRuleDao;
	
	@Autowired
	private CollectorRuleDao collectorRuleDao;
	
	@GetMapping("/ruleChannel")
	@ResponseBody
	public String reloadRuleChannel( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule & Registered System Mapping started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<String>> mappingSetupDecisionTableRuleChannel = new HashMap< String , List<String>>(); 
			mappingSetupDecisionTableRuleChannel = channelRuleDao.getRule();
			applicationCache.setChannelRuleCashMap(mappingSetupDecisionTableRuleChannel); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/ruleCollector")
	@ResponseBody
	public String reloadCollectorRule( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule Engine & Data Collector Mapping started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<String>> mappingSetupDecisionTableRuleCollector = new HashMap< String , List<String>>(); 
			mappingSetupDecisionTableRuleCollector = collectorRuleDao.getCollector();
//			applicationCache.setChannelRuleCashMap(mappingSetupDecisionTableRuleCollector); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/messageMapping")
	@ResponseBody
	public String reloadMessageMapping( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Message Mapping started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , List<String>> mappingSetupDecisionTableRuleCollector = new HashMap< String , List<String>>(); 
//			mappingSetupDecisionTableRuleCollector = collectorRuleDao.getCollector();
//			applicationCache.setChannelRuleCashMap(mappingSetupDecisionTableRuleCollector); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
