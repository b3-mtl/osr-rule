package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;

@Controller
@RequestMapping("rule11ReloadCache")
@CrossOrigin(origins = "*")
public class Rule11ReloadController {

	
	private static final Logger logger = LoggerFactory.getLogger(Rule11ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@GetMapping("/riskAreasOffice")
	@ResponseBody
	public String reloadRiskAreasOffice( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Risk Areas Office started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/riskAreasHouseRegis")
	@ResponseBody
	public String reloadRiskAreasHouseRegis( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Risk Areas House Regis started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/riskMobileNumber")
	@ResponseBody
	public String reloadRiskMobileNumber( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Risk Mobile Number started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/riskAreasPrestAddr")
	@ResponseBody
	public String reloadRiskAreasPrestAddr( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Risk Areas Prest Addr started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/rejectedDeclinePostpone")
	@ResponseBody
	public String reloadRejectedDeclinePostpone( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Rejected Decline Postpone started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/premiumPlus")
	@ResponseBody
	public String reloadPremiumPlus( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Premium Plus started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/planReferPremiumPlus")
	@ResponseBody
	public String reloadPlanReferPremiumPlus( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Plan Refer Premium Plus started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/ageForSpecialPlan")
	@ResponseBody
	public String reloadAgeForSpecialPlan( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Age For Special Plan started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/ageSpecialPlanAtleast")
	@ResponseBody
	public String reloadAgeSpecialPlanAtleast( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Age Special Plan Atleast started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/bmiBaby")
	@ResponseBody
	public String reloadBmiBaby( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Bmi Baby started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/bmiChildAndAdult")
	@ResponseBody
	public String reloadBmiChildAndAdult( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Bmi Child AndAdult started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/bmiDocument")
	@ResponseBody
	public String reloadBmiDocument( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Bmi Document started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/cofOccupationMain")
	@ResponseBody
	public String reloadCofOccupationMain( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Cof Occupation Main started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/cofOccupationRider")
	@ResponseBody
	public String reloadCofOccupationRider( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Cof Occupation Rider started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/occupationAccident")
	@ResponseBody
	public String reloadOccupationAccident( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Occupation Accident started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/occupationPa")
	@ResponseBody
	public String reloadOccupationPa( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Occupation PA started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifyAgeForRider")
	@ResponseBody
	public String reloadVerifyAgeForRider( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Age For Rider started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifyNationaRider")
	@ResponseBody
	public String reloadVerifyNationaRider( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Nationa Rider started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifyNationUsa")
	@ResponseBody
	public String reloadVerifyNationUsa( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Nation USA started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifyNationWithPlan")
	@ResponseBody
	public String reloadVerifyNationWithPlan( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Nation With Plan started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifyNationality")
	@ResponseBody
	public String reloadVerifyNationality( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Nationality started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/verifySexByPlancode")
	@ResponseBody
	public String reloadVerifySexByPlancode( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule11 Verify Sex By Plancode started ]]");
		String message = "";
		String status = "";
		try {
//			HashMap< String , String> rule12DecisionTableClaimCodeHistory = new HashMap< String , String>(); 
//			rule12DecisionTableClaimCodeHistory = rule12ClaimCodeHistoryDao.getClaimCodeHistory();
//			applicationCache.setClaimCodeHistoryMap(rule12DecisionTableClaimCodeHistory); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
