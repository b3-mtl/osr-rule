package com.mtl.appcache.reload;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule13Dt13AmloPilotDao;
import com.mtl.appcache.dao.Rule13Dt13NonIncomeOccupationDao;
import com.mtl.appcache.dao.Rule13Dt13RiskOccupationDao;
import com.mtl.rule.model.DT13AmloPilot;
import com.mtl.rule.model.DT13NonIncomeOccupation;
import com.mtl.rule.model.DT13RiskOccupation;

@Controller
@RequestMapping("rule13ReloadCache")
@CrossOrigin(origins = "*")
public class Rule13ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule13ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule13Dt13NonIncomeOccupationDao  rule13Dt13NonIncomeOccupationDao;
	
	@Autowired
	private Rule13Dt13RiskOccupationDao rule13Dt13RiskOccupationDao;
	
	@Autowired
	private Rule13Dt13AmloPilotDao  rule13Dt13AmloPilotDao;
	
	@GetMapping("/nonIncomeOccupation")
	@ResponseBody
	public String rule13NonIncomeOccupation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule13 Non Income Occupation started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT13NonIncomeOccupation> rule13NonIncomeOccupation = new HashMap< String , DT13NonIncomeOccupation>(); 
			rule13NonIncomeOccupation = rule13Dt13NonIncomeOccupationDao.getAllList();
			applicationCache.setRule13NonIncomeOccupation(rule13NonIncomeOccupation); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/riskOccupation")
	@ResponseBody
	public String reloadRiskOccupation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule13 Risk Occupation started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT13RiskOccupation> rule13RiskOccupation = new HashMap< String , DT13RiskOccupation>(); 
			rule13RiskOccupation = rule13Dt13RiskOccupationDao.getAllList();
			applicationCache.setRule13RiskOccupation(rule13RiskOccupation); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/amloPilot")
	@ResponseBody
	public String reloadAmloPilot( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule13 Amlo Pilot started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT13AmloPilot> rule13AmloPilot = new HashMap< String , DT13AmloPilot>(); 
			rule13AmloPilot = rule13Dt13AmloPilotDao.getAllList();
			applicationCache.setRule13AmloPilot(rule13AmloPilot); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
}
