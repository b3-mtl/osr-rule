package com.mtl.appcache.reload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule17BeneficiaryRelationDao;
import com.mtl.appcache.dao.Rule17MIBAMLODao;
import com.mtl.rule.model.DT17BeneficiaryRelation;

@Controller
@RequestMapping("rule17ReloadCache")
@CrossOrigin(origins = "*")
public class Rule17ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule17ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule17BeneficiaryRelationDao  rule17BeneficiaryRelationDao;
	
	@Autowired
	private Rule17MIBAMLODao rule17MIBAMLODao;
	
	@GetMapping("/beneficiaryRelation")
	@ResponseBody
	public String reloadBeneficiaryRelation( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule17 Beneficiary Relation started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT17BeneficiaryRelation> rule17DecisionTableBeneficiaryRelation = new HashMap< String , DT17BeneficiaryRelation>(); 
			rule17DecisionTableBeneficiaryRelation = rule17BeneficiaryRelationDao.getBeneficiaryRelation();
			applicationCache.setBeneficiaryRelationMap(rule17DecisionTableBeneficiaryRelation); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	@GetMapping("/mibAmlo")
	@ResponseBody
	public String reloadMibAmlo( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule17 Mib Amlo started ]]");
		String message = "";
		String status = "";
		try {
			List<String> dt17MIBAMLOList = new ArrayList<String>();
			dt17MIBAMLOList = rule17MIBAMLODao.getAll();
			applicationCache.setDt17MIBAMLOList(dt17MIBAMLOList);
			
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
}
