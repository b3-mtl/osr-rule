package com.mtl.appcache.reload;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule04AppMdcDao;
import com.mtl.appcache.dao.Rule04ExceptPlanCodeDao;
import com.mtl.appcache.dao.Rule04PremiumPaymentSpecialDao;
import com.mtl.rule.model.DT04AppMdc;
import com.mtl.rule.model.DT04PremiumPaymentSpecial;

@Controller
@RequestMapping("rule04ReloadCache")
@CrossOrigin(origins = "*")
public class Rule04ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule04ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule04ExceptPlanCodeDao rule04Dt04ExceptPlanCodeDao;
	 
	@Autowired
	private Rule04PremiumPaymentSpecialDao  rule04Dt04PremiumPaymentSpecialDao;
	
	@Autowired
	private Rule04AppMdcDao  rule04Dt04AppMdcDao;
	
	@GetMapping("/exceptPlanCode")
	@ResponseBody
	public String reloadExceptPlanCode( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule04 Except Plan Code started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , String> rule4DecisionTableExceptPlanCode = new HashMap< String , String>(); 
			rule4DecisionTableExceptPlanCode = rule04Dt04ExceptPlanCodeDao.findAll();
			applicationCache.setRule04_dt04ExceptPlanCode(rule4DecisionTableExceptPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/premiumPaymentSpecial")
	@ResponseBody
	public String reloadUWRQ( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule04 Premium Payment Special started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT04PremiumPaymentSpecial > rule4DecisionTablePremiumPaymentSpecial = new HashMap< String , DT04PremiumPaymentSpecial >(); 
			rule4DecisionTablePremiumPaymentSpecial = rule04Dt04PremiumPaymentSpecialDao.getAllPremiumPaymentSpecial();
			applicationCache.setRule04_dt04PremiumPaymentSpecial(rule4DecisionTablePremiumPaymentSpecial); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		
		return status;
	}
	
	@GetMapping("/appMdc")
	@ResponseBody
	public String reloadAppMdc( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule04 App Mdc started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT04AppMdc > rule4DecisionTableAppMdc = new HashMap< String , DT04AppMdc >(); 
			rule4DecisionTableAppMdc = rule04Dt04AppMdcDao.getAllAppMdc();
			applicationCache.setRule04_dt04AppMdc(rule4DecisionTableAppMdc); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
}
