package com.mtl.appcache.reload;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule14DocumentClaimDao;
import com.mtl.appcache.dao.Rule14ExceptionDao;
import com.mtl.appcache.dao.Rule14HnwDocumentDao;
import com.mtl.appcache.dao.Rule14PADocumentDao;
import com.mtl.rule.model.DT14DocumentClaim;
import com.mtl.rule.model.DT14Exception;
import com.mtl.rule.model.DT14HnwDocument;
import com.mtl.rule.model.DT14PADocument;

@Controller
@RequestMapping("rule14ReloadCache")
@CrossOrigin(origins = "*")
public class Rule14ReloadController {
	
	private static final Logger logger = LoggerFactory.getLogger(Rule06ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private Rule14ExceptionDao rule14ExceptionDao;
	
	@Autowired
	private Rule14HnwDocumentDao rule14HnwDocumentDao;
	
	@Autowired
	private Rule14PADocumentDao rule14PADocumentDao;
	
	@Autowired
	private Rule14DocumentClaimDao rule14DocumentClaimDao;
	
	@GetMapping("/exception")
	@ResponseBody
	public String reloadException( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule14 Exception started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , DT14Exception> rule14DecisionTableException = new HashMap< String , DT14Exception>(); 
			rule14DecisionTableException = rule14ExceptionDao.getCollector();
			applicationCache.setRule14Exception(rule14DecisionTableException); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/exceptionPlanType")
	@ResponseBody
	public String reloadExceptionPlanType( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule14 Exception Plan Type started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<DT14PADocument>> rule14DecisionTableExceptionPlanType = new HashMap< String , List<DT14PADocument>>(); 
			rule14DecisionTableExceptionPlanType = rule14PADocumentDao.getCollector();
			applicationCache.setPADocument(rule14DecisionTableExceptionPlanType); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/exceptionPlanCode")
	@ResponseBody
	public String reloadExceptionPlanCode( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule14 Exception Plan Code started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<DT14HnwDocument>> rule14DecisionTableExceptionPlanCode = new HashMap< String , List<DT14HnwDocument>>(); 
			rule14DecisionTableExceptionPlanCode = rule14HnwDocumentDao.getCollector();
			applicationCache.setHnwDocument(rule14DecisionTableExceptionPlanCode); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
	
	@GetMapping("/groupChannel")
	@ResponseBody
	public String reloadGroupChannel( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule14 Group Channel started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<DT14DocumentClaim>> rule14DecisionTableGroupChannel = new HashMap< String , List<DT14DocumentClaim>>(); 
			rule14DecisionTableGroupChannel = rule14DocumentClaimDao.getCollector();
			applicationCache.setRule14ExceptionClaim(rule14DecisionTableGroupChannel); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;
	}
}
