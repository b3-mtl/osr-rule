package com.mtl.appcache.reload;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtl.appcache.ApplicationCache;
import com.mtl.appcache.dao.Rule05Dt05SpecialPaymentDao;
import com.mtl.appcache.dao.common.MsPremiumPaymentDao;
import com.mtl.rule.model.MsPremiumPayment;

@Controller
@RequestMapping("rule05ReloadCache")
@CrossOrigin(origins = "*")
public class Rule05ReloadController {

	private static final Logger logger = LoggerFactory.getLogger(Rule05ReloadController.class);

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private MsPremiumPaymentDao msPremiumPaymentDao;
	
	@Autowired
	private Rule05Dt05SpecialPaymentDao rule5SpecialPaymentDao;
	
	@GetMapping("/premiumPayment")
	@ResponseBody
	public String reloadPremiumPayment( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule05 Premium Payment started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , List<String>> rule6DecisionTablePremiumPayment = new HashMap< String , List<String>>(); 
			rule6DecisionTablePremiumPayment = msPremiumPaymentDao.findAll();
			applicationCache.setRule05_premiumPayment(rule6DecisionTablePremiumPayment); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
	@GetMapping("/specialPayment")
	@ResponseBody
	public String reloadSpecialPayment( HttpServletRequest httpRequest) {
		logger.info("[[ ReloadCache Rule05 Special Payment started ]]");
		String message = "";
		String status = "";
		try {
			HashMap< String , Set<String>> rule6DecisionTableSpecialPayment = new HashMap< String , Set<String>>(); 
			rule6DecisionTableSpecialPayment = rule5SpecialPaymentDao.getSpecialPaymentChannel();
			applicationCache.setRule05_specialPaymentChannelMap(rule6DecisionTableSpecialPayment); 
			message = "[[ ReloadCache success ]]";
			status = "SUCCESS";
			logger.info(message);
			
		} catch (Exception e) {
			message = "[[ ReloadCache fail ]]";
			status = "FAILED";
			logger.error(message);
			logger.error(e.getMessage(), e);
		}
		
		return status;

	}
	
}
