package com.mtl.api.rest;



import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mtl.api.config.log.LogConfig;
import com.mtl.api.core.utils.Timmer;
import com.mtl.api.utils.ServiceConstants;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.master.CollectorFactory;
import com.mtl.collector.master.RuleEngineFactory;
import com.mtl.collector.service.CollectorService;
import com.mtl.collector.ws.service.GetAgentInfoService;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.ErrorMessage;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.ResponseHeader;
import com.mtl.model.underwriting.RulesResponse;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.model.underwriting.UnderwriteResponse;
import com.mtl.rule.service.impl.RuleEngineService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.logging.ServiceLoggingReq;
import com.mtl.rule.util.logging.ServiceLoggingService;
import com.mtl.rule.util.validate.InputValidation;
import com.mtl.rule.util.validate.Validation;

@RestController
@RequestMapping("${path.prefix}/underwriting")
@CrossOrigin(origins = "http://external-system.com")
public class RuleEngineRestApi {

	private static final Logger log = LogManager.getLogger(RuleEngineRestApi.class);
	private static final Logger auditLog = LogManager.getContext(false).getLogger(LogConfig.DATABASE_LOGGER);
	
	@Value("${config.service-loging}")
	private String loggingStatus;
	
	@Autowired
	private RuleEngineService ruleEngineService;
 
	@Autowired
	private ServiceLoggingService serviceLoggingService;
	
	@Autowired
	private RuleEngineFactory ruleEngineFactory;
	
	@Autowired
	private CollectorFactory collectorFactory;
	
	@Autowired
	private CollectorService collectorService;
 
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired 
	private GetAgentInfoService getAgentInfoService;
 
	@PostMapping( value = "/execute", 
			consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE )
	public @ResponseBody ResponseEntity<UnderwriteResponse> execute(@RequestBody UnderwriteRequest request)  {
		
		log.info("=========== Start [Trasation ID :"+request.getRequestHeader().getTransactionId()+"] ===========");
		
		UnderwriteResponse response = new UnderwriteResponse();
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		 
		ResponseHeader responseHeader = new ResponseHeader();
		Map<String, String> ruleSet =null;
		Set<String> collectorSet =null;
		Timmer timeUsage = new Timmer();
		String state = "";
		String collectorUsageTime ="";
		String serviceUsageTime ="";
		String totalTimeUsageTime ="";
		String allRuleLog ="";
		String requestJson = "";
		
		Validation headerValidation  = null;
		Validation inputValidation  = null;
		try {
			headerValidation  = InputValidation.validateHeader(request,applicationCache,getAgentInfoService);
		    if(request!=null&&request.getRequestHeader()!=null) {
		    	responseHeader.setTransactionId(request.getRequestHeader().getTransactionId());
		    	requestJson = mapper.writeValueAsString(request);
		    }
			
			
		log.info("Access: " + RuleEngineRestApi.class + ", Method: " + "post()");
		if(headerValidation.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
			responseHeader.setStatus(headerValidation.getStatus());
			responseHeader.setErrorMessage(headerValidation.getErrorMessage());
			
		}else {			
			
			state = " Request From  [RegisteredSystem: " + request.getRequestHeader().getRegisteredSystem() +"] [Section : " +request.getRequestHeader().getSection() + "]"  ;	
			
			// Step 1. Get Rule Set
			 ruleSet = ruleEngineFactory.getRuleSet( request.getRequestHeader().getRegisteredSystem(), request.getRequestHeader().getSection());
			 
			 
			 if(ruleSet!=null) {
				 collectorSet = collectorFactory.get(ruleSet.keySet());
				  inputValidation  = InputValidation.inputValidation(request,ruleSet);
					if(inputValidation.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
						responseHeader.setStatus(inputValidation.getStatus());
						responseHeader.setErrorMessage(inputValidation.getErrorMessage());
					}else {		
			 			String ruleSetStr = "";
						for (String key : ruleSet.keySet()) {			 
							ruleSetStr = ruleSetStr+ruleSet.get(key)+"  ";
						}			
						state = "Step 1 -> [Rules Set: " + ruleSetStr +"]";
						state = "Step 2 -> [Collector Set: " + collectorSet +"]";
						
						// Step 3. Call Collector WS
						timeUsage= new Timmer();
						Timmer step3 = new Timmer();
						CollectorModel collector = collectorService.getDataCollector(request, collectorSet,ruleSet);
						state = "Step 3 -> [Call Collector WS take time: " + step3.timeDiff() + " ms.]";
						collectorUsageTime =""+step3.timeDiff();
						
						// Step 4. Execute Underwrite
						Timmer step4 = new Timmer();
						RulesResponse rulesResponse  = ruleEngineService.underwriting( request,collector,ruleSet);
						
						state = "Step 4 -> [Execute Underwrite take time: " + step4.timeDiff() + " ms.]";
						serviceUsageTime =""+ step4.timeDiff();
						
						totalTimeUsageTime = ""+ timeUsage.timeDiff();
							
						responseHeader.setStatus(ServiceConstants.SUCCESS);	 
						responseHeader.setTransactionId(request.getRequestHeader().getTransactionId());
						com.mtl.model.underwriting.ResponseBody responseBody = new com.mtl.model.underwriting.ResponseBody();
						responseBody.setDocumentList(rulesResponse.getDocumentList());
						responseBody.setMessageList(rulesResponse.getMessageList()); 
			 
						if(rulesResponse.getDocumentList()!=null&&rulesResponse.getDocumentList().size()>0) {
							responseBody.setDocumentCount(rulesResponse.getDocumentList().size());
							 
						}
						if(rulesResponse.getMessageList()!=null&&rulesResponse.getMessageList().size()>0) {
							responseBody.setMessageCount(rulesResponse.getMessageList().size());
							 
						}
 
						allRuleLog = rulesResponse.getAllRuleExecuteLog();
						response.setResponseBody(responseBody);		
						 
					} 
			 }else {
				 	timeUsage= new Timmer();
					ErrorMessageDesc errorMesageDesc = new ErrorMessageDesc();
					List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
					errorMesageDesc.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					errorMesageDesc.setErrorDesc("Not Found Registered System or Section"); 
					errorMessageList.add(errorMesageDesc);
			 
					ErrorMessage errorMesage = new ErrorMessage();
					errorMesage.setErrorList(errorMessageList);
					responseHeader.setStatus(ServiceConstants.VALIDATION_ERROR);
					responseHeader.setErrorMessage(errorMesage);
				 
			 }

		}
		
		responseHeader.setTimestamp( LocalDateTime.now());
		responseHeader.setTimeUsage(timeUsage.timeDiff()+"ms.");
		response.setResponseHeader(responseHeader);
		
		}catch (Exception ex){
			// Technical Error Set Response 
			
			responseHeader.setStatus(ServiceConstants.FAIL);			
			responseHeader.setTimestamp( LocalDateTime.now());
			totalTimeUsageTime =""+timeUsage.timeDiff();
			response.setResponseHeader(responseHeader);
			
			ErrorMessageDesc errorDesc = new ErrorMessageDesc();
			errorDesc.setErrorCode("1");
			errorDesc.setErrorDesc("TECHNICAL ERROR");
			List<ErrorMessageDesc> messageDescList = new ArrayList<ErrorMessageDesc>();
			messageDescList.add(errorDesc);
			ErrorMessage errMsg = new ErrorMessage();
			errMsg.setErrorList(messageDescList);
			responseHeader.setErrorMessage(errMsg);
			ex.printStackTrace();
			log.error(ex);
		}finally {
			// Service Logging 
			
			try {
				ServiceLoggingReq serviceLoggingReq = new ServiceLoggingReq();
				
				
				if(ruleSet!=null) {
					ArrayList<String> listofValues = ruleSet.values().stream().collect(Collectors.toCollection(ArrayList::new)); 
					String listString = ""; 
					for (String s : listofValues) {
					    listString += s + "\t";
					} 
					serviceLoggingReq.setRuleList(listString);  
				}

				if(collectorSet!=null) {
					String collectorString = ""; 
					  for (Iterator<String> it = collectorSet.iterator(); it.hasNext(); ) {
					        String f = it.next();
					        collectorString += f + "\t"; 
					    }	
					  serviceLoggingReq.setCollectorList(collectorString);
				} 
				
				if(headerValidation.getStatus().equals(ServiceConstants.VALIDATION_ERROR)||(inputValidation!=null&&inputValidation.getStatus().equals(ServiceConstants.VALIDATION_ERROR))) {
					serviceLoggingReq.setStatus(ServiceConstants.VALIDATION_ERROR);	 
			        serviceLoggingReq.setCollectorUsageTime("0ms.");
			        serviceLoggingReq.setServiceUsageTime("0ms."); 
			        totalTimeUsageTime =""+timeUsage.timeDiff();
				} else {
			        serviceLoggingReq.setCollectorUsageTime(collectorUsageTime+"ms.");
			        serviceLoggingReq.setServiceUsageTime(serviceUsageTime+"ms."); 
				}
					
					String responseJson = mapper.writeValueAsString(response);   
					
				    if(request!=null&&request.getRequestHeader()!=null) {
						serviceLoggingReq.setChannelCode(request.getRequestHeader().getRegisteredSystem());
						serviceLoggingReq.setTransactionId(request.getRequestHeader().getTransactionId());
						serviceLoggingReq.setSection(request.getRequestHeader().getSection()); 
				    } 
				    serviceLoggingReq.setStatus(response.getResponseHeader().getStatus());
					serviceLoggingReq.setTimeUsage(totalTimeUsageTime+"ms.");
					serviceLoggingReq.setRequestJson(requestJson);
					serviceLoggingReq.setResponseJson(responseJson); 
					
					if(RuleConstants.YES.equals(loggingStatus)) {
						log.info(state);
						serviceLoggingReq.setAllRuleLog(allRuleLog);
						serviceLoggingService.saveServiceLogging(serviceLoggingReq);
					}



			}catch(Exception ex) {
				ex.printStackTrace();
				log.error(ex);
			}

			log.info("=========== End [Trasation ID :"+request.getRequestHeader().getTransactionId()+"] ===========");
		}
		return ResponseEntity.ok(response);
	}

}
