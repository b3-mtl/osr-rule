package com.mtl.api.rest;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mtl.api.core.service.interfaces.ArchiveService;
import com.mtl.model.document.Document;
import com.mtl.model.document.DocumentMetadata;


/**
 * REST web service for archive service {@link IArchiveService}.
 * 
 * /archive/upload?file={file}&person={person}&date={date}  Add a document  POST
 *   file: A file posted in a multipart request
 *   person: The name of the uploading person
 *   date: The date of the document
 *   
 * /archive/documents?person={person}&date={date}           Find documents  GET
 *   person: The name of the uploading person
 *   date: The date of the document
 *   
 * /archive/document/{id}                                   Get a document  GET
 *   id: The UUID of a document
 * 
 * All service calls are delegated to instances of {@link IArchiveService}.
 * 
 * @author buncha.p
 */

@RestController("/${path.prefix}/archive")
public class ArchiveRestApi  {

    private static final Logger log = LogManager.getLogger(ArchiveRestApi.class);
    
    @Autowired
    private ArchiveService archiveService;

    /**
     * Adds a document to the archive.
     * 
     * Url: /archive/upload?file={file}&person={person}&date={date} [POST]
     * 
     * @param file A file posted in a multipart request
     * @param person The name of the uploading person
     * @param date The date of the document
     * @return The meta data of the added document
     */
    @PostMapping("/upload")
    public @ResponseBody DocumentMetadata handleFileUpload(
            @RequestParam(value="file", required=true) MultipartFile file ,
            @RequestParam(value="person", required=false) String person,
            @RequestParam(value="date", required=false) @DateTimeFormat(pattern="dd/MM/yyyy") Date date) {
        try {
        	person = "person";
        	date = new Date();
        	 Document document = new Document(file.getBytes(), file.getOriginalFilename(), date, person );
            archiveService.save(document);
            return document.getMetadata();
        } catch (RuntimeException e) {
            log.error("Error while uploading.", e);
            throw e;
        } catch (Exception e) {
            log.error("Error while uploading.", e);
            throw new RuntimeException(e);
        }      
    }
    
    @GetMapping("/test-upload")
    public @ResponseBody DocumentMetadata testUpload() {
        try {
        	Date date = new Date();
        	 Document document = new Document(null, "xxx", date, "buncha.p" );
            archiveService.save(document);
            return document.getMetadata();
        } catch (RuntimeException e) {
            log.error("Error while uploading.", e);
            throw e;
        } catch (Exception e) {
            log.error("Error while uploading.", e);
            throw new RuntimeException(e);
        }      
    }
    
    
    /**
     * Finds document in the archive. Returns a list of document meta data 
     * which does not include the file data. Use getDocument to get the file.
     * Returns an empty list if no document was found.
     * 
     * Url: /archive/documents?person={person}&date={date} [GET]
     * 
     * @param person The name of the uploading person
     * @param date The date of the document
     * @return A list of document meta data
     */
    @GetMapping("/documents")
    public HttpEntity<List<DocumentMetadata>> findDocument(
            @RequestParam(value="person", required=false) String person,
            @RequestParam(value="date", required=false) @DateTimeFormat(pattern="d/MM/yyyy") Date date) {
        HttpHeaders httpHeaders = new HttpHeaders();
        return new ResponseEntity<List<DocumentMetadata>>(archiveService.findDocuments(person,date), httpHeaders,HttpStatus.OK);
    }
    
    /**
     * Returns the document file from the archive with the given UUID.
     * 
     * Url: /archive/document/{id} [GET]
     * 
     * @param id The UUID of a document
     * @return The document file
     * @throws ApplicationException 
     */
    @GetMapping("/document/{id}")
    public HttpEntity<byte[]> getDocument(@PathVariable String id) throws ApplicationException {         
        // send it back to the client
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(archiveService.getDocumentFile(id) , httpHeaders, HttpStatus.OK);
    }
    
    @GetMapping("/documentResizing/{id}")
    public HttpEntity<byte[]> getDocumentResizing(@PathVariable String id) throws ApplicationException {         
        // send it back to the client
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(archiveService.getDocumentFileResizing(id) , httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/sendDate/{date}")
    public ResponseEntity<String>  sendDate(@PathVariable(value="date") @DateTimeFormat(pattern="yyyy-MM-dd") Date date){       
        // send it back to the client
    	System.out.println( date );
        return new ResponseEntity<String>(HttpStatus.OK);
    }
    
    
 

}
