package com.mtl.api;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAsync
@ComponentScan("com.mtl")
@EntityScan(basePackages = { "com.mtl" })
@EnableJpaRepositories(basePackages = { "com.mtl" })
@EnableTransactionManagement
@SpringBootApplication
@EnableAutoConfiguration
@Configuration 
@PropertySources({ 
	@PropertySource(value = "classpath:application.properties", ignoreResourceNotFound = true),//for dev only
    @PropertySource(value = "file:${external.config.path}/application-rule.properties", ignoreResourceNotFound = true)// for deploy in sever
	
})


public class ApiWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiWsApplication.class, args);
	}
	
	@PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Bangkok"));
//        new ApplicationCache();
    }
}
