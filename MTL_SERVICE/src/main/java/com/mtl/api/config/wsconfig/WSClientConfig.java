package com.mtl.api.config.wsconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

@Configuration
public class WSClientConfig {

  @Value("${ws.getmib.endpoint}")
  private String defaultUri;

  @Bean
  Jaxb2Marshaller jaxb2Marshaller() {
    Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
  
//    jaxb2Marshaller.setContextPaths(
//            "com.mtl.collector.ws.GetClaimCode",
//            "com.mtl.collector.ws.GetDataColPolicyCoverage",
//            "com.mtl.collector.ws.GetMIB",          
//            "com.mtl.collector.ws.SearchClientProfilebyIDCardNo" );
    
    jaxb2Marshaller.setPackagesToScan(
            "com.mtl.collector.ws.codegen.GetDataColPolicyCoverage" );
    return jaxb2Marshaller;
  }
  
  
 

  @Bean
  public WebServiceTemplate webServiceTemplate() {
    WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
    webServiceTemplate.setMarshaller(jaxb2Marshaller());
    webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
    webServiceTemplate.setDefaultUri(defaultUri);

    // register the LogHttpHeaderClientInterceptor
    ClientInterceptor[] interceptors =
        new ClientInterceptor[] {new LogHttpHeaderClientInterceptor()};
    webServiceTemplate.setInterceptors(interceptors);

    return webServiceTemplate;
  }
}