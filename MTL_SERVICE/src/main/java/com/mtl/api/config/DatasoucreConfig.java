package com.mtl.api.config;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.mtl.api.utils.CommonJdbcTemplate;

@Configuration
@EnableTransactionManagement
public class DatasoucreConfig {
	
	@Value("${spring.datasource.jndi}")
	private String jndiMain;
	
	@Value("${loft.jndi}")
	private String jndiLoft;
	
	@Bean(name = "commonJdbcTemplate")
	public CommonJdbcTemplate commonJdbcTemplate(@Qualifier("jdbcTemplate") JdbcTemplate jdbcTemplate) {
		return new CommonJdbcTemplate(jdbcTemplate);
	}
	
	@Bean(name = "commonLoftJdbcTemplate")
	public CommonJdbcTemplate commonLoftJdbcTemplate(@Qualifier("loftJDBCTemplate") JdbcTemplate loftJDBCTemplate) {
		return new CommonJdbcTemplate(loftJDBCTemplate);
	}
	
	@Primary
	@Bean
    public JdbcTemplate jdbcTemplate() throws Exception { 
        return new JdbcTemplate(dataSource());
    }
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix="spring.datasource")
    public DataSource dataSource() throws IllegalArgumentException, NamingException {
		
		if (StringUtils.isNotBlank(jndiMain)) {
			JndiObjectFactoryBean jndiObj = new JndiObjectFactoryBean();
			jndiObj.setJndiName(jndiMain);
			jndiObj.setProxyInterface(DataSource.class);
			jndiObj.setLookupOnStartup(false);
			jndiObj.afterPropertiesSet();
			return (DataSource) jndiObj.getObject();
		}else {
			return DataSourceBuilder.create().build();
		}
    }
	
    @Bean
    @ConfigurationProperties(prefix="loft")
    public DataSource loftDataSource() throws IllegalArgumentException, NamingException {
    	
    	if (StringUtils.isNotBlank(jndiLoft)) {
			JndiObjectFactoryBean jndiObj = new JndiObjectFactoryBean();
			jndiObj.setJndiName(jndiLoft);
			jndiObj.setProxyInterface(DataSource.class);
			jndiObj.setLookupOnStartup(false);
			jndiObj.afterPropertiesSet();
			return (DataSource) jndiObj.getObject();
		}else {
			return DataSourceBuilder.create().build();
		}
    }

    @Bean(name = "loftJDBCTemplate")
    public JdbcTemplate loftJDBCTemplate() throws Exception { 
        return new JdbcTemplate(loftDataSource());
    }
    
    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("dataSource") DataSource dataSource) {
        return builder.dataSource(dataSource)
                .packages("com.mtl")
                .build();
    }

    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
