package com.mtl.api.security;

import java.util.stream.Collectors;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.mtl.core.repository.interfaces.ClientDetailRepository;

//@Service
//public class ClientDetailsService implements UserDetailsService {

//	@Autowired
//	private ClientDetailRepository clientDetailRepository;

//	@Override
//	public ClientDetail loadUserByUsername(String username) throws UsernameNotFoundException {
//		return clientDetailRepository.findOptionalByClientId(username).map(item -> new ClientDetail(item.getClientId(),
//				item.getClientSecret(),
//				item.getTrRoleMappingList().stream().map(m -> new SimpleGrantedAuthority(m.getRoleId().getRoleId()))
//						.collect(Collectors.toSet()),
//				item.getClientEnabled(), item.getPasswordExpiredDate(), item.getPasswordExpiredDate()))
//				.orElseThrow(() -> new UsernameNotFoundException("Client id: '" + username + "' not found."));
//	}
//
//	public ClientDetail loadByClientIdAndClientSecret(String clientId, String clientSecret)
//			throws UsernameNotFoundException {
//		return clientDetailRepository.findOptionalByClientIdAndClientSecret(clientId, clientSecret)
//				.map(item -> new ClientDetail(item.getClientId(), item.getClientSecret(),
//						item.getTrRoleMappingList().stream()
//								.map(m -> new SimpleGrantedAuthority(m.getRoleId().getRoleId()))
//								.collect(Collectors.toSet()),
//						item.getClientEnabled(), item.getClientExpiredDate(), item.getPasswordExpiredDate()))
//				.orElseThrow(() -> new UsernameNotFoundException("Client id: '" + clientId + "' not found."));
//	}

//}
