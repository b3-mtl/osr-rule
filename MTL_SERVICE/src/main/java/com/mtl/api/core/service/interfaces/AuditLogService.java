package com.mtl.api.core.service.interfaces;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;

import com.mtl.model.underwriting.AuditLogModel;

public interface AuditLogService {
	
	public static enum LogType {
		
		REQUEST("Request"), 
		RESPONSE("Response");

		private String name;

		LogType(String name) {
			this.name = name;
		}
		public String getName() {
			return this.name;
		}
	}

	/**
	 * 
	 * @param model
	 * @param logType
	 * @return
	 * @throws InterruptedException
	 */
	@Async
	CompletableFuture<?> writeLog(AuditLogModel model, LogType logType);
}
