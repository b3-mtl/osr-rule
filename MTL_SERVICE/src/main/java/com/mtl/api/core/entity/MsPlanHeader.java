/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_PLAN_HEADER", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsPlanHeader.findAll", query = "SELECT m FROM MsPlanHeader m")
    , @NamedQuery(name = "MsPlanHeader.findByPlanCode", query = "SELECT m FROM MsPlanHeader m WHERE m.planCode = :planCode")
    , @NamedQuery(name = "MsPlanHeader.findByAgentQualification", query = "SELECT m FROM MsPlanHeader m WHERE m.agentQualification = :agentQualification")
    , @NamedQuery(name = "MsPlanHeader.findByAnntyCntlKey", query = "SELECT m FROM MsPlanHeader m WHERE m.anntyCntlKey = :anntyCntlKey")
    , @NamedQuery(name = "MsPlanHeader.findByAnntyStartInd", query = "SELECT m FROM MsPlanHeader m WHERE m.anntyStartInd = :anntyStartInd")
    , @NamedQuery(name = "MsPlanHeader.findByBusinessPlanGroup", query = "SELECT m FROM MsPlanHeader m WHERE m.businessPlanGroup = :businessPlanGroup")
    , @NamedQuery(name = "MsPlanHeader.findByChannel", query = "SELECT m FROM MsPlanHeader m WHERE m.channel = :channel")
    , @NamedQuery(name = "MsPlanHeader.findByClassOfBusiness", query = "SELECT m FROM MsPlanHeader m WHERE m.classOfBusiness = :classOfBusiness")
    , @NamedQuery(name = "MsPlanHeader.findByDivOption", query = "SELECT m FROM MsPlanHeader m WHERE m.divOption = :divOption")
    , @NamedQuery(name = "MsPlanHeader.findByDivType", query = "SELECT m FROM MsPlanHeader m WHERE m.divType = :divType")
    , @NamedQuery(name = "MsPlanHeader.findByEffectiveDate", query = "SELECT m FROM MsPlanHeader m WHERE m.effectiveDate = :effectiveDate")
    , @NamedQuery(name = "MsPlanHeader.findByEtiValue", query = "SELECT m FROM MsPlanHeader m WHERE m.etiValue = :etiValue")
    , @NamedQuery(name = "MsPlanHeader.findByExpiryDate", query = "SELECT m FROM MsPlanHeader m WHERE m.expiryDate = :expiryDate")
    , @NamedQuery(name = "MsPlanHeader.findByFlagMicrr", query = "SELECT m FROM MsPlanHeader m WHERE m.flagMicrr = :flagMicrr")
    , @NamedQuery(name = "MsPlanHeader.findByHb2tVal", query = "SELECT m FROM MsPlanHeader m WHERE m.hb2tVal = :hb2tVal")
    , @NamedQuery(name = "MsPlanHeader.findByKLeasingNml", query = "SELECT m FROM MsPlanHeader m WHERE m.kLeasingNml = :kLeasingNml")
    , @NamedQuery(name = "MsPlanHeader.findByKkAffinityNml", query = "SELECT m FROM MsPlanHeader m WHERE m.kkAffinityNml = :kkAffinityNml")
    , @NamedQuery(name = "MsPlanHeader.findByLowerAge", query = "SELECT m FROM MsPlanHeader m WHERE m.lowerAge = :lowerAge")
    , @NamedQuery(name = "MsPlanHeader.findByMaturityIndicator", query = "SELECT m FROM MsPlanHeader m WHERE m.maturityIndicator = :maturityIndicator")
    , @NamedQuery(name = "MsPlanHeader.findByMaturityYear", query = "SELECT m FROM MsPlanHeader m WHERE m.maturityYear = :maturityYear")
    , @NamedQuery(name = "MsPlanHeader.findByMaximumAmount", query = "SELECT m FROM MsPlanHeader m WHERE m.maximumAmount = :maximumAmount")
    , @NamedQuery(name = "MsPlanHeader.findByMinimumAmount", query = "SELECT m FROM MsPlanHeader m WHERE m.minimumAmount = :minimumAmount")
    , @NamedQuery(name = "MsPlanHeader.findByMx40value", query = "SELECT m FROM MsPlanHeader m WHERE m.mx40value = :mx40value")
    , @NamedQuery(name = "MsPlanHeader.findByNcType", query = "SELECT m FROM MsPlanHeader m WHERE m.ncType = :ncType")
    , @NamedQuery(name = "MsPlanHeader.findByNonMedQualification", query = "SELECT m FROM MsPlanHeader m WHERE m.nonMedQualification = :nonMedQualification")
    , @NamedQuery(name = "MsPlanHeader.findByPaType", query = "SELECT m FROM MsPlanHeader m WHERE m.paType = :paType")
    , @NamedQuery(name = "MsPlanHeader.findByPayment", query = "SELECT m FROM MsPlanHeader m WHERE m.payment = :payment")
    , @NamedQuery(name = "MsPlanHeader.findByPhEtFaceType", query = "SELECT m FROM MsPlanHeader m WHERE m.phEtFaceType = :phEtFaceType")
    , @NamedQuery(name = "MsPlanHeader.findByPlanGrouptype", query = "SELECT m FROM MsPlanHeader m WHERE m.planGrouptype = :planGrouptype")
    , @NamedQuery(name = "MsPlanHeader.findByPlanSystem", query = "SELECT m FROM MsPlanHeader m WHERE m.planSystem = :planSystem")
    , @NamedQuery(name = "MsPlanHeader.findByPlantype", query = "SELECT m FROM MsPlanHeader m WHERE m.plantype = :plantype")
    , @NamedQuery(name = "MsPlanHeader.findByPrDate", query = "SELECT m FROM MsPlanHeader m WHERE m.prDate = :prDate")
    , @NamedQuery(name = "MsPlanHeader.findByPremiumChangeIndicator", query = "SELECT m FROM MsPlanHeader m WHERE m.premiumChangeIndicator = :premiumChangeIndicator")
    , @NamedQuery(name = "MsPlanHeader.findByPremiumChangeYear", query = "SELECT m FROM MsPlanHeader m WHERE m.premiumChangeYear = :premiumChangeYear")
    , @NamedQuery(name = "MsPlanHeader.findByPremiumIndicator", query = "SELECT m FROM MsPlanHeader m WHERE m.premiumIndicator = :premiumIndicator")
    , @NamedQuery(name = "MsPlanHeader.findByProductLicense", query = "SELECT m FROM MsPlanHeader m WHERE m.productLicense = :productLicense")
    , @NamedQuery(name = "MsPlanHeader.findByRiderAddInd", query = "SELECT m FROM MsPlanHeader m WHERE m.riderAddInd = :riderAddInd")
    , @NamedQuery(name = "MsPlanHeader.findByRpuValue", query = "SELECT m FROM MsPlanHeader m WHERE m.rpuValue = :rpuValue")
    , @NamedQuery(name = "MsPlanHeader.findBySex", query = "SELECT m FROM MsPlanHeader m WHERE m.sex = :sex")
    , @NamedQuery(name = "MsPlanHeader.findBySurrenderType", query = "SELECT m FROM MsPlanHeader m WHERE m.surrenderType = :surrenderType")
    , @NamedQuery(name = "MsPlanHeader.findByTakafulGroupType", query = "SELECT m FROM MsPlanHeader m WHERE m.takafulGroupType = :takafulGroupType")
    , @NamedQuery(name = "MsPlanHeader.findByTakafulType", query = "SELECT m FROM MsPlanHeader m WHERE m.takafulType = :takafulType")
    , @NamedQuery(name = "MsPlanHeader.findByTypeInsurance", query = "SELECT m FROM MsPlanHeader m WHERE m.typeInsurance = :typeInsurance")
    , @NamedQuery(name = "MsPlanHeader.findByUpperAge", query = "SELECT m FROM MsPlanHeader m WHERE m.upperAge = :upperAge")
    , @NamedQuery(name = "MsPlanHeader.findByUvType", query = "SELECT m FROM MsPlanHeader m WHERE m.uvType = :uvType")
    , @NamedQuery(name = "MsPlanHeader.findByUwRatingKey", query = "SELECT m FROM MsPlanHeader m WHERE m.uwRatingKey = :uwRatingKey")
    , @NamedQuery(name = "MsPlanHeader.findByUwRatio", query = "SELECT m FROM MsPlanHeader m WHERE m.uwRatio = :uwRatio")})
public class MsPlanHeader implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PLAN_CODE")
    private String planCode;
    @Column(name = "AGENT_QUALIFICATION")
    private String agentQualification;
    @Column(name = "ANNTY_CNTL_KEY")
    private String anntyCntlKey;
    @Column(name = "ANNTY_START_IND")
    private String anntyStartInd;
    @Column(name = "BUSINESS_PLAN_GROUP")
    private String businessPlanGroup;
    @Column(name = "CHANNEL")
    private String channel;
    @Column(name = "CLASS_OF_BUSINESS")
    private String classOfBusiness;
    @Column(name = "DIV_OPTION")
    private String divOption;
    @Column(name = "DIV_TYPE")
    private String divType;
    @Column(name = "EFFECTIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "ETI_VALUE")
    private String etiValue;
    @Column(name = "EXPIRY_DATE")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
    @Column(name = "FLAG_MICRR")
    private String flagMicrr;
    @Column(name = "HB2T_VAL")
    private String hb2tVal;
    @Column(name = "K_LEASING_NML")
    private String kLeasingNml;
    @Column(name = "KK_AFFINITY_NML")
    private String kkAffinityNml;
    @Column(name = "LOWER_AGE")
    private BigInteger lowerAge;
    @Column(name = "MATURITY_INDICATOR")
    private String maturityIndicator;
    @Column(name = "MATURITY_YEAR")
    private String maturityYear;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MAXIMUM_AMOUNT", precision = 126)
    private Double maximumAmount;
    @Column(name = "MINIMUM_AMOUNT", precision = 126)
    private Double minimumAmount;
    @Column(name = "MX40VALUE", length = 5)
    private String mx40value;
    @Column(name = "NC_TYPE", length = 5)
    private String ncType;
    @Column(name = "NON_MED_QUALIFICATION", length = 1)
    private String nonMedQualification;
    @Column(name = "PA_TYPE", length = 1)
    private String paType;
    @Column(name = "PAYMENT", length = 20)
    private String payment;
    @Column(name = "PH_ET_FACE_TYPE", length = 1)
    private String phEtFaceType;
    @Column(name = "PLAN_GROUPTYPE", length = 20)
    private String planGrouptype;
    @Column(name = "PLAN_SYSTEM", length = 25)
    private String planSystem;
    @Column(name = "PLANTYPE", length = 4)
    private String plantype;
    @Column(name = "PR_DATE")
    @Temporal(TemporalType.DATE)
    private Date prDate;
    @Column(name = "PREMIUM_CHANGE_INDICATOR", length = 2)
    private String premiumChangeIndicator;
    @Column(name = "PREMIUM_CHANGE_YEAR", length = 3)
    private String premiumChangeYear;
    @Column(name = "PREMIUM_INDICATOR", length = 1)
    private String premiumIndicator;
    @Column(name = "PRODUCT_LICENSE", length = 1)
    private String productLicense;
    @Column(name = "RIDER_ADD_IND", length = 1)
    private String riderAddInd;
    @Column(name = "RPU_VALUE", length = 7)
    private String rpuValue;
    @Column(name = "SEX", length = 1)
    private String sex;
    @Column(name = "SURRENDER_TYPE", length = 5)
    private String surrenderType;
    @Column(name = "TAKAFUL_GROUP_TYPE", length = 1)
    private String takafulGroupType;
    @Column(name = "TAKAFUL_TYPE", length = 10)
    private String takafulType;
    @Column(name = "TYPE_INSURANCE", length = 1)
    private String typeInsurance;
    @Column(name = "UPPER_AGE")
    private BigInteger upperAge;
    @Column(name = "UV_TYPE", length = 5)
    private String uvType;
    @Column(name = "UW_RATING_KEY", length = 2)
    private String uwRatingKey;
    @Column(name = "UW_RATIO", length = 3)
    private String uwRatio;

    public MsPlanHeader() {
    }

    public MsPlanHeader(String planCode) {
        this.planCode = planCode;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getAgentQualification() {
        return agentQualification;
    }

    public void setAgentQualification(String agentQualification) {
        this.agentQualification = agentQualification;
    }

    public String getAnntyCntlKey() {
        return anntyCntlKey;
    }

    public void setAnntyCntlKey(String anntyCntlKey) {
        this.anntyCntlKey = anntyCntlKey;
    }

    public String getAnntyStartInd() {
        return anntyStartInd;
    }

    public void setAnntyStartInd(String anntyStartInd) {
        this.anntyStartInd = anntyStartInd;
    }

    public String getBusinessPlanGroup() {
        return businessPlanGroup;
    }

    public void setBusinessPlanGroup(String businessPlanGroup) {
        this.businessPlanGroup = businessPlanGroup;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getClassOfBusiness() {
        return classOfBusiness;
    }

    public void setClassOfBusiness(String classOfBusiness) {
        this.classOfBusiness = classOfBusiness;
    }

    public String getDivOption() {
        return divOption;
    }

    public void setDivOption(String divOption) {
        this.divOption = divOption;
    }

    public String getDivType() {
        return divType;
    }

    public void setDivType(String divType) {
        this.divType = divType;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEtiValue() {
        return etiValue;
    }

    public void setEtiValue(String etiValue) {
        this.etiValue = etiValue;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getFlagMicrr() {
        return flagMicrr;
    }

    public void setFlagMicrr(String flagMicrr) {
        this.flagMicrr = flagMicrr;
    }

    public String getHb2tVal() {
        return hb2tVal;
    }

    public void setHb2tVal(String hb2tVal) {
        this.hb2tVal = hb2tVal;
    }

    public String getKLeasingNml() {
        return kLeasingNml;
    }

    public void setKLeasingNml(String kLeasingNml) {
        this.kLeasingNml = kLeasingNml;
    }

    public String getKkAffinityNml() {
        return kkAffinityNml;
    }

    public void setKkAffinityNml(String kkAffinityNml) {
        this.kkAffinityNml = kkAffinityNml;
    }

    public BigInteger getLowerAge() {
        return lowerAge;
    }

    public void setLowerAge(BigInteger lowerAge) {
        this.lowerAge = lowerAge;
    }

    public String getMaturityIndicator() {
        return maturityIndicator;
    }

    public void setMaturityIndicator(String maturityIndicator) {
        this.maturityIndicator = maturityIndicator;
    }

    public String getMaturityYear() {
        return maturityYear;
    }

    public void setMaturityYear(String maturityYear) {
        this.maturityYear = maturityYear;
    }

    public Double getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Double maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public Double getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Double minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public String getMx40value() {
        return mx40value;
    }

    public void setMx40value(String mx40value) {
        this.mx40value = mx40value;
    }

    public String getNcType() {
        return ncType;
    }

    public void setNcType(String ncType) {
        this.ncType = ncType;
    }

    public String getNonMedQualification() {
        return nonMedQualification;
    }

    public void setNonMedQualification(String nonMedQualification) {
        this.nonMedQualification = nonMedQualification;
    }

    public String getPaType() {
        return paType;
    }

    public void setPaType(String paType) {
        this.paType = paType;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPhEtFaceType() {
        return phEtFaceType;
    }

    public void setPhEtFaceType(String phEtFaceType) {
        this.phEtFaceType = phEtFaceType;
    }

    public String getPlanGrouptype() {
        return planGrouptype;
    }

    public void setPlanGrouptype(String planGrouptype) {
        this.planGrouptype = planGrouptype;
    }

    public String getPlanSystem() {
        return planSystem;
    }

    
    
    public void setPlanSystem(String planSystem) {
        this.planSystem = planSystem;
    }

    public String getPlantype() {
        return plantype;
    }

    public void setPlantype(String plantype) {
        this.plantype = plantype;
    }

    public Date getPrDate() {
        return prDate;
    }

    public void setPrDate(Date prDate) {
        this.prDate = prDate;
    }

    public String getPremiumChangeIndicator() {
        return premiumChangeIndicator;
    }

    public void setPremiumChangeIndicator(String premiumChangeIndicator) {
        this.premiumChangeIndicator = premiumChangeIndicator;
    }

    public String getPremiumChangeYear() {
        return premiumChangeYear;
    }

    public void setPremiumChangeYear(String premiumChangeYear) {
        this.premiumChangeYear = premiumChangeYear;
    }

    public String getPremiumIndicator() {
        return premiumIndicator;
    }

    public void setPremiumIndicator(String premiumIndicator) {
        this.premiumIndicator = premiumIndicator;
    }

    public String getProductLicense() {
        return productLicense;
    }

    public void setProductLicense(String productLicense) {
        this.productLicense = productLicense;
    }

    public String getRiderAddInd() {
        return riderAddInd;
    }

    public void setRiderAddInd(String riderAddInd) {
        this.riderAddInd = riderAddInd;
    }

    public String getRpuValue() {
        return rpuValue;
    }

    public void setRpuValue(String rpuValue) {
        this.rpuValue = rpuValue;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSurrenderType() {
        return surrenderType;
    }

    public void setSurrenderType(String surrenderType) {
        this.surrenderType = surrenderType;
    }

    public String getTakafulGroupType() {
        return takafulGroupType;
    }

    public void setTakafulGroupType(String takafulGroupType) {
        this.takafulGroupType = takafulGroupType;
    }

    public String getTakafulType() {
        return takafulType;
    }

    public void setTakafulType(String takafulType) {
        this.takafulType = takafulType;
    }

    public String getTypeInsurance() {
        return typeInsurance;
    }

    public void setTypeInsurance(String typeInsurance) {
        this.typeInsurance = typeInsurance;
    }

    public BigInteger getUpperAge() {
        return upperAge;
    }

    public void setUpperAge(BigInteger upperAge) {
        this.upperAge = upperAge;
    }

    public String getUvType() {
        return uvType;
    }

    public void setUvType(String uvType) {
        this.uvType = uvType;
    }

    public String getUwRatingKey() {
        return uwRatingKey;
    }

    public void setUwRatingKey(String uwRatingKey) {
        this.uwRatingKey = uwRatingKey;
    }

    public String getUwRatio() {
        return uwRatio;
    }

    public void setUwRatio(String uwRatio) {
        this.uwRatio = uwRatio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (planCode != null ? planCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsPlanHeader)) {
            return false;
        }
        MsPlanHeader other = (MsPlanHeader) object;
        if ((this.planCode == null && other.planCode != null) || (this.planCode != null && !this.planCode.equals(other.planCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsPlanHeader[ planCode=" + planCode + " ]";
    }
    
}
