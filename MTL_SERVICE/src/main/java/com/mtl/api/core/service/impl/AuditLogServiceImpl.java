package com.mtl.api.core.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mtl.api.core.service.interfaces.AuditLogService;
import com.mtl.api.core.utils.ObjectUtils;
import com.mtl.model.underwriting.AuditLogModel;

@Service
public class AuditLogServiceImpl implements AuditLogService {

	private static final Logger log = LogManager.getLogger(AuditLogServiceImpl.class);
	@Value("${audit.log.dir}")
	public String directory;

	public static final DateFormat SDF = new SimpleDateFormat("yyy-MM-dd", new Locale("th", "TH"));

	@Async
	@Override
	public CompletableFuture<?> writeLog(AuditLogModel model, LogType logType){

//		try {
//
//			String path = getDirectoryPath(model.getRef1());
//			createDirectory(path);
//			writeToFile(path, logType.getName(), model);
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return CompletableFuture.completedFuture(new Object());
	}

	private void writeToFile(String path, String fileName, Object o)
			throws JsonGenerationException, JsonMappingException, IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(path).append(File.separator).append(fileName).append(".json");
		ObjectUtils.MAPPER.writeValue(new File(sb.toString()), o);
		
	}

	private String getDirectoryPath(String uuid) {
		StringBuilder sb = new StringBuilder();
		String now = SDF.format(new Date());
		sb.append(directory).append(File.separator).append(now).append(File.separator).append(uuid);
		String path = sb.toString();
		return path;
	}

	private void createDirectory(String path) {
		File file = new File(path);
		file.mkdirs();
	}
}
