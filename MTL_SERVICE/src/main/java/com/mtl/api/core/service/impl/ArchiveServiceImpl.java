package com.mtl.api.core.service.impl;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.api.core.repository.interfaces.DocumentRepository;
import com.mtl.api.core.service.interfaces.ArchiveService;
import com.mtl.model.document.Document;
import com.mtl.model.document.DocumentMetadata;

/**
 * A service to save, find and get documents from an archive. 
 * 
 * @author buncha.p
 */
@Service
public class ArchiveServiceImpl implements ArchiveService, Serializable {

    private static final long serialVersionUID = 8119784722798361327L;
    
    @Autowired
    private DocumentRepository documentRepository;

    /**
     * Saves a document in the archive.
     * @see com.mtl.api.core.service.interfaces.ArchiveService.archive.service.IArchiveService#save(org.murygin.archive.service.Document)
     */
    @Override
    public DocumentMetadata save(Document document) {
        documentRepository.insert(document); 
        return document.getMetadata();
    }
    
    /**
     * Finds document in the archive
     * @see com.mtl.api.core.service.interfaces.ArchiveService.archive.service.IArchiveService#findDocuments(java.lang.String, java.util.Date)
     */
    @Override
    public List<DocumentMetadata> findDocuments(String personName, Date date) {
        return documentRepository.findByPersonNameDate(personName, date);
    }
    
    /**
     * Returns the document file from the archive
     * @see com.mtl.api.core.service.interfaces.ArchiveService.archive.service.IArchiveService#getDocumentFile(java.lang.String)
     */
    @Override
    public byte[] getDocumentFile(String id) {
        Document document = documentRepository.load(id);
        if(document!=null) {

        	System.out.println( document.toString());
        	return document.getFileData();
        } else {
            return null;
        }
    }
    
    @Override
    public byte[] getDocumentFileResizing( String id ) throws ApplicationException{
		int width = 128;
		int height = 96;
    	byte[] fileData = getDocumentFile( id );
    	if( fileData != null){
    		return scale(fileData, width, height);
    	}else{
    		return null;
    	}
    }
    
    
    public byte[] scale(byte[] fileData, int width, int height ) throws ApplicationException {
    	
    	try {
    		ByteArrayInputStream in = new ByteArrayInputStream(fileData);
    		BufferedImage img = ImageIO.read(in);
    		if(height == 0) {
    			height = (width * img.getHeight())/ img.getWidth(); 
    		}
    		if(width == 0) {
    			width = (height * img.getWidth())/ img.getHeight();
    		}
    		Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
    		BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    		imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);

    		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    		ImageIO.write(imageBuff, "jpg", buffer);

    		return buffer.toByteArray();
    	} catch (IOException e) {
    		throw new ApplicationException("IOException in scale", null);
    	} catch( NullPointerException e ){
    		throw new ApplicationException("Nullpointer in scale", null);
    	}
    }




}