package com.mtl.api.core.repository.interfaces;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsRuleChannelMapping;
@Repository
public interface MsRuleMappingRepository extends CrudRepository<MsRuleChannelMapping, BigDecimal> {
	public List<MsRuleChannelMapping> findAllByIsDelete(String isDelete);

}
