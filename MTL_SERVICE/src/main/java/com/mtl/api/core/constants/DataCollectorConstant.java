package com.mtl.api.core.constants;

public class DataCollectorConstant {

	public static class PolicyStatus {
		public static final String[] INFORCE = { "0", "1", "4", "6", "7", "8", "9", "Z" };
	}

	public static class PolicyRelationship {
		public static final String I = "I"; // Insured
		public static final String O = "O"; // Owner
	}

	public static class DeleteStatus {
		public static final Character T = 'T'; // 
		public static final Character F = 'F'; // 
	}

	public static class DataCollector{
		public static final String DC1 = "DC1";
		public static final String DC2 = "DC2";
		public static final String DC3 = "DC3";
		public static final String DC4 = "DC4";
		public static final String DC5 = "DC5";
		public static final String DC6 = "DC6";
	}
	
	public static class PlanTypeGroup {
		public static String BASIC = "BASIC";
		public static String RIDER = "RIDER";
	}
}
