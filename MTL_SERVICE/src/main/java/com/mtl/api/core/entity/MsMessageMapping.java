/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_MESSAGE_MAPPING", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsMessageMapping.findAll", query = "SELECT m FROM MsMessageMapping m")
    , @NamedQuery(name = "MsMessageMapping.findById", query = "SELECT m FROM MsMessageMapping m WHERE m.id = :id")
    , @NamedQuery(name = "MsMessageMapping.findByCreatedBy", query = "SELECT m FROM MsMessageMapping m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsMessageMapping.findByCreatedDate", query = "SELECT m FROM MsMessageMapping m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsMessageMapping.findByIsDelete", query = "SELECT m FROM MsMessageMapping m WHERE m.isDelete = :isDelete")
    , @NamedQuery(name = "MsMessageMapping.findByMessageCode", query = "SELECT m FROM MsMessageMapping m WHERE m.messageCode = :messageCode")
    , @NamedQuery(name = "MsMessageMapping.findByMessageTh", query = "SELECT m FROM MsMessageMapping m WHERE m.messageTh = :messageTh")
    , @NamedQuery(name = "MsMessageMapping.findByMessageEn", query = "SELECT m FROM MsMessageMapping m WHERE m.messageEn = :messageEn")
    , @NamedQuery(name = "MsMessageMapping.findByMessageLevel", query = "SELECT m FROM MsMessageMapping m WHERE m.messageLevel = :messageLevel")
    , @NamedQuery(name = "MsMessageMapping.findByUpdatedDate", query = "SELECT m FROM MsMessageMapping m WHERE m.updatedDate = :updatedDate")
    , @NamedQuery(name = "MsMessageMapping.findByChannelCode", query = "SELECT m FROM MsMessageMapping m WHERE m.channelCode = :channelCode")})
public class MsMessageMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @Basic(optional = false)
    @Column(name = "MESSAGE_CODE", nullable = false, length = 32)
    private String messageCode;
    @Column(name = "MESSAGE_TH")
    private String messageTh;
    @Column(name = "MESSAGE_EN")
    private String messageEn;
    @Column(name = "MESSAGE_LEVEL")
    private String messageLevel;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    @Column(name = "CHANNEL_CODE")
    private BigInteger channelCode;

    public MsMessageMapping() {
    }

    public MsMessageMapping(BigDecimal id) {
        this.id = id;
    }

    public MsMessageMapping(BigDecimal id, String createdBy, String messageCode) {
        this.id = id;
        this.createdBy = createdBy;
        this.messageCode = messageCode;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }


    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public BigInteger getChannelCode() {
        return channelCode;
    }

	public String getMessageTh() {
		return messageTh;
	}

	public void setMessageTh(String messageTh) {
		this.messageTh = messageTh;
	}

	public String getMessageEn() {
		return messageEn;
	}

	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}

	public String getMessageLevel() {
		return messageLevel;
	}

	public void setMessageLevel(String messageLevel) {
		this.messageLevel = messageLevel;
	}

	public void setChannelCode(BigInteger channelCode) {
        this.channelCode = channelCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsMessageMapping)) {
            return false;
        }
        MsMessageMapping other = (MsMessageMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsMessageMapping[ id=" + id + " ]";
    }
    
}
