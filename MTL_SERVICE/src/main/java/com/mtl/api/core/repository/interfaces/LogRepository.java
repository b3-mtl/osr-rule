package com.mtl.api.core.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.TrLogger;


@Repository
public interface LogRepository extends CrudRepository<TrLogger, Long> {

}
