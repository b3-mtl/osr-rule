package com.mtl.api.core.repository.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsCollectorMapping;
import com.mtl.api.core.entity.MsRule;

@Repository
public interface MsCollectorMappingRepository extends CrudRepository<MsCollectorMapping, Integer> {

	public List<MsCollectorMapping> findAllByRuleCode(MsRule ruleCode);

	public List<MsCollectorMapping> findAllByRuleCodeAndIsDelete( MsRule ruleCode, Character isDelete);
	
	@Query(value ="SELECT COLLECTOR_CODE FROM MS_COLLECTOR_MAPPING WHERE RULE_CODE = ?1 and is_active = 'Y' " ,  nativeQuery = true)
	public List<String> findAllByRuleCodeNativeQuery(String ruleCode);
}  

