/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_RULE_CHANNEL_MAPPING", catalog = "")
@XmlRootElement
public class MsRuleChannelMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "SECTION", nullable = false, length = 256)
    private String section;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "IS_DELETE")
    private String isDelete;
    @Column(name = "IS_ACTIVE")
    private String isActive;
    @JoinColumn(name = "APP_TYPE_CODE", referencedColumnName = "APP_TYPE_CODE", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MsAppType appTypeCode;
    @JoinColumn(name = "CHANNEL_CODE", referencedColumnName = "CHANNEL_CODE", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MsChannel channelCode;
    @JoinColumn(name = "RULE_CODE", referencedColumnName = "RULE_CODE", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MsRule ruleCode;

    public MsRuleChannelMapping() {
    }

    public MsRuleChannelMapping(BigDecimal id) {
        this.id = id;
    }

    public MsRuleChannelMapping(BigDecimal id, String section, String createdBy) {
        this.id = id;
        this.section = section;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public MsAppType getAppTypeCode() {
        return appTypeCode;
    }

    public void setAppTypeCode(MsAppType appTypeCode) {
        this.appTypeCode = appTypeCode;
    }

    public MsChannel getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(MsChannel channelCode) {
        this.channelCode = channelCode;
    }

    public MsRule getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(MsRule ruleCode) {
        this.ruleCode = ruleCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsRuleChannelMapping)) {
            return false;
        }
        MsRuleChannelMapping other = (MsRuleChannelMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsRuleMapping[ id=" + id + " ]";
    }
    
}
