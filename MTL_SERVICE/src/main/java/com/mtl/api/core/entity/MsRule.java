/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_RULE", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsRule.findAll", query = "SELECT m FROM MsRule m")
    , @NamedQuery(name = "MsRule.findById", query = "SELECT m FROM MsRule m WHERE m.id = :id")
    , @NamedQuery(name = "MsRule.findByClassName", query = "SELECT m FROM MsRule m WHERE m.className = :className")
    , @NamedQuery(name = "MsRule.findByCreatedBy", query = "SELECT m FROM MsRule m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsRule.findByCreatedDate", query = "SELECT m FROM MsRule m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsRule.findByIsDelete", query = "SELECT m FROM MsRule m WHERE m.isDelete = :isDelete")
    , @NamedQuery(name = "MsRule.findByRuleDesc", query = "SELECT m FROM MsRule m WHERE m.ruleDesc = :ruleDesc")
    , @NamedQuery(name = "MsRule.findByRuleCode", query = "SELECT m FROM MsRule m WHERE m.ruleCode = :ruleCode")
    , @NamedQuery(name = "MsRule.findByUpdatedDate", query = "SELECT m FROM MsRule m WHERE m.updatedDate = :updatedDate")})
public class MsRule implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "CLASS_NAME", nullable = false, length = 64)
    private String className;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @Column(name = "RULE_DESC", length = 2048)
    private String ruleDesc;
    @Basic(optional = false)
    @Column(name = "RULE_CODE", nullable = false, length = 32)
    private String ruleCode;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ruleCode", fetch = FetchType.LAZY)
    private List<MsRuleChannelMapping> msRuleMappingList;

    public MsRule() {
    }

    public MsRule(BigDecimal id) {
        this.id = id;
    }

    public MsRule(BigDecimal id, String className, String createdBy, String ruleCode) {
        this.id = id;
        this.className = className;
        this.createdBy = createdBy;
        this.ruleCode = ruleCode;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MsRuleChannelMapping> getMsRuleMappingList() {
        return msRuleMappingList;
    }

    public void setMsRuleMappingList(List<MsRuleChannelMapping> msRuleMappingList) {
        this.msRuleMappingList = msRuleMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsRule)) {
            return false;
        }
        MsRule other = (MsRule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsRule[ id=" + id + " ]";
    }
    
}
