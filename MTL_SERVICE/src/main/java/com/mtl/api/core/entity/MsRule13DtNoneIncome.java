/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_RULE_13_DT_NONE_INCOME", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsRule13DtNoneIncome.findAll", query = "SELECT m FROM MsRule13DtNoneIncome m")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findById", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.id = :id")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByOccCode", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.occCode = :occCode")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByMinSumIns", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.minSumIns = :minSumIns")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByMaxSumIns", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.maxSumIns = :maxSumIns")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByMessageDesc", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.messageDesc = :messageDesc")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByCreatedDate", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByUpdatedDate", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.updatedDate = :updatedDate")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByCreatedBy", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsRule13DtNoneIncome.findByIsDelete", query = "SELECT m FROM MsRule13DtNoneIncome m WHERE m.isDelete = :isDelete")})
public class MsRule13DtNoneIncome implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Column(name = "OCC_CODE", length = 32)
    private String occCode;
    @Column(name = "MIN_SUM_INS", precision = 18, scale = 2)
    private BigDecimal minSumIns;
    @Column(name = "MAX_SUM_INS", precision = 18, scale = 2)
    private BigDecimal maxSumIns;
    @Column(name = "MESSAGE_DESC", length = 4000)
    private String messageDesc;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;

    public MsRule13DtNoneIncome() {
    }

    public MsRule13DtNoneIncome(BigDecimal id) {
        this.id = id;
    }

    public MsRule13DtNoneIncome(BigDecimal id, String createdBy) {
        this.id = id;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getOccCode() {
        return occCode;
    }

    public void setOccCode(String occCode) {
        this.occCode = occCode;
    }

    public BigDecimal getMinSumIns() {
        return minSumIns;
    }

    public void setMinSumIns(BigDecimal minSumIns) {
        this.minSumIns = minSumIns;
    }

    public BigDecimal getMaxSumIns() {
        return maxSumIns;
    }

    public void setMaxSumIns(BigDecimal maxSumIns) {
        this.maxSumIns = maxSumIns;
    }

    public String getMessageDesc() {
        return messageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        this.messageDesc = messageDesc;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsRule13DtNoneIncome)) {
            return false;
        }
        MsRule13DtNoneIncome other = (MsRule13DtNoneIncome) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsRule13DtNoneIncome[ id=" + id + " ]";
    }
    
}
