/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "TR_LOGGER", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TrLogger.findAll", query = "SELECT t FROM TrLogger t")
    , @NamedQuery(name = "TrLogger.findById", query = "SELECT t FROM TrLogger t WHERE t.id = :id")
    , @NamedQuery(name = "TrLogger.findByEventDate", query = "SELECT t FROM TrLogger t WHERE t.eventDate = :eventDate")
    , @NamedQuery(name = "TrLogger.findByException", query = "SELECT t FROM TrLogger t WHERE t.exception = :exception")
    , @NamedQuery(name = "TrLogger.findByLevel", query = "SELECT t FROM TrLogger t WHERE t.level = :level")
    , @NamedQuery(name = "TrLogger.findByLogger", query = "SELECT t FROM TrLogger t WHERE t.logger = :logger")
    , @NamedQuery(name = "TrLogger.findByMessage", query = "SELECT t FROM TrLogger t WHERE t.message = :message")})
public class TrLogger implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Column(name = "EVENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Column(name = "EXCEPTION", length = 4000)
    private String exception;
    @Column(name = "LEVEL", length = 255)
    private String level;
    @Column(name = "LOGGER", length = 255)
    private String logger;
    @Column(name = "MESSAGE", length = 4000)
    private String message;

    public TrLogger() {
    }

    public TrLogger(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLogger() {
        return logger;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrLogger)) {
            return false;
        }
        TrLogger other = (TrLogger) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.TrLogger[ id=" + id + " ]";
    }
    
}
