package com.mtl.api.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.annotation.JsonInclude;
public class ObjectUtils {

	public static final  ObjectMapper MAPPER;
	
	static {
		MAPPER = new ObjectMapper();
		MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL); // ignore the null fields globally
		MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
	}
}
