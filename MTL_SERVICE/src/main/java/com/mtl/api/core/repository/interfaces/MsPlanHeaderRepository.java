package com.mtl.api.core.repository.interfaces;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsPlanHeader;

@Repository
public interface MsPlanHeaderRepository extends CrudRepository<MsPlanHeader, String>{
	
	public List<MsPlanHeader> findAll();
	
	@Query("select p from MsPlanHeader p where p.planCode = :planCode ")
	public MsPlanHeader findByPlanCode(@Param("planCode")String planCode);
	
	@Query("select p from MsPlanHeader p where p.channel = :channel ")
	public MsPlanHeader findByChannel(@Param("channel")String channel);
}
