/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_COLLECTOR", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsCollector.findAll", query = "SELECT m FROM MsCollector m")
    , @NamedQuery(name = "MsCollector.findById", query = "SELECT m FROM MsCollector m WHERE m.id = :id")
    , @NamedQuery(name = "MsCollector.findByCollectorDesc", query = "SELECT m FROM MsCollector m WHERE m.collectorDesc = :collectorDesc")
    , @NamedQuery(name = "MsCollector.findByCollectorCode", query = "SELECT m FROM MsCollector m WHERE m.collectorCode = :collectorCode")
    , @NamedQuery(name = "MsCollector.findByCreatedBy", query = "SELECT m FROM MsCollector m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsCollector.findByCreatedDate", query = "SELECT m FROM MsCollector m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsCollector.findByIsDelete", query = "SELECT m FROM MsCollector m WHERE m.isDelete = :isDelete")
    , @NamedQuery(name = "MsCollector.findByUpdatedDate", query = "SELECT m FROM MsCollector m WHERE m.updatedDate = :updatedDate")})
public class MsCollector implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Column(name = "COLLECTOR_DESC", length = 2048)
    private String collectorDesc;
    @Basic(optional = false)
    @Column(name = "COLLECTOR_CODE", nullable = false, length = 128)
    private String collectorCode;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "collectorCode", fetch = FetchType.LAZY)
    private List<MsCollectorMapping> msCollectorMappingList;

    public MsCollector() {
    }

    public MsCollector(BigDecimal id) {
        this.id = id;
    }

    public MsCollector(BigDecimal id, String collectorCode, String createdBy) {
        this.id = id;
        this.collectorCode = collectorCode;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCollectorDesc() {
        return collectorDesc;
    }

    public void setCollectorDesc(String collectorDesc) {
        this.collectorDesc = collectorDesc;
    }

    public String getCollectorCode() {
        return collectorCode;
    }

    public void setCollectorCode(String collectorCode) {
        this.collectorCode = collectorCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MsCollectorMapping> getMsCollectorMappingList() {
        return msCollectorMappingList;
    }

    public void setMsCollectorMappingList(List<MsCollectorMapping> msCollectorMappingList) {
        this.msCollectorMappingList = msCollectorMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsCollector)) {
            return false;
        }
        MsCollector other = (MsCollector) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsCollector[ id=" + id + " ]";
    }
    
}
