/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_RULE_13_DT_PILOT", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsRule13DtPilot.findAll", query = "SELECT m FROM MsRule13DtPilot m")
    , @NamedQuery(name = "MsRule13DtPilot.findById", query = "SELECT m FROM MsRule13DtPilot m WHERE m.id = :id")
    , @NamedQuery(name = "MsRule13DtPilot.findByAppType", query = "SELECT m FROM MsRule13DtPilot m WHERE m.appType = :appType")
    , @NamedQuery(name = "MsRule13DtPilot.findByChannel", query = "SELECT m FROM MsRule13DtPilot m WHERE m.channel = :channel")
    , @NamedQuery(name = "MsRule13DtPilot.findBySection", query = "SELECT m FROM MsRule13DtPilot m WHERE m.section = :section")
    , @NamedQuery(name = "MsRule13DtPilot.findByOccCode", query = "SELECT m FROM MsRule13DtPilot m WHERE m.occCode = :occCode")
    , @NamedQuery(name = "MsRule13DtPilot.findByMinSumIns", query = "SELECT m FROM MsRule13DtPilot m WHERE m.minSumIns = :minSumIns")
    , @NamedQuery(name = "MsRule13DtPilot.findByMaxSumIns", query = "SELECT m FROM MsRule13DtPilot m WHERE m.maxSumIns = :maxSumIns")
    , @NamedQuery(name = "MsRule13DtPilot.findByMessageDesc", query = "SELECT m FROM MsRule13DtPilot m WHERE m.messageDesc = :messageDesc")
    , @NamedQuery(name = "MsRule13DtPilot.findByCreatedDate", query = "SELECT m FROM MsRule13DtPilot m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsRule13DtPilot.findByUpdatedDate", query = "SELECT m FROM MsRule13DtPilot m WHERE m.updatedDate = :updatedDate")
    , @NamedQuery(name = "MsRule13DtPilot.findByCreatedBy", query = "SELECT m FROM MsRule13DtPilot m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsRule13DtPilot.findByIsDelete", query = "SELECT m FROM MsRule13DtPilot m WHERE m.isDelete = :isDelete")})
public class MsRule13DtPilot implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Column(name = "APP_TYPE", length = 255)
    private String appType;
    @Column(name = "CHANNEL", length = 255)
    private String channel;
    @Column(name = "SECTION", length = 255)
    private String section;
    @Column(name = "OCC_CODE", length = 32)
    private String occCode;
    @Column(name = "MIN_SUM_INS", precision = 18, scale = 2)
    private BigDecimal minSumIns;
    @Column(name = "MAX_SUM_INS", precision = 18, scale = 2)
    private BigDecimal maxSumIns;
    @Column(name = "MESSAGE_DESC", length = 4000)
    private String messageDesc;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;

    public MsRule13DtPilot() {
    }

    public MsRule13DtPilot(BigDecimal id) {
        this.id = id;
    }

    public MsRule13DtPilot(BigDecimal id, String createdBy) {
        this.id = id;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getOccCode() {
        return occCode;
    }

    public void setOccCode(String occCode) {
        this.occCode = occCode;
    }

    public BigDecimal getMinSumIns() {
        return minSumIns;
    }

    public void setMinSumIns(BigDecimal minSumIns) {
        this.minSumIns = minSumIns;
    }

    public BigDecimal getMaxSumIns() {
        return maxSumIns;
    }

    public void setMaxSumIns(BigDecimal maxSumIns) {
        this.maxSumIns = maxSumIns;
    }

    public String getMessageDesc() {
        return messageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        this.messageDesc = messageDesc;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsRule13DtPilot)) {
            return false;
        }
        MsRule13DtPilot other = (MsRule13DtPilot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsRule13DtPilot[ id=" + id + " ]";
    }
    
}
