package com.mtl.api.core.repository.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsRule;
@Repository
public interface MsRuleRepository extends CrudRepository<MsRule, Integer> {

	public List<MsRule> findAllByIsDelete(Character isDelete);
	
	@Query("SELECT r.className FROM MsRule r WHERE  r.isDelete = 'N' ")
	public List<String> getRuleClass();
	
	@Query(value ="SELECT RULE_CODE FROM MS_RULE WHERE CLASS_NAME = ?1 AND IS_DELETE = 'N'" ,  nativeQuery = true)
	public List<String> getRuleCode(String className);
	
}
