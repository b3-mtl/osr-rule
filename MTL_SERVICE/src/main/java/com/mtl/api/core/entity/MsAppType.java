/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_APP_TYPE", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsAppType.findAll", query = "SELECT m FROM MsAppType m")
    , @NamedQuery(name = "MsAppType.findById", query = "SELECT m FROM MsAppType m WHERE m.id = :id")
    , @NamedQuery(name = "MsAppType.findByAppTypeName", query = "SELECT m FROM MsAppType m WHERE m.appTypeName = :appTypeName")
    , @NamedQuery(name = "MsAppType.findByCreatedDate", query = "SELECT m FROM MsAppType m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsAppType.findByUpdatedDate", query = "SELECT m FROM MsAppType m WHERE m.updatedDate = :updatedDate")
    , @NamedQuery(name = "MsAppType.findByCreatedBy", query = "SELECT m FROM MsAppType m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsAppType.findByIsDelete", query = "SELECT m FROM MsAppType m WHERE m.isDelete = :isDelete")})
public class MsAppType implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "APP_TYPE_NAME", nullable = false, length = 128)
    private String appTypeName;
    @Column(name = "APP_TYPE_CODE", nullable = false, length = 128)
    private String appTypeCode;
	@Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "appTypeCode", fetch = FetchType.LAZY)
    private List<MsChannel> msChannelList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "appTypeCode", fetch = FetchType.LAZY)
    private List<MsRuleChannelMapping> msRuleMappingList;

    public MsAppType() {
    }

    public MsAppType(BigDecimal id) {
        this.id = id;
    }

    public MsAppType(BigDecimal id, String appType, String createdBy) {
        this.id = id;
        this.appTypeName = appType;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getAppTypeName() {
        return appTypeName;
    }

    public void setAppTypeName(String appTypeName) {
        this.appTypeName = appTypeName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public String getAppTypeCode() {
		return appTypeCode;
	}

	public void setAppTypeCode(String appTypeCode) {
		this.appTypeCode = appTypeCode;
	}
	
    @XmlTransient
    public List<MsChannel> getMsChannelList() {
        return msChannelList;
    }

    public void setMsChannelList(List<MsChannel> msChannelList) {
        this.msChannelList = msChannelList;
    }

    @XmlTransient
    public List<MsRuleChannelMapping> getMsRuleMappingList() {
        return msRuleMappingList;
    }

    public void setMsRuleMappingList(List<MsRuleChannelMapping> msRuleMappingList) {
        this.msRuleMappingList = msRuleMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsAppType)) {
            return false;
        }
        MsAppType other = (MsAppType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsAppType[ id=" + id + " ]";
    }
    
}
