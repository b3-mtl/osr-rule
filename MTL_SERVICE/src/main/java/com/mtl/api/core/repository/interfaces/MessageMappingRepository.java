package com.mtl.api.core.repository.interfaces;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsMessageMapping;

@Repository
public interface MessageMappingRepository extends CrudRepository<MsMessageMapping, BigDecimal> {

	public List<MsMessageMapping> findAllByIsDelete(Character isDelete);

}
