package com.mtl.api.core.utils;

import org.springframework.util.StringUtils;

public class ValidationUtils {

	public static boolean stringEmpty(String str) {
		return StringUtils.isEmpty(str);
	}

	public static boolean isOneOf(String source, String target) {
		if ( stringEmpty(source) )
			return true;
		else if (!stringEmpty(source)) {
			source = "," + source + ",";
			target = "," + target + ",";
			int i = source.indexOf(target);
			if (i != -1) {
				return true;
			}
		}
		return false;
	}

	public static boolean isOneOf(String source, String[] target) {
		if (stringEmpty(source) && target == null)
			return true;
		else if (!stringEmpty(source)) {
			for (String s : target) {
				if (source.equals(s)) {
					return true;
				}
			}
		}

		return false;
	}
}
