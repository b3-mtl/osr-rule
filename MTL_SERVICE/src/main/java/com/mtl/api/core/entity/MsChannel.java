/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_CHANNEL", catalog = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MsChannel.findAll", query = "SELECT m FROM MsChannel m")
    , @NamedQuery(name = "MsChannel.findById", query = "SELECT m FROM MsChannel m WHERE m.id = :id")
    , @NamedQuery(name = "MsChannel.findByChannelCode", query = "SELECT m FROM MsChannel m WHERE m.channelCode = :channelCode")
    , @NamedQuery(name = "MsChannel.findByCreatedDate", query = "SELECT m FROM MsChannel m WHERE m.createdDate = :createdDate")
    , @NamedQuery(name = "MsChannel.findByUpdatedDate", query = "SELECT m FROM MsChannel m WHERE m.updatedDate = :updatedDate")
    , @NamedQuery(name = "MsChannel.findByCreatedBy", query = "SELECT m FROM MsChannel m WHERE m.createdBy = :createdBy")
    , @NamedQuery(name = "MsChannel.findByIsDelete", query = "SELECT m FROM MsChannel m WHERE m.isDelete = :isDelete")})
public class MsChannel implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "CHANNEL_DESC", nullable = false, length = 128)
    private String channelDesc;
    @Column(name = "CHANNEL_CODE", nullable = false, length = 128)
    private String channelCode;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @JoinColumn(name = "APP_TYPE_CODE", referencedColumnName = "APP_TYPE_CODE", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MsAppType appTypeCode;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "channelCode", fetch = FetchType.LAZY)
    private List<MsRuleChannelMapping> msRuleMappingList;

    public MsChannel() {
    }

    public MsChannel(BigDecimal id) {
        this.id = id;
    }

    public MsChannel(BigDecimal id, String channelDesc, String createdBy) {
        this.id = id;
        this.channelDesc = channelDesc;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getChannelDesc() {
        return channelDesc;
    }

    public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public void setChannelDesc(String channelDesc) {
        this.channelDesc = channelDesc;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public MsAppType getAppTypeCode() {
        return appTypeCode;
    }

    public void setAppTypeCode(MsAppType appTypeCode) {
        this.appTypeCode = appTypeCode;
    }

    @XmlTransient
    public List<MsRuleChannelMapping> getMsRuleMappingList() {
        return msRuleMappingList;
    }

    public void setMsRuleMappingList(List<MsRuleChannelMapping> msRuleMappingList) {
        this.msRuleMappingList = msRuleMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsChannel)) {
            return false;
        }
        MsChannel other = (MsChannel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsChannel[ id=" + id + " ]";
    }
    
}
