package com.mtl.api.core.utils;

import lombok.Data;

@Data
public class Timmer {

	private Long start;
	private Long end;
	public Timmer() {
		super();
		start = System.currentTimeMillis();
	}
	
	
	public void stop() {
		end = System.currentTimeMillis();
	}
	
	
	public Long timeDiff() {
		stop();
		return (end - start);
	}
}
