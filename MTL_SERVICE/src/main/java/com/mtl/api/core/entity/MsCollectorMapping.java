/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mtl.api.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "MS_COLLECTOR_MAPPING", catalog = "")
	@XmlRootElement
	@NamedQueries({
	    @NamedQuery(name = "MsCollectorMapping.findAll", query = "SELECT m FROM MsCollectorMapping m")
	    , @NamedQuery(name = "MsCollectorMapping.findById", query = "SELECT m FROM MsCollectorMapping m WHERE m.id = :id")
	    , @NamedQuery(name = "MsCollectorMapping.findByCreatedBy", query = "SELECT m FROM MsCollectorMapping m WHERE m.createdBy = :createdBy")
	    , @NamedQuery(name = "MsCollectorMapping.findByCreatedDate", query = "SELECT m FROM MsCollectorMapping m WHERE m.createdDate = :createdDate")
	    , @NamedQuery(name = "MsCollectorMapping.findByIsDelete", query = "SELECT m FROM MsCollectorMapping m WHERE m.isDelete = :isDelete")
	    , @NamedQuery(name = "MsCollectorMapping.findByUpdatedDate", query = "SELECT m FROM MsCollectorMapping m WHERE m.updatedDate = :updatedDate")})
public class MsCollectorMapping implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 38, scale = 0)
    private BigDecimal id;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false, length = 128)
    private String createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "IS_DELETE")
    private Character isDelete;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "COLLECTOR_CODE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MsCollector collectorCode;
    @JoinColumn(name = "RULE_CODE")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MsRule ruleCode;

    public MsCollectorMapping() {
    }

    public MsCollectorMapping(BigDecimal id) {
        this.id = id;
    }

    public MsCollectorMapping(BigDecimal id, String createdBy) {
        this.id = id;
        this.createdBy = createdBy;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public MsCollector getCollectorCode() {
        return collectorCode;
    }

    public void setCollectorCode(MsCollector collectorCode) {
        this.collectorCode = collectorCode;
    }

    public MsRule getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(MsRule ruleCode) {
        this.ruleCode = ruleCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MsCollectorMapping)) {
            return false;
        }
        MsCollectorMapping other = (MsCollectorMapping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mtl.core.entity.MsCollectorMapping[ id=" + id + " ]";
    }
    
}
