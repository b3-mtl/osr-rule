package com.mtl.api.utils;

public class ServiceConstants {
	
	public static String SUCCESS = "0";
	public static String FAIL = "1";
	public static String VALIDATION_ERROR ="2";
	
	// Service Error Code
	
	public static String VALIDATION_PASS ="VALIDATION_PASS";
	
	public static String E001 ="E001";
	
	
	public static String UNDERWRITE_STATUS_WARN ="WARN";
	
	
	public static String RULE03="RULE03";
	public static String RULE04="RULE04";
	public static String RULE05="RULE05";
	public static String RULE06="RULE06";
	public static String RULE07="RULE07";
	public static String RULE11="RULE11";
	public static String RULE12="RULE12";
	public static String RULE13="RULE13";
	public static String RULE14="RULE14";
	public static String RULE16="RULE16";
	public static String RULE17="RULE17";
	public static String RULE21="RULE21";
 

}
