package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_BMI_BABY")
public class DT11BMIBaby {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_BMI_BABY")
    @SequenceGenerator(sequenceName = "SEQ_DT11_BMI_BABY", allocationSize = 1, name = "SEQ_DT11_BMI_BABY")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "SEX")
	private String sex;
	@Column (name = "AGE_IN_MONTH")
	private String ageInMonth;
	@Column (name = "HEIGHT_MIN")
	private String heightMin;
	@Column (name = "HEIGHT_MAX")
	private String heightMax;
	@Column (name = "WEIGHT_MIN")
	private String weightMin;
	@Column (name = "WEIGHT_MAX")
	private String weightMax;
	@Column (name = "RESULT")
	private String result;
	@Column (name = "BMI_PROBLEM")
	private String bmiProblem;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
