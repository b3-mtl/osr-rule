package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT07_MIB_HNW")
public class DT07MIBHnw {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT07_MIB_HNW")
    @SequenceGenerator(sequenceName = "SEQ_DT07_MIB_HNW", allocationSize = 1, name = "SEQ_DT07_MIB_HNW")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "CODE")
	private String code;
	@Column(name = "HIGH_RISK")
	private String highRisk;
	@Column(name = "MIB_AMLO")
	private String mibAmlo;
	@Column(name = "UNDERWRITE")
	private String underwrite;
	@Column(name = "IN_LIST")
	private String inList;
	@Column(name = "NO_LIST")
	private String noList;
	@Column(name = "STANDARD")
	private String standard;
	@Column(name = "MIB_CODE_LIST")
	private String mibCodeList;
	@Column(name = "I")
	private String i;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "CREATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
}
