package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

@Table(name = "DT14_EXCEPTION_PLAN_TYPE")

public class DT14ExceptionPlanType {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_EXCEPTION_PLAN_TYPE")
    @SequenceGenerator(sequenceName = "SEQ_DT14_EXCEPTION_PLAN_TYPE", allocationSize = 1, name = "SEQ_DT14_EXCEPTION_PLAN_TYPE")
	
	
	@Column (name ="ID")
	private Long id;
	@Column (name ="EXCEPTION_PLAN_TYPE")
	private String exceptionPlanType;
	@Column (name ="EXCEPTION_CHANNEL_CODE")
	private Long exceptionChannelCode;
	@Column (name ="CREATED_BY")
	private String createdBy;
	@Column (name ="CREATED_DATE")
	private Date createdDate;
	@Column (name ="UPDATED_BY")
	private String updatedBy;
	@Column (name ="UPDATED_DATE")
	private Date updatedDate;
	@Column (name ="IS_DELETE")
	private String isDelete = "N";
	
}