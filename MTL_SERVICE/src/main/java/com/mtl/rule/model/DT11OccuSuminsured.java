package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "SEQ_DT11_OCCU_SUMINSURED")
public class DT11OccuSuminsured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_OCCU_SUMINSURED")
	@SequenceGenerator(sequenceName = "SEQ_DT11_OCCU_SUMINSURED", allocationSize = 1, name = "SEQ_DT11_OCCU_SUMINSURED")

	@Column(name = "ID")
	private Long id;
	@Column(name = "SYSTEM_NAME")
	private String systemName;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "OCCUPATION_LEVEL")
	private String occupationLevel;
	@Column(name = "SUM_INSURED")
	private String sumInsured;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

}
