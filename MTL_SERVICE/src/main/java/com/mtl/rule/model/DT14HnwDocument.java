package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity

@Table(name = "DT14_HNW_DOCUMENT")
public class DT14HnwDocument {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_HNW_DOCUMENT")
    @SequenceGenerator(sequenceName = "SEQ_DT14_HNW_DOCUMENT", allocationSize = 1, name = "SEQ_DT14_HNW_DOCUMENT")
	
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "AGE_MIN")
	private Long ageMin;
	@Column (name = "AGE_MAX")
	private Long ageMax;
	@Column (name = "SUMINSURED_MIN")
	private Long sumInsuredMin;
	@Column (name = "SUMINSURED_MAX")
	private Long sumInsuredMax;
	@Column (name = "ADD_MIB_CLAIM_LIST")
	private String addMibClaimList;
	@Column (name = "FINANCIAL_FLAG")
	private String financialFlag;
	@Column (name = "REFER_IR")
	private String  referIR;
	@Column (name = "MESSAGE")
	private String message;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date  updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
	
}
