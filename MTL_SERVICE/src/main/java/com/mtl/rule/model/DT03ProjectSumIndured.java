package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_PROJECT_SUM_INSURED")
@Getter
@Setter
public class DT03ProjectSumIndured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PRODUCT_AMLO")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PRODUCT_AMLO", allocationSize = 1, name = "SEQ_DT03_PRODUCT_AMLO")
	
	@Column(name = "ID")
	private String id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "RIDER_PLAN_CODE")
	private String riderPlanCode;
	@Column(name = "SERVICE_BRANCH")
	private String serviceBranch;
	@Column(name = "AGENT_CODE")
	private String agentCode;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "PLAN_SYSTEM")
	private String planSystem;
	@Column(name = "SUM_INSURED")
	private String sumInsured;
	@Column(name = "LIMIT")
	private String limit;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}

