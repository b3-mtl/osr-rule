package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_OCCUPATION_PA")
public class DT11OccupationPa {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_OCCUPATION_PA")
    @SequenceGenerator(sequenceName = "SEQ_DT11_OCCUPATION_PA", allocationSize = 1, name = "SEQ_DT11_OCCUPATION_PA")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "OCCUPATION_CODE")
	private String occupationCode;
	@Column (name = "OCCUPATION_LEVEL_PA")
	private String occupationLevelPa;
	@Column (name = "RESULT_DT_11_1")
	private String resultDt111;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
