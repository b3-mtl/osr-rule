package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_RISK_AREAS_OFFICE")
public class DT11RiskAreasOffice {
	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_RISK_AREAS_OFFICE")
    @SequenceGenerator(sequenceName = "SEQ_DT11_RISK_AREAS_OFFICE", allocationSize = 1, name = "SEQ_DT11_RISK_AREAS_OFFICE")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "OFFICE_PROVINCE")
	private String officeProvince;
	@Column (name = "OFFICE_SUBURB")
	private String officeSuburb;
	@Column (name = "OFFICE_DISTRICT")
	private String officeDistrict;
	@Column (name = "MOO")
	private String moo;
	@Column (name = "HOUSE_NUMBER")
	private String houseNumber;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "LOG")
	private String log;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
