package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_AGE_SPECIAL_PLAN_ATLEAST")
public class DT11AgeSpecialPlanAtleast {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_AGE_SPECIAL_PLAN_A")
    @SequenceGenerator(sequenceName = "SEQ_DT11_AGE_SPECIAL_PLAN_A", allocationSize = 1, name = "SEQ_DT11_AGE_SPECIAL_PLAN_A")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "PLAN_CODE")
	private String planCode;
	@Column (name = "AGE")
	private String age;
	@Column (name = "RESULT_DT11_AGE")
	private String resultDt11Age;
	@Column (name = "FLAG11_AGE_SPECIAL")
	private String flag11AgeSpecial;

	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
