package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT16_FEMALE_DISEASE_LIST")
public class DT16FemaleDiseaseList {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT16_FEMALE_DISEASE_LIST")
    @SequenceGenerator(sequenceName = "SEQ_DT16_FEMALE_DISEASE_LIST", allocationSize = 1, name = "SEQ_DT16_FEMALE_DISEASE_LIST")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "SYMTON_NAME")
	private String symtonName;
	@Column(name = "UNDERWRITE")
	private String underwrite;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
