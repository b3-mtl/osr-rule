package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_BMI_CHILD_AND_ADULT")
public class DT11BMIChildAndAdult {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_BMI_CHILD_AND_ADULT")
    @SequenceGenerator(sequenceName = "SEQ_DT11_BMI_CHILD_AND_ADULT", allocationSize = 1, name = "SEQ_DT11_BMI_CHILD_AND_ADULT")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "SEX")
	private String sex;
	@Column (name = "AGE")
	private String age;
	@Column (name = "BMI_MIN")
	private String bmiMin;
	@Column (name = "BMI_MAX")
	private String bmiMax;
	@Column (name = "RESULT")
	private String result;
	@Column (name = "BMI_PROBLEM")
	private String bmiProblem;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
