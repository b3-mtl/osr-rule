package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_RISK_AREAS_PREST_ADDR")
public class DT11RiskAreasPrestAddr {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_RISK_AREAS_PREST_ADDR")
    @SequenceGenerator(sequenceName = "SEQ_DT11_RISK_AREAS_PREST_ADDR", allocationSize = 1, name = "SEQ_DT11_RISK_AREAS_PREST_ADDR")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "PRESENT_PROVINCE")
	private String presentProvince;
	@Column (name = "PRESENT_SUBURB")
	private String presentSuburb;
	@Column (name = "PRESENT_DISTRICT")
	private String presentDistrict;
	@Column (name = "MOO")
	private String moo;
	@Column (name = "HOUSE_NUMBER")
	private String houseNumber;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "LOG")
	private String log;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
