package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_OPD_SUM")
@Getter
@Setter
public class DT03OPDSum {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_OPD_SUM")
	@SequenceGenerator(sequenceName = "SEQ_DT03_OPD_SUM", allocationSize = 1, name = "SEQ_DT03_OPD_SUM")

	@Column(name = "CHANNEL_CODE")
	private String channelCode;
	@Column(name = "SYSTEM_REGISTED")
	private String systemRegisted;
	@Column(name = "MAX_OPD_SUMINSURED")
	private Long maxOpdSuminsured;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
