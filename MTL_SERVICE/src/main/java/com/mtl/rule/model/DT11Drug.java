package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_DRUG")
public class DT11Drug {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_DRUG")
    @SequenceGenerator(sequenceName = "SEQ_DT11_DRUG", allocationSize = 1, name = "SEQ_DT11_DRUG")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "DRUG")
	private String drug;
	@Column(name = "DRUG_TYPE")
	private String DrugType;
	@Column(name = "MESSAGE")
	private String message;
	@Column(name = "UNDERWRITE")
	private String underwrite;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete;
	@Column(name = "DOCUMENT")
	private String document;
}
