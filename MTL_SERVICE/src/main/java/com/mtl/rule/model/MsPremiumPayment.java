package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "MS_PREMIUM_PAYMENT")
@Getter
@Setter
public class MsPremiumPayment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MS_PREMIUM_PAYMENT")
	@SequenceGenerator(sequenceName = "SEQ_MS_PREMIUM_PAYMENT", allocationSize = 1, name = "SEQ_MS_PREMIUM_PAYMENT")

	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "CHANNEL_CODE")
	private String channelCode;
	@Column(name = "PREMIUM_PAYMENT")
	private String premiumPayment;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
	

}
