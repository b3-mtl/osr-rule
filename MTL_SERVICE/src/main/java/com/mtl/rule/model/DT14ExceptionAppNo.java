package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

@Table(name = "DT14_EXCEPTION_APPNO")
public class DT14ExceptionAppNo {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_EXCEPTION_APPNO")
    @SequenceGenerator(sequenceName = "SEQ_DT14_EXCEPTION_APPNO", allocationSize = 1, name = "SEQ_DT14_EXCEPTION_APPNO")
	
	
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "APP_FORM_NO")
	private String appFormNo;
	@Column (name = "APP_FORM_NO_KEY")
	private String appFormNoKey;
	@Column (name = "CHANNEL_GROUP")
	private String channelGroup;
	@Column (name = "DESCRIPTION")
	private String description;
	@Column (name = "APP_NO")
	private String appNo;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
	@Column (name = "IS_EXCEPT")
	private String isExcept = "N";
	
	
}
