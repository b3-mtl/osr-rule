package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_DIAG_SUM_PT")
@Getter
@Setter
public class DT03DiagnosisSumInsuredPT {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_DIAG_SUM_PT")
	@SequenceGenerator(sequenceName = "SEQ_DT03_DIAG_SUM_PT", allocationSize = 1, name = "SEQ_DT03_DIAG_SUM_PT")

	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "MIN_AGE")
	private String minAge;
	@Column(name = "MAX_AGE")
	private String maxAge;
	@Column(name = "LIMIT_WITH_LIFE_SUMINSURED")
	private String limitWithLifeSuminsured;
	@Column(name = "MIN_SUMINSURED")
	private String minSuminsured;
	@Column(name = "MAX_SUMINSURED")
	private String maxSuminsured;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
