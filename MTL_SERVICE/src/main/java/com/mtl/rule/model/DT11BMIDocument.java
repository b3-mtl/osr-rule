package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_BMI_DOCUMENT")
public class DT11BMIDocument {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_BMI_DOCUMENT")
    @SequenceGenerator(sequenceName = "SEQ_DT11_BMI_DOCUMENT", allocationSize = 1, name = "SEQ_DT11_BMI_DOCUMENT")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "AGE_MIN")
	private String ageMin;
	@Column (name = "AGE_MAX")
	private String ageMax;
	@Column (name = "BMI_MIN")
	private String bmiMin;
	@Column (name = "BMI_MAX")
	private String bmiMax;
	@Column (name = "EXCEPT_RIDER")
	private String exceptRider;
	@Column (name = "MAP_DOCUMENT")
	private String mapDocument;
	@Column (name = "REFER_UNDER_WRITE")
	private String referUnderWrite;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "LOG")
	private String log;
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
