package com.mtl.rule.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MAPPING_CLIENT_LOCATION_CODE")
public class MappingClientLocationCode {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MAPPING_CLIENT_LOCATION_CODE")
	@SequenceGenerator(sequenceName = "SEQ_MAPPING_CLIENT_LOCATION_CODE", allocationSize = 1, name = "SEQ_MAPPING_CLIENT_LOCATION_CODE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "CLIENT_LOCATION_CODE")
	private String clientLocationCode;
	@Column(name = "PROVINCE_CODE")
	private String provinceCode;
}
