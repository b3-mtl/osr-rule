package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT14_DOCUMENT_CLAIM")
@Access(AccessType.FIELD)
public class DT14DocumentClaim {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_DOCUMENT_CLAIM")
    @SequenceGenerator(sequenceName = "SEQ_DT14_DOCUMENT_CLAIM", allocationSize = 1, name = "SEQ_DT14_DOCUMENT_CLAIM")
	
	
	@Column (name = "ID")
	private Long   id;
	@Column (name = "CLAIM_CATEGORY")
	private String  claimCategory;
	@Column (name = "APP_TYPE")
	private String  appType;
	@Column (name = "PLAN_CODE")
	private String  planCode;
	@Column (name = "GROUP_CHANNEL")
	private String  groupChannel;
	@Column (name = "PLAN_TYPE")
	private String  planType;
	@Column (name = "CLAIM_LIST")
	private String  claimList;
	@Column (name = "MESSAGE_CODE")
	private String  messageCode;
	@Column (name = "AGE_MIN")
	private Long  ageMin;
	@Column (name = "AGE_MAX")
	private Long  ageMax;
	@Column (name = "SUM_INSURED_MIN")
	private Long  sumInsuredMin;
	@Column (name = "SUM_INSURED_MAX")
	private Long  sumInsuredMax;
	@Column (name = "CREATED_BY")
	private String  createdBy;
	@Column (name = "CREATED_DATE")
	private Date  createdDate;
	@Column (name = "UPDATED_BY")
	private String  updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date  updatedDate;
	@Column (name = "IS_DELETE")
	private String  isDelete = "N";
}
