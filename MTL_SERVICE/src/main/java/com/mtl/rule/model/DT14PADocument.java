package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT14_PA_DOCUMENT")
public class DT14PADocument {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_PA_DOCUMENT")
    @SequenceGenerator(sequenceName = "SEQ_DT14_PA_DOCUMENT", allocationSize = 1, name = "SEQ_DT14_PA_DOCUMENT")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "SUM_INSURED_MIN")
	private Integer sumInsuredMin;
	@Column(name = "SUM_INSURED_MAX")
	private Long sumInsuredMax;
	@Column(name = "ADD_MIB_CLAIM_LIST")
	private String addMibClaimList;
	@Column(name = "DATA_C")
	private String dataC;
	@Column(name = "DATA_D")
	private String dataD;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
