package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_RISK_DIAGNOSIS_PLAN_TYPE")
@Getter
@Setter
public class DT03RiskDiagnosisPlanType {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_RISK_DIAG_PLAN_TYPE")
	@SequenceGenerator(sequenceName = "SEQ_DT03_RISK_DIAG_PLAN_TYPE", allocationSize = 1, name = "SEQ_DT03_RISK_DIAG_PLAN_TYPE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "BRANCH")
	private String branch;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "LOWER_AGE")
	private String lowerAge;
	@Column(name = "HIGHER_AGE")
	private String higherAge;
	@Column(name = "PROJECT_TYPE")
	private String projectType;
	@Column(name = "AMOUNT")
	private String amount;
	@Column(name = "RESULT")
	private String result;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}

