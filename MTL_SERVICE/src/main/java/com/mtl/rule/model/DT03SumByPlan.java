package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_SUM_BY_PLAN")
@Getter
@Setter
public class DT03SumByPlan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_SUM_BY_PLAN")
	@SequenceGenerator(sequenceName = "SEQ_DT03_SUM_BY_PLAN", allocationSize = 1, name = "SEQ_DT03_SUM_BY_PLAN")

	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "MIN_SUMINSURED")
	private Long minSuminsured;
	@Column(name = "MAX_SUMINSURED")
	private Long maxSuminsured;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
