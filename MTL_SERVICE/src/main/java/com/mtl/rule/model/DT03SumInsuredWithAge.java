package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_SUM_INSURED_WITH_AGE")
@Getter
@Setter
public class DT03SumInsuredWithAge {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_SUM_INSURED_WITH_AGE")
	@SequenceGenerator(sequenceName = "SEQ_DT03_SUM_INSURED_WITH_AGE", allocationSize = 1, name = "SEQ_DT03_SUM_INSURED_WITH_AGE")
	
	@Column(name = "ID")
	private String id;
	@Column(name = "MAIN_PLAN")
	private String mainPlan;
	@Column(name = "AGE")
	private String age;
	@Column(name = "MAIN_SUM_INSURED")
	private String mainSumInsured;
	@Column(name = "HISTORY_SUM_INSURED")
	private String historySumInsured;

	@Column(name = "MESSAGE_CODE")
	private String messageCode;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}
