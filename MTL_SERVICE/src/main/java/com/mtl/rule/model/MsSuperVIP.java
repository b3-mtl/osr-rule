package com.mtl.rule.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MS_SUPER_VIP")
public class MsSuperVIP {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MS_SUPER_VIP")
    @SequenceGenerator(sequenceName = "SEQ_MS_SUPER_VIP", allocationSize = 1, name = "SEQ_MS_SUPER_VIP")
	

	@Column (name = "ID")
	private Long id;
	@Column (name = "CSCLI")
	private String cscli;
	@Column (name = "CSLVST")
	private String cslvst;
	@Column (name = "CSLVPM")
	private String cslvpm;
	@Column (name = "CSUSRU")
	private String csuru;
	@Column (name = "CSDATU")
	private String csdatu;
	@Column (name = "CSTIMU")
	private String cstimu;
	@Column (name = "CSUSRC")
	private String csusrc;
	@Column (name = "CSDATC")
	private String csdatc;
	@Column (name = "CSTIMC")
	private String cstimc;
	@Column (name = "CSFIL1")
	private String csfil1;
	@Column (name = "CSFIL2")
	private String csfil2;
	@Column (name = "CSFIL3")
	private String csfil3;
	@Column (name = "CSFIL4")
	private String csfil4;
	

}