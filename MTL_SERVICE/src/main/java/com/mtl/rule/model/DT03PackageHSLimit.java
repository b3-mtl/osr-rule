package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_PACKAGE_HS_LIMIT")
@Getter
@Setter
public class DT03PackageHSLimit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PACKAGE_HS_LIMIT")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PACKAGE_HS_LIMIT", allocationSize = 1, name = "SEQ_DT03_PACKAGE_HS_LIMIT")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "A_PLAN_CODE")
	private String aPlanCode;
	@Column(name = "B_RIDER")
	private String bRider;
	@Column(name = "C_PROJECT_TYPE")
	private String cProjectType;
	@Column(name = "D_PLAN_HISTORY")
	private String dPlanHistory;
	@Column(name = "D_PLAN_BRACH")
	private String dPlanBrach;
	
	@Column(name = "D_BRANCH")
	private String dBranch;
	@Column(name = "D_AMOUNT")
	private String dAmount;
	
	@Column(name = "MESSAGE_CODE")
	private String MessageCode;

	@Column(name = "E_MESSAGE_DESC")
	private String MessageDesc;

	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}

