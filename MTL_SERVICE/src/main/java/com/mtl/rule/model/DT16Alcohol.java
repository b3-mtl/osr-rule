package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT16_ALCOHOL")
public class DT16Alcohol {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT16_ALCOHOL")
    @SequenceGenerator(sequenceName = "SEQ_DT16_ALCOHOL", allocationSize = 1, name = "SEQ_DT16_ALCOHOL")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "ALCOHOL")
	private String aclcohol;
	@Column(name = "SEX")
	private String sex;
	@Column(name = "ALCOHOL_TYPE")
	private String alcoholType;
	@Column(name = "RATE")
	private String rate;
	@Column(name = "E")
	private String e;
	@Column(name = "UNDERWRITE")
	private String underwrite;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
