package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT13_NONE_INCOME")
public class DT13NoneIncome {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT13_NONE_INCOME")
    @SequenceGenerator(sequenceName = "SEQ_DT13_NONE_INCOME", allocationSize = 1, name = "SEQ_DT13_NONE_INCOME")

	
	@Column(name = "ID")
	private Long id;
	@Column(name = "MIN_SUM_INS")
	private Long minSumIns;
	@Column(name = "MAX_SUM_INS")
	private Long maxSumIns;
	@Column(name = "MESSAGE_DESC")
	private String messageDesc;
	@Column(name = "OCCUPATION_CODE")
	private String occupationCode;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "CREATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
	
//	public String getMessageCode() {
//		return messageCode == null ? null:messageCode.trim();
//	}

}