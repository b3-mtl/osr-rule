package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_CHANNEL_SUM_INSURED")
@Getter
@Setter
public class DT03ChannelSumInsured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_CHANNEL_SUM_INSURED")
	@SequenceGenerator(sequenceName = "SEQ_DT03_CHANNEL_SUM_INSURED", allocationSize = 1, name = "SEQ_DT03_CHANNEL_SUM_INSURED")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "CHANNEL")
	private String channel;
	@Column(name = "REFER_UNDERWRITER")
	private String referUnderwriter;

	@Column(name = "AMOUNT")
	private String amount;
	
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
}