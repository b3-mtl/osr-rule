package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_VERIFY_NATION_RIDER")
public class DT11VerifyNationaRider {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_VERIFY_NATION_RIDER")
    @SequenceGenerator(sequenceName = "SEQ_DT11_VERIFY_NATION_RIDER", allocationSize = 1, name = "SEQ_DT11_VERIFY_NATION_RIDER")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "CHECK_HAVE_RIDER")
	private String checkHaveRider;
	@Column (name = "NATIONALITY")
	private String nationality;
	@Column (name = "PLAN_CODE")
	private String planCode;
	@Column (name = "NOT_PLAN_CODE")
	private String notPlanCode;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
