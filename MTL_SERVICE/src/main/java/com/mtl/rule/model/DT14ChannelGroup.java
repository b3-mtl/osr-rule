package com.mtl.rule.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT14_CHANNEL_GROUP")
public class DT14ChannelGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2039863182140780255L;
	
	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "SUM_INSURE_MIN")
	private Integer sumInsuredMin;
	@Column(name = "SUM_INSURE_MAX")
	private Long sumInsuredMax;
	@Column(name = "CLAIM_LIST")
	private String claimList;
	@Column(name = "CHANNEL_GROUP")
	private String channelGroup;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
