package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MS_MESSAGE_MAPPING")
public class MessageMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MS_MESSAGE_MAPPING")
    @SequenceGenerator(sequenceName = "SEQ_MS_MESSAGE_MAPPING", allocationSize = 1, name = "SEQ_MS_MESSAGE_MAPPING")

	@Column(name = "ID")
	private Long id;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CHANNEL_CODE")
	private String channelCode;
	@Column(name = "RULE_CODE")
	private Long ruleCode;
	@Column(name = "MESSAGE_TH")
	private String messageTH;
	@Column(name = "MESSAGE_EN")
	private String messageEN;
	@Column(name = "MESSAGE_LEVEL")
	private String messageLevel;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "CREATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
	@Column(name = "MESSAGE_STATUS")
	private String messageStatus;
	
	
}
