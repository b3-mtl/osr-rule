package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_DISORER_AND_ACCIDENT")
@Getter
@Setter
public class Dt03DisorderAndAccident {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_DISORER_AND_ACCIDENT")
	@SequenceGenerator(sequenceName = "SEQ_DT03_DISORER_AND_ACCIDENT", allocationSize = 1, name = "SEQ_DT03_DISORER_AND_ACCIDENT")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_TYPE_A")
	private String planTypeA;
	@Column(name = "PLAN_TYPE_B")
	private String planTypeB;
	

	@Column(name = "SUM_INSURED_A")
	private String sumInsuredA;
	@Column(name = "SUM_INSURED_B")
	private String sumInsuredB;
	
	@Column(name = "LIMIT")
	private String limit;
	
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

	
}
