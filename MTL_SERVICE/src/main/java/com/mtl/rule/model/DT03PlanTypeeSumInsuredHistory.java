package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_PLAN_TYPE_SUM_INSURED_HIS")
@Getter
@Setter
public class DT03PlanTypeeSumInsuredHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS", allocationSize = 1, name = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS")
	
	@Column(name = "ID")
	private String id;
	@Column(name = "SYSTEM_NAME")
	private String systemName;
	@Column(name = "ACCIDENT")
	private String accident;
	@Column(name = "SUM_INSURED")
	private String sumInsured;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "PLAN_SYSTEM")
	private String planSystem;
	@Column(name = "SUM_PLAN_TYPE")
	private String sumPlanType;	
	@Column(name = "SUM_SYSTEM_NAME")
	private String sumSystemName;
	@Column(name = "SUM_LIMIT")
	private String sumLimit;
	@Column(name = "MULTI_PLANTYPE")
	private String MultiPlanType;
	@Column(name = "MULTI_SYSTEMNAME")
	private String MultiSystemName;	
	@Column(name = "MULTI_SYSTEM_NAME_LIMIT")
	private String multiSystemNameLimit;
	@Column(name = "MESSAGE_CODE")
	private String MessageCode	;
	@Column(name = "MESSAGE_DESC")
	private String MessageDesc;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}

