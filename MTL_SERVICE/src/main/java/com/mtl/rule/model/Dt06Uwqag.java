package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT06_UWQAG")
public class Dt06Uwqag {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT06_UWQAG")
    @SequenceGenerator(sequenceName = "SEQ_DT06_UWQAG", allocationSize = 1, name = "SEQ_DT06_UWQAG")

	@Column(name = "ID")
	private Long id;
	@Column(name = "AGENT_QUALIFICATION")
	private String agentQualification;
	@Column(name = "AGENT_QUALIFICATION_TYP")
	private String agentQualificationTyp;
	@Column(name = "CHANNEL_CODE")
	private String channelCode;
	@Column(name = "CHANNEL_TYPE")
	private String channelType;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "IS_DELETE")
	private String isDelete;
	@Column(name = "MEDICAL_LIMIT_TYPE")
	private String medicalLimitType;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "DATE_BEFORE")
	private String dateBefore;
	@Column(name = "DATE_AFTER")
	private String dateAfter;
	
}
