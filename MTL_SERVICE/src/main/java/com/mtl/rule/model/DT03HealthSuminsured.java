package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_HEALTH_SUM")
@Getter
@Setter
public class DT03HealthSuminsured {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_HEALTH_SUM")
	@SequenceGenerator(sequenceName = "SEQ_DT03_HEALTH_SUM", allocationSize = 1, name = "SEQ_DT03_HEALTH_SUM")

	@Column(name = "AGENT_GROUP")
	private String agentGroup;
	@Column(name = "MIN_AGE_MONTH")
	private String minAgeMonth;
	@Column(name = "MAX_AGE_MONTH")
	private String maxAgeMonth;
	@Column(name = "MIN_AGE_YEAR")
	private Long minAgeYear;
	@Column(name = "MAX_AGE")
	private Long maxAge;
	@Column(name = "MIN_SUMINSURED")
	private Long minSuminsured;
	@Column(name = "MAX_SUMINSURED")
	private Long maxSuminsured;
	@Column(name = "MAX_SUMINSURED_DISTR")
	private String maxSuminsuredDistr;
	@Column(name = "MAX_SUMINSURED_HEALTH")
	private String maxSuminsuredHealth;
	@Column(name = "MAX_SUMINSURED_FIXED")
	private String maxSuminsuredFixed;
	@Column(name = "MAX_SUMINSURED_FIXED_MIX")
	private String maxSuminsuredFixedMix;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
