package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT14_EXCEPTION_APP_TYPE")
public class DT14ExceptionAppType {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT14_EXCEPTION_APP_TYPE")
    @SequenceGenerator(sequenceName = "SEQ_DT14_EXCEPTION_APP_TYPE", allocationSize = 1, name = "SEQ_DT14_EXCEPTION_APP_TYPE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "FULL_TELE")
	private String fullTele;
	@Column(name = "SIO")
	private String sio;
	@Column(name = "GIO")
	private String gio;
	@Column(name = "PA")
	private String pa;
	@Column(name = "DM")
	private String dm;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
	
}
