package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT16_OCCUPATION")
public class DT16Occupation {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT16_OCCUPATION")
    @SequenceGenerator(sequenceName = "SEQ_DT16_OCCUPATION", allocationSize = 1, name = "SEQ_DT16_OCCUPATION")
	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "OCCUPATION_CODE")
	private String occupationCode;
	@Column(name = "PB")
	private String pb;
	@Column(name = "REFER_UNDERWRITER")
	private String referUnderwriter;
	@Column(name = "OCCUPATION_CODE_AMLO")
	private String occupationCodeAMLO;
	@Column(name = "RULE_MESSAGE_CODE_DESC")
	private String ruleMessageCodeDesc;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
