package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT16_VERIFY_NATIONALITY")
@Getter
@Setter
public class DT16VerifyNationality {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT16_VERIFY_NATIONALITY")
	@SequenceGenerator(sequenceName = "SEQ_DT16_VERIFY_NATIONALITY", allocationSize = 1, name = "SEQ_DT16_VERIFY_NATIONALITY")

	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "HEALTH_QUESTION")
	private String healthQuestion;
	@Column(name = "NATIONALITY")
	private String nationality;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
