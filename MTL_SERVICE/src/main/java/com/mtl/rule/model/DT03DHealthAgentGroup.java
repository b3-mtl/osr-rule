package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_D_HEALTH_AGENTGROUP")
@Getter
@Setter
public class DT03DHealthAgentGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_D_HEALTH_AGENTGROUP")
	@SequenceGenerator(sequenceName = "SEQ_DT03_D_HEALTH_AGENTGROUP", allocationSize = 1, name = "SEQ_DT03_D_HEALTH_AGENTGROUP")
	
		
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "AGENT_GROUP")
	private String agentGroup;
	@Column(name = "SUMINSURED")
	private String SUMINSURED;
	

	@Column(name = "PLAN_CODE")
	private String planCode;

	
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
}
