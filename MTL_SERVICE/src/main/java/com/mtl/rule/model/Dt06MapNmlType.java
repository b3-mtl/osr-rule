package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT06_MAP_NML_TYPE")
public class Dt06MapNmlType {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT06_MAP_NML_TYPE")
    @SequenceGenerator(sequenceName = "SEQ_DT06_MAP_NML_TYPE", allocationSize = 1, name = "SEQ_DT06_MAP_NML_TYPE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "BRANCH")
	private String bRANCH;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "DOCUMENT_TYPE")
	private String documentType;
	@Column(name = "IS_DELETE")
	private String isDelete;
	@Column(name = "TC_TYPE")
	private String tcType;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

}
