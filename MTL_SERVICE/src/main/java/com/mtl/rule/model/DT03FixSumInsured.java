package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_FIX_SUM_INSURED")
@Getter
@Setter
public class DT03FixSumInsured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_FIX_SUM_INSURED")
	@SequenceGenerator(sequenceName = "SEQ_DT03_FIX_SUM_INSURED", allocationSize = 1, name = "SEQ_DT03_FIX_SUM_INSURED")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_MAIN")
	private String planMain;
	@Column(name = "PLAN_RIDER")
	private String planRider;
	@Column(name = "D_PLAN")
	private String dPlan;
	
	@Column(name = "D_AMOUNT_A")
	private String dAmountA;
	@Column(name = "D_AMOUNT_B")
	private String dAmountB;
	
	@Column(name = "E_PLAN")
	private String ePlan;
	@Column(name = "E_AMOUNT_E")
	private String eAmountE;
	@Column(name = "F_PLAN")
	private String fPlan;
	
	@Column(name = "F_AMOUNT_E")
	private String fAmountE;

	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "MESSAGE_DESC")
	private String messageDesc;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}

