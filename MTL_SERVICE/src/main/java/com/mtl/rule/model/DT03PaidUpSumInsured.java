package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_PAID_UP_SUM_INSURED")
@Getter
@Setter
public class DT03PaidUpSumInsured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PAID_UP_SUM_INSURED")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PAID_UP_SUM_INSURED", allocationSize = 1, name = "SEQ_DT03_PAID_UP_SUM_INSURED")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "CHECK_PLAN_LAST_YEAR")
	private String checkPlanLastYear;
	@Column(name = "PLAN_CODE_LAST_YEAR")
	private String planCodeLastYear;
	@Column(name = "TIMES")
	private String times;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}



