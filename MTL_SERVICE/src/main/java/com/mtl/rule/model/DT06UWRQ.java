package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT06_UWRQ")
@Access(AccessType.FIELD)
public class DT06UWRQ {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT06_UWRQ")
    @SequenceGenerator(sequenceName = "SEQ_DT06_UWRQ", allocationSize = 1, name = "SEQ_DT06_UWRQ")

	@Column(name = "ID")
	private Long id;
	@Column(name = "NO")
	private Long no;
	@Column(name = "CLIENT_LOCATION_CODE")
	private String clientLocationCode;
	@Column(name = "AGENT_QUALIFICATION_UWRQ")
	private String agentQualificationUwrq;
	@Column(name = "MIN_AGE")
	private Long minAge;
	@Column(name = "MAX_AGE")
	private Long maxAge;
	@Column(name = "SUM_INSURED_MIN")
	private BigDecimal sumInsuredMin;
	@Column(name = "SUM_INSURED_MAX")
	private BigDecimal sumInsuredMax;
	@Column(name = "OTHERWISE")
	private String otherwise;
	@Column(name = "SEX_CODE")
	private String sexCode;
	@Column(name = "RESULT_UWRQ_CODE")
	private String resultUwrqCode;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "MESSAGE_TEXT")
	private String messageText;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "CREATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
	
	public String getMessageCode() {
		return messageCode == null ? null:messageCode.trim();
	}

}
