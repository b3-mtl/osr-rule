package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_VERIFY_NATIONALITY")
public class DT11VerifyNationality {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_VERIFY_NATIONALITY")
    @SequenceGenerator(sequenceName = "SEQ_DT11_VERIFY_NATIONALITY", allocationSize = 1, name = "SEQ_DT11_VERIFY_NATIONALITY")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "HEALTH_QUESTION")
	private String healthQuestion;
	@Column (name = "NATIONALITY")
	private String nationality;
	@Column (name = "NOT_NATIONALITY")
	private String notNationality;
	@Column (name = "LENGTH_ID_CARD")
	private String lengthIdCard;
	@Column (name = "INSURED")
	private String insured;
	@Column (name = "REFER_NATIONALITY_AMLO")
	private String referNationalityAMLO;
	@Column (name = "REFER_UNDERWRITER")
	private String referUnderwriter;
	@Column (name = "ADD_DOCUMENT")
	private String addDocument;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
