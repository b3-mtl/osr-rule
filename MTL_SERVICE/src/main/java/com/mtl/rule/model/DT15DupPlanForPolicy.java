package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT15_DUPLICATE_PLAN_FOR_POLICY")
public class DT15DupPlanForPolicy {

	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT15_DUP_PLAN_FOR_POLICY")
    @SequenceGenerator(sequenceName = "SEQ_DT15_DUP_PLAN_FOR_POLICY", allocationSize = 1, name = "SEQ_DT15_DUP_PLAN_FOR_POLICY")
	
	@Column (name = "ID")
	private String id;
	
	@Column (name = "PLAN_TYPE")
	private String planType;
	
	@Column (name = "POLICY")
	private String policy;
	
	@Column (name = "COUNT_PLAN")
	private String countPlan;
	
	@Column (name = "COUNT_POLICY")
	private String countPolicy;
	
	@Column (name = "COUNT_RESULT")
	private String countResult;
	
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column (name = "MESSAGE_DESC")
	private String messageDesc;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	
	
	
}
