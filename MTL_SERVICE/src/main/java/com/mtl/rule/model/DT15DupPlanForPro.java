package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT15_DUP_PLAN_FOR_PRO")
public class DT15DupPlanForPro {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT15_DUP_PLAN_FOR_PRO")
    @SequenceGenerator(sequenceName = "SEQ_DT15_DUP_PLAN_FOR_PRO", allocationSize = 1, name = "SEQ_DT15_DUP_PLAN_FOR_PRO")
	
	
	@Column (name = "ID")
	private String id;
	
	@Column (name = "PLAN_CODE")
	private String planCode;
	
	
	@Column (name = "SERVICE_BRANCH")
	private String serviceBranch;
	
	
	@Column (name = "CHANNEL_CODE")
	private String channelCode;
	
	@Column (name = "COUNT_PLAN_CODE")
	private String countPlanCode;
	
	@Column (name = "COVERAGE_LIST")
	private String coverageList;
	

	@Column (name = "PAFAMILY")
	private String pafamily;
	
	@Column (name = "PLAN_CODE_DURATION")
	private String planCodeDuration;
	
	@Column (name = "YEAR_DURATION")
	private String yearDuration;
	
	@Column (name = "DAY_DURATION")
	private String dayDuration;
	
	@Column (name = "REQUEST_DATE")
	private String requestDate;
	
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	
	@Column (name = "MESSAGE_DESC")
	private String messageDesc;

	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	
	
	
}
