package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_RIDER_PLAN_SUM_INSURED")
@Getter
@Setter
public class DT03RiderPlanSumInsured {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_RIDER_PLAN_SUM_IN")
	@SequenceGenerator(sequenceName = "SEQ_DT03_RIDER_PLAN_SUM_IN", allocationSize = 1, name = "SEQ_DT03_RIDER_PLAN_SUM_IN")
	
	@Column(name = "ID")
	private String id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "PROJECT_TYPE")
	private String projectType;
	@Column(name = "MAIN_PLAN")
	private String mainPlan;
	@Column(name = "CHANNEL")
	private String channel;
	@Column(name = "AGENT_CODE")
	private String agentCode;
	@Column(name = "SUM_INSURED_RATE_MORE_THAN")
	private String sumInsuredRateMoreThan;
	@Column(name = "SUM_INSURED_RATE")
	private String sumInsuredRate;
	@Column(name = "ROUNDING_UP")
	private String roundingUp;

	@Column(name = "MESSAGE_CODE")
	private String messageCode;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}


