package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_PLAN_CODE_SUM_INSURED_HIS")
@Getter
@Setter
public class DT03PlanCodeSumInsuredHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS", allocationSize = 1, name = "SEQ_DT03_PLAN_TYPE_SUMIN_HIS")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "BRANCH")
	private String branch;
	@Column(name = "SUM_BY_PLAN_CODE")
	private String sumByPlanCode;
	@Column(name = "LIMIT")
	private String limit;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "MESSAGE_DESC")
	private String MessageDesc;
	
	@Column(name = "MULTI_LIMIT")
	private String multiLimit;
	
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}
