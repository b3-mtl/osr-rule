package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_COF_OCCUPATION_MAIN")
public class DT11COFOccupationMAIN {

	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_COF_OCCUPATION_MAIN")
    @SequenceGenerator(sequenceName = "SEQ_DT11_COF_OCCUPATION_MAIN", allocationSize = 1, name = "SEQ_DT11_COF_OCCUPATION_MAIN")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "PLAN_CODE")
	private String planCode;
	@Column (name = "ISSUE_DATA_BEGIN")
	private String issueDataBegin;
	@Column (name = "ISSUE_DATA_END")
	private String issueDataEnd;
	@Column (name = "SINGLE")
	private String single;
	@Column (name = "YEAR")
	private String year;
	@Column (name = "SEMESTER")
	private BigDecimal semester;
	@Column (name = "QUARTER")
	private BigDecimal quarter;
	@Column (name = "MONTHLY")
	private BigDecimal monthly;

	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}