package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "DT06_MAP_MEDICAL_DISEASE")
public class DT06MapMedicalDisease  {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT06_MAP_MEDICAL_DISEASE")
    @SequenceGenerator(sequenceName = "SEQ_DT06_MAP_MEDICAL_DISEASE", allocationSize = 1, name = "SEQ_DT06_MAP_MEDICAL_DISEASE")

	@Column(name = "ID")
	private Long id;
	@Column(name = "CODE")
	private String code;
	@Column(name = "PRIORITY")
	private Long priority;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DOCUMENT_NAME_TH")
	private String documentNameTh;
	@Column(name = "DOCUMENT_NAME_EN")
	private String documentNameEn;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "CREATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
	
}
