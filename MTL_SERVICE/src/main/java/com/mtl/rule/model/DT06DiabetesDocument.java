package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name = "DT06_DIABETES_DOCUMENT")
public class DT06DiabetesDocument  {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT06_DIABETES_DOCUMENT")
    @SequenceGenerator(sequenceName = "SEQ_DT06_DIABETES_DOCUMENT", allocationSize = 1, name = "SEQ_DT06_DIABETES_DOCUMENT")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "MIN_SUM_INSURED")
	private String minSumInsured;
	@Column(name = "MAX_SUM_INSURED")
	private String maxSumInsured;
	@Column(name = "MIN_AGE")
	private String minAge;
	@Column(name = "MAX_AGE")
	private String maxAge;
	@Column(name = "DOCUMENT_CODE")
	private String documentCode;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "CREATED_DATE")
	private Date updatedDate;
	@Column(name = "IS_DELETE")
	private String isDelete = "N";
	
}
