package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_VERIFY_NATION_WITH_PLAN")
public class DT11VerifyNationWithPlan {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_VERIFY_NATION_WITH_P")
    @SequenceGenerator(sequenceName = "SEQ_DT11_VERIFY_NATION_WITH_P", allocationSize = 1, name = "SEQ_DT11_VERIFY_NATION_WITH_P")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "PLAN_CODE")
	private String planCode;
	@Column (name = "FOREIGNE_ID_CARD")
	private String foreigneIDCard;
	@Column (name = "NATIONALITY")
	private String nationality;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "E")
	private String e;
	
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
