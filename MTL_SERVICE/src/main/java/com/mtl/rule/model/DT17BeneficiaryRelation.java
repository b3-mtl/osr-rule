package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT17_BENEFICIARY_RELATION")
@Getter
@Setter
public class DT17BeneficiaryRelation {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT17_BENEFICIARY_RELATION")
	@SequenceGenerator(sequenceName = "SEQ_DT17_BENEFICIARY_RELATION", allocationSize = 1, name = "SEQ_DT17_BENEFICIARY_RELATION")

	@Column(name = "RELATION_CODE")
	private Long relationCode;
	@Column(name = "RELATION_TH")
	private String relationTh;
	@Column(name = "RELATION_EN")
	private String relationEn;
	@Column(name = "AUTO_PASS")
	private String autoPass;
	@Column(name = "PASS_WITH_CONDITION")
	private String passWithCondition;
	@Column(name = "DOCUMENT")
	private String document;
	@Column(name = "FLAG_LASTNAME")
	private String flagLastname;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
}
