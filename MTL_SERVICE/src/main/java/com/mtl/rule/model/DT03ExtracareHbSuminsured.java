package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;



@Entity
@Table(name = "DT03_EXTRACARE_HB_SUMINSURED")
@Getter
@Setter
public class DT03ExtracareHbSuminsured {


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_EXTRACARE_HB_SUMIN")
	@SequenceGenerator(sequenceName = "SEQ_DT03_EXTRACARE_HB_SUMIN", allocationSize = 1, name = "SEQ_DT03_EXTRACARE_HB_SUMIN")
	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "SERVICE_BRANCH")
	private String serviceBranch;
	
	@Column(name = "PLAN_CODE")
	private String planCode;
	
	@Column(name = "RIDER")
	private String rider;
	
	@Column(name = "HB_SUMINSURED")
	private String hbSuminsured;
	
	@Column(name = "HB_SUMINSURED_LIMIT")
	private String hbSuminsuredLimit;
	
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "MESSAGE_DESC")
	private String messageDesc;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}
