package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_ACCIDENT_FOR_TAKAFUL_NC")
@Getter
@Setter
public class DT03AccidentForTakafulNc {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_ACC_FOR_TAKAFUL_NC")
	@SequenceGenerator(sequenceName = "SEQ_DT03_ACC_FOR_TAKAFUL_NC", allocationSize = 1, name = "SEQ_DT03_ACC_FOR_TAKAFUL_NC")

	@Column(name = "ID")
	private String ID;
	@Column(name = "PROJECT_TYPE")
	private String projectType;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "PLANTYPE_IN_LIST")
	private String plantypeInList;
	@Column(name = "AAAB")
	private String aaab;
	@Column(name = "A1A2A3")
	private String a1a2a3;
		
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
}
