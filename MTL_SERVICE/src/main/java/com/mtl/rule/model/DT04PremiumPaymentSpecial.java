package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT04_PREMIUM_PAYMMENT_SPECIAL")
@Getter
@Setter
public class DT04PremiumPaymentSpecial {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT04_PREMIUM_PAYMMENT_S")
	@SequenceGenerator(sequenceName = "SEQ_DT04_PREMIUM_PAYMMENT_S", allocationSize = 1, name = "SEQ_DT04_PREMIUM_PAYMMENT_S")

	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "PREMIUM_PAYMENT")
	private String premiumPayment;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
	

}
