package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_RISK_MOBILE_NUMBER")
public class DT11RiskMobileNumber {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_RISK_MOBILE_NUMBER")
    @SequenceGenerator(sequenceName = "SEQ_DT11_RISK_MOBILE_NUMBER", allocationSize = 1, name = "SEQ_DT11_RISK_MOBILE_NUMBER")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "MOBILE_NUMBER")
	private String mobileNumber;
	@Column (name = "MESSAGE_CODE")
	private String messageCode;
	@Column (name = "LOG")
	private String log;
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
