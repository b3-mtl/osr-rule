package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_HEALTH_SUM_INCLUDE")
@Getter
@Setter
public class DT03HealthSuminsuredInclude {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_HEALTH_SUM_INCLUDE")
	@SequenceGenerator(sequenceName = "SEQ_DT03_HEALTH_SUM_INCLUDE", allocationSize = 1, name = "SEQ_DT03_HEALTH_SUM_INCLUDE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_TYPE_GROUP")
	private String planTypeGroup;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "INSURED")
	private String insured;
	@Column(name = "SUMINSURED_INCLUDE")
	private Long suminsuredInclude;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
