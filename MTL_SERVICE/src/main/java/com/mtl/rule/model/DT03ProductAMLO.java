package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_PRODUCT_AMLO")
@Getter
@Setter
public class DT03ProductAMLO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_PRODUCT_AMLO")
	@SequenceGenerator(sequenceName = "SEQ_DT03_PRODUCT_AMLO", allocationSize = 1, name = "SEQ_DT03_PRODUCT_AMLO")
	

	@Column(name = "ID")
	private String id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "AMOUNT")
	private String amount;
	@Column(name = "SET_SUM_INSURED_PRODUCT_AMLO")
	private String setSumInsuredProductAmlo;
	@Column(name = "REFER_UNDERWRITE")
	private String referUnderwrite;

	@Column(name = "MESSAGE_CODE")
	private String MessageCode	;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}
