package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT21_VERIFY_PERSONAL_KYC")
@Getter
@Setter
public class DT21KYC {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT21_VERIFY_PERSONAL_KYC")
	@SequenceGenerator(sequenceName = "SEQ_DT21_VERIFY_PERSONAL_KYC", allocationSize = 1, name = "SEQ_DT21_VERIFY_PERSONAL_KYC")

	@Column(name = "MIN_AGE")
	private Long minAge;
	@Column(name = "MAX_AGE")
	private Long maxAge;
	@Column(name = "NATIONALITY")
	private String nationality;
	@Column(name = "FIRSTNAME_EN")
	private String firstnameEn;
	@Column(name = "LASTNAME_EN")
	private String lastnameEn;
	@Column(name = "REFER")
	private String refer;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
	@Column(name = "ID")
	private Long id;
}
