package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClaimHistoryList {

	private List<DT16ClaimHistory> inDb;
	private List<String> notIndb;
}
