package com.mtl.rule.model;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SumInsure {

	private String sumType;
	private BigDecimal sumInsure;
}
