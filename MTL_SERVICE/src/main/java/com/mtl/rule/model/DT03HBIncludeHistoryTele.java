package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DT03_HB_INCLUDE_HISTORY_TELE")
@Getter
@Setter
public class DT03HBIncludeHistoryTele {


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_HB_INCLUDE_HIS_TELE")
	@SequenceGenerator(sequenceName = "SEQ_DT03_HB_INCLUDE_HIS_TELE", allocationSize = 1, name = "SEQ_DT03_HB_INCLUDE_HIS_TELE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "B")
	private String b;
	@Column(name = "SET_SUM_INSURED_PROD_ALMO")
	private String setSumInsuredProdAlmo;
	
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	
	
}
