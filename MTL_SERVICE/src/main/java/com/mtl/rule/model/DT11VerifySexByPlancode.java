package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_VERIFY_SEX_BY_PLANCODE")
public class DT11VerifySexByPlancode {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_VERIFY_SEX_PLANCODE")
    @SequenceGenerator(sequenceName = "SEQ_DT11_VERIFY_SEX_PLANCODE", allocationSize = 1, name = "SEQ_DT11_VERIFY_SEX_PLANCODE")
	
	@Column (name = "ID")
	private Long id;
	@Column (name = "PLAN_CODE")
	private String planCode;
	@Column (name = "SEX_IS_NOT")
	private String sexIsNot;
	@Column (name = "RESULT")
	private String result;
	@Column (name = "BMI_PROBLEM")
	private String bmiProblem;
	
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	@Column (name = "IS_DELETE")
	private String isDelete = "N";
}
