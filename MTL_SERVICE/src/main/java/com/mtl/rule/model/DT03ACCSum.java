package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "DT03_ACC_SUM")
@Getter
@Setter
public class DT03ACCSum {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT03_ACC_SUM")
	@SequenceGenerator(sequenceName = "SEQ_DT03_ACC_SUM", allocationSize = 1, name = "SEQ_DT03_ACC_SUM")

	@Column(name = "PLAN_TYPE")
	private String planType;
	@Column(name = "MIN_AGE_MONTH")
	private String minAgeMonth;
	@Column(name = "MAX_AGE_MONTH")
	private String maxAgeMonth;
	@Column(name = "MIN_AGE")
	private Long minAge;
	@Column(name = "MAX_AGE")
	private Long maxAge;
	@Column(name = "LIMIT_SUMINSURED")
	private String limitSuminsured;
	@Column(name = "MAX_SUMINSURED")
	private String maxSuminsured;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
