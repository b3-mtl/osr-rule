package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "MS_PARAMETER")
@Getter
@Setter
public class MsParameter {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MS_PARAMETER")
	@SequenceGenerator(sequenceName = "SEQ_MS_PARAMETER", allocationSize = 1, name = "SEQ_MS_PARAMETER")

	@Column(name = "KEY")
	private String key;
	@Column(name = "VALUE")
	private String value;
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	

}
