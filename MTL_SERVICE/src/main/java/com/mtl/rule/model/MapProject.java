package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "MAP_PROJECT")
@Getter
@Setter
public class MapProject {


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MAP_PROJECT")
	@SequenceGenerator(sequenceName = "SEQ_MAP_PROJECT", allocationSize = 1, name = "SEQ_MAP_PROJECT")
	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "SERVICE_BRANCH")
	private String servicBranch;
	
	@Column(name = "MAP_PROJECT")
	private String mapProject;
	
	@Column(name = "CHANNAL")
	private String channal;
	
	@Column(name = "AGENT_CODE")
	private String agentCode;
	
	@Column(name = "PLAN_CODE")
	private String planCode;
	
	@Column(name = "CHECKRIDER")
	private String checkrider;
	
	@Column(name = "RIDERPLAN")
	private String riderplan;
	
	@Column(name = "PLAN_TYPE")
	private String planType;
	
	@Column(name = "PLAN_CODE_LIST")
	private String planCodeList;
	
	@Column(name = "PROJECT_TYPE")
	private String projectType;
	
	@Column(name = "PROJECT_CODE")
	private String projectCode;
	
	@Column(name = "MESSAGE_CODE")
	private String MessageCode;

	@Column(name = "E_MESSAGE_DESC")
	private String MessageDesc;

	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}