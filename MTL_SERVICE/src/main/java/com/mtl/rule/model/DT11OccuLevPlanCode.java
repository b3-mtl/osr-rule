package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT11_OCCU_PLAN_CODE")
public class DT11OccuLevPlanCode {

	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT11_OCCU_LEV_PLAN_CODE")
    @SequenceGenerator(sequenceName = "SEQ_DT11_OCCU_LEV_PLAN_CODE", allocationSize = 1, name = "SEQ_DT11_OCCU_LEV_PLAN_CODE")
	
	@Column(name = "ID")
	private Long id;
	@Column(name = "PLAN_CODE")
	private String planCode;
	@Column(name = "OCCUPATION_LEVEL")
	private String occupationLevel;
	@Column (name = "CREATED_BY")
	private String createdBy;
	@Column (name = "CREATED_DATE")
	private Date createdDate;
	@Column (name = "UPDATED_BY")
	private String updatedBy;
	@Column (name = "UPDATED_DATE")
	private Date updatedDate;
	
}
