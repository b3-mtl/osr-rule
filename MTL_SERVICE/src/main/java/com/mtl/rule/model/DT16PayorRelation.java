package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DT16_PAYOR_RELATION")
public class DT16PayorRelation {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DT16_PAYOR_RELATION")
    @SequenceGenerator(sequenceName = "SEQ_DT16_PAYOR_RELATION", allocationSize = 1, name = "SEQ_DT16_PAYOR_RELATION")
	
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "RELATIONSHIP")
	private String relationship;
	@Column(name = "DOCUMENT")
	private String document;
	@Column(name = "MESSAGE_CODE")
	private String messageCode;
	@Column(name = "MESSAGE_DESC")
	private String messageDesc;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	@Column(name = "IS_DELETE")
	private String isDelete;
}
