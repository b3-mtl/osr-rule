package com.mtl.rule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MS_AGEN_INFO")
public class MsAgentInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8557982564198359938L;

	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "AG_CODE_NUMBER")
	private String agCodeNumber;
	
	@Column(name = "AGENT_QUALIFICATION")
	private String agentQualification;
	
	@Column(name = "AG_BLACKLIST_RSN")
	private String agBlacklistRsn;
	
	@Column(name = "AGENT_AGENCY")
	private String agentAgency;
	
	@Column(name = "AGENT_STAT")
	private String agentStat;
	
	@Column(name = "AGENT_START_DATE")
	private String agentStartDate;
	
	@Column(name = "AGENT_REGION")
	private String agentRegion;
	
	@Column(name = "AG_CODE_COMPANY")
	private String agCodeCompany;
	
	@Column(name = "AG_CODE_CURRENCY")
	private String agCodeCurrency;
	
	@Column(name = "AG_DIST_CHANEL_CODE")
	private String agDistChanelCode;
	
	@Column(name = "AG_BANK_ACCOUNT")
	private String agBankAccount;

}
