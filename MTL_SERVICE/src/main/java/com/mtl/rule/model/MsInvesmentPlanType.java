package com.mtl.rule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "MS_INVESTMENT_PLAN_TYPE")
public class MsInvesmentPlanType {


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_INVESTMENT_PLAN_TYPE")
    @SequenceGenerator(sequenceName = "SEQ_INVESTMENT_PLAN_TYPE", allocationSize = 1, name = "SEQ_INVESTMENT_PLAN_TYPE")

	@Column(name = "ID")
	private Long id;
	
	@Column(name = "PLAN_CODE")
	private String planCode;
	
	@Column(name = "INVESTMENT_TYPE")
	private String investmentType;
	
	@Column(name = "SYSTEM_NAME")
	private String systemName;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@Column(name = "CREATED_BY")
	private String createdBy;
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
}
