package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16Alcohol;



@Repository
public class DT16AlcoholDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT16Alcohol findAlcohol(String alcohol,String sex,String alcoholType) {
		DT16Alcohol dataRes = new DT16Alcohol();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_ALCOHOL WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(alcohol)) {
			sqlBuilder.append(" AND ALCOHOL LIKE ? ");
			params.add("%" + alcohol.replaceAll(" ", "%")+ "%");
		}
		
		if (StringUtils.isNotBlank(sex)) {
			sqlBuilder.append(" AND SEX LIKE ? ");
			params.add("%" + sex.replaceAll(" ", "%")+ "%");
		}
		
		if (StringUtils.isNotBlank(alcoholType)) {
			sqlBuilder.append(" AND ALCOHOL_TYPE LIKE ? ");
			params.add("%" + alcoholType.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	

	private RowMapper<DT16Alcohol> listRowmapper = new RowMapper<DT16Alcohol>() {
		@Override
		public DT16Alcohol mapRow(ResultSet rs, int arg1) throws SQLException {
			DT16Alcohol vo = new DT16Alcohol();
			vo.setId(rs.getLong("ID"));
			vo.setAclcohol(rs.getString("ALCOHOL"));
			vo.setAlcoholType(rs.getString("ALCOHOL_TYPE"));
			vo.setUnderwrite(rs.getString("UNDERWRITE"));
			vo.setSex(rs.getString("SEX"));
			vo.setRate(rs.getString("RATE"));
			vo.setE(rs.getString("E"));
			
			return vo;
		}
	};


}
