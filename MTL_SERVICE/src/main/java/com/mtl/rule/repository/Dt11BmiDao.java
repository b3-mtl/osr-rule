package com.mtl.rule.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11BMIBaby;
import com.mtl.rule.model.DT11BMIChildAndAdult;
import com.mtl.rule.model.DT11BMIDocument;
import com.mtl.rule.model.DT11VerifySexByPlancode;

@Repository
public class Dt11BmiDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT11BMIChildAndAdult> getAllBMIChildAndAdult() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_BMI_CHILD_AND_ADULT  ");
		List<DT11BMIChildAndAdult> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11BMIChildAndAdult.class));
		System.out.println(" Get All DT11_BMI_CHILD_AND_ADULT.. Size: " + ls.size());
		return rewriteDataBMIChildAndAdult(ls);
	}

	private HashMap<String, DT11BMIChildAndAdult> rewriteDataBMIChildAndAdult(List<DT11BMIChildAndAdult> ls) {
		HashMap<String, DT11BMIChildAndAdult> mapVal = new HashMap<String, DT11BMIChildAndAdult>();
		String key;
		for (DT11BMIChildAndAdult item : ls) {
			key = item.getSex() + "|" + item.getAge();
			mapVal.put(key, item);
		}
		return mapVal;
	}
	
	public HashMap<String, DT11VerifySexByPlancode> getVerifySexByPlancode() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_VERIFY_SEX_BY_PLANCODE ");
		List<DT11VerifySexByPlancode> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11VerifySexByPlancode.class));
		System.out.println(" Get All DT11_VERIFY_SEX_BY_PLANCODE .. Size: " + ls.size());
		return rewriteDataVerifySexByPlancode(ls);
	}

	private HashMap<String, DT11VerifySexByPlancode> rewriteDataVerifySexByPlancode(List<DT11VerifySexByPlancode> ls) {
		HashMap<String, DT11VerifySexByPlancode> mapVal = new HashMap<String, DT11VerifySexByPlancode>();
		for (DT11VerifySexByPlancode item : ls) {
			if(item.getPlanCode() != null) {
				List<String> listPlanCode = Arrays.asList(item.getPlanCode().split(","));
				listPlanCode.forEach((planCode) -> {
					mapVal.put(planCode, item);
				});
			}
		}
		return mapVal;
	}
	
	public HashMap<String, DT11BMIBaby> getDt11BMIBaby() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_BMI_BABY ");
		List<DT11BMIBaby> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11BMIBaby.class));
		System.out.println(" Get All DT11_BMI_BABY.. Size: " + ls.size());
		return rewriteDataBMIBaby(ls);
	}

	private HashMap<String, DT11BMIBaby> rewriteDataBMIBaby(List<DT11BMIBaby> ls) {
		HashMap<String, DT11BMIBaby> mapVal = new HashMap<String, DT11BMIBaby>();
		ls.parallelStream().forEach((item)->{
			String key = item.getSex() + "|" + item.getAgeInMonth();
			mapVal.put(key, item);
		});
		return mapVal;
	}
	
	public List<DT11BMIDocument> getDt11BMIDocument() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_BMI_DOCUMENT  ");
		List<DT11BMIDocument> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11BMIDocument.class));
		System.out.println(" Get All DT11_BMI_DOCUMENT.. Size: " + ls.size());
		return ls;
	}

	public DT11BMIChildAndAdult getDT11BMIChildAndAdult(String sex, String age, BigDecimal bmi) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		sql.append(" SELECT * FROM DT11_BMI_CHILD_AND_ADULT ");
		sql.append(" WHERE 1=1");
		if (StringUtils.isNotBlank(sex)) {
			sql.append(" AND SEX = ?");
			params.add(sex);
		}

		if (StringUtils.isNotBlank(age)) {
			sql.append(" AND AGE = ? ");
			params.add(age);
		}

		if (bmi != null) {
			sql.append(" AND ? BETWEEN BMI_MIN AND BMI_MAX ");
			params.add(bmi);
		}

		List<DT11BMIChildAndAdult> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11BMIChildAndAdult.class));

		return dt11Obj.get(0);
	}

	public DT11BMIBaby getDT11BMIBaby(String sex, BigDecimal ageInMonth, BigDecimal height, BigDecimal weight) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		sql.append(" SELECT * FROM DT11_BMI_BABY ");
		sql.append(" WHERE 1=1");
		if (StringUtils.isNotBlank(sex)) {
			sql.append(" AND SEX = ?");
			params.add(sex);
		}

		if (ageInMonth != null) {
			sql.append(" AND AGE_IN_MONTH = ? ");
			params.add(height);
		}

		List<DT11BMIBaby> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11BMIBaby.class));
		if (dt11Obj != null && dt11Obj.size() > 0) {
			return dt11Obj.get(0);
		} else {
			return null;
		}

	}
}
