package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT16ClaimHistory;

@Repository
public interface DT16ClaimHistoryRepository extends CrudRepository<DT16ClaimHistory, Long> {

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT16ClaimHistory> findAll();
}
