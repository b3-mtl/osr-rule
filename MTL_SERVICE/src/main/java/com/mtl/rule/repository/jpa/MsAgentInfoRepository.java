package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.MappingClientLocationCode;
import com.mtl.rule.model.MsAgentInfo;

@Repository
public interface MsAgentInfoRepository extends CrudRepository<MsAgentInfo, Long>{
	public List<MsAgentInfo> findAll();
}
