package com.mtl.rule.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.core.entity.MsPlanHeader;
import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11AgeSpecialPlanAtleast;
import com.mtl.rule.model.DT11AgeforSpecialPlan;
import com.mtl.rule.model.DT11VerifyNationUSA;
import com.mtl.rule.model.DT11VerifyNationWithPlan;
import com.mtl.rule.model.DT11VerifyNationaRider;
import com.mtl.rule.model.DT11VerifyNationality;
import com.mtl.rule.model.DT11VerifySexByPlancode;
import com.mtl.rule.util.RuleConstants;

@Repository
public class Dt11VerifyDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public List<DT11VerifyNationality> testQuery(String healthQuestion, String nationality, String lengthIdcard, String insureAmount) throws Exception {
		List<DT11VerifyNationality> selectedList = new ArrayList<DT11VerifyNationality>();

		List<DT11VerifyNationality> valList = getDt11VerifyNationalityTest(healthQuestion, nationality);
		if (valList != null && valList.size() > 0) {
			for (DT11VerifyNationality item : valList) {
				if (StringUtils.isBlank(item.getLengthIdCard())) {
//					if(StringUtils.isBlank(item.getInsured())) {
//						selectedList.add(item);
//					}else{
//						if(new BigDecimal(insureAmount).compareTo(new BigDecimal(item.getInsured())) > 0) {
//							
//						}
//						selectedList.add(item);
//					}else if(! ) {
//						selectedList.add(item);
//					}
				} else if (new BigDecimal(item.getLengthIdCard()).compareTo(new BigDecimal(lengthIdcard)) == 0) {
					if (StringUtils.isBlank(item.getInsured())) {
						selectedList.add(item);
					} else if (new BigDecimal(insureAmount).compareTo(new BigDecimal(item.getInsured())) > 0) {
						selectedList.add(item);
					} else if (!(new BigDecimal(insureAmount).compareTo(new BigDecimal(item.getInsured())) > 0)) {
						selectedList.add(item);
					}
				} else {
					if (StringUtils.isBlank(item.getInsured())) {
						selectedList.add(item);
					} else if (new BigDecimal(insureAmount).compareTo(new BigDecimal(item.getInsured())) > 0) {
						selectedList.add(item);
					} else if (!(new BigDecimal(insureAmount).compareTo(new BigDecimal(item.getInsured())) > 0)) {
						selectedList.add(item);
					}
				}
			}
		}

		return selectedList;
	}

	public List<DT11VerifyNationality> getDt11VerifyNationalityTest(String healthQuestion, String nationality) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_VERIFY_NATIONALITY ");
		sql.append(" WHERE 1=1");

		String nationality1 = "%," + nationality;
		String nationality2 = "%," + nationality + ",%";
		String nationality3 = nationality + ",%";

		sql.append(" AND  (nationality = ? ");
		sql.append(" OR nationality LIKE ? ");
		sql.append(" OR nationality LIKE ?  ");
		sql.append(" OR nationality LIKE ? ");
		sql.append(" OR nationality is null ) ");

		params.add(nationality);
		params.add(nationality1);
		params.add(nationality2);
		params.add(nationality3);

		sql.append(" AND (NOT_NATIONALITY != ?  ");
		sql.append(" OR NOT_NATIONALITY NOT LIKE ? ");
		sql.append(" OR NOT_NATIONALITY NOT LIKE ?  ");
		sql.append(" OR NOT_NATIONALITY NOT LIKE ?  ");
		sql.append(" OR NOT_NATIONALITY is null)  ");
		params.add(nationality);
		params.add(nationality1);
		params.add(nationality2);
		params.add(nationality3);
		List<DT11VerifyNationality> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11VerifyNationality.class));
		if (dt11Obj != null && dt11Obj.size() > 0) {
			return dt11Obj;
		} else {
			return null;
		}

	}

	public DT11VerifyNationality getDt11VerifyNationality(String healthQuestion, String nationality, BigDecimal insureAmount) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		sql.append(" SELECT * FROM DT11_VERIFY_NATIONALITY ");
		sql.append(" WHERE 1=1");
		if (StringUtils.isNotBlank(healthQuestion)) {
			sql.append(" AND HEALTH_QUESTION = ? ");
			params.add(healthQuestion);
		}

		if (StringUtils.isNotBlank(nationality)) {
			String nationality1 = "%," + nationality;
			String nationality2 = "%," + nationality + ",%";
			String nationality3 = nationality + ",%";
			params.add(nationality);
			params.add(nationality1);
			params.add(nationality2);
			params.add(nationality3);

			params.add(nationality);
			params.add(nationality1);
			params.add(nationality2);
			params.add(nationality3);
			sql.append(" AND ( ( NATIONALITY = ?  ");
			sql.append("  OR NATIONALITY LIKE ?  ");
			sql.append("  OR NATIONALITY LIKE ? ");
			sql.append("  OR NATIONALITY LIKE ? )  ");

			sql.append(" OR ( NOT_NATIONALITY != ?  ");
			sql.append("  OR NOT_NATIONALITY NOT LIKE ?  ");
			sql.append("  OR NOT_NATIONALITY NOT LIKE ? ");
			sql.append("  OR NOT_NATIONALITY NOT LIKE ? )  ) ");
		}

		List<DT11VerifyNationality> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11VerifyNationality.class));

		return dt11Obj.get(0);
	}

	public List<DT11VerifyNationaRider> getDt11VerifyNationalityForRider() {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_VERIFY_NATION_RIDER ");
		sql.append(" WHERE 1=1");
		List<DT11VerifyNationaRider> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11VerifyNationaRider.class));

		return dt11Obj;
	}

	//

	public List<DT11VerifyNationWithPlan> getDt11VerifyNationalityWithPlan(String nationality, String planCode) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_VERIFY_NATION_WITH_PLAN ");
		sql.append(" WHERE 1=1");
		List<DT11VerifyNationWithPlan> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11VerifyNationWithPlan.class));

		return dt11Obj;
	}

	// 6.
	public HashMap<String, DT11VerifyNationUSA> getDt11VerifyNationalityUSA() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_VERIFY_NATION_USA  ");
		List<DT11VerifyNationUSA> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11VerifyNationUSA.class));
		System.out.println(" Get All DT11_VERIFY_NATION_USA.. Size: " + ls.size());
		return rewriteDataVerifyNationalityUSA(ls);
	}

	private HashMap<String, DT11VerifyNationUSA> rewriteDataVerifyNationalityUSA(List<DT11VerifyNationUSA> ls) {
		HashMap<String, DT11VerifyNationUSA> mapVal = new HashMap<String, DT11VerifyNationUSA>();
		ls.parallelStream().forEach((item) -> {
			mapVal.put(item.getNationality(), item);
		});
		return mapVal;
	}

	public DT11VerifySexByPlancode getDT11VerifySexByPlancode(String planCode, String sex) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_VERIFY_SEX_BY_PLANCODE ");
		sql.append(" WHERE 1=1");

		if (StringUtils.isNotBlank(planCode)) {
			String planCode1 = "%," + planCode;
			String planCode2 = "%," + planCode + ",%";
			String planCode3 = planCode + ",%";
			params.add(planCode);
			params.add(planCode1);
			params.add(planCode2);
			params.add(planCode3);
			sql.append(" AND ( PLAN_CODE = ?  ");
			sql.append("  OR PLAN_CODE LIKE ?  ");
			sql.append("  OR PLAN_CODE LIKE ? ");
			sql.append("  OR PLAN_CODE LIKE ? )  ");
		}

		if (StringUtils.isNotBlank(sex)) {
			String sex1 = "%," + sex;
			String sex2 = "%," + sex + ",%";
			String sex3 = sex + ",%";
			params.add(sex);
			params.add(sex1);
			params.add(sex2);
			params.add(sex3);
			sql.append(" AND ( SEX_IS_NOT = ?  ");
			sql.append("  OR SEX_IS_NOT LIKE ?  ");
			sql.append("  OR SEX_IS_NOT LIKE ? ");
			sql.append("  OR SEX_IS_NOT LIKE ? )  ");
		}

		List<DT11VerifySexByPlancode> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11VerifySexByPlancode.class));

		if (dt11Obj != null && dt11Obj.size() > 0) {
			return dt11Obj.get(0);
		} else {
			return null;
		}

	}

	public HashMap<String, List<DT11VerifyNationality>> getAllListVerifyNationality() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_VERIFY_NATIONALITY  ");
		List<DT11VerifyNationality> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11VerifyNationality.class));
		System.out.println(" Get All DT11_VERIFY_NATIONALITY.. Size: " + ls.size());
		return rewriteDataVerifyNationality(ls);
	}

	private HashMap<String, List<DT11VerifyNationality>> rewriteDataVerifyNationality(List<DT11VerifyNationality> ls) {
		HashMap<String, List<DT11VerifyNationality>> mapVal = new HashMap<String, List<DT11VerifyNationality>>();
		ls.forEach((item) -> {
			String key = item.getHealthQuestion() + "|" + item.getNationality();
			List<DT11VerifyNationality> listInMap = mapVal.get(key);
			if (listInMap != null) {
				listInMap.add(item);
				mapVal.put(key, listInMap);
			} else {
				listInMap = new ArrayList<DT11VerifyNationality>();
				listInMap.add(item);
				mapVal.put(key, listInMap);
			}
		});
		return mapVal;
	}

	public HashMap<String, DT11VerifyNationWithPlan> getAllList(boolean isOtherwise) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		sb.append(" SELECT * FROM DT11_VERIFY_NATION_WITH_PLAN  ");
		sb.append(" WHERE FOREIGNE_ID_CARD ");
		sb.append(isOtherwise ? " = ? " : " != ? ");
		param.add(RuleConstants.OTHERWISE);

		sb.append(" OR NATIONALITY ");
		sb.append(isOtherwise ? " = ? " : " != ? ");
		param.add(RuleConstants.OTHERWISE);
		List<DT11VerifyNationWithPlan> ls = commonJdbcTemplate.executeQuery(sb.toString(), param.toArray(), new BeanPropertyRowMapper<>(DT11VerifyNationWithPlan.class));
		System.out.println(" Get All DT11_VERIFY_NATION_WITH_PLAN.. Size: " + ls.size());
		return rewriteData(ls);
	}

	private HashMap<String, DT11VerifyNationWithPlan> rewriteData(List<DT11VerifyNationWithPlan> ls) {
		HashMap<String, DT11VerifyNationWithPlan> mapVal = new HashMap<String, DT11VerifyNationWithPlan>();
		ls.forEach((item) -> {
			if (item.getPlanCode() != null) {
				List<String> listPlanCode = Arrays.asList(item.getPlanCode().split(","));
				for (String planCode : listPlanCode) {
					mapVal.put(planCode, item);
				}
			}
		});
		return mapVal;
	}
	
	
	
	public List<DT11AgeforSpecialPlan> getDT11AgeforSpecialOnlyPlan(String planCode) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_AGE_FOR_SPECIAL_PLAN ");
		sql.append(" WHERE PLAN_CODE =? ");

		params.add(planCode);

		List<DT11AgeforSpecialPlan> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11AgeforSpecialPlan.class));

		return dt11Obj;
	}
	
	
	public List<DT11AgeforSpecialPlan> getDT11AgeforSpecialPlan(String planCode, String ageMonth,String ageDay) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_AGE_FOR_SPECIAL_PLAN ");
		sql.append(" WHERE ");
		sql.append("  (PLAN_CODE =? )  ");
		sql.append(" AND  (AGE_IN_MONTH=? OR AGE_DAY=? ) ");
		
		params.add(planCode);
		params.add(ageMonth);
		params.add(ageDay);
		
		List<DT11AgeforSpecialPlan> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11AgeforSpecialPlan.class));

		return dt11Obj;
	}
	
	
	public List<DT11AgeSpecialPlanAtleast> getDT11AgeSpecialPlanAtleast(String planCode, String age) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_AGE_SPECIAL_PLAN_ATLEAST ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AGE = ? AND PLAN_CODE =? ");
		
		params.add(age);
		params.add(planCode);
		
		List<DT11AgeSpecialPlanAtleast> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11AgeSpecialPlanAtleast.class));

		return dt11Obj;
	}
	
	public List<MsPlanHeader> getMsPlanHeaderAge(String planCode, String age) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM  MS_PLAN_HEADER WHERE PLAN_CODE =? AND LOWER_AGE <=?  AND UPPER_AGE >= ?  ");

		
		params.add(planCode);

		params.add(age);
		params.add(age);
		List<MsPlanHeader> dt11Obj = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(MsPlanHeader.class));

		return dt11Obj;
	}
	
}
