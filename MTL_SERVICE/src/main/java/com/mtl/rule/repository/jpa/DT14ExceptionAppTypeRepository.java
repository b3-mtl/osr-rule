package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT14ExceptionAppType;

@Repository
public interface DT14ExceptionAppTypeRepository extends CrudRepository<DT14ExceptionAppType, Long>{

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT14ExceptionAppType> findAll();
}
