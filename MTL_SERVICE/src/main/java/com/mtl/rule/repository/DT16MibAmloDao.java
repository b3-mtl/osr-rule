package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16MibAmlo;

@Repository
public class DT16MibAmloDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public List<DT16MibAmlo> findMibAmlo(List<String> mibCodeList) {
		List<DT16MibAmlo> dataRes = new ArrayList<DT16MibAmlo>();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_MIB_AMLO WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (mibCodeList.size() >0) {
			sqlBuilder.append(" AND CODE IN ? ");
			params.add(mibCodeList);
		}

		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	

	private RowMapper<DT16MibAmlo> listRowmapper = new RowMapper<DT16MibAmlo>() {
		@Override
		public DT16MibAmlo mapRow(ResultSet rs, int arg1) throws SQLException {
			DT16MibAmlo vo = new DT16MibAmlo();
			vo.setId(rs.getLong("ID"));
			vo.setCode(rs.getString("CODE"));
			vo.setUnderwrite(rs.getString("UNDERWRITE"));
			vo.setC(rs.getString("C"));
			vo.setD(rs.getString("D"));
			
			return vo;
		}
	};
	
}
