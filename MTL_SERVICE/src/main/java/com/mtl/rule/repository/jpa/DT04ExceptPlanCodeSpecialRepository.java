package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT04ExceptPlanCodeSpecial;



@Repository
public interface DT04ExceptPlanCodeSpecialRepository extends CrudRepository<DT04ExceptPlanCodeSpecial, Long>{

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT04ExceptPlanCodeSpecial> findAll();
	
	@Query("select e from #{#entityName} e where e.isDelete = 'N' Order by e.id DESC")
	public List<DT04ExceptPlanCodeSpecial> findAllOrderId();
}