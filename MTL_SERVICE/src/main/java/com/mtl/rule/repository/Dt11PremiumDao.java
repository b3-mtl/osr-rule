package com.mtl.rule.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11PlanReferPremiumPlus;
import com.mtl.rule.model.DT11PremiumPlus;
import com.mtl.rule.model.MsSuperVIP;

@Repository
public class Dt11PremiumDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT11PremiumPlus> getListDT11PremiumPlus() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_PREMIUM_PLUS  ");
		List<DT11PremiumPlus> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}
					, new BeanPropertyRowMapper<>(DT11PremiumPlus.class));
		System.out.println(" Get All DT11_PREMIUM_PLUS.. Size: " + ls.size());
		return rewriteData(ls);
	}

	private HashMap<String, DT11PremiumPlus> rewriteData(List<DT11PremiumPlus> ls) {
		HashMap<String, DT11PremiumPlus> mapVal = new HashMap<String, DT11PremiumPlus>();
		ls.parallelStream().forEach((item) -> {
			mapVal.put(item.getOccupationCode(), item);
		});
		return mapVal;
	}
	
	public HashMap<String, DT11PlanReferPremiumPlus> getPlanReferPremiumPlus() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_PLAN_REFER_PREMIUM_PLUS  ");
		List<DT11PlanReferPremiumPlus> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}
						, new BeanPropertyRowMapper<>(DT11PlanReferPremiumPlus.class));
		System.out.println(" Get All DT11_PLAN_REFER_PREMIUM_PLUS.. Size: " + ls.size());
		return rewriteDataPlanReferPremiumPlus(ls);
	}

	private HashMap<String, DT11PlanReferPremiumPlus> rewriteDataPlanReferPremiumPlus(List<DT11PlanReferPremiumPlus> ls) {
		HashMap<String, DT11PlanReferPremiumPlus> mapVal = new HashMap<String, DT11PlanReferPremiumPlus>();
		for (DT11PlanReferPremiumPlus item : ls) {
			if(item.getPlanCode() != null) {
				List<String> listPlanCode = Arrays.asList(item.getPlanCode().split(","));
				listPlanCode.forEach((planCode) -> {
					mapVal.put(planCode, item);
				});
			}
		}
		return mapVal;
	}

	public boolean getMsSuperVIP(String clientNo) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM MS_SUPER_VIP ");
		sql.append(" WHERE 1=1");
		if (StringUtils.isNotBlank(clientNo)) {
			params.add(clientNo);
			sql.append(" AND  CSCLI = ?  ");
		}
		List<MsSuperVIP> preLis = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(MsSuperVIP.class));
		if (preLis != null && preLis.size() > 0) {
			return true;
		} else {
			return false;
		}

	}
}
