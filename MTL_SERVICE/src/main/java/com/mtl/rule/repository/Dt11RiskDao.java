package com.mtl.rule.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11RiskAreasHouseRegis;
import com.mtl.rule.model.DT11RiskAreasOffice;
import com.mtl.rule.model.DT11RiskAreasPrestAddr;
import com.mtl.rule.model.DT11RiskMobileNumber;

@Repository
public class Dt11RiskDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT11RiskAreasOffice> getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_RISK_AREAS_OFFICE  ");
		List<DT11RiskAreasOffice> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11RiskAreasOffice.class));
		System.out.println(" Get All DT11_RISK_AREAS_OFFICE.. Size: " + ls.size());
		return rewriteData(ls);
	}

	private HashMap<String, DT11RiskAreasOffice> rewriteData(List<DT11RiskAreasOffice> ls) {
		HashMap<String, DT11RiskAreasOffice> mapVal = new HashMap<String, DT11RiskAreasOffice>();
		ls.parallelStream().forEach((item)->{
			String key = item.getOfficeProvince() + "|" + item.getOfficeSuburb()  + "|" + item.getOfficeDistrict()  + "|" + item.getMoo() + "|" + item.getHouseNumber();
			mapVal.put(key, item);
		});
		
		return mapVal;
	}

	public HashMap<String, DT11RiskAreasHouseRegis> getAllListRiskAreasHouseRegis() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_RISK_AREAS_HOUSE_REGIS  ");
		List<DT11RiskAreasHouseRegis> ls = commonJdbcTemplate.executeQuery(sb.toString(), 
				new Object[] {}, new BeanPropertyRowMapper<>(DT11RiskAreasHouseRegis.class));
		System.out.println(" Get All DT11_RISK_AREAS_HOUSE_REGIS.. Size: " + ls.size());
		return rewriteDataRiskAreasHouseRegis(ls);
	}

	private HashMap<String, DT11RiskAreasHouseRegis> rewriteDataRiskAreasHouseRegis(List<DT11RiskAreasHouseRegis> ls) {
		HashMap<String, DT11RiskAreasHouseRegis> mapVal = new HashMap<String, DT11RiskAreasHouseRegis>();
		String key = "";
		for (DT11RiskAreasHouseRegis item : ls) {
			key = item.getProvince() + "|" + item.getSuburb() + "|" + item.getDistrict() + "|" + item.getMoo()+ "|" + item.getHouseNumber();
			mapVal.put(key, item);
		}
		return mapVal;
	}

	public HashMap<String, DT11RiskAreasPrestAddr> getAllListRiskAreasPrestAddr() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_RISK_AREAS_PREST_ADDR  ");
		List<DT11RiskAreasPrestAddr> ls = commonJdbcTemplate.executeQuery(sb.toString(), 
				new Object[] {}, new BeanPropertyRowMapper<>(DT11RiskAreasPrestAddr.class));
		System.out.println(" Get All DT11_RISK_AREAS_PREST_ADDR.. Size: " + ls.size());
		return rewriteDataRiskAreasPrestAddr(ls);
	}

	private HashMap<String, DT11RiskAreasPrestAddr> rewriteDataRiskAreasPrestAddr(List<DT11RiskAreasPrestAddr> ls) {
		HashMap<String, DT11RiskAreasPrestAddr> mapVal = new HashMap<String, DT11RiskAreasPrestAddr>();
		String key = "";
		for (DT11RiskAreasPrestAddr item : ls) {
			key = item.getPresentProvince() + "|" + item.getPresentSuburb() + "|" + item.getPresentDistrict()   + "|" + item.getMoo() + "|" + item.getHouseNumber();
			mapVal.put(key, item);
		}
		
		return mapVal;
	}

	public HashMap<String, DT11RiskMobileNumber> getAllListRiskMobileNumber() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_RISK_MOBILE_NUMBER  ");
		List<DT11RiskMobileNumber> ls = commonJdbcTemplate.executeQuery(sb.toString(), 
				new Object[] {}, new BeanPropertyRowMapper<>(DT11RiskMobileNumber.class));
		System.out.println(" Get All DT11_RISK_MOBILE_NUMBER.. Size: " + ls.size());
		return rewriteDataRiskMobileNumber(ls);
	}

	private HashMap<String, DT11RiskMobileNumber> rewriteDataRiskMobileNumber(List<DT11RiskMobileNumber> ls) {
		HashMap<String, DT11RiskMobileNumber> mapVal = new HashMap<String, DT11RiskMobileNumber>();
		String key = "";
		for (DT11RiskMobileNumber item : ls) {
			key = item.getMobileNumber();
			mapVal.put(key, item);
		}
		return mapVal;
	}
	
	public DT11RiskMobileNumber getDT11RiskMobileNumber(String mobile) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT11_RISK_MOBILE_NUMBER ");
		sql.append(" WHERE 1=1");

		if (StringUtils.isNotBlank(mobile)) {
			sql.append(" AND MOBILE_NUMBER = ?");
			params.add(mobile);
		}
		List<DT11RiskMobileNumber> mobileList = commonJdbcTemplate.executeQuery(sql.toString(), params.toArray(), new BeanPropertyRowMapper<>(DT11RiskMobileNumber.class));
		if (mobileList != null && mobileList.size() > 0) {
			return mobileList.get(0);
		} else {
			return null;
		}

	}

}
