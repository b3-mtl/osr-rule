package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.MessageMapping;

@Repository
public class MessageMappingDao {
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public MessageMapping findByMessageCode(String systemName,String messageCode) {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(messageCode);
		if(StringUtils.isNotEmpty(systemName)) {
			sql.append(" SELECT * FROM MS_MESSAGE_MAPPING where MESSAGE_CODE = ? AND IS_DELETE = 'N' AND SYSTEM_NAME like '%"+systemName+"%'");
		}
		
		MessageMapping dataRes = commonJdbcTemplate.executeQueryForObject(sql.toString(), params.toArray(), rowmapper);
		return dataRes;
	}

	private RowMapper<MessageMapping> rowmapper = new RowMapper<MessageMapping>() {
		@Override
		public MessageMapping mapRow(ResultSet rs, int arg1) throws SQLException {
			MessageMapping vo = new MessageMapping();
			vo.setMessageEN(rs.getString("MESSAGE_EN"));
			vo.setMessageTH(rs.getString("MESSAGE_TH"));
			vo.setChannelCode(rs.getString("CHANNEL_CODE"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
			vo.setMessageStatus(rs.getString("MESSAGE_STATUS"));
			return vo;
		}
	};
}
