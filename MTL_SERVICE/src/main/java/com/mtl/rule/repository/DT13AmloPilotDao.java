package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13AmloPilot;

@Repository
public class DT13AmloPilotDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT13AmloPilot findAmloPilot(String occupationCode) {
		DT13AmloPilot dataRes = new DT13AmloPilot();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT13_AMLO_PILOT WHERE 1=1  ");
		
		if (StringUtils.isNotBlank(occupationCode)) {
			sqlBuilder.append(" AND OCCUPATION_CODE LIKE ? ");
			params.add("%" + occupationCode.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	
	
	private RowMapper<DT13AmloPilot> listRowmapper = new RowMapper<DT13AmloPilot>() {
		@Override
		public DT13AmloPilot mapRow(ResultSet rs, int arg1) throws SQLException {
			DT13AmloPilot vo = new DT13AmloPilot();

			vo.setOccupationCode(rs.getString("OCCUPATION_CODE"));
			vo.setReferOccupationAmlo(rs.getString("REFER_OCCUPATION_AMLO"));
			vo.setReferOccupationPilot(rs.getString("REFER_OCCUPATION_PILOT"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
			
//			vo.setMessageDesc(rs.getString("MESSAGE_DESC"));
			
			return vo;
		}
	};
}
