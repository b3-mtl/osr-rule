package com.mtl.rule.repository;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT06DiabetesDocument;
import com.mtl.rule.util.RuleUtils;

@Repository
public class Dt06DiabetesDocumentDao {

	@Autowired
	private ApplicationCache applicationCache;

	
	public List<DT06DiabetesDocument> findByPlanTypeAndSumInsured(String planType,BigDecimal sumInsure) {
		List<DT06DiabetesDocument> list = new ArrayList<DT06DiabetesDocument>();
		List<DT06DiabetesDocument> diaDocCache = applicationCache.getDT06DiabetesDocument(planType);
		boolean  checkPlanType ,checkSumInsure;
		if(diaDocCache != null) {
			for (DT06DiabetesDocument item : diaDocCache) {
				checkPlanType = false; checkSumInsure = false;
				checkPlanType = RuleUtils.andString(planType, item.getPlanType());
				if(item.getMinSumInsured() != null && item.getMaxSumInsured() != null) {
					checkSumInsure = RuleUtils.betweenNumber(sumInsure, new BigDecimal(item.getMinSumInsured()) , new BigDecimal(item.getMaxSumInsured()));
				}
				if(checkPlanType && checkSumInsure) {
					list.add(item);
				}
			}
		}
		return list;
	}
	
	public List<DT06DiabetesDocument> findByPlanTypeAndAge(String planType,String age) {
		List<DT06DiabetesDocument> list = new ArrayList<DT06DiabetesDocument>();
		List<DT06DiabetesDocument> diaDocCache = applicationCache.getDT06DiabetesDocument(planType);
		boolean  checkPlanType ,checkAge;
		if(diaDocCache != null) {
			for (DT06DiabetesDocument item : diaDocCache) {
				checkPlanType = false; checkAge = false;
				checkPlanType = RuleUtils.andString(planType, item.getPlanType());
				if(item.getMinAge() != null && item.getMaxAge() != null) {
					checkAge = RuleUtils.betweenNumber(new BigDecimal(age), new BigDecimal(item.getMinAge()) , new BigDecimal(item.getMaxAge()));
				}
				if(checkPlanType && checkAge) {
					list.add(item);
				}
			}
		}
		return list;
	}

	
}
