package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT16MibHighRisk;

@Repository
public interface DT16MibHighRiskRepository extends CrudRepository<DT16MibHighRisk, Long> {

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT16MibHighRisk> findAll();
}
