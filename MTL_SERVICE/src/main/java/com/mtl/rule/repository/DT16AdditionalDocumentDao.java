package com.mtl.rule.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16VerifyNationality;

public class DT16AdditionalDocumentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	public List<DT16VerifyNationality> findAMLO(String healthQuestion,String nationality) {
		StringBuilder sql  = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT16_VERIFY_NATIONALITY");
		sql.append(" WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if(StringUtils.isNotBlank(healthQuestion)) {
			String value1 =  "%,"+healthQuestion;
			String value2 =  "%,"+healthQuestion+",%";
			String value3 =  healthQuestion+",%";
			params.add(healthQuestion);
			params.add(value1);
			params.add(value2);
			params.add(value3);
			sql.append(" AND ( HEALTH_QUESTION = ?  ");
			sql.append("  OR HEALTH_QUESTION LIKE ?  ");
			sql.append("  OR HEALTH_QUESTION LIKE ? ");
			sql.append("  OR HEALTH_QUESTION LIKE ? )  ");
		}
		
		if(StringUtils.isNotBlank(nationality)) {
			String value1 =  "%,"+nationality;
			String value2 =  "%,"+nationality+",%";
			String value3 =  nationality+",%";
			params.add(nationality);
			params.add(value1);
			params.add(value2);
			params.add(value3);
			sql.append(" AND ( NATIONALITY = ?  ");
			sql.append("  OR NATIONALITY LIKE ?  ");
			sql.append("  OR NATIONALITY LIKE ? ");
			sql.append("  OR NATIONALITY LIKE ? )  ");
		}
		List<DT16VerifyNationality> ls = commonJdbcTemplate.executeQuery(sql.toString()
				, params.toArray()
				, new BeanPropertyRowMapper<>(DT16VerifyNationality.class)
			);		
		return ls;
	}

	
}
