package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16PayorRelation;



@Repository
public class DT16PayorRelationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public List<DT16PayorRelation> findPayorRelation(String payorRelation) {
		List<DT16PayorRelation> dataRes = new ArrayList<DT16PayorRelation>();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_PAYOR_RELATION WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(payorRelation)) {
			sqlBuilder.append(" AND RELATIONSHIP LIKE ? ");
			params.add("%" + payorRelation.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	

	private RowMapper<DT16PayorRelation> listRowmapper = new RowMapper<DT16PayorRelation>() {
		@Override
		public DT16PayorRelation mapRow(ResultSet rs, int arg1) throws SQLException {
			DT16PayorRelation vo = new DT16PayorRelation();
			vo.setId(rs.getLong("ID"));
			vo.setRelationship(rs.getString("RELATIONSHIP"));
			vo.setDocument(rs.getString("DOCUMENT"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
			vo.setMessageDesc(rs.getString("MESSAGE_DESC"));

			return vo;
		}
	};

}
