package com.mtl.rule.repository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT11RejectDeclinePostpone;

@Repository
public class Dt11RejectDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public HashMap<String, DT11RejectDeclinePostpone> getAllList() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM DT11_REJECT_DECLINE_POSTPONE  ");
		List<DT11RejectDeclinePostpone> ls = commonJdbcTemplate.executeQuery(sb.toString(), new Object[] {}, new BeanPropertyRowMapper<>(DT11RejectDeclinePostpone.class));
		System.out.println(" Get All DT11_REJECT_DECLINE_POSTPONE.. Size: " + ls.size());
		return rewriteData(ls);
	}

	private HashMap<String, DT11RejectDeclinePostpone> rewriteData(List<DT11RejectDeclinePostpone> ls) {
		HashMap<String, DT11RejectDeclinePostpone> mapVal = new HashMap<String, DT11RejectDeclinePostpone>();
		for (DT11RejectDeclinePostpone item : ls) {
			if(item.getPlanCode() != null) {
				List<String> listPlanCode = Arrays.asList(item.getPlanCode().split(","));
				listPlanCode.forEach((planCode) -> {
					mapVal.put(planCode, item);
				});
			}
		}
		return mapVal;
	}
}
