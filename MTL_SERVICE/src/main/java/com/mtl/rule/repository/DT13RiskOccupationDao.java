package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13RiskOccupation;

@Repository
public class DT13RiskOccupationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT13RiskOccupation findRiskOccupationCode(String occupationCode) {
		DT13RiskOccupation dataRes = new DT13RiskOccupation();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT13_RISK_OCCUPATION WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(occupationCode)) {
			sqlBuilder.append(" AND OCCUPATION_CODE LIKE ? ");
			params.add("%" + occupationCode.replaceAll(" ", "%")+ "%");
		}

		sqlBuilder.append(" AND DOCUMENT_CODE IS NOT NULL ");
		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	
	
	private RowMapper<DT13RiskOccupation> listRowmapper = new RowMapper<DT13RiskOccupation>() {
		@Override
		public DT13RiskOccupation mapRow(ResultSet rs, int arg1) throws SQLException {
			DT13RiskOccupation vo = new DT13RiskOccupation();

			vo.setOccupationCode(rs.getString("OCCUPATION_CODE"));
			vo.setNotSystemName(rs.getString("NOT_SYSTEM_NAME"));
			vo.setReferUnderwrite(rs.getString("REFER_UNDERWRITE"));
			vo.setDocumentCode(rs.getString("DOCUMENT_CODE"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
			
			
			return vo;
		}
	};
}
