package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16Occupation;

@Repository
public class DT16OccupationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public List<DT16Occupation> findOccupation(String occupationCode) {
		List<DT16Occupation> dataRes = new ArrayList<DT16Occupation>();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_OCCUPATION WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(occupationCode)) {
			sqlBuilder.append(" AND OCCUPATION_CODE LIKE ? ");
			params.add("%" + occupationCode.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQuery(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	

	private RowMapper<DT16Occupation> listRowmapper = new RowMapper<DT16Occupation>() {
		@Override
		public DT16Occupation mapRow(ResultSet rs, int arg1) throws SQLException {
			DT16Occupation vo = new DT16Occupation();
			vo.setId(rs.getLong("ID"));
			vo.setOccupationCode(rs.getString("OCCUPATION_CODE"));
			vo.setPb(rs.getString("PB"));
			vo.setReferUnderwriter(rs.getString("REFER_UNDERWRITER"));
			vo.setOccupationCodeAMLO(rs.getString("OCCUPATION_CODE_AMLO"));
			vo.setRuleMessageCodeDesc(rs.getString("RULE_MESSAGE_CODE_DESC"));

			return vo;
		}
	};
	
}
