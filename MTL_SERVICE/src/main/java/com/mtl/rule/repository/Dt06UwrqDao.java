package com.mtl.rule.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT06UWRQ;
import com.mtl.rule.util.RuleUtils;

@Repository
public class Dt06UwrqDao {
	@Autowired
	private ApplicationCache applicationCache;
	/**
	 * @param clientLocal
	 * @param agenQua
	 * @param age
	 * @param sumInsure
	 * @param sex
	 * 
	 * Find by method .getlist()
	 * Case 1 by clientLocal , agenQua , age , sumInsure , sex
	 * Case 2 by clientLocal , agenQua , age , sumInsure 
	 * Case 3 by clientLocal , agenQua , age 
	 * Case 4 by clientLocal , agenQua 
	 * 
	 * @return
	 */
	public DT06UWRQ findResult(String clientLocal, String agenQua, String age, BigDecimal sumInsure, String sex) {
		DT06UWRQ uwrq = getuwrq( clientLocal,  agenQua,  age,  sumInsure,  sex);
		if(uwrq == null) {
			uwrq = getuwrq( clientLocal,  agenQua,  age,  sumInsure,  null);
			if(uwrq == null) {
				uwrq = getuwrq( clientLocal,  agenQua,  age,  null,  null);
				if(uwrq == null) {
					uwrq = getuwrq( clientLocal,  agenQua,  null,  null,  null);
				}
			}
		}
		
		return uwrq;
	}
	
	
	public DT06UWRQ getuwrq(String clientLocal, String agenQua, String age, BigDecimal sumInsure, String sex) {
		List<DT06UWRQ> cacheList = applicationCache.getDt06Uwrq(clientLocal+"|"+agenQua);
		Boolean checkAge, checkSex, checkSum;
		for (DT06UWRQ dt06uwrq : cacheList) {
			checkAge = false;  checkSex= false; checkSum= false;
			
			checkAge = RuleUtils.betweenNumber(Long.valueOf(age), dt06uwrq.getMinAge(), dt06uwrq.getMaxAge());
			checkSum = RuleUtils.betweenNumber(sumInsure, dt06uwrq.getSumInsuredMin(), dt06uwrq.getSumInsuredMax());
			checkSex = RuleUtils.andString(sex, dt06uwrq.getSexCode());
		
			if(checkAge && checkSex && checkSum) {
				return dt06uwrq;
			}
		}
		return null;
		
	}
	
	
}
