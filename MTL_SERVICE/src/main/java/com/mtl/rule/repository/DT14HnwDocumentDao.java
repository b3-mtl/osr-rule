package com.mtl.rule.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT14HnwDocument;

@Repository
public class DT14HnwDocumentDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;

	public List<DT14HnwDocument> findResult(BigDecimal sumInsured,String age) {
		StringBuilder sql  = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DT14_HNW_DOCUMENT ");
		sql.append(" WHERE 1=1 AND IS_DELETE = 'N' ");
if(sumInsured!=null) {
			sql.append(" AND (? BETWEEN SUM_INSURED_MIN AND SUM_INSURED_MAX )");
			params.add(sumInsured);
		}
		if(StringUtils.isNotBlank(age)) {
			sql.append(" AND (? BETWEEN AGE_MIN AND AGE_MAX )");
			params.add(age);
		}
		List<DT14HnwDocument> ls = commonJdbcTemplate.executeQuery(sql.toString()
				, params.toArray()
				, new BeanPropertyRowMapper<>(DT14HnwDocument.class)
			);		
		return ls;
	}
}
