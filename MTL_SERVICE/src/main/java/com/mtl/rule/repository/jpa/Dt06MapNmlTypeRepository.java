package com.mtl.rule.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.Dt06MapNmlType;

@Repository
public interface Dt06MapNmlTypeRepository extends CrudRepository<Dt06MapNmlType, Long>{

}
