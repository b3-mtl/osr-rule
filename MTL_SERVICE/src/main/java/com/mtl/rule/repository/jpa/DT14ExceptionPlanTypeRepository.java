package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT14ExceptionPlanType;

@Repository
public interface DT14ExceptionPlanTypeRepository extends CrudRepository<DT14ExceptionPlanType, Long> {

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT14ExceptionPlanType> findAll();

}
