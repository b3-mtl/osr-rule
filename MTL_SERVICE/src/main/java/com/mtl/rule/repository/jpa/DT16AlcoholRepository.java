package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT16Alcohol;

@Repository
public interface DT16AlcoholRepository extends CrudRepository<DT16Alcohol, Long> {

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT16Alcohol> findAll();
}
