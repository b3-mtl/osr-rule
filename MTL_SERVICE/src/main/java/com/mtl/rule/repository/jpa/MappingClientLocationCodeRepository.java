package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.MappingClientLocationCode;

@Repository
public interface MappingClientLocationCodeRepository extends CrudRepository<MappingClientLocationCode, Long>{
	public MappingClientLocationCode findByProvinceCode(String provinceCode);
	
	@Override
	public List<MappingClientLocationCode> findAll();
}
