package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT14Exception;


@Repository
public interface DT14ExceptionPlanCodeRepository extends CrudRepository<DT14Exception, Long>{

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT14Exception> findAll();
}