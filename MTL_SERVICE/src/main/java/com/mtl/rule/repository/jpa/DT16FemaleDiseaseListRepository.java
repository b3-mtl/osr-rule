package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT16FemaleDiseaseList;

@Repository
public interface DT16FemaleDiseaseListRepository extends CrudRepository<DT16FemaleDiseaseList, Long> {

	@Query("select e from #{#entityName} e where e.isDelete = 'N'")
	public List<DT16FemaleDiseaseList> findAll();
}
