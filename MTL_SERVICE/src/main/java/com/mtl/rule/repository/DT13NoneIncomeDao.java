package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13NoneIncome;

@Repository
public class DT13NoneIncomeDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT13NoneIncome findOccupationCode(String occupationCode) {
		DT13NoneIncome dataRes = new DT13NoneIncome();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT13_NONE_INCOME WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(occupationCode)) {
			sqlBuilder.append(" AND OCCUPATION_CODE LIKE ? ");
			params.add("%" + occupationCode.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	
	
	private RowMapper<DT13NoneIncome> listRowmapper = new RowMapper<DT13NoneIncome>() {
		@Override
		public DT13NoneIncome mapRow(ResultSet rs, int arg1) throws SQLException {
			DT13NoneIncome vo = new DT13NoneIncome();

			vo.setMinSumIns(Long.valueOf(rs.getString("MIN_SUM_INS")));
			vo.setMaxSumIns(Long.valueOf(rs.getString("MAX_SUM_INS")));
			vo.setMessageDesc(rs.getString("MESSAGE_DESC"));
			vo.setOccupationCode(rs.getString("OCCUPATION_CODE"));

			return vo;
		}
	};
}
