package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT16FemaleDiseaseList;

@Repository
public class DT16FemaleDiseaseListDao {
	
	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT16FemaleDiseaseList findFemaleDiseaseList(String symptonName) {
		DT16FemaleDiseaseList dataRes = new DT16FemaleDiseaseList();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT16_FEMALE_DISEASE_LIST WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(symptonName)) {
			sqlBuilder.append(" AND SYMTON_NAME LIKE ? ");
			params.add("%" + symptonName.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	

	private RowMapper<DT16FemaleDiseaseList> listRowmapper = new RowMapper<DT16FemaleDiseaseList>() {
		@Override
		public DT16FemaleDiseaseList mapRow(ResultSet rs, int arg1) throws SQLException {
			DT16FemaleDiseaseList vo = new DT16FemaleDiseaseList();
			vo.setId(rs.getLong("ID"));
			vo.setSymtonName(rs.getString("SYMTON_NAME"));
			vo.setUnderwrite(rs.getString("UNDERWRITE"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
			
			return vo;
		}
	};
	
}
