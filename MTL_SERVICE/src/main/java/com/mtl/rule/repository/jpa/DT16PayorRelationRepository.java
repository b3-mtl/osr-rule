package com.mtl.rule.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.model.DT16PayorRelation;

@Repository
public interface DT16PayorRelationRepository extends CrudRepository<DT16PayorRelation, Long>{

	@Query("select e from #{#entityName} e where e.isDelete = 'N' ")
	public List<DT16PayorRelation> findAll();

	@Query("select e from #{#entityName} e where e.isDelete = 'N' ")
	public List<DT16PayorRelation> findAllOrderId();
}
