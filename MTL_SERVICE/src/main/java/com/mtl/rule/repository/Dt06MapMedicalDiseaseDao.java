package com.mtl.rule.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mtl.appcache.ApplicationCache;
import com.mtl.rule.model.DT06MapMedicalDisease;

@Repository
public class Dt06MapMedicalDiseaseDao {
	
	@Autowired
	ApplicationCache applicationCache;

	public List<DT06MapMedicalDisease> findDocument(List<String> charList) {
		List<DT06MapMedicalDisease> list = new ArrayList<DT06MapMedicalDisease>();
		DT06MapMedicalDisease item = null;
		for (String key : charList) {
			item = applicationCache.getDt06MapMedicalDisease(key);
			if(item != null) {
				list.add(item);
			}
		}
		return list;
	}

	
}
