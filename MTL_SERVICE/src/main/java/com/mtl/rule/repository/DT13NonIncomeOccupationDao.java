package com.mtl.rule.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.mtl.api.utils.CommonJdbcTemplate;
import com.mtl.rule.model.DT13NonIncomeOccupation;


@Repository
public class DT13NonIncomeOccupationDao {

	@Autowired
	private CommonJdbcTemplate commonJdbcTemplate;
	
	
	public DT13NonIncomeOccupation findOccupationCode(String occupationCode) {
		DT13NonIncomeOccupation dataRes = new DT13NonIncomeOccupation();
		StringBuilder sqlBuilder = new StringBuilder();
		List<Object> params = new ArrayList<>();
		sqlBuilder.append(" SELECT * FROM DT13_NON_INCOME_OCCUPATION WHERE 1=1 AND IS_DELETE = 'N' ");
		
		if (StringUtils.isNotBlank(occupationCode)) {
			sqlBuilder.append(" AND OCCUPATION_CODE LIKE ? ");
			params.add("%" + occupationCode.replaceAll(" ", "%")+ "%");
		}

		dataRes = commonJdbcTemplate.executeQueryForObject(sqlBuilder.toString(), params.toArray(), listRowmapper);
		return dataRes;
	}
	
	
	private RowMapper<DT13NonIncomeOccupation> listRowmapper = new RowMapper<DT13NonIncomeOccupation>() {
		@Override
		public DT13NonIncomeOccupation mapRow(ResultSet rs, int arg1) throws SQLException {
			DT13NonIncomeOccupation vo = new DT13NonIncomeOccupation();

			vo.setOccupationCode(rs.getString("OCCUPATION_CODE"));
			vo.setDocumentCode(rs.getString("DOCUMENT_CODE"));
			vo.setMessageCode(rs.getString("MESSAGE_CODE"));
//			vo.setMessageDesc(rs.getString("MESSAGE_DESC"));
			
			return vo;
		}
	};
}
