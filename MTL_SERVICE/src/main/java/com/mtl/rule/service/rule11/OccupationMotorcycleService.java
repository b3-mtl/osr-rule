package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.collector.service.GetSumInsureService;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OccupationMotorcycleService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(OccupationMotorcycleService.class);
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private MessageService messageService;

	@Autowired
	private GetSumInsureService getSumInsureService;
	
	

//	@Autowired
//	private GetCollectorHistoryInsure getCollectorHistoryInsure;

	List<Message> message = null;

	List<HistoryInsure> historyDetails = null;

	public OccupationMotorcycleService(UnderwriteRequest input, CollectorModel collecter, List<HistoryInsure> h) {
		super(input, collecter);
		historyDetails = h;
	}

	@Override
	public void initial() {
		message = new ArrayList<Message>();
	}

	@Override
	public boolean preAction() {
		log.info("PreAction Occupation_Motorcycle");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute Occupation_Motorcycle");
		
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		String occupationCode = input.getRequestBody().getPersonalData().getOccupationCode();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> hsPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> a1PlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		
		for (InsureDetail temp : currentPlanList) {
			String tempPlanType = temp.getPlanHeader().getPlanType();

			if (RuleConstants.RULE_03.HB.equals(tempPlanType)) {
				hsPlanList.add(temp);
			}
			if (RuleConstants.RULE_03.A1.equals(tempPlanType)) {
				a1PlanList.add(temp);
			}

		}
//		BigDecimal sumInsure = getSumInsureService.getSumInsuredByPlanType("HB", historyInsureList, RuleConstants.SYSTEM_NORMAL);
		BigDecimal sumInsureHS = sumCurrentAndHist(historyInsureList,hsPlanList, RuleConstants.RULE_03.HS);
		BigDecimal sumInsureA1 = sumCurrentAndHist(historyInsureList,a1PlanList, RuleConstants.RULE_03.A1);
		/*
		 * 1. Check Occuation Code and Input Is Not Null
		 */
		if (StringUtils.isBlank(occupationCode)) {
			// end process
			return;
		}

		/*
		 * 2. Check Plan Type and History Insure > 300
		 */
		if (RuleConstants.RULE_11.MESSAGE_CODE_016002.equals(occupationCode)) {

//			List<InsureDetail> insureDetails = input.getRequestBody().getRiderInsureDetails();
			if (sumInsureHS.compareTo(new BigDecimal(300)) > 0 && RuleUtils.checkType("HB", currentPlanList)) {
				List<String> msgparam = Arrays.asList("300");
				setMessage("DT11004", msgparam);
			} else {
				checkPlanTypeA2A3(currentPlanList ,sumInsureA1 , basic);
			}
		}
		
	}

	/*
	 * 3. Check Plan Type is “A2,A3”
	 */
	private void checkPlanTypeA2A3(List<InsureDetail> insureDetails, BigDecimal sumInsureA1, InsureDetail basic) {
		if (RuleUtils.checkType("A2,A3", insureDetails)) {
			setMessage("DT11002");
		} else {
			checkPlanTypeA1(sumInsureA1,basic);
		}
	}

	/*
	 * 4. Check Plan Type is “A1” and Sum Insured > Main Insured
	 */
	private void checkPlanTypeA1(BigDecimal sumInsureA1, InsureDetail basicsumInsureA1) {

//		BigDecimal sumInsureA1 = getSumInsureService.getSumInsuredByPlanType("A1" , historyDetails, RuleConstants.SYSTEM_NORMAL);
		if (sumInsureA1.compareTo(new BigDecimal(input.getRequestBody().getBasicInsureDetail().getInsuredAmount())) > 0) {
			setMessage("DT11003");
		}
	}

	/*
	 * 5. Message Mapping
	 */
	private void setMessage(String messageCode) {
		Message msg = messageService.getOneMessage(registersystem, messageCode);
		message.add(msg);
	}

	private void setMessage(String messageCode, List<String> param) {
		Message msg = messageService.getMessage(registersystem, messageCode, param, param);
		message.add(msg);
	}

	@Override
	public void finalAction() {
		log.info("FinalAction Occupation_Motorcycle");
		if (message.size() > 0) {
			super.setMessageList(message);
		}
	}

	
	private BigDecimal sumCurrentAndHist(List<HistoryInsure> historyInsureList, List<InsureDetail> healthPlanList,
			String planType) {
		BigDecimal allSumInsuredCurr;
		allSumInsuredCurr = sumCurrentPlanType(healthPlanList);
		BigDecimal allSumInsuredHist = getSumInsureService.getSumHistoryInsureByPlanType(historyInsureList, planType);
		allSumInsuredCurr = allSumInsuredCurr.add(allSumInsuredHist);

		return allSumInsuredCurr;
	}
	
	private BigDecimal sumCurrentPlanType(List<InsureDetail> healthPlanList) {
		BigDecimal allSumInsured = BigDecimal.ZERO;
		for (InsureDetail temp : healthPlanList) {
			BigDecimal insured = new BigDecimal(temp.getInsuredAmount());
			allSumInsured = allSumInsured.add(insured);
		}
		return allSumInsured;
	}
}
