package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AdditionalInformationService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(AdditionalInformationService.class);

	@Autowired
	private MessageService messageService;

	AdditionalInformationService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		String aditionalHealth = input.getRequestBody().getPersonalData().getAdditionalInformation();
		if (StringUtils.isNotBlank(aditionalHealth)) {
			String registersystem = input.getRequestHeader().getRegisteredSystem();
			Message msg = messageService.getOneMessage(registersystem, "DT11304");
			messageList.add(msg);
		}
	}

	@Override
	public void finalAction() {
	}

}
