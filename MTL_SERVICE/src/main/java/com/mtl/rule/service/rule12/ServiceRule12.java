package com.mtl.rule.service.rule12;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
 
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule12 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule12.class); 
	String state = "";
	Timmer timeUsage = new Timmer();
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>(); 
	
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private MessageService messageService;
	public ServiceRule12(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {		
	}

	@Override
	public boolean preAction() {

		return true;
	}

	@Override
	public void execute() {
		PersonalData personalData = input.getRequestBody().getPersonalData();
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		String planCode = basic.getPlanCode();
		
		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE 12:  ประวัติการเคลม ";	 
		state  = state+"\n"+RuleConstants.RULE_SPACE+"INPUT";	
		state  = state+"\n"+RuleConstants.RULE_SPACE+"[ idCard:"+personalData.getIdCard()+", planCode:"+planCode+" ]";
		
		state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Check Except Claim";
		String key = planCode+"|";
		PlanPermission except = applicationCache.getPlanPermissionAppCashMap().get(key);
		
		boolean checkExcept = false;
		if(except != null) {
			if(RuleConstants.FALSE.equals(except.getExceptPBClaim())) {
				checkExcept = true;
			}else {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => End process";
			}
		}else {
			checkExcept = true;
		}
		
		if(checkExcept){
			state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Go to #3";
			state  = state+"\n"+RuleConstants.RULE_SPACE+"#3 Check Flag Claim";
			if(!personalData.isFlagClaim()) {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => End Process";
			}else {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Go to #4";
				String[] personClaimCodeList =input.getRequestBody().getPersonalData().getClaimCodeList();
				
				if(personClaimCodeList!=null&&personClaimCodeList.length>0) {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#4 Check Exception Claim DT12_EXCEPTION_CLAIM_CODE , DT12_CLAIM_HISTORY ";
					
					int loop =0;
					for (String strTemp : personClaimCodeList){
						loop++;
		 				
						String exceptionClaimCode = applicationCache.getExceptionClaimCodeMap().get(strTemp); 
						state  = state+"\n"+RuleConstants.RULE_SPACE+"  Loop "+loop+": ClaimCode: "+strTemp;
						
						if(StringUtils.isNotBlank(exceptionClaimCode)) {
							state  = state+"\n"+RuleConstants.RULE_SPACE+"	Found in DT12_EXCEPTION_CLAIM_CODE --> Skip";					
						}else {
							state  = state+"\n"+RuleConstants.RULE_SPACE+"	Not Found in DT12_EXCEPTION_CLAIM_CODE --> Go to #4";
							state  = state+"\n"+RuleConstants.RULE_SPACE+"	#4 CHECK DT12_CLAIM_HISTORY";
							String referClaimStatus = applicationCache.getClaimCodeHistoryMap().get(strTemp);
							if(referClaimStatus==null || RuleConstants.FALSE.equals(referClaimStatus)) {
								
								state  = state+"\n"+RuleConstants.RULE_SPACE+"		Found in DT12_CLAIM_HISTORY --> DT12001";
								RuleResultBean ruleResultBeanTmp = new RuleResultBean();
								ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT12001);
								List <String> stringInMessageList = new ArrayList<String>();
								stringInMessageList.add(strTemp); 
								ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);					
								ruleResultBeanList.add(ruleResultBeanTmp);						
								
							}else {
								state  = state+"\n"+RuleConstants.RULE_SPACE+"		Not Found in DT12_CLAIM_HISTORY --> Skip";
							}
						}
						state  = state+"\n"+RuleConstants.RULE_SPACE;		
					}
				}else {
					
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	Not Found ClaimCode -->END ";	
					RuleResultBean ruleResultBeanTmp = new RuleResultBean();
					ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT1200Y); 
					ruleResultBeanList.add(ruleResultBeanTmp);	
				}
			}
		}
		
 
	}

	@Override
	public void finalAction() {
		String registersystem =input.getRequestHeader().getRegisteredSystem();
		// DT12001 Rule 12 : ผู้ขอเอาประกันภัย พบประวัติเคลม $ ผ่านไม่ได้		
		if(ruleResultBeanList!=null) {
			for(RuleResultBean ruleResultTmp:ruleResultBeanList) {	  
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode();
					Message message1 = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message1);		 
			}
		}
		state  = state+"\n"+"Time usage in Rule 12 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END EXECUTE RULE 12 ";
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
 }
}

/*
 Question Rule 12
1.  
2.  การแสดงผลลัพธ์กรณีที่ Claim Code มีสถานะ Requirement ทาง UW จะส่งตาราง Excel Mapping ให้กับทางบริษัทฯเพื่อใช้ในการ Setup
3.	เอกสาร Matrix Mapping สำหรับขอเอกสาเพิ่มเติมสำหรับ Code MIB (ทางทีม Underwrite)
4.	เอกสาร Matrix Mapping สำหรับ Exclude Plan Code ที่ไม่ต้องการให้ตรวจสอบ Rule 12 ( ทางทีม Underwrite จะส่งให้ภายในวันศุกร์ที่ 31 มกราคม 2020)

 */
