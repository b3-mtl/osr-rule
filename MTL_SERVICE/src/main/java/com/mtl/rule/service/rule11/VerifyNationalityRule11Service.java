package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11PremiumPlus;
import com.mtl.rule.model.DT11VerifyNationUSA;
import com.mtl.rule.model.DT11VerifyNationWithPlan;
import com.mtl.rule.model.DT11VerifyNationaRider;
import com.mtl.rule.model.DT11VerifyNationality;
import com.mtl.rule.repository.Dt11VerifyDao;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VerifyNationalityRule11Service extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(VerifyNationalityRule11Service.class);

	@Autowired
	private Dt11VerifyDao dt11VerifyDao;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	public VerifyNationalityRule11Service(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		log.info("PreAction Verify_Personality");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute Verify_Personality");
		/*
		 * 1.
		 */
		String healthQuestion = input.getRequestBody().getPersonalData().getHealthQuestion();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		if (StringUtils.isBlank(healthQuestion)) {
			return;
		}

		/*
		 * 2.
		 */
//		String occupationCode = input.getRequestBody().getPersonalData().getOccupationCode();
//		DT11PremiumPlus premiumPlus = applicationCache.getRule11_premiumPlus().get(occupationCode);
//		if (premiumPlus == null) {
//			return;
//		}

		Message msg = null;
		List<String> param = new ArrayList<String>();
		String nationality = input.getRequestBody().getPersonalData().getNationality();
		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		List<InsureDetail> planCodeRider = input.getRequestBody().getRiderInsureDetails();
		BigDecimal insureAmount = new BigDecimal(input.getRequestBody().getBasicInsureDetail().getInsuredAmount());

		/*
		 * 3.
		 */
		healthQuestion = healthQuestion.trim();
		if(healthQuestion.equals("Pending Answer")){
			nationality = null;
		}
		List<DT11VerifyNationality> dt11VerifyNtList = applicationCache.getRule11_verifyNotinalrity().get(healthQuestion + "|" + nationality);
//		List<DT11VerifyNationality> dt11VerifyNttemp = new ArrayList<DT11VerifyNationality>();
		if (dt11VerifyNtList == null) {
			dt11VerifyNtList = applicationCache.getRule11_verifyNotinalrity().get(healthQuestion + "|" + RuleConstants.OTHERWISE);
			
//			for (DT11VerifyNationality verifyNationalityList : dt11VerifyNtList) {
//				insureAmount = 
//				
//				if(verifyNationalityList.getInsured() != null || RuleConstants.OTHERWISE.equals(verifyNationalityList.getInsured())){
//					verifyNationalityList.getInsured() ;
//				};
//			}
			
			
		}
		if(healthQuestion.equals("Pending Answer")){
			nationality = input.getRequestBody().getPersonalData().getNationality();
		}
		if (nationality == null && dt11VerifyNtList != null) {
			
			String idCard = input.getRequestBody().getPersonalData().getIdCard();
			String lengthIDCard = idCard != null ? Integer.toString(idCard.length()) : "0";
			boolean hasFound = false;
			
			// Check VerifyNtList
			for (DT11VerifyNationality dt11VerifyNt : dt11VerifyNtList) {
				
				
			if(dt11VerifyNt.getLengthIdCard()!=null && !RuleConstants.OTHERWISE.equals(dt11VerifyNt.getLengthIdCard()) )	
				if (lengthIDCard.equals(dt11VerifyNt.getLengthIdCard())) {
					hasFound = true;
//					if (dt11VerifyNt.getMessageCode() != null) {
//						msg = messageService.getMessage(registersystem, dt11VerifyNt.getMessageCode(),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()));
//						messageList.add(msg);
//						String[] docCode = dt11VerifyNt.getAddDocument().split(",");
//						for(String code :docCode) {
//							Document obj = applicationCache.getDocumentCashMap().get(code);
//							if(obj != null) {
//								documentList.add(obj);
//							}
//						}
//					}
					break;
				}
			}
			
			
			if (!hasFound) {
				dt11VerifyNtList = dt11VerifyNtList.stream()
						.filter(item -> RuleConstants.OTHERWISE.equals(item.getLengthIdCard()))
						.collect(Collectors.toList());
				
				boolean hasCorrectInsured = false;
				for (DT11VerifyNationality dt11VerifyNt : dt11VerifyNtList) {
					if (RuleConstants.OTHERWISE.equals(dt11VerifyNt.getInsured())) {
						continue;
					}
					if (RuleUtils.andLessthanNumber(insureAmount, new BigDecimal(dt11VerifyNt.getInsured()))) {
						hasCorrectInsured = true;
						if (dt11VerifyNt.getMessageCode() != null) {
							msg = messageService.getMessage(registersystem, dt11VerifyNt.getMessageCode(),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()));
							messageList.add(msg);
							String[] docCode = dt11VerifyNt.getAddDocument().split(",");
							for(String code :docCode) {
								Document obj = applicationCache.getDocumentCashMap().get(code);
								if(obj != null) {
									documentList.add(obj);
								}
							}
						}
					}
				}
				if (!hasCorrectInsured) {
					for (DT11VerifyNationality dt11VerifyNt : dt11VerifyNtList) {
						if (RuleConstants.OTHERWISE.equals(dt11VerifyNt.getInsured())) {
							if (dt11VerifyNt.getMessageCode() != null) {
								msg = messageService.getMessage(registersystem, dt11VerifyNt.getMessageCode(),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()));
								messageList.add(msg);
								String[] docCode = dt11VerifyNt.getAddDocument().split(",");
								for(String code :docCode) {
									Document obj = applicationCache.getDocumentCashMap().get(code);
									if(obj != null) {
										documentList.add(obj);
									}
								}
							}
							break;
						}
					}
				}
			}
			
			
			
			
		}
		if (dt11VerifyNtList != null) {
			// check NOT_NATIONALITY
			boolean isNotNationality = true;
			for (DT11VerifyNationality item : dt11VerifyNtList) {
				if (item.getNotNationality() != null) {
					List<String> notNationalityList = Arrays.asList(item.getNotNationality().split(","));
					for (String notNationality : notNationalityList) {
						if (notNationality.equals(nationality)) {
							isNotNationality = false;
						}
					}
				}
			}
			
			if (isNotNationality) {
				boolean hasCorrectInsured = false;
				for (DT11VerifyNationality dt11VerifyNt : dt11VerifyNtList) {
					if (RuleConstants.OTHERWISE.equals(dt11VerifyNt.getInsured()) || dt11VerifyNt.getInsured() ==null) {
						continue;
					}
					if (RuleUtils.andMorethanNumber(insureAmount, new BigDecimal(dt11VerifyNt.getInsured()))) {
						hasCorrectInsured = true;
						if (dt11VerifyNt.getMessageCode() != null) {
							msg = messageService.getMessage(registersystem, dt11VerifyNt.getMessageCode(),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()));
							messageList.add(msg);
							String[] docCode = dt11VerifyNt.getAddDocument().split(",");
							for(String code :docCode) {
								Document obj = applicationCache.getDocumentCashMap().get(code);
								if(obj != null) {
									documentList.add(obj);
								}
							}
						}
					}
				}
				if (!hasCorrectInsured) {
					for (DT11VerifyNationality dt11VerifyNt : dt11VerifyNtList) {
						if(dt11VerifyNt.getInsured()!=null){
						if (RuleConstants.OTHERWISE.equals(dt11VerifyNt.getInsured())) {
								if (dt11VerifyNt.getMessageCode() != null) {
									msg = messageService.getMessage(registersystem, dt11VerifyNt.getMessageCode(),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()),Arrays.asList(nationality,dt11VerifyNt.getAddDocument()));
									messageList.add(msg);
									String[] docCode = dt11VerifyNt.getAddDocument().split(",");
									for(String code :docCode) {
										Document obj = applicationCache.getDocumentCashMap().get(code);
										if(obj != null) {
											documentList.add(obj);
										}
									}
								}
								break;
							}
						}
					}
				}
			}
		}

		/*
		 * 4.
		 */
		//Check Have
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		for (InsureDetail insureDetail : currentPlanList) {
			DT11VerifyNationWithPlan nationPlan = applicationCache.getRule11_listVerifyNationWPlan().get(insureDetail.getPlanCode());
//			System.out.println("Bave");
		}
		DT11VerifyNationWithPlan nationPlan = applicationCache.getRule11_listVerifyNationWPlan().get(planCode);
		if (nationPlan == null) {
			nationPlan = applicationCache.getRule11_listOtwVerifyNationWPlan().get(planCode);
		}
		if (nationPlan != null) {
			if (nationPlan.getMessageCode() != null) {
				msg = messageService.getOneMessage(registersystem, nationPlan.getMessageCode());
				messageList.add(msg);
			}
		}

		/*
		 * 5.
		 */
		//nationality
		if(planCodeRider!=null && !planCodeRider.isEmpty()){
			List<DT11VerifyNationaRider> nationRider = dt11VerifyDao.getDt11VerifyNationalityForRider();
			for (DT11VerifyNationaRider itemRider : nationRider) {
				
				boolean checkBreak = false ;
				if(StringUtils.isNotBlank(itemRider.getNationality())){
					if (itemRider.getNationality().contains(nationality)) {
						checkBreak = true ;
						
						for(InsureDetail tempRider : planCodeRider)
						{
							String  PlanCode ="_";
							String  NotPlanCode = "_";						
							if(StringUtils.isNotBlank(itemRider.getPlanCode())){
								PlanCode = itemRider.getPlanCode().toString();
							}
							if(StringUtils.isNotBlank(itemRider.getNotPlanCode())){

								NotPlanCode = itemRider.getNotPlanCode().toString();
							}
							
							if(PlanCode.contains(tempRider.getPlanCode())){
								
								//get MC
								if (StringUtils.isNotBlank(itemRider.getMessageCode())) {
									
									
									param = new ArrayList<String>();
									param.add(nationality);
									param.add(tempRider.getPlanCode());
									param.add(planCode);
									
									msg = messageService.getMessage(registersystem, itemRider.getMessageCode(), param, param);
									messageList.add(msg);
								}
							}else if(NotPlanCode.contains(tempRider.getPlanCode())){
								
								//get MC
								if (StringUtils.isNotBlank(itemRider.getMessageCode())) {

									param = new ArrayList<String>();
									param.add(nationality);
									param.add(tempRider.getPlanCode());
									param.add(planCode);
									
									msg = messageService.getMessage(registersystem, itemRider.getMessageCode(), param, param);
									messageList.add(msg);
								}
							}
						}
					}else if(checkBreak){
						
							for(InsureDetail tempRider : planCodeRider)
							{ 
								String  PlanCode ="_";
								String  NotPlanCode = "_";						
								if(StringUtils.isNotBlank(itemRider.getPlanCode())){
									PlanCode = itemRider.getPlanCode().toString();
								}
								if(StringUtils.isNotBlank(itemRider.getNotPlanCode())){
									NotPlanCode = itemRider.getNotPlanCode().toString();
								}
								
								if(PlanCode.contains(tempRider.getPlanCode())){
									//get MC
									if (StringUtils.isNotBlank(itemRider.getMessageCode())) {
										param = new ArrayList<String>();
										param.add(nationality);
										param.add(tempRider.getPlanCode());
										param.add(planCode);
										
										msg = messageService.getMessage(registersystem, itemRider.getMessageCode(), param, param);
										messageList.add(msg);
									}
								}else if(NotPlanCode.contains(tempRider.getPlanCode())){
									//get MC
									if (StringUtils.isNotBlank(itemRider.getMessageCode())) {
										param = new ArrayList<String>();
										param.add(nationality);
										param.add(tempRider.getPlanCode());
										param.add(planCode);
										
										msg = messageService.getMessage(registersystem, itemRider.getMessageCode(), param, param);
										messageList.add(msg);
									}
								}
							
						}
	
					}
				}
			
			}
		}

		/*
		 * 6. Todo Check USA Invest
		 */
		DT11VerifyNationUSA nationUsa = applicationCache.getRule11_verifyNationalityUSA().get(nationality);
		if (nationUsa != null) {
			
			for (InsureDetail insureDetail : currentPlanList) {
				if("investment".equalsIgnoreCase(insureDetail.getPlanHeader().getPlanSystem()) ){
					msg = messageService.getOneMessage(registersystem, nationUsa.getMessageCode());
					messageList.add(msg);
				}
			}
		
			
		
		}
	}

	@Override
	public void finalAction() {
		log.info("FinalAction Verify_Personality");
	}

}
