package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckHealth;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03DHealthAgentGroup;
import com.mtl.rule.model.DT03HealthSuminsured;
import com.mtl.rule.model.DT03HealthSuminsuredInclude;
import com.mtl.rule.model.DT03RiskDiagnosisPlanType;
import com.mtl.rule.model.DT11OccuLevPlanCode;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03HealthService extends AbstractSubRuleExecutor2 {

	@Autowired
	private GetSumInsureService sumInsureService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private CheckHealth checkHealth;
	
	@Autowired
	private ApplicationCache applicationCache;

	String state = "";
	boolean result = false;

	public Rule03HealthService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 ตรวจสอบแบบประกันภัยสุขภาพ HS HB Health";

		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		String ageDay = input.getRequestBody().getPersonalData().getAgeDay();
		String ageMonth = input.getRequestBody().getPersonalData().getAgeInMonth();
		String ageYear = input.getRequestBody().getPersonalData().getAge();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		AgentInfo agentInfo = collectorModel.getAgentDetail();
		String agentGroup = agentInfo.getAgentGroup();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> healthPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> hsPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> hmPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> hbPlanList = new ArrayList<InsureDetail>();
		
		List<InsureDetail> hsPlanCodeList = new ArrayList<InsureDetail>();
		List<InsureDetail> hmPlanCodeList = new ArrayList<InsureDetail>();
		List<InsureDetail> hbPlanCodeList = new ArrayList<InsureDetail>();
		
		List<InsureDetail> planCodeInList = new ArrayList<InsureDetail>();
		
		BigDecimal SumInsuredInHS = BigDecimal.ZERO;
		BigDecimal SumInsuredInHM = BigDecimal.ZERO;
		BigDecimal SumInsuredInHB = BigDecimal.ZERO;
		BigDecimal allSumInsuredByFixedEnd  = BigDecimal.ZERO;
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		if(ageMonth.isEmpty()){
			ageMonth="0";
		}
		
		List<InsureDetail> currentHitPlanList = new ArrayList<InsureDetail>();
		
//		currentHitPlanList.addAll(currentPlanList);
		if(historyInsureList != null){
			for (HistoryInsure insureDetail : historyInsureList) {
				PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(insureDetail.getPlanCode().trim());
				
				InsureDetail obj = new InsureDetail();
				obj.setPlanCode(insureDetail.getPlanCode());
//				MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
//				if(planTypeInvestMent != null) {
//				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				if("F".equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null){
					obj.setInsuredAmount(insureDetail.getFaceAmount().toString());
				}else {
					obj.setInsuredAmount(insureDetail.getSumInsure().toString());
				}
				if(obj.getInsuredAmount()==null){
					obj.setInsuredAmount("0");
				}
				obj.setPlanHeader(hdr);
				obj.setServiceBranch(insureDetail.getServiceBrach());
				obj.setChannelCode(insureDetail.getChannelCode());
				currentHitPlanList.add(obj);
				
			}
		}
		
	
		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Health Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planType = temp.getPlanHeader().getPlanType();
			String planCode = temp.getPlanHeader().getPlanCode();
			
			if (StringUtils.isNoneBlank(planType)) {
				if (checkHealth.checkValueInParameter(RuleConstants.RULE_03.HEALTH_PLAN_TYPE, planType)) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] is Health Plan";
					healthPlanList.add(temp);
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] not Health Plan";
				}
			}
			
			if (StringUtils.isNoneBlank(planCode)) {
				if (checkHealth.checkValueInParameter(RuleConstants.RULE_03.HEALTH_BENEFIT_DISTRIBUTED, planCode)) {
					healthPlanList.add(temp);
					hsPlanCodeList.add(temp);
				} 
				if (checkHealth.checkValueInParameter(RuleConstants.RULE_03.HEALTH_BENEFIT_FIXED, planCode)) {			
					healthPlanList.add(temp);
					hbPlanCodeList.add(temp);
				} 
				if (checkHealth.checkValueInParameter(RuleConstants.RULE_03.HEALTH_BENEFIT_SMART, planCode)) {			
					healthPlanList.add(temp);
					hmPlanCodeList.add(temp);
				}
				

					
					List<DT03HealthSuminsuredInclude> suminsuredInclude = applicationCache.getRule03_dt03HealthSumInClude();
					for (DT03HealthSuminsuredInclude dt03HealthSuminsuredInclude : suminsuredInclude) {
						if(dt03HealthSuminsuredInclude.getPlanCode().contains(planCode)){
							if(dt03HealthSuminsuredInclude.getInsured()!=null){
								
								BigDecimal num1 =new BigDecimal(dt03HealthSuminsuredInclude.getInsured());
								BigDecimal num2 =new BigDecimal( temp.getInsuredAmount());
								if(  num1.compareTo(num1) == 0 ){
//								if(dt03HealthSuminsuredInclude.getInsured().equalsIgnoreCase(temp.getInsuredAmount())){
									//ถึงจะบวก

									healthPlanList.add(temp);
									hmPlanCodeList.add(temp);
									
									if (RuleConstants.RULE_03.HS.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
										SumInsuredInHS = SumInsuredInHS.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
									}

									if (RuleConstants.RULE_03.HM.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
										SumInsuredInHM = SumInsuredInHM.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
									}

									if (RuleConstants.RULE_03.HB.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
										SumInsuredInHB = SumInsuredInHB.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
									}
								}
							}else{
								//เลย
								healthPlanList.add(temp);
								hmPlanCodeList.add(temp);
								
								if (RuleConstants.RULE_03.HS.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHS = SumInsuredInHS.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}

								if (RuleConstants.RULE_03.HM.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHM = SumInsuredInHM.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}

								if (RuleConstants.RULE_03.HB.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHB = SumInsuredInHB.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}
								
							}
						}
						
					}
				
		
			}

		}
		
		//currentHitPlanList


		

		if (healthPlanList.size() > 0) {
			
			for (InsureDetail temp : currentHitPlanList) {
				/* DT03_HEALTH_SUM_INCLUDE */
				String planType = temp.getPlanHeader().getPlanType();
				String planCode = temp.getPlanHeader().getPlanCode();
				
				List<DT03HealthSuminsuredInclude> suminsuredInclude = applicationCache.getRule03_dt03HealthSumInClude();
				for (DT03HealthSuminsuredInclude dt03HealthSuminsuredInclude : suminsuredInclude) {
					if(dt03HealthSuminsuredInclude.getPlanCode().contains(planCode)){
						if(dt03HealthSuminsuredInclude.getInsured()!=null){
							
							BigDecimal num1 =new BigDecimal(dt03HealthSuminsuredInclude.getInsured());
							BigDecimal num2 =new BigDecimal( temp.getInsuredAmount());
							if(  num1.compareTo(num1) == 0 ){
								//ถึงจะบวก

								if (RuleConstants.RULE_03.HS.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHS = SumInsuredInHS.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}

								if (RuleConstants.RULE_03.HM.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHM = SumInsuredInHM.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}

								if (RuleConstants.RULE_03.HB.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
									SumInsuredInHB = SumInsuredInHB.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
								}
							}
						}else{
							//เลย
							
							if (RuleConstants.RULE_03.HS.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
								SumInsuredInHS = SumInsuredInHS.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
							}

							if (RuleConstants.RULE_03.HM.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
								SumInsuredInHM = SumInsuredInHM.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
							}

							if (RuleConstants.RULE_03.HB.equals(dt03HealthSuminsuredInclude.getPlanTypeGroup())) {
								SumInsuredInHB = SumInsuredInHB.add(new BigDecimal(dt03HealthSuminsuredInclude.getSuminsuredInclude()));
							}
							
						}
					}
					
				}
			}
			
			
			BigDecimal allSumInsuredBasic = new BigDecimal(basic.getInsuredAmount());
			allSumInsuredBasic = allSumInsuredBasic.add(sumInsureService.getSumHistoryInsureBasic(historyInsureList));
			BigDecimal allSumInsuredDISTR = BigDecimal.ZERO;
			BigDecimal allSumInsuredByHealth = BigDecimal.ZERO;
			BigDecimal allSumInsuredByFixed = BigDecimal.ZERO;
			state = state + "\n" + RuleConstants.RULE_SPACE + "#2 Prepare Data : SumInsured ";
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.1 SumInsured Current (Current + History) ";
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Basic) = " + allSumInsuredBasic;
			extractedPlanType(healthPlanList,hsPlanList,hmPlanList,hbPlanList);
			
			allSumInsuredDISTR = sumCurrentAndHist(historyInsureList,hsPlanList, RuleConstants.RULE_03.HS);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (DISTR) = " + allSumInsuredDISTR;
			
			allSumInsuredByHealth = sumCurrentAndHist(historyInsureList,hmPlanList, RuleConstants.RULE_03.HM);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Health) = " + allSumInsuredByHealth;
			
			allSumInsuredByFixed = sumCurrentAndHist(historyInsureList,hbPlanList, RuleConstants.RULE_03.HB);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Fixed) = " + allSumInsuredByFixed;
			
			for (InsureDetail insureDetail : hsPlanCodeList) {
				allSumInsuredDISTR = allSumInsuredDISTR.add(new BigDecimal(insureDetail.getInsuredAmount()));
			}
			for (InsureDetail insureDetail : hmPlanCodeList) {
				allSumInsuredByHealth = allSumInsuredByHealth.add(new BigDecimal(insureDetail.getInsuredAmount()));
			}
			for (InsureDetail insureDetail : hbPlanCodeList) {
				allSumInsuredByFixed = allSumInsuredByFixed.add(new BigDecimal(insureDetail.getInsuredAmount()));
			}
			
//			for (InsureDetail insureDetail : planCodeInList) {
				allSumInsuredDISTR = allSumInsuredDISTR.add(SumInsuredInHS);
				allSumInsuredByHealth  = allSumInsuredByHealth.add(SumInsuredInHM);
				allSumInsuredByFixed  = allSumInsuredByFixed.add(SumInsuredInHB);
//			}
				allSumInsuredByFixedEnd =	allSumInsuredByFixed;
			state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Set Message";
			checkHealthSuminsured(agentGroup, ageDay ,  ageMonth, ageYear, allSumInsuredBasic, allSumInsuredDISTR, allSumInsuredByHealth, allSumInsuredByFixed ,registersystem);
			
			
			//DT03_D_Health_AgentGroup
			
			List<DT03DHealthAgentGroup> dHealthAgentGroupList = applicationCache.getRule03_dT03DHealthAgentGroup();
			for (DT03DHealthAgentGroup dt03dHealthAgentGroups : dHealthAgentGroupList) {
				if(isOneOf(dt03dHealthAgentGroups.getAgentGroup(), agentGroup)){
					if(allSumInsuredByFixed.compareTo( BigDecimal.ZERO)> 0){
						if(oneInOf( dt03dHealthAgentGroups.getPlanCode() , currentPlanList)){
							//Get MessageCode DT03516
							List<String> param = new ArrayList<String>();
							//Todo  DT03068 ยังไม่มีในระบบ
							param.add(agentGroup);
							Message message = messageService.getMessage(registersystem, dt03dHealthAgentGroups.getMessageCode() , param, param);
							messageList.add(message);
						}
						
					}
				}
			}
			
		
			
			
			//			for(String code : msgList) {
//				state = state + "\n" + RuleConstants.RULE_SPACE + "	Message Code = " + code;
//				Message message = messageService.getOneMessage(registersystem, code);
//				messageList.add(message);
//			}
			
			
			
			

		} else {
			state = state + "\n" + RuleConstants.RULE_SPACE + "	#1 Result : Not found Health Plan ---> End process";
		}
		
		
//		if(messageList.size()==0){

			if("WTN05E".equals(basic.getPlanHeader().getPlanCode()) && allSumInsuredByFixedEnd.compareTo(new BigDecimal("1000") )>0 ){
				
				Message message = messageService.getMessage(registersystem, "DT03529"  , Arrays.asList("1000"), Arrays.asList("1000"));
				messageList.add(message);
			}
			
			if("TON15Q,TON15L,TON15J,TON15U,TON15W,TON15Y".contains(basic.getPlanHeader().getPlanCode()) && allSumInsuredByFixedEnd.compareTo(new BigDecimal("5000") )>0 ){
				
				Message message = messageService.getMessage(registersystem, "DT03522"  , Arrays.asList("5000",allSumInsuredByFixedEnd.toString()), Arrays.asList("5000",allSumInsuredByFixedEnd.toString()));
				messageList.add(message);
			}
//		}
		
	}

	@Override
	public void finalAction() {
		state = state + "\n" + "END Rule 03 ตรวจสอบแบบประกันภัยสุขภาพ HS HB Health";
		state = state + "\n" + "]";
		setSubRuleLog(state);
	}
	
	private boolean isOneOf(String source,String target){
		if(source!=null && (!source.isEmpty())){
			source = ","+source.trim()+",";
			target = ","+target.trim()+",";
			int i = source.indexOf(target);
			if(i!=-1){
				return true;
			}
		}
		return false;
	}
	
	public boolean oneInOf(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	

	private BigDecimal sumCurrentAndHist(List<HistoryInsure> historyInsureList, List<InsureDetail> healthPlanList,
			String planType) {
		BigDecimal allSumInsuredCurr;
		allSumInsuredCurr = sumCurrentPlanType(healthPlanList);
//		if(allSumInsuredCurr != BigDecimal.ZERO){
			BigDecimal allSumInsuredHist = sumInsureService.getSumHistoryInsureByPlanType(historyInsureList, planType);
			allSumInsuredCurr = allSumInsuredCurr.add(allSumInsuredHist);
//		}
		return allSumInsuredCurr;
	}

	private BigDecimal sumCurrentPlanType(List<InsureDetail> healthPlanList) {
		BigDecimal allSumInsured = BigDecimal.ZERO;
		for (InsureDetail temp : healthPlanList) {
			BigDecimal insured = new BigDecimal(temp.getInsuredAmount());
			allSumInsured = allSumInsured.add(insured);
		}
		return allSumInsured;
	}

	private void extractedPlanType(List<InsureDetail> accidentPlanList, List<InsureDetail> hsPlanList,
			List<InsureDetail> hmPlanList, List<InsureDetail> hbPlanList) {
		for (InsureDetail temp : accidentPlanList) {
			String tempPlanType = temp.getPlanHeader().getPlanType();
			if (RuleConstants.RULE_03.HS.equals(tempPlanType)) {
				hsPlanList.add(temp);
			}

			if (RuleConstants.RULE_03.HM.equals(tempPlanType)) {
				hmPlanList.add(temp);
			}

			if (RuleConstants.RULE_03.HB.equals(tempPlanType)) {
				hbPlanList.add(temp);
			}
			
			
			
			
		}
	}

	
	public List<String> checkHealthSuminsured(String agentGroup, String ageDay, String ageMonth, String ageYear,BigDecimal allSumInsuredBasic, BigDecimal allSumInsuredDISTR, BigDecimal allSumInsuredByHealth, BigDecimal allSumInsuredByFixed, String registersystem){
		List<DT03HealthSuminsured> valueList = applicationCache.getRule03_dt03HealthSuminsured().get(agentGroup);
		List<DT03HealthSuminsured> tempList = checkHealth.fillterData(valueList,allSumInsuredBasic,ageDay,ageMonth,ageYear);
		List<String> msg = new ArrayList<String>();
		
//		
		
		if(agentGroup == null){
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03508  , Arrays.asList(""), Arrays.asList(""));
			messageList.add(message);
			return null;
		}
		
		
		if(valueList!=null){
			
			if(tempList!=null) {
				
				boolean statusMaxSumFixedMix = false  	;
				boolean statusMaxSumDISTR  	 = false  	; 
				boolean statusMaxSumHealth 	 = false  	;
				boolean statusMaxSumFixed  	 = false  	; 
				boolean statusNotAge  	 	 = false  	;
				
				String msgSumFixedMix = "" ;
				String msgMaxSumDISTR = "" ;
				String msgMaxSumHealth = "" ;
				String msgMaxSumFixed = "" ;
				String msgstatusNotAge = "";
				
				String maxMaxSumDISTRMsg = "" ;
				String maxMaxSumHealthMsg = "" ;
				String maxMaxSumFixedMsg = "" ;
				String maxSumFixedMixMsg = "" ;
				String maxage = "";
				
				for(DT03HealthSuminsured temp:tempList) {
					
					String maxSumFixedMix = temp.getMaxSuminsuredFixedMix();
					String maxSumDISTR = temp.getMaxSuminsuredDistr();
					String maxSumHealth = temp.getMaxSuminsuredHealth();
					String maxSumFixed = temp.getMaxSuminsuredFixed();
					if(temp.getMaxAge() != null){
						maxage = temp.getMaxAge().toString();
					}
					
					
					if( maxSumHealth == null && maxSumDISTR == null  && maxSumFixed == null  && maxSumFixedMix == null) {
						statusNotAge = true;
						if(temp.getMessageCode()!=null){
							msgstatusNotAge = temp.getMessageCode();
						}
						
					}
					
					
					if(!RuleConstants.OTHERWISE.equals(maxSumDISTR) && maxSumDISTR != null  && !maxSumDISTR.equals("0") ) {
						statusMaxSumDISTR = allSumInsuredDISTR.compareTo(new BigDecimal(maxSumDISTR)) > 0 ;
						maxMaxSumDISTRMsg = maxSumDISTR;
					}else if(!RuleConstants.OTHERWISE.equals(maxSumDISTR) && maxSumDISTR != null && maxSumDISTR.equals("0")) {
						if((allSumInsuredDISTR.compareTo(BigDecimal.ZERO)> 0)){
							statusMaxSumDISTR = true;
							maxMaxSumDISTRMsg = maxSumDISTR;
							
						}
					}
					
					if(!RuleConstants.OTHERWISE.equals(maxSumHealth) && maxSumHealth != null && !maxSumHealth.equals("0") ) {
						statusMaxSumHealth = allSumInsuredByHealth.compareTo(new BigDecimal(maxSumHealth)) > 0	;
						maxMaxSumHealthMsg = maxSumHealth;
					}else if(!RuleConstants.OTHERWISE.equals(maxSumHealth) && maxSumHealth != null && maxSumHealth.equals("0")) {
						if((allSumInsuredByHealth.compareTo(BigDecimal.ZERO)> 0)){
							statusMaxSumHealth = true;
							maxMaxSumHealthMsg = maxSumHealth;
						}
					}
					
					if(!RuleConstants.OTHERWISE.equals(maxSumFixed) && maxSumFixed != null && !maxSumFixed.equals("0")) {
						statusMaxSumFixed = allSumInsuredByFixed.compareTo(new BigDecimal(maxSumFixed)) > 0 ;
						maxMaxSumFixedMsg = maxSumFixed;
						
						
						
					}else if(!RuleConstants.OTHERWISE.equals(maxSumFixed) && maxSumFixed != null && maxSumFixed.equals("0")) {
						if((allSumInsuredByFixed.compareTo(BigDecimal.ZERO)> 0)){
							statusMaxSumFixed = true;
							maxMaxSumFixedMsg = maxSumFixed;
//							List<String> param = new ArrayList<String>();
//							param.add(agentGroup);
//		
//							
//							Message message = messageService.getMessage(registersystem, "DT03502" , param, param);
//							messageList.add(message);
//							return null;
						}
					}
					
					if(!RuleConstants.OTHERWISE.equals(maxSumFixedMix) && maxSumFixedMix != null ) {
//						statusMaxSumFixedMix = new BigDecimal(maxSumFixedMix).compareTo(BigDecimal.ZERO) > 0 ;
						
						//มากกว่า คล้ายๆ ว่า
						//sumInsuredFixed <= MAX_SUMINSURED_FIXED_MIX pass;
						//sumInsuredFixed > MAX_SUMINSURED_FIXED_MIX DT03507;
					
							if((allSumInsuredDISTR.compareTo(BigDecimal.ZERO)> 0 || allSumInsuredByHealth.compareTo(BigDecimal.ZERO)> 0 )  && allSumInsuredByFixed.compareTo(BigDecimal.ZERO)> 0 ){
								//
								if(maxSumFixedMix.equals("0")){
									List<String> param = new ArrayList<String>();
									param.add(agentGroup);
				
									
									Message message = messageService.getMessage(registersystem, "DT03504" , param, param);
									messageList.add(message);
									return null;
								}
								
								if( allSumInsuredByFixed.compareTo(new BigDecimal(maxSumFixedMix)) > 0 ){
									List<String> param = new ArrayList<String>();
									param.add(maxSumDISTR);
									param.add(maxSumHealth);
									param.add(maxSumFixed);
									
									param.add(allSumInsuredDISTR.toString());
									param.add(allSumInsuredByHealth.toString());
									param.add(allSumInsuredByFixed.toString());
									param.add(agentGroup);
									Message message = messageService.getMessage(registersystem, "DT03507" , param, param);
									messageList.add(message);
									return null;
								}
							}
							
//						}else{
//							allSumInsuredByFixed.compareTo(maxSumFixed) > 0 
							
							 
//							sumInsuredFixed <= MAX_SUMINSURED_FIXED_MIX pass;
//							sumInsuredFixed > MAX_SUMINSURED_FIXED_MIX DT0350
							
//						}
						
						//เท่ากับ 0 หรือน้อยกว่า
						//if MAX_SUMINSURED_FIXED_MIX =0   
						//(sumInsuredDISTR >0 || sumInsuredHealth >0) && sumInsuredFixed >0
						//return DT03504;

						
						
//						if MAX_SUMINSURED_FIXED_MIX =0   
//								(sumInsuredDISTR >0 || sumInsuredHealth >0) && sumInsuredFixed >0
//								return DT03504;
						
						
						
						maxSumFixedMixMsg = maxSumFixedMix;
					}
					
					//Set Msg
					
					if(RuleConstants.OTHERWISE.equals(maxSumDISTR)){
						if(temp.getMessageCode()!=null){
							msgMaxSumDISTR =  temp.getMessageCode();
						}
					}
					
					if(RuleConstants.OTHERWISE.equals(maxSumHealth)){
						if(temp.getMessageCode()!=null){
							msgMaxSumHealth = temp.getMessageCode();
						}
					}
					
					if(RuleConstants.OTHERWISE.equals(maxSumFixed)){
						if(temp.getMessageCode()!=null){
							msgMaxSumFixed = temp.getMessageCode();
						}
					}
					
					if(RuleConstants.OTHERWISE.equals(maxSumFixedMix)){
						if(temp.getMessageCode()!=null){
							msgSumFixedMix = temp.getMessageCode();
						}
					}

				}
				
				//Todo
				if(statusNotAge){
					
					List<String> param = new ArrayList<String>();
					if(msgstatusNotAge.equals("DT03519")){
						param.add(agentGroup);
						param.add(maxage);
					}else{
						param.add(ageYear);
					}
//					param.add(maxMaxSumDISTRMsg);
//					param.add(allSumInsuredDISTR.toString());
					
					Message message = messageService.getMessage(registersystem, msgstatusNotAge , param, param);
					messageList.add(message);
					return null;
				}
				
				
				
				
				if(statusMaxSumFixedMix){
					
					List<String> param = new ArrayList<String>();
//					param.add(maxSumFixedMixMsg);
//					param.add(allSumInsuredDISTR.toString());
//					param.add(agentGroup);
					Message message = messageService.getMessage(registersystem, msgSumFixedMix , param, param);
					messageList.add(message);
					return null;
				}
				
				if(statusMaxSumDISTR){
					
					List<String> param = new ArrayList<String>();
					
					if("0".equals(maxMaxSumDISTRMsg)){
						param.add(agentGroup);
					}else{
						param.add(maxMaxSumDISTRMsg);
						param.add(allSumInsuredDISTR.toString());
						param.add(agentGroup);
					}
					
//					param.add(maxMaxSumDISTRMsg);
//					param.add(allSumInsuredDISTR.toString());
//					param.add(agentGroup);
					Message message = messageService.getMessage(registersystem, msgMaxSumDISTR , param, param);
					messageList.add(message);
					return null;
				}
				if(statusMaxSumHealth){
					
					List<String> param = new ArrayList<String>();
					
					if("0".equals(maxMaxSumHealthMsg)){
						param.add(agentGroup);
					}else{
						param.add(maxMaxSumHealthMsg);
						param.add(allSumInsuredByHealth.toString());
						param.add(agentGroup);
					}
					
//					param.add(maxMaxSumHealthMsg);
//					param.add(allSumInsuredByHealth.toString());
//					param.add(agentGroup);
					Message message = messageService.getMessage(registersystem, msgMaxSumHealth , param, param);
					messageList.add(message);
					return null;
				}
				if(statusMaxSumFixed){
				
					List<String> param = new ArrayList<String>();
					
					if(msgMaxSumFixed.equals("DT03502")){
						param.add(agentGroup);
					}else{
						param.add(maxMaxSumFixedMsg);
						param.add(allSumInsuredByFixed.toString());
						param.add(agentGroup);
					}
					
					Message message = messageService.getMessage(registersystem, msgMaxSumFixed , param, param);
					messageList.add(message);
					return null;
				}
		
				
			}else{
				msg.add(RuleConstants.MESSAGE_CODE.DT03533);
			}
			
		}else{
			msg.add(RuleConstants.MESSAGE_CODE.DT03508);
		}

		return msg;		
	}
}
