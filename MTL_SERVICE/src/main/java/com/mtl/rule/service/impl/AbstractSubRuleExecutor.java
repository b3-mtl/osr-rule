package com.mtl.rule.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.Message;
import com.mtl.rule.service.interfaces.RuleProcess;

public abstract class AbstractSubRuleExecutor<I, C> implements RuleProcess {

	private Map<String, Object> variable;
	//private Map<String, String> message;
	
	private List<Message> messageList;
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();
	protected I input;
	protected C collecter;
	
	public AbstractSubRuleExecutor(I input, C collecter ) {
		super();
		this.input = input;
		this.collecter = collecter;
	}

	final public void run() {
		if (preAction() == false )
			return;

		execute();

		finalAction();
	}

	public void putVar(String key, Object value) {
		if (this.variable == null)
			this.variable = new HashMap<>();
		this.variable.put(key, value);
	}
//
//	public void putMessage(String key, String value) {
//		if (this.messageList == null)
//			this.messageList = new ArrayList<Message>();
//		this.message.put(key, value);
//	}

	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	public List<RuleResultBean> getRuleResultBeanList() {
		return ruleResultBeanList;
	}

	public void setRuleResultBeanList(List<RuleResultBean> ruleResultBeanList) {
		this.ruleResultBeanList = ruleResultBeanList;
	}

	
//	public void putMessage(String key, String value) {
//		if (this.message == null)
//			this.message = new HashMap<>();
//		this.message.put(key, value);
//	}

//	/**
//	 * @return the variable
//	 */
//	public Map<String, Object> getVariable() {
//		return variable;
//	}
//
//	/**
//	 * @return the message
//	 */
//	public Map<String, String> getMessage() {
//		return message;
//	}

	
	
}
