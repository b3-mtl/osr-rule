package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckMibAmlo;
import com.mtl.collector.service.rule16.CheckMibHighRisk;
import com.mtl.collector.service.rule16.PlanPermissionService;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16MibAmlo;
import com.mtl.rule.model.DT16MibHighRisk;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleMIBService extends AbstractSubRuleExecutor2 {

	@Autowired
	private CheckMibHighRisk checkMibHighRisk;

	@Autowired
	private CheckMibAmlo checkMibAmlo;

	@Autowired
	private MessageService messageService;

	@Autowired
	private PlanPermissionService planPermissionService;

	@Autowired
	private ApplicationCache applicationCache;

	public RuleMIBService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleMIBService.class);

	String sex = input.getRequestBody().getPayorBenefitPersonalData().getSex();
	String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
	String registersystem = input.getRequestHeader().getRegisteredSystem();
	List<String> codeMib = input.getRequestBody().getPayorBenefitPersonalData().getMibCodeList();
	List<String> codeAmlo = input.getRequestBody().getPayorBenefitPersonalData().getMibAmloCodeList();
	List<String> tempMIB = new ArrayList<String>();
	List<String> tempAMLO = new ArrayList<String>();

	@Override
	public void initial() {

	}
	
	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute Rule16 MIBService");
		log.info("## Execute ##");

		if (step2CheckSex()) {
			if (step3CheckExceptPBMIB()) {
				step4LookupDT16MibAmlo();
			} else {
				step5LookupDT16MibHighRisk();
			}

		}

	}

	@Override
	public void finalAction() {
	}

	private boolean step2CheckSex() {
		return Arrays.asList("C", "O").indexOf(sex) >= 0 ? false : true;
	}

	private boolean step3CheckExceptPBMIB() {
		PlanPermission planPermission = planPermissionService.getPlanPermissionByPlanCode(planCode);
		if (planPermission == null) {
			return false;
		} else {
			return "true".equals(planPermission.getExceptPBMIB());

		}
	}

	private void step4LookupDT16MibAmlo() {
		codeAmlo.parallelStream().forEach((data -> {
			DT16MibAmlo findData = applicationCache.getDt16MibAmlo().get(data);
			if (findData != null) {
				tempAMLO.add(findData.getCode());
			}
		}));
		step6CheckPBMIBList();
	}

	private void step5LookupDT16MibHighRisk() {
		codeMib.parallelStream().forEach(data -> {
			DT16MibHighRisk findData = applicationCache.getDt16MibHighRisk().get(data);
			if (findData != null) {
				tempMIB.add(findData.getCode());
			}
		});
		codeMib.parallelStream().forEach((data -> {
			DT16MibAmlo findData = applicationCache.getDt16MibAmlo().get(data);
			if (findData != null) {
				tempAMLO.add(findData.getCode());
			}
		}));

		step7CheckPBMIBList();

	}

	private void step6CheckPBMIBList() {
		  
		Message message;
		if (tempAMLO.size() > 0) {
			List<String> resultCodeAmlo =   Arrays.asList(StringUtils.join(tempAMLO, ","));
			message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16014, resultCodeAmlo,
					resultCodeAmlo);
			messageList.add(message);
		} 
		
	}

	private void step7CheckPBMIBList() {
		Message message;
		 
		  
		
		if (tempAMLO.size() > 0) {
			 List<String> resultCodeAmlo =   Arrays.asList(StringUtils.join(tempAMLO, ","));
			message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16014, resultCodeAmlo,
					resultCodeAmlo);
			messageList.add(message);
		} else if (tempMIB.size() > 0) {
			List<String> resultCodeMib =   Arrays.asList(StringUtils.join(tempMIB, ","));
			message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16013, resultCodeMib,
					resultCodeMib);
			messageList.add(message);
		}
		
	}

}
