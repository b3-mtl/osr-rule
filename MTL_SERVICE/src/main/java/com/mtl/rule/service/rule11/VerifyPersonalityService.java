package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.entity.MsPlanHeader;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11AgeSpecialPlanAtleast;
import com.mtl.rule.model.DT11AgeforSpecialPlan;
import com.mtl.rule.model.DT11VerifyNationaRider;
import com.mtl.rule.repository.Dt11VerifyDao;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class VerifyPersonalityService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(VerifyPersonalityService.class);

	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private Dt11VerifyDao dt11VerifyDao;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	boolean result_dt11 = false;
	boolean result_dt11_Age = false;
	boolean isBaby = false;
	boolean flag11AgeSpecial = false;

	public VerifyPersonalityService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
		// TODO Auto-generated method stub
		result_dt11 = false;
		result_dt11_Age = false;
		result_dt11 = false;
		flag11AgeSpecial = false;
		isBaby = false;
	}

	@Override
	public boolean preAction() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void execute() {

		// 2.4.13. ตรวจสอบข้อมูลอาชีพ เช่น แบบประกันประเภทจลาจล
		// ห้ามอาชีพใดซื้อบ้าง หรือแบบประกัน RHL011 ซื้อได้เฉพาะขั้นอาชีพ

		// 2.4.14. ตรวจสอบข้อมูลอายุ
		// และเพศของผู้เอาประกันสามารถซื้อแบบประกันที่ระบุได้หรือไม่ (PHDR)

		// TODO Auto-generated method stub
		Message msg = null;

		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		String sex = input.getRequestBody().getPersonalData().getSex();
		String age = input.getRequestBody().getPersonalData().getAge();
		String occupation = input.getRequestBody().getPersonalData().getOccupationCode();

		// String ageInMonthStr = "";
		BigDecimal ageInMonth = new BigDecimal(age).multiply(new BigDecimal(12));

		String heightStr = input.getRequestBody().getPersonalData().getHeight();
		String weightStr = input.getRequestBody().getPersonalData().getWeight();

		List<String> param = new ArrayList<String>();

		// Strat
		// System.out.println("dd/mm/yyyy");
		String birthdayIp = input.getRequestBody().getPersonalData().getBirthday();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(birthdayIp, formatter);

		Period p = Period.between(birthday, today);
		log.info("You are " + p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days old.");

		int ageyear = p.getYears();
		int agemonth = p.getMonths();
		int ageday = p.getDays();

		List<DT11AgeforSpecialPlan> ageforSpecialOnlyPlan = dt11VerifyDao.getDT11AgeforSpecialOnlyPlan(planCode);
		List<DT11AgeforSpecialPlan> ageforSpecialPlan = dt11VerifyDao.getDT11AgeforSpecialPlan(planCode, String.valueOf(agemonth), String.valueOf(ageday));
		List<DT11AgeSpecialPlanAtleast> ageforSpecialPlanAtleast = dt11VerifyDao.getDT11AgeSpecialPlanAtleast(planCode, String.valueOf(ageyear));
		
		
		
		// อายุไม่ถึง12 เดือน
		if (ageyear == 0 && agemonth < 12) {
			// check ที่ DT11_Age_forspecial_plan;
			if (ageforSpecialOnlyPlan != null) {
				if(ageforSpecialPlan == null){
					flag11AgeSpecial = true;
					result_dt11_Age = true;
					param = new ArrayList<String>();
					param.add(planCode);
					param.add( String.valueOf(ageyear));
					param.add(String.valueOf(agemonth));
					param.add( String.valueOf(ageday));
					msg = messageService.getMessage(registersystem, "DT11212", param, param);
					messageList.add(msg);
				}else{
					result_dt11_Age = true;
//					flag11AgeSpecial = true;
				}
				

			}
			

		} else {
			if (ageforSpecialPlanAtleast != null && !ageforSpecialPlanAtleast.isEmpty()) {
				result_dt11_Age = true;
//				flag11AgeSpecial = true;
				
			}
		}

		
		if (!flag11AgeSpecial) {
			List<MsPlanHeader> msPlanHeader = dt11VerifyDao.getMsPlanHeaderAge(planCode, String.valueOf(ageyear));
			
			
			
			if (msPlanHeader != null && !msPlanHeader.isEmpty()) {
				param = new ArrayList<String>();
				param.add(planCode);
				param.add( String.valueOf(ageyear));
				param.add(String.valueOf(agemonth));
				param.add( String.valueOf(ageday));
				
				msg = messageService.getMessage(registersystem, "DT1120Y", param, param);
				messageList.add(msg);

			}else{
				
				param = new ArrayList<String>();
				param.add(planCode);
				param.add( String.valueOf(ageyear));
				param.add(String.valueOf(agemonth));
				param.add( String.valueOf(ageday));
				msg = messageService.getMessage(registersystem, "DT11202", param, param);
				messageList.add(msg);
			}
		}
		
	}

	@Override
	public void finalAction() {
		// TODO Auto-generated method stub
		log.info("FinalAction Verify_Personality");
	}

}
