package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.MsSuperVIP;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SuperVipService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(SuperVipService.class);

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ApplicationCache applicationCache;

	SuperVipService(UnderwriteRequest i, CollectorModel c) {

		super(i, c);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		log.info("PreAction AT11_I_Super_VIP");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute AT11_I_Super_VIP");
		String clientNo = input.getRequestBody().getPersonalData().getClientNumber();
		String idCard = input.getRequestBody().getPersonalData().getIdCard();
		String firstName = input.getRequestBody().getPersonalData().getFirstName();
		String lastName = input.getRequestBody().getPersonalData().getLastName();
		String registersystem = input.getRequestHeader().getRegisteredSystem();

		MsSuperVIP superVip = applicationCache.getMsSuperVip().get(clientNo);

		if (superVip != null) {
			List<Message> msgList = new ArrayList<Message>();
			List<String> param = new ArrayList<String>();
			param.add("(" + idCard + ")");
			param.add("(" + firstName + " " + lastName + ")");
			Message msg = messageService.getMessage(registersystem, RuleConstants.RULE_11.MESSAGE_CODE_DT11210, param, param);
			msgList.add(msg);
			super.setMessageList(msgList);
		}
	}

	@Override
	public void finalAction() {
		log.info("FinalAction AT11_I_Super_VIP");
	}

}
