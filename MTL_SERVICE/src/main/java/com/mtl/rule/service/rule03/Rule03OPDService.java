package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckOPD;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03OPDSum;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03OPDService extends AbstractSubRuleExecutor2 {

	@Autowired
	private GetSumInsureService sumInsureService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private CheckOPD checkOPD;

	String state = "";
	boolean result = false;

	public Rule03OPDService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 สุขภาพผู้ป่วยนอก OPD ";

		AgentInfo agentInfo = collectorModel.getAgentDetail();
		String agentChannelCode = agentInfo.getAgentChannel();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> opdPlanList = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}

		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is OPD Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planType = temp.getPlanHeader().getPlanType();

			if (StringUtils.isNoneBlank(planType)) {
				if (checkOPD.checkValueInParameter(RuleConstants.RULE_03.OPD_PLAN_TYPE, planType)) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] is OPD Plan";
					opdPlanList.add(temp);
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] not OPD Plan";
				}
			}
		}

		if (opdPlanList.size() > 0) {
			for (InsureDetail temp : opdPlanList) {
				BigDecimal allSumInsured = BigDecimal.ZERO;
				BigDecimal allSumInsuredCurrent = BigDecimal.ZERO;
				BigDecimal allSumInsuredHistByPlanType = BigDecimal.ZERO;
				state = state + "\n" + RuleConstants.RULE_SPACE + "#2 Prepare Data : SumInsured (Current + History)";
				state = state + "\n" + RuleConstants.RULE_SPACE + "	2.1 SumInsured Current (Basic + Rider) ";
				
				BigDecimal insure = new BigDecimal(temp.getInsuredAmount());
				allSumInsuredCurrent = allSumInsuredCurrent.add(insure);
			
				state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured Current (Basic + Rider) = "+ allSumInsuredCurrent;
				state = state + "\n" + RuleConstants.RULE_SPACE + "	2.2 History SumInsured By Plan Type : OPD";
				allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByFindPlanType(historyInsureList,RuleConstants.RULE_03.OPD_PLAN_TYPE);
				allSumInsured = allSumInsuredCurrent.add(allSumInsuredHistByPlanType);
				state = state + "\n" + RuleConstants.RULE_SPACE + "	allSumInsured = " + allSumInsured;
	
				state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Find DT03_OPD_SUMINSURED is found ?";
				state = state + "\n" + RuleConstants.RULE_SPACE + "	#3 Input :[channelCode :" + agentChannelCode + " , sumInsuredAll:" + allSumInsured + "]";
				DT03OPDSum dt03_opd = checkOPD.checkOPDSumInsured(agentChannelCode, registersystem);
				boolean resultopt  = false;
				if (dt03_opd != null) {
					
					
					if(allSumInsured.compareTo( new BigDecimal(dt03_opd.getMaxOpdSuminsured())) > 0 ){
						
						state = state + "\n" + RuleConstants.RULE_SPACE + "	Not found -> Add Message Code DT03518 and End process ";
		
						List<String> param = new ArrayList<String>();
						
						
						param.add(dt03_opd.getMaxOpdSuminsured().toString());
	//					param.add(allSumInsured.toString());
						
						Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03518, param,param);
						messageList.add(message);
					}
				}else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	Found -> End process ";
				}
			
			}
			
			
		}else {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	#1 Result : Not found OPD Plan ---> End process";
        }
	}

	@Override
	public void finalAction() {
		state = state + "\n" + "END Rule 03 สุขภาพผู้ป่วยนอก OPD ";
		state = state + "\n" + "]";
		setSubRuleLog(state);
	}

}
