package com.mtl.rule.service.rule07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT07MIB;
import com.mtl.rule.model.DT07MIBHnw;
import com.mtl.rule.model.DT07MIBSpecial;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule07 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule07.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	
	@Autowired
	private ApplicationCache applicationCache;
	
	private Set<String> listMSG = new HashSet<String>();
	
	@Autowired
	private MessageService messageService;
	
 

	public ServiceRule07(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@SuppressWarnings("null")
	@Override
	public void execute() {
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		PersonalData personal = input.getRequestBody().getPersonalData();
		String planCode = basic.getPlanCode();
		List<String> mibCodes = collector.getPersonMIBCodeList();
		List<String> mibAmloCodes = collector.getPersonMIBAmloCodeList();
		List<String> tempMIB = new ArrayList<String>();
		List<String> tempAMLO = new ArrayList<String>();
		
		
		
		
		state  = state+"\n"+"[";
		state  = state+"\n"+"Start EXECUTE 07:  ผู้ร้องขอไม่เคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข ";
		state  = state+"\n"+RuleConstants.RULE_SPACE+"INPUT";
		state  = state+"\n"+RuleConstants.RULE_SPACE+"[idCard:"+personal.getIdCard()+", planCode:"+planCode+", appForm:"+input.getRequestBody().getAppForm()+" ]";
	 
		
		String key = planCode+"|";
		List<Boolean> ListHRk = new ArrayList<Boolean>();
		List<Boolean> ListAmlo = new ArrayList<Boolean>();
		List<Boolean> ListUnder = new ArrayList<Boolean>();
		List<Boolean> ListStd = new ArrayList<Boolean>();
		boolean checkExceptMIB = false;
		boolean checkExcept = false;
		boolean nextNode = false;
		PlanPermission except = applicationCache.getPlanPermissionAppCashMap().get(key);
		
		//Step 8 first check List MIB
		if((mibCodes!=null&&mibCodes.size()>0)||(mibAmloCodes!=null&&mibAmloCodes.size()>0)) {
			nextNode = true;
		}else {
			state  = state+"\n"+RuleConstants.RULE_SPACE+"	Not found MIB list => Add DT0700Y && End process";
			//  Add message Code DT0700Y: Rule 7 : ผู้ร้องขอไม่เคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข
			Message message1 = messageService.getOneMessage("",RuleConstants.MESSAGE_CODE.DT0700Y);
			messageList.add(message1);
		}
		
		if(nextNode) {
			state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 => Check ExceptMib";
			
			if(except != null) {
				if(RuleConstants.FALSE.equals(except.getExceptMib())) {
					checkExcept = true;
				}else {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => End process";
				}
			}else {
				checkExcept = true;
			}
			
			if(checkExcept) {
				
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Go #5";
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#5 => Check PlanType";
				String planType = basic.getPlanHeader().getPlanType();
				if(RuleConstants.RULE_07.HNW.equals(planType)) {
					
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => Go #6";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#6 => Lookup in DT07_MIB_HNW";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	Process ..........";
					HashMap<String, DT07MIBHnw> tempHnw = applicationCache.getRule07_mibHnwMap();
					for(String code:mibCodes) {
						if(tempHnw.containsKey(code)) {
							
							DT07MIBHnw tempObj = tempHnw.get(code);
							ListHRk.add(RuleConstants.TRUE.equals(tempObj.getHighRisk()));
							ListAmlo.add(RuleConstants.TRUE.equals(tempObj.getUnderwrite()));
							ListUnder.add(RuleConstants.TRUE.equals(tempObj.getMibAmlo()));
							ListStd.add(RuleConstants.TRUE.equals(tempObj.getStandard()));
							
							tempMIB.add(code);
						}
					}
				}else {
					
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Go #7";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 => Lookup in DT07_MIB";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	Process ..........";
					HashMap<String, DT07MIB> tempMib = applicationCache.getRule07_mibMap();
					for(String code:mibCodes) {
						if(tempMib.containsKey(code)) {
							
							DT07MIB tempObj = tempMib.get(code);
							if(tempObj == null) {
								continue;
							}
							ListHRk.add(RuleConstants.TRUE.equals(tempObj.getHighRisk()));
							ListAmlo.add(RuleConstants.TRUE.equals(tempObj.getUnderwrite()));
							ListUnder.add(RuleConstants.TRUE.equals(tempObj.getMibAmlo()));
							ListStd.add(RuleConstants.TRUE.equals(tempObj.getStandard()));
							
							tempMIB.add(code);
						}
					}
				}
			}else {
				
				if(except != null) {
					if(RuleConstants.FALSE.equals(except.getExceptMibSpecial()) || except.getExceptMibSpecial() == null) {
						checkExceptMIB = true;
					}else {
						state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => End process";
					}
				}else {
					checkExceptMIB = true;
				}
				
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => Go #3";
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#3 => Check ExceptMibSpecial";
				if(checkExceptMIB) {
					
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Go #4";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#4 => Lookup in DT07_MIB_SPECIAL";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	Process ..........";
					HashMap<String, DT07MIBSpecial> tempSpecial = applicationCache.getRule07_mibSpecialMap();
					for(String code:mibAmloCodes) {
						if(tempSpecial.containsKey(code)) {
							
							DT07MIBSpecial tempObj = tempSpecial.get(code);
							ListHRk.add(RuleConstants.TRUE.equals(tempObj.getHighRisk()));
							ListAmlo.add(RuleConstants.TRUE.equals(tempObj.getUnderwrite()));
							ListUnder.add(RuleConstants.TRUE.equals(tempObj.getMibAmlo()));
							ListStd.add(RuleConstants.TRUE.equals(tempObj.getStandard()));
							
							tempAMLO.add(code);
						}
					}
				}else {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => End process";
				}
			}
			mibHiskRisk = findTrueData(ListHRk);
			mibAmlo = findTrueData(ListAmlo);
			mibUnderwriter= findTrueData(ListUnder);
			mibStandard = findTrueData(ListStd);
			
			state  = state+"\n"+RuleConstants.RULE_SPACE+"OUTPUT";
			state  = state+"\n"+RuleConstants.RULE_SPACE+"[ mibHiskRisk:"+mibHiskRisk+" ,mibAmlo:"+mibAmlo+" ,mibUnderwriter:"+mibUnderwriter+" , mibStandard:"+mibStandard+" ]";
			
			state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 => Have MIB List ?";
			if(tempMIB.size() > 0) {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => Add MSG DT07002";
				listMSG.add(RuleConstants.MESSAGE_CODE.DT07002);
			}else {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	FALSE => Add MSG DT0700Y";
				listMSG.add(RuleConstants.MESSAGE_CODE.DT0700Y);
			}
			
			state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 => Have MIB Amlo List ?";
			if(tempAMLO.size() > 0) {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	TRUE => Add MSG DT07003";
				listMSG.add(RuleConstants.MESSAGE_CODE.DT07003);
			}
		}
		
		
	}

	@Override
	public void finalAction() {
	 
		
		// DT07001 Rule 7 : ไม่ได้ระบุข้อมูลชื่อ-นามสกุล / รหัสบัตรประจำตัวประชาชนผู้เอาประกันภัย
		// DT0700Y Rule 7 : ผู้ร้องขอไม่เคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข
		// DT07002 Rule 7 : ผู้ร้องขอเคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข
		// DT07003 พบข้อมูลที่ต้องส่งปรึกษาฝ่ายพิจารณารับประกัน
		
		for(String msg:listMSG) {
			String registersystem = input.getRequestHeader().getRegisteredSystem();
			Message message = messageService.getMessage(registersystem,msg, Arrays.asList(""), Arrays.asList("")); 
			messageList.add(message);
		}
		state  = state+"\n"+"Time usage in Rule 07 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END EXECUTE RULE 07 ";
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
		log.info(state);
		
		
	}
	
	@SuppressWarnings("unused")
	private Boolean findTrueData(List<Boolean> list) {
		Boolean res = false;
		if(list == null) {
			return res;
		}else {
			for(boolean temp : list) {
				if(temp) {
					res = temp;
					break;
				}
			}
			return res;
		}
		
		
	}
}

/*
 Question Rule 7
1. Check MIB กรองเฉพาะ status=ACTIVE ?  , ต้อง Validate อะไรก่อน เรียก MIB
2. Check System Name POS ? ตอบ ไม่มี  เพราะของเรา provide งาน สมัครใหม่
3.  Messge Migrate ไม่ครบ และแปลก
 */
