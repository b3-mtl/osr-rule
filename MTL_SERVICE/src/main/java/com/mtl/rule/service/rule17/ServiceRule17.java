package com.mtl.rule.service.rule17;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.BeneficiaryDetail;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT17BeneficiaryRelation;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule17 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6816663611421576179L;
	
	@Autowired
	private ApplicationCache applicationCache;

	@Autowired
	private MessageService messageService;

	String state = "";
	Timmer timeUsage = new Timmer();
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	public ServiceRule17(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {

	}

	@Override
	public boolean preAction() {

		List<BeneficiaryDetail> beneficiaryDetails = input.getRequestBody().getBeneficiaryDetails();
		if(beneficiaryDetails != null){
			return true;
		}else{
			return false;
		}
		
		
		
//		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "Start EXECUTE 17: ความสัมพันธ์กับผู้รับผลประโยชน์";
		List<BeneficiaryDetail> beneficiaryDetails = input.getRequestBody().getBeneficiaryDetails();
		PersonalData personalData = input.getRequestBody().getPersonalData();

		state = state + "\n" + RuleConstants.RULE_SPACE + "INPUT";
		if (beneficiaryDetails != null && beneficiaryDetails.size() > 0) {
			for (BeneficiaryDetail benefitciaryTmp : beneficiaryDetails) {
				state = state + "\n " + RuleConstants.RULE_SPACE + "IdCard :" + benefitciaryTmp.getIdCard();
				state = state + "\n " + RuleConstants.RULE_SPACE + "Beneficiary :" + benefitciaryTmp.getFirstName()
						+ "  " + benefitciaryTmp.getLastName() + "  " + benefitciaryTmp.getEnFirstName() + "  "
						+ benefitciaryTmp.getEnLastName();
				state = state + "\n " + RuleConstants.RULE_SPACE + "Insurer     :" + personalData.getFirstName() + "  "
						+ personalData.getLastName() + "  " + personalData.getEnFirstName() + "  "
						+ personalData.getEnLastName();
			}
		}

		/**
		 * Step 1 From Document SDS Topic 3.11.4 Beneficiary Is Not Null
		 */
		if (beneficiaryDetails == null || beneficiaryDetails.size() == 0) {
			state = state + "\n" + RuleConstants.RULE_SPACE + "Check Benificiary List --> Null -->End";
			Message message = messageService.getOneMessage(registersystem, RuleConstants.MESSAGE_CODE.DT17004);
			messageList.add(message);

		} else {

			/**
			 * Step 2 From Document SDS Topic 3.11.4 Lookup Decision Table Beneficiary
			 * Relation Check Last name Insurer equal Last name Beneficiary
			 */
			state = state + "\n" + RuleConstants.RULE_SPACE + "#Check DT17_BENEFICIARY_RELATION";
			int loop = 1;
			for (BeneficiaryDetail benefitciaryTmp : beneficiaryDetails) {
				String relationDesc = benefitciaryTmp.getRelation();
				DT17BeneficiaryRelation currentBeneficiaryRelation = applicationCache.getBeneficiaryRelationMap()
						.get(relationDesc);

				if (currentBeneficiaryRelation != null) {
					if (RuleConstants.TRUE.equals(currentBeneficiaryRelation.getFlagLastname())) {
						if (benefitciaryTmp.getLastName().equals(personalData.getLastName())) {
							state = state + "\n" + RuleConstants.RULE_SPACE + "Relation :"
									+ benefitciaryTmp.getRelation() + "and Lastname is Equal --> Check Document and Message";
//							addMessageAndDocument(currentBeneficiaryRelation);
						}else {
							state = state + "\n" + RuleConstants.RULE_SPACE + "Relation :"
									+ benefitciaryTmp.getRelation()
									+ "Not equal --> Add MSG & Document";
							addMessageAndDocument(currentBeneficiaryRelation);
						}
					}else {
						state = state + "\n" + RuleConstants.RULE_SPACE + "Relation :"
								+ benefitciaryTmp.getRelation()
								+ "Not check Lastname --> Add MSG & Document";
						addMessageAndDocument(currentBeneficiaryRelation);
					}
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "Loop " + loop + ": "
							+ benefitciaryTmp.getIdCard() + ": " + benefitciaryTmp.getFirstName() + " "
							+ benefitciaryTmp.getLastName() + " Relation:" + benefitciaryTmp.getRelation()
							+ " Not Found--> End Process";
				}

				/**
				 * Step 3 From Document SDS Topic 3.11.4 Check MIB
				 */
				List<String> mibCodeListTmp = benefitciaryTmp.getMibAmloCodeList();
				state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Check MIB ";
				if (mibCodeListTmp != null && mibCodeListTmp.size() > 0) {
					state = state + "\n" + RuleConstants.RULE_SPACE + " Found MIB Size:" + mibCodeListTmp.size();
					List<String> dt17MIBAmlo = applicationCache.getDt17MIBAMLOList();
					
					outerloop:
					for (String mibCodeTmp : mibCodeListTmp) {

						/**
						 * Step 4 From Document SDS Topic 3.11.4 Lookup Decision Table Beneficiary AMLO
						 */
						for(String dt:dt17MIBAmlo) {
							if (dt.equals(mibCodeTmp)) {
								state = state + "\n" + RuleConstants.RULE_SPACE + "MIB Code :" + mibCodeTmp
										+ "in DT17_MIB_AMLO -->DT17002 :" + benefitciaryTmp.getFirstName() + " "
										+ benefitciaryTmp.getLastName();
//								Message message = messageService.getOneMessage("", RuleConstants.MESSAGE_CODE.DT17001);
//								messageList.add(message);
								Message message = messageService.getOneMessage(registersystem, RuleConstants.MESSAGE_CODE.DT17001);
								
								
						        String fullName = benefitciaryTmp.getFirstName() + " "+ benefitciaryTmp.getLastName(); 
						        String fullNameEn = benefitciaryTmp.getEnFirstName() + " "+ benefitciaryTmp.getEnLastName();
						        if(benefitciaryTmp.getEnFirstName()==null || benefitciaryTmp.getEnFirstName().isEmpty()) {
						        	fullNameEn = benefitciaryTmp.getFirstName() + " "+ benefitciaryTmp.getLastName(); 
						        }
						        if(benefitciaryTmp.getFirstName()==null || benefitciaryTmp.getFirstName().isEmpty() ) {
						        	fullName = benefitciaryTmp.getEnFirstName() + " "+ benefitciaryTmp.getEnLastName();
						        }
						        message.setMessageEN(message.getMessageEN().replaceAll("\\$", fullNameEn));
						        message.setMessageTH(message.getMessageTH().replaceAll("\\$", fullName));
						        messageList.add(message);
								break outerloop;
							}
						}
					}
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + benefitciaryTmp.getIdCard() + ":"
							+ benefitciaryTmp.getFirstName() + " " + benefitciaryTmp.getLastName()
							+ "   MIB From WS:  Not Found";
				}
				loop++;
			}
		}
		
	}

	@Override
	public void finalAction() {
		referUnderwriter = true;
		state  = state+"\n"+"Time usage in Rule 17 :"+timeUsage.timeDiff();
		state = state + "\n" + "End EXECUTE RULE 17 ";
		state = state + "\n" + "]";
		setRULE_EXECUTE_LOG(state);
	}

	private void addMessageAndDocument(DT17BeneficiaryRelation currentBeneficiaryRelation) {
		if(currentBeneficiaryRelation.getMessageCode()!=null) {
			List<String> thai = new ArrayList<String>();
			thai.add(currentBeneficiaryRelation.getRelationTh());
			thai.add(currentBeneficiaryRelation.getDocument());
			List<String> eng = new ArrayList<String>();
			thai.add(currentBeneficiaryRelation.getRelationEn());
			thai.add(currentBeneficiaryRelation.getDocument());
			
			Message message = messageService.getMessage(registersystem, currentBeneficiaryRelation.getMessageCode(), thai, eng);
			messageList.add(message);
		}
		if(currentBeneficiaryRelation.getDocument()!=null) {
			Document document = applicationCache.getDocumentCashMap()
					.get(currentBeneficiaryRelation.getDocument());
			documentList.add(document);
		}
	}
}
