package com.mtl.rule.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.interfaces.RuleProcess;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractSubRuleExecutor2 implements RuleProcess {

	private Map<String, Object> variable;
	protected UnderwriteRequest input ;
	protected CollectorModel collectorModel;
	protected String subRuleLog;
	protected List<Message> messageList  = new ArrayList<Message>();
	protected List<Document> documentList  = new ArrayList<Document>();
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();
	 
	
	public AbstractSubRuleExecutor2(UnderwriteRequest input, CollectorModel collecter ) {
		super();
		this.input = input;
		this.collectorModel = collecter;
	}

	final public void run() {
		if (preAction() == false )
			return;
		
		initial();
		
		execute();

		finalAction();
	}

	public void putVar(String key, Object value) {
		if (this.variable == null)
			this.variable = new HashMap<>();
		this.variable.put(key, value);
	}
 
	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	public List<RuleResultBean> getRuleResultBeanList() {
		return ruleResultBeanList;
	}

	public void setRuleResultBeanList(List<RuleResultBean> ruleResultBeanList) {
		this.ruleResultBeanList = ruleResultBeanList;
	}

	public List<Document> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Document> documentList) {
		this.documentList = documentList;
	}

	
 
	
	
}
