package com.mtl.rule.service.interfaces;

public interface RuleProcess {

	void initial();

	boolean preAction();

	void execute();

	void finalAction();
}
