package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule06.GetDocumenService;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11OccuLevPlanAcc;
import com.mtl.rule.model.DT11OccuLevPlanCode;
import com.mtl.rule.model.DT11OccuLevPlanType;
import com.mtl.rule.model.DT11OccuSuminsured;
import com.mtl.rule.model.DT11OcuupationAccident;
import com.mtl.rule.model.MsInvesmentPlanType;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OccupationService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(OccupationService.class);
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private MessageService messageService;

	@Autowired
	private GetSumInsureService getSumInsureService;

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private GetDocumenService documenService;
	
//	@Autowired
//	private GetCollectorHistoryInsure getCollectorHistoryInsure;

	List<Message> message = null;

	List<HistoryInsure> historyDetails = null;

	public OccupationService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
		message = new ArrayList<Message>();
	}

	@Override
	public boolean preAction() {
		log.info("PreAction Occupation");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute Occupation");
		String occupationCode = input.getRequestBody().getPersonalData().getOccupationCode();
		BigDecimal sumInsure = getSumInsureService.getSumInsuredByPlanType("HB", historyDetails, RuleConstants.SYSTEM_NORMAL);

		
		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		String planType = input.getRequestBody().getBasicInsureDetail().getPlanHeader().getPlanType();
		String insuredAmount = input.getRequestBody().getBasicInsureDetail().getInsuredAmount();
		List<InsureDetail> planCodeRider = input.getRequestBody().getRiderInsureDetails();
		
		List<DT11OcuupationAccident> dt11OcuupationAccident = applicationCache.getRule11OccupationAccident();
		String occupationLevel =null;
		boolean rule11Occupation = false;
		
		 List<DT11OccuLevPlanCode> dt11OccuLevPlanCode = applicationCache.getRule11OccuLevPlanCode();
		 List<DT11OccuSuminsured> dt11OccuSuminsured   = applicationCache.getRule11OccuSuminsured();
		 List<DT11OccuLevPlanType> dt11OccuLevPlanType = applicationCache.getRule11OccuLevPlanType();
		 List<DT11OccuLevPlanAcc> dt11OccuLevPlanAcc  = applicationCache.getRule11OccuLevPlanAcc();
		 
		 InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		 String registersystem = input.getRequestHeader().getRegisteredSystem();
		 List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
			
		 List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		 List<InsureDetail> currentHitPlanList = new ArrayList<InsureDetail>();
		 currentPlanList.add(basic);

		 
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
 
	
		if (!StringUtils.isBlank(occupationCode)) {
			/*
			 * 1. Check Occuation level
			 */
			for (DT11OcuupationAccident dt11OcuupationAccident2 : dt11OcuupationAccident) {
				if(occupationCode.equals(dt11OcuupationAccident2.getOccupationCode())){
					occupationLevel = dt11OcuupationAccident2.getOccupationLevelAccident();
				}
			}
			
			/*
			 * 2. Check DT11_Occupation_Level_Plan_Accident
			 */

			
			
			if(occupationLevel!=null){
				for (DT11OccuLevPlanAcc dt11OccuLevPlanAcclist : dt11OccuLevPlanAcc) {
					
					boolean resultSet = false;
					if(dt11OccuLevPlanAcclist.getPlanCode()!=null){
						if(dt11OccuLevPlanAcclist.getPlanCode().startsWith("P")){
							
							resultSet = true;
						}else if(dt11OccuLevPlanAcclist.getPlanType() != null){
							
								if(oneInOfType(dt11OccuLevPlanAcclist.getPlanType(),currentHitPlanList)){
									resultSet = true;
								}
							
						}
					}
					
					
					if(resultSet){
						BigDecimal insuredByPlanType = BigDecimal.ZERO;
						insuredByPlanType = getSumInsuredByPlanType( dt11OccuLevPlanAcclist.getLimitType() ,currentHitPlanList, null );
						 
						if(insuredByPlanType.compareTo(new BigDecimal(dt11OccuLevPlanAcclist.getLimitType())) > 0){
							if(Integer.parseInt(occupationLevel) > Integer.parseInt(dt11OccuLevPlanAcclist.getOccupationLevel())){
								rule11Occupation = true ;
								break;
							}
						}
					}

				}
				
			}
			
			
			/*
			 * 3. Check DT11_Occupation_Level_Plan_Code
			 */
			if(occupationLevel!=null){
				if(dt11OccuLevPlanCode!=null){
					for (DT11OccuLevPlanCode dt11OccuLevPlanCodelist : dt11OccuLevPlanCode) {
						if(dt11OccuLevPlanCodelist.getPlanCode().equals(planCode) ){
							if(Integer.parseInt(occupationLevel) > Integer.parseInt(dt11OccuLevPlanCodelist.getOccupationLevel())){
								rule11Occupation = true ;
								break;
							}
						}
					}
				}
			
			}
			
			/*
			 * 4. Check DT11_Occupation_Level_Plan_Type
			 */
			if(occupationLevel!=null){
				if(dt11OccuLevPlanType!=null){
					for (DT11OccuLevPlanType dt11OccuLevPlanTypeList : dt11OccuLevPlanType) {
						if(dt11OccuLevPlanTypeList.getSystemName().equals(registersystem)){	
							if(dt11OccuLevPlanTypeList.getPlanType().equals(planType)){
								if(dt11OccuLevPlanTypeList.getPlanCode()!=null && dt11OccuLevPlanTypeList.getPlanCode().equals(RuleConstants.OTHERWISE)){
									if(dt11OccuLevPlanTypeList.getPlanCode().contains(planCode)){
										if(dt11OccuLevPlanTypeList.getPlanCode().contains(planCode)){
											if(Integer.parseInt(occupationLevel) > Integer.parseInt(dt11OccuLevPlanTypeList.getOccupationLevel())){
												rule11Occupation = true ;
												break;
											}
										}
									}
								}else{
									if(Integer.parseInt(occupationLevel) > Integer.parseInt(dt11OccuLevPlanTypeList.getOccupationLevel())){
										rule11Occupation = true ;
										break;
									}
								}
							}
						}
					}
				}
			}
			/*
			 * 5. Check DT11_Occupation_SumInsured
			 */
			if(occupationLevel!=null){
				if(dt11OccuSuminsured!=null){
					for (DT11OccuSuminsured dt11OccuSuminsuredlist : dt11OccuSuminsured) {
						if(dt11OccuSuminsuredlist.getSystemName().equals(registersystem)){
							if(dt11OccuSuminsuredlist.getPlanCode().equals(planCode) ){
								if(dt11OccuSuminsuredlist.getOccupationLevel().equals(occupationLevel)){
									if( Integer.parseInt(insuredAmount) > Integer.parseInt(dt11OccuSuminsuredlist.getSumInsured()) ){							
										rule11Occupation = true ;
										break;
									}
								}
							}
						}
					}
				}
				
			}
			
			/*
			 * 6. Msg
			 */
			if(rule11Occupation){
//				DT11001
				
				List<String> param = new ArrayList<String>();
				param.add(planCode);
				
				Message msg = messageService.getMessage(registersystem, RuleConstants.RULE_11.MESSAGE_CODE_DT11001, param, param);
				messageList.add(msg);
				
			}
			
		}else{
			// end process
			return;
		}

	}

	

	@Override
	public void finalAction() {
		log.info("FinalAction Occupation");
		if (message.size() > 0) {
			super.setMessageList(message);
		}
	}
	
	public BigDecimal getSumInsuredByPlanType(String planType, List<InsureDetail> insureDetails, String planSystem) {
		BigDecimal sumInsured = BigDecimal.ZERO;
	
		if (insureDetails != null && insureDetails.size() > 0) {
			for (InsureDetail insure : insureDetails) {
				if (insure.getPlanHeader().getPlanType() != null
						&& isOneOf(planType, insure.getPlanHeader().getPlanType())) {
					if (planSystem != null && !planSystem.trim().isEmpty()) {
						if (insure.getPlanHeader().getPlanSystem() != null
								&& isOneOf(planSystem, insure.getPlanHeader().getPlanSystem())) {
							sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
						}
					} else {
						sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
					}
				}
			}
		}
		
//		System.out.println("sumInsured : " +sumInsured);
		return sumInsured;
	}
	
	
	private boolean isOneOf(String source,String target){
		if(source!=null && (!source.isEmpty())){
			source = ","+source.trim()+",";
			target = ","+target.trim()+",";
			int i = source.indexOf(target);
			if(i!=-1){
				return true;
			}
		}
		return false;
	}

	public boolean oneInOfType(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanHeader().getPlanType();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
}
