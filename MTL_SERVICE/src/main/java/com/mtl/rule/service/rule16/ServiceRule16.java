package com.mtl.rule.service.rule16;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule16 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule16.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	 
	@Autowired
	private MessageService messageService; 
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	public ServiceRule16(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		log.info("0 initial Rule 16");
		rules = new ArrayList<>();
		RuleVerifyPersonalityService ruleVerifyPersonalityService = (RuleVerifyPersonalityService)context.getBean(RuleVerifyPersonalityService.class,this.input,this.collector);
		RuleSuperVIPService ruleSuperVIPService = (RuleSuperVIPService)context.getBean(RuleSuperVIPService.class,this.input,this.collector);
		RuleVerifyNationalityService verifyNationalityService = (RuleVerifyNationalityService)context.getBean(RuleVerifyNationalityService.class,this.input,this.collector);
		RulePayorRelationService payorRelationService = (RulePayorRelationService)context.getBean(RulePayorRelationService.class,this.input,this.collector);
		RuleOccupationService occupationService = (RuleOccupationService)context.getBean(RuleOccupationService.class,this.input,this.collector);
		RuleHealthProblemService healthProblemService = (RuleHealthProblemService)context.getBean(RuleHealthProblemService.class,this.input,this.collector);
		RuleClaimHistoryService claimHistoryService = (RuleClaimHistoryService)context.getBean(RuleClaimHistoryService.class,this.input,this.collector);
		RuleMIBService mibService = (RuleMIBService)context.getBean(RuleMIBService.class,this.input,this.collector);
		

		rules.add(ruleVerifyPersonalityService);
		rules.add(ruleSuperVIPService);
		rules.add(verifyNationalityService);
		rules.add(payorRelationService);
		rules.add(occupationService);
		rules.add(healthProblemService);
		rules.add(claimHistoryService);
		rules.add(mibService);
	}

	@Override
	public boolean preAction() {
		log.info("1 ## validate  Rule 16");
		
//
//		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
//		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
//		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
//		currentPlanList.add(basic);
//		if (rider != null) {
//			currentPlanList.addAll(rider);
//		}
		
		String personIDCard = input.getRequestBody().getPersonalData().getIdCard();
		String pbPersonIDCard = input.getRequestBody().getPayorBenefitPersonalData().getIdCard();

		if(personIDCard!=null && personIDCard!=null){
			if(!personIDCard.equals(pbPersonIDCard)){
				return true;
			}
		}

		return false;
//		return true;
	}

	@Override
	public void execute() {
		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 16";
		
		this.rules.parallelStream().forEach(rule -> {
			rule.run();
		});
		
 
	}

	@Override
	public void finalAction() {
 
		this.rules.parallelStream().forEach(rule -> {	 
			if(rule.getRuleResultBeanList()!= null&& rule.getRuleResultBeanList().size()>0) {
				super.getRuleResultBeanList().addAll(rule.getRuleResultBeanList());
			}
			if(rule.getMessageList() != null ) {
				messageList.addAll(rule.getMessageList());
			}
			if(rule.getDocumentList() != null ) {
				documentList.addAll(rule.getDocumentList());
			}
			state = state +"\n"+rule.getSubRuleLog();
		});
		
		
		if(super.getRuleResultBeanList()!=null) {
			for(RuleResultBean ruleResultTmp:super.getRuleResultBeanList()) {	 
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode(); 
					Message message1 = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message1);
			}
		}
		state  = state+"\n"+"Time usage in Rule 16 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 16";	
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}
	
	public boolean oneInOfType(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanHeader().getPlanType();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
}

/*
DT16301 Rule 16 (PB) : ไม่ได้ระบุ เพศ / ขั้นอาชีพ	Rule 16 (PB): Gender/ occupation is not indicated.
DT16302	 Rule 16 (PB) : ข้อมูลเพศ / อายุ ไม่อยู่ในเกณฑ์ที่กำหนด สำหรับซื้อแบบประกัน $	Rule 16 (PB): Gender/ age does not meet the criteria to buy $ insurance plan.
DT16001	 Rule 16 (PB) : ข้อมูล ส่วนสูง-น้ำหนัก ไม่อยู่ในเกณฑ์มาตรฐาน	Rule 16 (PB): Height-weight does not meet the standard criteria.
DT16002	 Rule 16 (PB) : ระบุสัญชาติ $ (ไม่ใช่สัญชาติไทย) ขอเอกสาร $	Rule 11 : $ nationality (non-Thai nationality) requires $.	
DT16003	 Rule 16 (PB) : อาชีพ $ (Occupation Amlo)	Rule 13 : $ (Occupation AMLO)
DT16004	 Rule 16 (PB) : พบประวัติเคลม $ ผ่านไม่ได้	Rule 16 (PB) : The applicant used to claim for $ so it does not meet the criteria.
DT16005	 Rule 16 (PB) : ผู้ชำระเบี้ย มีความสัมพันธ์ $ ขอเอกสาร $	Rule 16 (PB): Premium payer is a $. $ is required.
DT16006	 Rule 16 (PB) : เคยถูกปฏิเสธ/เลื่อน/เพิ่มอัตราเบี้ยประกันภัย/เปลี่ยนแปลงเงื่อนไขจากบริษัทนี้หรือบริษัทอื่น	Rule 16 (PB): Declined/ postponed/ charged for extra premium/ changed the conditions by this Company or other companies
DT16007	 Rule 16 (PB) : ดื่มหรือเคยดื่มเครื่องดื่มที่มีแอลกอฮอล์เป็นประจำ ประเภท $ ปริมาณ $ (ขวด/สัปดาห์)	Rule 16 (PB): Consume/ consumed alcoholic beverages regularly; Type: $, Consumption: $ (bottle/week)
DT16008	 Rule 16 (PB) : ในรอบ 6 เดือน น้ำหนักเพิ่มขึ้น/ลดลง $ ก.ก.	Rule 16 (PB): Within the past 6 months, weight has increased/ decreased $ k.g.	
DT16009	 Rule 16 (PB) : มีสุขภาพทางร่างกายและจิตใจสมบูรณ์ดีไม่มีอวัยวะใดของร่างกายพิการและ/หรือทุพพลภาพหรือไม่ป่วยเป็นโรคร้ายใดๆ (ตอบไม่ใช่)	Rule 16 (PB): Good physical and mental health without any handicap and/or disability or critical illnesses (Tick No)
DT16010	 Rule 16 (PB) : ในระหว่าง 3 ปีที่ผ่านมา เคยให้แพทย์ตรวจหรือเข้าสถานพยาบาลตรวจรักษา	Rule 16 (PB): During the past 3 years, you have been diagnosed by a physician or treated in a medical center.	
DT16011	 Rule 16 (PB) : เฉพาะสตรี เคยมีหรือกำลังมีอาการ ตั้งครรภ์/โรคแทรกซ้อนในการตั้งครรภ์และคลอดบุตร/เลือดออกผิดปกติทางช่องคลอด	Rule 16 (PB): For female only, you were or are pregnant/ had or have pregnancy and child delivery complications/ abnormal vaginal bleeding.
DT16012	 Rule 16 (PB) : อาชีพ $ (อาชีพเพิ่มเบี้ย)	Rule 16 (PB): $ (extra premium charged)
DT16013	 Rule 16 (PB) : ผู้ชำระเบี้ย พบข้อมูล MIB $	Rule 16 (PB): MIB $ of the premium payer is found.	
DT16014	 Rule 16 (PB) : ผู้ชำระเบี้ย พบข้อมูล MIB $ (MIB AMLO)	Rule 16 (PB): MIB $ of the premium payer is found. (MIB AMLO).
DT16015	 Rule 16 : ขอให้ท่านเปลี่ยนแปลงผู้ชำระเบี้ย ให้เป็นผู้ที่มีส่วนได้เสียที่เหมาะสมในการทำประกัน	Rule 16 : Please change the premium payer to be more appropriate for this insurance application.
DT16016	 Rule 16 (PB) : สัญชาติอเมริกา ไม่สามารถซื้อแบบประกันที่ระบุได้	Rule 16 PB) : U.S. national is not eligible to buy the insurance plan.
DT16017	 Rule 16 (PB) : ผู้ชำระเบี้ย รหัสบัตรประชาชน : $ / ชื่อ-นามสกุล : $ เป็น Super VIP	Rule 16 (PB): Premium Payer - Identification card no.: $/ Name-Surname: $ is Super VIP.
DT16018	 Rule 16 (PB) : ในระหว่าง 3 ปีที่ผ่านมา เคยให้แพทย์ตรวจหรือเข้าสถานพยาบาลตรวจรักษา ไม่ระบุรายละเอียด	Rule 16 (PB): During the past 3 years, you have been diagnosed by a physician or treated in a medical center. No details are specified.	
DT16019	 Rule 16 (PB) : เฉพาะสตรี เคยมีหรือกำลังมีอาการ ตั้งครรภ์/โรคแทรกซ้อนในการตั้งครรภ์และคลอดบุตร/เลือดออกผิดปกติทางช่องคลอด ไม่ระบุรายละเอียด	Rule 16 (PB): For female only, you were or are pregnant/ had or have pregnancy and child delivery complications/ abnormal vaginal bleeding. No details are specified.	
DT16020	 Rule 16 (PB) : ระบุสัญชาติ $ (Nationality AMLO)	Rule 16 (PB): $ nationality (Nationality AMLO)
 */
