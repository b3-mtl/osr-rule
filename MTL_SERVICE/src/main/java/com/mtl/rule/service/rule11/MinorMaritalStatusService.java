package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MinorMaritalStatusService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(MinorMaritalStatusService.class);

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;
	
	MinorMaritalStatusService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
		log.info("PreAction F11x7_Minor_Marital_Status");
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute F11x7_Minor_Marital_Status");
		String minorMaritalStatus = input.getRequestBody().getPersonalData().getMinorMaritalStatus();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		if (RuleConstants.TRUE.equalsIgnoreCase(minorMaritalStatus)) {
			Document document = applicationCache.getDocumentCashMap().get(RuleConstants.RULE_11.DOCUMENT_CODE_MC);
			if (document != null) {
				documentList.add(document);
				
				List<Message> msgList = new ArrayList<Message>();
				Message msg = messageService.getMessage(registersystem, RuleConstants.RULE_11.MESSAGE_CODE_DT11211, 
						Arrays.asList(document.getDescTH()), Arrays.asList(document.getDescEN()));
				msgList.add(msg);
				super.setMessageList(msgList);
			}
		}
	}

	@Override
	public void finalAction() {
		log.info("Final F11x7_Minor_Marital_Status");
	}

}
