package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckTerm;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03TermService  extends AbstractSubRuleExecutor2 {
	
	@Autowired
	private CheckTerm checkTerm;
	
	@Autowired
	private GetSumInsureService sumInsureService;
	
	@Autowired
	private MessageService messageService; 

	String state = ""; 
	boolean result = false;
	
	public Rule03TermService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 03 ตรวจสอบแบบประกันภัยกำหนดระยะเวลา";	 
		
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
        List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
        List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
        List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> termPlanList = new ArrayList<InsureDetail>();
        currentPlanList.add(basic);
        if(rider !=null) {
        	currentPlanList.addAll(rider);
        }
        
     // get planType
   		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Term Plan (true or false) ?";
   		for (InsureDetail temp : currentPlanList) {
   			String planType = temp.getPlanHeader().getPlanType();
   			if (StringUtils.isNoneBlank(planType)) {
  				if (checkTerm.checkValueInParameter(RuleConstants.RULE_03.TERM_PLAN_TYPE, planType)) {
   					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
   							+ " ,PlanType :" + planType + " ] is Saving Plan";
   					termPlanList.add(temp);
   				} else {
   					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
   							+ " ,PlanType :" + planType + " ] not Saving Plan";
   				}
   			}
   		}
   		
   		if(termPlanList.size()>0) {
   			BigDecimal allSumInsured= BigDecimal.ZERO;
        	BigDecimal allSumInsuredCurrent= BigDecimal.ZERO;
        	BigDecimal allSumInsuredHistByPlanType= BigDecimal.ZERO;
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Prepare Data : SumInsured (Current + History)";
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	2.1 SumInsured Current (Basic + Rider) ";
        	for(InsureDetail temp:currentPlanList) {
        		BigDecimal insure = new BigDecimal(temp.getInsuredAmount());
        		allSumInsuredCurrent = allSumInsuredCurrent.add(insure);
            }
        	state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured Current (Basic + Rider) = "+ allSumInsuredCurrent;
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.2 History SumInsured By Plan Type : Term";
			allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByFindPlanType(historyInsureList,RuleConstants.RULE_03.TERM_PLAN_TYPE);
			allSumInsured = allSumInsuredCurrent.add(allSumInsuredHistByPlanType);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	allSumInsured = " + allSumInsured;
			
			state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Check Suminsured is between in PlanCode ";
			for(InsureDetail temp:currentPlanList) {
				PlanHeader planHeader= temp.getPlanHeader();
				Double min = planHeader.getMinimumAmount();
				Double max = planHeader.getMaximumAmount();
				boolean checkBetween = checkTerm.checkBetweenSumInsured(min, max, allSumInsured);
	        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	3.1 Input :[planCode : "+temp.getPlanCode()+" , min :"+min+" , max:"+max+" , allSumInsured:"+allSumInsured+"]";
	        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result  ?:"+checkBetween;
	        	if(checkBetween) {
	        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	3.2 Add Message Code DT0XXXXX and End process ";
	        		Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT03518, Arrays.asList(""), Arrays.asList("")); 
					messageList.add(message);
	        	}
			}
   		}else {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	#1 Result : Not found Term Plan ---> End process";
        }

	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"END Rule 03 ตรวจสอบแบบประกันภัยกำหนดระยะเวลา";	
		state  = state+"\n"+"]";
		setSubRuleLog(state);
	}

}
