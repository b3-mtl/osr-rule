package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ChildWpService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(ChildWpService.class);
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private MessageService messageService;

	ChildWpService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		log.info("PreAction AT11_Child_WP");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute AT11_Child_WP");
		String ageStr = input.getRequestBody().getPersonalData().getAge();
		int age = Integer.parseInt(ageStr);
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<InsureDetail> currentPlan = new ArrayList<InsureDetail>();
		currentPlan.add(basic);
		if(rider !=null) {
			currentPlan.addAll(rider);
		}
//		if(input.getRequestBody().getBasicInsureDetail() != null) {
//			insureDetails.add(input.getRequestBody().getBasicInsureDetail());
//		}

		// create constant Type
		if ((RuleUtils.checkType("WP", currentPlan) || RuleUtils.checkType("WPF", currentPlan)) && age < 15 && rider.size() > 0) {
			List<Message> messageList = new ArrayList<Message>();
			Message msg = messageService.getOneMessage(registersystem, "DT11205");
			messageList.add(msg);
			super.setMessageList(messageList);
		}
	}

	@Override
	public void finalAction() {
		log.info("Execute AT11_Child_WP");
	}

}
