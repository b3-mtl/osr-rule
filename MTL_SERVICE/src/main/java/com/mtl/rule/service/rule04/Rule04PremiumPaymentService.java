package com.mtl.rule.service.rule04;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule04.CheckAdditionalDocument;
import com.mtl.collector.service.rule04.CheckMsPlanHeader;
import com.mtl.collector.service.rule04.CheckPremiumPayment;
import com.mtl.collector.service.rule04.ExceptPlanService;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT04AppMdc;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule04PremiumPaymentService extends AbstractSubRuleExecutor2  {
	
	private static final Logger log = LogManager.getLogger(Rule04PremiumPaymentService.class); 
	@Autowired
	private MessageService messageService; 
	
	@Autowired
	private ExceptPlanService exceptPlanService; 
	
	@Autowired
	private CheckMsPlanHeader checkMsPlanHeader; 
	
	@Autowired
	private CheckPremiumPayment checkPremiumPayment; 
	
	@Autowired
	private CheckAdditionalDocument checkAdditionalDocument; 
	
	@Autowired
	private ApplicationCache applicationCache;
	
	String state = ""; 
	public Rule04PremiumPaymentService(UnderwriteRequest input, CollectorModel collecter ) {
		super(input, collecter);
	}

	@Override
	public void initial() {  
	}

	@Override
	public boolean preAction() {  
		return true;
	}

	@Override
	public void execute() { 
		
		/**
		 * Preparing data from input
		 */
		String basicPlanCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		String basiePremiumPayment = input.getRequestBody().getBasicInsureDetail().getPremiumPayment();
		String basiInsureAmount = input.getRequestBody().getBasicInsureDetail().getInsuredAmount();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		AgentInfo agentInfo = collectorModel.getAgentDetail();
		String agentChannel = agentInfo.getAgentChannel(); 
		
		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE 04:Flow ตรวจสอบงวดการชำระเงิน";	 
 
		state  = state+"\n"+"INPUT REQUEST";
		state  = state+"\n"+"Agent Code:"+input.getRequestBody().getAgentCode();
		state  = state+"\n"+"Basic Insure: [Plan Code:"+basicPlanCode+",PremiumPayment:"+basiePremiumPayment+",InsureAmount:"+basiInsureAmount+"]";
	 
		
		state  = state+"\n"+"Rider Insure List";
		List<InsureDetail> allInsureList = new ArrayList<InsureDetail>();
		allInsureList.add(input.getRequestBody().getBasicInsureDetail());
		List<InsureDetail> riderList = input.getRequestBody().getRiderInsureDetails();
		if(riderList!=null&&riderList.size()>0) {
			for(InsureDetail riderTmp:riderList) {
				state  = state+"\n"+"[Plan Code:"+riderTmp.getPlanCode()+",PremiumPayment:"+riderTmp.getPremiumPayment()+",InsureAmount:"+riderTmp.getInsuredAmount()+"]";
				allInsureList.add(riderTmp)	;
				
			} 
		} 
		
		state  = state+"\n"+"INPUT DATA COLLECTOR";	 

		List<String> thaiList = null;
		List<String> engList = null;
		Set<String> documentSet = new HashSet<String>(); 
		Boolean next = null;  

		
		int loop =1;
		for(InsureDetail insureTmp:allInsureList) {

			state  = state+"\n"+RuleConstants.RULE_SPACE+"Loop "+loop+++":"+insureTmp.getPlanCode();	
	        String tmpPlanCode = insureTmp.getPlanCode();
	        PlanHeader currentPlanHeader = insureTmp.getPlanHeader();
	        String tmpPremiumPayment = insureTmp.getPremiumPayment();
	        String tempInsureAmount = insureTmp.getInsuredAmount();  
	        String wordingPP = convertPremiumPayment(tmpPremiumPayment);
	        
	        
	        /**
	         * Flow ตรวจสอบงวดการชำระเงิน
	         * 
			 * Step 2 From Document SDS Topic 3.2.3 Check Except Plan
			 * Check planCode is except in DT04_EXCEPT_PLAN_CODE
			 * true -> go step3
			 * false -> end
			 */
			state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Check Exception Plan DT04_EXCEPT_PLAN_CODE ["+tmpPlanCode+"]";
			boolean foundExceptionCode = exceptPlanService.findPlanCode(tmpPlanCode);
			if(!foundExceptionCode) {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Not Except --> #3 Check Premium Indicator";		
				next = true;
			}else {
				thaiList = new ArrayList<String>();
				thaiList.add(tmpPlanCode);
				thaiList.add(wordingPP);
				engList = new ArrayList<String>();
				engList.add(tmpPlanCode);
				engList.add(wordingPP);
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT0400Y, thaiList, engList); 
				messageList.add(message);
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Except --> DT0400Y -->End";	
				next = false;
			}
			
			if (next) {
				next = false; 
				String premiumIndecator = currentPlanHeader.getPremiumIndicator()==null?"":currentPlanHeader.getPremiumIndicator();
				boolean checkPremiumIndecator = RuleConstants.RULE_04.PREMIUM_INDICATOR.equalsIgnoreCase(premiumIndecator);
				boolean checkAgentChannel = RuleConstants.RULE_04.AGENT_CHANNEL_CODE.equals(agentChannel); 
				boolean checkPremiumPayment = RuleConstants.RULE_04.PREMIUM_PAYMENT.equals(tmpPremiumPayment);
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#3 Check Premium Indicator   [PremiumIndicator=2 &&  AgentChannel = 19 && PremiumPayment = S] :"
						+ "["+premiumIndecator+","+agentChannel+","+tmpPremiumPayment+"]? ";

				/**
				 * Step 3 From Document SDS Topic 3.2.3 Check Premium Indicator
				 * Check premiumIndicator is "2" and agentChannel is "19" and premiumPayment is "S"
				 * true -> go step 4
				 * false -> go step 5
				 */
				if(checkPremiumIndecator && checkAgentChannel && checkPremiumPayment) {
					
					/**
					 * Step 4 From Document SDS Topic 3.2.3 Check is Basic Plan
					 * Check planCode is "BASIC" 
					 * true -> go step 5
					 * false -> end & add Message DT0400Y
					 */
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#3 TRUE ---> Next to #4 Check is Basic Plan";
					boolean checkPlan = checkMsPlanHeader.checkBasicPlan(tmpPlanCode); 
					if(checkPlan) {
						state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#4 TRUE --> #5 Check Premium_Payment in Special";
						next = true;
					}else {
						state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#4 FAlSE --> is Rider and End Process";
						state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"::::::::::::::::::::: Except Case  ::::::::::::::::::::";
						thaiList = new ArrayList<String>();
						thaiList.add(tmpPlanCode);
						thaiList.add(wordingPP);
						engList = new ArrayList<String>();
						engList.add(tmpPlanCode);
						engList.add(wordingPP);
						Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT0400Y, thaiList, engList);
						messageList.add(message);
					}
					
				}else {
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#3 FALSE --> #5 Check Premium_Payment in Special";
					next = true;
				} 
			}		
			
			if(next) {
				/**
				 * Step 5 From Document SDS Topic 3.2.3 Check Premium_Payment is Special
				 * Check by planCode and premiumPayment 
				 * Case 1 -> add Message DT0400Y and end
				 * Case 2 -> go step 6
				 */
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#5 Check in Premium_Payment Special";
				boolean inSpecialPayment = checkPremiumPayment.checkPremiumPaymentSpecial(tmpPlanCode, tmpPremiumPayment);
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"	found in (DT04_PREMIUM_PAYMENT_SPECIAL) case => "+inSpecialPayment;
				if(inSpecialPayment) {
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#5 Case = 1: --- >DT0400Y";
					thaiList = new ArrayList<String>();
					thaiList.add(tmpPlanCode);
					thaiList.add(wordingPP);
					engList = new ArrayList<String>();
					engList.add(tmpPlanCode);
					engList.add(wordingPP);
					Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT0400Y, thaiList, engList); 
					messageList.add(message);
				}
				
//				if(inSpecialPayment == 2) {
//					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#5 Case = 2:---> DT04004";
//					thaiList = new ArrayList<String>();
//					thaiList.add(tmpPlanCode);
//					thaiList.add(wordingPP);
//					engList = new ArrayList<String>();
//					engList.add(tmpPlanCode);
//					engList.add(wordingPP);
//					Message message = messageService.getMessage("",RuleConstants.MESSAGE_CODE.DT04004, thaiList, engList); 
//					messageList.add(message);
//				}
				
				//new business logic
				else{
					/**
					 * Step 6 From Document SDS Topic 3.2.3 Check in Premium_Payment 
					 * Check by planCode and agentChannel and premiumPayment
					 * true -> add Message DT0400Y and end
					 * false -> add Message DT04004 and end
					 */
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"	#5 Case = 2 --> Next to #6";
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#6 Check Premim Payment [planCode:"+tmpPlanCode+", agentChannel:"+agentChannel+", premiumPayment:"+tmpPremiumPayment+"] in MS_PREMIUM_PAYMENT ";
					boolean inPayment = checkPremiumPayment.checkPremiumPayment(tmpPlanCode,agentChannel, tmpPremiumPayment); 
					if(inPayment) { 
						
						state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"	#6 TRUE  --> DT0400Y";
						thaiList = new ArrayList<String>();
						thaiList.add(tmpPlanCode);
						thaiList.add(wordingPP);
						engList = new ArrayList<String>();
						engList.add(tmpPlanCode);
						engList.add(wordingPP);
						Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT0400Y, thaiList, engList); 
						messageList.add(message);
					}else {
	 
						state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#6 FALSE  --> DT04004";
						thaiList = new ArrayList<String>();
						thaiList.add(tmpPlanCode);
						thaiList.add(wordingPP);
						engList = new ArrayList<String>();
						engList.add(tmpPlanCode);
						engList.add(wordingPP);
						Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT04004, thaiList, engList); 
						messageList.add(message);
					}
				} 
			}
			
			
			/**
	         * Flow ตรวจสอบการขอเอกสาร APP/MDC
	         * 
	         * Step 2 From Document SDS Topic 3.2.4 Check Additional Document
	         * find Additional Document of planCode 
	         * found -> add MessageCode and Document Code
	         * not found -> end
			 **/
			state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"Check Additional Document ";
			DT04AppMdc additionalDocList = checkAdditionalDocument.ListAdditionalDoc(tmpPlanCode, (long)Double.parseDouble(tempInsureAmount), tmpPremiumPayment);
			
			if(additionalDocList!=null) {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"FOUND Additional Document "+additionalDocList.getDocument();
				thaiList = new ArrayList<String>();
				thaiList.add(tmpPlanCode);
				thaiList.add(wordingPP);
				thaiList.add(additionalDocList.getDocument() == null? "":additionalDocList.getDocument());
				
				engList = new ArrayList<String>();
				engList.add(tmpPlanCode);
				engList.add(wordingPP);
				engList.add(additionalDocList.getDocument() == null? "":additionalDocList.getDocument());
				
				Message message = messageService.getMessage(registersystem,additionalDocList.getMessageCode(), thaiList, engList); 
				messageList.add(message);
				documentSet.add(additionalDocList.getDocument());
					
			}else {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+" NOT FOUND Additional Document List ";
			}
//			if(RuleConstants.PLAN_TYPE.BASIC.equalsIgnoreCase(currentPlanHeader.getPlanGroupType())) {

//			}else {
//				
//				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+" NOT BASIC Skip Check Additional Document ";
//			}	
		}// End For
		
		for(String doc:documentSet) {
			Document document = applicationCache.getDocumentCashMap().get(doc);
			if(document !=null) {
				documentList.add(document);
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"-"+document.getCode()+":"+document.getDescTH();
			}
		}
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"END EXECUTE SUB RULE Rule04PremiumPaymentService";	
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"]";
		setSubRuleLog(state);
		log.info(state); 
	}
	
	public String convertPremiumPayment(String premiumPayment) {
		String wordTh = "";
		
		if(StringUtils.isAllBlank(premiumPayment)) {
			return wordTh;
		}
		
		if(RuleConstants.RULE_04.PP_1.equals(premiumPayment)) {
			wordTh ="รายเดือน";
		}
		
		if(RuleConstants.RULE_04.PP_3.equals(premiumPayment)) {
			wordTh ="ราย 3 เดือน";
		}
		
		if(RuleConstants.RULE_04.PP_6.equals(premiumPayment)) {
			wordTh ="ราย 6 เดือน";
		}
		
		if(RuleConstants.RULE_04.PP_Y.equals(premiumPayment)) {
			wordTh ="รายปี";
		}
		
		if(RuleConstants.RULE_04.PP_S.equals(premiumPayment)) {
			wordTh ="ครั้งเดียว";
		}

		return wordTh;
		
	}
}
