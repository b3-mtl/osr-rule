package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11RiskMobileNumber;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RiskMobileNumberService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(RiskMobileNumberService.class);

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	RiskMobileNumberService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public boolean preAction() {
		log.info("PreAction DT11_Risk_Mobile_Number");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute DT11_Risk_Mobile_Number");
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		DT11RiskMobileNumber riskMobile = applicationCache.getRule11_riskMobileNumber().get(
				input.getRequestBody().getPersonalData().getMobileNumber());
		if (riskMobile != null) {
			List<Message> messageList = new ArrayList<Message>();
			Message msg = messageService.getOneMessage(registersystem, riskMobile.getMessageCode());
			messageList.add(msg);
			super.setMessageList(messageList);
		}

	}

	@Override
	public void finalAction() {
		log.info("Final DT11_Risk_Mobile_Number");
	}

	@Override
	public void initial() {

	}

}
