package com.mtl.rule.service.rule15;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule13.GetType;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT15DupPackagePlan;
import com.mtl.rule.model.DT15DupPackageProject;
import com.mtl.rule.model.DT15DupPlanForClient;
import com.mtl.rule.model.DT15DupPlanForPolicy;
import com.mtl.rule.model.DT15DupPlanForPro;
import com.mtl.rule.model.MapProject;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.service.rule16.ServiceRule16;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleUtils;
import com.mtl.rule.util.SumInsureService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule15  extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6658744194206592569L;
	
	private static final Logger log = LogManager.getLogger(ServiceRule16.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();
	
	
	@Autowired
	private MessageService messageService;

	@Autowired
	private SumInsureService sumInsureService;
	
	@Autowired
	private GetType getType;
	
	@Autowired
	private ApplicationCache applicationCache;

	public ServiceRule15(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}
	
	
	@Override
	public void initial() {
		// TODO Auto-generated method stub
		log.info("0 initial Rule 15");
	}

	@Override
	public boolean preAction() {
		// TODO Auto-generated method stub
		
		return true;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		
		state  = state+"\n"+"[";
		state  = state+"\n"+"Start Rule 15 ";
		
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
        List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
        List<HistoryInsure> historyInsureList = collector.getHistoryInsureList();
        String planCode = basic.getPlanCode();
        PlanHeader planHeader = basic.getPlanHeader();
        BigDecimal tempSumBasic = BigDecimal.ZERO;
		tempSumBasic = tempSumBasic.add(new BigDecimal(basic.getInsuredAmount()));
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> currentHitPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> HitPlanListAll = new ArrayList<InsureDetail>();
		AgentInfo agenCode = collector.getAgentDetail();
		
		currentPlanList.add(basic);
		String projectType = null ;

		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		
		for (InsureDetail insureDetail : currentPlanList) {
			if(agenCode!=null){
				insureDetail.setServiceBranch(agenCode.getServiceBranch());
				insureDetail.setChannelCode(agenCode.getAgentChannel());
			}
		}
		
		currentHitPlanList.addAll(currentPlanList);
		if(historyInsureList != null){
			for (HistoryInsure insureDetail : historyInsureList) {
				PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(insureDetail.getPlanCode().trim());
				
				InsureDetail obj = new InsureDetail();
				obj.setPlanCode(insureDetail.getPlanCode().trim());
//				MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
//				if(planTypeInvestMent != null) {
//				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				if("F".equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null){
					obj.setInsuredAmount(insureDetail.getFaceAmount().toString());
				}else {
					obj.setInsuredAmount(insureDetail.getSumInsure().toString());
				}
				if(obj.getInsuredAmount()==null){
					obj.setInsuredAmount("0");
				}
				obj.setPlanHeader(hdr);
				obj.setServiceBranch(insureDetail.getServiceBrach());
				obj.setChannelCode(insureDetail.getChannelCode());
				obj.setWritingAgent(insureDetail.getWritingAgent());
				obj.setPolicyNumber(insureDetail.getPolicyNumber());
				obj.setIssueDate(insureDetail.getIssueDate());
//				setPolicyNumber
				HitPlanListAll.add(obj);
				
				currentHitPlanList.add(obj);
				
			}
		}
		
		//0.  Map Project
		List<MapProject> mapProject = applicationCache.getMapProjectList();

			for (MapProject mapProjectlist : mapProject) {
				if(mapProjectlist.getServicBranch() != null ){
					if(! RuleUtils.isOneOf( mapProjectlist.getServicBranch() , agenCode.getServiceBranch())){
						continue;
					}
				}
				if(mapProjectlist.getChannal() != null){
					if(!(mapProjectlist.getChannal().equalsIgnoreCase(agenCode.getAgentChannel()))){
						continue;
					}
				}
				if(mapProjectlist.getAgentCode() != null ){
					if(! RuleUtils.isOneOf( mapProjectlist.getAgentCode() , agenCode.getAgentCode())){
						continue;
					}
				}
				if(mapProjectlist.getPlanCode() != null ){
					if(!mapProjectlist.getPlanCode().equalsIgnoreCase(basic.getPlanCode())){
						continue;
					}
				}
				if(mapProjectlist.getPlanCodeList() != null ){
					if( ! RuleUtils.isOneOf(mapProjectlist.getPlanCodeList() , basic.getPlanCode()) ){
						continue;
					}
				}
				if(mapProjectlist.getCheckrider() != null){
					if(RuleUtils.oneInOf(mapProjectlist.getCheckrider(), currentPlanList )){
						projectType = mapProjectlist.getProjectType();
						continue;
					}
				}
				
				if( mapProjectlist.getRiderplan() != null && ! "Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())  ){
					if(RuleUtils.oneInOf(mapProjectlist.getRiderplan(), currentPlanList )){	
						projectType = mapProjectlist.getProjectType();
					}
				}else if("Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())){
					if(mapProjectlist.getPlanType() != null){
						if(RuleUtils.checkTypeInsureDetail(mapProjectlist.getPlanType() , currentPlanList) ){
							projectType = mapProjectlist.getProjectType();
						}
					}
				}
				
			}
			
//		}
		
		// mapProject History
	for (InsureDetail insureDetailList : HitPlanListAll) {
		if(insureDetailList.getPlanHeader().getPlanGroupType()!=null){
			if("BASIC".equals(insureDetailList.getPlanHeader().getPlanGroupType())){
				
				String serviceBranch = insureDetailList.getServiceBranch();
				String channelCode  = insureDetailList.getChannelCode();
				String agentChannel  = insureDetailList.getGroupChannelCode();
				
				for (MapProject mapProjectlist : mapProject) {
					if(mapProjectlist.getServicBranch() != null ){
						if(! RuleUtils.isOneOf( mapProjectlist.getServicBranch() , serviceBranch)){
							continue;
						}
					}
					if(mapProjectlist.getChannal() != null){
						if(!(mapProjectlist.getChannal().equalsIgnoreCase(channelCode))){
							continue;
						}
					}
					
					if(mapProjectlist.getAgentCode() != null ){
						if(! RuleUtils.isOneOf( mapProjectlist.getAgentCode() , agentChannel)){
							continue;
						}
					}
					if(mapProjectlist.getPlanCode() != null ){
						if(!mapProjectlist.getPlanCode().equalsIgnoreCase(insureDetailList.getPlanCode() )){
							continue;
						}
					}
					if(mapProjectlist.getPlanCodeList() != null ){
						if( ! RuleUtils.isOneOf(mapProjectlist.getPlanCodeList() , insureDetailList.getPlanCode()) ){
							continue;
						}
					}
					if(mapProjectlist.getCheckrider() != null){
						if(RuleUtils.oneInOf(mapProjectlist.getCheckrider(), HitPlanListAll )){
//							projectType = mapProjectlist.getProjectType();
							insureDetailList.setProjectType( mapProjectlist.getProjectType());
							continue;
						}
					}
					
					if( mapProjectlist.getRiderplan() != null && ! "Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())  ){
						if(RuleUtils.oneInOf(mapProjectlist.getRiderplan(), HitPlanListAll )){	
//							projectType = mapProjectlist.getProjectType();
							insureDetailList.setProjectType( mapProjectlist.getProjectType());
						}
					}else if("Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())){
						if(mapProjectlist.getPlanType() != null){
							if(RuleUtils.checkTypeInsureDetail(mapProjectlist.getPlanType() , HitPlanListAll) ){
//								projectType = mapProjectlist.getProjectType();
								insureDetailList.setProjectType( mapProjectlist.getProjectType());
							}
						}
					}
					
				}
				
			}
		}
	}
			
					

		
//		DT15_Duplicate_Package_Plan.dta
		List<DT15DupPackagePlan> dupPackagePlan = applicationCache.getDupPackagePlan();
		for (DT15DupPackagePlan dupPackagePlanList : dupPackagePlan) {			
//			System.out.println("------------ DT15_Duplicate_Package_Plan  getPlanCode -----------------" + dupPackagePlanList.getPlanCode());
			if(RuleUtils.isOneOf( dupPackagePlanList.getPlanCode() , planCode )){
//				System.out.println("------------ getSubPlanCode -----------------" + dupPackagePlanList.getSubPlanCode());
				if(RuleUtils.oneInOf(dupPackagePlanList.getSubPlanCode(), currentPlanList)){
					
					//Check ServiceBranch
				
					if(agenCode!=null && dupPackagePlanList.getServiceBranch()!=null ){
//						System.out.println("------------------  agenCode.getServiceBranch ---------------------------" + agenCode.getServiceBranch());
//						System.out.println("-------------------  dupPackagePlanList.getServiceBranch -------------------" + dupPackagePlanList.getServiceBranch());
						
						if(!dupPackagePlanList.getServiceBranch().equals("Otherwise") && agenCode.getServiceBranch() != null) {
							
							if(!agenCode.getServiceBranch().equals(dupPackagePlanList.getServiceBranch())){
								continue;
							}
						}
						
					}
					
					//Check AgenCode
//					System.out.println("-------------------------------  dupPackagePlanList.getAgentCode ------------------------------------------" + dupPackagePlanList.getAgentCode());
					if(dupPackagePlanList.getAgentCode()!=null){
						if(!RuleUtils.isOneOf(dupPackagePlanList.getAgentCode() ,agenCode.getAgentCode())){
							continue;
						};
					}				
//					System.out.println("-------------------------------  dupPackagePlanList.getCountPlanLimit ------------------------------------------" + dupPackagePlanList.getCountPlanLimit());
//					System.out.println("------------------------------- dupPackagePlanList.getCountPlanCode  ------------------------------------------" + dupPackagePlanList.getCountPlanCode());
//					System.out.println("------------------------------- countHistoryPackagePlan  ------------------------------------------" + RuleUtils.countHistoryPackagePlan( dupPackagePlanList.getCountPlanCode() , HitPlanListAll));
										//Check PackagePlanList
					if(dupPackagePlanList.getCountPlanLimit() != null) {				
						
						if( RuleUtils.countHistoryPackagePlan( dupPackagePlanList.getCountPlanCode() , HitPlanListAll) >= Integer.parseInt(dupPackagePlanList.getCountPlanLimit()) ){
						//MSg
							
							List<String> param = new ArrayList<String>();
							
							param = RuleUtils.mapMessageCodeDesc(dupPackagePlanList.getMessageDesc());
							Message message = messageService.getMessage(registersystem, dupPackagePlanList.getMessageCode()  , param, param);
							messageList.add(message);
							break;
						}				
					}	
					
					}else if (dupPackagePlanList.getSystemName() !=null){						
						//checkDuplicatePlanCodeInPlackage
						if( registersystem.equals(dupPackagePlanList.getSystemName())  && RuleUtils.checkDuplicatePlanCodeInPlackage( currentPlanList)  ){
//							System.out.println("------------------------------- getSystemName()  ------------------------------------------" + dupPackagePlanList.getSystemName());
						//MSg	
							List<String> param = new ArrayList<String>();
							param = RuleUtils.mapMessageCodeDesc(dupPackagePlanList.getMessageDesc());
							Message message = messageService.getMessage(registersystem,  dupPackagePlanList.getMessageCode() , param, param);
							messageList.add(message);
						}
					}
				}
			}

					
		
//		DT15_Duplicate_Package_Project.dta
		List<DT15DupPackageProject> dupPackageProject = applicationCache.getDupPackageProject();
		if(projectType!=null){
			for (DT15DupPackageProject dt15DupPackageProject : dupPackageProject) {
//				System.out.println("------------ DT15_Duplicate_Package_Project  getProjectType -----------------" + dt15DupPackageProject.getProjectType());
			//             find projectType
				if(RuleUtils.isOneOf(dt15DupPackageProject.getProjectType(),projectType)){
					if(dt15DupPackageProject.getCountResult()!=null){
						if(RuleUtils.countPackageProjectInList(dt15DupPackageProject.getCountPackage(), HitPlanListAll) >= Integer.parseInt(dt15DupPackageProject.getCountResult())){
							//MSg	
							List<String> param = new ArrayList<String>();
							if("DT15001".equals(dt15DupPackageProject.getMessageCode())){
								param.add(planCode);
								param.add(dt15DupPackageProject.getMessageDesc());
							}else{
								param = RuleUtils.mapMessageCodeDesc(dt15DupPackageProject.getMessageDesc());
								
							}
							Message message = messageService.getMessage(registersystem, dt15DupPackageProject.getMessageCode()  , param, param);
							messageList.add(message);
						}
					}else{
						if(RuleUtils.isOneOf(dt15DupPackageProject.getProjectType(),projectType)){
							//checkDuplicatePlanCodeInPlackage
							if( registersystem.equals(dt15DupPackageProject.getSystemName())  && RuleUtils.checkDuplicatePlanCodeInPlackage( currentPlanList)  ){
							//MSg	
								List<String> param = new ArrayList<String>();
								if("DT15001".equals(dt15DupPackageProject.getMessageCode())){
									param.add(planCode);
									param.add(dt15DupPackageProject.getMessageDesc());
								}else{
									param = RuleUtils.mapMessageCodeDesc(dt15DupPackageProject.getMessageDesc());
									
								}
								param = RuleUtils.mapMessageCodeDesc(dt15DupPackageProject.getMessageDesc());
								Message message = messageService.getMessage(registersystem, dt15DupPackageProject.getMessageCode()  , param, param);
								messageList.add(message);
							}
						}
						
					}
					
					
				}
			}	
		
			
		}

//		DT15_Duplicate_Plan_For_Client.dta
		List<DT15DupPlanForClient> dupPlanForClient = applicationCache.getDupPlanForClient();
		for (DT15DupPlanForClient dt15DupPlanForClient : dupPlanForClient) {
//			System.out.println("------------ DT15_Duplicate_Plan_For_Client  getPlanCode -----------------" + dt15DupPlanForClient.getPlanCode());
			//            find PlanCode
			if(RuleUtils.oneInOf(dt15DupPlanForClient.getPlanCode(), currentPlanList) ){
				if(agenCode!=null && dt15DupPlanForClient.getServiceBranch()!=null ){
//					System.out.println("--------- agenCode.getServiceBranch----------" +agenCode.getServiceBranch());
//					System.out.println("---------dt15DupPlanForClient.getServiceBranch  ----------" + dt15DupPlanForClient.getServiceBranch());
					if(!agenCode.getServiceBranch().equals(dt15DupPlanForClient.getServiceBranch())){
						continue;
					}
				}
				
				if(dt15DupPlanForClient.getChannelCode()!=null  && agenCode.getAgentChannel() !=null ){
					if(!dt15DupPlanForClient.getChannelCode().equals(agenCode.getAgentChannel())){
						continue;
					}
				}
				
				if(RuleUtils.countPlanCodeforClient(planCode, dt15DupPlanForClient.getChannelCode()!=null?dt15DupPlanForClient.getChannelCode():"" , HitPlanListAll ) >= Integer.parseInt(dt15DupPlanForClient.getCountResult()) ){
					//MSg	
					List<String> param = new ArrayList<String>();
					param.add(planCode);
					param.add(dt15DupPlanForClient.getMessageDesc());
					Message message = messageService.getMessage(registersystem,  dt15DupPlanForClient.getMessageCode() , param, param);
					messageList.add(message);
				}
			}
		}
		
		
		
		
//		DT15_Duplicate_Plan_For_Policy.dta
		List<DT15DupPlanForPolicy> dupPlanForPolicy = applicationCache.getDupPlanForPolicy();
		for (DT15DupPlanForPolicy dt15DupPlanForPolicy : dupPlanForPolicy) {
//			System.out.println("------------ DT15_Duplicate_Plan_For_Policy  getPlanType -----------------" + dt15DupPlanForPolicy.getPlanType());
//			System.out.println("------------ DT15_Duplicate_Plan_For_Policy  getPolicy -----------------" + dt15DupPlanForPolicy.getPolicy());
			if(RuleUtils.checkPlanTypeInCurrentRequest(dt15DupPlanForPolicy.getPlanType(),dt15DupPlanForPolicy.getPolicy(), currentPlanList)){
				if(RuleUtils.countPlanTypeforPolicy(dt15DupPlanForPolicy.getCountPlan(), dt15DupPlanForPolicy.getCountPolicy(), currentPlanList) >= Integer.parseInt(dt15DupPlanForPolicy.getCountResult())){
					//MSg	
					List<String> param = new ArrayList<String>();
					Message message = messageService.getMessage(registersystem, dt15DupPlanForPolicy.getMessageCode()  , param, param);
					messageList.add(message);
				}
			}
		}

//		DT15_Duplicate_Plan_For_Project.dta
		List<DT15DupPlanForPro> DupPackagePlanlist = applicationCache.getDupPlanForPro();
		for (DT15DupPlanForPro dupPackagePlans : DupPackagePlanlist) {
//			System.out.println("------------ DT15_Duplicate_Plan_For_Project  getPlanCode -----------------" + dupPackagePlans.getPlanCode());
			if(RuleUtils.oneInOf(dupPackagePlans.getPlanCode(), currentPlanList)){
//				if(agenCode!=null && dupPackagePlans.getServiceBranch()!=null ){
					
					// PAFamily
				if(dupPackagePlans.getPafamily()!=null){
					BigDecimal pAFamilyTotalDB = new BigDecimal(dupPackagePlans.getPafamily()); 
					BigDecimal pAFamily = BigDecimal.ZERO; 
					if(pAFamily.compareTo(pAFamilyTotalDB)>0){
						//Msg
					}
				}
				
					
				if(dupPackagePlans.getServiceBranch()!=null){
					if(agenCode.getServiceBranch().equals(dupPackagePlans.getServiceBranch())){
						
					}
				}
					
//					if(agenCode.getServiceBranch().equals(dupPackagePlans.getServiceBranch())){
						

						//Check Chanal
				if(dupPackagePlans.getChannelCode()!=null && !"Otherwise".equals(dupPackagePlans.getChannelCode())){
					//Check Count 
					Date requestDate = new Date();
					Integer yearDuration = Integer.parseInt(dupPackagePlans.getYearDuration());
					Integer dayDuration = Integer.parseInt(dupPackagePlans.getDayDuration());
					Integer resultRequestDate = Integer.parseInt(dupPackagePlans.getRequestDate());
					if(RuleUtils.countPlanCodeBeforeExpirationDay(dupPackagePlans.getPlanCodeDuration() , HitPlanListAll , yearDuration , dayDuration , requestDate, currentHitPlanList) > resultRequestDate){
						//MSg	
						List<String> param = new ArrayList<String>();
						param = RuleUtils.mapMessageCodeDesc(dupPackagePlans.getMessageDesc());
						Message message = messageService.getMessage(registersystem, dupPackagePlans.getMessageCode()  , param, param);
						messageList.add(message);
						
					}
//					System.out.println("--------------------- getChannelCode()  -------------" + dupPackagePlans.getChannelCode());
				}else if("Otherwise".equals(dupPackagePlans.getChannelCode()) || dupPackagePlans.getChannelCode()== null ){
					//Check  
					if(dupPackagePlans.getCountPlanCode() != null){
						if(RuleUtils.countPlanCodeInCoverageList(dupPackagePlans.getCountPlanCode(), currentHitPlanList ) > Integer.parseInt(dupPackagePlans.getCoverageList())){
							List<String> param = new ArrayList<String>();
							param = RuleUtils.mapMessageCodeDesc(dupPackagePlans.getMessageDesc());						
							Message message = messageService.getMessage(registersystem, dupPackagePlans.getMessageCode()  , param, param);
							messageList.add(message);
						} 
					}
					
				}
						
						
						
//					}
//				}
			}
		}
		
		
	}

	@Override
	public void finalAction() {
		// TODO Auto-generated method stub

//		this.rules.parallelStream().forEach(rule -> {	 
//			if(rule.getRuleResultBeanList()!= null&& rule.getRuleResultBeanList().size()>0) {
//				super.getRuleResultBeanList().addAll(rule.getRuleResultBeanList());
//			}
//			if(rule.getMessageList() != null ) {
//				messageList.addAll(rule.getMessageList());
//			}
//			if(rule.getDocumentList() != null ) {
//				documentList.addAll(rule.getDocumentList());
//			}
//			state = state +"\n"+rule.getSubRuleLog();
//		});
		
		
		if(super.getRuleResultBeanList()!=null) {
			for(RuleResultBean ruleResultTmp:super.getRuleResultBeanList()) {	 
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode(); 
					Message message1 = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message1);
			}
		}
		state  = state+"\n"+"Time usage in Rule 15 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 15";	
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}

}
