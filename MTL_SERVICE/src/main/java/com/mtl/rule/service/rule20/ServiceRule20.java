package com.mtl.rule.service.rule20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckOccupation;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Fatca;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16Occupation;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule20 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	/**
	 * 
	 */
	String state = "";
	Timmer timeUsage = new Timmer();
	private static final long serialVersionUID = -2402417870890551591L;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;
	
	private Set<String> listDOC = new HashSet<String>();

	public ServiceRule20(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(ServiceRule20.class);

	@Override
	public void initial() {

	}

	@Override
	public boolean preAction() {
		Fatca fatca = input.getRequestBody().getPersonalData().getFatca();
		return true;
	}

	@Override
	public void execute() {

		Fatca fatca = input.getRequestBody().getPersonalData().getFatca();

		// varible Question
		String pbUsGreenCard = fatca.getPbUSGreenCard();
		String pbObligationToPayTax = fatca.getPbObligationToPayTax();
		String pbResidentForTaxUSCollection = fatca.getPbResidentForTaxUSCollection();
		String pbUSNationalityAndBirthCountry = fatca.getPbUSNationalityAndBirthCountry();
		String pbUSNationalityAndBirthCountryDetail = fatca.getPbUSNationalityAndBirthCountryDetail();
		String usGreenCard = fatca.getUsGreenCard();
		String obligationToPayTax = fatca.getObligationToPayTax();
		String residentForTaxUSCollection = fatca.getResidentForTaxUSCollection();
		String usNationalityAndBirthCountry = fatca.getUsNationalityAndBirthCountry();
		String usNationalityAndBirthCountryDetail = fatca.getUsNationalityAndBirthCountryDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();

		List<String> thaiList = null;
		List<String> engList = null;

		state = state + "\n" + "[";
		state = state + "\n" + "Start EXECUTE 20: FATCA";
		state = state + "\n" + "INPUT:";
		state = state + "\n" + "usGreenCard:" + usGreenCard;
		state = state + "\n" + "obligationToPayTax:" + obligationToPayTax;
		state = state + "\n" + "residentForTaxUSCollection:" + residentForTaxUSCollection;
		state = state + "\n" + "usNationalityAndBirthCountry:" + usNationalityAndBirthCountry;
		state = state + "\n" + "pbUsGreenCard:" + pbUsGreenCard;
		state = state + "\n" + "pbObligationToPayTax:" + pbObligationToPayTax;
		state = state + "\n" + "pbResidentForTaxUSCollection:" + pbUsGreenCard;
		state = state + "\n" + "pbUSNationalityAndBirthCountry:" + pbUSNationalityAndBirthCountry;

		// start flow
		state = state + "\n" + " check usNationalityAndBirthCountry == Yes ";
		
		if (usNationalityAndBirthCountry.equals("Yes")) {
			System.out.println("===================  US  Yes    ==========================");

			state = state + "\n" + " check usGreenCard is USHolder";
			if (usNationalityAndBirthCountryDetail.equals("USHolder")) {
				System.out.println("===================  US  USHolder    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20101, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20101 ";

				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
			state = state + "\n" + " check usGreenCard is USBorn";
			if (usNationalityAndBirthCountryDetail.equals("USBorn")) {
				System.out.println("===================  US  USBorn    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20102, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20102 ";
				
				String[] doc = new String[] { "W9", "W8", "US" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
			state = state + "\n" + " check usGreenCard is Both";
			if (usNationalityAndBirthCountryDetail.equals("Both")) {
				System.out.println("===================  US  Both    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20103, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20103 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
					listDOC.add(RuleConstants.MESSAGE_CODE.DT07002);
				}
			}
		}
		
		
		state = state + "\n" + " check usGreenCard != null ";
		if (usGreenCard != null) {

			state = state + "\n" + " check usGreenCard is Yes";
			if (usGreenCard.equals("Yes")) {

				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20201, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20201 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
			state = state + "\n" + " check usGreenCard is NotEffective";
			if (usGreenCard.equals("NotEffective")) {

				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20202, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20202 ";
				
				String[] doc = new String[] { "W8" , "UG" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}
		state = state + "\n" + " check obligationToPayTax != null ";
		if (obligationToPayTax != null) {
			state = state + "\n" + " check obligationToPayTax is Yes";
			if (obligationToPayTax.equals("Yes")) {

				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20301, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20301 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}
		state = state + "\n" + " check residentForTaxUSCollection != null ";
		if (residentForTaxUSCollection != null) {
			state = state + "\n" + " check residentForTaxUSCollection is Yes";
			if (residentForTaxUSCollection.equals("Yes")) {

				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20401, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20401 ";
				
				String[] doc = new String[] { "W9", "W8", "UT" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}

		state = state + "\n" + " check pbUSNationalityAndBirthCountry !=null";
		if (pbUSNationalityAndBirthCountry.equals("Yes")) {
			System.out.println("===================  PB  Yes    ==========================");
			state = state + "\n" + " check pbUSNationalityAndBirthCountry is USHolder";
			if (pbUSNationalityAndBirthCountryDetail.equals("USHolder")) {
				System.out.println("===================  PB  USHolder    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20104, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20104 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
			state = state + "\n" + " check pbUSNationalityAndBirthCountry is USBorn";
			if (pbUSNationalityAndBirthCountryDetail.equals("USBorn")) {
				System.out.println("===================  PB  USBorn    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20105, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20105 ";
				
				String[] doc = new String[] { "W9", "W8", "US" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
			state = state + "\n" + " check pbUSNationalityAndBirthCountry is Both";
			if (pbUSNationalityAndBirthCountryDetail.equals("Both")) {
				System.out.println("===================  PB  Both    ==========================");
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20106, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20106 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}

		// pbUsGreenCard
		state = state + "\n" + " check pbUsGreenCard !=null";
		if (pbUsGreenCard != null) {

			state = state + "\n" + "	result == true go to #2";

			state = state + "\n" + "check pbUsGreenCard is Yes ?";
			if (pbUsGreenCard.equals("Yes")) {

				state = state + "\n" + "	result == true";
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20203, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20203 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}

			state = state + "\n" + "check pbUsGreenCard is NotEffective";
			if (pbUsGreenCard.equals("NotEffective")) {
				// add new ClaimCode to List
				state = state + "\n" + "	result == true ";
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20204, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20204 ";
				
				String[] doc = new String[] { "W8", "UG" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}

		// pbObligationToPayTax
		state = state + "\n" + "check pbObligationToPayTax !=null ";
		if (pbObligationToPayTax != null) {

			state = state + "\n" + "check pbObligationToPayTax is Yes";
			if (pbObligationToPayTax.equals("Yes")) {
				state = state + "\n" + "	result == true ";
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20302, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20302 ";
				
				String[] doc = new String[] { "W9" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}

		// pbResidentForTaxUSCollection
		state = state + "\n" + "check pbResidentForTaxUSCollection !=null";
		if (pbResidentForTaxUSCollection != null) {

			state = state + "\n" + "check pbResidentForTaxUSCollection is Yes";
			if (pbResidentForTaxUSCollection.equals("Yes")) {

				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT20402, thaiList, engList);
				messageList.add(message);
				state = state + "\n" + "	Add message DT20402 ";
				
				String[] doc = new String[] { "W9", "W8", "UT" };
				for (String code : doc) {
					listDOC.add(code);
					state = state + "\n" + "	Add document "+code;
				}
			}
		}

	}

	public static String[] GetStringArray(List<String> arr) {
		String str[] = new String[arr.size()];
		Object[] objArr = arr.toArray();
		int i = 0;
		for (Object obj : objArr) {
			str[i++] = (String) obj;
		}
		return str;
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"Time usage in Rule 20 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 20";	
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
		
		for(String msg:listDOC) {
			Document document = applicationCache.getDocumentCashMap().get(msg);
			documentList.add(document);
		}
		
	}

}
