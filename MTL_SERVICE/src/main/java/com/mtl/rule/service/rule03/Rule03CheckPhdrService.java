package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03CheckPhdrService extends AbstractSubRuleExecutor2 {


	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ApplicationCache applicationCache;

	String state = "";
	boolean result = false;

	public Rule03CheckPhdrService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		PlanHeader phdr = applicationCache.getPlanHeaderAppCashMap().get(basic.getPlanCode());
		BigDecimal insureAmount = new BigDecimal(basic.getInsuredAmount());
		boolean checkAmount = RuleUtils.betweenNumber(insureAmount, new BigDecimal(phdr.getMinimumAmount()), new BigDecimal(phdr.getMaximumAmount()));
		
		if(basic.getPlanCode().equals(phdr.getPlanCode()) && checkAmount )
		{
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT0310Y, Arrays.asList(basic.getInsuredAmount(),basic.getPlanCode()),Arrays.asList(basic.getInsuredAmount(),basic.getPlanCode()));
			messageList.add(message);
		}
		else
		{
			if(basic.getPlanCode()!=null)
			{
				if(RuleUtils.andLessthanNumber(insureAmount, new BigDecimal(phdr.getMinimumAmount())))
				{
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03103, Arrays.asList(),Arrays.asList());
					messageList.add(message);
				}
				else
				{
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03103, Arrays.asList(),Arrays.asList());
					messageList.add(message);
				}
			}
			else
			{
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03102, Arrays.asList(),Arrays.asList());
				messageList.add(message);
			}
		}
		
		
	}

	@Override
	public void finalAction() {
		state = state + "\n" + "END Rule 03 Check PHDR";
		state = state + "\n" + "]";
		setSubRuleLog(state);
	}

	

}
