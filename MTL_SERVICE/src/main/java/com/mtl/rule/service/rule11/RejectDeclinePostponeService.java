package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11RejectDeclinePostpone;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RejectDeclinePostponeService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(RejectDeclinePostponeService.class);
	
	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;
	
	List<HistoryInsure> historyDetails = null;

	RejectDeclinePostponeService(UnderwriteRequest i, CollectorModel c, List<HistoryInsure> h) {
		super(i, c);
		historyDetails = h;
	}

	@Override
	public boolean preAction() {
		log.info("PreAction DT11_REJECT_DECLINE_POSTPONE");
		return true;
	}

	@Override
	public void execute() {
		log.info("Execute DT11_REJECT_DECLINE_POSTPONE");
		String plancode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		if (StringUtils.isNotBlank(plancode)) {
			DT11RejectDeclinePostpone reject = applicationCache.getRule11_rejectDeclinePostpone().get(plancode);
			if (reject != null) {
				if (StringUtils.isNotBlank(reject.getMessageCode())) {
					List<Message> messageList = new ArrayList<Message>();
					List<String> msgparam = Arrays.asList(plancode);
					Message msg = messageService.getMessage(registersystem, reject.getMessageCode(), msgparam, msgparam);
					messageList.add(msg);
					super.setMessageList(messageList);
				}
			}
		}
	}

	@Override
	public void finalAction() {
		log.info("Final DT11_REJECT_DECLINE_POSTPONE");
	}

	@Override
	public void initial() {

	}

}
