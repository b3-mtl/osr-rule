package com.mtl.rule.service.rule16;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleVerifyPersonalityService extends AbstractSubRuleExecutor2 {

	public RuleVerifyPersonalityService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleVerifyPersonalityService.class);
	String state = "";
	
	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		state = state + "\n" + "#Start EXECUTE RULE 16 SubFlow: RuleVerifyPersonalityService";
		PlanHeader basicPlanHeader = input.getRequestBody().getBasicInsureDetail().getPlanHeader();
		List<InsureDetail> riderList = input.getRequestBody().getRiderInsureDetails();
		
		if (basicPlanHeader != null) {
			BigInteger planHeaderLowerAge = basicPlanHeader.getLowerAge();
			BigInteger planHeaderUpperAge = basicPlanHeader.getUpperAge();
			String planHeaderPlanType = basicPlanHeader.getPlanType();
			String planHeaderSex = basicPlanHeader.getSex();
			String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
			String planName = input.getRequestBody().getBasicInsureDetail().getPlanName();
			String pbPersonAge = input.getRequestBody().getPayorBenefitPersonalData().getAge();
			String pbPersonSex = input.getRequestBody().getPayorBenefitPersonalData().getSex();
			RuleResultBean ruleResultBeanTmp = null;
			
			
			if (pbPersonAge != null && pbPersonSex != null) {
//				int pbPersonAgeInt = Integer.parseInt(pbPersonAge);
				state = state + "\n" + " Found Age  => Check PlanType = 'RP' & Check age between ";
				state = state + "\n" + " LOWER AGE ,UPPER AGE,PLAN_TYPE,SEX From MS_PLAN_HEADE by PLAN_CODE: " 
				+ "" + planCode + " =[" + planHeaderLowerAge + "," + planHeaderUpperAge + "," + planHeaderPlanType + "," + planHeaderSex + "]";
				state = state + "\n" + " Check PBPerson age:" + pbPersonAge + " is between [" + planHeaderLowerAge + "," + planHeaderUpperAge + "] && PLAN_TYPE IS RP [planType:"+planHeaderPlanType+"]";
//				boolean ageBetween = pbPersonAgeInt >= planHeaderLowerAge.intValue() && pbPersonAgeInt <= planHeaderUpperAge.intValue();
				
				if(riderList!=null)
				{
					for(InsureDetail riderItem:riderList)
					{
						PlanHeader riderplanHeaderPlanType = riderItem.getPlanHeader();
						String riderPlanType = riderplanHeaderPlanType.getPlanType();
						BigInteger riderPlanTypeLowerAge = riderplanHeaderPlanType.getLowerAge();
						BigInteger riderPlanTypeUpperAge = riderplanHeaderPlanType.getUpperAge();
						int pbPersonAgeInt = Integer.parseInt(pbPersonAge);
						boolean ageBetween = pbPersonAgeInt >= riderPlanTypeLowerAge.intValue() && pbPersonAgeInt <= riderPlanTypeUpperAge.intValue();
						
						if (RuleConstants.RULE_16.PLANTYPE_RP.equals(riderPlanType) && ageBetween) {

						} else {
							state = state + "\n" + " FALSE => Add DT16302 & End process ";
							ruleResultBeanTmp = new RuleResultBean();
							ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT16302);
							List<String> stringInMessageList = new ArrayList<String>();
							stringInMessageList.add(planCode + ":" + planName);
							ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
							super.getRuleResultBeanList().add(ruleResultBeanTmp);
						}
					}
					
					
					
				}

				

			} else {
				state = state + "\n" + " Not Found Age or Sex   => Add DT16301 & End process ";
				ruleResultBeanTmp = new RuleResultBean();
				ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT16301);
				List<String> stringInMessageList = new ArrayList<String>();
				stringInMessageList.add(planCode + ":" + planName);
				ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
				super.getRuleResultBeanList().add(ruleResultBeanTmp);
			}
		}
	}

	@Override
	public void finalAction() {
		setSubRuleLog(state);
	}

}