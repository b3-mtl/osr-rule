package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleSuperVIPService extends AbstractSubRuleExecutor2 {
	@Autowired
	private ApplicationCache applicationCache;
	private String idCard;
	private String firstName;
	private String lastName;
	private String clientNo;

	public RuleSuperVIPService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleSuperVIPService.class);
	String state = "";

	@Override
	public void initial() {

	}
	
	@Override
	public boolean preAction() {
		if (StringUtils.isEmpty(input.getRequestBody().getPayorBenefitPersonalData().getIdCard())) {
			RuleResultBean ruleResultBeanTmp = new RuleResultBean();
			ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT16301);
			ruleResultBeanTmp.setNoParameterMessage(true);
			super.getRuleResultBeanList().add(ruleResultBeanTmp);

			return false;
		} else {
			return true;
		}

	}

	@Override
	public void execute() {
		// UnderwriteCollectorDaoImpl
		state = state + "\n" + "#Start EXECUTE RULE 16 SubFlow: RuleSuperVIPService";
		idCard = input.getRequestBody().getPayorBenefitPersonalData().getIdCard();
		firstName = input.getRequestBody().getPayorBenefitPersonalData().getFirstName();
		lastName = input.getRequestBody().getPayorBenefitPersonalData().getLastName();
		clientNo = input.getRequestBody().getPayorBenefitPersonalData().getClientNumber();
		state = state + "\n" + " INPUT [idCard,firstName,lastName,clientNo ]:[" + idCard + "," + firstName + ","
				+ lastName + "," + clientNo + "]";
		// Start flow check Client === CSCLI from table MS_SUPER_VIP
		checkSuperVIP();

	}

	private void checkSuperVIP() {
		if (applicationCache.getMsSuperVip().get(clientNo) != null) {
			state = state + "\n" + " Found Super VIP --->Add MESSAGE_CODE:DT16017";
			RuleResultBean ruleResultBeanTmp = new RuleResultBean();
			ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT16017);
			List<String> stringInMessageList = new ArrayList<String>();
			stringInMessageList.add(idCard);
			stringInMessageList.add(firstName + "-" + lastName);

			ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
			super.getRuleResultBeanList().add(ruleResultBeanTmp);
		} else {
			state = state + "\n" + " NOT Super VIP  ";
		}
	}

	@Override
	public void finalAction() {
		setSubRuleLog(state);
	}

}

/*
 * public boolean getSuperVIP(String clientNumber, Statement db2Statement){
 * 
 * StringBuilder sqlSuperVIP = new StringBuilder();
 * sqlSuperVIP.append(" SELECT VIP.CSCLI ");
 * sqlSuperVIP.append(" FROM DB_UW.SUPER_VIP VIP"); //
 * sqlSuperVIP.append(" WHERE VIP.VIP_TYPE = '50' "); //
 * sqlSuperVIP.append(" AND VIP.ID_CARD ='"+idCard+"'");
 * sqlSuperVIP.append(" WHERE VIP.CSCLI ='"+clientNumber+"'");
 * 
 * log.debug("SQL[GET SUPER VIP]:"+sqlSuperVIP.toString());
 * 
 * List<String> superVIPList = new ArrayList<String>(); try {
 * logMonitor.requestLog("getSuperVIP", null, clientNumber, null, null, null);
 * ResultSet superVIPRS = db2Statement.executeQuery(sqlSuperVIP.toString());
 * logMonitor.responseLog("getSuperVIP", null, null); while(superVIPRS.next())
 * superVIPList.add((StringUtils.isNotBlank(superVIPRS.getString("CSCLI")))?
 * superVIPRS.getString("CSCLI").trim():""); } catch (SQLException e) {
 * e.printStackTrace(); log.debug("Error Super VIP : " + e.getMessage());
 * logMonitor.responseLog("getSuperVIP", null, e.getMessage()); } return
 * superVIPList.size()>0?true:false; }
 * 
 */