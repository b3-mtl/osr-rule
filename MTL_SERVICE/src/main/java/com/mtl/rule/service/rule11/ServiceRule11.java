package com.mtl.rule.service.rule11;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.collector.service.GetCollectorHistoryInsure;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;


@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule11 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule11.class);
 
	private List<AbstractSubRuleExecutor2> rules;
	String state = "";
	Timmer timeUsage = new Timmer();

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private GetCollectorHistoryInsure getCollectorHistoryInsure;
	
	public ServiceRule11(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		String idCard = input.getRequestBody().getPersonalData().getIdCard();
		List<HistoryInsure> historyDetails = getCollectorHistoryInsure.getAllHistoryInsure(idCard);
		
		rules = new ArrayList<>();

		rules.add(context.getBean(RiskAreasOfficeService.class, super.input, super.collector));
		rules.add(context.getBean(RiskAreasHouseRegisService.class, super.input, super.collector));
		rules.add(context.getBean(RiskAreasPrestAddrService.class, super.input, super.collector ));
		rules.add(context.getBean(RiskMobileNumberService.class, super.input, super.collector));
		rules.add(context.getBean(RejectDeclinePostponeService.class, super.input, super.collector , historyDetails));
		rules.add(context.getBean(AdditionalInformationService.class ,super.input, super.collector));// TODO additional information
		rules.add(context.getBean(MinorMaritalStatusService.class, super.input, super.collector));
		rules.add(context.getBean(SuperVipService.class ,super.input, super.collector)); // TODO service supper vip
		rules.add(context.getBean(ChildWpService.class ,super.input, super.collector));
		rules.add(context.getBean(PbSameClientService.class ,super.input, super.collector));
		rules.add(context.getBean(OccupationMotorcycleService.class, super.input, super.collector, historyDetails));
		rules.add(context.getBean(CofService.class,super.input, super.collector));
		rules.add(context.getBean(VerifyNationalityRule11Service.class, super.input, super.collector));
		rules.add(context.getBean(BmiService.class, super.input, super.collector));
		rules.add(context.getBean(VerifyPersonalityService.class, super.input, super.collector));
//		rules.add(context.getBean(HealthProblemService.class, super.input, super.collector));
		rules.add(context.getBean(OccupationService.class, super.input, super.collector));
		
		
	}

	@Override
	public boolean preAction() {
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		
		if(StringUtils.isBlank(input.getRequestBody().getPersonalData().getNationality())) {
			Message msg = messageService.getOneMessage(registersystem, "DT11203");
			messageList.add(msg);
			return false;
		}
		
		if(StringUtils.isBlank(input.getRequestBody().getPersonalData().getWeight())
			|| 	StringUtils.isBlank(input.getRequestBody().getPersonalData().getHeight())
			|| 	StringUtils.isBlank(input.getRequestBody().getPersonalData().getSex())
			|| 	StringUtils.isBlank(input.getRequestBody().getPersonalData().getBirthday())
				) {
			Message msg = messageService.getOneMessage(registersystem, "DT11301");
			messageList.add(msg);
			return false;
		}
		
		
		String birthdayIp = input.getRequestBody().getPersonalData().getBirthday();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(birthdayIp, formatter);

		Period p = Period.between(birthday, today);
		log.info("You are " + p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days old.");

		int ageyear = p.getYears();
		int agemonth = p.getMonths();
		int ageday = p.getDays();

		input.getRequestBody().getPersonalData().setAge(ageyear+"");
		input.getRequestBody().getPersonalData().setAgeInMonth(agemonth+"");
		input.getRequestBody().getPersonalData().setAgeDay(ageday+"");
		
		return true;
	}

	@Override
	public void execute() {
		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 11";
	 
		this.rules.parallelStream().forEach(rule -> {
			rule.run();
		});
		
 
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"Time usage in Rule 11 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 11";	
		state  = state+"\n"+"]";
		
		setRULE_EXECUTE_LOG(state);
		rules.parallelStream().forEach((rule -> {
				if(rule.getMessageList() != null ) {
					messageList.addAll(rule.getMessageList());
				}
				if(rule.getDocumentList() != null ) {
					documentList.addAll(rule.getDocumentList());
				}
				
			}
		));
		
	}
}
