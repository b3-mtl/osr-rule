package com.mtl.rule.service.rule13;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule13.GetType;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT13AmloPilot;
import com.mtl.rule.model.DT13NonIncomeOccupation;
import com.mtl.rule.model.DT13RiskOccupation;
import com.mtl.rule.model.SumInsure;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.SumInsureService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule13 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4454857722437173369L;

	private static final Logger log = LogManager.getLogger(ServiceRule13.class);

	private List<AbstractSubRuleExecutor<UnderwriteRequest, CollectorModel>> rules;

	BigDecimal currentSumInsure  = BigDecimal.ZERO;
	
	
	@Autowired
	private MessageService messageService;

	@Autowired
	private SumInsureService sumInsureService;
	
	@Autowired
	private GetType getType;
	
	@Autowired
	private ApplicationCache applicationCache;

	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();
	
	String state = "";
	Timmer timeUsage = new Timmer();
	
	public ServiceRule13(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		log.info("0 initial Rule 13");

	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
        List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
        List<HistoryInsure> historyInsureList = collector.getHistoryInsureList();
        String occupationCode = input.getRequestBody().getPersonalData().getOccupationCode();
        String planCode = basic.getPlanCode();
        PlanHeader planHeader = basic.getPlanHeader();
        BigDecimal tempSumBasic = BigDecimal.ZERO;
		tempSumBasic = tempSumBasic.add(new BigDecimal(basic.getInsuredAmount()));
		
		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE 13: :  สิ่งต้องการเพื่อการพิจารณา (Underwriting Requirement)";
		state  = state+"\n"+"INPUT:";	 
		state  = state+"\n"+"planCode:"+planCode;
		state  = state+"\n"+"personalData.occupationCode:"+occupationCode;
		
		state  = state+"\n"+RuleConstants.RULE_SPACE+"#1 SumCurrentInsure"; 
//		currentSumInsure = sumInsureService.getSumInsureByCIRule13(docType, basic, rider, historyInsureList);//		currentSumInsure = sumInsureService.getSumInsurePlanTypeCI(basic, rider, historyInsureList);
		SumInsure responseSum = new SumInsure();
		responseSum = sumInsureService.getSumInsureByCIRule13(basic, rider, historyInsureList);
		currentSumInsure = responseSum.getSumInsure();
		
		state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result SumCurrentInsure)="+currentSumInsure;
        
		state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Get OccupationCode [occupationCode:"+occupationCode+"]"; 
		
		String key = occupationCode;
		DT13NonIncomeOccupation occupationCodeInput = applicationCache.getRule13NonIncomeOccupation(key);
		
        if(occupationCodeInput != null) {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	#2.1 Found OccupationCode -> Compare SumCurrentInsure > 2,000,000 ?"; 
        	if(currentSumInsure.compareTo(new BigDecimal(2000000)) == 1) {
        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result true -> Set message DT13002"; 
        		RuleResultBean ruleResultBeanTmp = new RuleResultBean();
				ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT13002);
				List <String> stringInMessageList = new ArrayList<String>();
				stringInMessageList.add(occupationCode);
				stringInMessageList.add(""+currentSumInsure);
				stringInMessageList.add(""+""+occupationCodeInput.getDocumentCode());
				
				ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
				ruleResultBeanList.add(ruleResultBeanTmp);
				
        	
        	if(StringUtils.isNoneBlank(occupationCodeInput.getDocumentCode())) {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"	Doccument -> Set document [code:"+occupationCodeInput.getDocumentCode()+"]"; 
				
				String[] docList = occupationCodeInput.getDocumentCode().split(",");
				for(String docCode:docList) {  
					Document document = applicationCache.getDocumentCashMap().get(docCode);   
					documentList.add(document); 
				}
			}else {
        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result false -> Go to #3"; 
        	}
        }else {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result Not found -> Go to #3"; 
        }
	}
        
        
        
        state  = state+"\n"+RuleConstants.RULE_SPACE+"#3 Check ExceptHIV by [planCode:"+planCode+"]"; 
        boolean checkHiv = getType.getPlanPermission(planCode);
        if(!checkHiv) {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	#3.1 Get Risk OccupationCode [occupationCode:"+occupationCode+"]"; 
        	
        	String key2 = occupationCode;
        	DT13RiskOccupation checkRisk = applicationCache.getRule13RiskOccupation(key2);
        	
        	if(checkRisk != null ) {
        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result Found Risk OccupationCode -> Set message "+checkRisk.getMessageCode(); 
        		RuleResultBean ruleResultBeanTmp = new RuleResultBean();
				ruleResultBeanTmp.setMessageCode(checkRisk.getMessageCode());
				List <String> stringInMessageList = new ArrayList<String>();
				stringInMessageList.add(occupationCode);
				stringInMessageList.add(checkRisk.getDocumentCode());
				
				ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
				ruleResultBeanList.add(ruleResultBeanTmp);	
				
				if(StringUtils.isNoneBlank(checkRisk.getDocumentCode())) {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	Doccument -> Set document [code:"+checkRisk.getDocumentCode()+"]"; 
					
					String[] docList = checkRisk.getDocumentCode().split(",");
					for(String docCode:docList) {  
						Document document = applicationCache.getDocumentCashMap().get(docCode);   
						documentList.add(document); 
					}
				}else {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	No Doccument  -> Go to #4"; 
				}
        	}else {
        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result Not found Risk OccupationCode -> Go to #4"; 
        	}
        }
        
        state  = state+"\n"+RuleConstants.RULE_SPACE+"#4 Check Amlo Pilot by [occupationCode:"+occupationCode+"]";
        DT13AmloPilot checkAmloPilot = getType.getAmloPilot(occupationCode);
        if(checkAmloPilot != null) {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result Found Amlo Pilot -> Set message "+checkAmloPilot.getMessageCode()+" & End process"; 
        	RuleResultBean ruleResultBeanTmp = new RuleResultBean();
			ruleResultBeanTmp.setMessageCode(checkAmloPilot.getMessageCode());
			List<String> stringInMessageList = new ArrayList<String>();
			stringInMessageList.add(occupationCode);
			
			ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
			ruleResultBeanList.add(ruleResultBeanTmp);

        }else {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result Not found Amlo Pilot -> End process"; 
        }
         
	}

	@Override
	public void finalAction() { 
		if(ruleResultBeanList!=null) {
			for(RuleResultBean ruleResultTmp:ruleResultBeanList) {	 
				if(ruleResultTmp.getMessageCode()!=null) { 
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode();
					String registersystem = input.getRequestHeader().getRegisteredSystem();
					Message message = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message);	 
				} 
				if(ruleResultTmp.getDocumentList()!=null) { 
					documentList.addAll(ruleResultTmp.getDocumentList());
				} 
			}
		} 
		state  = state+"\n"+"Time usage in Rule 13 :"+timeUsage.timeDiff();
		state  = state+"\n"+"End EXECUTE RULE 13 ";
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}
}
