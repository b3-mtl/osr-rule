package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PbSameClientService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(PbSameClientService.class);

	
	@Autowired
	private MessageService messageService;
	
	PbSameClientService(UnderwriteRequest i, CollectorModel c ) {
		super(i, c);
	}


	@Override
	public void initial() {
	}


	@Override
	public boolean preAction() {
		log.info("PreAction AT11_PB_Same_Client");
		return true;
	}


	@Override
	public void execute() {
		log.info("Execute AT11_PB_Same_Client");
		String ageStr = input.getRequestBody().getPersonalData().getAge();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		int age = Integer.parseInt(ageStr);
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		
		
		String idCard = input.getRequestBody().getPersonalData().getIdCard();
		String idCardPb = input.getRequestBody().getPayorBenefitPersonalData().getIdCard();
		
		if(RuleUtils.checkType("RP", rider)
			&& age > 20
			&& idCard.equals(idCardPb)
			) {
			List<Message> msgList = new ArrayList<Message>();
			Message msg = messageService.getOneMessage(registersystem, "DT11204");
			msgList.add(msg);
			super.setMessageList(msgList);
		}
	}


	@Override
	public void finalAction() {
		log.info("Final AT11_PB_Same_Client");
	}

}
