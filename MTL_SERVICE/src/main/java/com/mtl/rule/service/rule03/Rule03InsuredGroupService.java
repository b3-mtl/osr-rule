package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.model.ClientProfile;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckSumByPlan;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03AccidentForTakaful;
import com.mtl.rule.model.DT03AccidentForTakafulNc;
import com.mtl.rule.model.DT03ChannelSumInsured;
import com.mtl.rule.model.DT03ChannelSumInsuredEqual;
import com.mtl.rule.model.DT03ExtracareHbSuminsured;
import com.mtl.rule.model.DT03FixSumInsured;
import com.mtl.rule.model.DT03HBIncludeHistoryTele;
import com.mtl.rule.model.DT03PackageHSLimit;
import com.mtl.rule.model.DT03PaidUpSumInsured;
import com.mtl.rule.model.DT03PlanCodeSumInsuredHistory;
import com.mtl.rule.model.DT03PlanTypeeSumInsuredHistory;
import com.mtl.rule.model.DT03ProductAMLO;
import com.mtl.rule.model.DT03ProjectSumIndured;
import com.mtl.rule.model.DT03RiderPlanSumInsured;
import com.mtl.rule.model.DT03RiskDiagnosisPlanType;
import com.mtl.rule.model.DT03SumInsuredWithAge;
import com.mtl.rule.model.DT03SumInsuredWithMustHaveRider;
import com.mtl.rule.model.Dt03BasicPlanSumInsured;
import com.mtl.rule.model.Dt03DisorderAndAccident;
import com.mtl.rule.model.MapProject;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.ModelUtils;
import com.mtl.rule.util.RuleConstants;
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03InsuredGroupService  extends AbstractSubRuleExecutor2 {
	
	@Autowired
	private CheckSumByPlan checkSumByPlan;
	
	@Autowired
	private GetSumInsureService sumInsureService;
	
	@Autowired
	private MessageService messageService; 
	
	@Autowired
	private ApplicationCache applicationCache;

	String state = ""; 
	List<String> listMsgCode = new ArrayList<String>();
	boolean result = false;
	
	public Rule03InsuredGroupService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 ตรวจสอบแบบประกันภัยกลุ่ม";

		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		String age = input.getRequestBody().getPersonalData().getAge();
		AgentInfo agenCode = collectorModel.getAgentDetail();
		
		ClientProfile ClientPF = collectorModel.getClientProfile();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> currentHitPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> paFamilyPlanList = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);

		BigDecimal sumInsuredMain = BigDecimal.ZERO;
		BigDecimal sumInsuredPAFamily = BigDecimal.ZERO;
		sumInsuredMain = new BigDecimal(basic.getInsuredAmount()); 
		String projectType = null ;
		
		boolean planHealth = false;
		
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		//Add Service Branch
		for (InsureDetail insureDetail : currentPlanList) {
			if(agenCode!=null){
				insureDetail.setServiceBranch(agenCode.getServiceBranch());
				insureDetail.setChannelCode(agenCode.getAgentChannel());
			}
			
		}
		currentHitPlanList.addAll(currentPlanList);
		if(historyInsureList != null){
			for (HistoryInsure insureDetail : historyInsureList) {
				PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(insureDetail.getPlanCode().trim());
				
				InsureDetail obj = new InsureDetail();
				obj.setPlanCode(insureDetail.getPlanCode().trim());
//				MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
//				if(planTypeInvestMent != null) {
//				PlanHeader planHeader = applicationCache.getPlanHeaderAppCashMap().get(planCodeTrim);
				if("F".equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null){
					obj.setInsuredAmount(insureDetail.getFaceAmount().toString());
				}else {
					obj.setInsuredAmount(insureDetail.getSumInsure().toString());
				}
				if(obj.getInsuredAmount()==null){
					obj.setInsuredAmount("0");
				}
				obj.setPlanHeader(hdr);
				obj.setServiceBranch(insureDetail.getServiceBrach());
				obj.setChannelCode(insureDetail.getChannelCode());
				currentHitPlanList.add(obj);
				
			}
		}

   		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Other Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planCode = temp.getPlanCode();
			if (StringUtils.isNoneBlank(planCode)) {
				//Todo
				if ( checkSumByPlan.checkValueInParameter(RuleConstants.RULE_03.PA_FAMILY, planCode)){
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode() + " ,planCode :" + planCode + " ] is PaFamily  Plan";
					paFamilyPlanList.add(temp);
					sumInsuredPAFamily = sumInsuredPAFamily.add(new BigDecimal(temp.getInsuredAmount()));
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode() + " ,planCode :" + planCode + " ] not PaFamily Plan";
				}
			}
		}
		
		//0.  Map Project
		List<MapProject> mapProject = applicationCache.getMapProjectList();
		for (MapProject mapProjectlist : mapProject) {
			if(mapProjectlist.getServicBranch() != null ){
				if(! isOneOf( mapProjectlist.getServicBranch() , agenCode.getServiceBranch())){
					continue;
				}
			}
			
			if(mapProjectlist.getChannal() != null){
				if(!(mapProjectlist.getChannal().equalsIgnoreCase(agenCode.getAgentChannel()))){
					continue;
				}
			}
			
			if(mapProjectlist.getChannal() != null){
				if(! isOneOf( mapProjectlist.getChannal() , agenCode.getAgentChannel())){
					continue;
				}
			}
			
			if(mapProjectlist.getAgentCode() != null ){
				if(! isOneOf( mapProjectlist.getAgentCode() , agenCode.getAgentCode())){
					continue;
				}
			}
			
			if(mapProjectlist.getPlanCode() != null ){
				if(!mapProjectlist.getPlanCode().equalsIgnoreCase(basic.getPlanCode())){
					continue;
				}
			}
			
			if(mapProjectlist.getPlanCodeList() != null ){
				if( ! isOneOf(mapProjectlist.getPlanCodeList() , basic.getPlanCode()) ){
					continue;
				}
			}
			

			
			if(mapProjectlist.getCheckrider() != null){
				if(oneInOf(mapProjectlist.getCheckrider(), currentPlanList )){
					
					projectType = mapProjectlist.getProjectType();
					continue;
				}
			}
			
			if( mapProjectlist.getRiderplan() != null && ! "Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())  ){
				if(oneInOf(mapProjectlist.getRiderplan(), currentPlanList )){
					
					projectType = mapProjectlist.getProjectType();

				}
			}else if("Otherwise".equalsIgnoreCase(mapProjectlist.getRiderplan())){
				if(mapProjectlist.getPlanType() != null){
					if(checkTypeInsureDetail(mapProjectlist.getPlanType() , currentPlanList) ){
						
						projectType = mapProjectlist.getProjectType();
						
					}
				}
			}
			

		}
		
		
		//1. Check Plan permission
//		if(paFamilyPlanList.size()>0 && sumInsuredPAFamily.compareTo(sumInsuredMain) > 0 ){
//			//MessageCodeDesc("DT03036","");
//			List<String> param = new ArrayList<String>();
//			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03036 , param, param);
//			messageList.add(message);
//		}
		
		
		//2. Check AT03_PA_Family
		if(paFamilyPlanList.size()>0 && sumInsuredPAFamily.compareTo(sumInsuredMain) > 0 ){
			//MessageCodeDesc("DT03036","");
			List<String> param = new ArrayList<String>();
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03036 , param, param);
			messageList.add(message);
		}
		
		//3.Check DT03_Basic_Plan_SumInsured
		//ผ่าน
		for (InsureDetail temp : currentPlanList) {
			List<Dt03BasicPlanSumInsured> list = applicationCache.getRule03_dt03BasicPlanSumInsured();
			for (Dt03BasicPlanSumInsured basicPlanSumInsured : list) {
				if(basicPlanSumInsured.getPlanCode().contains(temp.getPlanCode())){
						if(basicPlanSumInsured.getMainSumInsured()!=null){
							if(sumInsuredMain.compareTo( new BigDecimal(basicPlanSumInsured.getMainSumInsured())) < 0){
								List<String> param = new ArrayList<String>();
								param.add(basicPlanSumInsured.getMainSumInsured());
								Message message = messageService.getMessage(registersystem, basicPlanSumInsured.getMessageCode() , param, param);
								messageList.add(message);
							}
						}
				}
			}
		}
		
	
		//4.Check DT03_Disorder_and_Accident
		//ผ่าน
		List<Dt03DisorderAndAccident> DisorderAndAccidentList = applicationCache.getRule03_dT03DisorderAndAccident();
		for (Dt03DisorderAndAccident disorderAndAccident : DisorderAndAccidentList) {
			for (InsureDetail tempA : currentPlanList) {
				if(tempA.getPlanHeader().getPlanType()!=null){
					if(tempA.getPlanHeader().getPlanType().equalsIgnoreCase(disorderAndAccident.getPlanTypeA()) && tempA.getPlanHeader().getPlanSystem().equalsIgnoreCase(disorderAndAccident.getSumInsuredA() ) ){
						for (InsureDetail tempB : currentPlanList) {
							if(tempB.getPlanHeader().getPlanType()!=null){
								if(tempB.getPlanHeader().getPlanType().equalsIgnoreCase(disorderAndAccident.getPlanTypeB()) && tempB.getPlanHeader().getPlanSystem().equalsIgnoreCase(disorderAndAccident.getSumInsuredB() ) ){
									
									BigDecimal planA = new BigDecimal( tempA.getInsuredAmount());
									BigDecimal PlanB = new BigDecimal( tempB.getInsuredAmount());
									PlanB = PlanB.multiply(new BigDecimal(disorderAndAccident.getLimit()));
									if(planA.compareTo(PlanB) > 0){
										List<String> param = new ArrayList<String>();
										param.add(tempA.getInsuredAmount());
										param.add(tempB.getInsuredAmount());
										//Todo  DT03068 ยังไม่มีในระบบ
										Message message = messageService.getMessage(registersystem, disorderAndAccident.getMessageCode() , param, param);
										messageList.add(message);
									}
								}
							}
						
						}
					}
				}
			}
		}
		
		
		
		//5.Check DT03_PaidUp_SumInsured
		//pass
		List<DT03PaidUpSumInsured> paidUpSumInsured = applicationCache.getRule03_dT03PaidUpSumInsured();
		for (DT03PaidUpSumInsured dt03PaidUpSumInsured : paidUpSumInsured) {
			if(isOneOf(dt03PaidUpSumInsured.getPlanCode(),basic.getPlanCode())){
				if(countPlanCodeInCoverageList(dt03PaidUpSumInsured.getCheckPlanLastYear(),currentHitPlanList )>1){
					if(maxSumInsurePlanCodeInCoverageList(dt03PaidUpSumInsured.getCheckPlanLastYear(),currentHitPlanList).multiply(new BigDecimal(dt03PaidUpSumInsured.getTimes())).compareTo(sumInsuredMain)<0  ){
						List<String> param = new ArrayList<String>();
						param.add(dt03PaidUpSumInsured.getTimes());
						Message message = messageService.getMessage(registersystem, dt03PaidUpSumInsured.getMessageCode() , param, param);
						messageList.add(message);
					}
				}
			}
		}
		
		
		//6.Check DT03_Plan_Code_SumInsured_History
		//ขาด  Msg
		List<DT03PlanCodeSumInsuredHistory> planCodeSumInsuredHistoryList = applicationCache.getRule03_dT03PlanCodeSumInsuredHistory();
		for (DT03PlanCodeSumInsuredHistory dt03PlanCodeSumInsuredHistory : planCodeSumInsuredHistoryList) {

				if(oneInOf( dt03PlanCodeSumInsuredHistory.getPlanCode() , currentPlanList) ){
					
					InsureDetail tempA = oneInOfReturnPlan( dt03PlanCodeSumInsuredHistory.getPlanCode() , currentPlanList);
					
						if(dt03PlanCodeSumInsuredHistory.getBranch()!=null && agenCode.getServiceBranch() != null){
		
							if(dt03PlanCodeSumInsuredHistory.getBranch().equalsIgnoreCase( tempA.getServiceBranch())){
								if(isOneOf(dt03PlanCodeSumInsuredHistory.getPlanCode(),tempA.getPlanCode())){
									 BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
									 InsuredByPlanCode = getSumInsuredByPlanCode(dt03PlanCodeSumInsuredHistory.getSumByPlanCode(),currentHitPlanList );
										if (InsuredByPlanCode.compareTo(new BigDecimal(dt03PlanCodeSumInsuredHistory.getLimit())) > 0) {
											List<String> param = new ArrayList<String>();
											if("DT03909".equals(dt03PlanCodeSumInsuredHistory.getMessageCode())){
												param.add(dt03PlanCodeSumInsuredHistory.getMessageDesc());
											}
											param.add(dt03PlanCodeSumInsuredHistory.getLimit());
											param.add(InsuredByPlanCode.toString());
											Message message = messageService.getMessage(registersystem, dt03PlanCodeSumInsuredHistory.getMessageCode() , param, param);
											messageList.add(message);
										}
								}	
							}
							
						}else{
							if(isOneOf(dt03PlanCodeSumInsuredHistory.getPlanCode(),tempA.getPlanCode())){
								
								if(dt03PlanCodeSumInsuredHistory.getLimit() != null){
									 BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
									 InsuredByPlanCode = getSumInsuredByPlanCode(dt03PlanCodeSumInsuredHistory.getSumByPlanCode(),currentHitPlanList );
										if (InsuredByPlanCode.compareTo(new BigDecimal(dt03PlanCodeSumInsuredHistory.getLimit())) > 0) {
											List<String> param = new ArrayList<String>();
											if("DT03909".equals(dt03PlanCodeSumInsuredHistory.getMessageCode())){
												param.add(dt03PlanCodeSumInsuredHistory.getMessageDesc());
											}
											param.add(dt03PlanCodeSumInsuredHistory.getLimit());
											param.add(InsuredByPlanCode.toString());
											Message message = messageService.getMessage(registersystem, dt03PlanCodeSumInsuredHistory.getMessageCode() , param, param);
											messageList.add(message);
										}
								}
							
									
									
							}	
						}
						
						if(dt03PlanCodeSumInsuredHistory.getMultiLimit() != null){
							
							 BigDecimal insuredBylimit = BigDecimal.ZERO;
							 BigDecimal insuredByMain = BigDecimal.ZERO;
							 insuredBylimit = getSumInsuredByPlanCode(dt03PlanCodeSumInsuredHistory.getSumByPlanCode(),currentHitPlanList );
							 
							 insuredByMain = sumInsuredMain.multiply(new BigDecimal(dt03PlanCodeSumInsuredHistory.getMultiLimit()));
							 if (insuredBylimit.compareTo( insuredByMain ) > 0) {
									List<String> param = new ArrayList<String>();
									if("DT03909".equals(dt03PlanCodeSumInsuredHistory.getMessageCode())){
										param.add(dt03PlanCodeSumInsuredHistory.getMessageDesc());
									}
									param.add(insuredByMain.toString());
									param.add(insuredBylimit.toString());
									Message message = messageService.getMessage(registersystem, dt03PlanCodeSumInsuredHistory.getMessageCode() , param, param);
									messageList.add(message);
							 }
						}
						
						
					}
		}
		
		
		//7.Check DT03_Plan_Type_SumInsured_History
		List<DT03PlanTypeeSumInsuredHistory> planTypeeSumInsuredHistory = applicationCache.getRule03_dT03PlanTypeeSumInsuredHistory();
		for (DT03PlanTypeeSumInsuredHistory dt03PlanTypeeSumInsuredHistory : planTypeeSumInsuredHistory) {
			
			//RULE3
			if(dt03PlanTypeeSumInsuredHistory.getSystemName().equals(registersystem) ){
				if(dt03PlanTypeeSumInsuredHistory.getAccident() !=null){
					 BigDecimal insuredByPlanType = BigDecimal.ZERO;
					 insuredByPlanType = getSumInsuredByPlanType( RuleConstants.RULE_03.ACCIDENT_PLAN_TYPE ,currentHitPlanList, null );
						if (insuredByPlanType.compareTo(new BigDecimal(dt03PlanTypeeSumInsuredHistory.getAccident())) > 0) {
							if(checkPlanTypeInCurrentRequest(dt03PlanTypeeSumInsuredHistory.getPlanType() ,  dt03PlanTypeeSumInsuredHistory.getPlanSystem() , currentHitPlanList)){
								//todo
								if(  dt03PlanTypeeSumInsuredHistory.getMultiPlanType() != null  && ! "Otherwise".equalsIgnoreCase(dt03PlanTypeeSumInsuredHistory.getSumPlanType()) ){
									 BigDecimal insuredBylimit = BigDecimal.ZERO;
									 insuredBylimit = getSumInsuredByPlanType( dt03PlanTypeeSumInsuredHistory.getMultiPlanType() ,currentHitPlanList, null );
									 if (insuredBylimit.compareTo(new BigDecimal(dt03PlanTypeeSumInsuredHistory.getSumLimit() )) > 0) {
											List<String> param = new ArrayList<String>();
											//Todo  DT03068 ยังไม่มีในระบบ
											param.add("1");
											Message message = messageService.getMessage(registersystem, dt03PlanTypeeSumInsuredHistory.getMessageCode() , param, param);
											messageList.add(message);
									 }
									 
								}
								
							}else if("Otherwise".equalsIgnoreCase(dt03PlanTypeeSumInsuredHistory.getPlanType())){
								//todo
								if( dt03PlanTypeeSumInsuredHistory.getSumPlanType() != null && ! "Otherwise".equalsIgnoreCase(dt03PlanTypeeSumInsuredHistory.getSumPlanType())  ){
									 BigDecimal insuredBylimitSum = BigDecimal.ZERO;
									 insuredBylimitSum = getSumInsuredByPlanType( dt03PlanTypeeSumInsuredHistory.getSumPlanType(),currentHitPlanList, null );
									 if (insuredBylimitSum.compareTo(new BigDecimal(dt03PlanTypeeSumInsuredHistory.getSumLimit() )) > 0) {
											List<String> param = new ArrayList<String>();
											//Todo  DT03068 ยังไม่มีในระบบ
											param.add("1");
											
											Message message = messageService.getMessage(registersystem, dt03PlanTypeeSumInsuredHistory.getMessageCode() , param, param);
											messageList.add(message);
									 }	
								}
								
						}
					 }
				}
			}else if(dt03PlanTypeeSumInsuredHistory.getSystemName().equals("Otherwise")){
				
				
//				System.out.println(dt03PlanTypeeSumInsuredHistory.getPlanType());
				
				if(checkPlanTypeInCurrentRequest(dt03PlanTypeeSumInsuredHistory.getPlanType() ,  dt03PlanTypeeSumInsuredHistory.getPlanSystem() , currentPlanList)){
					//Con
					if( dt03PlanTypeeSumInsuredHistory.getSumPlanType() != null && ! "Otherwise".equalsIgnoreCase(dt03PlanTypeeSumInsuredHistory.getSumPlanType())  ){
						 BigDecimal insuredBylimitSum = BigDecimal.ZERO;
						 insuredBylimitSum = getSumInsuredByPlanType( dt03PlanTypeeSumInsuredHistory.getSumPlanType(),currentHitPlanList, null );
						 if (insuredBylimitSum.compareTo(new BigDecimal(dt03PlanTypeeSumInsuredHistory.getSumLimit() )) > 0) {
								List<String> param = new ArrayList<String>();
								//Todo  DT03068 ยังไม่มีในระบบ
								param.add(dt03PlanTypeeSumInsuredHistory.getSumLimit());
								param.add(insuredBylimitSum.toString());
								Message message = messageService.getMessage(registersystem, dt03PlanTypeeSumInsuredHistory.getMessageCode() , param, param);
								messageList.add(message);
						 }	
					}
					
					if(  dt03PlanTypeeSumInsuredHistory.getMultiPlanType() != null  && ! "Otherwise".equalsIgnoreCase(dt03PlanTypeeSumInsuredHistory.getMultiPlanType()) ){
						 BigDecimal insuredBylimit = BigDecimal.ZERO;
						 BigDecimal insuredByMain = BigDecimal.ZERO;
						 insuredBylimit = getSumInsuredByPlanType( dt03PlanTypeeSumInsuredHistory.getMultiPlanType() ,currentHitPlanList, null );
						 
						 insuredByMain = sumInsuredMain.multiply(new BigDecimal(dt03PlanTypeeSumInsuredHistory.getMultiSystemNameLimit()));
						 if (insuredBylimit.compareTo(insuredByMain) > 0) {
								List<String> param = new ArrayList<String>();
								//Todo  DT03068 ยังไม่มีในระบบ
								param.add(insuredByMain.toString());
								param.add(insuredBylimit.toString());
								Message message = messageService.getMessage(registersystem, dt03PlanTypeeSumInsuredHistory.getMessageCode() , param, param);
								messageList.add(message);
						 }
						 
					}
					
				}
				
			}

		}
		

		
		//8.Check DT03_Project_SumIndured
			//Today
		List<DT03ProjectSumIndured> projectSumIndured = applicationCache.getRule03_dT03ProjectSumIndured();
		for (DT03ProjectSumIndured dt03ProjectSumIndured : projectSumIndured) {
			if(basic.getPlanCode().equalsIgnoreCase(dt03ProjectSumIndured.getPlanCode())){
				for (InsureDetail current : rider) {
					if(current.getPlanCode().equals(dt03ProjectSumIndured.getRiderPlanCode())&&agenCode!=null){
						if(dt03ProjectSumIndured.getServiceBranch().equals(agenCode.getServiceBranch()) && dt03ProjectSumIndured.getAgentCode().equals(agenCode.getAgentCode())){
							
							if(dt03ProjectSumIndured.getPlanType()!=null){
								if(checkPlanTypeInCurrentRequest(dt03ProjectSumIndured.getPlanType(), dt03ProjectSumIndured.getPlanSystem() , currentPlanList)){
									
									 BigDecimal InsuredByPlanType = BigDecimal.ZERO;
									 InsuredByPlanType = getSumInsuredByPlanType(dt03ProjectSumIndured.getPlanType() , currentHitPlanList, dt03ProjectSumIndured.getPlanSystem() );
									
									 if (InsuredByPlanType.compareTo(new BigDecimal(dt03ProjectSumIndured.getSumInsured())) > 0) {
											List<String> param = new ArrayList<String>();
											param.add(current.getPlanCode());
											param.add(dt03ProjectSumIndured.getSumInsured());
											param.add(InsuredByPlanType.toString());
											//Todo  DT03068 ยังไม่มีในระบบ
											Message message = messageService.getMessage(registersystem, dt03ProjectSumIndured.getMessageCode() , param, param);
											messageList.add(message);
										}
								}
							}else{
								BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
								InsuredByPlanCode = getSumInsuredByPlanCode(dt03ProjectSumIndured.getRiderPlanCode(),currentHitPlanList );
								if(InsuredByPlanCode.compareTo(new BigDecimal(dt03ProjectSumIndured.getLimit())) > 0){
									List<String> param = new ArrayList<String>();
									//Todo  DT03068 ยังไม่มีในระบบ
									param.add(current.getPlanCode());
									param.add(dt03ProjectSumIndured.getLimit());
									param.add(InsuredByPlanCode.toString());
									Message message = messageService.getMessage(registersystem, dt03ProjectSumIndured.getMessageCode() , param, param);
									messageList.add(message);
								}
							}

						}
					}
				}
			}
		}
		

		//9.Check DT03_Rider_Plan_SumInsured
		List<DT03RiderPlanSumInsured> riderPlanSumInsured = applicationCache.getRule03_dT03_RiderPlanSumInsured();
		for (InsureDetail riderlist : rider) {
			for (DT03RiderPlanSumInsured riderPlanSumInsuredlist : riderPlanSumInsured) {
				boolean statusPlan = false;
				if (riderPlanSumInsuredlist.getPlanCode() != null) {
					if (riderPlanSumInsuredlist.getPlanCode().equals(riderlist.getPlanCode())) {
						statusPlan = true;
					}
				} else {
					if (riderlist.getPlanHeader().getPlanType() != null) {
						if (isOneOf(riderPlanSumInsuredlist.getPlanType(), riderlist.getPlanHeader().getPlanType())) {
							statusPlan = true;
						}
					}

				}

				if (statusPlan) {

					// todo Project Type
					if (riderPlanSumInsuredlist.getProjectType() != null) {
						if (projectType != null) {
							if (!riderPlanSumInsuredlist.getProjectType().equals(projectType)) {
								continue;
							}
						} else {
							continue;
						}
					}

					// Check main plan
					if (riderPlanSumInsuredlist.getMainPlan() != null) {
						if (!(riderPlanSumInsuredlist.getMainPlan().equals(basic.getPlanCode()))) {
							continue;
						}
					}

					// Check Chanel
					if (riderPlanSumInsuredlist.getChannel() != null) {
						if (!(isOneOfOrBlank(riderPlanSumInsuredlist.getChannel(), agenCode.getAgentChannel()))) {
							continue;
						}
					}

					// Check AgentCode
					if (riderPlanSumInsuredlist.getAgentCode() != null) {
						if (!(isOneOfOrBlank(riderPlanSumInsuredlist.getAgentCode(), agenCode.getAgentCode()))) {
							continue;
						}
					}

					// Check sum insured rate more than
					if (riderPlanSumInsuredlist.getSumInsuredRateMoreThan() != null) {

						BigDecimal riderAmount = new BigDecimal(riderlist.getInsuredAmount());
						BigDecimal mainAmount = sumInsuredMain
								.multiply(new BigDecimal(riderPlanSumInsuredlist.getSumInsuredRateMoreThan()));
						if (riderAmount.compareTo(mainAmount) > 0) {
							List<String> param = new ArrayList<String>();
							param.add(riderlist.getPlanCode());
							param.add(riderPlanSumInsuredlist.getSumInsuredRateMoreThan());
							Message message = messageService.getMessage(registersystem,
									riderPlanSumInsuredlist.getMessageCode(), param, param);
							messageList.add(message);
						}

					} else if (riderPlanSumInsuredlist.getSumInsuredRate() != null) {

						BigDecimal riderAmount = new BigDecimal(riderlist.getInsuredAmount());
						BigDecimal mainAmount = sumInsuredMain
								.multiply(new BigDecimal(riderPlanSumInsuredlist.getSumInsuredRate()));
						mainAmount = rounding(mainAmount, 0);
						if (riderAmount.compareTo(mainAmount) != 0) {
							List<String> param = new ArrayList<String>();
							param.add(riderlist.getPlanCode());
							param.add(riderPlanSumInsuredlist.getSumInsuredRate());
							Message message = messageService.getMessage(registersystem,
									riderPlanSumInsuredlist.getMessageCode(), param, param);
							messageList.add(message);
						}

					} else if (riderPlanSumInsuredlist.getRoundingUp() != null) {

						BigDecimal riderAmount = new BigDecimal(riderlist.getInsuredAmount());
						BigDecimal mainAmount = sumInsuredMain
								.multiply(new BigDecimal(riderPlanSumInsuredlist.getRoundingUp()));
						mainAmount = rounding(mainAmount, 0);
						if (riderAmount.compareTo(mainAmount) != 0) {
							List<String> param = new ArrayList<String>();
							param.add(riderlist.getPlanCode());
							param.add(riderPlanSumInsuredlist.getRoundingUp());
							Message message = messageService.getMessage(registersystem,
									riderPlanSumInsuredlist.getMessageCode(), param, param);
							messageList.add(message);
						}
					}

				}
			}
		}

		//10.Check DT03_SumInsured_with_Age
			//Today
		List<DT03SumInsuredWithAge> sumInsuredWithAge = applicationCache.getRule03_dT03_SumInsuredWithAge();
		for (DT03SumInsuredWithAge sumInsuredWithAgelist : sumInsuredWithAge) {
			if(oneInOf(sumInsuredWithAgelist.getMainPlan(),currentPlanList)){
				
			if(sumInsuredWithAgelist.getMainSumInsured()!=null)	{
				if(sumInsuredWithAgelist.getAge() !=null && !"Otherwise".equalsIgnoreCase(sumInsuredWithAgelist.getAge())){
					if(Integer.parseInt(age) <  Integer.parseInt(sumInsuredWithAgelist.getAge() )){
						
						BigDecimal data1 = new BigDecimal( basic.getInsuredAmount() );
						BigDecimal data2 = new BigDecimal( sumInsuredWithAgelist.getMainSumInsured() );
						
						if(data1.compareTo(data2) > 0 ){
//						if( Integer.parseInt(basic.getInsuredAmount()) > Integer.parseInt(sumInsuredWithAgelist.getMainSumInsured())  ){
							
							
							List<String> param = new ArrayList<String>();
							//Todo  DT03068 ยังไม่มีในระบบ
							param.add(sumInsuredWithAgelist.getMainPlan());
							Message message = messageService.getMessage(registersystem, sumInsuredWithAgelist.getMessageCode() , param, param);
							messageList.add(message);
							break;	
						}
					}
				}
				
			}else{
				if(sumInsuredWithAgelist.getAge() !=null && !"Otherwise".equalsIgnoreCase(sumInsuredWithAgelist.getAge())){
					if(Integer.parseInt(age) <=  Integer.parseInt(sumInsuredWithAgelist.getAge() )){
						BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
						InsuredByPlanCode = getSumInsuredByPlanCode(sumInsuredWithAgelist.getMainPlan(),currentHitPlanList );
						if(InsuredByPlanCode.compareTo(new BigDecimal(sumInsuredWithAgelist.getHistorySumInsured())) > 0){
							List<String> param = new ArrayList<String>();
							param.add(basic.getPlanCode());
							param.add(InsuredByPlanCode.toString());
							//Todo  DT03068 ยังไม่มีในระบบ
							Message message = messageService.getMessage(registersystem, sumInsuredWithAgelist.getMessageCode() , param, param);
							messageList.add(message);
							break;
						}
					}
				}else if("Otherwise".equalsIgnoreCase(sumInsuredWithAgelist.getAge())){
					BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
					InsuredByPlanCode = getSumInsuredByPlanCode(sumInsuredWithAgelist.getMainPlan(),currentHitPlanList );
					if(InsuredByPlanCode.compareTo(new BigDecimal(sumInsuredWithAgelist.getHistorySumInsured())) > 0){
						List<String> param = new ArrayList<String>();
						param.add(basic.getPlanCode());
						param.add(InsuredByPlanCode.toString());
						//Todo  DT03068 ยังไม่มีในระบบ
						Message message = messageService.getMessage(registersystem, sumInsuredWithAgelist.getMessageCode() , param, param);
						messageList.add(message);
						break;
					}
				}
			}
			}
		}
		
		//11.Check DT03_SumInsured_with_Must_Have_Rider
			//Today
		List<DT03SumInsuredWithMustHaveRider> sumInsuredWithMustHaveRider = applicationCache.getRule03_dT03_SumInsuredWithMustHaveRider();
		for (DT03SumInsuredWithMustHaveRider dt03SumInsuredWithMustHaveRider : sumInsuredWithMustHaveRider) {
			if(basic.getPlanCode().equalsIgnoreCase(dt03SumInsuredWithMustHaveRider.getBasicPlan())){
				if(basic.getInsuredAmount().equals(dt03SumInsuredWithMustHaveRider.getSumInsured())){
					if (rider != null) {
						
						for(InsureDetail insureDetail : rider ){					
							if(!isOneOf(dt03SumInsuredWithMustHaveRider.getNotHavePlanCode(),insureDetail.getPlanCode())){
								List<String> param = new ArrayList<String>();
								//Todo  DT03068 ยังไม่มีในระบบ
								param.add(dt03SumInsuredWithMustHaveRider.getSumInsured());
								Message message = messageService.getMessage(registersystem, dt03SumInsuredWithMustHaveRider.getMessageCode() , param, param);
								messageList.add(message);
								break;
							}
						}
						
						
//						if(oneInOf(dt03SumInsuredWithMustHaveRider.getNotHavePlanCode(), currentPlanList) == false){
//							
//						}
					}
				}
			}
		}
		
		
		//12.Check DT03_Fix Sum Insured 
		List<DT03FixSumInsured> fixSumInsured = applicationCache.getRule03_dT03_FixSumInsured();
		
		boolean resultA = false ;
		boolean resultB = false ;
		
		boolean resultD = true ;
		boolean resultE = true ;
		boolean resultF = true ;
		
		String msgD = null;
		String msgE = null;
		String msgF = null;
		
		String msgDescD = null;
		String msgDescE = null;
		String msgDescF = null;
		
		String msgPlanD = null;
		
		for (DT03FixSumInsured dt03FixSumInsured : fixSumInsured) {
			if(oneInOf(dt03FixSumInsured.getPlanMain(), currentPlanList)){
				resultA = false;
				if(oneInOf(dt03FixSumInsured.getPlanRider(), rider)){
					
					if( dt03FixSumInsured.getDPlan() !=null && !"Otherwise".equalsIgnoreCase(dt03FixSumInsured.getDPlan())   ){
						BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
						InsuredByPlanCode = getSumInsuredByPlanCode( dt03FixSumInsured.getPlanRider() ,currentPlanList );

						if ( (InsuredByPlanCode.compareTo( new BigDecimal( dt03FixSumInsured.getDAmountA()) ) > 0 
							     && InsuredByPlanCode.compareTo( new BigDecimal( dt03FixSumInsured.getDAmountB()) ) < 0) ) {
							resultD = false ;
						}
					}
					if( "Otherwise".equalsIgnoreCase(dt03FixSumInsured.getDPlan())   ){
						msgD = dt03FixSumInsured.getMessageCode();
						msgDescD = dt03FixSumInsured.getMessageDesc();
						msgPlanD = dt03FixSumInsured.getDPlan();
					}
					
				}else{
					BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
					if(dt03FixSumInsured.getEPlan()!=null){
						InsuredByPlanCode = getSumInsuredByPlanCode( dt03FixSumInsured.getEPlan() ,currentPlanList );
						if(!"Otherwise".equals(dt03FixSumInsured.getEAmountE()) && dt03FixSumInsured.getEAmountE()!= null ){
							if(InsuredByPlanCode.compareTo(new BigDecimal(dt03FixSumInsured.getEAmountE())) == 0  ){
								resultE = false ;
									BigDecimal InsuredByPlanCodeF = BigDecimal.ZERO;
									InsuredByPlanCodeF = getSumInsuredByPlanCode( dt03FixSumInsured.getFPlan() ,currentPlanList );
									if(!"Otherwise".equals(dt03FixSumInsured.getFAmountE()) && dt03FixSumInsured.getFAmountE()!= null ){
										if(InsuredByPlanCodeF.compareTo(new BigDecimal(dt03FixSumInsured.getFAmountE())) == 0){
											resultF = false ;
											break;
										}
									}
								
							}
						}
						
						if( "Otherwise".equalsIgnoreCase(dt03FixSumInsured.getEPlan())   ){
							msgE = dt03FixSumInsured.getMessageCode();
							msgDescE = dt03FixSumInsured.getMessageDesc();
						}
						
						if( "Otherwise".equalsIgnoreCase(dt03FixSumInsured.getFPlan())   ){
							msgF = dt03FixSumInsured.getMessageCode();
							msgDescF = dt03FixSumInsured.getMessageDesc();
						}
						
						
						
					}

					
					
				}
			}
			
		}
		
		if(resultD   && !resultA ){
			if(msgD !=null){
				List<String> param = new ArrayList<String>();
				param = mapMessageCodeDesc(msgDescD);
				param.add(getSumInsuredByPlanCode( msgPlanD ,currentPlanList ).toString());
				Message message = messageService.getMessage(registersystem, msgD , param, param);
				messageList.add(message);
			}
		}
		
		if(resultE  && !resultA  ){
			if(msgE !=null){
				List<String> param = new ArrayList<String>();
				param.add(getSumInsuredByPlanCode( msgDescE ,currentPlanList ).toString());
				Message message = messageService.getMessage(registersystem, msgE , param, param);
				messageList.add(message);
			}
			
		}
		if(resultF && !resultA && !resultE){
			if(msgF !=null){
				List<String> param = new ArrayList<String>();
				param.add(getSumInsuredByPlanCode( msgDescF ,currentPlanList ).toString());
				Message message = messageService.getMessage(registersystem, msgF , param, param);
				messageList.add(message);
			}
			
		}
		
		//13.Check DT03_Package_HS_Limit 
		List<DT03PackageHSLimit> packageHSLimit = applicationCache.getRule03_dT03_PackageHSLimit();
		for (DT03PackageHSLimit dt03PackageHSLimit : packageHSLimit) {
			if(oneInOf(dt03PackageHSLimit.getAPlanCode(), currentPlanList)){
				if(dt03PackageHSLimit.getBRider()!=null){
					if(oneInOf(dt03PackageHSLimit.getBRider(), rider)){
						if(projectType!=null){
							if(isOneOf(dt03PackageHSLimit.getCProjectType(), projectType)){
								BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
								BigDecimal InsuredByService = BigDecimal.ZERO;
								InsuredByPlanCode = getSumInsuredByPlanCode(dt03PackageHSLimit.getDPlanHistory(),currentHitPlanList );
								InsuredByService = getSumInsuredByPlanCodeAndServiceBranch(dt03PackageHSLimit.getDPlanBrach(),currentHitPlanList,dt03PackageHSLimit.getDBranch() );
								
								BigDecimal sumInsured = InsuredByPlanCode.subtract(InsuredByService);
								if(sumInsured.compareTo(new BigDecimal(dt03PackageHSLimit.getDAmount())) > 0){
									List<String> param = new ArrayList<String>();
									//Todo  DT03068 ยังไม่มีในระบบ
									
									param = mapMessageCodeDesc(dt03PackageHSLimit.getMessageDesc());
									Message message = messageService.getMessage(registersystem, dt03PackageHSLimit.getMessageCode() , param, param);
									messageList.add(message);
									break;
								}
								
							}
						}
						
					}
				}else{
					if(projectType!=null){
						if(isOneOf(dt03PackageHSLimit.getCProjectType(), "")){
							BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
							BigDecimal InsuredByService = BigDecimal.ZERO;
							InsuredByPlanCode = getSumInsuredByPlanCode(dt03PackageHSLimit.getDPlanHistory(),currentHitPlanList );
							InsuredByService = getSumInsuredByPlanCodeAndServiceBranch(dt03PackageHSLimit.getDPlanBrach(),currentHitPlanList,dt03PackageHSLimit.getDBranch() );
							
							BigDecimal sumInsured = InsuredByPlanCode.subtract(InsuredByService);
							if(sumInsured.compareTo(new BigDecimal(dt03PackageHSLimit.getDAmount())) > 0){
								List<String> param = new ArrayList<String>();
								//Todo  DT03068 ยังไม่มีในระบบ
								Message message = messageService.getMessage(registersystem, dt03PackageHSLimit.getMessageCode() , param, param);
								messageList.add(message);
								break;
							}
							
						}
					}
					
				}
				
			}
		}

		
		//15.Check DT03_PRODUCT_AMLO
		List<DT03ProductAMLO> productAMLO = applicationCache.getRule03_dT03_ProductAMLO();
		for (DT03ProductAMLO dt03ProductAMLO : productAMLO) {
			BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
			InsuredByPlanCode = getSumInsuredByPlanCode(dt03ProductAMLO.getPlanCode(),currentPlanList );
			if(InsuredByPlanCode.compareTo(new BigDecimal(dt03ProductAMLO.getAmount())) >= 0){
				List<String> param = new ArrayList<String>();
				param.add(dt03ProductAMLO.getPlanCode());
				Message message = messageService.getMessage(registersystem, dt03ProductAMLO.getMessageCode() , param, param);
				messageList.add(message);
				
				message = messageService.getMessage(registersystem, "REFERUW" , param, param);
				messageList.add(message);
				break;
			}
		}
		
		
		//16.DT03_Channel_SumInsured
		List<DT03ChannelSumInsured> channelSumInsured = applicationCache.getRule03_dT03_ChannelSumInsured();
		for (DT03ChannelSumInsured dt03ChannelSumInsured : channelSumInsured) {
			
			BigDecimal Insured = BigDecimal.ZERO;
			Insured = getSuminsuredHistoryByChannel(dt03ChannelSumInsured.getPlanCode() ,dt03ChannelSumInsured.getChannel() ,currentHitPlanList );
			if(Insured.compareTo(new BigDecimal(dt03ChannelSumInsured.getAmount())) > 0){
				List<String> param = new ArrayList<String>();
				//Todo  DT03068 ยังไม่มีในระบบ
				param.add(dt03ChannelSumInsured.getChannel());
				param.add(dt03ChannelSumInsured.getAmount());
				param.add(Insured.toString());
				Message message = messageService.getMessage(registersystem, dt03ChannelSumInsured.getMessageCode() , param, param);
				messageList.add(message);
				if("TRUE".equalsIgnoreCase(dt03ChannelSumInsured.getReferUnderwriter())){
					 message = messageService.getMessage(registersystem, "REFERUW" , param, param);
					 messageList.add(message);
				}
				
			}
		}
		
		//17.DT03_Channel_SumInsured_equal
		List<DT03ChannelSumInsuredEqual> channelSumInsuredEqual = applicationCache.getRule03_dT03_ChannelSumInsuredEqual();
		for (DT03ChannelSumInsuredEqual dt03ChannelSumInsuredEqual : channelSumInsuredEqual) {
			if(agenCode!=null){
				if(isOneOf(dt03ChannelSumInsuredEqual.getChanel() ,agenCode.getAgentChannel()) && oneInOf(dt03ChannelSumInsuredEqual.getPlanCode(),currentPlanList)){
					if(oneInOf(dt03ChannelSumInsuredEqual.getRiderPlan(),rider)){
						BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
						InsuredByPlanCode = getSumInsuredByPlanCode(dt03ChannelSumInsuredEqual.getPlanCode(),currentHitPlanList );
						if(InsuredByPlanCode.compareTo(new BigDecimal(dt03ChannelSumInsuredEqual.getBasicSumInsured())) > 0){
							List<String> param = new ArrayList<String>();
							//Todo  DT03068 ยังไม่มีในระบบ
							
							param.add(dt03ChannelSumInsuredEqual.getChanel());
							param.add(dt03ChannelSumInsuredEqual.getBasicSumInsured());
							param.add(InsuredByPlanCode.toString());
							
							Message message = messageService.getMessage(registersystem, dt03ChannelSumInsuredEqual.getMessageCode() , param, param);
							messageList.add(message);
							break;
						}
					}
				}

			}
		}

		//20.DT03_Risk_Diagnosis_Plan_Type_Electornic
		List<DT03RiskDiagnosisPlanType> riskDiagnosisPlanType = applicationCache.getRule03_dT03_riskDiagnosisPlanType();
		for (DT03RiskDiagnosisPlanType dt03RiskDiagnosisPlanType : riskDiagnosisPlanType) {
			if(agenCode!=null){
				if(dt03RiskDiagnosisPlanType.getBranch().equalsIgnoreCase(agenCode.getServiceBranch())){
					if(checkSuminsuredType(dt03RiskDiagnosisPlanType.getPlanType() , new BigDecimal(dt03RiskDiagnosisPlanType.getLowerAge()), new BigDecimal(dt03RiskDiagnosisPlanType.getHigherAge()), new BigDecimal( age ) , currentHitPlanList)){
						
						BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
						InsuredByPlanCode = getSumInsuredByPlanCodeAndServiceBranch(dt03RiskDiagnosisPlanType.getProjectType(),currentHitPlanList ,dt03RiskDiagnosisPlanType.getBranch() );
						if(InsuredByPlanCode.compareTo(new BigDecimal(dt03RiskDiagnosisPlanType.getAmount())) > 0){
							List<String> param = new ArrayList<String>();
							//Todo  DT03068 ยังไม่มีในระบบ
							param.add(dt03RiskDiagnosisPlanType.getAmount());
							Message message = messageService.getMessage(registersystem, dt03RiskDiagnosisPlanType.getMessageCode() , param, param);
							messageList.add(message);
							break;
						}
					}
				}
			}
		}
	
		
		if(projectType!=null){
			if("TAKAPS".equals(projectType)){
				if(ClientPF != null){
					//
					
					boolean resultAAAB =false;
					String codeAAAB ="";
					String insurAAAB = "";
					
					boolean resultA1A2A3 = false;
					String codeA1A2A3 = "";
					String insurA1A2A3 = "";
					
					List<DT03AccidentForTakaful> dHealthAgentGroupList = applicationCache.getRule03_dT03AccidentForTakaful();
					for (DT03AccidentForTakaful dt03AccidentForTakaful : dHealthAgentGroupList) {
						if(projectType.equals(dt03AccidentForTakaful.getProjectType())){
							if(!"Otherwise".equals(dt03AccidentForTakaful.getAaab()) && dt03AccidentForTakaful.getAaab()!= null){
								
								BigDecimal InsuredByPlanType = BigDecimal.ZERO;
								InsuredByPlanType = getSumInsuredByPlanType("AA,AB", currentHitPlanList , null);
								if(InsuredByPlanType.compareTo(new BigDecimal(dt03AccidentForTakaful.getAaab())) <=  0){
									if(!"Otherwise".equals(dt03AccidentForTakaful.getA1a2a3())  && dt03AccidentForTakaful.getA1a2a3()!= null ){
										
										BigDecimal InsuredByPlanType2 = BigDecimal.ZERO;
										InsuredByPlanType2 = getSumInsuredByPlanType("A1,A2,A3", currentHitPlanList , null);
										if(InsuredByPlanType2.compareTo(new BigDecimal(dt03AccidentForTakaful.getA1a2a3())) <= 0){
										
										}else{
											resultA1A2A3 = true ;
											insurA1A2A3 = InsuredByPlanType2.toString();
										}
										
									}
									
								}else{
									resultAAAB = true ;
									insurAAAB = InsuredByPlanType.toString();
								}

							}
						}
						
						if("Otherwise".equals(dt03AccidentForTakaful.getAaab())){
							codeAAAB = dt03AccidentForTakaful.getMessageCode();
						}
						if("Otherwise".equals(dt03AccidentForTakaful.getA1a2a3())){
							codeA1A2A3 = dt03AccidentForTakaful.getMessageCode(); 
						}
						
					}
					
					if(resultAAAB){
						List<String> param = new ArrayList<String>();
//						param.add(dt03RiskDiagnosisPlanType.getAmount());
						param.add(insurAAAB);
						Message message = messageService.getMessage(registersystem, codeAAAB , param, param);
						messageList.add(message);
					}
					if(resultA1A2A3){
						List<String> param = new ArrayList<String>();
//						param.add(dt03RiskDiagnosisPlanType.getAmount());
						param.add(insurA1A2A3);
						Message message = messageService.getMessage(registersystem, codeA1A2A3 , param, param);
						messageList.add(message);
					}
					
				}else{
					//
					
					boolean resultAAAB =false;
					String codeAAAB ="";
					String insurAAAB = "";
					
					boolean resultA1A2A3 = false;
					String codeA1A2A3 = "";
					String insurA1A2A3 = "";
					
					
					List<DT03AccidentForTakafulNc> dHealthAgentGroupList = applicationCache.getRule03_dT03AccidentForTakafulNc();
					for (DT03AccidentForTakafulNc dt03AccidentForTakafulNc : dHealthAgentGroupList) {

						if(projectType.equals(dt03AccidentForTakafulNc.getProjectType())){
							
							if(!"Otherwise".equals(dt03AccidentForTakafulNc.getPlanCode())  && dt03AccidentForTakafulNc.getPlanCode()!= null){
								
								if( oneInOf(dt03AccidentForTakafulNc.getPlanCode(), currentPlanList) ){
									if(!"Otherwise".equals(dt03AccidentForTakafulNc.getPlantypeInList())  && dt03AccidentForTakafulNc.getA1a2a3()!= null){
										if( oneInOfType(dt03AccidentForTakafulNc.getPlantypeInList(), currentPlanList) ){
											if(!"Otherwise".equals(dt03AccidentForTakafulNc.getAaab()) && dt03AccidentForTakafulNc.getAaab()!= null){
												
												
												BigDecimal InsuredByPlanType = BigDecimal.ZERO;
												InsuredByPlanType = getSumInsuredByPlanType("AA,AB", currentHitPlanList , null);
												if(InsuredByPlanType.compareTo(new BigDecimal(dt03AccidentForTakafulNc.getAaab())) <= 0){
													if(!"Otherwise".equals(dt03AccidentForTakafulNc.getA1a2a3())  && dt03AccidentForTakafulNc.getA1a2a3()!= null ){
														
														BigDecimal InsuredByPlanType2 = BigDecimal.ZERO;
														InsuredByPlanType2 = getSumInsuredByPlanType("A1,A2,A3", currentHitPlanList , null);
														if(InsuredByPlanType2.compareTo(new BigDecimal(dt03AccidentForTakafulNc.getA1a2a3())) <= 0){
														
														}else{
															resultA1A2A3 = true ;
															insurA1A2A3 = InsuredByPlanType2.toString();
														}
														
													}
													
												}else{
													resultAAAB = true ;
													insurAAAB = InsuredByPlanType.toString();
												}

											}
										}
									}
								}
							}
						}
						
						if("Otherwise".equals(dt03AccidentForTakafulNc.getAaab())){
							codeAAAB = dt03AccidentForTakafulNc.getMessageCode();
						}
						if("Otherwise".equals(dt03AccidentForTakafulNc.getA1a2a3())){
							codeA1A2A3 = dt03AccidentForTakafulNc.getMessageCode(); 
						}
						
					
					}
					
					
					if(resultAAAB){
						List<String> param = new ArrayList<String>();
						param.add(insurAAAB);
//						param.add(dt03RiskDiagnosisPlanType.getAmount());
						Message message = messageService.getMessage(registersystem, codeAAAB , param, param);
						messageList.add(message);
					}
					if(resultA1A2A3){
						List<String> param = new ArrayList<String>();
						param.add(insurA1A2A3);
//						param.add(dt03RiskDiagnosisPlanType.getAmount());
						Message message = messageService.getMessage(registersystem, codeA1A2A3 , param, param);
						messageList.add(message);
					}
					
				}
			}
		}

	
		
		
		
		
		
	 /*Flow TELE Channel  25 28 */ 

	boolean planExtracare = false;
	if( agenCode != null ){
		if(isOneOf("25,28", agenCode.getAgentChannel())){
			
			//AT03 Check Plan Health 
		if(oneInOf("TON15J,TON15U,TON15K,TON15M,TON15R,TON15L,TON15Q,TON15W,EON20M,EON20N", currentPlanList )
				|| oneInOfType("HS,HM,HB,CK" , currentPlanList) 
				|| startWithCheck("RHL" , currentPlanList) 
				|| startWithCheck("RHD" , currentPlanList) 
				|| startWithCheck("RHM" , currentPlanList) ){
			
			planHealth = true;
		}
			
		if(planHealth){
			if(startWithCheck("RHD" , currentPlanList) ){
				planExtracare = true;
			}
		}
			
		
		if(planExtracare){
			//19.DT03_Extracare_HB_SumInsured
			List<DT03ExtracareHbSuminsured> extracareHbSuminsuredlist = applicationCache.getRule03_dT03_extracareHbSuminsured();
			for (DT03ExtracareHbSuminsured extracareHbSuminsured : extracareHbSuminsuredlist) {
				if(agenCode!=null){
						if(extracareHbSuminsured.getServiceBranch()!=null){
							if(extracareHbSuminsured.getServiceBranch().equalsIgnoreCase(agenCode.getServiceBranch())){
								if(extracareHbSuminsured.getPlanCode()!=null && !"Otherwise".equalsIgnoreCase(extracareHbSuminsured.getPlanCode()))
								if( basic.getPlanCode().equalsIgnoreCase(extracareHbSuminsured.getPlanCode())){
									if(oneInOf(extracareHbSuminsured.getRider() , rider )){
										
										if(extracareHbSuminsured.getHbSuminsured()!=null &&  !"Otherwise".equalsIgnoreCase(extracareHbSuminsured.getPlanCode()) ){
											BigDecimal InsuredByPlanCode = BigDecimal.ZERO;
											InsuredByPlanCode = getSumInsuredByPlanCode(extracareHbSuminsured.getHbSuminsured(),currentHitPlanList );
											if(InsuredByPlanCode.compareTo(new BigDecimal(extracareHbSuminsured.getHbSuminsuredLimit())) > 0){
												List<String> param = new ArrayList<String>();
												//Todo  DT03068 ยังไม่มีในระบบ
												
												param = mapMessageCodeDesc(extracareHbSuminsured.getMessageDesc());
												
												Message message = messageService.getMessage(registersystem, extracareHbSuminsured.getMessageCode() , param, param);
												messageList.add(message);
												break;
											}
										}
										
									}
								}
							}
						}
							
					}
			}
			
			
		}else{
			
				BigDecimal suminsuredHB = BigDecimal.ZERO; ;//นำเข้าจาก ตาราง DT03_HB_Include_History_TELE
				//DT03_HB_Include_History_TELE
				List<DT03HBIncludeHistoryTele> hbincludehistorytele = applicationCache.getRule03_dT03_hbIncludeHistoryTele();
				for (DT03HBIncludeHistoryTele dt03hbIncludeHistoryTele : hbincludehistorytele) {
					InsureDetail InsureDetailresult = oneInOfReturnPlan(dt03hbIncludeHistoryTele.getPlanCode() , currentPlanList );
					if(InsureDetailresult!=null){
						if(dt03hbIncludeHistoryTele.getB()!=null){
							if(InsureDetailresult.getInsuredAmount().equals(dt03hbIncludeHistoryTele.getB())){
								suminsuredHB = suminsuredHB.add(new BigDecimal(dt03hbIncludeHistoryTele.getSetSumInsuredProdAlmo()) );
							}
						}else{
							suminsuredHB = suminsuredHB.add(new BigDecimal(dt03hbIncludeHistoryTele.getSetSumInsuredProdAlmo()) );
						}
					}
				}
				
				
				//AT03_Tele(Fixed Health)
				
				BigDecimal suminsuredHistoryInsureDetail = getSuminsuredHistoryInsureDetail("HB",  "25,28", "NORMAL" , "RIDER", currentHitPlanList);
				
				BigDecimal getSuminsuredHistoryByChannel = getSuminsuredHistoryByChannel("RBO  W" , "25,28" ,currentHitPlanList );
				BigDecimal sumAll = (suminsuredHistoryInsureDetail.add(suminsuredHB)).subtract(getSuminsuredHistoryByChannel) ;
				if(sumAll.compareTo(new BigDecimal("5000"))>0){
					//Get MessageCode DT03516
					List<String> param = new ArrayList<String>();
					//Todo  DT03068 ยังไม่มีในระบบ
					param.add("5000");
					param.add(sumAll.toString());
					Message message = messageService.getMessage(registersystem, "DT03516" , param, param);
					messageList.add(message);
					
				}
			}
		}
	}

		
		
		
		
	


		
		
		
		
   		
	}

	@Override
	public void finalAction() {
		
		for(String code : listMsgCode) {
			String registersystem = input.getRequestHeader().getRegisteredSystem();
			Message message = messageService.getMessage(registersystem,code, Arrays.asList(""), Arrays.asList("")); 
			messageList.add(message);
		}
		state  = state+"\n"+"END Rule 03 ตรวจสอบการนับทุนรวม 25 ล้านบาท";	
		state  = state+"\n"+"]";
		setSubRuleLog(state);
	}

	
	public BigDecimal getSuminsuredHistoryInsureDetail(String planType, String channelCode, String planSystem, String planGroupType , List<InsureDetail> currentHitPlanList){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(getHistoryInsureDetails(currentHitPlanList))){
			for(InsureDetail insureDetail:getHistoryInsureDetails(currentHitPlanList)){
				if((insureDetail.getPlanHeader().getPlanType() != null && isOneOf(planType, insureDetail.getPlanHeader().getPlanType()))
					&& (insureDetail.getChannelCode() != null && isOneOf(channelCode, insureDetail.getChannelCode()))
					&& (insureDetail.getPlanHeader().getPlanSystem() != null && isOneOf(planSystem, insureDetail.getPlanHeader().getPlanSystem()))
					&& (insureDetail.getPlanHeader().getPlanGroupType() != null && isOneOf(planGroupType, insureDetail.getPlanHeader().getPlanGroupType())))
					sumInsured = sumInsured.add(new BigDecimal( insureDetail.getInsuredAmount()));
			}
		}
		return sumInsured;
	}
	
	
	public BigDecimal getSumInsuredByPlanCode(String planCode,List<InsureDetail> insureDetails){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(insureDetails != null && insureDetails.size() > 0){
			for(InsureDetail insureDetail:insureDetails){
				if(insureDetail.getPlanCode() != null && isOneOf(planCode, insureDetail.getPlanCode())){
					sumInsured = sumInsured.add(new BigDecimal (insureDetail.getInsuredAmount()));
				}
			}
		}
		return sumInsured;
	}
	
	private boolean isOneOf(String source,String target){
		if(source!=null && (!source.isEmpty())){
			source = ","+source.trim()+",";
			target = ","+target.trim()+",";
			int i = source.indexOf(target);
			if(i!=-1){
				return true;
			}
		}
		return false;
	}
	
	public int countPlanCodeInCoverageList(String planCode, List<InsureDetail> insureDetails){
		int count = 0;
		if(planCode!=null && (!planCode.trim().isEmpty())){
			planCode = ","+planCode+",";
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){					
					if(isOneOf(planCode,insureDetail.getPlanCode())){
						count++;
					}
				}
			}
		}
		return count;
	}
	
	public BigDecimal maxSumInsurePlanCodeInCoverageList(String planCode, List<InsureDetail> insureDetails){
		BigDecimal max = null;
		if(planCode!=null && (!planCode.trim().isEmpty())){
			planCode = ","+planCode+",";
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){					
					if(isOneOf(planCode,insureDetail.getPlanCode())){
						if(max == null)max = new BigDecimal(insureDetail.getInsuredAmount());
						else if(max.compareTo(new BigDecimal( insureDetail.getInsuredAmount() )) > 0)
							max = new BigDecimal( insureDetail.getInsuredAmount() );
					}
				}
			}
		}
		return max;
	}
	
	public boolean checkPlanTypeInCurrentRequest(String planType, String planSystem , List<InsureDetail> insureDetails){
		if(!planType.trim().isEmpty() && insureDetails != null && insureDetails.size()>0){
			for(InsureDetail detail : insureDetails){
				if(detail.getPlanHeader().getPlanType() != null && isOneOf(planType,detail.getPlanHeader().getPlanType())){
					if(!planSystem.trim().isEmpty()){
						if(detail.getPlanHeader().getPlanSystem() != null && isOneOf(planSystem,detail.getPlanHeader().getPlanSystem()))
							return true;
					}else return true;
				}
			}
		}
		return false;
	}
	
	public boolean oneInOf(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean oneInOfNotHave(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	public InsureDetail oneInOfReturnPlan(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return insureDetail;
				}
			}
		}
		return null;
	}
	
	public boolean startWithCheck(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			
			for(InsureDetail insureDetail:insuredList){
				if(insureDetail.getPlanCode().startsWith(source)){
					return true;
				}
			}
			
		}
		return false;
	}
	
	public boolean oneInOfType(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanHeader().getPlanType();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	
	public BigDecimal getSumInsuredByPlanType(String planType, List<InsureDetail> insureDetails, String planSystem) {
		BigDecimal sumInsured = BigDecimal.ZERO;
	
		if (insureDetails != null && insureDetails.size() > 0) {
			for (InsureDetail insure : insureDetails) {
				if (insure.getPlanHeader().getPlanType() != null
						&& isOneOf(planType, insure.getPlanHeader().getPlanType())) {
					if (planSystem != null && !planSystem.trim().isEmpty()) {
						if (insure.getPlanHeader().getPlanSystem() != null
								&& isOneOf(planSystem, insure.getPlanHeader().getPlanSystem())) {
							sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
						}
					} else {
						sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
					}
				}
			}
		}
		
//		System.out.println("sumInsured : " +sumInsured);
		return sumInsured;
	}
	
	public BigDecimal getSuminsuredHistoryByChannel(String planCode, String channelCode,List<InsureDetail> currentHitPlanList){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(getHistoryInsureDetails(currentHitPlanList))){
			for(InsureDetail insureDetail:getHistoryInsureDetails(currentHitPlanList)){
				if(insureDetail.getPlanCode() != null && isOneOf(planCode, insureDetail.getPlanCode())
						&& insureDetail.getChannelCode() != null && isOneOf(channelCode, insureDetail.getChannelCode()))
					sumInsured = sumInsured.add(new BigDecimal( insureDetail.getInsuredAmount()));
			}
		}
		return sumInsured;
	}
	
	public List<InsureDetail> getHistoryInsureDetails(List<InsureDetail> currentHitPlanList) {
		List<InsureDetail> temp = new ArrayList<InsureDetail>();

			
		for(InsureDetail detail:currentHitPlanList){
			if(ModelUtils.isOneOf(RuleConstants.POLICY_STATUS, detail.getPolicyStatus()))
			{
				temp.add(detail);
			};
		}
		
		return temp;
	}
	
	
	public boolean checkSuminsuredType(String codeList,BigDecimal lowerAge,BigDecimal higherAge ,BigDecimal age,List<InsureDetail> currentHitPlanList){
		if(getHistoryInsureDetails(currentHitPlanList) != null){
			for(InsureDetail insure:getHistoryInsureDetails(currentHitPlanList)){
				if(insure.getPlanHeader().getPlanType() != null && isOneOf(codeList, insure.getPlanHeader().getPlanType())){
					if(age.compareTo(lowerAge)>=0 && age.compareTo(higherAge)<=0)
						return true;
				}
			}
		}
		return false;
	}
	
	public BigDecimal getSumInsuredByPlanCodeAndServiceBranch(String planCode,List<InsureDetail> insureDetails,String serviceBranch){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(insureDetails)){
			for(InsureDetail insure:insureDetails){
				if(insure.getPlanCode() != null && isOneOf(planCode, insure.getPlanCode())
					&& insure.getServiceBranch() != null && isOneOf(serviceBranch, insure.getServiceBranch())){
						sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
				}
			}
		}
		return sumInsured;
	}
	
	public static boolean isOneOfOrBlank(String source,String target){
		boolean result=false;
		if(source==null||source.length()==0||"".equals(source)){
			result = true;
		}else{
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				result=true;
			}
		}		
		return result;
	}
	
	public static BigDecimal rounding(BigDecimal a, int digit){
		return a.setScale(digit, RoundingMode.HALF_UP);
	}
	
	public  List<String>  mapMessageCodeDesc(String descCode){
		
		
		List<String> param = new ArrayList<String>();
		String[] descArr = descCode.split("_split_");
		for (String result : descArr) {
			param.add(result);
		}
		return param;
	}
	
	public boolean checkTypeInsureDetail(String planType, List<InsureDetail> details){
		if(ModelUtils.isBlank(planType) && ModelUtils.checkNullList(details)){
			for(InsureDetail detail : details){
				if(detail.getPlanHeader().getPlanType() != null){
					
					if(detail.getPlanHeader().getPlanType() != null && isOneOf(planType,detail.getPlanHeader().getPlanType()))
					{	return true; }
				
				}
			}
		}
		return false;
	}
	
}
