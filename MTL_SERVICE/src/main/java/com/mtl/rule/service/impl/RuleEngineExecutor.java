package com.mtl.rule.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.rule.service.interfaces.RuleProcess;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class RuleEngineExecutor<I, C> extends Thread implements Serializable, RuleProcess {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String uuid;
	 
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();
	protected List<AbstractSubRuleExecutor2 > rules;
	protected List<Message> messageList;
	protected Set<Document> documentList;
	protected String RULE_EXECUTE_LOG;
	protected boolean mibHiskRisk;
	protected boolean mibAmlo;
	protected boolean mibUnderwriter;
	protected boolean mibStandard;
	protected boolean referUnderwriter;
	
	@Autowired
	protected ApplicationContext context;
	protected I input;
	protected C collector;

	public RuleEngineExecutor(I input, C collector) {
		super();
		this.uuid = this.getClass().getSimpleName().toString();
		this.input = input;
		this.collector = collector;
		//this.message = new HashMap<String, String>();
		messageList = new ArrayList<Message>();
		documentList = new HashSet<Document>();
		mibHiskRisk=false;
		mibAmlo=false;
		mibUnderwriter=false;
		mibStandard=false;
		referUnderwriter=false;
		RULE_EXECUTE_LOG="";
	}
	
	public RuleEngineExecutor() {
		super();
		uuid = this.getClass().getSimpleName().toString();
	}


	@Override
	public final void run() {

		initial();

		if ( preAction() != true )
			return;
		
		execute();

		finalAction();
		
	}



	
}
