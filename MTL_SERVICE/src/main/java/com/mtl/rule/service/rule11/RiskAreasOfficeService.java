package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.Address;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11RiskAreasOffice;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RiskAreasOfficeService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(RiskAreasOfficeService.class);

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	RiskAreasOfficeService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public boolean preAction() {
		log.info("PreAction DT11_Risk_Areas_Office");
		return true;
	}
	
	@Override
	public void initial() {

	}

	@Override
	public void execute() {
		log.info("Execute DT11_Risk_Areas_Office");
		Address officeAddress = input.getRequestBody().getPersonalData().getOfficeAddressDetail();
		DT11RiskAreasOffice riskAreaOffice = findRiskArea(officeAddress);
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		
		if (riskAreaOffice != null && StringUtils.isNotBlank(riskAreaOffice.getMessageCode())) {
			Message msg = messageService.getOneMessage(registersystem, riskAreaOffice.getMessageCode());
			messageList.add(msg);
		}
	}

	@Override
	public void finalAction() {
		log.info("Final DT11_Risk_Areas_Office");
	}

	private DT11RiskAreasOffice findRiskArea(Address address) {
		
		if(address.getProvinceCode().equals(""))
		{
			address.setProvinceCode(null);
		}
		if(address.getSubDisctrict().equals(""))
		{
			address.setSubDisctrict(null);
		}
		if(address.getDistrict().equals(""))
		{
			address.setDistrict(null);
		}
		if(address.getMoo().equals(""))
		{
			address.setMoo(null);
		}
		if(address.getHouseNumber().equals(""))
		{
			address.setHouseNumber(null);
		}
		
		DT11RiskAreasOffice riskArea = applicationCache.getRule11_riskAreasOfficeMap()
				.get(address.getProvinceCode() + "|" + address.getDistrict()  + "|" + address.getSubDisctrict() + "|"
						+ address.getMoo()  + "|" + address.getHouseNumber()); 

		if (riskArea == null) {
			riskArea = applicationCache.getRule11_riskAreasOfficeMap().get(
					address.getProvinceCode() + "|" + address.getDistrict() + "|" +   address.getSubDisctrict()   + "|null|null");

			if (riskArea == null) {
				riskArea = applicationCache.getRule11_riskAreasOfficeMap()
						.get(address.getProvinceCode() + "|" + address.getDistrict() + "|null|null|null");

				if (riskArea == null) {
					riskArea = applicationCache.getRule11_riskAreasOfficeMap()
							.get(address.getProvinceCode() + "|null|null|null|null");

					if (riskArea == null) {
						riskArea = applicationCache.getRule11_riskAreasOfficeMap()
								.get(RuleConstants.OTHERWISE + "|null|null|null|null");

					}
				}
			}
		}
		return riskArea;
	}

}
