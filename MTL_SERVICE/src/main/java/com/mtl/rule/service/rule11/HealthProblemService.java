package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule06.GetDocumenService;
import com.mtl.collector.service.rule06.GetMedicalLimitType;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.CriticalIllnessDisease;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Health;
import com.mtl.model.underwriting.MedicalHistoryExamination;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.DT11Alcohol;
import com.mtl.rule.model.DT11Disease;
import com.mtl.rule.model.DT11Drug;
import com.mtl.rule.model.DT11Examination;
import com.mtl.rule.model.DT11Smoke;
import com.mtl.rule.model.DT16Alcohol;
import com.mtl.rule.model.DT16FemaleDiseaseList;
import com.mtl.rule.model.Dt06Uwqag;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.ModelUtils;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HealthProblemService extends AbstractSubRuleExecutor2 {


	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private GetMedicalLimitType getMedicalLimitType;
	
	@Autowired
	private GetDocumenService documenService;
	
	List<Document> documentResult;

	public HealthProblemService(UnderwriteRequest input, CollectorModel collecter,List<Document> doc) {
		super(input, collecter);
		this.documentResult = doc;
	}

	private static final Logger log = LogManager.getLogger(HealthProblemService.class);

	Health health = input.getRequestBody().getPersonalData().getHealth();
	String sex = input.getRequestBody().getPersonalData().getSex();
	List<CriticalIllnessDisease> diseaseList = health.getCriticalIllnessDetailList();
//	List<MedicalHistoryExamination> examinationList = health.getMedicalHistoryExaminationList();
	List<MedicalHistoryExamination> examinationDetailList = health.getMedicalHistoryExaminationDetailList();
	List<MedicalHistoryExamination> illnessSymptomDetailList = health.getMedicalHistoryIllnessSymptomDetailList();

	AgentInfo agentDetail = collectorModel.getAgentDetail();
	String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
	String weight = input.getRequestBody().getPersonalData().getWeight();
	String height = input.getRequestBody().getPersonalData().getHeight();
	String registersystem = input.getRequestHeader().getRegisteredSystem();
	String alcoholDateQuitInput = input.getRequestBody().getPersonalData().getHealth().getAlcoholDateQuit();
	String smokingDateQuitInput = input.getRequestBody().getPersonalData().getHealth().getSmokingDateQuit();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	LocalDate today = LocalDate.now();
	
//	int ageyear = p.getYears();
//	int agemonth = p.getMonths();
//	int ageday = p.getDays();
	
	@Override
	public void initial() {

	}
	
	@Override
	public boolean preAction() {
		if (StringUtils.isEmpty(sex) && health == null) {
			log.info("Validate Rule11 HealthProblemService False");
			log.info("#### End Rule11 HealthProblemService ####");
			return false;
		} else {
			return true;
		}

	}

	@Override
	public void execute() {

		log.info("Execute Rule11 HealthProblemService");
		log.info("## Execute ##");
		
		/* 1. Check Alcohol */
		if(CheckAlcoholIsTrue()) {
			LookupTableDT11Alcohol();
		}
		
		/* 2. Smoking */
		if(CheckSmokelIsTrue())
		{
			LookupTableDT11Smoke();
		}
		
		/* 3. Check Height And Weight */
		if(CheckDruglIsTrue())
		{
			LookupTableDT11Drug();
		}
		
		/* 4. Check Height And Weight */
		if(CheckSixMonthChange())
		{
			CheckWeightChangeIncreased();
			CheckWeightChangeDecreased();
			
		}
		
		/* 5. Hiv & Hepatitis */
		String medicalType = CheckMedicalType();
		if(CheckHIVIsTrue()) {
			LookupTableDT11Disease("ภรรยา/สามีของท่านเป็นโรคเอดส์",medicalType);
		}
		
		if(CheckHepatitisIsTrue()) {
			LookupTableDT11Disease("ภรรยา/สามีของท่านเป็นโรคไวรัสตับอักเสบ",medicalType);
		}
		
		/* 6. diseaseList */
		if(diseaseList!=null)
		{
			for(CriticalIllnessDisease disease:diseaseList) {
				LookupTableDT11Disease(disease.getDiseaseName(),medicalType);
			}
		}		
		
		/* 7. dt11ExaminationList */
		List<DT11Examination> dt11ExaminationList = applicationCache.getRule11Examination();
		if(examinationDetailList!=null)
		{
			for(MedicalHistoryExamination examination:examinationDetailList) {
				LookupTableDT11Examination(examination.getExaminationName(),medicalType,dt11ExaminationList,examination.getResult());
			}
		}
		if(illnessSymptomDetailList!=null)
		{
			for(MedicalHistoryExamination examination:illnessSymptomDetailList) {
				LookupTableDT11Examination(examination.getExaminationName(),medicalType,dt11ExaminationList,examination.getResult());
			}
		}
		
	}

	@Override
	public void finalAction() {
		
	}

	private String CheckMedicalType() {
		String medicalTypeN = "Y";
		boolean rule06 = false;
		boolean rule14 = false;
		for(Document data: documentResult) {
			if(RuleConstants.RULE.RULE06.equals(data.getRule())) {
				rule06 = true;
			}
			if(RuleConstants.RULE.RULE14.equals(data.getRule())) {
				rule14 = true;
			}
			
		}
//		Dt06Uwqag uwqagReturn = getMedicalLimitType.getmedical(planCode ,agentDetail.getAgentChannel(),agentDetail.getQualifiation());
//      String medicalTypeN = "Y";
//      if(uwqagReturn!=null) {
//    		if(RuleConstants.NO.equalsIgnoreCase(uwqagReturn.getMedicalLimitType())) {
//				medicalTypeN= "N";
//			}
//      }else {
//      	medicalTypeN= "N";
//      }
		if(rule06 || rule14 ){
			medicalTypeN= "Y";
		}else{
			medicalTypeN= "N";
		}
		return medicalTypeN;
	}

	
	private void LookupTableDT11Alcohol() {
		
		DT11Alcohol dt11Alcohol = applicationCache.getRule11Alcohol().get(sex + "|" + health.getAlcoholType());
		
		String alcoholQuantity = health.getAlcoholQuantity();
		String alcoholFrequency = health.getAlcoholFrequency();		

		if(alcoholQuantity!=null && alcoholFrequency !=null){

			BigDecimal quantity = new BigDecimal(alcoholQuantity);
			BigDecimal frequency = new BigDecimal(alcoholFrequency);
			BigDecimal alcoholRate = quantity.multiply(frequency).divide(new BigDecimal("7"),5,RoundingMode.CEILING);
			BigDecimal alcoholRate2 = new BigDecimal(dt11Alcohol.getRate());
			
			boolean checkRate =  RuleUtils.andMorethanNumber(alcoholRate, alcoholRate2);
			
			if (dt11Alcohol != null) {
				if(checkRate) {
					
					List<String> param = new ArrayList<String>();
					param.add(health.getAlcoholType());
					param.add(alcoholQuantity);
					param.add(dt11Alcohol.getDocument());

					Message message = messageService.getMessage(registersystem, dt11Alcohol.getE() , param ,param);
					messageList.add(message);
					
					String[] docCode = dt11Alcohol.getDocument().split(",");
					for(String code :docCode) {
						Document obj = applicationCache.getDocumentCashMap().get(code);
						obj.setRule(RuleConstants.RULE.RULE11);
						if(obj != null) {
							documentList.add(obj);
						}
					}
				}
			}	
		}
	}

	
	
	private void LookupTableDT11Smoke() {
		List<DT11Smoke> dt11Smoke = applicationCache.getDT11Smoke();
		String dt11SmokePeriod = health.getSmokePeriod();
		String dt11SmokePerDay = health.getSmokePerDay();
		if(dt11SmokePeriod != null && dt11SmokePerDay !=null){
			for(DT11Smoke item :dt11Smoke)
			{
				if(Double.parseDouble(dt11SmokePeriod) >= Double.parseDouble(item.getSmokePeriod())){
					if(Double.parseDouble(dt11SmokePerDay) > Double.parseDouble(item.getSmokePerDay()) || Double.parseDouble(dt11SmokePeriod) > Double.parseDouble(item.getSmokePeriod()))
					{
						List<String> param = new ArrayList<String>();
						param.add(dt11SmokePerDay);
						param.add(dt11SmokePeriod);
//						param.add(dt11Alcohol.getDocument());
						
						Message message = messageService.getMessage(registersystem, item.getMessage() , param, param);
						messageList.add(message);
						Document document = applicationCache.getDocumentCashMap().get( item.getDocument() );
						document.setRule(RuleConstants.RULE.RULE11);
						documentList.add(document);
						break;
					}
					
					
					break;
				}

			}
		}
	}
	
	
	private void LookupTableDT11Drug() {
		String dt11Drug = health.getDrugType();
		List<DT11Drug> dt11DrugInfo = applicationCache.getDT11Drug();
		for(DT11Drug item:dt11DrugInfo)
		{
				if(dt11Drug.equalsIgnoreCase(item.getDrugType()))
				{
					
					List<String> param = new ArrayList<String>();
					
					param.add(dt11Drug);
					if(item.getDocument()!=null){
						param.add(item.getDocument());
					}else{
						param.add("-");
					}
					
//					param.add(dt11Alcohol.getDocument());
					
					Message message = messageService.getMessage(registersystem, item.getMessage() , param, param);
					messageList.add(message);
					if(item.getDocument()!=null){
						Document document = applicationCache.getDocumentCashMap().get( item.getDocument() );
						document.setRule(RuleConstants.RULE.RULE11);
						documentList.add(document);
					}
					
					
					if(item.getUnderwrite() !=null) {
						message = messageService.getMessage(registersystem, item.getUnderwrite() , null, null);
						messageList.add(message);
					}
			
					
				}
		}
	}
	

	private void CheckWeightChangeIncreased() {
		if(StringUtils.isNotEmpty(health.getSixMonthChangeKgIncreased())&&health.getSixMonthChangeIncreased().equalsIgnoreCase("TRUE"))
		{
			
			if(StringUtils.isNotEmpty(health.getSixMonthChangeIncreased())&&Double.parseDouble(health.getSixMonthChangeKgIncreased()) > 10) {
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DTMG007, Arrays.asList(health.getSixMonthChangeKgIncreased()), Arrays.asList(health.getSixMonthChangeKgIncreased()));
				messageList.add(message);
				Document document = applicationCache.getDocumentCashMap().get( "AP");
				document.setRule(RuleConstants.RULE.RULE11);
				documentList.add(document);
				document = applicationCache.getDocumentCashMap().get( "ME" );
				documentList.add(document);
			}
		} 
	}
	
	private void CheckWeightChangeDecreased() {
		if(StringUtils.isNotEmpty(health.getSixMonthChangeKgDecreased())&&health.getSixMonthChangeDecreased().equalsIgnoreCase("TRUE"))
		{
			
			if(StringUtils.isNotEmpty(health.getSixMonthChangeDecreased())&&Double.parseDouble(health.getSixMonthChangeKgDecreased()) > 5) {
				
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DTMG007, Arrays.asList(health.getSixMonthChangeKgDecreased()), Arrays.asList(health.getSixMonthChangeKgDecreased()));
				messageList.add(message);
				Document document = applicationCache.getDocumentCashMap().get( "AP");
				document.setRule(RuleConstants.RULE.RULE11);
				documentList.add(document);
				document = applicationCache.getDocumentCashMap().get( "ME" );
				documentList.add(document);
			}
		}
		
	}
	

	private void LookupTableDT11Disease(String disease , String medicalType) {

		DT11Disease dt11Disease = applicationCache.getRule11Disease().get(disease + "|" + medicalType);
	
		if (dt11Disease != null) {
			if(dt11Disease.getMessageCode() !=null) {
				
				List<String> param = new ArrayList<String>();
				param.add(dt11Disease.getDiseaseName());
				if(dt11Disease.getDocument()!=null){
					param.add(dt11Disease.getDocument());
				}else{
					param.add("-");
				}

				
				Message message = messageService.getMessage(registersystem, dt11Disease.getMessageCode() , param, param);
				
				
//				Message message = messageService.getMessage(registersystem, dt11Disease.getMessageCode(), Arrays.asList(dt11Disease.getDiseaseName()), Arrays.asList(dt11Disease.getDiseaseName()));
				messageList.add(message);
			}
			
			if(dt11Disease.getDocument() !=null) {
				

				String[] docCode = dt11Disease.getDocument().split(",");
				for(String code :docCode) {
					Document obj = applicationCache.getDocumentCashMap().get(code);
					obj.setRule(RuleConstants.RULE.RULE11);
					if(obj != null) {
						documentList.add(obj);
					}
				}
				
				
			}
		}
	}



	private void LookupTableDT11Examination(String examination , String medicalType , List<DT11Examination> dt11ExaminationList, String result) {

		if(dt11ExaminationList != null) {
			
			for(DT11Examination dt11Examination:dt11ExaminationList) {
				
				if(examination.contains(dt11Examination.getExaminationName()) && medicalType.equalsIgnoreCase(dt11Examination.getMedical()) && result.equalsIgnoreCase(dt11Examination.getMoreQuestion()) ) {
					
					if(dt11Examination.getMessageCode() !=null) {
						
						List<String> param = new ArrayList<String>();
						param.add(examination);
						if(dt11Examination.getDocument()!=null){
							param.add(dt11Examination.getDocument());
						}else{
							param.add("-");
						}

						
						Message message = messageService.getMessage(registersystem, dt11Examination.getMessageCode() , param, param);
						messageList.add(message);
					}
					
					if(dt11Examination.getDocument() !=null) {
						String[] docCode = dt11Examination.getDocument().split(",");
						for(String code :docCode) {
							Document obj = applicationCache.getDocumentCashMap().get(code);
							obj.setRule(RuleConstants.RULE.RULE11);
							if(obj != null) {
								documentList.add(obj);
							}
						}
					}
					
					if(dt11Examination.getRemark() !=null) {
						Message message = messageService.getMessage(registersystem, dt11Examination.getRemark()  , null, null);
						messageList.add(message);
					}
					
					
				}
			}
		}
	}
	
	
	private boolean CheckAlcoholIsTrue() {
		
//		if(alcoholDateQuitInput!=null ){//		}
//		LocalDate alcoholDateQuit = LocalDate.parse(alcoholDateQuitInput, formatter);
//		Period ad = Period.between(alcoholDateQuit, today);

		
		if ("TRUE".equalsIgnoreCase(health.getAlcohol())&& "FALSE".equalsIgnoreCase(health.getAlcoholQuit())) {

			return true;
		}else if ("TRUE".equalsIgnoreCase(health.getAlcohol()) && "TRUE".equalsIgnoreCase(health.getAlcoholQuit()))  {
			
			if(alcoholDateQuitInput !=null){
				LocalDate alcoholDateQuit = LocalDate.parse(alcoholDateQuitInput, formatter);
				Period ad = Period.between(alcoholDateQuit, today);
				if(ad.getYears()<10){
					return true;
				}
			}else{
				return false;
			}
		}
		return false;
	}
	
	
	private boolean CheckSmokelIsTrue() {
		
		if ("TRUE".equalsIgnoreCase(health.getSmoking()) && "FALSE".equalsIgnoreCase(health.getSmokingQuit())) {
			return true;
		}else if ("TRUE".equalsIgnoreCase(health.getSmoking()) && "TRUE".equalsIgnoreCase(health.getSmokingQuit()))  {
			
			if(smokingDateQuitInput !=null){
				LocalDate smokingDateQuit = LocalDate.parse(smokingDateQuitInput, formatter);
				Period sd = Period.between(smokingDateQuit, today);
				if(sd.getYears()<10){
					return true;
				}
			}else{
				return false;
			}
		}
		return false;
	}


	private boolean CheckDruglIsTrue() {
		if ("TRUE".equalsIgnoreCase(health.getDrugProblem())) {
			return true;
		}
		return false;
	}
	
	private boolean CheckSixMonthChange() {		
		if ("TRUE".equalsIgnoreCase(health.getSixMonthChange())) {
			return true;
		}
		return false;

	}
	
	
	private boolean CheckHIVIsTrue() {
		if ("TRUE".equalsIgnoreCase(health.getSpouseHIV())) {
			return true;
		}
		return false;
	}
	
	private boolean CheckHepatitisIsTrue() {
		if ("TRUE".equalsIgnoreCase(health.getSpouseHepatitis())) {
			return true;
		}
		return false;
	}
	 
	
	
}
