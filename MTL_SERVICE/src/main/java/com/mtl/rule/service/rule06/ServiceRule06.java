package com.mtl.rule.service.rule06;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.GetClientLocationService;
import com.mtl.collector.service.rule06.GetDiabetesDocument;
import com.mtl.collector.service.rule06.GetDocumenService;
import com.mtl.collector.service.rule06.GetMapNmlTypeService;
import com.mtl.collector.service.rule06.GetMedicalLimitType;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT06DiabetesDocument;
import com.mtl.rule.model.DT06MapMedicalDisease;
import com.mtl.rule.model.DT06UWRQ;
import com.mtl.rule.model.Dt06Uwqag;
import com.mtl.rule.model.SumInsure;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.SumInsureService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule06 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule06.class);

	List<DT06MapMedicalDisease> docList;
	BigDecimal allSumInsure  = BigDecimal.ZERO;
	String messageCode = RuleConstants.NA;
 
	@Autowired
	private GetDocumenService documenService;
	
	@Autowired
	private ApplicationCache applicationCache;

	@Autowired
	private GetMedicalLimitType getMedicalLimitType;

	@Autowired
	private GetClientLocationService getClientInfoService;
	
	@Autowired
	private GetMapNmlTypeService getMapNmlTypeService;
	
	@Autowired
	private SumInsureService sumInsureService;
	
	@Autowired
	private GetDiabetesDocument getDiabetesDocument;
	
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();

	@Autowired
	private MessageService messageService; 
	
	String state = ""; 
	Timmer timeUsage = new Timmer();
	boolean foundErrorCode = false;

	public ServiceRule06(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		log.info("0 initial Rule 6");

	}

	@Override
	public boolean preAction() {

		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE 06: :  สิ่งต้องการเพื่อการพิจารณา (Underwriting Requirement)";	 
		
		state  = state+"\n"+"INPUT:";	 
		state  = state+"\n"+"agentCode:"+input.getRequestBody().getAgentCode();	 
		state  = state+"\n"+"personalData.idCard:"+input.getRequestBody().getPersonalData().getIdCard();	 
		state  = state+"\n"+"personalData.sex:"+input.getRequestBody().getPersonalData().getSex();	 
		state  = state+"\n"+"personalData.age:"+input.getRequestBody().getPersonalData().getAge();	 
		state  = state+"\n"+"personalData.addressDetil.provinceCode:"+input.getRequestBody().getPersonalData().getAddressDetail().getProvinceCode();	 
		
		InsureDetail basicInsure = input.getRequestBody().getBasicInsureDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		List<InsureDetail> riderList = input.getRequestBody().getRiderInsureDetails();
		state  = state+"\n"+"Basic:[planCode:"+basicInsure.getPlanCode()+","+"insureAmount:"+basicInsure.getInsuredAmount()+"]";
		state  = state+"\n"+"Rider List:" ;
		int riderLoop =1;
		if(riderList!=null&&riderList.size()>0) {
			for(InsureDetail riderTmp:riderList) {
				state  = state+"\n"+"Rider "+riderLoop+":[PlanCode:"+riderTmp.getPlanCode()+","+"InsureAmount:"+riderTmp.getInsuredAmount()+"]";				
			}
			riderLoop++;
		}

		return true;
	}

	@Override
	public void execute() {
		try { 
			
			/**
			 * Step 2 From Document SDS Topic 3.4.4 Get Agent Qualification
			 * Preparing data from input
			 */
			AgentInfo agentInfo = collector.getAgentDetail();
			String agentChannelCode = agentInfo.getAgentChannel();
	        String agentQualification = agentInfo.getQualifiation();
	        String branch = agentInfo.getServiceBranch();
	        InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
	        List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
	        String planCode = basic.getPlanCode();
	        PlanHeader planHeader = basic.getPlanHeader();
	        String ncType = planHeader.getNcType();
	        
			state  = state+"\n"+"INPUT DATA COLLECTOR";	
			state  = state+"\n"+"[ncType:"+ncType;
			state  = state+"\n"+"[Agent ServiceBranch:"+branch;
	        state  = state+"\n"+RuleConstants.RULE_SPACE+"#1 Get Agent Info -->agentQualification:"+agentQualification+"]";
	        
	    	/**
	    	 * Step 3 From Document SDS Topic 3.4.4 Check Current Basic Plan Med or Non-Med
			 * Get medicalType from method .getmedical()
			 * If medicalType != null -> set value to agentQualification
			 */
	        state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Check Medical Type =='N'?";
	        List<Dt06Uwqag> uwqagReturn = getMedicalLimitType.getMedical(planCode ,agentChannelCode,agentQualification);
	        boolean medicalTypeN = false;
	        boolean checkDateAfter = false;
	        boolean checkDateBefore = false;
	        boolean checkFisrtOtherwise = false;
	        
	        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
	        Date currendate = new Date();
	        
			if(uwqagReturn!=null) { 
				for(Dt06Uwqag uwqag : uwqagReturn) {
					Date dateAfter =  new Date();
					Date dateBefore =  new Date();
					if(uwqag.getDateAfter() !=null) {
						if(uwqag.getDateAfter().equals(RuleConstants.OTHERWISE)) {
							checkDateAfter = true;
						}else {
							dateAfter =  dateFormat.parse(uwqag.getDateAfter());
							if(dateAfter.compareTo(currendate) >= 0) {
								checkDateAfter = true;
							}
						}
					}
					
					if(uwqag.getDateBefore() !=null) {
						if(uwqag.getDateBefore().equals(RuleConstants.OTHERWISE)) {
							checkDateBefore = true;
							checkFisrtOtherwise = true;
						}else {
							dateBefore =  dateFormat.parse(uwqag.getDateBefore());
							if(dateBefore.compareTo(currendate) < 0) {
								checkDateBefore = true;
							}
						}
					}
					
					if(RuleConstants.NO.equals(uwqag.getMedicalLimitType())) {
						medicalTypeN = true;
					}
					
					if(checkDateAfter && checkDateBefore) {
						String tmpAgenQ = uwqag.getMedicalLimitType()==null?agentQualification:uwqag.getMedicalLimitType();
						state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 medicalTypeN:"+medicalTypeN+", Dt06Uwqag AgentQualification:"+agentQualification+", New AgentQualification:"+tmpAgenQ;
						agentQualification = tmpAgenQ;
						if(checkFisrtOtherwise) {
							continue;
						}else {
							break;
						}
					}
				}
			} 
		
			/**
			 * Step 4 From Document SDS Topic 3.4.4 Non-Med Type = 'N'
			 * Check medicalType (true or false)
			 * true -> end process
			 * False -> Check next condition
			 */
			if(!medicalTypeN) { 
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 FALSE  --> Next to #3 Check NML_TYPE(CP or NCP)";
				
				/**
				 * Step 5 From Document SDS Topic 3.4.4 Look up Sum insured Rate
				 * Check planCode is Not Null (true or false)
				 * false -> end process
				 */
				if(planHeader!= null) {
					List<HistoryInsure> historyInsureList = collector.getHistoryInsureList(); // Get HistoryList Web Service
					
					/**
					 * Step 6 From Document SDS Topic 3.4.4 Map 'NML_TYPE' By NC_Type (CP or NCP)
					 * Get DocType and Filter docType
					 */
					String docType = getMapNmlTypeService.findDocType(ncType, branch);
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#3 :"+docType+" --> Next to #4 AllSumInsure("+docType+")"; 
					
					/**
					 * Step 8 and 9 From Document SDS Topic 3.4.4 SumInsured
					 * allSumInsure on CI concept from method .getSumInsurePlanTypeCI()
					 */
					BigDecimal tempSumBasic = BigDecimal.ZERO;
					tempSumBasic = tempSumBasic.add(new BigDecimal(basic.getInsuredAmount()));
					
					/**
					 * Calculate tempSum by UWRatio
					 */
					BigDecimal ratio = BigDecimal.ONE;
					if (planHeader.getUwRatio() != null) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
					tempSumBasic = ratio.multiply(tempSumBasic);
					
					SumInsure responseSum = new SumInsure();
					responseSum = sumInsureService.getSumInsurePlanTypeCI(docType,basic, rider, historyInsureList);
					allSumInsure = responseSum.getSumInsure();
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	History Size :"+historyInsureList.size();
					state  = state+"\n"+RuleConstants.RULE_SPACE+"	CurrentSumInsure(basic)="+tempSumBasic;
					if(RuleConstants.PLAN_TYPE.BASIC.equals(responseSum.getSumType())) {
						state  = state+"\n"+RuleConstants.RULE_SPACE+"	HistorySumInsure("+docType+",BASIC CI greater )="+allSumInsure.subtract(tempSumBasic);
					}else {
						state  = state+"\n"+RuleConstants.RULE_SPACE+"	HistorySumInsure("+docType+",RIDER CI greater )="+allSumInsure;
					}
					
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#4 AllSumInsure="+allSumInsure;
					
					
//					state  = state+"\n"+RuleConstants.RULE_SPACE+"   Total SumInsure =>> AllSumInsure * Ratio ="+allSumInsure;
					
					/**
					 * Step 10 From Document SDS Topic 3.4.4 Look up Client Location
					 * Get clientLocation by provinceCode
					 */
					String clientLocal = getClientInfoService.getClientLocal(input.getRequestBody() .getPersonalData().getAddressDetail().getProvinceCode()); // Find in DB
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#5 Lookup Client Location:"+clientLocal;
					
					/**
					 * Step 11 From Document SDS Topic 3.4.4 Look Up Table UWRQ By Age, Agent Qualify, Location, Sum Insured
					 * Get MessageCode and DocumentCode by method .getDocDetail()
					 */
					String age = input.getRequestBody() .getPersonalData().getAge();
					String sex = input.getRequestBody() .getPersonalData().getSex();
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#6 Look Up Table UWRQ By Age, Agent Qualify, Location, Sum Insured";
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 Mapp Message Code,Document List in DT06UWRQ by [localClient:"+clientLocal+",Agent Qualification:"+
							agentQualification+", age:"+age+", allSumInsure:"+allSumInsure+", sex:"+sex;
					DT06UWRQ dt06Uwrq = documenService.getDocDetail(clientLocal, agentQualification, age, allSumInsure, sex); // Find in DB
					
					/**
					 * Set MessageCode and DocumentCode
					 */
					if(dt06Uwrq != null) {
						if(dt06Uwrq.getMessageCode() !=null || dt06Uwrq.getResultUwrqCode() !=null) {
							state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 DT06UWRQ ID:"+dt06Uwrq.getId()+" Message Code :"+dt06Uwrq.getMessageCode()+"  Document Code:"+dt06Uwrq.getResultUwrqCode();
							
							/**
							 * Step 14 From Document SDS Topic 3.4.4 Mapping Message
							 * Get&Set Message by Code 
							 */
							if(RuleConstants.MESSAGE_CODE.DT06009.equalsIgnoreCase(dt06Uwrq.getMessageCode())) {//DT06009";//Rule 6 : อายุ $ ปี ไม่สามารถซื้อทุนประกันมากกว่า $ บาท ได้	 
								RuleResultBean ruleResultBeanTmp = new RuleResultBean();
								ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT06009);
								List <String> stringInMessageList = new ArrayList<String>();
								stringInMessageList.add(age);
								stringInMessageList.add(""+allSumInsure);
								
								ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
								ruleResultBeanList.add(ruleResultBeanTmp);			
							}
							
							if(RuleConstants.MESSAGE_CODE.DT06209.equalsIgnoreCase(dt06Uwrq.getMessageCode())) {//Rule 6 : ทุนตรวจสุขภาพรวมจำนวน $ บาท ต้องตรวจสุขภาพ ดังนี้ $ (เพื่ออนุมัติงานสำหรับ IL ให้isAutoApproved=true)	 
								RuleResultBean ruleResultBeanTmp = new RuleResultBean();
								ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT06209);
								List <String> stringInMessageList = new ArrayList<String>();
								stringInMessageList.add(""+allSumInsure);
								stringInMessageList.add(dt06Uwrq.getResultUwrqCode());
						 
								ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
								ruleResultBeanList.add(ruleResultBeanTmp);			
							}	
							
							if(StringUtils.isNotBlank(dt06Uwrq.getResultUwrqCode())) {
								
								/**
								 * Step 13 From Document SDS Topic 3.4.4 Found Document Code
								 * Split DocumentCode by method .getDocument()
								 */
								state  = state+"\n"+RuleConstants.RULE_SPACE+"   #7.1 Input for Document List in DT06UWRQ [Input:"+dt06Uwrq.getResultUwrqCode()+"]";
								docList = documenService.getDocument(dt06Uwrq.getResultUwrqCode()); // Find in DB
								if(docList !=null) {
									state  = state+"\n"+RuleConstants.RULE_SPACE+"   #7.2 Result of Document List in DT06UWRQ  [Document size:"+docList.size()+"]";
									int i = 0;
									for(DT06MapMedicalDisease temp :docList) {
										i++;
										/**
										 * Step 15 From Document SDS Topic 3.4.4 Get Document
										 * Get&Set Document by Code 
										 */
										state  = state+"\n"+RuleConstants.RULE_SPACE+"   #7.2 ->"+i+" Document Description [Detail:"+temp.getDocumentNameTh()+"]";
										Document document = applicationCache.getDocumentCashMap().get(temp.getCode());
										document.setRule(RuleConstants.RULE.RULE06);
										documentList.add(document);
		
									}
									
								}
							}
						}else {
							state  = state+"\n"+RuleConstants.RULE_SPACE+"   DT06UWRQ Not found Message and Document";
						}
						
						/**
						 * Check Rider CD case 
						 * set to planCodeList
						 */
						List<InsureDetail> planCodeList = new ArrayList<InsureDetail>();
				        if(rider !=null) {
				        	for(InsureDetail temp : rider) {
				        		String checkCD = temp.getPlanHeader().getPlanType();
				        		if(RuleConstants.RULE_06.CD.equals(checkCD)) {
				        			planCodeList.add(temp);
				        		}
				        	}
				        }
						
				        /**
				         * Step 12 From Document SDS Topic 3.4.4 Lookup Diabetes Document
						 * sumInsureCD by planType == "CD"
						 * Find document by method .getOtherDocument()
						 */
						if(planCodeList !=null && planCodeList.size() > 0) {
							
							state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 Other Document List by PlanType =>";
							state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.1 SumInsure Rider CD ]";
							boolean checkHaveDoc = false;
							BigDecimal sumInsureCD =  BigDecimal.ZERO;
							for(InsureDetail temp:planCodeList) {
								if(temp.getPlanHeader().getPlanType().equals("CD")) {
									BigDecimal riderInsured = new BigDecimal(temp.getInsuredAmount());
									sumInsureCD = sumInsureCD.add(riderInsured);
								}
							}
							
							for(InsureDetail temp:planCodeList) {
								
								if(temp.getPlanHeader().getPlanType().equals("CD")) {
									state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.2 Input [planType:"+temp.getPlanHeader().getPlanType()+", age:"+age+", sumInsureCD:"+sumInsureCD+"]";
									List<DT06DiabetesDocument> otherList = getDiabetesDocument.getOtherDocument(sumInsureCD, age, temp.getPlanHeader().getPlanType()); // Find in DB
									if(otherList !=null && otherList.size() > 0 ) {
										checkHaveDoc = true;
										for(DT06DiabetesDocument objOther:otherList) {
											
											/**
											 * Split DocumentCode by method .getDocument()
											 */
											state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.2 PlanType found document [planType:"+temp.getPlanHeader().getPlanType()+", Document:"+objOther.getDocumentCode()+"]";
											docList = documenService.getDocument(objOther.getDocumentCode()); // Find in DB
											if(docList !=null && docList.size() > 0) {
												state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.3 Result of Document List [Document size:"+docList.size()+"]";
												int i = 0;
												for(DT06MapMedicalDisease doc :docList) {
													i++;
													/**
													 * Get&Set Document by Code 
													 */
													state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.3 ->"+i+" Document Description [Detail:"+doc.getDocumentNameTh()+"]";
													Document document = applicationCache.getDocumentCashMap().get(doc.getCode());
													document.setRule(RuleConstants.RULE.RULE06);
													documentList.add(document);
												}
											}
										}
										break;
									}else {
										state  = state+"\n"+RuleConstants.RULE_SPACE+"   #8.2 PlanType not found document --> End Process]";
										continue;
									}
								}
							}if(!checkHaveDoc) {
								state  = state+"\n"+RuleConstants.RULE_SPACE+"#9 Not found CD --> End process";
							}else {
								state  = state+"\n"+RuleConstants.RULE_SPACE+"#9 Found CD --> End process";
							}
						}
					}else {
						state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 Not found DT06UWRQ data in Step 7 ::]";
					}
				}
			}else {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 TRUE  - Medical Limit Type is 'N' --> #END";
			}
			
			
			
		}catch(Exception ex) {
			log.info(state);
			ex.printStackTrace();
		}
		
	}

	
	@Override
	public void finalAction() {
		referUnderwriter =true;
		if(ruleResultBeanList!=null) {
			for(RuleResultBean ruleResultTmp:ruleResultBeanList) {	  
				List<String> thaiList = ruleResultTmp.getStringInMessageCode();
				List<String> engList =  ruleResultTmp.getStringInMessageCode();
				String registersystem = input.getRequestHeader().getRegisteredSystem();
				Message message = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
				messageList.add(message);	 
				if(ruleResultTmp.getDocumentList()!=null) { 
					documentList.addAll(ruleResultTmp.getDocumentList());
				}
			}
		}
		state  = state+"\n"+"Time usage in Rule 06 :"+timeUsage.timeDiff();
		state  = state+"\n"+"End EXECUTE RULE 06 ";
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}
}
