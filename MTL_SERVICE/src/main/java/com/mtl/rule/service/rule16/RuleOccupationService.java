package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckOccupation;
import com.mtl.model.rule.PayorOccupation;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16Occupation;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleOccupationService extends AbstractSubRuleExecutor2  {
	
	@Autowired
	private CheckOccupation checkOccupation;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private MessageService messageService;
	
	public RuleOccupationService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleOccupationService.class);
	String state = "";
	
	@Override
	public void initial() {
		
	}
 
	@Override
	public boolean preAction() { 
		return true;
	}

	@Override
	public void execute() {
		String occupationCode = input.getRequestBody().getPayorBenefitPersonalData().getOccupationCode();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		state = state + "\n" + "#Start EXECUTE RULE 16 SubFlow: OccupationService";
		state = state + "\n" + "	occupationCode is ["+occupationCode+"]";
		List<String> thaiList = null;
		List<String> engList = null;
		
		//start flow
		PayorOccupation payorOccupation = applicationCache.getPayorOccupationMap().get(occupationCode);
		if(payorOccupation !=null) {
			state = state + "\n" + "	Found => Add message [MSG : "+payorOccupation.getRuleMessageCode()+"]";
	     	thaiList = new ArrayList<String>();
			thaiList.add(occupationCode);
			engList = new ArrayList<String>();
			engList.add(occupationCode);
			
			Message message = messageService.getMessage(registersystem,payorOccupation.getRuleMessageCode(), thaiList, engList);
			messageList.add(message);
		}else {
			state = state + "\n" + "	Not found => End process";
		}
	}

	@Override
	public void finalAction() {
		setSubRuleLog(state);
	}
	
}
 