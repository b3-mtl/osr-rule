package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckNationality;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16VerifyNationality;
import com.mtl.rule.model.DT16VerifyNationalityUSA;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleVerifyNationalityService extends AbstractSubRuleExecutor2 {

	@Autowired
	private CheckNationality checkNationality;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	String state = "";
	
	public RuleVerifyNationalityService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleVerifyNationalityService.class);

	List<String> thaiList;
	List<String> engList;
	String nationality;
	String healthQuestion;
	String registersystem = input.getRequestHeader().getRegisteredSystem();
	
	@Override
	public void initial() {

	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		nationality = input.getRequestBody().getPayorBenefitPersonalData().getNationality();
		healthQuestion = input.getRequestBody().getPayorBenefitPersonalData().getHealthQuestion();
		
		state = state + "\n" + "#Start EXECUTE RULE 16 SubFlow: VerifyNationalityService";
		state = state + "\n" + "	nationality is ["+nationality+"] && healthQuestion is ["+healthQuestion+"]";

		// start flow
		Map<String, DT16VerifyNationality> verifyNationalityMap = applicationCache.getVerifyNationality();
		Map<String, DT16VerifyNationalityUSA> verifyNationalityUSAMap = applicationCache.getVerifyNationalityUSA();

		DT16VerifyNationality verifyNationality = verifyNationalityMap.get(healthQuestion + "|" + nationality);
		DT16VerifyNationalityUSA verifyNationalityUSA = verifyNationalityUSAMap.get(nationality);

		state = state + "\n" + "#1 Check verifyNationality Table DT16VerifyNationality";
		if (verifyNationality != null) {
			step2VerifyNationality(verifyNationality);
		}else {
			state = state + "\n" + "	Not found => Go to #2";
			
		}

		state = state + "\n" + "#2 Check verifyNationalityUSA Table DT16VerifyNationalityUSA";
		if (verifyNationalityUSA != null) {
			step3VerifyNationalityUSA(verifyNationalityUSA);
		}else {
			state = state + "\n" + "	Not found => End process";
		}
		
		step4ChectNationality();

	}

	@Override
	public void finalAction() {
		setSubRuleLog(state);
	}

	private void step2VerifyNationality(DT16VerifyNationality verifyNationality) {
		state = state + "\n" + "	Found => Add message [MSG : "+verifyNationality.getMessageCode()+"]";
		addMessage(verifyNationality.getMessageCode());
//		step4ChectNationality();
	}

	private void addMessage (String messageCode) {
		thaiList = new ArrayList<String>();
		thaiList.add(nationality);
		engList = new ArrayList<String>();
		engList.add(nationality);
		Message message = messageService.getMessage(registersystem, messageCode, thaiList, engList);
		messageList.add(message);
	}
	private void step3VerifyNationalityUSA(DT16VerifyNationalityUSA verifyNationalityUSA) {
		state = state + "\n" + "	Found => Add message [MSG : "+verifyNationalityUSA.getMessageCode()+"]";
		addMessage(verifyNationalityUSA.getMessageCode());
//		step4ChectNationality();
	}

	private void step4ChectNationality() {
		state = state + "\n" + "#3 Check nationality != 1 && nationality != TH";
		if (!"1".equals(nationality) &&  !"TH".equals(nationality)) {
			state = state + "\n" + "	TRUE => Add message && Document [MSG : "+RuleConstants.MESSAGE_CODE.DT16002+",DOC : PAPB,VIPB,WPPB ]";
			Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT16002 , Arrays.asList(nationality,"PAPB,VIPB,WPPB"), Arrays.asList(nationality,"PAPB,VIPB,WPPB"));
			messageList.add(message);
			String[] docCode = new String[] {"PAPB","VIPB","WPPB"};
			for(String code :docCode) {
				Document document = applicationCache.getDocumentCashMap().get(code);
				documentList.add(document);
			}
		}else {
			state = state + "\n" + "	FALSE => End process";
		}
	}

}
