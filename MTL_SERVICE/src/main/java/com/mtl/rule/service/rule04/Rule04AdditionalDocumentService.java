package com.mtl.rule.service.rule04;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule04.CheckAdditionalDocument;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT04AppMdc;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule04AdditionalDocumentService extends AbstractSubRuleExecutor2{ 
	
	private static final Logger log = LogManager.getLogger(Rule04AdditionalDocumentService.class); 
	@Autowired
	private CheckAdditionalDocument checkAdditionalDocument; 
	@Autowired
	private MessageService messageService; 
	@Autowired
	private ApplicationCache applicationCache;
	String state = ""; 
	public Rule04AdditionalDocumentService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}
	
	@Override
	public void initial() {
 
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"[START EXECUTE SUB RULE: Rule04AdditionalDocumentService";	
		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		String premiumPayment = input.getRequestBody().getBasicInsureDetail().getPremiumPayment();
		String basicInsured = input.getRequestBody().getBasicInsureDetail().getInsuredAmount();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		List<InsureDetail> listRiderInsured = input.getRequestBody().getRiderInsureDetails();
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"INPUT: [planCode:"+planCode+", premiumPayment:"+premiumPayment+", basicInsured:"+basicInsured;	
		
		BigDecimal sumInsured = new BigDecimal(basicInsured);
		if(listRiderInsured !=null && listRiderInsured.size() > 0) {
			for(InsureDetail temp:listRiderInsured) {
				BigDecimal insureDetail = new BigDecimal(temp.getInsuredAmount());
				sumInsured = sumInsured.add(insureDetail);
			}
		}
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"Sum Insured For in Basic and Rider:"+sumInsured;	
		
		List<String> thaiList = null;
		List<String> engList = null;
		Boolean next = false;
	 
		
		if (StringUtils.isBlank(planCode) || StringUtils.isBlank(sumInsured.toString()) || StringUtils.isBlank(premiumPayment) ) {
			Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT04001, Arrays.asList(""), Arrays.asList(""));
			messageList.add(message);
		}else {
			next = true;
		}
		
		if(next) {
			state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"Check Additional Document";	
			
			DT04AppMdc additionalDocList = checkAdditionalDocument.ListAdditionalDoc(planCode, sumInsured.longValue(), premiumPayment);
			
			if(additionalDocList!=null) {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"FOUND Additional Document "+additionalDocList.getDocument();
				thaiList = new ArrayList<String>();
				thaiList.add(planCode);
				thaiList.add(premiumPayment);
				thaiList.add(additionalDocList.getDocument());
				
				engList = new ArrayList<String>();
				engList.add(planCode);
				engList.add(premiumPayment);
				engList.add(additionalDocList.getDocument());
				
				Message message = messageService.getMessage(registersystem,additionalDocList.getMessageCode(), thaiList, engList); 
				Document document = applicationCache.getDocumentCashMap().get(additionalDocList.getDocument());
				messageList.add(message);
				documentList.add(document);
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"-"+document.getCode()+":"+document.getDescTH();	
			}else {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+" NOT FOUND Additional Document ";
			} 
		}
		
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"END EXECUTE SUB RULE Rule04AdditionalDocumentService";	
		state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"]";	
		setSubRuleLog(state);
		log.info(state); 

	}

	
}
