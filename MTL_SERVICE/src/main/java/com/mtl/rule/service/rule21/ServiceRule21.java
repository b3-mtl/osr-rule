package com.mtl.rule.service.rule21;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckOccupation;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Fatca;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16Occupation;
import com.mtl.rule.model.DT21KYC;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule21 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel>  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -240241787089662391L;
	
	String state = "";
	Timmer timeUsage = new Timmer();
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	public ServiceRule21(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(ServiceRule21.class);
	
	
	
	@Override
	public void initial() {
		
	}

	@Override
	public boolean preAction() { 
			return true;
	}

	@Override
	public void execute() {
		
		List<String> thaiList = null;
		List<String> engList = null;
		
		String enFistName = input.getRequestBody().getPersonalData().getEnFirstName();
		String enLastName = input.getRequestBody().getPersonalData().getEnLastName();
		String age = input.getRequestBody().getPersonalData().getAge();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		String nationality = input.getRequestBody().getPersonalData().getNationality();
//		String clientHouseRegistration = input.getRequestBody().getPersonalData().getAddressDetail().getClientHouseRegistration();
//		String clientAllAddress = input.getRequestBody().getPersonalData().getAddressDetail().getClientAllAddress();
//		String clientRegionAddress = input.getRequestBody().getPersonalData().getAddressDetail().getClientRegionAddress();
		String clientHouseRegistration = input.getRequestBody().getClientHouseRegistration();
		String clientAllAddress = input.getRequestBody().getClientAllAddress();
		String clientRegionAddress = input.getRequestBody().getClientRegionAddress();
		
		state = state + "\n" + "[";
		state = state + "\n" + "Start EXECUTE 20: FATCA";
		state  = state+"\n"+"INPUT:";
		state  = state+"\n"+"enFistName:"+enFistName;
		state  = state+"\n"+"enLastName:"+enLastName;
		state  = state+"\n"+"age:"+age;
		state  = state+"\n"+"nationality:"+nationality;
		state  = state+"\n"+"clientHouseRegistration:"+clientHouseRegistration;
		state  = state+"\n"+"clientAllAddress:"+clientAllAddress;
		state  = state+"\n"+"clientRegionAddress:"+clientRegionAddress;
		
		
		state  = state+"\n"+"#1 check enfirstname and enlastname";
		//start flow
		if(enFistName!=null && enLastName!=null) {
			
			List<DT21KYC> dt21infor = applicationCache.getDt21kyc();
			
			
			  boolean isOtherwise = false;
			   for(DT21KYC dt21infors : dt21infor) {
			     String[] nationalityList = dt21infors.getNationality().split(",");
			          for (String nationalitys : nationalityList) {
			              if(nationalitys.equals(nationality)){
			               isOtherwise = true;
			               
			              }
			          
			          }
			   }
			   
			   
			for(DT21KYC dt21infors : dt21infor) {
		        boolean checkage =  RuleUtils.betweenNumber(Long.valueOf(age), dt21infors.getMinAge(), dt21infors.getMaxAge());
		        
		        
		          boolean checkname =  enFistName.equals(dt21infors.getFirstnameEn()) && enFistName.equals(dt21infors.getLastnameEn());
		          if(dt21infors.getFirstnameEn()==null && dt21infors.getLastnameEn()==null){
		           checkname =true;
		          }
//		          boolean checknation = false;
		    
		             
		          boolean checknation = false;
		          if(dt21infors.getNationality().equals("Otherwise")){
		        	   if(isOtherwise){
		                   checknation = false;          
		                  }else{
		                   checknation = true;
		                  }
		          }else{
		  
		            String[] nationalityList = dt21infors.getNationality().split(",");
		            for (String nationalitys : nationalityList) {
		                if(nationalitys.equals(nationality)){
		               checknation = true;
		                 
		                }
		            
		            }
		          }
		          
		          System.out.println(dt21infors.getNationality() +" "+ (checkage && checkname && checknation));
		          state  = state+"\n"+"#2 check checkage and checkname and checknation";
		          if(checkage && checkname && checknation)
		          {
		           if(dt21infors.getMessageCode()!=null)
		           {
		            Message message = messageService.getOneMessage(registersystem, dt21infors.getMessageCode());
		            messageList.add(message);
		           }
		           

		          }
				
			}
			
			state  = state+"\n"+"#3 check clientAllAddress clientHouseRegistration clientRegionAddress";
			if(clientAllAddress!=null && clientAllAddress.equals("InComplete")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21004, thaiList, engList);
				messageList.add(message);
				
			}
			state  = state+"\n"+"check clientHouseRegistration";
			if(clientHouseRegistration!=null && clientHouseRegistration.equals("No")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21003, thaiList, engList);
				messageList.add(message);
				
			}
			state  = state+"\n"+"check clientRegionAddress";
			if(clientRegionAddress!=null && clientRegionAddress.equals("No")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21002, thaiList, engList);
				messageList.add(message);
				
			}
		}else {
			if(clientAllAddress!=null && clientAllAddress.equals("InComplete")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21004, thaiList, engList);
				messageList.add(message);
			}
			
			if(clientHouseRegistration!=null && clientHouseRegistration.equals("No")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21003, thaiList, engList);
				messageList.add(message);
			}
			
			if(clientRegionAddress!=null && clientRegionAddress.equals("No")) {
				Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT21002, thaiList, engList);
				messageList.add(message);
			}
		}
		
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"Time usage in Rule 21 :"+timeUsage.timeDiff();
		state  = state+"\n"+"End EXECUTE RULE 21 ";
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}

	
	
}
 