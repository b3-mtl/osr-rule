package com.mtl.rule.service.rule14;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule06.GetMapNmlTypeService;
import com.mtl.collector.service.rule06.GetMedicalLimitType;
import com.mtl.collector.service.rule14.ClaimService;
import com.mtl.collector.service.rule14.ExceptionService;
import com.mtl.collector.service.rule14.HnwService;
import com.mtl.collector.service.rule14.PADocumentService;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.AppNo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT14ChannelGroup;
import com.mtl.rule.model.DT14DocumentClaim;
import com.mtl.rule.model.DT14HnwDocument;
import com.mtl.rule.model.DT14PADocument;
import com.mtl.rule.model.MsAgentInfo;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.SumInsureService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule14 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule14.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>(); 
	
	@Autowired
	private ExceptionService exceptionService; 
	
	@Autowired
	private ClaimService claimService;
	
	@Autowired
	private HnwService hnwService;
	
	@Autowired 
	private PADocumentService paDocumentService;
	
	@Autowired
	private GetMedicalLimitType getMedicalLimitType;
	
	@Autowired
	private SumInsureService sumInsureService;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private MessageService messageService;
	
	
	public ServiceRule14(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}
	
	@Override
	public void initial() { 
		rules = new ArrayList<>(); 
 
	}

	@Override
	public boolean preAction() {
		
		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE 14:เอกสารทางการเงิน";	 
		InsureDetail basicInsure = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> riderList = input.getRequestBody().getRiderInsureDetails();
		state  = state+"\n"+"INPUT REQUEST";	
		state  = state+"\n"+"RegisteredSystem:"+input.getRequestHeader().getRegisteredSystem();
		state  = state+"\n"+"AgentCode:"+input.getRequestBody().getAgentCode();
		state  = state+"\n"+"AppNo:"+input.getRequestBody().getAppNo();
		state  = state+"\n"+"Basic:[PlanCode:"+basicInsure.getPlanCode()+","+"InsureAmount:"+basicInsure.getInsuredAmount()+"]";
		state  = state+"\n"+"Rider List:" ;
		int riderLoop =1;
		if(riderList!=null&&riderList.size()>0) {
			for(InsureDetail riderTmp:riderList) {
				state  = state+"\n"+"Rider "+riderLoop+":[PlanCode:"+riderTmp.getPlanCode()+","+"InsureAmount:"+riderTmp.getInsuredAmount()+"]";				
			}
			riderLoop++;
		}
		
		return true;
	}

	@Override
	public void execute() {
		
		try { 
			
			/**
			 * Preparing data from input
			 */
			String appNo = input.getRequestBody().getAppNo(); // Chang From AppType--> Check check health no health
			InsureDetail basicInsure = input.getRequestBody().getBasicInsureDetail();
			List<InsureDetail> riderInsure = input.getRequestBody().getRiderInsureDetails();
			String planType = basicInsure.getPlanHeader().getPlanType();
			String planCode = basicInsure.getPlanCode();
			
			String registeredSystem = input.getRequestHeader().getRegisteredSystem();
			String age = input.getRequestBody().getPersonalData().getAge();
			AgentInfo agentInfo = collector.getAgentDetail();
			String agentChannelCode = agentInfo.getAgentChannel(); 
			String agentQualification = agentInfo.getQualifiation();
	        PlanHeader planHeader = basicInsure.getPlanHeader();
			String groupChannelCode = basicInsure.getGroupChannelCode();
			List<HistoryInsure> historyInsureList = collector.getHistoryInsureList();  // Get HistoryList Web Service
			
			state  = state+"\n"+"INPUT DATA COLLECTOR";	
			state  = state+"\n"+"[AgentChannelCode:"+agentChannelCode+", AgentQualification:"+agentQualification;
			state  = state+"\n"+"Basic PlanType:"+planType;
			state  = state+"\n"+"groupChannelCode:"+groupChannelCode;
			
			state  = state+"\n"+"HistoryInsureList ";
			if(historyInsureList!=null&&historyInsureList.size()>0) {
				for(HistoryInsure item:historyInsureList) {
					state  = state+"\n"+ "[PlanCode:"+item.getPlanCode()+","+"planType:"+item.getPlanType()+",planGroupType:"+item.getPlanGroupType()+ 
							",documentType:"+item.getDocumentType()+", inSureAmount:"+item.getSumInsure()+",issueDate:"+item.getIssueDate()+"]";				
				}
			 
			}else {
				state  = state+"\n"+"historyInsureList size: 0";
			} 
			
			/**
			 * Check found Exception  (true or false)
			 * true -> end process
			 */
			boolean foundException= exceptionService.getException(planCode ,planType , agentChannelCode);
		 
			state  = state+"\n"+RuleConstants.RULE_SPACE+"#1 Check Exception Plan:DT14_EXCEPTION [planCode:"+planCode+",planType:"+planType+",agentChannelCode:"+agentChannelCode+"]";
	 		if(foundException) {
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#1:TRUE-->End";
			}else {
				/**
				 * Check found AppNo  (true or false)
				 * true -> end process
				 */
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#1:FALSE-->#2 Check Exception AppNo";
				boolean foundAppNo = exceptionService.checkAppNoException(appNo); 
//				boolean foundAppNo = false; // mock data
				state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Exception AppNo [appNo:"+appNo+"]";
				if(foundAppNo) {
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#2:TRUE-->End";
				}else {
					/**
					 * Check found medicalType (true or false)
					 * true -> end process
					 */
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#2:FALSE-->#3 Check MedicalType N";
					boolean foundMedicalType = getMedicalLimitType.checkMedicalTypN(planCode ,agentChannelCode,agentQualification);
					state  = state+"\n"+RuleConstants.RULE_SPACE+"#3 Check MedicalType is Not N DT06_UWQAG  [planCode:"+planCode+",agentChannelCode:"+agentChannelCode+",agentQualification:"+agentQualification+" ]";
					if(foundMedicalType) {
						state  = state+"\n"+RuleConstants.RULE_SPACE+"#3:TRUE-->End"; 
					}else {
						/**
						 * Check is PlanType ACCIDENT (true or false)
						 * true -> SumInsure (PA)
						 * false -> Check HNW
						 */
						state  = state+"\n"+RuleConstants.RULE_SPACE+"#3:FALSE-->#4 Check PlanType ACCIDENT";
						boolean isPlanTypeACCIDENT = exceptionService.isPlanTypeACCIDENT(planType);
						if(isPlanTypeACCIDENT) {
							/**
							 * Check is PlanType ACCIDENT (true or false)
							 * Get SumHistory Insure PA
							 * Get List PA by totalInsurePA
							 * Add Document from ListPA
							 */
							state  = state+"\n"+RuleConstants.RULE_SPACE+"4:TRUE-->#5 SumInsure (PA)"; 
							
							BigDecimal totalInsurePA = sumInsureService.getSumInsureAccident(basicInsure,riderInsure,historyInsureList);
							
							state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure(PA):"+totalInsurePA;
							
							state  = state+"\n"+RuleConstants.RULE_SPACE+"6:Map DT14_PA_Document"; 
							DT14PADocument PA = paDocumentService.getPADocument(totalInsurePA);
							String[] docList = PA.getAddMibClaimList().split(",");
							RuleResultBean ruleResultBeanTmp = new RuleResultBean();
							List <Document> documentList = new ArrayList<Document>();
							for(String docCode:docList) {  
								Document document = applicationCache.getDocumentCashMap().get(docCode);   
								document.setRule(RuleConstants.RULE.RULE14);
								documentList.add(document); 
							} 
							ruleResultBeanTmp.setDocumentList(documentList);  
							ruleResultBeanList.add(ruleResultBeanTmp);				
							 
							
						}else {
							/**
							 * Check is NHW  (true or false)
							 * true -> SumInsure HNW
							 * false -> Check Group Channel
							 */
							state  = state+"\n"+RuleConstants.RULE_SPACE+"#4:FALSE-->#5 Check HNW";
							boolean isNHW = exceptionService.isNHW(planCode);
							if(isNHW) {
								/**
								 * Get SumHistory Insure HNW
								 * Get List HNW by totalInsureHNW , age
								 * Add Document from listHnw
								 * Add Message DT14002 if ReferID == Y
								 */
								state  = state+"\n"+RuleConstants.RULE_SPACE+"5:TRUE-->#6 SumInsure HNW"; 
								
								BigDecimal totalInsureHNW  = sumInsureService.getSumInsureHnw(basicInsure,historyInsureList);
								
								state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure HNW:"+totalInsureHNW;
								state  = state+"\n"+RuleConstants.RULE_SPACE+"7:Map Map DT14_HNW_Document"; 
								DT14HnwDocument hnwDocument = hnwService.getHnwDocument(totalInsureHNW, age);
								
								RuleResultBean ruleResultBeanTmp = new RuleResultBean();
								if(hnwDocument.getReferIR().equals("Y")) {  
									ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT14002);///Rule 14 : ทุนรวมช่องทาง $ จำนวน $ บาท ขอสอบสภาวะ
									List <String> stringInMessageList = new ArrayList<String>();
									stringInMessageList.add(groupChannelCode);
									stringInMessageList.add(""+totalInsureHNW); 
									ruleResultBeanTmp.setStringInMessageCode(stringInMessageList); 	 
								}
								String[] docList = hnwDocument.getMessage().split(","); 
								List <Document> documentList = new ArrayList<Document>();
								for(String docCode:docList) {  
									Document document = applicationCache.getDocumentCashMap().get(docCode);   
									document.setRule(RuleConstants.RULE.RULE14);
									documentList.add(document); 
								} 
								ruleResultBeanTmp.setDocumentList(documentList);  
								ruleResultBeanList.add(ruleResultBeanTmp);		
								
							}else {
								/**
								 * Check Group Channel of AppNo is MR or RT or CB 
								 * Case 1 MR
								 * Case 2 RT
								 * Case 3 CB
								 */
								state  = state+"\n"+RuleConstants.RULE_SPACE+"#5:FALSE-->#5 Check Group Channel  is MR or RT or CB ";
								state  = state+"\n"+RuleConstants.RULE_SPACE+"#5 Check Group Channel by appNo:"+appNo+"  is MR or RT or CB ? ";
								AppNo objAppNo = applicationCache.getAppNoAppCashMap(appNo);
								String channelGroup = "";
								if(objAppNo!=null) {
									channelGroup = objAppNo.getChannelGroup();
								}
								
								state  = state+"\n"+RuleConstants.RULE_SPACE+"#6 :"+channelGroup; 
	//							
	//							HNW:DT14_HNW_Document
	//							MR:DT14_MRTA_Document
	//							RT:DT14_Ordinary_Document
	//							CB:DT14_Corporate_Document
								
								/**
								 * Get SumInsure
								 */
								
								BigDecimal totalInsure = sumInsureService.getSumInsureAllPlan(basicInsure,riderInsure,historyInsureList);
								
								if(channelGroup.equals("MR")) {//MR:DT14_MRTA_Document
									/**
									 * Get List MR by totalInsureMR , planType
									 * Add Document from listMR
									 * Add Message DT14001 
									 */
									state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 MR --> SumInsure(MR)"; 
									
									
									state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure MR:"+totalInsure+" groupChannelCode: MRTA";
									DT14DocumentClaim MR = claimService.findByGroupChannelCodeAndSumInsured("MRTA", totalInsure);
									
									if(MR != null) {
										RuleResultBean ruleResultBeanTmp = new RuleResultBean();
										ruleResultBeanTmp.setMessageCode(MR.getMessageCode());//Rule 14 : ทุนรวมช่องทาง $1 จำนวน $2 บาท ขอเอกสารทางการเงิน $3
										List <String> stringInMessageList = new ArrayList<String>();
										stringInMessageList.add("MRTA ");
										stringInMessageList.add(""+totalInsure);
										stringInMessageList.add(MR.getClaimList()); 
										ruleResultBeanTmp.setStringInMessageCode(stringInMessageList); 
										
										state  = state+"\n"+RuleConstants.RULE_SPACE+"Document claimList :"+MR.getClaimList();
										List<Document> documentList = new ArrayList<Document>();
										String[] docList = MR.getClaimList().split(",");
										for(String docCode:docList) {  
											Document document = applicationCache.getDocumentCashMap().get(docCode);   
											document.setRule(RuleConstants.RULE.RULE14);
											documentList.add(document); 
											state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 Add Document => ["+docCode+" : "+document.getDescTH()+"]"; 
										
										} 
										ruleResultBeanTmp.setDocumentList(documentList);  
										ruleResultBeanList.add(ruleResultBeanTmp);
									}else {
										state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 No Document => End process";
									}
								}
								
								if(channelGroup.equals("RT")) {//RT:DT14_Ordinary_Document Ordinary
									/**
									 * Get SumHistory Insure RT
									 * Get List RT by totalInsureRT , planType
									 * Add Document from listRT
									 * Add Message DT14001 
									 */
									state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 RT --> SumInsure(RT)"; 
									
									state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure RT:"+totalInsure+" groupChannelCode: Ordinary";
									DT14DocumentClaim RT = claimService.findByGroupChannelCodeAndSumInsured("Ordinary", totalInsure);
									
									BigDecimal totalInsureORD  = sumInsureService.getSumInsureByOrdGroupChannel(basicInsure,riderInsure,historyInsureList);
									state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure RT:"+totalInsureORD+" groupChannelCode: ORD";
									DT14DocumentClaim ORD = claimService.findByGroupChannelCodeAndSumInsured("ORD", totalInsureORD);
									
									if(RT != null) {
										RuleResultBean ruleResultBeanTmp = new RuleResultBean();
										ruleResultBeanTmp.setMessageCode(RT.getMessageCode());//Rule 14 : ทุนรวมช่องทาง $ จำนวน $ บาท ขอสอบสภาวะ
										List <String> stringInMessageList = new ArrayList<String>();
										stringInMessageList.add("ORDINARY ");
										stringInMessageList.add(""+totalInsure);
										stringInMessageList.add(RT.getClaimList());
										ruleResultBeanTmp.setStringInMessageCode(stringInMessageList); 
										
										state  = state+"\n"+RuleConstants.RULE_SPACE+"Document claimList :"+RT.getClaimList();
										List<Document> documentList = new ArrayList<Document>();
										String[] docList = RT.getClaimList().split(",");
										for(String docCode:docList) { 
												Document document = applicationCache.getDocumentCashMap().get(docCode);
												document.setRule(RuleConstants.RULE.RULE14);
												documentList.add(document); 
												state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 Add Document => ["+docCode+" : "+document.getDescTH()+"]"; 
										
										}
										ruleResultBeanTmp.setDocumentList(documentList);  
										ruleResultBeanList.add(ruleResultBeanTmp);
									}else {
										state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 No Document => End process";
									}
									
									if(ORD != null) {
										RuleResultBean ruleResultBeanTmp = new RuleResultBean();
										ruleResultBeanTmp.setMessageCode(ORD.getMessageCode());//Rule 14 : ทุนรวมช่องทาง $ จำนวน $ บาท ขอสอบสภาวะ
										List <String> stringInMessageList = new ArrayList<String>();
										stringInMessageList.add("ORD ");
										stringInMessageList.add(""+totalInsureORD);
										ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);
										ruleResultBeanList.add(ruleResultBeanTmp);
									}
								}
								
								if(channelGroup.equals("CB")) {//CB:DT14_Corporate_Document 
									/**
									 * Get SumHistory Insure CB
									 * Get List CB by totalInsureCB , planType
									 * Add Document from listCB
									 * Add Message DT14001 
									 */
									state  = state+"\n"+RuleConstants.RULE_SPACE+"#7 CB --> SumInsure(CB)";
									
									state  = state+"\n"+RuleConstants.RULE_SPACE+"Total SumInsure CB:"+totalInsure+" groupChannelCode: Corporate";
									DT14DocumentClaim CB = claimService.findByGroupChannelCodeAndSumInsured("Corporate", totalInsure);

									if(CB != null) {
										RuleResultBean ruleResultBeanTmp = new RuleResultBean();
										ruleResultBeanTmp.setMessageCode(CB.getMessageCode());//Rule 14 : ทุนรวมช่องทาง $1 จำนวน $2 บาท ขอเอกสารทางการเงิน $3
										List <String> stringInMessageList = new ArrayList<String>();
										stringInMessageList.add("CORPORATE ");
										stringInMessageList.add(""+totalInsure);
										stringInMessageList.add(CB.getClaimList());
										ruleResultBeanTmp.setStringInMessageCode(stringInMessageList); 
										
										state  = state+"\n"+RuleConstants.RULE_SPACE+"Document claimList :"+CB.getClaimList();
										List<Document> documentList = new ArrayList<Document>();
										String[] docList = CB.getClaimList().split(",");
										for(String docCode:docList) { 
											Document document = applicationCache.getDocumentCashMap().get(docCode);
											document.setRule(RuleConstants.RULE.RULE14);
											documentList.add(document);
											state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 Add Document => ["+docCode+" : "+document.getDescTH()+"]"; 
											
										}
										ruleResultBeanTmp.setDocumentList(documentList);
										ruleResultBeanList.add(ruleResultBeanTmp);
									}else {
										state  = state+"\n"+RuleConstants.RULE_SPACE+"#8 No Document => End process";
									}
								}
							}
						}
					}
				}
			}
		}catch(Exception ex) {
			log.info(state);
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public void finalAction() { 
		if(ruleResultBeanList!=null) {
			for(RuleResultBean ruleResultTmp:ruleResultBeanList) {	 
				if(ruleResultTmp.getMessageCode()!=null) { 
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode();
					String registersystem = input.getRequestHeader().getRegisteredSystem();
					Message message = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message);	 
				} 
				if(ruleResultTmp.getDocumentList()!=null) { 
					documentList.addAll(ruleResultTmp.getDocumentList());
				} 
			}
		} 
		state  = state+"\n"+"Time usage in Rule 14 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 14 ";	
		state  = state+"\n"+"]";	
		setRULE_EXECUTE_LOG(state);
		//log.info(state); 
	}

}



