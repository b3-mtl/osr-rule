package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11BMIBaby;
import com.mtl.rule.model.DT11BMIChildAndAdult;
import com.mtl.rule.model.DT11BMIDocument;
import com.mtl.rule.model.DT11VerifySexByPlancode;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.ModelUtils;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BmiService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(BmiService.class);
	
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	boolean result4 = false;
	boolean result5 = false;
	boolean isBaby = false;

	public BmiService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
		result4 = false;
		result5 = false;
		isBaby = false;
	}

	@Override
	public boolean preAction() {

		return true;
	}

	@Override
	public void execute() {
		Message msg = null;
		String sex = input.getRequestBody().getPersonalData().getSex();
		String age = input.getRequestBody().getPersonalData().getAge();
//		String ageInMonthStr = "";
		

		String heightStr = input.getRequestBody().getPersonalData().getHeight();
		String weightStr = input.getRequestBody().getPersonalData().getWeight();

		// 1.
		if (StringUtils.isBlank(sex) && StringUtils.isBlank(heightStr) && StringUtils.isBlank(weightStr)) {
			msg = messageService.getOneMessage(registersystem, "DT11003");
			messageList.add(msg);
			return;
		}
		BigDecimal height = new BigDecimal(heightStr);
		BigDecimal weight = new BigDecimal(weightStr);

		// 2.
		BigDecimal bmiValue = ModelUtils.divideBigDecimal(weight, height.multiply(new BigDecimal(0.01)).pow(2));

		// 3.
		String birthdayIp = input.getRequestBody().getPersonalData().getBirthday();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(birthdayIp, formatter);

		Period p = Period.between(birthday, today);
		log.info("You are " + p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days old.");

		int ageyear = p.getYears();
		int agemonth = p.getMonths();
		int ageday = p.getDays();
		BigDecimal ageInMonth = new BigDecimal(ageyear).multiply(new BigDecimal(12)).add(new BigDecimal(agemonth));
		
		if (ageInMonth.compareTo(new BigDecimal(24)) > 0) {
			// 4.
			DT11BMIChildAndAdult childAndAdult = applicationCache.getRule11_BMIChildAndAdult().get(sex + "|" + ageyear);
			if (childAndAdult == null) {
				childAndAdult = applicationCache.getRule11_BMIChildAndAdult().get(sex + "|" + RuleConstants.OTHERWISE);
			}
			boolean compareBMI = bmiValue.compareTo(new BigDecimal(childAndAdult.getBmiMin())) >= 0 && bmiValue.compareTo(new BigDecimal(childAndAdult.getBmiMax())) <= 0;
			if (compareBMI ) {
				result4 = RuleConstants.TRUE.equals(childAndAdult.getResult());
//				result5 = RuleConstants.TRUE.equals(bMIBaby.getResult());
//				checkDoc(bmiValue);
			}

			checkDoc(bmiValue);
		} else {
			// 5.
			this.isBaby = true;
			DT11BMIBaby bMIBaby = applicationCache.getRule11_BMIBaby().get(sex + "|" + ageInMonth);
			if (bMIBaby != null) {
				boolean compareHeight = height.compareTo(new BigDecimal(bMIBaby.getHeightMin())) >= 0 && height.compareTo(new BigDecimal(bMIBaby.getHeightMax())) <= 0;
				boolean compareWeight = weight.compareTo(new BigDecimal(bMIBaby.getWeightMin())) >= 0 && weight.compareTo(new BigDecimal(bMIBaby.getWeightMax())) <= 0;
				if (compareHeight && compareWeight) {
					result5 = RuleConstants.TRUE.equals(bMIBaby.getResult());
//					checkDoc(bmiValue);
				}
				
//				result5 = RuleConstants.TRUE.equals(bMIBaby.getResult());
				checkDoc(bmiValue);
			}
		}
	}

	private void checkDoc(BigDecimal bmi) {
		// 6.
		String birthdayIp = input.getRequestBody().getPersonalData().getBirthday();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(birthdayIp, formatter);

		Period p = Period.between(birthday, today);
		log.info("You are " + p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days old.");

		int ageyear = p.getYears();
		int agemonth = p.getMonths();
		int ageday = p.getDays();
		BigDecimal ageInMonth = new BigDecimal(ageyear).multiply(new BigDecimal(12)).add(new BigDecimal(agemonth));
		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		boolean hasFound = false;
		List<DT11BMIDocument> listNotOtherwise = applicationCache.getRule11_BMIDocument();
		boolean itemNotIsPlanCode = true;
		for (DT11BMIDocument entry : listNotOtherwise) {
			boolean conditionAge = RuleUtils.betweenNumber(new BigDecimal(ageyear), new BigDecimal(entry.getAgeMin()), new BigDecimal(entry.getAgeMax()));
			boolean conditionBmi = RuleUtils.betweenNumber(bmi, new BigDecimal(entry.getBmiMin()), new BigDecimal(entry.getBmiMax()));
			if(!(conditionAge && conditionBmi)) {
				continue;
			}
			
			if (entry.getExceptRider() != null) {
				List<String> listCode = Arrays.asList(entry.getExceptRider().split(","));
				for (String code : listCode) {
					for (InsureDetail riderInsureDetail : rider) {
						if(riderInsureDetail.getPlanCode().equals(code)){
							itemNotIsPlanCode = false;
							hasFound = true;
							break;
						}
					}
//					if (code.equals(planCode)) {
//						
//					}
				}
			}
			
			if(itemNotIsPlanCode ) {
				hasFound = true;
				if(entry.getMapDocument() != null) {
					String[] docCode = entry.getMapDocument().split(",");
					for(String code :docCode) {
						Document document = applicationCache.getDocumentCashMap().get(code);
						if (document != null) {
							documentList.add(document);
						}
					}
				}
				
				if(entry.getMessageCode() != null) {
					Message message  = messageService.getMessage(registersystem, entry.getMessageCode(),Arrays.asList(new BigDecimal(ageyear).toString(),bmi.toString(),entry.getMapDocument()),Arrays.asList(new BigDecimal(ageyear).toString(),bmi.toString(),entry.getMapDocument()));
					if (message != null) {
						messageList.add(message);
					}
				}
			}
		}

		if (!hasFound) {
			List<DT11BMIDocument> listOtherwise = applicationCache.getRule11_BMIDocument();
			for (DT11BMIDocument entry : listOtherwise) {
				boolean conditionAge = RuleUtils.betweenNumber(new BigDecimal(ageyear), new BigDecimal(entry.getAgeMin()), new BigDecimal(entry.getAgeMax()));
				boolean conditionBmi = RuleUtils.betweenNumber(bmi, new BigDecimal(entry.getBmiMin()), new BigDecimal(entry.getBmiMax()));
				if (conditionAge && conditionBmi) {
					hasFound = true;
					if(entry.getMapDocument() != null) {
						String[] docCode = entry.getMapDocument().split(",");
						for(String code :docCode) {
							Document document = applicationCache.getDocumentCashMap().get(code);
							if (document != null) {
								documentList.add(document);
							}
						}
					}
					
					if(entry.getMessageCode() != null) {
						Message message  = messageService.getMessage(registersystem, entry.getMessageCode(),Arrays.asList(new BigDecimal(ageyear).toString(),bmi.toString(),entry.getMapDocument()),Arrays.asList(new BigDecimal(ageyear).toString(),bmi.toString(),entry.getMapDocument()));
						if (message != null) {
							messageList.add(message);
						}
					}
				}
			}
		}
//		if (!hasFound) {
//			return;
//		}

		// 7.
		String sex = input.getRequestBody().getPersonalData().getSex();
		DT11VerifySexByPlancode verifySexByPlancode = applicationCache.getRule11_verifySexByPlancode().get(planCode);
		if (verifySexByPlancode != null) {
			if (!sex.equals(verifySexByPlancode.getSexIsNot())) {
				result4 = false ;
				result5 = false ;
//				result7 = RuleConstants.TRUE.equals(verifySexByPlancode.getResult());
			}
		}

		Message msg = null;
		if (!isBaby) {
			if (result4) {
				msg = messageService.getOneMessage(registersystem, "DT1130Y");
				messageList.add(msg);
			} else {
				msg = messageService.getOneMessage(registersystem, "DT11302");
				messageList.add(msg);
			}
		} else {
			if (result5) {
				msg = messageService.getOneMessage(registersystem, "DT1130Y");
				messageList.add(msg);
			} else {
				msg = messageService.getOneMessage(registersystem, "DT11302");
				messageList.add(msg);
			}
		}
	}

	@Override
	public void finalAction() {
		log.info("FinalAction Verify_Personality");

	}

}
