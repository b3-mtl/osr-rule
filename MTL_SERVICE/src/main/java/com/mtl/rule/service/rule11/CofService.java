package com.mtl.rule.service.rule11;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT11PlanReferPremiumPlus;
import com.mtl.rule.model.DT11PremiumPlus;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CofService extends AbstractSubRuleExecutor2 {

	private static final Logger log = LogManager.getLogger(CofService.class);
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Autowired
	private ApplicationCache applicationCache;

	@Autowired
	private MessageService messageService;

	List<Message> msgList = null;

	public CofService(UnderwriteRequest i, CollectorModel c) {
		super(i, c);
	}

	@Override
	public void initial() {
		msgList = new ArrayList<Message>();
	}

	@Override
	public boolean preAction() {
		log.info("PreAction COF");
		return true;
	}

	@Override
	public void execute() {

		/**
		 * Preparing data from input
		 */
		log.info("Execute COF");
//		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
//		PlanHeader planHdr = applicationCache.getPlanHeaderAppCashMap().get(planCode);
		PersonalData personal = input.getRequestBody().getPersonalData();
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<InsureDetail> currPlan = new ArrayList<InsureDetail>();
		currPlan.add(basic);
		if(rider !=null) {
			currPlan.addAll(rider);
		}

		for(InsureDetail item:currPlan) {
			String planCode = item.getPlanCode();
			PlanHeader planHdr = item.getPlanHeader();
			if (planHdr != null) {
				String classOfBusiness = planHdr.getClassOfBusiness();
				String occupationCode = input.getRequestBody().getPersonalData().getOccupationCode();
				/**
				 * Step 1 From Document SDS Topic 3.6.16.2 Check Class of Business Get Class of
				 * Business from planCode in method .getPlanHeaderAppCashMap().get(planCode) If
				 * Class of Business equal "L" -> Check Occupation in Decision table
				 */
				if ("L".equals(classOfBusiness)) {
					DT11PremiumPlus prem = applicationCache.getRule11_premiumPlus().get(occupationCode);
					
					
					
					
					
					if(prem != null) {
						if (new BigDecimal(prem.getPremiumPlus()).compareTo(BigDecimal.ZERO) > 0) {
							DT11PlanReferPremiumPlus refer = applicationCache.getRule11_planReferPremiumPlus().get(planCode);
							if (refer != null) {
								Message message = messageService.getMessage(registersystem, "REFERUW"  , null, null);
								messageList.add(message);
								
							}
							//Todo
//								List<String> thai = new ArrayList<String>();
//								thai.add(personal.getOccupationCode());
//								thai.add(item.getInsuredAmount());
//								List<String> eng = new ArrayList<String>();
//								eng.add(personal.getOccupationCode());
//								eng.add(personal.getInsuredAmount());
								
//							}
								
							List<String> param = new ArrayList<String>();
							param.add(personal.getOccupationCode());
							param.add(prem.getPremiumPlus());
								
							Message message = messageService.getMessage(registersystem, "DT11208" , param, param);
//							Message msg = messageService.getMessage(registersystem, "DT11208", thai, eng);
							msgList.add(message);
							break;
						}
					}
				}
			}
		}
	}

	@Override
	public void finalAction() {
		log.info("FinalAction COF");
		super.setMessageList(msgList);
	}

}
