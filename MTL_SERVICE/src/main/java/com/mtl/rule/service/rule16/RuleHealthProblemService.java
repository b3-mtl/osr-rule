package com.mtl.rule.service.rule16;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Health;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16Alcohol;
import com.mtl.rule.model.DT16FemaleDiseaseList;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.ModelUtils;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleHealthProblemService extends AbstractSubRuleExecutor2 {


	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	public RuleHealthProblemService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleHealthProblemService.class);

	Health health = input.getRequestBody().getPayorBenefitPersonalData().getPbHealth();
	String sex = input.getRequestBody().getPayorBenefitPersonalData().getSex();
	String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
	String weight = input.getRequestBody().getPayorBenefitPersonalData().getWeight();
	String height = input.getRequestBody().getPayorBenefitPersonalData().getHeight();
	String registersystem = input.getRequestHeader().getRegisteredSystem();

	@Override
	public boolean preAction() {
		if (StringUtils.isEmpty(sex) && health == null) {
			log.info("Validate Rule16 HealthProblemService False");
			log.info("#### End Rule16 HealthProblemService ####");
			return false;
		} else {
			return true;
		}

	}

	@Override
	public void execute() {
		log.info("Execute Rule16 HealthProblemService");
		log.info("## Execute ##");
		if (step2CheckExceptPBHealth()) {
			step3CheckCriticalIllness();
			if (step4CheckVisionDisorder()) {
				step5LookupTableDT16femaleDiseaseList();
			}
			if (step6CheckMedicalHistoryPayor()) {
				step7CheckExaminationListisNull();
			}
			step8CheckRejectedDeclinedPostponed();
			if (step9CheckAlcoholisTrue()) {
				step10LookupTableDT16Alcohol();
			}
			if(step11CheckHeightAndWeight()) {
				step13CheckBMIBetween(step12BMI());
			}
			step14CheckWeightChangeDecreased();
			step15CheckWeightChangeIncreased();
		}
	}

	@Override
	public void finalAction() {
	}

	@Override
	public void initial() {

	}

	private boolean step2CheckExceptPBHealth() {
		PlanPermission PlanPermission =	applicationCache.getPlanPermissionAppCashMap().get(planCode+"|");
			if (PlanPermission!=null) {
				if ("true".equals(PlanPermission.getExceptPBHealth()))
					return true;
				else 
					return false;
			}
			else {
				return true;
			}
	}

	private void step3CheckCriticalIllness() {
		if (!"TRUE".equals(health.getCriticalIllnessDisease())) {
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16009, Arrays.asList(),
					Arrays.asList());
			messageList.add(message);
		}
	}

	private boolean step4CheckVisionDisorder() {
		if ("TRUE".equals(health.getVisionDisorderFemale())) {
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16019, Arrays.asList(),
					Arrays.asList());
			messageList.add(message);
			return false;
		} else {
			return true;
			
		}
	}

	private void step5LookupTableDT16femaleDiseaseList() {
		DT16FemaleDiseaseList femaleDiseaseList = applicationCache.getFemaleDiseaseList().get(health.getSymtonName());
		if (femaleDiseaseList != null) {
			Message message = messageService.getMessage(registersystem, femaleDiseaseList.getMessageCode(), Arrays.asList(),
					Arrays.asList());
			messageList.add(message);
		}
	}

	private boolean step6CheckMedicalHistoryPayor() {
		if ("TRUE".equals(health.getMedicalHistory())) {
			return true;
		}
		return false;
	}

	private void step7CheckExaminationListisNull() {
		Message message;
		if (StringUtils.isEmpty(health.getMedicalHistoryExamination())) {
			message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16018, Arrays.asList(),
					Arrays.asList());
		} else {
			message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16010, Arrays.asList(),
					Arrays.asList());
		}
		messageList.add(message);
	}

	private void step8CheckRejectedDeclinedPostponed() {
		if ("TRUE".equals(health.getRejectedDeclinedPostponed())) {
			Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT16006, Arrays.asList(), Arrays.asList());
			messageList.add(message);
		}
	}

	private boolean step9CheckAlcoholisTrue() {
		if ("TRUE".equals(health.getAlcohol())) {
			return true;
		}
		return false;
	}

	private void step10LookupTableDT16Alcohol() {
		DT16Alcohol dt16Alcohol = applicationCache.getRule16Alcohol()
				.get(sex + "|" + health.getAlcoholType());
		
		String alcoholQuantity = health.getAlcoholQuantity();
		String alcoholFrequency = health.getAlcoholFrequency();
		
		BigDecimal quantity = new BigDecimal(alcoholQuantity);
		BigDecimal frequency = new BigDecimal(alcoholFrequency);
		
		BigDecimal alcoholRate = quantity.multiply(frequency).divide(new BigDecimal("7"),5,RoundingMode.CEILING);
		BigDecimal alcoholRate2 = new BigDecimal(dt16Alcohol.getRate());
		boolean checkRate =  RuleUtils.andMorethanNumber(alcoholRate, alcoholRate2);
		
		if (dt16Alcohol != null) {
			if(checkRate) {
				Message message = messageService.getMessage(registersystem, dt16Alcohol.getE(), Arrays.asList(health.getAlcoholType(),health.getAlcoholFrequency()), Arrays.asList(health.getAlcoholType(),health.getAlcoholFrequency()));
				messageList.add(message);
			}
		}
	}

	private boolean step11CheckHeightAndWeight() {
		if (StringUtils.isNotEmpty(weight) && StringUtils.isNotEmpty(height)) {
			if ((Double.parseDouble(weight) * Double.parseDouble(height)) > 0) {
				return true;
			}
		}
		return false;

	}

	private double step12BMI() {
		double bmi = Double.parseDouble(weight) / (Math.pow(Double.parseDouble(height) * 0.01, 2));
		return bmi;

	}

	private void step13CheckBMIBetween(double bmi) {
		if (bmi >= 17 && bmi <= 33) {
			
		}
		else
		{
			Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16001, Arrays.asList(), Arrays.asList());
			messageList.add(message);
		}
		
	}
	
	private void step14CheckWeightChangeDecreased () {
		if(StringUtils.isNotEmpty(health.getSixMonthChangeKgDecreased())&&health.getSixMonthChangeDecreased().equals("TRUE"))
		{
			if(StringUtils.isNotEmpty(health.getSixMonthChangeDecreased())&&Double.parseDouble(health.getSixMonthChangeKgDecreased()) > 5) {
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16008, Arrays.asList(health.getSixMonthChangeKgDecreased()), Arrays.asList(health.getSixMonthChangeKgDecreased()));
				messageList.add(message);
			}
		}
		
	}
	
	private void step15CheckWeightChangeIncreased() {
		if(StringUtils.isNotEmpty(health.getSixMonthChangeKgIncreased())&&health.getSixMonthChangeIncreased().equals("TRUE"))
		{
			if(StringUtils.isNotEmpty(health.getSixMonthChangeIncreased())&&Double.parseDouble(health.getSixMonthChangeKgIncreased()) > 10) {
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT16008, Arrays.asList(health.getSixMonthChangeKgIncreased()), Arrays.asList(health.getSixMonthChangeKgIncreased()));
				messageList.add(message);
			}
		}
	}
}
