package com.mtl.rule.service.rule11;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.collector.service.GetCollectorHistoryInsure;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;


@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceMagnum extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceMagnum.class);
 
	private List<AbstractSubRuleExecutor2> rules;
	String state = "";
	Timmer timeUsage = new Timmer();
	List<Document> documentAllRule;
	
	public ServiceMagnum(UnderwriteRequest input, CollectorModel collectorModel , List<Document> document) {
		super(input, collectorModel);
		this.documentAllRule = document;
		
	}

	@Override
	public void initial() {
		rules = new ArrayList<>();
		rules.add(context.getBean(HealthProblemService.class, super.input, super.collector,this.documentAllRule));
		
		
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE MagnumService ";
	 
		this.rules.parallelStream().forEach(rule -> {
			rule.run();
		});
		
 
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"Time usage in MagnumService :"+timeUsage.timeDiff();
		state  = state+"\n"+"END MagnumService";	
		state  = state+"\n"+"]";
		
		setRULE_EXECUTE_LOG(state);
		rules.parallelStream().forEach((rule -> {
				if(rule.getMessageList() != null ) {
					messageList.addAll(rule.getMessageList());
				}
				if(rule.getDocumentList() != null ) {
					documentList.addAll(rule.getDocumentList());
				}
				
			}
		));
		
	}
}
