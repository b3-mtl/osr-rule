package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckPayorRelationShip;
import com.mtl.model.rule.PayorRelation;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT16PayorRelation;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RulePayorRelationService extends AbstractSubRuleExecutor2  {

	@Autowired
	private CheckPayorRelationShip checkPayorRelationShip;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	@Autowired
	private MessageService messageService;
	
	
	String state = "";
	String registersystem = input.getRequestHeader().getRegisteredSystem();
	
	public RulePayorRelationService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}
	
	private static final Logger log = LogManager.getLogger(RulePayorRelationService.class);
  
	@Override
	public void initial() {
		
	}
	
	@Override
	public boolean preAction() {		 

		return true;

	}

	@Override
	public void execute() {

		String relationShip = input.getRequestBody().getPayorBenefitPersonalData().getPayorRelationship();
		state = state + "\n" + "#Start EXECUTE RULE 16 SubFlow: PayorRelationService";
		state = state + "\n" + "	relationShip is ["+relationShip+"]";
		
		//start flow
		PayorRelation payorRelation = applicationCache.getPayorRelationMap().get(relationShip);
		
		state = state + "\n" + "#1 Check PayorRelation Table DT16_PAYOR_RELATION";
		if(payorRelation != null) {
			state = state + "\n" + "	Found => Add message && Document [MSG : "+payorRelation.getMessageCode()+",DOC : "+payorRelation.getDocumentCode()+"]";
			
			if(payorRelation.getMessageCode() !=null) {
				List<String> thaiList = new ArrayList<String>();
				thaiList.add(relationShip);
				thaiList.add(payorRelation.getDocumentCode());
				List<String> engList = new ArrayList<String>();
				engList.add(relationShip);
				engList.add(payorRelation.getDocumentCode());
				Message message = messageService.getMessage(registersystem,payorRelation.getMessageCode(), thaiList, engList);
				messageList.add(message);
			}
			if(payorRelation.getDocumentCode() !=null) {
				Document document = applicationCache.getDocumentCashMap().get(payorRelation.getDocumentCode());
				documentList.add(document);
			}
		}else {
			state = state + "\n" + "	Not found => End process";
		}
	}


	@Override
	public void finalAction() {
		setSubRuleLog(state);
	}

	
}

 