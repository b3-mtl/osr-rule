package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.appcache.ApplicationCache;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule03 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {


	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8866567388523039186L;
	private static final Logger log = LogManager.getLogger(ServiceRule03.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	
	boolean checkmessage = true;
	public ServiceRule03(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 03 ตรวจสอบทุนประกัน";	 
		

		
		
		rules = new ArrayList<>();	 
		Rule03DiagnosisService diagnosisService = (Rule03DiagnosisService)context.getBean(Rule03DiagnosisService.class,this.input,this.collector);
		Rule03AccidentService accidentService = (Rule03AccidentService)context.getBean(Rule03AccidentService.class,this.input,this.collector);
		Rule03HealthService healthService = (Rule03HealthService)context.getBean(Rule03HealthService.class,this.input,this.collector);
		Rule03OPDService odpService = (Rule03OPDService)context.getBean(Rule03OPDService.class,this.input,this.collector);
		Rule03SavingService savingService = (Rule03SavingService)context.getBean(Rule03SavingService.class,this.input,this.collector);
		Rule03TDPService tdpService = (Rule03TDPService)context.getBean(Rule03TDPService.class,this.input,this.collector);
		Rule03GIOService gioService = (Rule03GIOService)context.getBean(Rule03GIOService.class,this.input,this.collector);
		Rule03TermService termService = (Rule03TermService)context.getBean(Rule03TermService.class,this.input,this.collector);
		Rule03CheckPhdrService  checkPhdrService = (Rule03CheckPhdrService)context.getBean(Rule03CheckPhdrService.class,this.input,this.collector);
//		Rule03SummaryOtherService  summaryOtherService = (Rule03SummaryOtherService)context.getBean(Rule03SummaryOtherService.class,this.input,this.collector);
		Rule03InsuredGroupService  insuredGroupService = (Rule03InsuredGroupService)context.getBean(Rule03InsuredGroupService.class,this.input,this.collector);
		
		
		rules.add(diagnosisService);
		rules.add(accidentService);
		rules.add(healthService);
		rules.add(odpService);
		rules.add(savingService);
		rules.add(tdpService);
		rules.add(gioService);
		rules.add(termService);
//		rules.add(summaryOtherService);
		rules.add(insuredGroupService);
		
		

	}

	@Override
	public boolean preAction() {

		/* Edit Set Age on Birth Day*/
		String birthdayIp = input.getRequestBody().getPersonalData().getBirthday();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.parse(birthdayIp, formatter);

		Period p = Period.between(birthday, today);
		log.info("You are " + p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days old.");

		int ageyear = p.getYears();
		int agemonth = p.getMonths();
		int ageday = p.getDays();

		input.getRequestBody().getPersonalData().setAge(ageyear+"");
		input.getRequestBody().getPersonalData().setAgeInMonth(agemonth+"");
		input.getRequestBody().getPersonalData().setAgeDay(ageday+"");
		return true;
	}

	@Override
	public void execute() {
		
		this.rules.parallelStream().forEach(rule -> {
			rule.run();
		});
		
	}

	@Override
	public void finalAction() {
		
		
		this.rules.parallelStream().forEach(rule -> {
			if(rule.getMessageList() != null && !rule.getMessageList().isEmpty() ) {
				messageList.addAll(rule.getMessageList());
				documentList.addAll(rule.getDocumentList());
				
				checkmessage=false;
			}
			
			state = state +"\n"+rule.getSubRuleLog();
		});

		if(checkmessage)
		{
			checkPdr();
		}
		
		
		state  = state+"\n"+"Time usage in Rule 03 :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 03";	
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
	}
	
	public void checkPdr() {
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		List<InsureDetail> current = new ArrayList<InsureDetail>();
		
		current.add(basic);
		if(rider!=null)
		{
			current.addAll(rider);
		}
	
		for(InsureDetail tempBasic:current)
		{
			PlanHeader phdr = applicationCache.getPlanHeaderAppCashMap().get(tempBasic.getPlanCode());
			BigDecimal insureAmountBasic = new BigDecimal(tempBasic.getInsuredAmount());
			boolean checkAmount = RuleUtils.betweenNumber(insureAmountBasic, new BigDecimal(phdr.getMinimumAmount()), new BigDecimal(phdr.getMaximumAmount()));
			if(phdr!=null&& checkAmount )
			{
				Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT0310Y, Arrays.asList(tempBasic.getInsuredAmount(),tempBasic.getPlanCode()),Arrays.asList(tempBasic.getInsuredAmount(),tempBasic.getPlanCode()));
				messageList.add(message);
			}
			else
			{
				if(basic.getPlanCode()!=null)
				{
					if(RuleUtils.andLessthanNumber(insureAmountBasic, new BigDecimal(phdr.getMinimumAmount())))
					{
						List<String> paramTH = new ArrayList<String>();
						List<String> paramEN = new ArrayList<String>();
						paramTH.add(insureAmountBasic.toString());
						paramTH.add("ต่ำกว่า");
						paramTH.add(tempBasic.getPlanCode());
						
						paramEN.add(insureAmountBasic.toString());
						paramEN.add("less than");
						paramEN.add(tempBasic.getPlanCode());
						
						Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03103, paramTH,paramEN);
						messageList.add(message);
					}
					else
					{
						List<String> paramTH = new ArrayList<String>();
						List<String> paramEN = new ArrayList<String>();
						paramTH.add(insureAmountBasic.toString());
						paramTH.add("สูงกว่า");
						paramTH.add(tempBasic.getPlanCode());
						
						paramEN.add(insureAmountBasic.toString());
						paramEN.add("greater than");
						paramEN.add(tempBasic.getPlanCode());
						Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03103, paramTH,paramEN);
						messageList.add(message);
					}
				}
				else
				{
					List<String> param = new ArrayList<String>();
					param.add(tempBasic.getPlanCode());
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03102, param,param);
					messageList.add(message);
				}
			}
		}
	}
}
