package com.mtl.rule.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Document;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.RulesResponse;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.rule11.HealthProblemService;
import com.mtl.rule.service.rule11.ServiceMagnum;

@Service
public class RuleEngineService  implements ApplicationContextAware{

	protected static final Logger log = LogManager.getLogger(RuleEngineService.class);

	@Autowired
	private ApplicationContext context; 
  
	public RulesResponse  underwriting(UnderwriteRequest request,CollectorModel collector,Map<String, String> ruleSet) throws Exception { 
		//List<Message>  allRuleMessageList  = new ArrayList<Message>();
		RulesResponse rulesResponse = new RulesResponse();
		try { 
			List<Message> serviceMessageList = new ArrayList<Message>(); 
			List<Document> documentList = new ArrayList<Document>(); 
			String allRuleEXECUTELOG ="";
			String referUnderwriter = "";
			
			Set<String> rules =new HashSet<String> (ruleSet.values());
			
			RuleEngineExecutor<?, ?>[] threads = new RuleEngineExecutor<?, ?>[rules.size()];
			int i = 0;
			
			for (String rule : rules) {			 
				RuleEngineExecutor<?, ?> r = (RuleEngineExecutor<?, ?>) context.getBean(rule, request, collector);			
				threads[i] = r;
				threads[i++].start();
			}
	 
			try {
				for (RuleEngineExecutor<?, ?> thread : threads) {  
					thread.join();
	 				serviceMessageList.addAll(thread.getMessageList());
	 				documentList.addAll(thread.getDocumentList());
	 				allRuleEXECUTELOG  = allRuleEXECUTELOG+"\n"+thread.getRULE_EXECUTE_LOG();
	 				
	 				if(thread.isReferUnderwriter()) {
	 					referUnderwriter = "TRUE TEST";
	 				}
	 				log.info(thread.getUuid() + " Run success.");
	 			}
				
				if(rules.contains("serviceRule11")) {
					RuleEngineExecutor<?, ?> magnum = (RuleEngineExecutor<?, ?>) context.getBean(ServiceMagnum.class, request, collector, documentList);
					magnum.start();
					magnum.join();
					serviceMessageList.addAll(magnum.getMessageList());
					documentList.addAll(magnum.getDocumentList());
					allRuleEXECUTELOG  = allRuleEXECUTELOG+"\n"+magnum.getRULE_EXECUTE_LOG();
				}
				
			} catch (InterruptedException e) {
				throw new  InterruptedException( e.getLocalizedMessage());
			}		
			
			log.info( "  ALL RULE_EXECUTE_LOG : \n"+allRuleEXECUTELOG);
			rulesResponse.setMessageList(serviceMessageList);
			rulesResponse.setDocumentList(documentList);
			rulesResponse.setReferUnderwriter(referUnderwriter);
			rulesResponse.setAllRuleExecuteLog(allRuleEXECUTELOG);
			//allRuleMessageList  =  callRuleEngine( new HashSet<String> (ruleSet.values() ) , request, collector); 
 			
		}catch (InterruptedException ex) {
			log.error(ex);
		}catch (Exception e) {
			throw new Exception(e);
		} 
		return rulesResponse;
	}


 
	private List<Message>  callRuleEngine(Set<String> rules, UnderwriteRequest input, CollectorModel collector ) throws InterruptedException {
	 
		List<Message> serviceMessageList = new ArrayList<Message>(); 
		RuleEngineExecutor<?, ?>[] threads = new RuleEngineExecutor<?, ?>[rules.size()];
		int i = 0;
 
		for (String rule : rules) {			 
			RuleEngineExecutor<?, ?> r = (RuleEngineExecutor<?, ?>) context.getBean(rule, input, collector);			
			threads[i] = r;
			threads[i++].start();
		}
 
		try {
			for (RuleEngineExecutor<?, ?> thread : threads) {  
				thread.join();
 				serviceMessageList.addAll(thread.getMessageList());
 				log.info(thread.getUuid() + " Run success.");
 			}
		} catch (InterruptedException e) {
			throw new  InterruptedException( e.getLocalizedMessage());
		}


		log.info("Transaction '"+ input.getRequestHeader().getTransactionId() + "' Done all.");
		return serviceMessageList;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}

}

