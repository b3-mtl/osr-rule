package com.mtl.rule.service.rule16;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule16.CheckClaimHistory;
import com.mtl.model.common.PlanPermission;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.ClaimHistoryList;
import com.mtl.rule.model.DT16ClaimHistory;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RuleClaimHistoryService extends AbstractSubRuleExecutor2  {
	
	@Autowired
	private CheckClaimHistory checkClaimHistory;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private ApplicationCache applicationCache;
	
	public RuleClaimHistoryService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	private static final Logger log = LogManager.getLogger(RuleClaimHistoryService.class);
  
	@Override
	public void initial() {
		
	}
	
	@Override
	public boolean preAction() {
		
		return true;
	}

	@Override
	public void execute() {			 
		log.info("Execute Rule16 ClaimHistoryService");
		log.info("## Execute ##");
		List<String> thaiList = null;
		List<String> engList = null;
		String planCode = input.getRequestBody().getBasicInsureDetail().getPlanCode();
		PlanPermission planPermission = applicationCache.getPlanPermissionAppCashMap().get((planCode+"|"));
		boolean exceptPBClaim = false;
		if(planPermission!=null)
		{
			exceptPBClaim =  RuleConstants.FALSE.equals(planPermission.getExceptPBClaim());
		}else {
			exceptPBClaim = true;
		}
		
		
		//start flow
		String[] claimCodeList = input.getRequestBody().getPayorBenefitPersonalData().getClaimCodeList();
		if(claimCodeList !=null && claimCodeList.length >0) {
			
			if(exceptPBClaim) {
				
				ClaimHistoryList list = checkClaimHistory.checkClaimHistoryCodeList(claimCodeList);
				
				for(DT16ClaimHistory temp : list.getInDb()) {
					
					if(temp.getCode()==null || RuleConstants.RULE_16.FALSE_STRING.equals(temp.getB())) {
						String registersystem = input.getRequestHeader().getRegisteredSystem();
						thaiList = new ArrayList<String>();
						thaiList.add(temp.getCode());
						engList = new ArrayList<String>();
						engList.add(temp.getCode());
						Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT16004, thaiList, engList);
						messageList.add(message);
					}
				}
				for(String tempclaim : list.getNotIndb()) {
					
					if(tempclaim!=null) {
						String registersystem = input.getRequestHeader().getRegisteredSystem();
						thaiList = new ArrayList<String>();
						thaiList.add(tempclaim);
						engList = new ArrayList<String>();
						engList.add(tempclaim);
						Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT16004, thaiList, engList);
						messageList.add(message);
					}
				}
				
			}
		}
	}

	@Override
	public void finalAction() {
		//log.info(super.getMessage());
	}
}
 