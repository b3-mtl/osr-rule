package com.mtl.rule.service.rule04;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.service.impl.RuleEngineExecutor;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule04 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule04.class);
	String state = "";
	Timmer timeUsage = new Timmer();
	
	public ServiceRule04(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 04 งวดการชำระเงินเบี้ยประกันภัย";
		rules = new ArrayList<>();	 
		Rule04PremiumPaymentService premiumPaymenService = (Rule04PremiumPaymentService)context.getBean(Rule04PremiumPaymentService.class,this.input,this.collector);
//		Rule04AdditionalDocumentService addDocumentService = (Rule04AdditionalDocumentService)context.getBean(Rule04AdditionalDocumentService.class,this.input,this.collecter);
		rules.add(premiumPaymenService);
//		rules.add(addDocumentService);

	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {
		
		this.rules.parallelStream().forEach(rule -> {
			rule.run();
		});
			
		
	}

	@Override
	public void finalAction() {
		this.rules.parallelStream().forEach(rule -> {
			if(rule.getMessageList() != null ) {
				messageList.addAll(rule.getMessageList());
				documentList.addAll(rule.getDocumentList());
			}
			state = state +"\n"+rule.getSubRuleLog();
		});
		state  = state+"\n"+"Time usage in Rule 04 งวดการชำระเงินเบี้ยประกันภัย :"+timeUsage.timeDiff();
		state  = state+"\n"+"END Rule 04 งวดการชำระเงินเบี้ยประกันภัย";	
		state  = state+"\n"+"]";	
		setRULE_EXECUTE_LOG(state);
		log.info(state); 
	}
}
