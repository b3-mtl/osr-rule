package com.mtl.rule.service.rule05;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.api.core.utils.Timmer;
import com.mtl.collector.service.rule05.PremiumPaymentService;
import com.mtl.collector.service.rule05.SpecialPaymentService;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.rule.RuleResultBean;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.RuleEngineExecutor;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ServiceRule05 extends RuleEngineExecutor<UnderwriteRequest, CollectorModel> {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ServiceRule05.class);
	private List<RuleResultBean> ruleResultBeanList  = new ArrayList<RuleResultBean>();
 
	@Autowired
	private SpecialPaymentService specialPaymentService;
	@Autowired
	private PremiumPaymentService premiumPaymentService;
	@Autowired
	private MessageService messageService;
	String state = "";
	Timmer timeUsage = new Timmer();
	public ServiceRule05(UnderwriteRequest input, CollectorModel collectorModel) {
		super(input, collectorModel);
	}

	@Override
	public void initial() {
	//	log.info("0 initial Rule 05");
 
	}

	@Override
	public boolean preAction() {
 
		return true;
	}

	@Override
	public void execute() { 
		state  = state+"\n"+RuleConstants.RULE_SPACE+"[";	
		state  = state+"\n"+RuleConstants.RULE_SPACE+"Start EXECUTE RULE 5: : ช่องทางการขาย ";	
		Timmer timeUsage = new Timmer();
		
		String basicPlanCode = input.getRequestBody().getBasicInsureDetail().getPlanCode(); 
		AgentInfo agentInfo = collector.getAgentDetail();
		String agentChannel = agentInfo.getAgentChannel(); 
		
		
		state  = state+"\n"+"[";	
		state  = state+"\n"+"Start EXECUTE RULE 5: : ช่องทางการขาย ";	
 
		state  = state+"\n"+"INPUT REQUEST";
		state  = state+"\n"+"Agent Code:"+input.getRequestBody().getAgentCode();
		state  = state+"\n"+"Agent Channel Code:"+agentChannel;
		state  = state+"\n"+"Basic Insure: [Plan Code:"+basicPlanCode+"]";
	 
		
		state  = state+"\n"+"Rider Insure List";
		List<InsureDetail> allInsureList = new ArrayList<InsureDetail>();
		allInsureList.add(input.getRequestBody().getBasicInsureDetail());
		List<InsureDetail> riderList = input.getRequestBody().getRiderInsureDetails();
		if(riderList!=null&&riderList.size()>0) {
			for(InsureDetail riderTmp:riderList) {
				state  = state+"\n"+"[Plan Code:"+riderTmp.getPlanCode()+"]";
				allInsureList.add(riderTmp)	;
				
			} 
		} 
		
		state  = state+"\n"+"INPUT DATA COLLECTOR";	 		
		
		
		
		int loop =1;
		for(InsureDetail insureTmp:allInsureList) {
			
			/**
			 * Step 2 From Document SDS Topic 3.3.4 Check Special Channel
			 * Check planCode and agentChannel in DT05_SPECIAL_PAYMENT
			 * Case 1 -> add Message DT0500Y
			 * Case 2 -> add Message DT05003
			 * Case 3 -> go step3 Check Premium Payment Channel
			 */
			state  = state+"\n"+RuleConstants.RULE_SPACE+"Loop "+loop+++":"+insureTmp.getPlanCode(); 
			String planCodeTmp = insureTmp.getPlanCode(); 
			String planCodeDesc = insureTmp.getPlanCode()+" "+insureTmp.getPlanName();
			 
			state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Check Special Channel in DT05_SPECIAL_PAYMENT [ planCode:"+planCodeTmp+",agentChannel:"+agentChannel+"] ";	
			int specialResult = specialPaymentService.isInSpecialPaymentChannel(planCodeTmp, agentChannel);	
		 
			if(specialResult==1) {
	 
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Case = 1: Found Plancode In  --- >DT0500Y:Rule 5 : แบบประกัน "+planCodeTmp+" สามารถขายในช่องทางการขายนี้ได้"; 
				RuleResultBean ruleResultBeanTmp = new RuleResultBean();
				ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT0500Y);
				List <String> stringInMessageList = new ArrayList<String>();
				stringInMessageList.add(planCodeTmp);
				ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);					
				ruleResultBeanList.add(ruleResultBeanTmp);			
			}else if(specialResult==2) {
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Case=  2: Found Plancode Only   --- >DT05003:Rule 5 : แบบประกัน "+planCodeTmp+" ไม่สามารถขายในช่องทางการขายนี้ได้"; 
	  
				RuleResultBean ruleResultBeanTmp = new RuleResultBean();
				ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT05003);
				List <String> stringInMessageList = new ArrayList<String>();
				stringInMessageList.add(planCodeTmp);
				ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);					
				ruleResultBeanList.add(ruleResultBeanTmp);		
				
			}else if(specialResult==3) { 
				/**
				 * Step 3 From Document SDS Topic 3.3.4 Check Premium Payment Channel
				 * Check planCode and agentChannel in MS_PREMIUM_PAYMENT
				 * true -> add Message DT0500Y
				 * false -> add Message DT05003
				 * end
				 */
				state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#2 Case = 3: Not Found PlanCode --> Next #3 Check Premium Payment Channel"; 
				boolean isCorrectChannel = premiumPaymentService.isInPremiumPaymentChannel(planCodeTmp, agentChannel);
				
				if(isCorrectChannel) {
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#3 TRUE-->  DT0500Y:Rule 5 : แบบประกัน $ สามารถขายในช่องทางการขายนี้ได้";
					RuleResultBean ruleResultBeanTmp = new RuleResultBean();
					ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT0500Y);
					List <String> stringInMessageList = new ArrayList<String>();
					stringInMessageList.add(planCodeTmp);
					ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);					
					ruleResultBeanList.add(ruleResultBeanTmp);		
				}else {
					state  = state+"\n"+RuleConstants.SUB_RULE_SPACE+"#3 FALSE-->DT05003:Rule 5 : แบบประกัน "+planCodeTmp+" ไม่สามารถขายในช่องทางการขายนี้ได้"; 
					RuleResultBean ruleResultBeanTmp = new RuleResultBean();
					ruleResultBeanTmp.setMessageCode(RuleConstants.MESSAGE_CODE.DT05003);
					List <String> stringInMessageList = new ArrayList<String>();
					stringInMessageList.add(planCodeTmp);
					ruleResultBeanTmp.setStringInMessageCode(stringInMessageList);					
					ruleResultBeanList.add(ruleResultBeanTmp);						
					
				}
			}  
		}	
	}

	@Override
	public void finalAction() {
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		if(ruleResultBeanList!=null) {
			for(RuleResultBean ruleResultTmp:ruleResultBeanList) {	
				if(ruleResultTmp.isNoParameterMessage()) {
					Message message1 = messageService.getOneMessage(registersystem,ruleResultTmp.getMessageCode() );
					messageList.add(message1);	
				}else {
					//log.info("ruleResultTmp.getStringInMessageCode size:"+ruleResultTmp.getStringInMessageCode().size());
					List<String> thaiList = ruleResultTmp.getStringInMessageCode();
					List<String> engList =  ruleResultTmp.getStringInMessageCode();
					Message message1 = messageService.getMessage(registersystem,ruleResultTmp.getMessageCode(), thaiList, engList);
					messageList.add(message1);							
				}
			}
		}
		state  = state+"\n"+"Time usage in Rule 05 ช่องทางการขาย :"+timeUsage.timeDiff();
		state  = state+"\n"+"END RULE 5: : ช่องทางการขาย ";	
		state  = state+"\n"+"]";
		setRULE_EXECUTE_LOG(state);
		log.info(state); 
	 
	}
}


/*
DT05001	 Rule 5 : ไม่ได้ระบุชื่อแบบประกันภัย / ช่องทางการขาย	Rule 5 : Insurance plan/ sale channel is not indicated.	
DT05002	 Rule 5 : ไม่พบข้อมูลแบบประกัน $ ในระบบ	Rule 5 : $ insurance plan cannot be found in the system.
DT05003	 Rule 5 : แบบประกัน $ ไม่สามารถขายในช่องทางการขายนี้ได้	Rule 5 : $ insurance plan is not available to sell through this sales channel.	
DT0500Y	 Rule 5 : แบบประกัน $ สามารถขายในช่องทางการขายนี้ได้	Rule 5 : $ insurance plan is available on this sale channel.
 * 
 * */
