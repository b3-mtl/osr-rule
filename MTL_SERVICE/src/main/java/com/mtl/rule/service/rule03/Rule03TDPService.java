package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckSumByPlan;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03SumByPlan;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03TDPService extends AbstractSubRuleExecutor2 {

	@Autowired
	private CheckSumByPlan checkSumByPlan;

	@Autowired
	private GetSumInsureService sumInsureService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private ApplicationCache applicationCache;

	String state = "";
	List<String> listMsgCode = new ArrayList<String>();
	boolean result = false;

	public Rule03TDPService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 ตรวจสอบแบบประกันภัยทุพลภาพสิ้นเชิงถาวร";

		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> tdpPlanList = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}

		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is TDP Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planType = temp.getPlanHeader().getPlanType();
			if (StringUtils.isNoneBlank(planType)) {
				if (checkSumByPlan.checkValueInParameter(RuleConstants.RULE_03.TDP_PLAN_TYPE, planType)) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] is TDP Plan";
					tdpPlanList.add(temp);
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] not TDP Plan";
				}
			}
		}

		if (tdpPlanList.size() > 0) {

			state = state + "\n" + RuleConstants.RULE_SPACE + "#2 Prepare Data : SumInsured (Current + History)";
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.1 SumInsured Current (Basic + Rider) ";

			state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Check Suminsured is between in PlanCode ";
			for (InsureDetail temp : tdpPlanList) {
				PlanHeader planHeader = temp.getPlanHeader();
				BigDecimal allSumInsured = BigDecimal.ZERO;
				BigDecimal allSumInsuredHistByPlanType = BigDecimal.ZERO;
				state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured Current [planType :"
						+ planHeader.getPlanType() + "] = " + temp.getInsuredAmount();
				state = state + "\n" + RuleConstants.RULE_SPACE + "	2.2 History SumInsured By Plan Type : TDP";
				allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByFindPlanType(historyInsureList,
						RuleConstants.RULE_03.TDP_PLAN_TYPE);
				allSumInsured = allSumInsured.add(allSumInsuredHistByPlanType);
				allSumInsured = allSumInsured.add(new BigDecimal(temp.getInsuredAmount()));
				state = state + "\n" + RuleConstants.RULE_SPACE + "	allSumInsured = " + allSumInsured;

				List<DT03SumByPlan> listByPlan = applicationCache.getRule03_dt03SumByPlan();
				
				
				
				if (rider != null) {
					if(allSumInsured.compareTo(new BigDecimal (basic.getInsuredAmount())) > 0){
						List<String> param = new ArrayList<String>();
						param.add(allSumInsured.toString());
						param.add(basic.getInsuredAmount());
						Message message = messageService.getMessage(registersystem, "DT03031", param,
								param);
						messageList.add(message);
						break;
					}
				}
				
				for (DT03SumByPlan item : listByPlan) {
					DT03SumByPlan msgCode = checkSumByPlan.checkBetweenSumInsuredNew(planHeader, item, allSumInsured);
					state = state+"\n"+RuleConstants.RULE_SPACE+" 3.1 Input :[planCode : "+temp.getPlanCode()+" ,  allSumInsured:"+allSumInsured+"]";
					state = state + "\n" + RuleConstants.RULE_SPACE + "	Result  ?:" + msgCode;
					if (msgCode != null) {
						if (StringUtils.isNoneBlank(msgCode.getMessageCode())) {
							// listMsgCode.add(msgCode);
							List<String> param = new ArrayList<String>();
							param.add(msgCode.getMaxSuminsured().toString());
							param.add(allSumInsured.toString());
							Message message = messageService.getMessage(registersystem, msgCode.getMessageCode(), param,
									param);
							messageList.add(message);

						}
					}
					
				
					
					
				}
			}
		} else {
			state = state + "\n" + RuleConstants.RULE_SPACE + "	#1 Result : Not found Term Plan ---> End process";
		}
	}

	@Override
	public void finalAction() {

		state = state + "\n" + "END Rule 03 ตรวจสอบแบบประกันภัยทุพลภาพสิ้นเชิงถาวร";
		state = state + "\n" + "]";
		setSubRuleLog(state);
	}

}
