package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckAccident;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03ACCSum;
import com.mtl.rule.model.DT03ACCSumMax;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03AccidentService extends AbstractSubRuleExecutor2 {

	@Autowired
	private GetSumInsureService sumInsureService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private CheckAccident checkAccident;

	@Autowired
	private ApplicationCache applicationCache;
	
	String state = "";
	boolean result = false;

	public Rule03AccidentService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 ตรวจสอบแบบประกันกลุ่มอุบัติเหตุ";

		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		BigDecimal basicInsure = new BigDecimal(basic.getInsuredAmount());
		String age = input.getRequestBody().getPersonalData().getAge();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> accidentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> accidentPlanListAcc = new ArrayList<InsureDetail>();
		List<InsureDetail> accidentPlanListDea = new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}

		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Accident Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planType = temp.getPlanHeader().getPlanType();
			String planCode = temp.getPlanHeader().getPlanCode();
			if (StringUtils.isNoneBlank(planType)) {
				if (checkAccident.checkValueInParameter(RuleConstants.RULE_03.ACCIDENT_PLAN_TYPE, planType)) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] is Accident Plan";
					accidentPlanList.add(temp);
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] not Accident Plan";
				}
			}
			
			if (StringUtils.isNoneBlank(planCode)) {
				if (checkAccident.checkValueInParameter(RuleConstants.RULE_03.ACCIDENTAL, planCode)) {
					accidentPlanListAcc.add(temp);
				} 
			}
			
			if (StringUtils.isNoneBlank(planCode)) {
				if (checkAccident.checkValueInParameter(RuleConstants.RULE_03.DEATH, planCode)) {			
					accidentPlanListDea.add(temp);
				} 
			}
			
		}

		if (accidentPlanList.size() > 0 || accidentPlanListAcc.size() > 0  || accidentPlanListDea.size() > 0 ) {
			BigDecimal allSumInsured = BigDecimal.ZERO;
			BigDecimal allSumInsuredByA1 = BigDecimal.ZERO;
			BigDecimal allSumInsuredByA2A3 = BigDecimal.ZERO;
			BigDecimal allSumInsuredByPA = BigDecimal.ZERO;
			BigDecimal allSumInsuredByAcdt = BigDecimal.ZERO;
			BigDecimal allSumInsuredByDeath = BigDecimal.ZERO;
			BigDecimal allSumInsuredBysum = BigDecimal.ZERO;
			
			state = state + "\n" + RuleConstants.RULE_SPACE + "#2 Prepare Data : SumInsured ";
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.1 SumInsured (Current + History) ";

			String[] a1 = new String[] { RuleConstants.RULE_03.A1 };
			allSumInsuredByA1 = sumCurrentAndHist(historyInsureList, accidentPlanList, a1);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (A1) = " + allSumInsuredByA1;

			String[] a2a3 = new String[] { RuleConstants.RULE_03.A2, RuleConstants.RULE_03.A3 };
			allSumInsuredByA2A3 = sumCurrentAndHist(historyInsureList, accidentPlanList, a2a3);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (A2,A3) = " + allSumInsuredByA2A3;

			String[] pa = new String[] { RuleConstants.RULE_03.AA, RuleConstants.RULE_03.AB };
			allSumInsuredByPA = sumCurrentAndHist(historyInsureList, accidentPlanList, pa);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (PA) = " + allSumInsuredByPA;


			/* Different from where on plan code */
			String[] acdt = new String[] { RuleConstants.RULE_03.RAD };
//			allSumInsuredByAcdt = sumCurrentAndHistByPlanCode(historyInsureList, accidentPlanListAcc, acdt);
			allSumInsuredByAcdt = sumInsureService.getSumHistoryInsureByFindPlanCode(accidentPlanListAcc,historyInsureList, RuleConstants.RULE_03.ACCIDENTAL);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Accident) = " + accidentPlanListAcc;

			String[] death = new String[] { RuleConstants.RULE_03.RAE };
//			allSumInsuredByDeath = sumCurrentAndHistByPlanCode(historyInsureList, accidentPlanListDea, death);
			allSumInsuredByDeath = sumInsureService.getSumHistoryInsureByFindPlanCode(accidentPlanListDea,historyInsureList, RuleConstants.RULE_03.DEATH);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Death) = " + accidentPlanListDea;

			/* summary all New  */
//			String[] sumall = checkAccident.getPlanTypeFormPlanCode(RuleConstants.RULE_03.ACCIDENT_PLAN_CODE);
//			allSumInsuredBysum = sumCurrentAndHist(historyInsureList, accidentPlanList, acdt);
			
			allSumInsuredBysum = allSumInsuredByA1.add(allSumInsuredByA2A3).add(allSumInsuredByPA).add(allSumInsuredByAcdt).add(allSumInsuredByDeath) ;
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (Summay All) = " + allSumInsuredBysum;
			
			
//			allSumInsuredByAcdt = sumInsureService.getSumHistoryInsureByFindPlanCode(accidentPlanList,historyInsureList,
//			RuleConstants.RULE_03.ACCIDENT_PLAN_CODE);
			
			String[] allPlanType = new String[] { RuleConstants.RULE_03.A1, RuleConstants.RULE_03.A2, RuleConstants.RULE_03.A3, RuleConstants.RULE_03.AA, RuleConstants.RULE_03.AB };
			allSumInsured = allSumInsuredByA1.add(allSumInsuredByA2A3.add(allSumInsuredByPA));
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured (All) = " + allSumInsured;
			
			Set<String> msgList = new HashSet<String>();
			String oldplanType = "start";
			for(InsureDetail temp:accidentPlanList) {
				PlanHeader phdr = temp.getPlanHeader();
				String planType = phdr.getPlanType();

//				if(oldplanType.equals("start")){
//					oldplanType = planType ;
//				}
				
				if(oldplanType.equals(planType)){
					
					
					continue;
					
				}else{
					
					if((RuleConstants.RULE_03.A2.equals(planType) ||RuleConstants.RULE_03.A3.equals(planType)) && (RuleConstants.RULE_03.A2.equals(oldplanType) ||RuleConstants.RULE_03.A3.equals(oldplanType))){
						continue;
					}
					if((RuleConstants.RULE_03.AA.equals(planType) || RuleConstants.RULE_03.AB.equals(planType)) && (RuleConstants.RULE_03.AA.equals(oldplanType) || RuleConstants.RULE_03.AB.equals(oldplanType))){
						continue;
					}
					
					oldplanType = planType ;
				}
				
				//step 3
				if(RuleConstants.RULE_03.A1.equals(planType)) {
					if(allSumInsuredByA1 != BigDecimal.ZERO) {
						msgList.addAll(checkAccSumPlanType(a1, age, allSumInsuredByA1 , basicInsure,registersystem));
					}
				}
				//step 4
				if(RuleConstants.RULE_03.A2.equals(planType) || RuleConstants.RULE_03.A3.equals(planType)) {
					
					if(allSumInsuredByA2A3 != BigDecimal.ZERO) {
						msgList.addAll(checkAccSumPlanType(a2a3, age, allSumInsuredByA2A3, basicInsure,registersystem));

					}
				}
				//step 5
				if(RuleConstants.RULE_03.AA.equals(planType) || RuleConstants.RULE_03.AB.equals(planType)) {
					if(allSumInsuredByPA != BigDecimal.ZERO) {
						msgList.addAll(checkAccSumPlanType(pa, age, allSumInsuredByPA, basicInsure,registersystem));

					}
				}
				
			}
				//step 6.1
				if(allSumInsuredByAcdt != BigDecimal.ZERO ) {
					msgList.addAll(checkAccSumPlanType(acdt, age, allSumInsuredByAcdt, basicInsure,registersystem));
				}
				//step 6.2
				if( allSumInsuredByDeath != BigDecimal.ZERO) {
					msgList.addAll(checkAccSumPlanType(death, age, allSumInsuredByDeath, basicInsure,registersystem));	
				}
//				step 7	All
				messageList.size();
				if(messageList.size()==0){
					msgList.addAll(checkAccSumMaxPlanType( age, allSumInsuredBysum, basicInsure,registersystem,allSumInsuredByA1));
				}

//			for(String code :msgList) {
//				Message msg = messageService.getOneMessage(registersystem, code);
//				messageList.add(msg);
//			}
		} else {
			state = state + "\n" + RuleConstants.RULE_SPACE + "	#1 Result : Not found Accident Plan ---> End process";
		}
	}

	@Override
	public void finalAction() {
		state = state + "\n" + "END Rule 03 ตรวจสอบแบบประกันกลุ่มอุบัติเหตุ";
		state = state + "\n" + "]";
		setSubRuleLog(state);
	}

	private BigDecimal sumCurrentAndHist(List<HistoryInsure> historyInsureList, List<InsureDetail> accidentPlanList,
			String[] planType) {
		BigDecimal allSumInsuredCurr;
		allSumInsuredCurr = sumCurrentPlanType(accidentPlanList, planType);
		BigDecimal allSumInsuredHist = sumInsureService.getSumHistoryInsureByPlanType(historyInsureList, planType);
		allSumInsuredCurr = allSumInsuredCurr.add(allSumInsuredHist);
		return allSumInsuredCurr;
	}
	
	private BigDecimal sumCurrentAndHistByPlanCode(List<HistoryInsure> historyInsureList, List<InsureDetail> accidentPlanList,
			String[] planCode) {
		BigDecimal allSumInsuredCurr;
		allSumInsuredCurr = sumCurrentPlanCode(accidentPlanList, planCode);
		BigDecimal allSumInsuredHist = sumInsureService.getSumHistoryInsureByPlanType(historyInsureList, planCode);
		allSumInsuredCurr = allSumInsuredCurr.add(allSumInsuredHist);
		return allSumInsuredCurr;
	}

	private BigDecimal sumCurrentPlanType(List<InsureDetail> accidentPlanList, String[] valueList) {
		BigDecimal allSumInsured = BigDecimal.ZERO;
		for (String planType : valueList) {
			for (InsureDetail temp : accidentPlanList) {
				BigDecimal insured = new BigDecimal(temp.getInsuredAmount());
				String tempPlanType = temp.getPlanHeader().getPlanType();
				if (planType.equals(tempPlanType)) {
					allSumInsured = allSumInsured.add(insured);
				}
			}
		}
		return allSumInsured;
	}
	
	private BigDecimal sumCurrentPlanCode(List<InsureDetail> accidentPlanList, String[] valueList) {
		BigDecimal allSumInsured = BigDecimal.ZERO;
		for (String planCode : valueList) {
			for (InsureDetail temp : accidentPlanList) {
				BigDecimal insured = new BigDecimal(temp.getInsuredAmount());
				String tempPlanCode = temp.getPlanHeader().getPlanCode();
				if (planCode.equals(tempPlanCode)) {
					allSumInsured = allSumInsured.add(insured);
				}
			}
		}
		return allSumInsured;
	}
	
	
	public Set<String> checkAccSumPlanType(String[] planType, String age, BigDecimal allSuminsure , BigDecimal basicSuminsure, String registersystem) {
		Set<String> msgCode = new HashSet<String>();
		for (String type : planType) {

			AgentInfo agenCode = collectorModel.getAgentDetail();
			String agentGroup = agenCode.getAgentGroup();
			boolean result_limit = false;
			boolean result_max = false;
			String limitCode = "";
			String maxCode = "";
			String ageCode = "";
			List<DT03ACCSum> list = applicationCache.getRule03_dt03ACCSum().get(type);
			List<DT03ACCSum> listfilterAge = new ArrayList<>();
			String limitSuminsured = "";
			String maxSuminsured = "";

			if(list != null) {
				for(DT03ACCSum item:list) {
					if(item.getMinAge() != null || item.getMaxAge() != null ) {
						if(RuleUtils.betweenNumber(Long.valueOf(age), Long.valueOf(item.getMinAge()), Long.valueOf(item.getMaxAge()))) {
							listfilterAge.add(item);
						}
					}
					
				}
			}
			
			if(listfilterAge != null) {
				for(DT03ACCSum item:listfilterAge) {
					
					if(RuleConstants.OTHERWISE.equals(item.getLimitSuminsured())) {
						limitCode = item.getMessageCode();
					}
					if(RuleConstants.OTHERWISE.equals(item.getMaxSuminsured())) {
						maxCode = item.getMessageCode();
					}
				}
			}
			list = listfilterAge;
			
			if (list == null || list.size() == 0) {
				if(RuleConstants.RULE_03.A1.equals(type)) {
					
					List<String> param = new ArrayList<String>();
					param.add(age);
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03313 , param, param);
					messageList.add(message);
					return msgCode;
					
//					msgCode.add(RuleConstants.MESSAGE_CODE.DT03313);
				}
				if(RuleConstants.RULE_03.A2.equals(type) || RuleConstants.RULE_03.A3.equals(type)) {
					
					List<String> param = new ArrayList<String>();
					param.add(age);
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03312 , param, param);
					messageList.add(message);
					return msgCode;
//					msgCode.add(RuleConstants.MESSAGE_CODE.DT03312);
				}
				if(RuleConstants.RULE_03.AA.equals(type) || RuleConstants.RULE_03.AB.equals(type)) {
					
					List<String> param = new ArrayList<String>();
					param.add(age);
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03316 , param, param);
					messageList.add(message);
					return msgCode;
//					msgCode.add(RuleConstants.MESSAGE_CODE.DT03316);
				}
				if(RuleConstants.RULE_03.ACCIDENT_PLAN_TYPE.equals(type)) {
					
					List<String> param = new ArrayList<String>();
					param.add(age);
					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03317 , param, param);
					messageList.add(message);
					return msgCode;
//					msgCode.add(RuleConstants.MESSAGE_CODE.DT03317);
				}
			} else {
				
				boolean checkAge = false;
				for (DT03ACCSum temp : list) {
					
					
					if(temp.getMinAge() == null || temp.getMaxAge() == null ) {
						checkAge = false;
					}else{
						checkAge = RuleUtils.betweenNumber(Long.valueOf(age), temp.getMinAge(), temp.getMaxAge());
					}

					if(!(temp.getLimitSuminsured() == null || RuleConstants.OTHERWISE.equals(temp.getLimitSuminsured()))){
						BigDecimal limit = new BigDecimal(temp.getLimitSuminsured());
						limit = limit.multiply(basicSuminsure);
						result_limit = allSuminsure.compareTo(limit) > 0;
						
						
						limitSuminsured = temp.getLimitSuminsured();
						if(result_limit){
							break;
						}
					}
					
					
					if(!(temp.getMaxSuminsured() == null || RuleConstants.OTHERWISE.equals(temp.getMaxSuminsured()))){
						BigDecimal max = new BigDecimal(temp.getMaxSuminsured());
						result_max = allSuminsure.compareTo(max) > 0;
						maxSuminsured = temp.getMaxSuminsured();
					
					}

				}
						
						if(result_limit) {
							if(RuleConstants.RULE_03.A1.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(limitSuminsured);
								param.add(allSuminsure.toString());
								param.add(basicSuminsure.toString());
								Message message = messageService.getMessage(registersystem, limitCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03304);
							}
							if(RuleConstants.RULE_03.A2.equals(type) || RuleConstants.RULE_03.A3.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(limitSuminsured);
								param.add(allSuminsure.toString());
								param.add(basicSuminsure.toString());
								Message message = messageService.getMessage(registersystem, limitCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03307);
							}
							if(RuleConstants.RULE_03.AA.equals(type) || RuleConstants.RULE_03.AB.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(limitSuminsured);
								param.add(allSuminsure.toString());
								param.add(basicSuminsure.toString());
								Message message = messageService.getMessage(registersystem, limitCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03309);
							}
							if(RuleConstants.RULE_03.RAE.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(limitSuminsured);
								param.add(allSuminsure.toString());
								param.add(basicSuminsure.toString());
								Message message = messageService.getMessage(registersystem, limitCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03318);
							}
							if(RuleConstants.RULE_03.RAD.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(limitSuminsured);
								param.add(allSuminsure.toString());
								param.add(basicSuminsure.toString());
								Message message = messageService.getMessage(registersystem, limitCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03318);
							}
						}
						
						
						
						
						if(result_max) {
							if(RuleConstants.RULE_03.A1.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								param.add(agentGroup);
								Message message = messageService.getMessage(registersystem, maxCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03305);
							}
							if(RuleConstants.RULE_03.A2.equals(type) || RuleConstants.RULE_03.A3.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								param.add(agentGroup);
								
								Message message = messageService.getMessage(registersystem, maxCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03308);
								
							}
							if(RuleConstants.RULE_03.AA.equals(type) || RuleConstants.RULE_03.AB.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								param.add(agentGroup);
								Message message = messageService.getMessage(registersystem, maxCode , param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03308);
								
							}
							if(RuleConstants.RULE_03.ACCIDENT_PLAN_TYPE.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								param.add(agentGroup);
								Message message = messageService.getMessage(registersystem, maxCode, param, param);
								messageList.add(message);
								return msgCode;
//								msgCode.add(RuleConstants.MESSAGE_CODE.DT03319);
							}
							if(RuleConstants.RULE_03.RAE.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								param.add(agentGroup);
								Message message = messageService.getMessage(registersystem, maxCode, param, param);
								messageList.add(message);
								return msgCode;
							}
							if(RuleConstants.RULE_03.RAD.equals(type)) {
								
								List<String> param = new ArrayList<String>();
								param.add(age);
								param.add(maxSuminsured);
								param.add(allSuminsure.toString());
								Message message = messageService.getMessage(registersystem, maxCode , param, param);
								messageList.add(message);
								return msgCode;

							}
						}
					
					
					
				
			}
		}
		return msgCode;
	}

	
	public Set<String> checkAccSumMaxPlanType(String age, BigDecimal allSuminsure, BigDecimal basicSuminsure,
			String registersystem, BigDecimal allSumInsuredByA1) {
		Set<String> msgCode = new HashSet<String>();

		boolean result_limit = false;
		boolean result_max = false;
		String limitCode = "";
		String maxCode = "";
		String ageCode = "";
		List<DT03ACCSumMax> list = applicationCache.getRule03_dt03ACCSumMax();
		List<DT03ACCSumMax> listfilterAge = new ArrayList<>();
		List<DT03ACCSumMax> listfilterInsured = new ArrayList<>();
		String limitSuminsured = "";
		String maxSuminsured = "";

		if (list != null) {
			for (DT03ACCSumMax item : list) {
				if (item.getMinAge() != null || item.getMaxAge() != null) {
					if (RuleUtils.betweenNumber(Long.valueOf(age), Long.valueOf(item.getMinAge()),Long.valueOf(item.getMaxAge()))) {
						listfilterAge.add(item);
					}
				}
//				if (item.getMessageCode()!=null) {
//					maxCode = item.getMessageCode();
//				}
			}
		}
		list = listfilterAge;
		
		if(list.size()>1){
			if(allSumInsuredByA1.compareTo(new BigDecimal("10000000"))>0){
				for (DT03ACCSumMax dt03accSumMax : listfilterAge) {
					if("15000000".equals(dt03accSumMax.getMaxSuminsuredAll())){
						listfilterInsured.add(dt03accSumMax);
					}
				}
				list = listfilterInsured ;
			}else{
				for (DT03ACCSumMax dt03accSumMax : listfilterAge) {
					if("10000000".equals(dt03accSumMax.getMaxSuminsuredAll())){
						listfilterInsured.add(dt03accSumMax);
					}
				}
				list = listfilterInsured ;
			}
		}
		
		if (list != null || list.size() != 0) {

			boolean checkAge = false;
			for (DT03ACCSumMax temp : list) {

				if (temp.getMinAge() == null || temp.getMaxAge() == null) {
					checkAge = false;
				} else {
					checkAge = RuleUtils.betweenNumber(Long.valueOf(age), temp.getMinAge(), temp.getMaxAge());
				}

				if (!(temp.getMaxSuminsuredAll() == null || RuleConstants.OTHERWISE.equals(temp.getMaxSuminsuredAll()))) {
					BigDecimal max = new BigDecimal(temp.getMaxSuminsuredAll());
					
					
					result_max = allSuminsure.compareTo(max) > 0;
					maxSuminsured = temp.getMaxSuminsuredAll();
					
					
//					maxCode = temp.getMessageCode();
				}
				if (temp.getMessageCode()!=null) {
					maxCode = temp.getMessageCode();
				}

			}

			if (result_max) {

				List<String> param = new ArrayList<String>();
				param.add(age);
				param.add(maxSuminsured);
				param.add(allSuminsure.toString());
				Message message = messageService.getMessage(registersystem, maxCode, param, param);
				messageList.add(message);
				return msgCode;

			}

		}

		return msgCode;
	}
	

}
