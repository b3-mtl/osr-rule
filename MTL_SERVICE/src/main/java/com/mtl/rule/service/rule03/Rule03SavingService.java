package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckOPD;
import com.mtl.collector.service.rule03.CheckSaving;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03SavingService  extends AbstractSubRuleExecutor2 {

	@Autowired
	private GetSumInsureService sumInsureService;
	
	@Autowired
	private MessageService messageService; 
	
	@Autowired
	private CheckSaving checkSaving;

	String state = ""; 
	boolean result = false;
	
	public Rule03SavingService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state  = state+"\n"+"[";	
		state  = state+"\n"+"START EXECUTE Rule 03 ตรวจสอบแบบประกันภัยสะสมทรัพย์ 5/5 และ 10/10";	 
		
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
        List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
        List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
        List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> savingPlanList = new ArrayList<InsureDetail>();
        currentPlanList.add(basic);
        if(rider !=null) {
        	currentPlanList.addAll(rider);
        }
        
     // get planType
   		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Saving Plan (true or false) ?";
   		for (InsureDetail temp : currentPlanList) {
   			String planType = temp.getPlanHeader().getPlanType();
   			if (StringUtils.isNoneBlank(planType)) {
  				if (checkSaving.checkValueInParameter(RuleConstants.RULE_03.SAVING_PLAN_TYPE, planType)) {
   					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
   							+ " ,PlanType :" + planType + " ] is Saving Plan";
   					savingPlanList.add(temp);
   				} else {
   					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
   							+ " ,PlanType :" + planType + " ] not Saving Plan";
   				}
   			}
   		}
        
        if(savingPlanList.size() > 0) {
        	BigDecimal allSumInsured= BigDecimal.ZERO;
        	BigDecimal allSumInsuredCurrent= BigDecimal.ZERO;
        	BigDecimal allSumInsuredHistByPlanType= BigDecimal.ZERO;
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"#2 Prepare Data : SumInsured (Current + History)";
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	2.1 SumInsured Current (Basic + Rider) ";
        	for(InsureDetail temp:currentPlanList) {
        		BigDecimal insure = new BigDecimal(temp.getInsuredAmount());
        		allSumInsuredCurrent = allSumInsuredCurrent.add(insure);
            }
        	state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured Current (Basic + Rider) = "+ allSumInsuredCurrent;
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.2 History SumInsured By Plan Type : Saving";
			allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByFindPlanType(historyInsureList,RuleConstants.RULE_03.SAVING_PLAN_TYPE);
			allSumInsured = allSumInsuredCurrent.add(allSumInsuredHistByPlanType);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	allSumInsured = " + allSumInsured;
			
			state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Check Suminsured is between in PlanCode ";
			for(InsureDetail temp:currentPlanList) {
				PlanHeader planHeader= temp.getPlanHeader();
				Double min = planHeader.getMinimumAmount();
				Double max = planHeader.getMaximumAmount();
				boolean checkBetween = checkSaving.checkBetweenSumInsured(min, max, allSumInsured);
	        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	3.1 Input :[planCode : "+temp.getPlanCode()+" , min :"+min+" , max:"+max+" , allSumInsured:"+allSumInsured+"]";
	        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	Result  ?:"+checkBetween;
	        	if(checkBetween) {
	        		state  = state+"\n"+RuleConstants.RULE_SPACE+"	3.2 Add Message Code DT03518 and End process ";
	        		Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT03518, Arrays.asList(""), Arrays.asList("")); 
					messageList.add(message);
	        	}
			}
        	
        }else {
        	state  = state+"\n"+RuleConstants.RULE_SPACE+"	#1 Result : Not found Saving Plan ---> End process";
        }
	}

	@Override
	public void finalAction() {
		state  = state+"\n"+"END Rule 03 ตรวจสอบแบบประกันภัยสะสมทรัพย์ 5/5 และ 10/10";	
		state  = state+"\n"+"]";
		setSubRuleLog(state);
	}

}
