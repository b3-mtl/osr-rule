package com.mtl.rule.service.rule03;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mtl.collector.service.GetSumInsureService;
import com.mtl.collector.service.rule03.CheckDiagnosis;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.CollectorModel;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.Message;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPG;
import com.mtl.rule.model.DT03DiagnosisSumInsuredPT;
import com.mtl.rule.service.impl.AbstractSubRuleExecutor2;
import com.mtl.rule.util.MessageService;
import com.mtl.rule.util.RuleConstants;
import com.mtl.rule.util.RuleUtils;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Rule03DiagnosisService extends AbstractSubRuleExecutor2 {

	@Autowired
	private GetSumInsureService sumInsureService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private CheckDiagnosis checkDiagnosis;

	String state = "";
	boolean result = false;

	public Rule03DiagnosisService(UnderwriteRequest input, CollectorModel collecter) {
		super(input, collecter);
	}

	@Override
	public void initial() {
	}

	@Override
	public boolean preAction() {
		return true;
	}

	@Override
	public void execute() {

		state = state + "\n" + "[";
		state = state + "\n" + "START EXECUTE Rule 03 ตรวจสอบทุนประกันประเภทโรคร้ายแรง";

		PersonalData personalData = input.getRequestBody().getPersonalData();
		String age = personalData.getAge();
		String registersystem = input.getRequestHeader().getRegisteredSystem();
		InsureDetail basic = input.getRequestBody().getBasicInsureDetail();
		List<InsureDetail> rider = input.getRequestBody().getRiderInsureDetails();
		List<HistoryInsure> historyInsureList = collectorModel.getHistoryInsureList();
		List<InsureDetail> currentPlanList = new ArrayList<InsureDetail>();
		List<InsureDetail> diagnosisPlanList = new ArrayList<InsureDetail>();

//		InsureDetail basicTemp = input.getRequestBody().getBasicInsureDetail();
		InsureDetail TempBasic =  new InsureDetail();
		List<InsureDetail> TempRider =  new ArrayList<InsureDetail>();
		currentPlanList.add(basic);
		if (rider != null) {
			currentPlanList.addAll(rider);
		}
		
		TempBasic.setPlanCode(basic.getPlanCode());
		TempBasic.setInsuredAmount(basic.getInsuredAmount());
		TempBasic.setPlanHeader(basic.getPlanHeader());
		
		
		
		for (InsureDetail insureDetail : rider) {
			InsureDetail insureNew = new InsureDetail();
			insureNew.setPlanCode(insureDetail.getPlanCode());
			insureNew.setInsuredAmount(insureDetail.getInsuredAmount());
			TempRider.add(insureNew);
		};

		
		// get planType
		state = state + "\n" + RuleConstants.RULE_SPACE + "#1 Check List PlanType is Diagnosis Plan (true or false) ?";
		for (InsureDetail temp : currentPlanList) {
			String planType = temp.getPlanHeader().getPlanType();
			boolean CheckDupilat = false;
			if (StringUtils.isNoneBlank(planType)) {
				if (checkDiagnosis.checkValueInParameter(RuleConstants.RULE_03.DIAG_PLAN_TYPE, planType)) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode() + " ,PlanType :" + planType + " ] is OPD Plan";
				for (InsureDetail insureDetail : diagnosisPlanList) {
					if (insureDetail.getPlanHeader().getPlanType()!=null) {
						if(insureDetail.getPlanHeader().getPlanType().equals(planType)){		
							BigDecimal insuredAmount = new BigDecimal(insureDetail.getInsuredAmount());
							insuredAmount = insuredAmount.add(new BigDecimal(temp.getInsuredAmount()));
							insureDetail.setInsuredAmount(insuredAmount.toString());
							CheckDupilat  = true;
							break;
						}	
					}			
				}
					
				if(CheckDupilat){
					continue;
				}
					diagnosisPlanList.add(temp);
				} else {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	1.1 [PlanCode :" + temp.getPlanCode()
							+ " ,PlanType :" + planType + " ] not OPD Plan";
				}
			}
		}

		if (diagnosisPlanList.size() > 0) {
			BigDecimal allSumInsured = BigDecimal.ZERO;
			BigDecimal allSumInsuredCurrent = BigDecimal.ZERO;
			BigDecimal allSumInsuredHistByPlanType = BigDecimal.ZERO;
//			BigDecimal allSumInsuredHistByPlanType = BigDecimal.ZERO;
			
			
			state = state + "\n" + RuleConstants.RULE_SPACE + "#2 Prepare Data : SumInsured (Current + History)";
			state = state + "\n" + RuleConstants.RULE_SPACE + "	#2.1 SumInsured Current (Basic + Rider) ";
			for (InsureDetail temp : currentPlanList) {
				BigDecimal insure = new BigDecimal(temp.getInsuredAmount());
				allSumInsuredCurrent = allSumInsuredCurrent.add(insure);
			}
			state = state + "\n" + RuleConstants.RULE_SPACE + "	SumInsured Current (Basic + Rider) = "
					+ allSumInsuredCurrent;
			state = state + "\n" + RuleConstants.RULE_SPACE + "	2.2 History SumInsured By Plan Type : Diagnosis";
			allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByFindPlanType(historyInsureList,
					RuleConstants.RULE_03.DIAG_PLAN_TYPE);
			allSumInsured = allSumInsuredCurrent.add(allSumInsuredHistByPlanType);
			state = state + "\n" + RuleConstants.RULE_SPACE + "	allSumInsured = " + allSumInsured;
			// Check By Plan Type
			
			boolean checkMaxInsured = false;
			String mesCode = null;
			
			for (InsureDetail temp : diagnosisPlanList) {

				PlanHeader hdr = temp.getPlanHeader();
				String planType = hdr.getPlanType();
				// String planGroup = hdr.getClassOfBusiness();
				BigDecimal histByPlanType = sumInsureService.getSumHistoryInsureByPlanType(historyInsureList,hdr.getPlanType());

				state = state + "\n" + RuleConstants.RULE_SPACE + "#3 Check Diagnosis By Plan Type : [" + planType
						+ "]";
				List<DT03DiagnosisSumInsuredPT> dt03_planType = checkDiagnosis.checkDiagnosisPlanType(planType, age);
				state = state + "\n" + RuleConstants.RULE_SPACE + "	#3 Result size == 0 ?:" + dt03_planType.size();
				if (dt03_planType == null || dt03_planType.size() == 0) {
					state = state + "\n" + RuleConstants.RULE_SPACE + "	#3.1 Add Message Code DT03101 and End process ";
//					Message message = messageService.getMessage(registersystem, RuleConstants.MESSAGE_CODE.DT03101,
//							Arrays.asList(""), Arrays.asList(""));
//					messageList.add(message);
				} else {
					
					boolean checkLimit = false;
					boolean checkSum = false;
					BigDecimal basicInsured = new BigDecimal(basic.getInsuredAmount());
					BigDecimal insuredAmount = new BigDecimal(temp.getInsuredAmount());
					BigDecimal limitLifeInsured = BigDecimal.ZERO;
					BigDecimal amountShow = insuredAmount.add(histByPlanType);
					for (DT03DiagnosisSumInsuredPT item : dt03_planType) {
						if (item.getPlanType().contains(planType) && temp.getPlanHeader().getPlanSystem().equalsIgnoreCase("NORMAL")) {
							/* Check Age */
							boolean checkAge = RuleUtils.betweenNumber(Long.valueOf(age),
									Long.valueOf(item.getMinAge()), Long.valueOf(item.getMaxAge()));
							if (!checkAge) {
								// state = state+"\n"+RuleConstants.RULE_SPACE+"
								// #3.1 Add Message Code DT03201 and End process
								// ";
								Message message = messageService.getMessage(registersystem,
										RuleConstants.MESSAGE_CODE.DT03201, Arrays.asList(""), Arrays.asList(""));
								messageList.add(message);
								break;
							} else {
								/* Check LIMIT_LIFE_SUM_INSURED */



								if (!( item.getMinSuminsured() == null
										|| RuleConstants.OTHERWISE.equals(item.getMinSuminsured())
										|| item.getMaxSuminsured() == null
										|| RuleConstants.OTHERWISE.equals(item.getMaxSuminsured()))) {
									

									BigDecimal min = new BigDecimal(item.getMinSuminsured());
									BigDecimal max = new BigDecimal(item.getMaxSuminsured());
									checkSum = RuleUtils.betweenNumber( amountShow , min, max);

								}
								
								if (!(item.getLimitWithLifeSuminsured() == null
										|| RuleConstants.OTHERWISE.equals(item.getLimitWithLifeSuminsured())
										)) {
									limitLifeInsured = new BigDecimal(item.getLimitWithLifeSuminsured());
									limitLifeInsured = limitLifeInsured.multiply(basicInsured);
									checkLimit = amountShow.compareTo(limitLifeInsured) > 0;

									
								}
								
								if ((RuleConstants.OTHERWISE.equals(item.getMinSuminsured())
										&& RuleConstants.OTHERWISE.equals(item.getMaxSuminsured()))) {
									
									if(!checkSum){
										state = state + "\n" + RuleConstants.RULE_SPACE + "	#3.1 TRUE --> Go to #3.2 ";
										state = state + "\n" + RuleConstants.RULE_SPACE+ "	#3.2 Get and Set Message Code : "+ item.getMessageCode();

										List<String> param = new ArrayList<String>();
										param.add(amountShow.toString());
										Message message = messageService.getMessage(registersystem,item.getMessageCode(), param, param);
										messageList.add(message);
										continue;
									}else{
										state = state + "\n" + RuleConstants.RULE_SPACE + "#4 Check Diagnosis By Plan Group";
										List<DT03DiagnosisSumInsuredPG> dt03_planGroup = checkDiagnosis.checkDiagnosisPlanGroup(planType, age);
										state = state + "\n" + RuleConstants.RULE_SPACE + "	#4 Result size == 0 ?:"+ dt03_planGroup.size();
										if (dt03_planGroup == null || dt03_planGroup.size() == 0) {
											state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.1 Add Message Code DT03101 and End process ";
											Message message = messageService.getMessage(registersystem,RuleConstants.MESSAGE_CODE.DT03201, Arrays.asList(""),Arrays.asList(""));
											messageList.add(message);
										} else {
											
											BigDecimal maxSumInsured = BigDecimal.ZERO;
											
											BigDecimal sumPlanGroup = BigDecimal.ZERO;
											for (DT03DiagnosisSumInsuredPG diagnosis : dt03_planGroup) {

												allSumInsuredHistByPlanType = BigDecimal.ZERO;
												state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.1 Summary SumInsured By PlanGroup : " + planType;
												allSumInsuredHistByPlanType = sumInsureService.getSumHistoryInsureByPlanGroup(historyInsureList, planType);
												state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.2 AllSumInsuredHistByPlanGroup : "+ allSumInsuredHistByPlanType;
												state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.3 Check AllSumInsuredHistByPlanGroup > MAX_SUMINSURED ?";
												
												
												for (InsureDetail sumData : diagnosisPlanList) {
													if(diagnosis.getPlanGroup().contains(sumData.getPlanHeader().getPlanType())){
														BigDecimal histByPlanTypeSum = sumInsureService.getSumHistoryInsureByPlanType(historyInsureList,sumData.getPlanHeader().getPlanType());
														sumPlanGroup = sumPlanGroup.add(new BigDecimal(sumData.getInsuredAmount()));
														sumPlanGroup = sumPlanGroup.add(histByPlanTypeSum);
														
													}
												}
									
												

												if (!(diagnosis.getMinSuminsured() == null
														|| RuleConstants.OTHERWISE.equals(diagnosis.getMinSuminsured())
														|| diagnosis.getMaxSuminsured() == null
														|| RuleConstants.OTHERWISE.equals(diagnosis.getMaxSuminsured()))) {

													maxSumInsured = new BigDecimal(diagnosis.getMaxSuminsured());
													checkMaxInsured = sumPlanGroup.compareTo(maxSumInsured) > 0;

												}

												if ((RuleConstants.OTHERWISE.equals(diagnosis.getMinSuminsured())
														&& RuleConstants.OTHERWISE.equals(diagnosis.getMaxSuminsured()))) {
														mesCode = diagnosis.getMessageCode();
												
												}
												state = state + "\n" + RuleConstants.RULE_SPACE + "	End loop";
											}
										}
									}
								} 

								if (RuleConstants.OTHERWISE.equals(item.getLimitWithLifeSuminsured())) {
									if (checkLimit) {
										state = state + "\n" + RuleConstants.RULE_SPACE + "	#3.1 TRUE --> Go to #3.2 ";
										state = state + "\n" + RuleConstants.RULE_SPACE+ "	#3.2 Get and Set Message Code : "+ item.getMessageCode();

										List<String> param = new ArrayList<String>();
										param.add(amountShow.toString());
										param.add(basicInsured.toString());
										Message message = messageService.getMessage(registersystem,item.getMessageCode(), param, param);

										messageList.add(message);
										continue;
									}
								}
								
							}

						}
						state = state + "\n" + RuleConstants.RULE_SPACE + "	End loop";
					}
				}

				state = state + "\n" + RuleConstants.RULE_SPACE + "End loop Step #3 && #4";
			}
			
			
			
			if (checkMaxInsured) {
				if (StringUtils.isNoneBlank(mesCode)) {
					state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.3 TRUE --> Go to #4.4 ";
					state = state + "\n" + RuleConstants.RULE_SPACE+ "	#4.4 Get and Set Message Code : "+ mesCode;
					Message message = messageService.getMessage(registersystem,mesCode, Arrays.asList(""),Arrays.asList(""));
					messageList.add(message);

				}
			}
//			System.out.println("Test");
//			currentPlanList.clear();
//			currentPlanList.addAll(Temp);
//			for (InsureDetail insureDetail : currentPlanList) {
//				insureDetail Temp.add(insureDetail);
//			};

			input.getRequestBody().setBasicInsureDetail(TempBasic);
			input.getRequestBody().setRiderInsureDetails(TempRider);

		} else {
			state = state + "\n" + RuleConstants.RULE_SPACE + "	#1 Result : Not found Diagnosis Plan ---> End process";
		}
	}

	@Override
	public void finalAction() {
//		state = state + "\n" + "END Rule 03 ตรวจสอบทุนประกันประเภทโรคร้ายแรง";
//		state = state + "\n" + "]";
		setSubRuleLog(state);
	}

}
