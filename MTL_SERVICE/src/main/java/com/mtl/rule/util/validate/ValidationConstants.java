package com.mtl.rule.util.validate;

public class ValidationConstants {
	
	public  boolean agenCode =false;
	
	public  boolean basicinsure =false;
	public  boolean riderinsurelist =false;
	public  boolean beneficialry =false;
	
	// PersonData
	public  boolean personData_idCard =false;
	public  boolean personData_sex =false;
	public  boolean personData_age =false;
	public  boolean personData_birthday =false;
	public  boolean personData_weight =false;
	public  boolean personData_height =false;
	public  boolean personData_occupationCode =false;
	public  boolean personData_clientNumber =false;
	public  boolean personData_nationality =false;
	public  boolean personData_ageInMonth =false;
	public  boolean personData_firstName =false;
	public  boolean personData_lastName =false;
	public  boolean personData_maritalStatus =false;
	
	public  boolean personData_Add_provinceCode =false;
	public  boolean personData_Add_mobileNumber =false;
	
	
	
	// PBPersonData
	public  boolean pb_idCard =false;
	public  boolean pb_sex =false;
	public  boolean pb_age =false;
	public  boolean pb_birthday =false;
	public  boolean pb_weight =false;
	public  boolean pb_height =false;
	public  boolean pb_relationship =false;
	public  boolean pb_occupationCode =false;
	public  boolean pb_clientNumber =false;
	public  boolean pb_nationality =false;
	public  boolean pb_ageInMonth =false;
	public  boolean pb_firstName =false;
	public  boolean pb_lastName =false;
	public  boolean pb_maritalStatus =false;
	
	public  boolean pb_Add_provinceCode =false;
	public  boolean pb_Add_mobileNumber =false;
	
	// Beneficiary
	public  boolean beneficiary_idCard =false;
	public  boolean beneficiary_firstName =false;
	public  boolean beneficiary_lastName =false;
	public  boolean beneficiary_age =false;
	public  boolean beneficiary_percent =false;
	public  boolean beneficiary_relation =false;
  
	
	// BasicInsure
	public  boolean basic_planCode =false;
	public  boolean basic_insuredAmount =false;
	public  boolean basic_premiumAmount =false;
	public  boolean basic_topupPremiumAmount =false;
	public  boolean basic_premiumIndicator =false;
	public  boolean basic_premiumPayment =false;
	
 
	// RiderInsure
	public  boolean rider_planCode =false;
	public  boolean rider_insuredAmount =false;
	public  boolean rider_premiumAmount =false;
	public  boolean rider_topupPremiumAmount =false;
	public  boolean rider_premiumIndicator =false;
	public  boolean rider_premiumPayment =false;
	
	// ClientAdress
	public  boolean client_allAddress =false;
	public  boolean client_houseRegistration =false;
	public  boolean client_regionAddress =false;
 
}
