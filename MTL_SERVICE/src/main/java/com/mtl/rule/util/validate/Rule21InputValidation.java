package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule21InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
  
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		
 
		 if( request.getRequestBody()==null) 	{		 
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE21+" RequestBody  Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
		 }	else {
				 if (StringUtils.isBlank(request.getRequestBody().getClientAllAddress())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE21 + " RequestBody ClientAllAddress Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR);
						vlidation.client_allAddress = true;
					}
	
					if (StringUtils.isBlank(request.getRequestBody().getClientHouseRegistration())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE21 + " RequestBody ClientHouseRegistration Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR);
						vlidation.client_houseRegistration = true;
					}
					if (StringUtils.isBlank(request.getRequestBody().getClientRegionAddress())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE21 + " RequestBody ClientRegionAddress Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR);
						vlidation.client_regionAddress = true;
					}
		 }
 
		result.setErrorMessageList(errorMessageList);
		return result;
	}
	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}
	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
