package com.mtl.rule.util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.model.underwriting.Message;
import com.mtl.rule.model.MessageMapping;
import com.mtl.rule.repository.MessageMappingDao;

@Service
public class MessageService {

	@Autowired
	private MessageMappingDao messageMappingDao;

	public Message getMessage(String systemName, String messageCode, List<String> listTextTH, List<String> listTextEN) {
		// System.out.println(" ###### MessageService.getMessage by Code:"+messageCode);
		MessageMapping dataFind = messageMappingDao.findByMessageCode(systemName, messageCode);
		if (dataFind == null) {
			dataFind = messageMappingDao.findByMessageCode(RuleConstants.OTHERWISE, messageCode);
		}
		Message dataRes = new Message();
		dataRes.setCode(messageCode);

		Pattern p = Pattern.compile("\\$");
		String messageTH = dataFind.getMessageTH();
		String messageUS = dataFind.getMessageEN();

		if (listTextTH != null && listTextTH.size() > 0) {
			Matcher thMacher;
			for (String tmp : listTextTH) {
				thMacher = p.matcher(messageTH);
				if (thMacher.find()) {
					messageTH = thMacher.replaceFirst(tmp);
					thMacher = p.matcher(messageTH);

				}
			}
		}
		if(messageUS!=null){
		if (listTextEN != null && listTextEN.size() > 0) {
			Matcher enMacher;
			for (String tmp : listTextEN) {
				enMacher = p.matcher(messageUS);
				if (enMacher.find()) {
					messageUS = enMacher.replaceFirst(tmp);
					enMacher = p.matcher(messageUS);

				}
			}
		}
}
		dataRes.setStatus(dataFind.getMessageStatus());
		dataRes.setMessageTH(messageTH);
		dataRes.setMessageEN(messageUS);

		return dataRes;
	}

	public MessageMapping getMessageMapping(String systemName, String messageCode) {
		MessageMapping dataFind = messageMappingDao.findByMessageCode(systemName, messageCode);
		if (StringUtils.isNoneBlank(systemName)) {
			if (dataFind == null) {
				dataFind = messageMappingDao.findByMessageCode(RuleConstants.OTHERWISE, messageCode);
			}
		}
		return dataFind;
	}

	public Message getMessage(MessageMapping dataFind, String textTH, String textEN) {
		Message dataRes = new Message();
		dataRes.setCode(dataFind.getMessageCode());
		dataRes.setStatus(dataFind.getMessageStatus());
		dataRes.setMessageTH(dataFind.getMessageTH().replaceAll("\\$", textTH));
		dataRes.setMessageEN(dataFind.getMessageEN().replaceAll("\\$", textEN));

		return dataRes;
	}

	public Message getOneMessage(String systemName, String messageCode) {
		// System.out.println(" ###### MessageService.getOneMessage by
		// Code:"+messageCode);
		MessageMapping dataFind = new MessageMapping();
		dataFind = messageMappingDao.findByMessageCode(systemName, messageCode);
		if (StringUtils.isNoneBlank(systemName)) {
			if (dataFind == null) {
				dataFind = messageMappingDao.findByMessageCode(RuleConstants.OTHERWISE, messageCode);
			}
		}
		Message dataRes = new Message();
		dataRes.setCode(messageCode);
		dataRes.setStatus(dataFind.getMessageStatus());
		dataRes.setMessageTH(dataFind.getMessageTH());
		dataRes.setMessageEN(dataFind.getMessageEN());
		return dataRes;
	}
}
