package com.mtl.rule.util.logging.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mtl.rule.util.logging.ServiceLogging;

@Repository
public interface ServiceLoggingRepository extends CrudRepository<ServiceLogging, Long> {

}
