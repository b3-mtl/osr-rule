package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.ws.service.GetAgentInfoService;
import com.mtl.model.common.AgentInfo;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.ErrorMessage;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.RequestBody;
import com.mtl.model.underwriting.RequestHeader;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 
	//UnderwriteRequest
	//public Validation validate(String appType, String channel, String section) {
	public Validation validate(UnderwriteRequest request) {
		Validation result = new Validation();
		result.setStatus("PASS");
		// Do Validate

		return result;
	}
	
	public static Validation inputValidation(UnderwriteRequest request,Map<String, String> ruleSet) {		
		
		Set<String> ruleNameSet = new HashSet<String>();
		for (String key : ruleSet.keySet()) {			 
			//ruleSetStr = ruleSetStr+ruleSet.get(key)+"  ";
			String ruleStr = ruleSet.get(key);
			System.out.println("inputValidation:"+ruleStr);
			ruleNameSet.add(ruleStr);
		}	 
		ErrorMessage errorMesage = new ErrorMessage();
		List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		ValidationConstants cont = new ValidationConstants();
		
		if(ruleNameSet.contains("serviceRule03")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule03" );
			Validation validationRule03 = new Rule03InputValidation().inputValidation(request, cont);
			if(validationRule03.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule03.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		
		if(ruleNameSet.contains("serviceRule04")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule04" );
			Validation validationRule04 = new Rule04InputValidation().inputValidation(request, cont);
			if(validationRule04.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule04.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		
		if(ruleNameSet.contains("serviceRule05")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule05" );
			Validation validationRule05 = new Rule05InputValidation().inputValidation(request, cont);
			if(validationRule05.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule05.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		if(ruleNameSet.contains("serviceRule06")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule06" );
			Validation validationRule06 = new Rule06InputValidation().inputValidation(request, cont);
			if(validationRule06.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule06.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		if(ruleNameSet.contains("serviceRule07")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule07" );
			Validation validationRule07 = new Rule07InputValidation().inputValidation(request, cont);
			if(validationRule07.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule07.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		if(ruleNameSet.contains("serviceRule11")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule11" );
			Validation validationRule11 = new Rule11InputValidation().inputValidation(request, cont);
			if(validationRule11.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule11.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		if(ruleNameSet.contains("serviceRule12")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule12" );
			Validation validationRule12 = new Rule12InputValidation().inputValidation(request, cont);
			if(validationRule12.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule12.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
		}
		
		
		
		if(ruleNameSet.contains("serviceRule14")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule14" );
			Validation validationRule14 = new Rule14InputValidation().inputValidation(request, cont);
			if(validationRule14.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule14.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
			
		}
		
		if(ruleNameSet.contains("serviceRule16")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule16" );
			Validation validationRule16 = new Rule16InputValidation().inputValidation(request, cont);
			if(validationRule16.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule16.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
			
		}
		
		if(ruleNameSet.contains("serviceRule17")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule17" );
			Validation validationRule17 = new Rule17InputValidation().inputValidation(request, cont);
			if(validationRule17.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule17.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
			
		}
		
		if(ruleNameSet.contains("serviceRule21")) {
			System.out.println("inputValidation RuleSet Contain : serviceRule21" );
			Validation validationRule21 = new Rule21InputValidation().inputValidation(request, cont);
			if(validationRule21.getStatus().equals(ServiceConstants.VALIDATION_ERROR)) {
				errorMessageList.addAll(validationRule21.getErrorMessageList());
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}
			
		}
		
 
 
		errorMesage.setErrorList(errorMessageList);
		result.setErrorMessage(errorMesage);

		return result;
	}
	
 
	public static Validation validateHeader(UnderwriteRequest request,ApplicationCache applicationCache,GetAgentInfoService getAgentInfoService) throws Exception {		
		ErrorMessage errorMesage = new ErrorMessage();
		List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
		
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS);
		if(request==null) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc("Request Object Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR);
		}else {
			RequestHeader requestHeader =request.getRequestHeader(); 
			if(requestHeader==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc("Request Header Object Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}else {
				
				if(StringUtils.isEmpty(requestHeader.getRegisteredSystem())) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc("RegisteredSystem request cannot be empty");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR);
				}
 
				if(StringUtils.isEmpty(requestHeader.getTransactionId())) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc("transactionId request cannot be empty");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR);
				}
			}
			
			RequestBody requestBody =request.getRequestBody();
			if(requestBody==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc("Request Body Object Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
			}else {
				
				
				if(StringUtils.isNotBlank(request.getRequestBody().getAgentCode())) { 
					AgentInfo agentInfo = getAgentInfoService.getAgentDetailByAgentCode(request.getRequestBody().getAgentCode(),"");
					System.out.println(" ######### Check Agent Exist :"+agentInfo);
					if(agentInfo==null||StringUtils.isEmpty(agentInfo.getAgentCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc("Agent Code "+request.getRequestBody().getAgentCode()+" Not Found");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR);
					}
					
				}
				
				
				// PlanHeader 
				if(request.getRequestBody().getBasicInsureDetail()!=null&&StringUtils.isNotBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
					PlanHeader baiscPlanHeader =applicationCache.getPlanHeaderAppCashMap().get(request.getRequestBody().getBasicInsureDetail().getPlanCode());
					if(baiscPlanHeader==null) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc("Basic Plan Code "+request.getRequestBody().getBasicInsureDetail().getPlanCode()+" Not Found");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR);
						
					}
		 		}
				
		  		 List<InsureDetail> riderInsureList =request.getRequestBody().getRiderInsureDetails();
				 if(riderInsureList!=null) {
					 for(InsureDetail insureTmp:riderInsureList) {  
						 if(StringUtils.isNoneBlank(insureTmp.getPlanCode()))
						 {
							PlanHeader riderPlanHeader =applicationCache.getPlanHeaderAppCashMap().get(insureTmp.getPlanCode()); 
							if(riderPlanHeader==null) {
								ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
								requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
								requestObjValidation.setErrorDesc("Rider Plan Code "+insureTmp.getPlanCode()+" Not Found");
								errorMessageList.add(requestObjValidation);
								result.setStatus(ServiceConstants.VALIDATION_ERROR);
								
							}
						 }
		 			 }			 
				 }
			}
		}
		
		errorMesage.setErrorList(errorMessageList);
		result.setErrorMessage(errorMesage);

		return result;
	}

}
