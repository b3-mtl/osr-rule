package com.mtl.rule.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.mtl.model.common.HistoryInsure;
import com.mtl.model.underwriting.InsureDetail;

public class RuleUtils {
	
	public static boolean checkType(String planType, List<InsureDetail> insureDetails){
		if(!planType.trim().isEmpty() && insureDetails != null){
			for(InsureDetail detail : insureDetails){
				if(detail.getPlanHeader().getPlanType() != null && isOneOf(planType,detail.getPlanHeader().getPlanType()))
					return true;
			}
		}
		return false;
	}
	
	public static boolean checkStatusHistory(String status, List<HistoryInsure> historyInsure){
		if(!status.trim().isEmpty() && historyInsure != null){
			for(HistoryInsure detail : historyInsure){
				if(detail.getStatus() != null && isOneOf(status,detail.getStatus()))
					return true;
			}
		}
		return false;
	}
	
	public static boolean isOneOf(String source,String target){
		if(source!=null && (!source.isEmpty())){
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				return true;
			}
		}
		return false;
	}

	public static boolean andString(String prameter , String column) {
		if(StringUtils.isNotBlank(column)) {
			Pattern pattern = Pattern.compile("(?<=,|^)"+prameter+"(?=,|$)");
			Matcher matcher = pattern.matcher(column);//The text you want to search in
			if(matcher.find()) {
				return true ; 
			}else {
				return false ;
			}
		}
		return true;
	}
	
	public static boolean andNumber(BigDecimal prameter ,  BigDecimal column) {
		if(prameter != null) {
			if(column !=  null && column != null) {
				if(prameter.compareTo(column) == 0 ) {
					return true ; 
				}else {
					return false ;
				}
			}else {
				return true;
			}
		}
		return true;
	}
	public static boolean andMorethanNumber(BigDecimal prameter ,  BigDecimal column) {
		if(prameter != null) {
			if(column !=  null) {
				if(prameter.compareTo(column) > 0 ) {
					return true ; 
				}else {
					return false ;
				}
			}else {
				return true;
			}
		}
		return true;
	}
	public static boolean andLessthanNumber(BigDecimal prameter ,  BigDecimal column) {
		if(prameter != null) {
			if(column !=  null) {
				if(prameter.compareTo(column) < 0 ) {
					return true ; 
				}else {
					return false ;
				}
			}else {
				return true;
			}
		}
		return true;
	}
	public static boolean betweenNumber(BigDecimal prameter ,  BigDecimal columnStart, BigDecimal columnEnd ) {
		if(prameter != null) {
			if(columnStart !=  null && columnEnd != null) {
				if(prameter.compareTo(columnStart) >= 0 
						&& prameter.compareTo(columnEnd) <= 0) {
					return true; 
				}else {
					return false;
				}
			}else {
				return true;
			}
		}
		return true;
	}
	
	public static boolean betweenNumber(Long prameter ,  Long columnStart, Long columnEnd ) {
		if( prameter != null ) {
			if(columnStart <= prameter && prameter <= columnEnd) {
				return true ; 
			}else {
				return false ;
			}
		}
		return true;
	}
	
	public static boolean betweenNumberYear(Long prameter ,  Long columnStart, Long columnEnd ) {
		if( prameter != null ) {
			if(columnStart <= prameter && prameter < columnEnd) {
				return true ; 
			}else {
				return false ;
			}
		}
		return true;
	}
	
	
	//News Function 

	public static BigDecimal getSuminsuredHistoryInsureDetail(String planType, String channelCode, String planSystem, String planGroupType , List<InsureDetail> currentHitPlanList){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(getHistoryInsureDetails(currentHitPlanList))){
			for(InsureDetail insureDetail:getHistoryInsureDetails(currentHitPlanList)){
				if((insureDetail.getPlanHeader().getPlanType() != null && isOneOf(planType, insureDetail.getPlanHeader().getPlanType()))
					&& (insureDetail.getChannelCode() != null && isOneOf(channelCode, insureDetail.getChannelCode()))
					&& (insureDetail.getPlanHeader().getPlanSystem() != null && isOneOf(planSystem, insureDetail.getPlanHeader().getPlanSystem()))
					&& (insureDetail.getPlanHeader().getPlanGroupType() != null && isOneOf(planGroupType, insureDetail.getPlanHeader().getPlanGroupType())))
					sumInsured = sumInsured.add(new BigDecimal( insureDetail.getInsuredAmount()));
			}
		}
		return sumInsured;
	}
	
	
	public static BigDecimal getSumInsuredByPlanCode(String planCode,List<InsureDetail> insureDetails){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(insureDetails != null && insureDetails.size() > 0){
			for(InsureDetail insureDetail:insureDetails){
				if(insureDetail.getPlanCode() != null && isOneOf(planCode, insureDetail.getPlanCode())){
					sumInsured = sumInsured.add(new BigDecimal (insureDetail.getInsuredAmount()));
				}
			}
		}
		return sumInsured;
	}
	

	public static int countPlanCodeInCoverageList(String planCode, List<InsureDetail> insureDetails){
		int count = 0;
		if(planCode!=null && (!planCode.trim().isEmpty())){
			planCode = ","+planCode+",";
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){					
					if(isOneOf(planCode,insureDetail.getPlanCode())){
						count++;
					}
				}
			}
		}
		return count;
	}
	
	public static BigDecimal maxSumInsurePlanCodeInCoverageList(String planCode, List<InsureDetail> insureDetails){
		BigDecimal max = null;
		if(planCode!=null && (!planCode.trim().isEmpty())){
			planCode = ","+planCode+",";
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){					
					if(isOneOf(planCode,insureDetail.getPlanCode())){
						if(max == null)max = new BigDecimal(insureDetail.getInsuredAmount());
						else if(max.compareTo(new BigDecimal( insureDetail.getInsuredAmount() )) > 0)
							max = new BigDecimal( insureDetail.getInsuredAmount() );
					}
				}
			}
		}
		return max;
	}
	
	public static  boolean checkPlanTypeInCurrentRequest(String planType, String planSystem , List<InsureDetail> insureDetails){
		if(!planType.trim().isEmpty() && insureDetails != null && insureDetails.size()>0){
			for(InsureDetail detail : insureDetails){
				if(detail.getPlanHeader().getPlanType() != null && isOneOf(planType,detail.getPlanHeader().getPlanType())){
					if(!planSystem.trim().isEmpty()){
						if(detail.getPlanHeader().getPlanSystem() != null && isOneOf(planSystem,detail.getPlanHeader().getPlanSystem()))
							return true;
					}else return true;
				}
			}
		}
		return false;
	}
	
	public static boolean oneInOf(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean oneInOfNotHave(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	public static InsureDetail oneInOfReturnPlan(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanCode();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return insureDetail;
				}
			}
		}
		return null;
	}
	
	public static boolean startWithCheck(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			
			for(InsureDetail insureDetail:insuredList){
				if(insureDetail.getPlanCode().startsWith(source)){
					return true;
				}
			}
			
		}
		return false;
	}
	
	public static boolean oneInOfType(String source, List<InsureDetail> insuredList){
		if(source!=null && (!source.isEmpty()) && insuredList!=null && insuredList.size()>0){
			source = ","+source+",";
			for(InsureDetail insureDetail:insuredList){
				String target = insureDetail.getPlanHeader().getPlanType();
				target = ","+target+",";
				int i = source.indexOf(target);
				if(i!=-1){
					return true;
				}
			}
		}
		return false;
	}
	
	
	public static BigDecimal getSumInsuredByPlanType(String planType, List<InsureDetail> insureDetails, String planSystem) {
		BigDecimal sumInsured = BigDecimal.ZERO;
	
		if (insureDetails != null && insureDetails.size() > 0) {
			for (InsureDetail insure : insureDetails) {
				if (insure.getPlanHeader().getPlanType() != null
						&& isOneOf(planType, insure.getPlanHeader().getPlanType())) {
					if (planSystem != null && !planSystem.trim().isEmpty()) {
						if (insure.getPlanHeader().getPlanSystem() != null
								&& isOneOf(planSystem, insure.getPlanHeader().getPlanSystem())) {
							sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
						}
					} else {
						sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
					}
				}
			}
		}
		
//		System.out.println("sumInsured : " +sumInsured);
		return sumInsured;
	}
	
	public static BigDecimal getSuminsuredHistoryByChannel(String planCode, String channelCode,List<InsureDetail> currentHitPlanList){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(getHistoryInsureDetails(currentHitPlanList))){
			for(InsureDetail insureDetail:getHistoryInsureDetails(currentHitPlanList)){
				if(insureDetail.getPlanCode() != null && isOneOf(planCode, insureDetail.getPlanCode())
						&& insureDetail.getChannelCode() != null && isOneOf(channelCode, insureDetail.getChannelCode()))
					sumInsured = sumInsured.add(new BigDecimal( insureDetail.getInsuredAmount()));
			}
		}
		return sumInsured;
	}
	
	public static List<InsureDetail> getHistoryInsureDetails(List<InsureDetail> currentHitPlanList) {
		List<InsureDetail> temp = new ArrayList<InsureDetail>();

			
		for(InsureDetail detail:currentHitPlanList){
			if(ModelUtils.isOneOf(RuleConstants.POLICY_STATUS, detail.getPolicyStatus()))
			{
				temp.add(detail);
			};
		}
		
		return temp;
	}
	
	
	public static boolean checkSuminsuredType(String codeList,BigDecimal lowerAge,BigDecimal higherAge ,BigDecimal age,List<InsureDetail> currentHitPlanList){
		if(getHistoryInsureDetails(currentHitPlanList) != null){
			for(InsureDetail insure:getHistoryInsureDetails(currentHitPlanList)){
				if(insure.getPlanHeader().getPlanType() != null && isOneOf(codeList, insure.getPlanHeader().getPlanType())){
					if(age.compareTo(lowerAge)>=0 && age.compareTo(higherAge)<=0)
						return true;
				}
			}
		}
		return false;
	}
	
	public static BigDecimal getSumInsuredByPlanCodeAndServiceBranch(String planCode,List<InsureDetail> insureDetails,String serviceBranch){
		BigDecimal sumInsured = BigDecimal.ZERO;
		if(ModelUtils.checkNullList(insureDetails)){
			for(InsureDetail insure:insureDetails){
				if(insure.getPlanCode() != null && isOneOf(planCode, insure.getPlanCode())
					&& insure.getServiceBranch() != null && isOneOf(serviceBranch, insure.getServiceBranch())){
						sumInsured = sumInsured.add(new BigDecimal(insure.getInsuredAmount()));
				}
			}
		}
		return sumInsured;
	}
	
	public static boolean isOneOfOrBlank(String source,String target){
		boolean result=false;
		if(source==null||source.length()==0||"".equals(source)){
			result = true;
		}else{
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				result=true;
			}
		}		
		return result;
	}
	
	public static BigDecimal rounding(BigDecimal a, int digit){
		return a.setScale(digit, RoundingMode.HALF_UP);
	}
	
	public static  List<String>  mapMessageCodeDesc(String descCode){
		
		
		List<String> param = new ArrayList<String>();
		String[] descArr = descCode.split("_split_");
		for (String result : descArr) {
			param.add(result);
		}
		return param;
	}
	
	public static boolean checkTypeInsureDetail(String planType, List<InsureDetail> details){
		if(ModelUtils.isBlank(planType) && ModelUtils.checkNullList(details)){
			for(InsureDetail detail : details){
				if(detail.getPlanHeader().getPlanType() != null){
					
					if(detail.getPlanHeader().getPlanType() != null && isOneOf(planType,detail.getPlanHeader().getPlanType()))
					{	return true; }
				
				}
			}
		}
		return false;
	}
	
	
	public static int countHistoryPackagePlan(String packageTemplate,List<InsureDetail>  historyInsureDetailslist) {
		int countPackage = 0;

		if (packageTemplate != null && !packageTemplate.isEmpty()) {
			// Find Policy Number List
			List<String> policyNumberList = new ArrayList<String>();
			boolean checkDuplicate = false;
			
			for (InsureDetail insure : historyInsureDetailslist) {
				checkDuplicate = false;
				for (String policyNumber : policyNumberList) {
					if (insure.getPolicyNumber() != null && insure.getPolicyNumber().equalsIgnoreCase(policyNumber)) {
						checkDuplicate = true;
						break;
					}
				}

				if (!checkDuplicate) {
					if(insure.getPolicyNumber()!= null){
						System.out.println("POLICY_NUMBER :: x" + insure.getPolicyNumber() + "x");
						policyNumberList.add(insure.getPolicyNumber());
					}
					
				}
			}

			// Split Package
			boolean checkFoundPlanCode = false;
			boolean checkCompletedPackage = false;
			
			String[] packagePlanList = packageTemplate.trim().split("-");
			
//			TON10D,RBH310
//			TON10H,RBH310
			
			if (packagePlanList != null && packagePlanList.length > 0) {
				for (int packageRunning = 0; packageRunning < packagePlanList.length; packageRunning++) {
					// System.out.println("PACKAGE :: "+packageRunning+" :: "+packagePlanList[packageRunning]);
					
					// Split Plan Code in Package
					String[] planCodeList = packagePlanList[packageRunning].trim().split(",");
					
					if (planCodeList != null && planCodeList.length > 0) {
						// Loop each Policy
						for (String policyNumber : policyNumberList) {
							// System.out.println("POLICY_NUMBER LOOP :: x"+policyNumber+"x");
							
							// Loop each Plan Code in Package
							checkCompletedPackage = true;
							for (int planCodeRunning = 0; planCodeRunning < planCodeList.length; planCodeRunning++) {
								// System.out.println("PLAN CODE in PACKAGE :: "+planCodeRunning+" :: "+planCodeList[planCodeRunning]);
								
								checkFoundPlanCode = false;
								for (InsureDetail insure : historyInsureDetailslist) {
									insure.setPlanCode(insure.getPlanCode().trim());
									if(insure.getPolicyNumber()!=null){
										if (insure.getPolicyNumber().equalsIgnoreCase(policyNumber)) {
											// System.out.println("PLAN CODE in HISTORY :: "+insure.getPlanCode());
											
											if (insure.getPlanCode().equalsIgnoreCase(planCodeList[planCodeRunning])) {
												checkFoundPlanCode = true;
												break;
											}
										}
									}
									
								}

								if (!checkFoundPlanCode) {
									checkCompletedPackage = false;
									break;
								}
							}

							if (checkCompletedPackage) {
								countPackage++;
								System.out.println("COUNT PACKAGE :: " + countPackage);
							}
						}
					}
				}
			}
		}
		return countPackage;
	}
	
	public static boolean checkDuplicatePlanCodeInPlackage( List<InsureDetail> insureDetails){
		boolean count = false;
		Set<String> planCode = new HashSet<String>();
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){	
					System.out.println("checkDuplicatePlanCodeInPlackage : "+insureDetail.getPlanCode());
					planCode.add(insureDetail.getPlanCode());
				}
				if(planCode.size()!=insureDetails.size()){
					count=true;
				}
			}
		System.out.println("Count insureDetails : "+insureDetails.size()+" planCode.size "+planCode.size()+" : "+planCode);
		return count;
	}
	
	public static int countPackageProjectInList(String projectType, List<InsureDetail> insureDetails){
		int count = 0;
		if(projectType!=null && (!projectType.trim().isEmpty())){
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){					
					if(isOneOf(projectType,insureDetail.getProjectType())){
						count++;
					}
				}
			}
		}
		return count;
	}
	

	public static int countPlanCodeforClient(String planCode,String channelCode , List<InsureDetail>  historyInsureDetailslist){
		int count = 0;
		if(ModelUtils.checkNullList(historyInsureDetailslist)){
			for(InsureDetail history:historyInsureDetailslist){
				if(history.getPlanCode().trim().equals(planCode)){
					if(!ModelUtils.isBlank(channelCode) || (channelCode.equals(history.getChannelCode())))
						count++;
				}
			}
		}
		return count;
	}
	
	public static int countPlanTypeforPolicy(String planType, String planSystem, List<InsureDetail>  insureDetails){
		int count = 0;
		if(planType!=null && (!planType.trim().isEmpty())){
			if(insureDetails != null && insureDetails.size() > 0){
				for(InsureDetail insureDetail : insureDetails){
					if(insureDetail.getPlanHeader().getPlanType() != null && isOneOf(planType,insureDetail.getPlanHeader().getPlanType())){
						if(!planSystem.trim().isEmpty()){
							if(isOneOf(planSystem,insureDetail.getPlanHeader().getPlanSystem())){
								count++;
							}
						}
						else{
							count++;
						}
					}
				}
			}
		}
		return count;
	}
	
	
	public static int countPlanCodeBeforeExpirationDay( String planCode,List<InsureDetail> insureDetails,Integer YearDuration, Integer DayDuration, Date requestDate ,List<InsureDetail> historyinsureDetails){
		int count = 0;
		if(ModelUtils.checkNullList(historyinsureDetails)){
			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.setTime(requestDate);
			currentCalendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR));
			Date currentYear = currentCalendar.getTime();
//			  24 * 60 * 60 * 1000
//			requestDate = new Date(requestDate.getTime()+(DayDuration.intValue()* 24 * 60 * 60 * 1000));
//			System.out.println("New date : "+requestDate);
			
			Calendar previousCalendar = Calendar.getInstance();
			previousCalendar.setTime(requestDate);
			previousCalendar.add(Calendar.DATE,  DayDuration.intValue());
			previousCalendar.add(Calendar.YEAR,  YearDuration.intValue());
//			previousCalendar.set(Calendar.YEAR, previousCalendar.get(Calendar.YEAR) - YearDuration.intValue());
			Date pastYear = previousCalendar.getTime();
			
			
			System.out.println("==== CURRENT DATE :: "+currentCalendar.getTime());
			System.out.println("==== PAST DATE -"+YearDuration.intValue()+" YEAR + "+DayDuration.intValue()+" Day  :: "+previousCalendar.getTime());
			
			
			
			if(planCode!=null && (!planCode.trim().isEmpty())){
				planCode = ","+planCode+",";
				if(insureDetails != null && insureDetails.size() > 0){
					for(InsureDetail insureDetail : insureDetails){		
						Calendar issueCalendar = Calendar.getInstance();
						issueCalendar.setTime(insureDetail.getIssueDate());
						issueCalendar.set(Calendar.YEAR, issueCalendar.get(Calendar.YEAR));
//						issueCalendar.set(Calendar.DATE, issueCalendar.get(Calendar.DATE) + DayDuration.intValue());
						
						if(isOneOf(planCode,insureDetail.getPlanCode())){
//							System.out.println("==== Plan Code : "+insureDetail.getPlanCode()+"  date + "+DayDuration.intValue()+" day :: "+issueCalendar.getTime());
							if((CalendarHelper.dateEquals(currentCalendar, issueCalendar) || insureDetail.getIssueDate().before(currentYear))
									&& (CalendarHelper.dateEquals(previousCalendar, issueCalendar) || insureDetail.getIssueDate().after(pastYear))){
								count++;
							}
							
						}
					}
				}
			}
			
		}
		System.out.println("Plan Code : "+planCode+" Count = "+count);
		return count;
	}

	
}
