
package com.mtl.rule.util.validate;

import java.util.ArrayList;
import java.util.List;

import com.mtl.model.underwriting.ErrorMessage;
import com.mtl.model.underwriting.ErrorMessageDesc;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Validation {

	private String status;
	private String count;
	private ErrorMessage errorMessage;
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	

}
