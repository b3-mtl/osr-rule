package com.mtl.rule.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.appcache.ApplicationCache;
import com.mtl.collector.service.rule06.GetMapNmlTypeService;
import com.mtl.model.common.HistoryInsure;
import com.mtl.model.common.PlanHeader;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.rule.model.MsInvesmentPlanType;
import com.mtl.rule.model.SumInsure;


@Service
public class SumInsureService {
	
	@Autowired
	private GetMapNmlTypeService getMapNmlTypeService;
	
	@Autowired
	private ApplicationCache applicationCache;

	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By Group
	 * Overload Method use in rule 13
	 * 
	 * @return sumInsured
	 */
	public BigDecimal getSumInsurePlanTypeCI(InsureDetail basicCurrent,List<InsureDetail> riderCurrent , List<HistoryInsure> historyInsureList) {
		
		// แยก PlanCode เป็น BASIC และ RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {
			for(HistoryInsure temp:historyInsureList) {
				String groupType = temp.getPlanGroupType();
				if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
					InsureDetail obj = new InsureDetail();
					obj.setPlanCode(temp.getPlanCode());
					obj.setInsuredAmount(temp.getSumInsure().toString());
					basicList.add(obj);
				}else {
					InsureDetail obj = new InsureDetail();
					obj.setPlanCode(temp.getPlanCode());
					obj.setInsuredAmount(temp.getSumInsure().toString());
					
					PlanHeader hdr = new PlanHeader();
					hdr.setPlanType(temp.getPlanType());
					obj.setPlanHeader(hdr);
					riderList.add(obj);
				}
			}
		}
		
		//sumInsure ของ BASIC และ RIDER
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsured = BigDecimal.ZERO;
		BigDecimal sumInsured = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
			sumBasicInsured = sumBasicInsured.add(insured);
		}
		for(InsureDetail tempRider:riderList) {
			
			BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
			String planType = tempRider.getPlanHeader().getPlanType();
			if(RuleConstants.RULE_06.CI.equals(planType)) {
				sumRiderInsured = sumRiderInsured.add(insured);
			}
		}
		
		if(sumBasicInsured.compareTo(sumRiderInsured) >= 0) {
			sumInsured = sumBasicInsured;
		}else {
			sumInsured = sumRiderInsured;
		}
		
		return sumInsured;
	}
	
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By Group
	 * Overload Method use in rule 13
	 * 
	 * @return sumInsured
	 */
	public SumInsure getSumInsureByCIRule13(InsureDetail basicCurrent ,List<InsureDetail> riderCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC and RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {
			
			//calculate year before now 1 year
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(new Date());
			calDate.add(Calendar.YEAR, -1);
			Date oneYearBefore = calDate.getTime();
			System.out.println("nowDate :"+new Date());
			System.out.println("oneYearBefore :"+oneYearBefore);
			
			for(HistoryInsure temp:historyInsureList) {
				Date hisDate = temp.getIssueDate();
				System.out.println("hisDate :"+hisDate);
				if(oneYearBefore.before(hisDate)) {
					String groupType = temp.getPlanGroupType();
					Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
					
					if(set.contains(temp.getStatus())) {
						if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
							PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
							
							InsureDetail obj = new InsureDetail();
							obj.setPlanCode(temp.getPlanCode());
							if(RuleConstants.RULE_06.F.equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
							obj.setPlanHeader(hdr);
							basicList.add(obj);
						}else {
							PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
							
							InsureDetail obj = new InsureDetail();
							obj.setPlanCode(temp.getPlanCode());
							if(RuleConstants.RULE_06.F.equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
							obj.setPlanHeader(hdr);
							riderList.add(obj);
						}
					}
				}
			}
		}
		
		// BASIC and RIDER sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredCI = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			PlanHeader planHeader = tempBasic.getPlanHeader();
			
			if(planHeader != null ) {
				BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
				BigDecimal ratio = BigDecimal.ONE;
				
				if (planHeader.getUwRatio() != null) {
					ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
				}
				insured = ratio.multiply(insured);
				sumBasicInsured = sumBasicInsured.add(insured);
				
			}
		}
	
		for(InsureDetail tempRider:riderList) {
			PlanHeader planHeader = tempRider.getPlanHeader();
			
			if(planHeader != null ) {
				String planType = planHeader.getPlanType();
				BigDecimal ratio = BigDecimal.ONE;
				BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
				
				if(RuleConstants.RULE_06.CI.equals(planType)){
					
					if (planHeader.getUwRatio() != null) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
					insured = ratio.multiply(insured);
					sumRiderInsuredCI = sumRiderInsuredCI.add(insured);
					
				}
			}
		}
		
		SumInsure res = new SumInsure();
		if(sumBasicInsured.compareTo(sumRiderInsuredCI) >= 0) {
			res.setSumInsure(sumBasicInsured);
			res.setSumType(RuleConstants.PLAN_TYPE.BASIC);
		}else {
			res.setSumInsure(sumRiderInsuredCI);
			res.setSumType(RuleConstants.PLAN_TYPE.RIDER);
		}
		
		return res;
	}
	
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By Group
	 * Overload Method use in rule 6
	 * 
	 * @return sumInsured
	 */
	public SumInsure getSumInsurePlanTypeCI(String docType ,InsureDetail basicCurrent ,List<InsureDetail> riderCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC and RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {
			
			//calculate year before now 1 year
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(new Date());
			calDate.add(Calendar.YEAR, -1);
			Date oneYearBefore = calDate.getTime();
			System.out.println("nowDate :"+new Date());
			System.out.println("oneYearBefore :"+oneYearBefore);
			
			for(HistoryInsure temp:historyInsureList) {
				Date hisDate = temp.getIssueDate();
				System.out.println("hisDate :"+hisDate);
				if(oneYearBefore.before(hisDate)) {
					String groupType = temp.getPlanGroupType();
					Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
					
					if(set.contains(temp.getStatus())) {
						if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
							PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
							
							InsureDetail obj = new InsureDetail();
							obj.setPlanCode(temp.getPlanCode());
							obj.setInsuredAmount(temp.getFaceAmount().toString());
//							if(RuleConstants.RULE_06.F.equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null) {
//								obj.setInsuredAmount(temp.getFaceAmount().toString());
//							}else {
//								obj.setInsuredAmount(temp.getSumInsure().toString());
//							}
							obj.setPlanHeader(hdr);
							basicList.add(obj);
						}else {
							PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
							
							InsureDetail obj = new InsureDetail();
							obj.setPlanCode(temp.getPlanCode());
							obj.setInsuredAmount(temp.getFaceAmount().toString());
//							if(RuleConstants.RULE_06.F.equals(hdr.getUwRatingKey()) || hdr.getUwRatingKey() == null) {
//								obj.setInsuredAmount(temp.getFaceAmount().toString());
//							}else {
//								obj.setInsuredAmount(temp.getSumInsure().toString());
//							}
							obj.setPlanHeader(hdr);
							riderList.add(obj);
						}
					}
				}
			}
		}
		
		// BASIC and RIDER sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredCI = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredOther = BigDecimal.ZERO;
		BigDecimal sumInsured = BigDecimal.ZERO;
		List<String> tempUN =applicationCache.getMsParameter().get("percentUN1");
		List<String> tempUL = applicationCache.getMsParameter().get("percentUL1");
		BigDecimal percentUN = BigDecimal.ZERO;
		BigDecimal percentUL = BigDecimal.ZERO;
		if(tempUN !=null && tempUL!=null)
		{
			 percentUN = new BigDecimal(tempUN.get(0));
			 percentUL = new BigDecimal(tempUL.get(0));
		}
		
		for(InsureDetail tempBasic:basicList) {
			PlanHeader planHeader = tempBasic.getPlanHeader();
			
			if(planHeader != null ) {
				BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
				BigDecimal ratio = BigDecimal.ONE;
					
				/**
				 * Step 7 From Document SDS Topic 3.4.4 Check NML Type (CP or NCP)
				 */
				if(RuleConstants.RULE_06.NCP.equals(docType)) {
					String tempDocType = getMapNmlTypeService.findDocType(planHeader.getNcType(), "");
						
					if(RuleConstants.RULE_06.NCP.equals(tempDocType)) {
						
						if (planHeader.getUwRatio() != null) {
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}
						
						MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempBasic.getPlanCode());
						if(planTypeInvestMent != null) {
							BigDecimal sumInsuredILP = getSumConceptILP(tempBasic,percentUN,percentUL);
							sumInsuredILP = ratio.multiply(sumInsuredILP);
							sumBasicInsured = sumBasicInsured.add(sumInsuredILP);
						}else{
							
							insured = ratio.multiply(insured);
							sumBasicInsured = sumBasicInsured.add(insured);
						}

					}
				}else {
					if (planHeader.getUwRatio() != null) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
					
					MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempBasic.getPlanCode());
					if(planTypeInvestMent != null) {
						BigDecimal sumInsuredILP = getSumConceptILP(tempBasic,percentUN,percentUL);
						sumInsuredILP = ratio.multiply(sumInsuredILP);
						sumBasicInsured = sumBasicInsured.add(sumInsuredILP);
					}else{
						insured = ratio.multiply(insured);
						sumBasicInsured = sumBasicInsured.add(insured);
					}
				}
			}
		}
		for(InsureDetail tempRider:riderList) {
			PlanHeader planHeader = tempRider.getPlanHeader();
			
			if(planHeader != null ) {
				String planType = planHeader.getPlanType();
				BigDecimal ratio = BigDecimal.ONE;
				
				if(RuleConstants.RULE_06.CI.equals(planType) || 
					RuleConstants.RULE_06.CL.equals(planType) || 
					RuleConstants.RULE_06.CK.equals(planType) || 
					RuleConstants.RULE_06.CF.equals(planType) || 
					RuleConstants.RULE_06.T.equals(planType)  || 
					RuleConstants.RULE_06.C.equals(planType)){ 
					
					BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
					
					if(RuleConstants.RULE_06.CI.equals(planType)){
						/**
						 * Step 7 From Document SDS Topic 3.4.4 Check NML Type (CP or NCP)
						 * sumRiderInsuredCI = sumRiderInsuredCI.add(insured);
						 */
						if(RuleConstants.RULE_06.NCP.equals(docType)) {
							String tempDocType = getMapNmlTypeService.findDocType(planHeader.getNcType(), "");
							
							if(RuleConstants.RULE_06.NCP.equals(tempDocType)){ 
								
								if (planHeader.getUwRatio() != null) {
									ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
								}
//								sumRiderInsuredCI = getSumConceptILP(tempRider,percentUN,percentUL);
//								insured = ratio.multiply(insured);
//								
//								sumRiderInsuredCI = sumRiderInsuredCI.add(insured);
								
								MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempRider.getPlanCode());
								if(planTypeInvestMent != null) {
									BigDecimal sumInsuredILP = getSumConceptILP(tempRider,percentUN,percentUL);
									sumInsuredILP = ratio.multiply(sumInsuredILP);
									sumRiderInsuredCI = sumRiderInsuredCI.add(sumInsuredILP);
								}else{
									insured = ratio.multiply(insured);
									sumRiderInsuredCI = sumRiderInsuredCI.add(insured);
								}
								
								
							}
						}else {
							
							if (planHeader.getUwRatio() != null) {
								ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
							}
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempRider.getPlanCode());
							if(planTypeInvestMent != null) {
								BigDecimal sumInsuredILP = getSumConceptILP(tempRider,percentUN,percentUL);
								sumInsuredILP = ratio.multiply(sumInsuredILP);
								sumRiderInsuredCI = sumRiderInsuredCI.add(sumInsuredILP);
							}else{
								insured = ratio.multiply(insured);
								sumRiderInsuredCI = sumRiderInsuredCI.add(insured);
							}
						}
					}else {
						/**
						 * Step 7 From Document SDS Topic 3.4.4 Check NML Type (CP or NCP)
						 * sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
						 */
						if(RuleConstants.RULE_06.NCP.equals(docType)) {
							String tempDocType = getMapNmlTypeService.findDocType(planHeader.getNcType(), "");
							
							if(RuleConstants.RULE_06.NCP.equals(tempDocType)){ 
								
								if (planHeader.getUwRatio() != null) {
									ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
								}
								
								MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempRider.getPlanCode());
								if(planTypeInvestMent != null) {
									BigDecimal sumInsuredILP = getSumConceptILP(tempRider,percentUN,percentUL);
									sumInsuredILP = ratio.multiply(sumInsuredILP);
									sumRiderInsuredOther = sumRiderInsuredOther.add(sumInsuredILP);
								}else{
									insured = ratio.multiply(insured);
									sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
								}
								
//								sumRiderInsuredCI = getSumConceptILP(tempRider,percentUN,percentUL);
//								insured = ratio.multiply(insured);
//								
//								sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
							}
						}else {
							
							if (planHeader.getUwRatio() != null) {
								ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
							}
							
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(tempRider.getPlanCode());
							if(planTypeInvestMent != null) {
								BigDecimal sumInsuredILP = getSumConceptILP(tempRider,percentUN,percentUL);
								sumInsuredILP = ratio.multiply(sumInsuredILP);
								sumRiderInsuredOther = sumRiderInsuredOther.add(sumInsuredILP);
							}else{
								insured = ratio.multiply(insured);
								sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
							}
							
							
						}
					}	
				}
			}
		}
		SumInsure res = new SumInsure();
		sumBasicInsured = sumBasicInsured.add(sumRiderInsuredOther);
		sumRiderInsuredCI = sumRiderInsuredCI.add(sumRiderInsuredOther);
		if(sumBasicInsured.compareTo(sumRiderInsuredCI) >= 0) {
			sumInsured = sumBasicInsured;
			res.setSumInsure(sumInsured);
			res.setSumType(RuleConstants.PLAN_TYPE.BASIC);
		}else {
			sumInsured = sumRiderInsuredCI;
			res.setSumInsure(sumInsured);
			res.setSumType(RuleConstants.PLAN_TYPE.RIDER);
		}
		
		return res;
	}
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By ORD Group Channel
	 * Overload Method use in rule 14
	 * 
	 * @return sumInsured
	 */
	public BigDecimal getSumInsureByOrdGroupChannel(InsureDetail basicCurrent ,List<InsureDetail> riderCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC and RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {

			for(HistoryInsure temp:historyInsureList) {
				String groupType = temp.getPlanGroupType();
				Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
				
				if(set.contains(temp.getStatus())) {
					if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						if(hdr!=null) {
							
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {

								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						basicList.add(obj);
					}else {
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						if(hdr!=null) {
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {

								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						riderList.add(obj);
					}
				}
			}
		}
		
		// BASIC and RIDER sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredOther = BigDecimal.ZERO;
		BigDecimal sumInsured = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			if(RuleConstants.RULE_14.ORD.equals(tempBasic.getGroupChannelCode())){
				PlanHeader planHeader = tempBasic.getPlanHeader();
				
				if(planHeader != null ) {
					BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
					BigDecimal ratio = BigDecimal.ONE;
					
					if (planHeader.getUwRatio() != null) {
						BigDecimal hundred = new BigDecimal(100);
						BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
						if(uw.compareTo(hundred) >=0 ) {
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}
					}
					insured = ratio.multiply(insured);
					
					sumBasicInsured = sumBasicInsured.add(insured);
				}
			}
		}
		for(InsureDetail tempRider:riderList) {
			if(RuleConstants.RULE_14.ORD.equals(tempRider.getGroupChannelCode())){
				PlanHeader planHeader = tempRider.getPlanHeader();
				
				if(planHeader != null ) {
					String planType = planHeader.getPlanType();
					BigDecimal ratio = BigDecimal.ONE;
					
					if(RuleConstants.RULE_14.CL.equals(planType) || 
						RuleConstants.RULE_14.CK.equals(planType) || 
						RuleConstants.RULE_14.CF.equals(planType) || 
						RuleConstants.RULE_14.T.equals(planType)  || 
						RuleConstants.RULE_14.C.equals(planType)){
						BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
		
						if (planHeader.getUwRatio() != null) {
							BigDecimal hundred = new BigDecimal(100);
							BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
							if(uw.compareTo(hundred) >=0 ) {
								ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
							}
						}
						insured = ratio.multiply(insured);
						
						sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
					}
				}
			}
		}
		
		sumBasicInsured = sumBasicInsured.add(sumRiderInsuredOther);
		sumInsured = sumBasicInsured;

		return sumInsured;
	}
	
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured All Plan
	 * Overload Method use in rule 14
	 * 
	 * @return sumInsured
	 */
	public BigDecimal getSumInsureAllPlan(InsureDetail basicCurrent ,List<InsureDetail> riderCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC and RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {

			for(HistoryInsure temp:historyInsureList) {
				String groupType = temp.getPlanGroupType();
				Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
				
				if(set.contains(temp.getStatus())) {
					if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						if(hdr!=null) {
							
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {
								
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						basicList.add(obj);
					}else {
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						if(hdr!=null) {
							
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						riderList.add(obj);
					}
				}
			}
		}
		
		// BASIC and RIDER sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredOther = BigDecimal.ZERO;
		BigDecimal sumInsured = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			PlanHeader planHeader = tempBasic.getPlanHeader();
			
			if(planHeader != null ) {
				BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
				BigDecimal ratio = BigDecimal.ONE;
				
				if (planHeader.getUwRatio() != null) {
					BigDecimal hundred = new BigDecimal(100);
					BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
					if(uw.compareTo(hundred) >=0 ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
				}
				insured = ratio.multiply(insured);
				
				sumBasicInsured = sumBasicInsured.add(insured);
			}
		}
		for(InsureDetail tempRider:riderList) {
			PlanHeader planHeader = tempRider.getPlanHeader();
			
			if(planHeader != null ) {
				String planType = planHeader.getPlanType();
				BigDecimal ratio = BigDecimal.ONE;
				
				if(RuleConstants.RULE_14.CL.equals(planType) || 
					RuleConstants.RULE_14.CK.equals(planType) || 
					RuleConstants.RULE_14.CF.equals(planType) || 
					RuleConstants.RULE_14.T.equals(planType)  || 
					RuleConstants.RULE_14.C.equals(planType)){
					BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
	
					if (planHeader.getUwRatio() != null) {
						BigDecimal hundred = new BigDecimal(100);
						BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
						if(uw.compareTo(hundred) >=0 ) {
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}
					}
					insured = ratio.multiply(insured);
					
					sumRiderInsuredOther = sumRiderInsuredOther.add(insured);
				}
			}
		}
		
		sumBasicInsured = sumBasicInsured.add(sumRiderInsuredOther);
		sumInsured = sumBasicInsured;

		return sumInsured;
	}
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By Group
	 * Overload Method use in rule 14
	 * 
	 * @return sumInsured
	 */
	public BigDecimal getSumInsureAccident(InsureDetail basicCurrent ,List<InsureDetail> riderCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC and RIDER
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		List<InsureDetail> riderList = new ArrayList<InsureDetail>();
		basicList.add(basicCurrent);
		if(riderCurrent != null) {
			riderList.addAll(riderCurrent);
		}
		if(historyInsureList !=null) {

			for(HistoryInsure temp:historyInsureList) {
				String groupType = temp.getPlanGroupType();
				Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
				
				if(set.contains(temp.getStatus())) {
					if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType)) {
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						if(hdr!=null) {
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						basicList.add(obj);
					}else {
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						if(hdr!=null) {
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						riderList.add(obj);
					}
				}
			}
		}
		
		// BASIC and RIDER sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		BigDecimal sumRiderInsuredOther = BigDecimal.ZERO;
		BigDecimal sumInsured = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			PlanHeader planHeader = tempBasic.getPlanHeader();
			
			if(planHeader != null ) {
				BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
				BigDecimal ratio = BigDecimal.ONE;
				
				if (planHeader.getUwRatio() != null) {
					BigDecimal hundred = new BigDecimal(100);
					BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
					if(uw.compareTo(hundred) >=0 ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
				}
				insured = ratio.multiply(insured);
				sumBasicInsured = sumBasicInsured.add(insured);
			}
		}
		for(InsureDetail tempRider:riderList) {
			PlanHeader planHeader = tempRider.getPlanHeader();
			
			if(planHeader != null ) {
				String planType = planHeader.getPlanType();
				BigDecimal ratio = BigDecimal.ONE;
				
				if(RuleConstants.RULE_14.AA.equals(planType) || 
					RuleConstants.RULE_14.AB.equals(planType) || 
					RuleConstants.RULE_14.A1.equals(planType) || 
					RuleConstants.RULE_14.A2.equals(planType) || 
					RuleConstants.RULE_14.A3.equals(planType)){
					BigDecimal insured = new BigDecimal(tempRider.getInsuredAmount());
					
					if (planHeader.getUwRatio() != null) {
						BigDecimal hundred = new BigDecimal(100);
						BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
						if(uw.compareTo(hundred) >=0 ) {
							ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
						}
					}
					insured = ratio.multiply(insured);
					sumRiderInsuredOther =  sumRiderInsuredOther.add(insured);
					
				}
			}
		}
		
		sumBasicInsured = sumBasicInsured.add(sumRiderInsuredOther);
		sumInsured = sumBasicInsured;
		
		return sumInsured;
	}
	
	
	/**
	 * @param basicCurrent
	 * @param riderCurrent
	 * @param historyInsureList
	 *
	 * Filter group BASIC and RIDER
	 * SumInsured By Group
	 * Overload Method use in rule 14
	 * 
	 * @return sumInsured
	 */
	public BigDecimal getSumInsureHnw(InsureDetail basicCurrent ,List<HistoryInsure> historyInsureList) {
		
		// PlanCode divided to BASIC
		List<InsureDetail> basicList = new ArrayList<InsureDetail>();
		basicList.add(basicCurrent);
		if(historyInsureList !=null) {

			for(HistoryInsure temp:historyInsureList) {
				String groupType = temp.getPlanGroupType();
				String planCode = temp.getPlanCode();
				Set<String> set = new HashSet<String>(Arrays.asList("0","1","4","7","8","9","Z"));
				
				if(set.contains(temp.getStatus())) {
					if(RuleConstants.PLAN_TYPE.BASIC.equals(groupType) &&
						(planCode.equals("WON05D") || planCode.equals("WON05E") || planCode.equals("WON10D") || planCode.equals("WON10E") )) {
						InsureDetail obj = new InsureDetail();
						obj.setPlanCode(temp.getPlanCode());
						obj.setPremium("0");
						PlanHeader hdr = applicationCache.getPlanHeaderAppCashMap().get(temp.getPlanCode().trim());
						if(hdr!=null) {
							MsInvesmentPlanType planTypeInvestMent = applicationCache.getMsInvesmentPlanType().get(hdr.getPlanCode());
							if(planTypeInvestMent != null) {
								obj.setInsuredAmount(temp.getFaceAmount().toString());
							}else {
								obj.setInsuredAmount(temp.getSumInsure().toString());
							}
						}else {
							obj.setInsuredAmount(temp.getSumInsure().toString());
						}
						obj.setPlanHeader(hdr);
						basicList.add(obj);
					}
				}
			}
		}
		
		// BASIC  sumInsrued
		BigDecimal sumBasicInsured = BigDecimal.ZERO;
		for(InsureDetail tempBasic:basicList) {
			PlanHeader planHeader = tempBasic.getPlanHeader();
			
			if(planHeader != null ) {
				BigDecimal insured = new BigDecimal(tempBasic.getInsuredAmount());
				BigDecimal ratio = BigDecimal.ONE;
				
				if (planHeader.getUwRatio() != null) {
					BigDecimal hundred = new BigDecimal(100);
					BigDecimal uw = new BigDecimal(planHeader.getUwRatio());
					if(uw.compareTo(hundred) >=0 ) {
						ratio = new BigDecimal("0.01").multiply(new BigDecimal(planHeader.getUwRatio()));
					}
				}
				insured = ratio.multiply(insured);
				sumBasicInsured = sumBasicInsured.add(insured);
			}
		}
		return sumBasicInsured;
	}
	
	public BigDecimal getSumConceptILP(InsureDetail InsureDetail,BigDecimal percentUL, BigDecimal percentUN) {
		BigDecimal sumInsure = BigDecimal.ZERO;
		BigDecimal faceAmount = new BigDecimal(InsureDetail.getInsuredAmount());
		String plantype = InsureDetail.getPlanHeader().getPlanType();
		BigDecimal premium =  BigDecimal.ONE;
		if(InsureDetail.getPremium()==null)
		{
			premium = BigDecimal.ONE;
		}else
		{
			premium = new BigDecimal(InsureDetail.getPremium());
		}

		BigDecimal ratio = BigDecimal.ONE;
		if (InsureDetail.getPlanHeader().getUwRatio() != null) {
			ratio = new BigDecimal("0.01").multiply(new BigDecimal(InsureDetail.getPlanHeader().getUwRatio()));
		}
//		BigDecimal tempinsure = ratio.multiply(new BigDecimal(InsureDetail.getInsuredAmount()));
		
		if(RuleConstants.RULE_06.UL1.equalsIgnoreCase(plantype) || RuleConstants.RULE_06.UN1.equalsIgnoreCase(plantype))
		{
			sumInsure = sumInsure.add(faceAmount.subtract(premium));
		}
		else if(RuleConstants.RULE_06.ULR.equalsIgnoreCase(plantype) || RuleConstants.RULE_06.UNR.equalsIgnoreCase(plantype) || RuleConstants.RULE_06.UL1H.equalsIgnoreCase(plantype))
		{
			
			sumInsure = sumInsure.add(faceAmount);
		}
//		else
//		{
//			sumInsure = sumInsure.add(tempinsure);
//		}
		
		return sumInsure;
		
	}
}