package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule14InputValidation implements Serializable {

 
	private static final long serialVersionUID = 1L;
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
 
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
/*
1	planCode		รหัสแบบประกันหลัก
2	planCode (List)		รหัสแบบประกัน Rider
3	Insured		จำนวนเงินเอาประกัน
4	Insured		จำนวนเงินเอาประกันของสัญญาเพิ่มเติม
5	idCard		เลขประจำตัวประชาชนหรือเลขหนังสือเดินทางในกรณีที่เป็นคนต่างด้าว
6	requestDate		วันที่ขอเอาประกัน (วันที่เขียนเอกสารใบคำขอเอาประกันชีวิต)
7	clientNumber		รหัสของผู้เอาประกัน
8	groupChannelCode		ประเภทช่องทางการจำหน่าย


 * 
 */
		
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		 if( StringUtils.isBlank(request.getRequestBody().getAgentCode())) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" Agent Code Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR); 
			vlidation.agenCode=true;
		 } 	
		 
		 if( StringUtils.isBlank(request.getRequestBody().getAppNo()) ){
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" App No Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR); 
			vlidation.agenCode=true;
		 } 	
		 
		 if( request.getRequestBody().getPersonalData()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" PersonalData Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getPersonalData().getAge())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" PersonalData Age  Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 }  
		 } 		 
		 
		 
		 if( request.getRequestBody().getBasicInsureDetail()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" BasicInsure Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" BasicInsure PlanCode Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
			 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getInsuredAmount())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" BasicInsure InsuredAmount Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
		 
		 } 
	 
		 if(  request.getRequestBody().getRiderInsureDetails()!=null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{	
			 vlidation.rider_planCode=true;
			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
				 if( StringUtils.isBlank(tmp.getPlanCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" RiderInsure PlanCode Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				 }
				 
				 if( StringUtils.isBlank(tmp.getInsuredAmount())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE14+" RiderInsure InsuredAmount Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				 }
		 
			 }			 
		 }	
		

 
		 result.setErrorMessageList(errorMessageList);
		return result;
	}

	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}

	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
