package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.PBPersonalData;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule16InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
 
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
//		 if(StringUtils.isBlank(request.getRequestBody().getAgentCode())) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE16+"Agent Code Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR);
//				vlidation.agenCode=true;
//		 }
		 
			PersonalData personalData =request.getRequestBody().getPersonalData();
			
			 if( personalData==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PersonalData Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				 
			 }else {
				 if( StringUtils.isBlank(personalData.getIdCard())) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PersonalData idCard Cannot be null");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR); 
					vlidation.personData_idCard=true;
				 } 
			 }
			 
	
				PBPersonalData pbPersonalData =request.getRequestBody().getPayorBenefitPersonalData();
				
				 if( pbPersonalData==null) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData Cannot be null");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR); 
					 
				 }else {
					 if( StringUtils.isBlank(pbPersonalData.getIdCard())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData idCard Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.personData_idCard=true;
					 } 
					 
					 if( StringUtils.isBlank(pbPersonalData.getSex())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData sex Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.pb_sex=true;
					 } 					 
					 
					 if( StringUtils.isBlank(pbPersonalData.getAge())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData age Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.pb_age=true;
					 } 
					 
					 if( StringUtils.isBlank(pbPersonalData.getWeight())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData weight Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.pb_weight=true;
					 }
					 
					 if( StringUtils.isBlank(pbPersonalData.getPayorRelationship())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" PBPersonalData PayorRelationship Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.pb_relationship=true;
					 }
					  
				 }		 
			  
				 InsureDetail basicInsureDetail =request.getRequestBody().getBasicInsureDetail();
					
					 if( basicInsureDetail==null) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" BasicInsureDetail Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						 
					 }else {
						 if( StringUtils.isBlank(basicInsureDetail.getPlanCode())) {
							ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
							requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
							requestObjValidation.setErrorDesc(ServiceConstants.RULE16+" BasicInsureDetail planCode Cannot be null");
							errorMessageList.add(requestObjValidation);
							result.setStatus(ServiceConstants.VALIDATION_ERROR); 
							vlidation.personData_idCard=true;
						 } 
					 }


		result.setErrorMessageList(errorMessageList);
		return result;
	}
	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}
	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
