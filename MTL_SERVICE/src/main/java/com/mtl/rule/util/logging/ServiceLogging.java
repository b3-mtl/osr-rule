package com.mtl.rule.util.logging;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "SERVICE_LOGGING")
public class ServiceLogging {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SERVICE_LOGGING")
	@SequenceGenerator(sequenceName = "SEQ_SERVICE_LOGGING", allocationSize = 1, name = "SEQ_SERVICE_LOGGING")

	@Column(name = "ID")
	private Long id;
	@Column(name = "CHANNEL_CODE")
	private String channelCode;
	@Column(name = "REQUEST_JSON")
	private String requestJson;
	@Column(name = "RESPONSE_JSON")
	private String responseJson;
	@Column(name = "SECTION")
	private String section;
	@Column(name = "STATUS")
	private String status;
	@Column(name = "TIME_USAGE")
	private String timeUsage;
	@Column(name = "TRANSACTION_ID")
	private String transactionId;
	@Column(name = "CREATED_DATE")
	private Date createdDate = new Date();
	@Column(name = "RULE_LIST")
	private String ruleList;
	
	@Column(name = "COLLECTOR_USAGE_TIME")
	private String colectorUsageTime;
	@Column(name = "COLLECTOR_LIST")
	private String collectorList;
	@Column(name = "SERVICE_USAGE_TIME")
	private String serviceUsageTime;
	
	@Column(name = "ALL_RULE_EXECUTE_LOG")
	private String allRuleLog;
	
}
