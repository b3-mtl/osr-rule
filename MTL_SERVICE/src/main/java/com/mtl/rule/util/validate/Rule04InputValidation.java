package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule04InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
 
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
/*
1	agentCode	true	รหัสของตัวแทนประกันชีวิตที่ขายในรอบปัจจุบัน
2	planCode	true	รหัสแบบประกันหลัก (Basic)
3	planCode (List)	false	รหัสแบบประกันเพิ่มเติม (Rider)
4	premiumPayment	true	งวดการชำระเงิน
5	sumInsured	true	จำนวนเงินเอาประกัน ทั้งแบบประกันหลักและแบบประกันเพิ่มเติม

 * 
 */
		
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		 if( StringUtils.isBlank(request.getRequestBody().getAgentCode())) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" Agent Code Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR); 
			vlidation.agenCode=true;
		 } 	
		
		 if(  request.getRequestBody().getBasicInsureDetail()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure PlanCode Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getInsuredAmount())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure InsuredAmount Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_premiumPayment=true;
			 } 				 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPremiumPayment())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure PremiumPayment Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_premiumPayment=true;
			 } 			 
		 } 
	 
		 if( request.getRequestBody().getRiderInsureDetails()!=null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{		 
			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
				 if( StringUtils.isBlank(tmp.getPlanCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" RiderInsure PlanCode Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				 }
				 if( StringUtils.isBlank(tmp.getInsuredAmount())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" RiderInsure InsuredAmount Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.basic_premiumPayment=true;
					 } 	
				 if( StringUtils.isBlank(tmp.getPremiumPayment())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" RiderInsure PremiumPayment Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.basic_premiumPayment=true;
					 } 			 
			 }			 
		 }		
	 
//		 if( !vlidation.basicinsure&&request.getRequestBody().getBasicInsureDetail()==null) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR);
//				vlidation.basicinsure=true;
//		 } else { 
//			 if(!vlidation.basic_planCode&&StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" BasicInsure PlanCode Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//				vlidation.basic_planCode=true;
//			 } 
//			 
//			 if(!vlidation.basic_premiumPayment&&StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPremiumPayment())) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" PremiumPayment Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//				vlidation.basic_premiumPayment=true;
//			 } 			 
//		 } 
//	 
//		 if( request.getRequestBody().getRiderInsureDetails()==null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{		 
//			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
//				 if(!vlidation.rider_planCode&&StringUtils.isBlank(tmp.getPlanCode())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE04+" RiderInsure PlanCode Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//						vlidation.rider_planCode=true;
//				 }
// 		 
//			 }			 
//		 }
 
		 result.setErrorMessageList(errorMessageList);
		return result;
	}

	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}

	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
