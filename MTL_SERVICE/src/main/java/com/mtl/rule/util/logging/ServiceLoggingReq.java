package com.mtl.rule.util.logging;

import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceLoggingReq {
	
	@Id
	private String id;
	private String channelCode;	
	private String requestJson;
	private String responseJson;
	private String section;
	private String status;
	private String timeUsage;
	private String transactionId;
	private String ruleList;
	private String collectorUsageTime;
	private String collectorList;
	private String serviceUsageTime;
	
	private String allRuleLog;
}
