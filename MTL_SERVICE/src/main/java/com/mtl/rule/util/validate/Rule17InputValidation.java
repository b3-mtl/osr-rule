package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.BeneficiaryDetail;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule17InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
  
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		
 
//		 if( request.getRequestBody().getBeneficiaryDetails()==null||request.getRequestBody().getBeneficiaryDetails().size()==0) 	{		 
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE17+" Beneficiary  Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//		 }	else {
//			 for(BeneficiaryDetail tmp:request.getRequestBody().getBeneficiaryDetails()) {
//				 if( !vlidation.beneficiary_idCard&&StringUtils.isBlank(tmp.getIdCard())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE17+" Beneficiary ID Card Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR);  
//				 }
//				 if( !vlidation.beneficiary_firstName&&StringUtils.isBlank(tmp.getFirstName())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE17+" Beneficiary First Name Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR);  
//				 }
//				 if( !vlidation.beneficiary_lastName&&StringUtils.isBlank(tmp.getLastName())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE17+" Beneficiary Last Name Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR);  
//				 }
//				 if( !vlidation.beneficiary_relation&&StringUtils.isBlank(tmp.getRelation())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE17+" Beneficiary Relation Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR);  
//				 }
//		 
//			 }	
//		 }
 
//		result.setErrorMessageList(errorMessageList);
		return result;
	}
	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}
	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
