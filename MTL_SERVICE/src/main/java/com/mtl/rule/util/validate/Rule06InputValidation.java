package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule06InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
 
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
/*
agentCode
planCode
planCode (List)

 * 
 */
		
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
		
		 if( StringUtils.isBlank(request.getRequestBody().getAgentCode())) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" Agent Code Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR); 
			vlidation.agenCode=true;
		 } 	
		 
		 if( request.getRequestBody().getPersonalData()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" PersonalData Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getPersonalData().getIdCard())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" PersonalData ID Card  Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
			 
			 if( request.getRequestBody().getPersonalData().getAddressDetail()==null) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" PersonalData Address Cannot be null");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR);
					vlidation.basicinsure=true;
			 } else {
				 
				 if( StringUtils.isBlank(request.getRequestBody().getPersonalData().getAddressDetail().getProvinceCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" Province Code of Personal Data Address Code Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.basic_planCode=true;
					 } 
				 
			 }
		 
		 } 		 
		 
		 
		 if( request.getRequestBody().getBasicInsureDetail()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure PlanCode Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
			 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getInsuredAmount())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure InsuredAmount Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 }
//			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPremium())) {
//					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//					requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure Premium Cannot be null");
//					errorMessageList.add(requestObjValidation);
//					result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//					vlidation.basic_planCode=true;
//				 }
		 
		 } 
	 
		 if(  request.getRequestBody().getRiderInsureDetails()!=null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{	
			 vlidation.rider_planCode=true;
			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
				 if( StringUtils.isBlank(tmp.getPlanCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" RiderInsure PlanCode Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				 }
				 
				 if( StringUtils.isBlank(tmp.getInsuredAmount())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" RiderInsure InsuredAmount Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				 }
//				 if( StringUtils.isBlank(tmp.getPremium())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" RiderInsure Premium Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//						vlidation.rider_planCode=true;
//				 }
		 
			 }			 
		 }	
		
		
//	 
//		 if( !vlidation.basicinsure&&request.getRequestBody().getBasicInsureDetail()==null) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR);
//				vlidation.basicinsure=true;
//		 } else { 
//			 if(!vlidation.basic_planCode&&StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
//				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//				requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" BasicInsure PlanCode Cannot be null");
//				errorMessageList.add(requestObjValidation);
//				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//				vlidation.basic_planCode=true;
//			 } 
// 		 
//		 } 
//	 
//		 if( !vlidation.riderinsurelist&&request.getRequestBody().getRiderInsureDetails()==null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{	
//			 vlidation.rider_planCode=true;
//			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
//				 if(!vlidation.rider_planCode&&StringUtils.isBlank(tmp.getPlanCode())) {
//						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
//						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
//						requestObjValidation.setErrorDesc(ServiceConstants.RULE06+" RiderInsure PlanCode Cannot be null");
//						errorMessageList.add(requestObjValidation);
//						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
//						vlidation.rider_planCode=true;
//				 }
// 		 
//			 }			 
//		 }
 
		 result.setErrorMessageList(errorMessageList);
		return result;
	}

	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}

	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
