package com.mtl.rule.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ModelUtils {
	
	public static BigDecimal divideBigDecimal(BigDecimal a, BigDecimal b){
		return a.divide(b, 4, RoundingMode.HALF_UP);
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean checkNullList(List list){
		if(list != null && list.size()>0)
			return true;
		return false;
	}
	
	public static boolean isBlank(String s){
		return (s!=null&&!s.trim().equals(""))?true:false;
	}
	
	public static boolean isOneOf(String source,String target){
		boolean result=false;
		if(source==null||source.length()==0||"".equals(source)){
			result = false;
		}else{
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				result=true;
			}
		}		
		return result;
	}
	

	public static boolean isOneOfOrBlank(String source,String target){
		boolean result=false;
		if(source==null||source.length()==0||"".equals(source)){
			result = true;
		}else{
			source = ","+source+",";
			target = ","+target+",";
			int i = source.indexOf(target);
			if(i!=-1){
				result=true;
			}
		}		
		return result;
	}
	
}
