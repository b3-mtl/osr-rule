package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule13InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();	
 
	public  Validation inputValidation(UnderwriteRequest request ,ValidationConstants vlidation) {		
		
/*
1	planCode		true	รหัสแบบประกันหลัก (Basic)
2	planCode (List)	false	รหัสแบบประกันเพิ่มเติม (Rider)
3	idCard			true	เลขบัตรประชาชน
4	occupationCode	true	รหัสอาชีพ
5	sumInsured		true	จำนวนเงินเอาประกัน ทั้งแบบประกันหลักและแบบประกันเพิ่มเติม
*/
		
		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS); 
	 
		 if(  request.getRequestBody().getPersonalData()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" PersonalData Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getPersonalData().getIdCard())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" PersonalData ID Card Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 }
			 
			 if( StringUtils.isBlank(request.getRequestBody().getPersonalData().getOccupationCode())) {
					ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
					requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
					requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" PersonalData Occupation Code Cannot be null");
					errorMessageList.add(requestObjValidation);
					result.setStatus(ServiceConstants.VALIDATION_ERROR); 
					vlidation.basic_planCode=true;
				 }
		 } 
		
		 if(  request.getRequestBody().getBasicInsureDetail()==null) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" BasicInsure Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.basicinsure=true;
		 } else { 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getPlanCode())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" BasicInsure PlanCode Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_planCode=true;
			 } 
			 if( StringUtils.isBlank(request.getRequestBody().getBasicInsureDetail().getInsuredAmount())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" BasicInsure InsuredAmount Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR); 
				vlidation.basic_premiumPayment=true;
			 }
		 } 
	 
		 if( request.getRequestBody().getRiderInsureDetails()!=null&&request.getRequestBody().getRiderInsureDetails().size()>0) 	{		 
			 for(InsureDetail tmp:request.getRequestBody().getRiderInsureDetails()) {
				 if( StringUtils.isBlank(tmp.getPlanCode())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" RiderInsure PlanCode Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.rider_planCode=true;
				}
				 if( StringUtils.isBlank(tmp.getInsuredAmount())) {
						ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
						requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
						requestObjValidation.setErrorDesc(ServiceConstants.RULE13+" RiderInsure InsuredAmount Cannot be null");
						errorMessageList.add(requestObjValidation);
						result.setStatus(ServiceConstants.VALIDATION_ERROR); 
						vlidation.basic_premiumPayment=true;
				}		 
			 }			 
		 }		

		 result.setErrorMessageList(errorMessageList);
		return result;
	}

	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}

	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}
	
 
}
