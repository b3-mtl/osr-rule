package com.mtl.rule.util.logging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mtl.rule.util.logging.repository.ServiceLoggingRepository;

@Service
public class ServiceLoggingService {
	
	@Autowired
	private ServiceLoggingRepository serviceLoggingRepository;

	public void saveServiceLogging(ServiceLoggingReq serviceLoggingReq) {
		ServiceLogging dataSave = new ServiceLogging();
		dataSave.setChannelCode(serviceLoggingReq.getChannelCode());
		dataSave.setRequestJson(serviceLoggingReq.getRequestJson());
		dataSave.setResponseJson(serviceLoggingReq.getResponseJson());
		dataSave.setSection(serviceLoggingReq.getSection());
		dataSave.setStatus(serviceLoggingReq.getStatus());
		dataSave.setTimeUsage(serviceLoggingReq.getTimeUsage());
		dataSave.setTransactionId(serviceLoggingReq.getTransactionId());
		dataSave.setRuleList(serviceLoggingReq.getRuleList());
		
		dataSave.setServiceUsageTime(serviceLoggingReq.getServiceUsageTime());
		dataSave.setColectorUsageTime(serviceLoggingReq.getCollectorUsageTime());
		dataSave.setCollectorList(serviceLoggingReq.getCollectorList());
		dataSave.setAllRuleLog(serviceLoggingReq.getAllRuleLog());
		serviceLoggingRepository.save(dataSave);
	}

}
