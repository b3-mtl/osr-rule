package com.mtl.rule.util;

public class RuleConstants {

	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String NA = "N/A";
	public static final String OTHERWISE = "Otherwise";
	public static final String SYSTEM_NORMAL = "NORMAL";
	public static final String POLICY_STATUS = "0,1,4,6,7,8,9,Z,";
	
	public class PLAN_TYPE {
		public static final String BASIC = "BASIC";
		public static final String RIDER = "RIDER";
	 
	}
	
	public class RULE {
		public static final String RULE06 = "RULE06";
		public static final String RULE14 = "RULE14";
		public static final String RULE11 = "RULE11";
	 
	}
	
	public class RULE_03 {
		public static final String DIAG_PLAN_TYPE = "DIAGNOSIS_PLAN_TYPE";
		public static final String ACCIDENT_PLAN_TYPE = "ACCIDENT_PLAN_TYPE";
		public static final String ACCIDENT_PLAN_CODE = "ACCIDENT_PLAN_CODE";
		public static final String HEALTH_PLAN_TYPE = "HEALTH_PLAN_TYPE";
		public static final String OPD_PLAN_TYPE = "OPD_PLAN_TYPE";
		public static final String SAVING_PLAN_TYPE = "SAVING_PLAN_TYPE";
    	public static final String TERM_PLAN_TYPE = "TERM_PLAN_TYPE";
    	public static final String TDP_PLAN_TYPE = "TPD_PLAN_TYPE";
    	public static final String GIO_PLAN_TYPE = "GIO_PLAN_TYPE";    	
    	public static final String ACCIDENTAL = "ACCIDENTAL";
    	public static final String DEATH = "DEATH";

    	public static final String HEALTH_BENEFIT_DISTRIBUTED = "HEALTH_BENEFIT_DISTRIBUTED";
    	public static final String HEALTH_BENEFIT_FIXED = "HEALTH_BENEFIT_FIXED";    	
    	public static final String HEALTH_BENEFIT_SMART = "HEALTH_BENEFIT_SMART";

    	public static final String OTHER_TYPE = "OTHER_TYPE";
    	public static final String A1 = "A1";
    	public static final String A2 = "A2";
    	public static final String A3 = "A3";
    	public static final String AA = "AA";
    	public static final String AB = "AB";
    	public static final String PA_FAMILY = "PA_FAMILY";
    	public static final String ACCIDENTAL_PLANTYPE = "AA,AB,A1,A2,A3";
    	
    	public static final String RAD = "RAD";
    	public static final String RAE = "RAE";

    	
    	public static final String HS = "HS";
    	public static final String HM = "HM";
    	public static final String HB = "HB";
    	public static final String GB = "GB";
    	public static final String GH = "GH";
    	public static final String GT = "GT";
	}
	public class RULE_04 {
		public static final String IL = "IL";
		public static final String POS = "POS";
		public static final String PREMIUM_INDICATOR = "2";
		public static final String AGENT_CHANNEL_CODE = "19";
		public static final String PREMIUM_PAYMENT = "S";
		public static final String PLAN_GROUP_TYPE = "BASIC";
		public static final String PP_1 = "1";
		public static final String PP_3 = "3";
		public static final String PP_6 = "6";
		public static final String PP_Y = "Y";
		public static final String PP_S = "S";
	}
	public class RULE_06 {
		public static final String NC = "NC";
		public static final String NCP = "NCP";
		public static final String CD = "CD";
		public static final String CI = "CI";
		public static final String CL = "CL";
		public static final String CK = "CK";
		public static final String CF = "CF";
		public static final String T = "T";
		public static final String C = "C";
		public static final String F = "F";
		public static final String UL1 = "UL1";
		public static final String UN1 = "UN1";
		public static final String ULR = "ULR";
		public static final String UNR = "UNR";
		public static final String UL1H = "UL1H";
	}
	public class RULE_07 {
		public static final int FIXED_FILLTERPERCENT_MIB = 80;
		public static final int FIXED_FILLTERPERCENT_AMLO = 90;
		public static final String HNW = "HNW";
	}
	public class RULE_11{
		public static final String MARITAL_STATUS = "IL";
		public static final String DOCUMENT_CODE_MC  = "MC";
		public static final String MESSAGE_CODE_DT11211 = "DT11211";
		public static final String MESSAGE_CODE_DT11210 = "DT11210";
		
		public static final String MESSAGE_CODE_DT11001 = "DT11001";
		public static final String MESSAGE_CODE_016002 = "ก016002";
		
	}
	public class RULE_14 {
		public static final String APP_TYPE = "APP_TYPE";
		public static final String PLAN_CODE = "PLAN_CODE";
		public static final String PLAN_TYPE = "PLAN_TYPE";
		public static final String GROUP_CHANNEL = "GROUP_CHANNEL";
		public static final String CD = "CD";
		public static final String CI = "CI";
		public static final String CL = "CL";
		public static final String CK = "CK";
		public static final String CF = "CF";
		public static final String T = "T";
		public static final String C = "C";
		public static final String F = "F";
		public static final String AA = "AA";
		public static final String AB = "AB";
		public static final String A1 = "A1";
		public static final String A2 = "A2";
		public static final String A3 = "A3";
		public static final String ILP = "ILP";
		public static final String ORD = "ORD";
	}
	public class RULE_16 {
		public static final String TRUE_STRING = "true";
		public static final String FALSE_STRING = "false";
		public static final String PLANTYPE_RP = "RP";
		public static final String O_SEX = "O";
		public static final String C_SEX = "C";
		
	}
	public class MESSAGE_CODE{
		public static final String RULE_01 = "MSG01-01";
		public static final String RULE_02 = "MSG02-01";
		public static final String RULE_03 = "MSG03-01";
		public static final String DT03101 = "DT03101";
		public static final String DT03201 = "DT03201";
		public static final String DT03518 = "DT03518";
		public static final String DT03030 = "DT03030";
		public static final String DT0310Y = "DT0310Y";
		public static final String DT03103 = "DT03103";
		public static final String DT03102 = "DT03102";
		public static final String DT03304 = "DT03304";
		public static final String DT03305 = "DT03305";
		public static final String DT03307 = "DT03307";
		public static final String DT03308 = "DT03308";
		public static final String DT03309 = "DT03309";
		public static final String DT03310 = "DT03310";
		public static final String DT03312 = "DT03312";
		public static final String DT03313 = "DT03313";
		public static final String DT03316 = "DT03316";
		public static final String DT03317 = "DT03317";
		public static final String DT03318 = "DT03318";
		public static final String DT03319 = "DT03319";
		public static final String DT03504 = "DT03504";
		public static final String DT03505 = "DT03505";
		public static final String DT03506 = "DT03506";
		public static final String DT03507 = "DT03507";
		public static final String DT03508 = "DT03508";
		public static final String DT03520 = "DT03520";
		public static final String DT03532 = "DT03532";
		public static final String DT0400Y = "DT0400Y";
		public static final String DT04001 = "DT04001";
		public static final String DT04004 = "DT04004";
		public static final String DT04005 = "DT04005";
		public static final String DT0500Y = "DT0500Y";
		public static final String DT05003 = "DT05003";
		public static final String DT03533 = "DT03533";
		
		public static final String DTMG007 = "DTMG007";
		
		public static final String DT03036 = "DT03036";
		public static final String DT03006 = "DT03006";
		
		// Rule 7
		public static final String DT07001 = "DT07001";// DT07001	Rule 7 : ไม่ได้ระบุข้อมูลชื่อ-นามสกุล / รหัสบัตรประจำตัวประชาชนผู้เอาประกันภัย
		public static final String DT0700Y = "DT0700Y";// DT0700Y Rule 7 : ผู้ร้องขอไม่เคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข
		public static final String DT07002 = "DT07002";// DT07002 Rule 7 : ผู้ร้องขอเคยถูกปฏิเสธ และ/หรือ รับประกันแบบมีเงื่อนไข
		public static final String DT07003 = "DT07003";// DT07003 พบข้อมูลที่ต้องส่งปรึกษาฝ่ายพิจารณารับประกัน
		 
		
		public static final String RULE_08 = "MSG08-01";
		public static final String RULE_09 = "MSG09-01";
		public static final String RULE_10 = "MSG10-01";
		public static final String RULE_11 = "MSG11-01";
		public static final String RULE_12 = "MSG12-01";
		public static final String RULE_13 = "MSG13-01";
		public static final String RULE_14 = "MSG14-01";
		public static final String RULE_17 = "MSG17-01";
		
		public static final String DT06009 = "DT06009";//Rule 6 : อายุ $ ปี ไม่สามารถซื้อทุนประกันมากกว่า $ บาท ได้	
		public static final String DT06209 = "DT06209";//Rule 6 : ทุนตรวจสุขภาพรวมจำนวน $ บาท ต้องตรวจสุขภาพ ดังนี้ $ (เพื่ออนุมัติงานสำหรับ IL ให้isAutoApproved=true)
		
		
		
		
		
		
		public static final String DT00700Y = "DT00700Y";
		
		public static final String DT12001 = "DT12001";
		public static final String DT1200Y = "DT1200Y";
		
		public static final String DT14001 = "DT14001";//Rule 14 : ทุนรวมช่องทาง $1 จำนวน $2 บาท ขอเอกสารทางการเงิน $3
		public static final String DT14002 = "DT14002";//Rule 14 : ทุนรวมช่องทาง $ จำนวน $ บาท ขอสอบสภาวะ
		
		public static final String DT16001 = "DT16001";
		public static final String DT16002 = "DT16002";
		public static final String DT16003 = "DT16003";
		public static final String DT16004 = "DT16004";
		public static final String DT16005 = "DT16005";
		public static final String DT16006 = "DT16006";
		public static final String DT16007 = "DT16007";
		public static final String DT16008 = "DT16008";
		public static final String DT16009 = "DT16009";
		public static final String DT16010 = "DT16010";
		public static final String DT16013 = "DT16013";
		public static final String DT16014 = "DT16014";
		public static final String DT16018 = "DT16018";
		public static final String DT16301 = "DT16301";
		public static final String DT16302 = "DT16302";
		public static final String DT16017 = "DT16017";//Rule 16 (PB) : ผู้ชำระเบี้ย รหัสบัตรประชาชน : $ / ชื่อ-นามสกุล : $ เป็น Super VIP
		public static final String DT16020 = "DT16020";
		public static final String DT16021 = "DT16021";
		
		
		// Rule 13
		 
		public static final String DT13001 = "DT13001";
		public static final String DT13002 = "DT13002";
		public static final String DT13003 = "DT13003";
		public static final String DT13004 = "DT13004";
		public static final String DT13005 = "DT13005";
		
		// Rule 17
		public static final String DT17001 = "DT17001";
		public static final String DT17002 = "DT17002";
		public static final String DT17003 = "DT17003";
		public static final String DT17004 = "DT17004";
		
		// Rule 20
		public static final String DT20203 = "DT20203";
		public static final String DT20204 = "DT20204";
		public static final String DT20302 = "DT20302";
		public static final String DT20402 = "DT20402";
		public static final String DT20105 = "DT20105";
		public static final String DT20106 = "DT20106";
		public static final String DT20101 = "DT20101";
		public static final String DT20102 = "DT20102";
		public static final String DT20103 = "DT20103";
		public static final String DT20104 = "DT20104";
		public static final String DT20201 = "DT20201";
		public static final String DT20202 = "DT20202";
		public static final String DT20301 = "DT20301";
		public static final String DT20401 = "DT20401";
		
		
		// Rule 20
		public static final String DT21002 = "DT21002";
		public static final String DT21003 = "DT21003";
		public static final String DT21004 = "DT21004";
		
		//Rule 16
		public static final String DT16019  = "DT16019";

		
	}
	public static final String RULE_SPACE =".	";
	public static final String SUB_RULE_SPACE =".		";
}
