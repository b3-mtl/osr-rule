package com.mtl.rule.util.validate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mtl.api.utils.ServiceConstants;
import com.mtl.model.underwriting.ErrorMessageDesc;
import com.mtl.model.underwriting.InsureDetail;
import com.mtl.model.underwriting.PBPersonalData;
import com.mtl.model.underwriting.PersonalData;
import com.mtl.model.underwriting.UnderwriteRequest;

@Component
public class Rule11InputValidation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<ErrorMessageDesc> errorMessageList = new ArrayList<ErrorMessageDesc>();

	public Validation inputValidation(UnderwriteRequest request, ValidationConstants vlidation) {

		Validation result = new Validation();
		result.setStatus(ServiceConstants.VALIDATION_PASS);

		PersonalData personalData = request.getRequestBody().getPersonalData();

		if (personalData == null) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PersonalData Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR);

		} else {
			if (StringUtils.isBlank(personalData.getIdCard())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PersonalData idCard Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.personData_idCard = true;
			}

			if (StringUtils.isBlank(personalData.getSex())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PersonalData sex Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.personData_sex = true;
			}

			if (StringUtils.isBlank(personalData.getAge())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PersonalData age Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.personData_age = true;
			}
		}

		PBPersonalData pbPersonalData = request.getRequestBody().getPayorBenefitPersonalData();

		if (pbPersonalData == null) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PBPersonalData Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR);

		} else {
			if (StringUtils.isBlank(pbPersonalData.getIdCard())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " PBPersonalData idCard Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.personData_idCard = true;
			}

		}

		InsureDetail basicInsureDetail = request.getRequestBody().getBasicInsureDetail();

		if (basicInsureDetail == null) {
			ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
			requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
			requestObjValidation.setErrorDesc(ServiceConstants.RULE11 + " BasicInsureDetail Cannot be null");
			errorMessageList.add(requestObjValidation);
			result.setStatus(ServiceConstants.VALIDATION_ERROR);

		} else {
			if (StringUtils.isBlank(basicInsureDetail.getPlanCode())) {
				ErrorMessageDesc requestObjValidation = new ErrorMessageDesc();
				requestObjValidation.setErrorCode(ServiceConstants.VALIDATION_ERROR);
				requestObjValidation
						.setErrorDesc(ServiceConstants.RULE11 + " BasicInsureDetail planCode Cannot be null");
				errorMessageList.add(requestObjValidation);
				result.setStatus(ServiceConstants.VALIDATION_ERROR);
				vlidation.personData_idCard = true;
			}
		}

		result.setErrorMessageList(errorMessageList);
		return result;
	}

	public List<ErrorMessageDesc> getErrorMessageList() {
		return errorMessageList;
	}

	public void setErrorMessageList(List<ErrorMessageDesc> errorMessageList) {
		this.errorMessageList = errorMessageList;
	}

}
